import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { MultiSelectModule } from 'primeng/multiselect';
import {TreeModule} from 'primeng/tree';
import {RadioButtonModule} from 'primeng/radiobutton';

import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ContextMenuModule} from 'primeng/contextmenu';


@NgModule({
  declarations: [],

  imports: [
    CommonModule,
    TableModule,
    InputTextModule,
    ButtonModule,
    MultiSelectModule,
    TreeModule,

    ConfirmDialogModule,
    ContextMenuModule,
    RadioButtonModule
  ],

  exports: [
    CommonModule,
    TableModule,
    InputTextModule,
    ButtonModule,
    MultiSelectModule,
    TreeModule,
    ConfirmDialogModule,
    ContextMenuModule,
    RadioButtonModule 
  ]

  
 

})
export class PrimengModule { }
