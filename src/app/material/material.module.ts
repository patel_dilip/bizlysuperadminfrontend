import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule, MatSlideToggleModule, MatSelectModule, MatChipsModule, MatSidenavModule, MatToolbarModule, MatListModule, MatIconModule, MatDialog, MatDialogRef, MatDialogModule, MatRadioModule, MatMenuModule, MatCheckboxModule, MatAutocompleteModule, MatTabsModule} from '@angular/material';
import {MatChipInputEvent} from '@angular/material/chips';
// MatInputModule
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule , MatDatepickerModule, MatNativeDateModule, 
  MatTooltipModule, MatExpansionModule, 
} from '@angular/material';

import {DragDropModule} from '@angular/cdk/drag-drop';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatCardModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatChipsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatRadioModule,
    MatDatepickerModule, MatNativeDateModule,
    MatTooltipModule,
    MatDialogModule,
    MatMenuModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    DragDropModule,
    MatExpansionModule
  ],
  exports: [
    MatCardModule,
    MatStepperModule,
    MatFormFieldModule,
    MatDatepickerModule, MatNativeDateModule,
    MatInputModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatChipsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    MatRadioModule,
    MatMenuModule,
    MatCheckboxModule ,
    MatAutocompleteModule,
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    DragDropModule, 
    MatExpansionModule

  ]
})
export class MaterialModule { }
