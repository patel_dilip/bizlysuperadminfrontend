import { TestBed } from '@angular/core/testing';

import { EstablishmentTypeService } from './establishment-type.service';

describe('EstablishmentTypeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EstablishmentTypeService = TestBed.get(EstablishmentTypeService);
    expect(service).toBeTruthy();
  });
});
