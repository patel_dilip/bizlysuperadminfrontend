import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EmpService {

  roleData: any
  // userData: any
  posroleData: any
  receiptant: any

  // loan emp history
  emploanhistory: any
  empSalName: any
  constructor(private http: HttpClient) { }

  // Get all USer

//  getAllUsers(){
//   return this.http.get('../assets/bizly-data/bizlyUserData/allUserData.json')
//  }

  // ************************** ROLE RELATED SERVICES***********************************
  // ************************************************************************************

  // ADD ROLE

  addRole(obj){
    console.log("hello service", obj)
    return this.http.post("http://bizlypos.com:1300/bizlyemproles/addbizlyemprole",obj)
  }

  // GET ROLE

  getAllRole(){
    return this.http.get("http://bizlypos.com:1300/bizlyemproles/getallbizlyroles")
  }

  // ************************************************************************************

  // ************************** USER RELATED SERVICES***********************************
  // ************************************************************************************

  // GET ALL USERS
  getAllUsers(){
    return this.http.get("http://bizlypos.com:1300/bizuser/getallusers");
  }

}
