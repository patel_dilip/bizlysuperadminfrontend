import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  popAddCategData: any
  popSubCategData: any
  popAtrrCategData: any

  popMediaAddData: any = []
  selectedAlbums: any = []

  popVideoAddData: any = []
  selectedVideoAlbums: any = []
  
  layoutName: any

  selectedTables: any
  tabTableName: any

  tableObj: any


  constructor(private http: HttpClient) { }


  getAllDialCode(){
    return this.http.get('/assets/bizly-data/Dial-code/dial_code.json')
  }

  getAllRevenue(){
    return this.http.get('/assets/bizly-data/Revenue/revenue.json')
  }

  getAllEstablishment(){
    return this.http.get('/assets/bizly-data/Establishment-type/establishment.json')
  }

  getAllMenu(){
    return this.http.get('/assets/bizly-data/Menu-mgmt/menus.json')
  }
}
