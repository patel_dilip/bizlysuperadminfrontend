import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MoreinfoService {

  constructor(private http: HttpClient) { }

  getAllMoreInfo(){
    return this.http.get('/assets/bizly-data/More_info/more_info.json')
  }

  getAllServices(){
    return this.http.get('/assets/bizly-data/Services/services.json')
  }

  getAllRules(){
    return this.http.get('/assets/bizly-data/Rules/rules.json')
  }

  getAllPlaces(){
    return this.http.get('/assets/bizly-data/Rules/rules.json')
  }

  getAllAmbience(){
    return this.http.get('/assets/bizly-data/Ambience/ambience.json')
  }
}
