import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { runInNewContext } from 'vm';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddMenuFoodsService {
  editObject: any
  constructor(private http: HttpClient) { }
  ////////////////////////////////////////////// get admin ///////////////////
  getAdmin() {
    return this.http.get(environment.base_Url + "admin/getalladmins")
    //environment.getCuisines

  }

  //******************************************Country Json************************************************* */
  country_state_city():Observable<any> 
  {
    return this.http.get<any>('/assets/bizly-data/Dial-code/selected-country.json');
    }

  // *************************Menu managment FOOD APIS ************************************


  // ****************************************************************************************
  // ******************************* CUISINES API *****************************************
  // ****************************************************************************************

  // GET API Chips of CUSINES in MENU PAGE ADD FOOD
  getCuisines() {
    return this.http.get(environment.base_Url + "cuisine/getallcuisines")
    //environment.getCuisines

  }

  // POST API of CUISINES IN MENU Page ADD CUISINES
  postCuisines(addCuisineObject) {
    console.log('add cuisines object', addCuisineObject);

    return this.http.post(environment.base_Url + "cuisine/addcuisine", addCuisineObject)
  }


  //PUT CUISINES API
  updateCuisines(id, data) {
    return this.http.put(environment.base_Url + "cuisine/updatecuisine/" + id, data)
  }

  //delete cuisine
  deleteCuisine(cuisineID) {
    return this.http.delete(environment.base_Url + "cuisine/deletecuisine/" + cuisineID)
  }

  // **************************************************************************************
  // ********************************** TAGS API *************************************
  // **************************************************************************************

  // GET API FOR TAGS CHIPS 
  getTags() {
    return this.http.get(environment.base_Url + 'tag/getalltags')
  }

  // Post API For TAGS
  postTags(addtagObject) {
    console.log('add tag object', addtagObject);
    return this.http.post(environment.base_Url + 'tag/addtag', addtagObject)
  }





  // **********************************************************************************
  // ********************************* POST API FOR CATEGORY ****************************
  // ************************************************************************************


  postCategory(addCategoryObject) {
    console.log('add category object', addCategoryObject);

    return this.http.post(environment.base_Url + 'category/addcategory', addCategoryObject)

  }

  updateCategory(id, categoryData) {
    return this.http.put(environment.base_Url + "category/updatecategory/" + id, categoryData)
  }


  gelAllCategory() {
    return this.http.get(environment.base_Url + "category/getallcategories")
  }

  deleteCategory(categoryID) {
    return this.http.delete(environment.base_Url + "category/deletecategory/" + categoryID)
  }




  // **********************************************************************************************
  // *********************************** API FOR VARIENT CONTENT *************************************
  // ***************************************************************************************************

  //post varient Content in menu page => food => add varient => add new
  postVarientContent(varientContentObject) {
    return this.http.post(environment.base_Url + "varientcontent/addcontent", varientContentObject)
  }

  //get varient content in menu page => food => add varient
  getVarientContent() {
    return this.http.get(environment.base_Url + "varientcontent/getallcontents")
  }






  // **********************************************************************************************
  // ****************************************** API FOR VARIENT ****************************************
  // ************************************************************************************************

  //post varient API in menu page =>food =>add varient
  postVarient(varientObject) {
    return this.http.post(environment.base_Url + "varient/addvarient", varientObject)
  }

  //get All varients in menu page
  getAllvarients() {
    return this.http.get(environment.base_Url + "varient/getallvarients")
  }


  //update varient
  updateVarient(id, varientData) {
    return this.http.put(environment.base_Url + "varient/updatevarient/" + id, varientData)
  }


  deleteVarient(varientID) {
    return this.http.delete(environment.base_Url + "varient/deletevarient/" + varientID)
  }



  // ********************************************************************************
  // ******************************** API FOR DISH **************************************
  // *******************************************************************************

  //post dish menupage=> add dishes
  postDish(dishObject) {
    return this.http.post(environment.base_Url + "dish/adddish", dishObject)
  }

  //get all dishes API =>menu management =>food
  getAllDishes() {
    return this.http.get(environment.base_Url + "dish/getalldishes")
  }

  updateDish(id, dishData) {
    return this.http.put(environment.base_Url + "dish/updatedish/" + id, dishData)
  }


  deleteDish(dishID) {
    return this.http.delete(environment.base_Url + "dish/deletedish/" + dishID)
  }




  // *************************************************************************************
  // ************************************* API FOR ADD ONS ****************************************
  // *************************************************************************************8

  //post addon api
  postAddOn(addOnObject) {
    return this.http.post(environment.base_Url + "adon/addadon", addOnObject)
  }

  //get all addons API
  getAllAddons() {
    return this.http.get(environment.base_Url + "adon/getalladons")
  }

  //update addon
  updateAddon(id, addonData) {
    return this.http.put(environment.base_Url + "adon/updateadon/" + id, addonData)
  }

  deleteAddOn(addonID) {
    return this.http.delete(environment.base_Url + "adon/deleteadon/" + addonID)
  }







  // ****************************************************************************
  // *************************** Liquor Attrbute ****************************
  // ***********************************************************************


  // ************************************API FOR ADD ROOT DRINK *******************

  // post root drink api
  postRootDrink(addRootDrinkObject) {
    return this.http.post(environment.base_Url + 'drinktype/adddrinktype', addRootDrinkObject)
  }

  // get root drinks api
  getAllRootDrinks() {
    return this.http.get(environment.base_Url + 'drinktype/getalldrinktypes')
  }

  // ************************************API FOR ADD VARIENT DRINK *******************

  //PUT Varient name api
  putVarientDrink(id, liquorVarientName) {
    return this.http.put(environment.base_Url + 'drinktype/addliquorvarient/' + id, liquorVarientName)
  }


  //  *******************************************API SUB VARIENT DRINK**********************

  putSubVarientDrink(d_id, v_id, liquorSubVarientName) {
    return this.http.put(environment.base_Url + "drinktype/addliquorsubvarient/" + d_id + "/" + v_id, liquorSubVarientName)
  }

  //  *******************************************API SUB SUB DRINK**********************

  putSubSubDrink(d_id, s_id, liquorSubSubVarientType) {
    return this.http.put(environment.base_Url + "drinktype/addliquorsubsubvarient/" + d_id + "/" + s_id, liquorSubSubVarientType)
  }


  //  *********************************API add brand*************************
  postBrand(addBrandObj) {
    return this.http.post(environment.base_Url + "liquorbrand/addbrand", addBrandObj)
  }

  //***************************** API get all brand ************************************
  getAllBrand() {
    return this.http.get(environment.base_Url + "liquorbrand/getallbrands")
  }


  // **************************** Delete Brand ***************************
  deleteLiquorBrand(brandID) {
    return this.http.delete(environment.base_Url + "liquorbrand/deletebrand/" + brandID)
  }

  getSingleLiquorBrand(brandID) {
    return this.http.get(environment.base_Url + "liquorbrand/getbrand/" + brandID)
  }

  // ******************************** API get brand by category ********************
  getBrandsByCategory(categoryOBJ) {
    return this.http.post(environment.base_Url + "liquorbrand/getbrandnamefromcategories", categoryOBJ)
  }
  getBrandsByCategoryinretailFood(categoryOBJ) {
    return this.http.post(environment.base_Url + "retailfoodbrand/getretailfoodbrandfromcategories", categoryOBJ)
  }
  getBrandsByCategoryinretailbeverages(categoryOBJ) {
    return this.http.post(environment.base_Url + "retailbeveragebrand/getretailbeveragebrandfromcategory", categoryOBJ)
  }
  //ADD LIQUOR PRODUCT
  addLiquorProduct(productObj) {
    return this.http.post(environment.base_Url + "product/addproduct", productObj)
  }

  //GET ALL LIQUOR
  getAllLiquorProducts() {
    return this.http.get(environment.base_Url + "product/getallproducts")
  }

  //delete liquor product
  deleteLiquorProduct(productID) {
    return this.http.delete(environment.base_Url + "product/deleteproduct/" + productID)
  }

  //update product
  updateProductLiquor(productID, productObj) {
    return this.http.put(environment.base_Url + "product/updateproduct/" + productID, productObj)
  }

  //update liquor brand
  updateLiquorBrand(brandID, brandObj){
    console.log("brand obj", brandObj);
    
    return this.http.put(environment.base_Url + "liquorbrand/updatebrand/" + brandID, brandObj)
  }


  // Soft delete liquor brand
  softDeleteLiquorBrand(brandID, status){
    return this.http.put(environment.base_Url + "liquorbrand/softdeletebrand/" + brandID, status)
  }

  // soft delete liquor product
  softDeleteLiquorProduct(productID, status){
    return this.http.put(environment.base_Url + "product/softdeleteproduct/" + productID, status)
  }


  // **************************************************************************************************
  // **************************************  ATTRBUTE  ************************************************
  // **************************************************************************************************


  // *****************************Add liquor attribute POST API *********************

  postLiquorAttribute(liquorAttributeObj) {
    return this.http.post(environment.base_Url + "attribute/addAttribute", liquorAttributeObj)
  }

  // ********************************** Get All Liquor Attribute  *******************************
  getAllLiquorAttribute() {
    return this.http.get(environment.base_Url + "attribute/getAllAttribute")
  }

  // **************************** Add Liquor Attribute Group **************************************
  postAttributeGroup(attributeGroupObj) {
    return this.http.post(environment.base_Url + "attributegroup/addattributegroup", attributeGroupObj)
  }

  // ***************************** Get All Liquor Attribute Group ***************************
  getLiquorAttributeGroup() {
    return this.http.get(environment.base_Url + "attributegroup/getallattributegroups")
  }
  getLiquorAttributeGroupwithSetName(name,attributeType) {
    return this.http.get(environment.base_Url + "attributegroup/getallassigngroupset/"+name+"/"+attributeType )
  }

  // ***************************** Add Liquor Attribute Set **************************************
  postAttriubteSet(attributeSetObj) {
    return this.http.post(environment.base_Url + "attributeset/addattributeset", attributeSetObj)
  }

  // router.get('/getallassigngroupset/:attributeSetName', attributesetCtrl.getAllAssignGroupset)

  // **************************** Get All Liquor Attribute set ***********************************
  getAllLiquorAttributeSets() {
    return this.http.get(environment.base_Url + "attributeset/getallattributes")
  }

  // **************************** Update Liquor Attribute filterable  true or false****************************
  updateLiquorAttributeIsFilterable(attributeId, data) {
    return this.http.put(environment.base_Url + "attribute/isfilterable/" + attributeId, data)
  }

  // ************************** Update Liquor Attribute Searchable true or false *******************************
  updateLiquorAttributeIsSearchable(attributeId, data) {
    return this.http.put(environment.base_Url + "attribute/issearchable/" + attributeId, data)
  }

  // ************************** get single attribute group ******************************************
  getSingleAttributeGroup(groupid) {
    return this.http.get(environment.base_Url + "attributegroup/getattributegroup/" + groupid)
  }

  // ************************ Get Single Attribute ******************************************
  getSingleAttribute(attributeID) {
    return this.http.get(environment.base_Url + "attribute/getAttribute/" + attributeID)
  }

  //deleteAttribute
  deleteAttribute(attributeID) {
    return this.http.delete(environment.base_Url + "attribute/deleteAttribute/" + attributeID)
  }

  //deletegroup
  deleteGroup(id) {
    return this.http.delete(environment.base_Url + "attributegroup/delteattributegroup/" + id)
  }

  //delete Attribute Set
  deleteAttributeSet(setID) {
    return this.http.delete(environment.base_Url + "attributeset/deleteattribute/" + setID)
  }

  //update attribute
  updateAttribute(attributeID, attributeObj) {
    return this.http.put(environment.base_Url + "attribute/updateAttribute/" + attributeID, attributeObj)
  }

  // update attribute set
  updateAttributeSet(attributeSetID, attributeSetObj) {
    return this.http.put(environment.base_Url + "attributeset/updateattribute/" + attributeSetID, attributeSetObj)
  }

  //SOFT DELETE ATTRIBUTE
 softDeleteAttribute(attributeID, status){
   return this.http.put(environment.base_Url + "attribute/softdeleteAttribute/" +attributeID, status)
 }

  //SOFT DELETE ATTRIBUTE SET
softDeleteAttributeSet(attributeSetID, status){
  return this.http.put(environment.base_Url + "attributeset/softdeleteattribute/" +attributeSetID, status)
}







  // *******************************************************
  // ******************** ASSET INVENTORY *********************
  //********************************************************** */

  //get all categories of asset inventory
  getAllAssetInventoryCategories() {
    return this.http.get(environment.base_Url + "inventoryassets/getallrootinventoryassetscategory")
  }

  //add root category of asset inventory
  addRootCategoryAssetInventory(dataOBJ) {
    return this.http.post(environment.base_Url + "inventoryassets/addrootinventoryassetscategoryname", dataOBJ)
  }

  //add sub category of asset inventory
  addSubCategoryAssetInventory(rootID, subCatobj) {
    return this.http.put(environment.base_Url + "inventoryassets/addinventoryassetssubcategory/" + rootID, subCatobj)
  }

  //add sub sub category of asset inventory
  addsubsubCategoryAssetInventory(rootID, subCatID, subSubObj) {
    return this.http.put(environment.base_Url + "inventoryassets/addinventoryassetssubsubcategory/" + rootID + "/" + subCatID, subSubObj)
  }

  //add sub sub category type of asset inventory 
  addSubSubCategoryTypeAssetInventory(rootID, subSubID, subSubTypeObj) {
    return this.http.put(environment.base_Url + "inventoryassets/addinventoryassetssubsubcategorytype/" + rootID + "/" + subSubID, subSubTypeObj)
  }

  ////////// ADD EQUIPMENT ////////////////
  addEquipmentAssetInventory(equipmentObj) {
    return this.http.post(environment.base_Url + "equipment/addequipment", equipmentObj)
  }

  /////////////// GET ALL EQUIPMENT //////////////////
  getAllEquipmentAssetInventory() {
    return this.http.get(environment.base_Url + "equipment/getallequipments")
  }

  ///////////////// Delete Equipment //////////////////
  deleteEquipmentAssetInventory(equipmentID) {
    return this.http.delete(environment.base_Url + "equipment/deleteequipment/" + equipmentID)
  }

  updateEquipmentAssetInventory(equipmentID, equipmentObj) {
    return this.http.put(environment.base_Url + "equipment/updateequipment/" + equipmentID, equipmentObj)
  }







  // *******************************************************
  // ******************** FOOD INVENTORY *********************
  //********************************************************** */



  //get all food inventory categories
  getAllFoodInventoryCategories() {
    return this.http.get(environment.base_Url + "fooodinventory/getallfoodcategories")
  }

  //add root category of food inventory
  addRootCategoryFoodInventory(rootObj) {
    return this.http.post(environment.base_Url + "fooodinventory/addfoodcategoryname", rootObj)
  }

  //add sub category of food inventroy
  addSubCategoryFoodInventory(rootID, subCatobj) {
    return this.http.put(environment.base_Url + "fooodinventory/addsubcategory/" + rootID, subCatobj)
  }

  //add sub sub category of food inventory
  addSubSubcategoryFoodInventory(rootID, subCatID, subSubObj) {
    return this.http.put(environment.base_Url + "fooodinventory/addsubsubcategory/" + rootID + "/" + subCatID, subSubObj)
  }

  //add sub sub category type of food inventory
  addSubSubCategoryTypeFoodInventory(rootID, subSubID, subSubTypeObj) {
    return this.http.put(environment.base_Url + "fooodinventory/addsubsubcategorytype/" + rootID + "/" + subSubID, subSubTypeObj)
  }


  //////// ADD INGREDIENT //////////////
  addIngredientFoodInventory(ingredientObj) {
    return this.http.post(environment.base_Url + "ingredientinventory/addinventoryingredient", ingredientObj)
  }

  /////////// GET ALL INGREDIENT /////////////////
  getAllIngredientFoodInventory() {
    return this.http.get(environment.base_Url + "ingredientinventory/getallinventoryingredient")
  }

  ////// ADD BRAND FOOD INVENTORY /////////////
  addBrandFoodInventory(brandObj) {
    return this.http.post(environment.base_Url + "brandinventory/addinventorybrand", brandObj)
  }

  //////// GET ALL BRAND /////////////////
  getAllBrandFoodInventory() {
    return this.http.get(environment.base_Url + "brandinventory/getallinventorybrand")
  }

  /////////////// ADD ITEM ///////////////
  addItemFoodInventory(itemObj) {
    return this.http.post(environment.base_Url + "iteminventory/addinventoryitem", itemObj)
  }

  ///////////// Get All Items //////////////
  getAllItemsFoodInventory() {
    return this.http.get(environment.base_Url + "iteminventory/getallinventoryitem")
  }


  /////////// Delete Ingredient /////////////////
  deleteIngredientFoodInventory(ingredeintID) {
    return this.http.delete(environment.base_Url + "ingredientinventory/deleteinventoryingredient/" + ingredeintID)
  }

  /////////////////// Delete Item ///////////////
  deleteItemFoodInventory(itemID) {
    return this.http.delete(environment.base_Url + "iteminventory/deleteinventoryitem/" + itemID)
  }

  ////////////////// UPDATE INVENTORY /////////////////////////
  updateIngredientInventory(ingredientID, ingredientObj) {
    return this.http.put(environment.base_Url + "ingredientinventory/updateinventoryingredient/" + ingredientID, ingredientObj)
  }

  ////////////////// UPDATE ITEM ///////////////////////
  updateItemFoodInventory(itemID, itemObj) {
    return this.http.put(environment.base_Url + "iteminventory/updateinventoryitem/" + itemID, itemObj)
  }

  ////////////////////////////  SOFT DELETE INGREDIENT /////////////////////////////////////
 softDeleteIngredient(ingredientID, status){
   return this.http.put(environment.base_Url + "ingredientinventory/softdeleteinventoryingredient/" + ingredientID, status)
 }


  //************************************************************************************************* */
  // *************************************************************************************************
  // *********************************** BEVERAGES ***************************************************
  // ************************************************************************************************
  // **************************************************************************************************


  // get all beverages categories
  getAllBeverageCategories() {
    return this.http.get(environment.base_Url + "beveragescategory/getallrootbeveragescategory")
  }

  //ADD ROOT CATEGORY
  addRootCategoryBeverage(rootObj) {
    return this.http.post(environment.base_Url + "beveragescategory/addrootbeveragescategoryname", rootObj)
  }

  //ADD SUB CATEGORY
  addSubCategoryBeverage(rootID, subCategoryObj) {
    return this.http.put(environment.base_Url + "beveragescategory/addbeveragescategorysubcategory/" + rootID, subCategoryObj)
  }

  // ADD SUB SUB CATEGORY
  addSubSubCategoryBeverages(rootID, SubCatID, subSubCategoryObj) {
    return this.http.put(environment.base_Url + "beveragescategory/addbeveragescategorysubsubcategory/" + rootID + "/" + SubCatID, subSubCategoryObj)
  }

  // ADD SUB SUB CATEGORY TYPE
  addSubSubCategoryTypeBeverages(rootID, subSubID, subSubTypeObj) {
    return this.http.put(environment.base_Url + "beveragescategory/addbeveragescategorysubsubcategorytype/" + rootID + "/" + subSubID, subSubTypeObj)
  }

  // ADD BEVERAGE BRAND
  addBeverageBrand(brandObj) {
    return this.http.post(environment.base_Url + "beveragebrand/addbeveragebrand", brandObj)
  }

  // GET ALL BEVERAGES BRANDS
  getAllBeverageBrands() {
    return this.http.get(environment.base_Url + "beveragebrand/getallbeveragebrand")
  }

  //update beverage brands
  updateBeverageBrand(brandID, brandObj) {
    return this.http.put(environment.base_Url + "beveragebrand/updatebeveragebrand/" + brandID, brandObj)
  }

  // ADD BEVERAGE PRODUCT
  addBeverageProduct(productObj) {
    return this.http.post(environment.base_Url + "beverageproduct/addbeverageproduct", productObj)
  }

  // GET ALL BEVERAGE PRODUCTS
  getAllBeverageProducts() {
    return this.http.get(environment.base_Url + "beverageproduct/getallbeverageproduct")
  }

  // UPDATE BEVERAGE PRODUCT
  updateProductBeverage(productID, productObj) {
    return this.http.put(environment.base_Url + "beverageproduct/updatebeverageproduct/" + productID, productObj)
  }

  // ADD BEVERAGE
  addBeverage(beverageObj){
    return this.http.post(environment.base_Url + "beverage/addbeverages", beverageObj)
  }

  // GET ALL BEVERAGES
  getAllBeverages(){
    return this.http.get(environment.base_Url + "beverage/getallbeverages")
  }

  // UPDATE BEVERAGE
  updateBeverage(beverageID, beverageObj){
    return this.http.put(environment.base_Url + "beverage/updatebeverage/" + beverageID, beverageObj)
  }

// SOFT DELETE BEVERAGE
  softDeleteBeverage(beverageID, status){
    return this.http.put(environment.base_Url + "beverage/softdeletebeverage/" + beverageID, status)
  }

//SOFT DELETE BEVERAGE PRODUCT
softDeleteBeverageProduct(productID, status){
  return this.http.put(environment.base_Url + "beverageproduct/softdeletebeverageproduct/" + productID, status)
}

// SOFT DELETE BEVERAGE BRAND
softDeleteBeverageBrand(brandID, status){
  return this.http.put(environment.base_Url + "beveragebrand/softdeletebeveragebrand/" + brandID, status)
}





  //********************************************************************************************* */
  // **********************************************************************************************
  // ********************************** RETAIL FOOD *********************************************
  // *********************************************************************************************
  // *********************************************************************************************


  //get all retail food categories
  getAllRetailFoodCategories() {
    return this.http.get(environment.base_Url + "retailfood/getallrootretailfoodcategory")
  }

  //ADD ROOT CATEGORY
  addRootCategoryRetailFood(rootObj) {
    return this.http.post(environment.base_Url + "retailfood/addrootretailfoodcategoryname", rootObj)
  }

  // ADD SUB CATEGORY
  addSubCategoryRetailFood(rootID, subCategoryObj) {
    return this.http.put(environment.base_Url + "retailfood/addretailfoodsubcategory/" + rootID, subCategoryObj)
  }

  // ADD SUB SUB CATEGORY
  addSubSubCategoryRetailFood(rootID, subCatID, subSubCategoryObj) {
    return this.http.put(environment.base_Url + "retailfood/addretailfoodsubsubcategory/" + rootID + "/" + subCatID, subSubCategoryObj)
  }

  // ADD SUB SUB CATEGORY TYPE
  addSubSubCategoryTypeRetailFood(rootID, subSubCatID, subSubTypeObj) {
    return this.http.put(environment.base_Url + "retailfood/addretailfoodsubsubcategorytype/" + rootID + "/" + subSubCatID, subSubTypeObj)
  }

  //  ADD RETAIL FOOD BRAND
  addRetailFoodBrand(brandObj) {
    return this.http.post(environment.base_Url + "retailfoodbrand/addretailfoodbrand", brandObj)
  }

  // GET ALL RETAIL FOOD BRANDS
  getAllRetailFoodBrands() {
    return this.http.get(environment.base_Url + "retailfoodbrand/getallretailfoodbrand")
  }

  //UPDATE RETAIL FOOD BRAND
  updateRetailFoodBrand(brandID, brandObj) {
    return this.http.put(environment.base_Url + "retailfoodbrand/updateretailfoodbrand/" + brandID, brandObj)
  }

  //ADD RETAIL FOOD PRODUCT
  addRetailFoodProduct(productObj) {
    return this.http.post(environment.base_Url + "retailfoodproduct/addretailfoodproduct", productObj)
  }

  // GET ALL RETAIL FOOD PRODUCTS
  getAllRetailFoodProducts() {
    return this.http.get(environment.base_Url + "retailfoodproduct/getallretailfoodproduct")
  }

  // UPDATE RETAIL FOOD Product
  updateRetailFoodProduct(productID, productObj) {
    return this.http.put(environment.base_Url + "retailfoodproduct/updateretailfoodproduct/" + productID, productObj)
  }

  // SOFT DELETE RETAIL FOOD PRODUCT
  softDeleteRetailFoodProduct(productID, status){
    return this.http.put(environment.base_Url + "retailfoodproduct/softdeleteretailfoodproduct/" + productID, status)
  }

  //SOFT DELETE RETAIL FOOD BRAND
  softDeleteRetailFoodBrand(brandID, status){
    return this.http.put(environment.base_Url + "retailfoodbrand/softdeleteretailfoodbrand/" + brandID, status)
  }

 //********************************************************************************************* */
  // **********************************************************************************************
  // ********************************** Retail beverages Brand*********************************************
  // *********************************************************************************************
  // *********************************************************************************************

addRetailBeveragesBrand(brandObj) {
  return this.http.post(environment.base_Url + "retailbeveragebrand/addretailbeveragebrand", brandObj)
}
getAllRetailBeveragesBrands() {
  return this.http.get(environment.base_Url + "retailbeveragebrand/getallretailbeveragebrand")
}
getRetailBeveragesBrands(id) {
  return this.http.get(environment.base_Url + "retailbeveragebrand/getretailbeveragebrand/"+id)
}
updateRetailBeveragesBrand(ID, Obj) {
  return this.http.put(environment.base_Url + "retailbeveragebrand/updateretailbeveragebrand/" + ID, Obj)
}

// SOFT DELETE RETAIL beverages PRODUCT
softDeleteRetailBeveragesBeand(ID, status){
  return this.http.put(environment.base_Url + "retailbeveragebrand/deleteretailbeveragebrand/" + ID, status)
}

 //********************************************************************************************* */
  // **********************************************************************************************
  // ********************************** Retail beverages Product*********************************************
  // *********************************************************************************************
  // *********************************************************************************************

  addRetailBeveragesProduct(Obj) {
    return this.http.post(environment.base_Url + "retailbeverageproduct/addretailbeverageproduct", Obj)
  }
  getAllRetailBeveragesProduct() {
    return this.http.get(environment.base_Url + "retailbeverageproduct/getallretailbeverageproduct")
  }
  getRetailBeveragesProduct(id) {
    return this.http.get(environment.base_Url + "retailbeverageproduct/getretailbeverageproduct/"+id)
  }
  updateRetailBeveragesProduct(ID, Obj) {
    return this.http.put(environment.base_Url + "retailbeverageproduct/updateretailbeverageproduct/" + ID, Obj)
  }
  
  // SOFT DELETE RETAIL beverages PRODUCT
  softDeleteRetailBeveragesProduct(ID, status){
    return this.http.put(environment.base_Url + "retailbeverageproduct/deleteretailbeverageproduct/" + ID, status)
  }



   //********************************************************************************************* */
  // **********************************************************************************************
  // ********************************** CHANGE STATUS FOOD *********************************************
  // *********************************************************************************************
  // *********************************************************************************************

   
  softDeleteCusines(ID, status){
    console.log(">>>>>>",ID, status);
    
    return this.http.put(environment.base_Url + "cuisine/softdeletecuisone/" + ID, status)
  }
  softDeleteAddon(ID, status){
    return this.http.put(environment.base_Url + "adon/softdeleteadon/" + ID, status)
  }
  softDeleteCategory(ID, status){
    return this.http.put(environment.base_Url + "category/softdeletecategory/" + ID, status)
  }
  softDeleteDish(ID, status){
    return this.http.put(environment.base_Url + "dish/softdeletedish/" + ID, status)
  }
  softDeleteVarient(ID, status){
    return this.http.put(environment.base_Url + "varient/softdeletevarient/" + ID, status)
  }
  ///softdeleteinventoryitem/:inventoryitemid
//softdeleteequipment/:equipmentid
softDeleteItem(ID, status){
  return this.http.put(environment.base_Url + "iteminventory/softdeleteinventoryitem/" + ID, status)
} softDeleteEquipment(ID, status){
  return this.http.put(environment.base_Url + "equipment/softdeleteequipment/" + ID, status)
}


   //********************************************************************************************* */
  // **********************************************************************************************
  // ********************************** Already FOOD exists or not       *********************************************
  // *********************************************************************************************
  // *********************************************************************************************

  checkAddonExist(addon_name){
    return this.http.get(environment.base_Url + "adon/checkadonalreadyexists/" + addon_name)
  }
  checkCategoryExist(categoryName){
    return this.http.get(environment.base_Url + "category/checkcategorynamealreadyexists/" + categoryName)
  }
  checkCuisineExist(cuisineName){
    return this.http.get(environment.base_Url + "cuisine/checkcuisinealreadyexists/" + cuisineName)
  }
  checkDishExist(DishName){
    return this.http.get(environment.base_Url + "dish/checkdishalreadyexists/" + DishName)
  }
  checkVarientExist(varientName){
    return this.http.get(environment.base_Url + "varient/checkvarientalreadyexists/" + varientName)
  }

  checkTagExist(tagName){
    return this.http.get(environment.base_Url + "tag/checktagalreadyexists/" + tagName)
  }
  checkVarientContentExist(VarientContent){
    return this.http.get(environment.base_Url + "varientcontent/checkvarientcontentalreadyexits/" + VarientContent)
  }


   //********************************************************************************************* */
  // **********************************************************************************************
  // ********************************** Already Liquor exists or not       *********************************************
  // *********************************************************************************************
  // *********************************************************************************************

  checkAttributeExist(attributeName,attributetype){
    console.log(attributeName,attributetype);
    
    return this.http.get(environment.base_Url + "attribute/checkattributealreadyexists/" + attributeName +"/" + attributetype )
  }
  checkAttributesetExists(attributeSetName, attributetype){
    return this.http.get(environment.base_Url + "attributeset/checkattributesetalreadyexists/" + attributeSetName +"/" + attributetype )
  }
  checkAttributeGroupExists(groupName, attributeSetName){
    return this.http.get(environment.base_Url + "attributegroup/checkattributegroupalreadyexists/" + groupName +"/" + attributeSetName )
  }  
  checkLiquorBrandExist(name){
    return this.http.get(environment.base_Url + "liquorbrand/checkbrandalreadyexists/" + name)
  }
  checkLiquorProductExist(name){
    return this.http.get(environment.base_Url + "product/checkproductalreadyexists/" + name)
  }
  checkLiquorEquipmentExist(name){
    return this.http.get(environment.base_Url + "equipment/checkequipmentalreadyexists/" + name)
  }
  checkBeverageBrandExist(name){
    return this.http.get(environment.base_Url + "beveragebrand/checkbeveragebrandalreadyexists/" + name)
  }

  checkBeverageProductExist(name){
    return this.http.get(environment.base_Url + "beverageproduct/checkbeverageproductalreadyexists/" + name)
  }

  checkBeverageExist(name){
    return this.http.get(environment.base_Url + "beverage/checkbeveragealreadyexists/" + name)
  }
  checkInventoryBrandExist(name){
    return this.http.get(environment.base_Url + "brandinventory/checkinventroybrandalreadyexists/" + name)
  }
  checkInventoryIngredientExist(name){
    return this.http.get(environment.base_Url + "ingredientinventory/checkinventroyingredientalreadyexists/" + name)
  }

  checkInventoryItemExist(name){
    return this.http.get(environment.base_Url + "iteminventory/checkinventroyitemalreadyexists/" + name)
  }
checkRetailBeverageBrandExist(name){
  return this.http.get(environment.base_Url + "retailbeveragebrand/checkretailbeveragebrandalreadyexists/" + name)
}
checkRetailBeverageProductExist(name){
  return this.http.get(environment.base_Url + "retailbeverageproduct/checkretailbeverageproductalreadyexists/" + name)
}
checkRetailFoodBrandExist(name){
  return this.http.get(environment.base_Url + "retailfoodbrand/checkretailfoodbrandalreadyexists/" + name)
}
// ('/checkretailfoodbproductalreadyexists/:productName', 
checkRetailFoodProductExist(name){
  return this.http.get(environment.base_Url + "retailfoodproduct/checkretailfoodbproductalreadyexists/" + name)
}

  //////////////////////////////update group
  updateGroup(id, data) {
    return this.http.put(environment.base_Url + "attributegroup/updateattributegruop/" + id, data)
  } 


  getattributeDatafromCategories(obj){
    return this.http.post(environment.base_Url + "attributeset/getattributeSetfromliquorcategories", obj)
  }
  getattributeDatafromotherCategories(obj){
    return this.http.post(environment.base_Url + "attributeset/getattributeSetfromothercategories", obj)
  }
  

  
  // retail beverage brand 

   addRetailBeverageBrand(brandObj) {
    return this.http.post(environment.base_Url + "retailbeveragebrand/addretailbeveragebrand", brandObj)
  }

  // GET ALL RETAIL FOOD BRANDS
  getAllRetailBeveargesBrands() {
    return this.http.get(environment.base_Url + "retailbeveragebrand/getallretailbeveragebrand")
  }

  //UPDATE RETAIL FOOD BRAND
  updateRetailBevergesBrand(brandID, brandObj) {
    return this.http.put(environment.base_Url + "retailbeveragebrand/updateretailbeveragebrand/" + brandID, brandObj)
  }

  softDeleteRetailBeveragesBrand(brandID, status){
    return this.http.put(environment.base_Url + "retailbeveragebrand/deleteretailbeveragebrand/" + brandID, status)
  }


//**************************************************Liquor tree update********************************************************* */
UpdateLiquorTreeRoot(rootID, obj){
  return this.http.put(environment.base_Url + "drinktype/updatedrinktypename/" + rootID, obj)
}
UpdateLiquorTreeVarient(rootID,vid, obj){
  return this.http.put(environment.base_Url + "drinktype/updateliquorvarientname/" + rootID+"/"+vid, obj)
}

UpdateLiquorTreeVarientsub(rootID, vid, vsid, obj){
  return this.http.put(environment.base_Url + "drinktype/updateliquorsubvarientsname/" + rootID+"/"+vid+"/"+vsid, obj)
}
UpdateLiquorTreeVarientsubsubtype(rootID, vid, vsid, tid, obj){
  return this.http.put(environment.base_Url + "drinktype/updateliquorsubsubvarienttypename/" + rootID+"/"+vid+"/"+vsid+"/"+tid, obj)
}


//**************************************************Asset inventory tree update********************************************************* */
UpdateAssetinventoryTreeRoot(rootID, obj){
  return this.http.put(environment.base_Url + "inventoryassets/updateRootInventoryAssetsCategoryName/" + rootID, obj)
}
upateAssetinventoryTreesub(rootID, vid, obj){
  return this.http.put(environment.base_Url + "inventoryassets/updatesubcategoryname/" + rootID+"/"+vid, obj)
}
updateAssetinventoryTreesubsub(rootID, vid, vsid, obj){
  return this.http.put(environment.base_Url + "inventoryassets/updatesubsubcategoriesname/" + rootID+"/"+vid+"/"+vsid, obj)
}
UpdateAssetinventoryTreetype(rootID, vid, vsid, tid, obj){
  return this.http.put(environment.base_Url + "inventoryassets/updatesubsubcategorytypename/" + rootID+"/"+vid+"/"+vsid+"/"+tid, obj)
}
/*/updaterootinventoryfoodcategoryname/:rootcategoryid'
'/updatesubcategoryname/:rootcategoryid/:subcategoryid'
'/updatesubsubcategoriesname/:rootcategoryid/:subsubcategoryid'
'/updatesubsubcategorytypename/:rootcategoryid/:subsubcategoryid/:subsubcategorytypeid'



*/
//**************************************************Food tree update********************************************************* */
UpdateFoodinventoryTreeRoot(rootID, obj){
  return this.http.put(environment.base_Url + "fooodinventory/updaterootinventoryfoodcategoryname/" + rootID, obj)
}
updateFoodinventoryTreesub(rootID, vid, obj){
  return this.http.put(environment.base_Url + "fooodinventory/updatesubcategoryname/" + rootID+"/"+vid, obj)
}
updateFoodinventoryTreesubsub(rootID,vsid, obj){
  return this.http.put(environment.base_Url + "fooodinventory/updatesubsubcategoriesname/" + rootID+"/"+vsid, obj)
}
UpdateFoodinventoryTreetype(rootID, vsid, tid ,obj){
  return this.http.put(environment.base_Url + "fooodinventory/updatesubsubcategorytypename/" + rootID+"/"+vsid+"/"+tid, obj)
}
/*retail food :===

'/updateRootRetilFoodCategoryName/:rootcategoryid'
'/updatesubcategoryname/:rootcategoryid/:subcategoryid'
'/updateSubSubCategoriesName/:rootcategoryid/:subcategoryid/:subsubcategoryid', 
'/updateSubSubCategoryTypeName/:rootcategoryid/:subcategoryid/:subsubcategoryid/:subsubcategorytypeid'*/

//**************************************************Retail tree update********************************************************* */
UpdateRetailFoodTreeRoot(rootID, obj){
  return this.http.put(environment.base_Url + "retailfood/updateRootRetilFoodCategoryName/" + rootID, obj)
}
UpdateRetailFoodTreeSub(rootID,sid, obj){
  return this.http.put(environment.base_Url + "retailfood/updatesubcategoryname/" + rootID+"/"+sid, obj)
}

UpdateRetailFoodTreeSubSub(rootID, sid, ssid, obj){
  return this.http.put(environment.base_Url + "retailfood/updateSubSubCategoriesName/" + rootID+"/"+sid+"/"+ssid, obj)
}
UpdateRetailFoodTreeType(rootID, sid, ssid, tid, obj){
  return this.http.put(environment.base_Url + "retailfood/updateSubSubCategoryTypeName/" + rootID+"/"+sid+"/"+ssid+"/"+tid, obj)
}
/*retail food :===

'/updateRootRetilFoodCategoryName/:rootcategoryid'
'/updatesubcategoryname/:rootcategoryid/:subcategoryid'
'/updateSubSubCategoriesName/:rootcategoryid/:subcategoryid/:subsubcategoryid', 
'/updateSubSubCategoryTypeName/:rootcategoryid/:subcategoryid/:subsubcategoryid/:subsubcategorytypeid'*/

//**************************************************Retail tree update********************************************************* */
UpdateBeveragesRoot(rootID, obj){
  return this.http.put(environment.base_Url + "beveragescategory/updaterootbeveragescategoryname/" + rootID, obj)
}
UpdateBeveragesTreeSub(rootID,sid, obj){
  return this.http.put(environment.base_Url + "beveragescategory/updatesubcategoryname/" + rootID+"/"+sid, obj)
}

UpdateBeveragesTreeSubSub(rootID, sid, ssid, obj){
  return this.http.put(environment.base_Url + "beveragescategory/updatesubsubcategoriesname/" + rootID+"/"+sid+"/"+ssid, obj)
}
UpdateBeveragesTreeType(rootID, sid, ssid, tid, obj){
  return this.http.put(environment.base_Url + "beveragescategory/updatesubsubcategorytypename/" + rootID+"/"+sid+"/"+ssid+"/"+tid, obj)
}
}