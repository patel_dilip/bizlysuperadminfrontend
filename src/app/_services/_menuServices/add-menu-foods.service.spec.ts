import { TestBed } from '@angular/core/testing';

import { AddMenuFoodsService } from './add-menu-foods.service';

describe('AddMenuFoodsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddMenuFoodsService = TestBed.get(AddMenuFoodsService);
    expect(service).toBeTruthy();
  });
});
