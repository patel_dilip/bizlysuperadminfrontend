import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EstablishmentTypeService {

  constructor(private http: HttpClient) { }

  getAllEstablishmentTypes(){
    return this.http.get('/assets/bizly-data/Establishment-type/establishment.json')
  }
}
