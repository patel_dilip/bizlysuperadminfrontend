import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }


  adminLogin(adminLogData){
    return this.http.post(environment.base_Url + 'admin/signin', adminLogData)
  }
}
