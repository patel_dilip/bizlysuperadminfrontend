import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  respn;
  feedbackQuestions: any;
  queId;
  screenFlag: any;
  editObject: any;
  editQueSetObj: any;
  editTemplateSetObj : any;
  setValueObj : any;
  queSetScreenFlag: any;
  quesettype;
  queSetId: any;
  temId: any;
  templateQueSetScreenFlag: any;
  selectopts: any;
  constructor(private http: HttpClient) { }


//***************bizly to cust or pos *************//

setSelectedValue(selectopts) {
  this.selectopts = selectopts;

}

getSelectedValue() {
  return this.selectopts ;
}


getAllFeedbackApiData(selectedOption) : Observable<any> {
  debugger;
  const url = environment.base_Url + `feedback/getbizlytocustomerandbizlytoposdata/${selectedOption}`;
  return this.http.get(url)

}



  //*******post call for  add feedback qoestion*******//
  createQuestion(queryparams): Observable<any> {
    debugger;
    const url = environment.base_Url + "feedback/addfeedback";
    return this.http.post(url, queryparams)
  }


  //*******get call for add question list in table*********//  
  getAllFeedbackQuestionList() {
    return this.http.get(environment.base_Url + "feedback/getallfeedbacks")
  }
  //********get call for question type set********//
  getAllQuestionTypeSet() {
    // return this.http.get(environment.base_Url + "feedback/getallfeedbacks")
    return this.http.get('/assets/bizly-data/feedback/createQuestionType.json')
  }
  //********get call for question type set estserviceliquorcategories******//
  getAllQuestionTypeSetFromApi() {
    return this.http.get(environment.base_Url + "feedback/getestserviceliquorcategories")
  
  }//********get call for question type set allfeedbackCategory******//
  getAllFeedbackQueTypeSetFromApi(){ 
    
    return this.http.get(environment.base_Url + "feedbackcategory/getallfeedbackCategory")
  }

  // **********API FOR ADD question category type *******************

  //*******post call for  add rootquestiontype*******//
  postRootQuestionType(rootObj) {
    return this.http.post(environment.base_Url + "feedbackcategory/addrootquestiontype", rootObj)
  }

  //  ************API question category type**********************
  putCategoryType(id, categoryName) {
    const Url = environment.base_Url + `feedbackcategory/addquestiontype/${id}`;
    return this.http.put(Url, categoryName)
  }


  //  ************API question sub category type**********************

  putsubCategoryType(root_id, cat_id, childcategorytypename) {
    // return this.http.put(environment.base_Url + "feedbackcategory/addquestionsubtype/" + root_id + "/" + cat_id, childcategorytypename)
    return this.http.put(environment.base_Url + "feedbackcategory/addcategory/" + root_id + "/" + cat_id, childcategorytypename)
  }

 //  ************API question sub sub category type**********************


  putSubSubCategorytype(root_id, catsub_id, childchildcategorytype) {
    // return this.http.put(environment.base_Url + "feedbackcategory/addquestionsubchildtype/" + root_id + "/" + catsub_id, childchildcategorytype)
    return this.http.put(environment.base_Url + "feedbackcategory/addchildcategory/" + root_id + "/" + catsub_id, childchildcategorytype)
  }

  putSubSubChildCategorytype(root_id, catsub_id,catsubchild_id, childchildcategorytype) {
    // return this.http.put(environment.base_Url + "feedbackcategory/addquestionsubchildtype/" + root_id + "/" + catsub_id, childchildcategorytype)
    return this.http.put(environment.base_Url + "feedbackcategory/addchildchildcategory/" + root_id + "/" + catsub_id+ "/" + catsubchild_id, childchildcategorytype)
  }
 
  // feedbackcategory/addcategory/:rootquestiontypeid/:questiontypeid   add category   method:put
  // feedbackcategory/addchildcategory/:rootquestiontypeid/:categoryid  add child category   method:put
  // feedbackcategory/addchildchildcategory/:rootquestiontypeid/:categoryid/:childcategoryid   add child child category  method:put

  //************Edit Feedbackquestion *****//
  setFeedbackQuetsionId(queId, screenFlag) {
    this.queId = queId;
    this.screenFlag = screenFlag
  }

  getFeedbackQuestionId() {
    return { 'queId': this.queId, 'screenFlag': this.screenFlag }
  }

  getFeedbackQuestion(queId) {
    const Url = environment.base_Url + `feedback/getfeedback/${queId}`;
    console.log("getFeedbackquestion url", Url);
    return this.http.get(Url)
  }

  updateFeedbackQuestion(queId, updatedObj) {
    const Url = environment.base_Url + `feedback/updatefeedback/${queId}`;
    return this.http.put(Url, updatedObj)
  }




  //********post call for  add question set*******//

  createQuestionSet(questionset): Observable<any> {
    debugger;
    const url = environment.base_Url + "questionset/addquestionset";
    return this.http.post(url, questionset)
  }


  //*******get call for add question set list in table*********//
  getAllQuestionSetList() {
    return this.http.get(environment.base_Url + "questionset/getallquestionset")
  }


  //************Edit Question Set *****//
  setQuetsionSetId(queSetId, screenFlag) {
    this.queSetId = queSetId;
    this.queSetScreenFlag = screenFlag


  }

  getQuestionSetId() {
    return { 'queSetId': this.queSetId, 'screenFlag': this.queSetScreenFlag }
  }

  getQuestionset(queSetId) {
    const Url = environment.base_Url + `questionset/getquestionsetbyid/${queSetId}`;
    console.log("getquestionseturl", Url);
    return this.http.get(Url)
  }

  updateQuestionSet(queSetId, updatedObj) {
    const Url = environment.base_Url + `questionset/updatequestionset/${queSetId}`;
    return this.http.put(Url, updatedObj)
  }

  //***********status change for question set **************//

  queSetStatusActiveDeactive(quesetStatusid, status) {
    // const Url = environment.base_Url + `questionset/updatequestionset/${quesetStatusid}`;

    console.log("id", quesetStatusid);
    console.log("status", status)
    return this.http.put(environment.base_Url +"questionset/updatequestionset/"+quesetStatusid, status)

  }

  //*******************Filter question set tree*************//

  questionsetFilter(seletedType) {
    debugger;
    let queryparamsroot;
    queryparamsroot = {
      string: seletedType,
    }
    console.log('change filter seletedType in service', seletedType)
    console.log('change filter queryparamsroot', queryparamsroot)

    const Url = environment.base_Url + "feedback/getfeedbackquestionfromcategory";
    return this.http.post(Url, queryparamsroot)

  }





  //********post call for  add question Template*******//

  createTemplate(questionTemplate): Observable<any> {
    debugger;
    const url = environment.base_Url + "template/addtemplate";
    return this.http.post(url, questionTemplate)
  }

  //*******get call for add tempaleset list in table*********//

  getAllTempaletSetList() {
    return this.http.get(environment.base_Url + "template/getAlltemplate")
  }


  //************Edit Template *****//
  setTemplateSetId(temId, screenFlag) {
    this.temId = temId;
    this.templateQueSetScreenFlag = screenFlag

  }

  getTemplateSetId() {
    return { 'temId': this.temId, 'screenFlag': this.templateQueSetScreenFlag }
  }

  getTemplateSet(temId) {
    const Url = environment.base_Url + `template/gettemplate/${temId}`;
    console.log("getTemplateSetUrl", Url);
    return this.http.get(Url)
  }


  updateTemplateSet(temId, updatedObj) {
    const Url = environment.base_Url + `template/updatetemplate/${temId}`;
    return this.http.put(Url, updatedObj)
  }


  //***********status change for template set **************//
  templateSetStatusActiveDeactive(id, status) {
    const Url = environment.base_Url + `template/changetempaltestatus/${id}`;

    return this.http.put(Url, status)

  }

  //********post call for  add question Template*******//

  processAssign(assignId, assignTemplate): Observable<any> {

    debugger;
    console.log("assignid", assignId);
    console.log("assignid", assignTemplate);

    const url = environment.base_Url + `template/assignedtempaltetoposorcustomer/${assignId}`;
    return this.http.put(url, assignTemplate)
  }

}
