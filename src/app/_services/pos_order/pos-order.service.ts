import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class PosOrderService {

  constructor(private http: HttpClient) { }
  editObject:any
  viewObject:any

  BuyProduct(obj) {
    return this.http.post(environment.base_Url+'posorders/addposproductorder', obj)
  }

  getProductOrder() {
    return this.http.get(environment.base_Url+'posorders/getallposproductorders')
  }
  getIntegrationOrder() {
    return this.http.get(environment.base_Url+'posorders/getallposintegrationorders')
  }
  getAddonOrder() {
    return this.http.get(environment.base_Url+'posorders/getallposaddonorders')
  }
  getallcount(){
    return this.http.get(environment.base_Url+'posorders/getposorderstats')
  }
  addPaymentLink(obj){
    return this.http.post(environment.base_Url+'posorders/addpospaymentlink', obj)
  }
  getallPaymentLinkHistory(){
    return this.http.get(environment.base_Url+'posorders/getallpospaymentlinks')
  }
  getPaymentHistoryById(id){
    return this.http.get(environment.base_Url+'posorders/getPOSPaymentLink/'+id)
  }
  addIntegrationOrders(id, obj){
    return this.http.put(environment.base_Url+'posorders/addposintegrationorders/'+id, obj)
  }
  addAddonOrders(id, obj){
    return this.http.put(environment.base_Url+'posorders/addposaddonorders/'+id, obj)
  }
  getProductById(id){
    return this.http.get(environment.base_Url+'posorders/getposproductorderbyid/'+id)
  }
  getAddonById(id){
    return this.http.get(environment.base_Url+'posorders/getposaddonorderbyid/'+id)
  }

  getIntegrationById(id){
    return this.http.get(environment.base_Url+'posorders/getposintegrationorderbyid/'+id)
  }
  cancelIntegrationOrders(id,ordeid, obj){
    return this.http.put(environment.base_Url+'posorders/addposintegrationorders/'+id+'/'+ordeid, obj)
  }
  cancelAddonOrders(id,ordeid, obj){
    return this.http.put(environment.base_Url+'posorders/addposaddonorders/'+id+'/'+ordeid, obj)
  }
  cancelProductOrders(id,ordeid, obj){
    return this.http.put(environment.base_Url+'posorders/updateposproductorder/'+id+'/'+ordeid, obj)
  }
}