import { TestBed } from '@angular/core/testing';

import { PosOrderService } from './pos-order.service';

describe('PosOrderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PosOrderService = TestBed.get(PosOrderService);
    expect(service).toBeTruthy();
  });
});
