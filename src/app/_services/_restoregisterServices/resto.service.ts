import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { idText } from 'typescript';
import { ChildActivationEnd } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RestoService {

  singleViewRestaurant: any

  RestoId: any
  onlyRestoInfo: any  
  onlyEstablish: any
  onlyLegalInfo: any
  onlyMedia: any
  onlyMenu: any
  onlyLayout: any




  // Ogranizations variables
    OrganizationId: any
    OrganizationObj: any

    // Restaurant variables
    view_restaurant: any
    current_restoid: any
    resto_update: any

    // Cinema variables DISPLAY COLUMNS
    cinemaCol: any = []
    view_cinema: any
    update_cinema: any

    // CHAIN VARIABLES
    view_update: any
    chainEdit: any



  constructor(private http: HttpClient) {

  }


  // ADD LAYOUT INFO

  postLayout(restoid, layObj){
    return this.http.put(environment.base_Url+"restaurant/addrestaurantlayout/"+restoid, layObj)
  }

  // __________________-CHANI UPDATE MEDIA_________________

  updateMediaChain(chainid,mediaObj){
    return this.http.put(environment.base_Url+"chain/updatemediainfo/"+chainid, mediaObj)
  }

  // _________________________Chaininfo Update Functions____________________
  updateChaininfo(chainid,chainobj){
    return this.http.put(environment.base_Url+"chain/updatechaininfo/"+chainid, chainobj)
  }

  // _________________________Chain moreinfo Update Functions____________________
  updateChainMoreinfo(chainid,moreinfo){
    return this.http.put(environment.base_Url+"chain/updatemoreinfo/"+chainid,moreinfo)
  }

  // _________________________Chain servies Update Functions____________________
  updateChainServices(chainid,services){
    return this.http.put(environment.base_Url+"chain/updateServices/"+chainid,services)
  }

  // _________________________Chain Rules Update Functions____________________
  updateChainRules(chainid,rules){
    return this.http.put(environment.base_Url+"chain/updaterules/"+chainid,rules)
  }

  // _________________________Chain Ambiences Update Functions____________________
  updateChainAmbiences(chainid,ambiences){
    return this.http.put(environment.base_Url+"chain/updateambience/"+chainid,ambiences)
  }

  // _________________________Chain moreinfo Update Functions____________________
  updateChainCuisines(chainid,cuisines){
    return this.http.put(environment.base_Url+"chain/updatecuisine/"+chainid,cuisines)
  }

  // ****************************UPDATE MEDIA DATA***********************
  updateMedia(restoid, mediaDataObj){
    return this.http.put(environment.base_Url + "restaurant/addmedia/"+restoid, mediaDataObj)
  }


  // ***************************UPDATE LEGAL DATA***************
 

 updateLegal(restoid, legalobj){
   return this.http.put(environment.base_Url+"restaurant/addrestaurantlegaldata/"+restoid, legalobj)
 }

// ***************************************UPDATE CUISINES********************
updateCuisines(restoid,cuisineObj){
  return this.http.put(environment.base_Url+"restaurant/updatecuisines/"+restoid,cuisineObj)
  
}

// ****************************UPDATE RESTO INFO***************************
updateRestoInfo(restoid, restObj){
  return this.http.put(environment.base_Url+"restaurant/addrestaurantinfo/"+restoid,restObj)
}

// ********************************UPDATE EST CATEGORIES******************
  updateEStCategory(restoid, estObj){
    return this.http.put(environment.base_Url+"restaurant/updateEstcategory/"+restoid,estObj)
  }



  // *********************update more info********
  // updatemoreinfo

  updateMoreInfo(restoid, moreinfo){
    
    return this.http.put(environment.base_Url+"restaurant/updatemoreinfo/"+restoid, moreinfo)
  }

  // ********************update Services*************
  updateServices(restoid, services){
    console.log("id", restoid)
    console.log("services", services)
    return this.http.put(environment.base_Url+"restaurant/updateservices/"+restoid,services)
  }

  // **************************UPDATE RULES AND REGULAIONS**********************
  updateRules(restoid, rules){
    console.log("id", restoid)
    console.log("rules",rules)
    return this.http.put(environment.base_Url+"restaurant/updaterulesandregulation/"+restoid,rules)
  }

   // **************************UPDATE Ambiences**********************
  updateAmbiences(restoid,ambiences){
    console.log("id",restoid)
    console.log("ambience",ambiences)
    return this.http.put(environment.base_Url+"restaurant/updateambiences/"+restoid,ambiences)
  }

  // ***********PUT OULET INFO****************
  putouletInfo(restoid, ouletObj){
    return this.http.put(environment.base_Url+"restaurant/updateOutlet/"+restoid,ouletObj )
  }

  // ********post website name*********

  postwebsiteName(websitename){
    return this.http.post(environment.base_Url+"website/addwebsite",websitename)
  }

  // *****Change website name*********

  putChangeWebName(websiteid, webObj){
    return this.http.put(environment.base_Url+"website/updatewebsite/"+websiteid, webObj)
  }
  
  //  ***ADD RESTO INFO****

  postAddRestoInfo(restoInfoObj) {
    return this.http.post(environment.base_Url + "restaurant/addrestaurantinfo", restoInfoObj)
  }

  // ***PUT RESTO INFO METHOD***
putRestoInfo(restoid,restoInfoObj){

  console.log("outlet id", restoid);
  console.log('RestObj', restoInfoObj)
  
  return this.http.put(environment.base_Url+"restaurant/addrestaurantinfo/"+restoid, restoInfoObj)
}

  //  get ALL SERVICES
  getAllServices() {
    return this.http.get(environment.base_Url + 'services/getallservies')
  }

  // get ALL Rules
  getAllRules() {
    return this.http.get(environment.base_Url + "ruleandregulation/getallruleandregulations")
  }

  // get all Ambience
  getAllAmbience() {
    return this.http.get(environment.base_Url + "ambience/getallambiences")
  }


  // GET ALL MORE INFO
  getAllMoreInfo() {
    return this.http.get(environment.base_Url + "moreinfo/getallmoreinfos")
  }

  // Get all cuisines info
  getAllCuisines() {
    return this.http.get(environment.base_Url + "cuisine/getallcuisines")
  }


  // ******************REVENUE SERVIECES***********************

  // deleteall revenue
  deleteAllRev(){
    return this.http.delete(environment.base_Url+"restaurantrevenue/deleteallrestaurantrevenue")
  }

  // post avenue
  postAddRevenue(revenueObj) {
    return this.http.post(environment.base_Url + "restaurantrevenue/addrestaurantrevenue", revenueObj)
  }

  getAllRevenue() {
    return this.http.get(environment.base_Url + "restaurantrevenue/getallrestaurantrevenues")
  }

  // ******************Resto Type SERVIECES***********************

  // post resto type
  postAddRestoType(addRestoTypeObj) {
    return this.http.post(environment.base_Url + "restaurantType/addRestaurantType", addRestoTypeObj)
  }

  // Get resto type
  getAllRestoType() {
    return this.http.get(environment.base_Url + "restaurantType/getAllrestaurantType")
  }


  // get all restaurant API
  getAllRestaurant() {
    return this.http.get(environment.base_Url + "restaurant/getallrestaurants")
  }

  // get single restauarant
  getSelectedResto(id) {
    return this.http.get(environment.base_Url + "restaurant/getrestaurant/"+id)
  }

  // **************ESTABLISHMENT APIS*******************

  // post establishment root category

  postRootCategory(addRootObj) {
    return this.http.post(environment.base_Url + "establishment/addestablishmentrootcategory", addRootObj)
  }


  // get establishment root category

  getRootCategory() {
    return this.http.get(environment.base_Url + "establishment/getallestablishments");
  }

  // POST CATEGORY
  postCategory(id, addCategoryObj) {
    console.log('id:', id);
    console.log('addCategoryObj', addCategoryObj);


    return this.http.put(environment.base_Url + "establishment/addestablishmentcategories/" + id, addCategoryObj)
  }

  // Post sub Category
  postSubCategory(rid, cid, addsubObj) {
    return this.http.put(environment.base_Url + "establishment/addestablishmentchildcategories/" + rid + "/" + cid, addsubObj)
  }

  // Post sub sub category
  postSubSubCategory(rid, childid, addSubSubObj) {
    return this.http.put(environment.base_Url + "establishment/addestablishmentchildchildcategories/" + rid + "/" + childid, addSubSubObj)
  }

  // put more info
  putMoreInfoToResto(restoid, moreInfoObj) {
    return this.http.put(environment.base_Url + "restaurant/addrestaurantmoreinfo/" + restoid, moreInfoObj)
  }

  //  put services info to
  putServicesToResto(restoid, servicesObj) {
    return this.http.put(environment.base_Url + "restaurant/addrestaurantservices/" + restoid, servicesObj)
  }

  // put Rules info to resto
  putRulesToResto(restoid, rulesObj) {
    return this.http.put(environment.base_Url + "restaurant/addrestaurantrulesandregulations/" + restoid, rulesObj)
  }

  // put Ambience info to resto
  putAmbiencesToResto(restoid, ambienceObj) {
    return this.http.put(environment.base_Url + "restaurant/addrestaurantambiences/" + restoid, ambienceObj)
  }


  // Put Legal RestoInfo
  PutLegalDataResto(id, legalDataObj) {
    return this.http.put(environment.base_Url + "restaurant/addrestaurantlegaldata/" + id, legalDataObj)
  }


  // PUT Media resto

  PutMediaDataResto(id, mediaDataObj) {
    return this.http.put(environment.base_Url + "restaurant/addmedia/" + id, mediaDataObj)
  }

  // put LAYOUT MEDIA

  PutLayoutFinalSubmit(id, layoutSubmit) {
    console.log(layoutSubmit);

    return this.http.put(environment.base_Url + "restaurant/addrestaurantlayout/" + id, layoutSubmit)
  }

  // PUT ADD ESTABLSIMENT 

  putEstablishType(restoid, estCategories) {
    return this.http.put(environment.base_Url + "restaurant/addrestaurantestablishments/" + restoid, estCategories)
  }

  // POST ADD ROOT MENU
  postRootMenu(addRootMenu) {
    return this.http.post(environment.base_Url + "menu/addmenurootcategory", addRootMenu)
  }


  // Get ALL ROOT MENU
  getAllRootMenu() {
    return this.http.get(environment.base_Url + "menu/getallmenus")
  }

  // put ADD CATEGORY MENU
  putCategoryMenu(rootCategoryId, addMenuCatObj) {
    return this.http.put(environment.base_Url + "menu/addmenucategories/" + rootCategoryId, addMenuCatObj)
  }

  // Put Add Sub Category Menu
  putSubCategoryMenu(rootCategoryId, categoresId, addSubCatMenuObj) {
    return this.http.put(environment.base_Url + "menu/addmenuchildcategories/" + rootCategoryId + "/" + categoresId, addSubCatMenuObj)
  }

  // PUT ADD SUPER CHILD 
  putSuperChildMenu(rootCategoryId, childcategoryid, addSuperChildMenuObj) {
    return this.http.put(environment.base_Url + "menu/addmenuchildchildcategories/" + rootCategoryId + "/" + childcategoryid, addSuperChildMenuObj)
  }


  // ******PUT CHIPS INTO ESTABLISHMENT

  putchipstoMore(moreinfoid, moreinfo){
    return this.http.put(environment.base_Url+"moreinfo/updatemoreinfo/"+moreinfoid, moreinfo)
  }

  putchipstoService(serviceId, servInfo){
    return this.http.put(environment.base_Url+"services/updateservice/"+serviceId, servInfo)
  }

  putchipstoRules(ruleandregulationid, rulesInfo){
    return this.http.put(environment.base_Url+"ruleandregulation/updateruleandregulation/"+ruleandregulationid, rulesInfo)
  }

  putchipstoAmbience(ambienceid, ambienceInfo){
    return this.http.put(environment.base_Url+"ambience/updateambience/"+ambienceid, ambienceInfo)
  }

  // **********MEDIA APIS*********

  putMediaObjtoResto(restoid, mediaObj) {
    return this.http.put(environment.base_Url + "restaurant/addmedia/" + restoid, mediaObj)
  }

  // ***************Post Selected Menu*********************88
  putSelectedMenu(restoid, menu) {
    return this.http.put(environment.base_Url + "restaurant/addrestaurantmenumanagementmenu/" + restoid, menu)
  }

  // Post Add Liqour Root drink in Restaurant menu
  postAddRootLiquorResto(addRootDrinkObj) {
    return this.http.post(environment.base_Url + "liquor/addliquortyperootcategory", addRootDrinkObj)
  }

  // Put Add Liquor Varient in Restuarant menu
  putAddVarientResto(rootCategoryId, addVarientObj) {
    return this.http.put(environment.base_Url + "liquor/addliquortypecategories/" + rootCategoryId, addVarientObj)
  }

  // Put ADD Liquor SUB VARIENT IN restaurant menu
  putAddSubVarientResto(rootCategoryId, varientId, addSubVarientObj) {
    return this.http.put(environment.base_Url + "liquor/addliquortypechildcategories/" + rootCategoryId + "/" + varientId, addSubVarientObj)
  }

  // put ADD LIQUOR SUPER CHILD IN RESTO MENU
  putAddSuperChildLiquorResto(rootCategoryId, subvarientid, addSuperChildObj) {
    return this.http.put(environment.base_Url + "liquor/addliquortypechildchildcategories/" + rootCategoryId + "/" + subvarientid, addSuperChildObj)
  }

  //GET ALL LIQUOR IN rESTO MENU
  getALlLiquorTree() {
    return this.http.get(environment.base_Url + "liquor/getallliquortypes")
  }

  // put Selected Cuisnes 
  putSelectedLiquor(restoid, liquorObj) {
    return this.http.put(environment.base_Url + "restaurant/addrestaurantmenumanagementliquor/" + restoid, liquorObj)
  }

  // ***************Add selected Cuisines*************************
  putSelectedCuisines(restoid, selectedCusines) {
    return this.http.put(environment.base_Url + "restaurant/addrestaurantmenumanagementcuisines/" + restoid, selectedCusines)
  }


  // **************Establishement moreInfo, Services, ambience, rules***********

  // ADD MORE INFO CHIPS //*** */
  postMoreInfo(moreInfoObj) {
    return this.http.post(environment.base_Url + "moreinfo/addmoreinfo", moreInfoObj)
  }

  // Get all MORE INFOS
  getAllMoreInfos() {
    return this.http.get(environment.base_Url + "moreinfo/getallmoreinfos")
  }

  // add Services info chips
  postServicesInfo(serviceObj) {
    return this.http.post(environment.base_Url + "services/addservice", serviceObj)
  }

  // getAllServicesInfo
  getAllServicesInfo() {
    return this.http.get(environment.base_Url + "services/getallservies")
  }

  // add rules info chips
  postRulesInfo(rulesObj) {
    return this.http.post(environment.base_Url + "ruleandregulation/addruleandregulation", rulesObj)
  }

  // getAllRulesInfo
  getAllRulesInfo() {
    return this.http.get(environment.base_Url + "ruleandregulation/getallruleandregulations")
  }

  // add ambience info chips
  postAmbienceInfo(ambienceObj) {
    return this.http.post(environment.base_Url + "ambience/addambience", ambienceObj)
  }

  // get ALL AMBIENCE INFORMATION
  getAllAmbienceInfo() {
    return this.http.get(environment.base_Url + "ambience/getallambiences")
  }


  

  // SET RESTAURANT STATUS ACTIVE / DEACTIVE
  setRestaurantStatus(restoid,statusObj){
    return this.http.put(environment.base_Url+"restaurant/deleterestaurant/"+restoid,statusObj)
  }

  deleteRestobyId(restoid){
    return this.http.delete(environment.base_Url+"restaurant/deleterestaurantbyid/"+restoid)
  }

  deleteAllResto(){
    return this.http.delete(environment.base_Url+"restaurant/deleteallrestaurant")
  }

  // **********************
  // ORGANIZATION API 
  // **********************

  // Add Organization

  addOrganization(body){
    return this.http.post(environment.base_Url+"organization/addorganization",body)
  }

  // Get ALL Organization

  getAllOrg(){
    return this.http.get(environment.base_Url+"organization/getallorganization")
  }

  //Get Organization BY ID organizationid

  getOrgById(organizationid){
    return this.http.get(environment.base_Url+"organization/getorganization/"+organizationid)
  }

  //Update Organization organizationid
  updateOrg(organizationid, objBody){
    return this.http.put(environment.base_Url+"organization/updateorganization/"+organizationid, objBody)
  }

  // Delete All Organization once
  deleteAllOrg(){
    return this.http.delete(environment.base_Url+"organization/deleteallorganization");
  }

  // Delete Organization organizationid
  deleteOrg(organizationid){
    return this.http.delete(environment.base_Url+"organization/deleteorganization/"+organizationid)
  }

  // Put Add Oultet Array in Organizaiton
  addOutletToOrg(organizationid, outletObj){
    console.log('org id', organizationid );
    console.log('ouletid', outletObj);  
    
    return this.http.put(environment.base_Url+"organization/addorganizationoutlets/"+organizationid, outletObj)
  }


  // pOST OUTLET INFO
  addOultetInfo(obj){
    return this.http.post(environment.base_Url+'restaurant/addrestaurantoulets', obj)
  }
  
  // update Outlet info
  // updateOutletInfo(){
  //   return this.http.put(environment+base_Url+"")
  // }


  //******************* */
  
  //******************* */
  // Cinema api
  //******************* */

  addCinema(obj){
    return this.http.post(environment.base_Url+"cinema/addcinemainfo",obj)
  }

  getAllCinema(){
    return this.http.get(environment.base_Url+"cinema/getallcinemas")
  }

  deleteAllCine(){
    return this.http.delete(environment.base_Url+"cinema/deleteallcinema")
  }


  // ################
  // Chian INFO API
  // ################

  addChain(obj){
    return this.http.post(environment.base_Url+"chain/addchaininfo",obj)
  }

  getAllChains(){
    return this.http.get(environment.base_Url+"chain//getallchains")
  }
  deleteAllChains(){
    return this.http.delete(environment.base_Url+"chain/deleteallchain")
  }

 
}
