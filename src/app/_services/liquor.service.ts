import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LiquorService {

  constructor(private http: HttpClient) { }

  getAllRootDrinks(){
    return this.http.get("/assets/bizly-data/Menu-mgmt/liquor.json")
  }
}
