import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChargeService {
  editObject:any
  viewObject:any 
  constructor(private http:HttpClient) { }
  addproduct(obj) {

    return this.http.post(environment.base_Url+'chargesproduct/addchargesproduct', obj)

  }
  addintegration(obj) {

    return this.http.post(environment.base_Url+'chargesintegration/addchargesintegration', obj)

  }
  addaddons(obj) {

    return this.http.post(environment.base_Url+'chargesaddon/addchargesaddon', obj)

  }
  getProduct() {

    return this.http.get(environment.base_Url+'chargesproduct/getallchargesproduct')
  }
  getIntegration() {

    return this.http.get(environment.base_Url+'chargesintegration/getallchargesintegration')
  }
  getAddons() {

    return this.http.get(environment.base_Url+'chargesaddon/getallchargesaddon')
  }
  statusIntegration(id,  country, obj){
    console.log(obj);
    return this.http.put(environment.base_Url+'chargesintegration/updateintegrationstatus/'+id+'/'+country, obj)
    
    
  }

  statusProduct(id,country,obj){
    return this.http.put(environment.base_Url+'chargesproduct/updateproductstatus/'+id+'/'+country,obj)
  }
  statusaddons(id, country, obj){
    return this.http.put(environment.base_Url+'chargesaddon/updatechargesaddonstatus/'+id+'/'+country,obj)
  }
  //get charges addon := /chargesaddon/getchargesaddon/:addonid
  getSingleAddons(id) {

    return this.http.get(environment.base_Url+'chargesaddon/getchargesaddon/'+id)
  }
  getSingleProduct(id) {

    return this.http.get(environment.base_Url+'chargesproduct/getchargesproduct/'+id)
  }
  getSingleIntegration(id) {

    return this.http.get(environment.base_Url+'chargesintegration/getchargesintegration/'+id)
  }
  updateAddons(id,obj) {

    return this.http.put(environment.base_Url+'chargesaddon/updatechargesaddon/'+id,obj)
  }
  updateProduct(id,obj) {

    return this.http.put(environment.base_Url+'chargesproduct/updatechargesproduct/'+id,obj)
  }
  updateIntegration(id,obj) {

    return this.http.put(environment.base_Url+'chargesintegration/updatechargesintegration/'+id,obj)
  }

  getProductCountry(productname) {
console.log(">>>>>",productname);

    return this.http.get(environment.base_Url+'chargesproduct/getcompletedata/'+productname)
  }
  getAddonCountry(addonname) {

    return this.http.get(environment.base_Url+'chargesaddon/getcompletedata/'+addonname)
  }
  getIntegrationCountry(integrationname) {

    return this.http.get(environment.base_Url+'chargesintegration/getcompletedata/'+integrationname)
  }

  getIntegrationByCountry(country) {

    return this.http.get(environment.base_Url+'chargesintegration/getchargesintegrationbycountryname/'+country)
  }
  getAddonByCountry(country) {

    return this.http.get(environment.base_Url+'chargesaddon/getchargesaddonsbycountry/'+country)
  }

}
