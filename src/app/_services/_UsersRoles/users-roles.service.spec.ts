import { TestBed } from '@angular/core/testing';

import { UsersRolesService } from './users-roles.service';

describe('UsersRolesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsersRolesService = TestBed.get(UsersRolesService);
    expect(service).toBeTruthy();
  });
});
