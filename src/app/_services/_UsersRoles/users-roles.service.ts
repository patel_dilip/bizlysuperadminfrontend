import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
//import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UsersRolesService {
rolesEdit:any;
usersEdit:any;
  constructor(private http:HttpClient) { }
  Supplies():Observable<any>{
    return this.http.get<any>('assets/supplies.json');
  }
  Roles():Observable<any>{
    return this.http.get<any>('src/assets/bizly-data/user_roles/roles.json');
  }
  getAllEstablishment(){
    return this.http.get('/assets/bizly-data/Establishment-type/establishment.json')
  }
  //**********post role methode*************/
  postRole(obj){
    return this.http.post(environment.base_Url+'userrolebizly/addbizlyrole',obj)
  }
  //**********get all role methode*************** */
  getAllRole(){
    return this.http.get(environment.base_Url+'userrolebizly/getallbizlyrole')
  }
  //**********get single role methode******* */
  getSingleRole(roleid){
    return this.http.get(environment.base_Url+'userrolebizly/getbizlyrole'+roleid)
  }
  //************post pesonal details of user Methode*************/
  postUserPersonalDetail(obj){
    console.log("service obj",obj);
    
    return this.http.post(environment.base_Url+'userrolebizly/addbizlyuserpersonaldetails',obj)
  }
   //************put work details of user Methode*************/
   putUserWorkDetail(id,obj){
    return this.http.put(environment.base_Url+'userrolebizly/addbizlyuserworkdetails/'+id,obj)
  }
   //************put bank details of user Methode*************/
   putUserBankDetail(id,obj){
     console.log(environment.base_Url+'userrolebizly/addbizlyuserbankdetails/',id,obj);
     
    return this.http.put(environment.base_Url+'userrolebizly/addbizlyuserbankdetails/'+id,obj)
  }
  //****************put Legal documents details of user Methode***********/
  putUserDocumnetDetail(id,obj){
    return this.http.put(environment.base_Url+'userrolebizly/addbizlyuserlegaldocuments/'+id,obj)
  }

  //******************put assets details of user Methode*************/
  putAssetsDetails(id,obj){
    return this.http.put(environment.base_Url+'userrolebizly/addbizlyuserassetsdetails/'+id,obj)
  }
  //*******************put Credentials details of user methode*********/
  putUserCredentialDetails(id,obj){
    console.log("credentials service",obj);
    
    return this.http.put(environment.base_Url+'userrolebizly/addbizlyusercredentialdetails/'+id,obj)
  }
    //**********post pos role methode*************/
    postPosRole(obj){
      return this.http.post(environment.base_Url+'userrolePOS/addposrole',obj)
    }
  //**********get all role methode****************/
  getAllUser(){
    return this.http.get(environment.base_Url+'userrolebizly/getallbizlyusers')
  }
  //**************post POS personal details****************/
  postUserPosPersonalDetails(obj){
    return this.http.post(environment.base_Url+'userrolePOS/addposuserpersonaldetails',obj)
  }
  //*****************put work details*****************/
  putUserPosWorkDetails(id,obj){
    return this.http.put(environment.base_Url+'userrolePOS/addposuserworkdetails/'+id,obj)
  }
  //*****************put bank details****************/
  putUserPosBankdetails(id,obj){
    return this.http.put(environment.base_Url+'userrolePOS/addposuserbankdetails/'+id,obj)
  }
   //*****************put legal details****************/
   putUserPosLegaldetails(id,obj){
    return this.http.put(environment.base_Url+'userrolePOS/addposuserlegaldocuments/'+id,obj)
  }
     //*****************put corporate details****************/
  putUserPosCorporatdetails(id,obj){
      return this.http.put(environment.base_Url+'userrolePOS/addposusercorporatedetails/'+id,obj)
  }
       //*****************put assets details****************/
       putUserPosAssetsdetails(id,obj){
        return this.http.put(environment.base_Url+'userrolePOS/addposuserassetsdetails/'+id,obj)
    }
    //****************put  creadentials details*************/
    putUserPosCredntialsdetails(id,obj){
      return this.http.put(environment.base_Url+'userrolePOS/addposusercredentialdetails/'+id,obj)
  }
    //**********get all role methode****************/
    getAllPosRole(){
      return this.http.get(environment.base_Url+'userrolePOS/getallposrole')
    }
    //***********get all pos user methode************/
    getAllPosUser(){
      return this.http.get(environment.base_Url+'userrolePOS/getallposusers')
    }
 
    //************update role ***************/
    updateRole(id,obj){
      
      return this.http.put(environment.base_Url+'userrolebizly/updatebizlyrole/'+id,obj)
    }
    //***********update user***********/
    updateUser(id,obj){
      return this.http.put(environment.base_Url+'userrolebizly/updatebizlyuser/'+id,obj)
    }
            //***************role soft delete*************/
            deleteRole(id,obj){
              return this.http.put(environment.base_Url+'userrolebizly/deletebizlyrole/'+id,obj)
            }
            //******************user soft delete************* */
            deleteUser(id,obj){
              return this.http.put(environment.base_Url+'userrolebizly/deletebizlyuser/'+id,obj)
            }
            //******************* pos role soft delete************/
            deletePosRole(id,obj){
              return this.http.put(environment.base_Url+'userrolePOS/deleteposrole/'+id,obj)
            }
            //*****************update bizly user************ */
            updateUsers(id,obj){
              console.log("update id",id);
              console.log("update obj",obj);
              return this.http.put(environment.base_Url+'userrolebizly/updatebizlyuser/'+id,obj)
            }
            //*************select country state city********* */
            country_state_city():Observable<any> 
            {
              return this.http.get<any>('/assets/bizly-data/Dial-code/selected-country.json');
              }
              //***********get single user by id******** */
              getSingleUser(id,obj){
                
                return this.http.put(environment.base_Url+'userrolebizly/getbizlyuserbyid/'+id,obj)
              }
              //**********get edited history roles********** */
  getEditedHistory(id){
    return this.http.get(environment.base_Url+'bizlyeditedhistory/getalllastupdatedrole/'+id)
    
  }
  //********get edited history users****** */
  getUserEditedHistory(id,obj){
    return this.http.get(environment.base_Url+'bizlyeditedhistory/getalllastupdatedusers/'+id,obj)
  }
   // get role root category

  getRoleRootCategory(){
    return this.http.get(environment.base_Url + "b2bcategory/getcategories");
  }
  //******post parent root category**** */
  postParentRootCategory(obj){
    
    console.log("role service object",obj);
    
    return this.http.post(environment.base_Url+'b2bcategory/addrootparent',obj)
  }
  //********put 2nd level***** */
  putChildrenCat(rootparentid,obj){
    return this.http.put(environment.base_Url+'b2bcategory/addchildern/'+rootparentid,obj)
  }
  //******put 3rd level***** */
  putSubChildrenCat(id_1,id_2,obj){
    return this.http.put(environment.base_Url+'b2bcategory/addchildern2/'+id_1,id_2,obj)

  }
  //*******put 4th level**** */
  putSubSubChildrenCat(id_2,id_3,obj){
    return this.http.put(environment.base_Url+'b2bcategory/addchildern3/'+id_2,id_3,obj)
  }
  //*******put 5th level*****/
  putSubSubSubChildrenCat(id_3,id_4,obj){
    return this.http.put(environment.base_Url+'b2bcategory/addchildern4/'+id_3,id_4,obj)
  }
  //********put 6th level*****/
  putSubSubSubSubchildrenCat(id_4,id_5,obj){
    return this.http.put(environment.base_Url+'b2bcategory/addchildernlast/'+id_4,id_5,obj)
  }

  // *******Permanent Delete****
  permanentDelete(){
    return this.http.delete(environment.base_Url+"b2bcategory/deleteallcategories")
  }


// ******************CREATE ACESS POINTS API*************************

// 1. Add User
addUser(obj){
  return this.http.post(environment.base_Url+"user/addUser", obj)
}
// 2. GET SINGLE USER
getaSingleUser(id){
  return this.http.get(environment.base_Url+"user/getSingleuser/"+id)
}

// 3. GET ALL USERS
getallUsers(){
  return this.http.get(environment.base_Url+"user/getAlluser")
}

// 4. Delete A User
deleteSingleUser(id){
  return this.http.delete(environment.base_Url+"user/deleteUser/"+id)
}

// 5. Update A Password
updatePassword(id,obj){
  return this.http.put(environment.base_Url+"user/changepassword/"+id, obj)
}

}
