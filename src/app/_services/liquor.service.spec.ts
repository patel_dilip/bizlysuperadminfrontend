import { TestBed } from '@angular/core/testing';

import { LiquorService } from './liquor.service';

describe('LiquorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LiquorService = TestBed.get(LiquorService);
    expect(service).toBeTruthy();
  });
});
