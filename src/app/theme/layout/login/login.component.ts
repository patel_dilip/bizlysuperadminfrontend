import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// import { AuthService } from 'src/app/auth.service';
import Swal from 'sweetalert2'
import { AuthService } from 'src/app/auth/auth.service';
import { LoginService } from 'src/app/_services/adminLogin/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  bizlylogin: FormGroup

  constructor(private fb: FormBuilder, private router: Router, private auth: AuthService,
    private adlog: LoginService) { 

    // localStorage.removeItem('token')
    localStorage.clear()
    this.getForm()
  }

  ngOnInit() {
  }

  

  // declare login form

  getForm(){
    this.bizlylogin = this.fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  onLogin(){
    if(this.bizlylogin.valid ){
      
      this.adlog.adminLogin(this.bizlylogin.value).subscribe(adminResponse=>{
        console.log('adminLogin', adminResponse);
        
        if(adminResponse['success'] == true){
          localStorage.setItem('token',adminResponse['data'].role);
          localStorage.setItem('loginUser',JSON.stringify(adminResponse['data']));
          
          this.auth.isAuthorized(adminResponse['data'].role)
          this.router.navigate(['/restaurants'])
        }

        if(adminResponse['success'] == false && adminResponse['msg'] == 'userName'){
          Swal.fire('Incorrect Username', 'Please Enter Correct One', 'warning')
        }
        else if(adminResponse['success'] == false && adminResponse['msg'] == 'incorrect password'){
          Swal.fire('Incorrect Password', 'Please Enter Correct One', 'warning')
        }

        this.bizlylogin.reset()
      })
      
      
    
     
    
    }
   
  }

}
