import {Injectable} from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}


@Injectable()
export class NavigationItem {

  
  access: any
  role: any
  username: any
  NavigationItems: any = []
  myTitle: any
  Firstname: any
  constructor(){
    let data = JSON.parse(localStorage.getItem('loginUser'))
    this.username = data.username
   
    this.role = data.role

    if(this.role == 'user'){
      this.Firstname = data.firstName
      this.access = 'nav-item hidden'
      this.myTitle =  this.Firstname +' (User) '
    }
    if(this.role == 'admin'){
      this.access = 'nav-item'
      this.myTitle = 'Super Admin (Bizly)'
    }

    
 this.NavigationItems = [
  {
    id: 'other',
    title: this.myTitle,
    type: 'group',
    icon: 'feather icon-align-left',
    children: [
      {
        id: 'dashboard-page',
        title: 'Dashboard',
        type: 'item',
        url: '/sample-page',
        classes: 'nav-item',
        icon: 'feather icon-sidebar'
      },
      
      {
        id: 'restaurant-home',
        title: 'Restaurants',
        type: 'collapse',       
        icon: 'feather icon-sidebar',
        children: [
          {
            id: 'restaurant-home',
            title: 'Add Organization',
            type: 'item',
            url: '/restaurants',
            classes: 'nav-item',
            icon: 'feather icon-sidebar'           
         },
         {
          id: 'restaurant-chain',
          title: 'Add Chain Home',
          type: 'item',
          url: '/restaurants/chain-home',
          classes: 'nav-item',
          icon: 'feather icon-sidebar'         
       },
         {
          id: 'restaurant-multiplex',
          title: 'Add Cinema',
          type: 'item',
          url: '/restaurants/cinema-home',
          classes: 'nav-item',
          icon: 'feather icon-sidebar'         
       },
        

        ]
      },


      //  ORDER NAV BAR
      {
        id: 'customer-orders-page',
        title: 'Orders',
        type: 'item',
        url: '/customer-orders',
        classes: 'nav-item',
        icon: 'feather icon-sidebar'
      },


      {
        id: 'menu-page',
        title: 'Menu Management',
        type: 'item',
        url: '/menu',
        classes: 'nav-item',
        icon: 'feather icon-sidebar'
      },  
      
      {
        id: 'inventory-page',
        title: 'Inventory Management',
        type: 'item',
        url: '/inventory',
        classes: 'nav-item',
        icon: 'feather icon-sidebar'
      },  
      {
        id: 'beverageandretail-page',
        title: 'Beverages and Retail Food',
        type: 'item',
        url: '/beverages_and_retailfood',
        classes: 'nav-item',
        icon: 'feather icon-sidebar'
      },  
     

      //  Integration bar
      {
        id: 'integration-home',
        title: 'Integrations',
        type: 'item',
        url: '/integrations',
        classes: 'nav-item',
        icon: 'feather icon-sidebar',
      },

// Feedback NAV BAR
      {
        id: 'feedback-home',
        title: 'Feedback',
        type: 'item',
        url: '/feedback',
        classes: 'nav-item',
        icon: 'feather icon-sidebar',
      },

// Notice home nav bar
      // {
      //   id: 'notice-home',
      //   title: 'Notice',
      //   type: 'item',
      //   url: '/notice',
      //   classes: 'nav-item',
      //   icon: 'feather icon-sidebar',
      // },
      {
        id: 'notice',
        title: 'Notice',
        type: 'item',
        url: '/notice-page',
        classes: 'nav-item',
        icon: 'feather icon-sidebar',
      },

      // User Home Nav bar
        {
        id: 'employee-home',
        title: 'Employees',
        type: 'collapse',
      
        url: '/employee/bizly',
        icon: 'feather icon-users',
        children: [
          { 
            id: 'bizly',
          title: 'Bizly',
          type: 'item',
          url: '/employee/bizly',
          // breadcrumbs: false
        },
        { 
          id: 'pos-merchant',
        title: 'Pos',
        type: 'item',
        url: '/employee/pos-merchant',
        // breadcrumbs: false
      }
        ]
      }, 
      {
        id: 'users-roles',
        title: 'Users-Roles',
        type: 'item',
        url: '/users-roles',
        classes: this.access,
        icon: 'feather icon-users',
      }, 
      {
        id: 'charges-home',
        title: 'Charges',
        type: 'item',
        url: '/charges',
        classes: 'nav-item',
        icon: 'feather icon-sidebar',
      },
      {
        id: 'buy-product-page',
        title: 'Buy Product',
        type: 'item', 
        url: '/marchant-Buy-product',
        classes: 'nav-item',
        icon: 'feather icon-sidebar'
      },
      {
        id: 'pos-orders-home',
        title: 'POS Orders',
        type: 'item',
        url: '/pos-orders',
        classes: 'nav-item',
        icon: 'feather icon-sidebar',
      }, 
      {
        id: 'log-out',
        title: 'Logout',
        type: 'item',
        url: '/logout',
        classes: 'nav-item',
        icon: 'feather icon-log-out',
      },   
     
     
    ]
  }
];
  }
  public get() {
    return this.NavigationItems;
  }
}
