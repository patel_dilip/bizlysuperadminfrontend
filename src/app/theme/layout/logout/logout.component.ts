import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router) {
    
    let status = JSON.parse(localStorage.getItem('loginUser'))
    
    if(status){
      localStorage.clear()
      location.reload()
    }else{
      this.router.navigate(['/login'])
    }
   
      
   }

  ngOnInit() {
  }

}
