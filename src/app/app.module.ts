import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './theme/shared/shared.module';

// My code
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { FormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect'; 


// import { NgxGaugeModule } from 'ngx-gauge';
import { AppComponent } from './app.component';
import { AdminComponent } from './theme/layout/admin/admin.component';
import { AuthComponent } from './theme/layout/auth/auth.component';
import { NavigationComponent } from './theme/layout/admin/navigation/navigation.component';
import { NavContentComponent } from './theme/layout/admin/navigation/nav-content/nav-content.component';
import { NavGroupComponent } from './theme/layout/admin/navigation/nav-content/nav-group/nav-group.component';
import { NavCollapseComponent } from './theme/layout/admin/navigation/nav-content/nav-collapse/nav-collapse.component';
import { NavItemComponent } from './theme/layout/admin/navigation/nav-content/nav-item/nav-item.component';
import { NavBarComponent } from './theme/layout/admin/nav-bar/nav-bar.component';
import { NavLeftComponent } from './theme/layout/admin/nav-bar/nav-left/nav-left.component';
import { NavSearchComponent } from './theme/layout/admin/nav-bar/nav-left/nav-search/nav-search.component';
import { NavRightComponent } from './theme/layout/admin/nav-bar/nav-right/nav-right.component';
import { ConfigurationComponent } from './theme/layout/admin/configuration/configuration.component';

import { ToggleFullScreenDirective } from './theme/shared/full-screen/toggle-full-screen';


/* Menu Items */
import { NavigationItem } from './theme/layout/admin/navigation/navigation';
import { NgbButtonsModule, NgbDropdownModule, NgbTabsetModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { MaterialModule } from './material/material.module';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { EstAddCatComponent } from './PopoversList/est-add-cat/est-add-cat.component';
import { SubCatComponent } from './PopoversList/sub-cat/sub-cat.component';
import { AttrCatComponent } from './PopoversList/attr-cat/attr-cat.component';
import { CreateAlbumComponent } from './PopoversList/create-album/create-album.component';
import { AddLayoutComponent } from './PopoversList/add-layout/add-layout.component';
import { OpenTableComponent } from './PopoversList/open-table/open-table.component';
import { AddIngredientsComponent } from './demo/pages/menu-page/add-ingredients/add-ingredients.component';
import { AddItemsComponent } from './demo/pages/menu-page/add-items/add-items.component';
import { AddEquipmentComponent } from './demo/pages/menu-page/add-equipment/add-equipment.component';
import { SendPaymentLinkComponent } from './demo/pages/pos-orders/send-payment-link/send-payment-link.component';
import { DeleteProductAndAddOnsComponent } from './demo/pages/pos-orders/delete-product-and-add-ons/delete-product-and-add-ons.component';
import { IntegrationsPosOrdersComponent } from './demo/pages/pos-orders/integrations-pos-orders/integrations-pos-orders.component';
import { ViewProductAndAddOnsComponent } from './demo/pages/pos-orders/view-product-and-add-ons/view-product-and-add-ons.component';
import { ViewIntegrationsPosOrdersComponent } from './demo/pages/pos-orders/view-integrations-pos-orders/view-integrations-pos-orders.component';
import { AddAttributeRadioButtonComponent } from './demo/pages/menu-page/add-attribute-radio-button/add-attribute-radio-button.component';

import { RolesViewComponent } from './demo/pages/users-page/roles-view/roles-view.component';
import { LoginComponent } from './theme/layout/login/login.component';
import { AccessDeniedComponent } from './demo/pages/access-denied/access-denied.component';
import { AddCuisinesComponent } from './PopoversList/add-cuisines/add-cuisines.component';
import { NgxLoadingModule } from 'ngx-loading';
import { FileUploadModule } from "ng2-file-upload";
import { AddTagComponent } from './PopoversList/add-tag/add-tag.component';
import { AddVarientContentComponent } from './PopoversList/add-varient-content/add-varient-content.component';
import { AddVarientPopoverComponent } from './PopoversList/add-varient-popover/add-varient-popover.component';
import { AddDishPopoverComponent } from './PopoversList/add-dish-popover/add-dish-popover.component';
import { AddRootDrinkComponent } from './PopoversList/add-root-drink/add-root-drink.component';
import { AddDrinkVarientComponent } from './PopoversList/add-drink-varient/add-drink-varient.component';
import { AddDrinkSubvarientComponent } from './PopoversList/add-drink-subvarient/add-drink-subvarient.component';
import { AddSubSubVarientComponent } from './PopoversList/add-sub-sub-varient/add-sub-sub-varient.component';
import { ViewCuisineComponent } from './demo/pages/menu-page/view-cuisine/view-cuisine.component';
import { ViewdishComponent } from './demo/pages/menu-page/viewdish/viewdish.component';
import { ViewAddOnComponent } from './demo/pages/menu-page/view-add-on/view-add-on.component';
import { ViewCategoryComponent } from './demo/pages/menu-page/view-category/view-category.component';
import { ViewVarientComponent } from './demo/pages/menu-page/view-varient/view-varient.component';
import { AddRevenueComponent } from './PopoversList/add-revenue/add-revenue.component';
import { AddRestoTypeComponent } from './PopoversList/add-resto-type/add-resto-type.component';
import { AddEstCatNameComponent } from './PopoversList/add-est-cat-name/add-est-cat-name.component';
import { AddCatoNameComponent } from './PopoversList/add-cato-name/add-cato-name.component';
import { AddSubCatComponent } from './PopoversList/add-sub-cat/add-sub-cat.component';
import { AddSubSubCatComponent } from './PopoversList/add-sub-sub-cat/add-sub-sub-cat.component';
import { AddLiquorAttributeGroupComponent } from './demo/pages/menu-page/add-liquor-attribute-group/add-liquor-attribute-group.component';
import { AddLiquorAttributeSetComponent } from './demo/pages/menu-page/add-liquor-attribute-set/add-liquor-attribute-set.component';
import { AddAttributeCheckboxComponent } from './demo/pages/menu-page/add-attribute-checkbox/add-attribute-checkbox.component';
import { ViewLiquorAttributeSetComponent } from './demo/pages/menu-page/view-liquor-attribute-set/view-liquor-attribute-set.component';
import { RootCategoryAssetInventoryComponent } from './demo/pages/menu-page/asset-Inventory-category-dailogs/root-category-asset-inventory/root-category-asset-inventory.component';
import { SubCategoryAssetInventoryComponent } from './demo/pages/menu-page/asset-Inventory-category-dailogs/sub-category-asset-inventory/sub-category-asset-inventory.component';
import { SubSubCategoryAssetInventoryComponent } from './demo/pages/menu-page/asset-Inventory-category-dailogs/sub-sub-category-asset-inventory/sub-sub-category-asset-inventory.component';
import { SubSubCategoryTypeAssetInventoryComponent } from './demo/pages/menu-page/asset-Inventory-category-dailogs/sub-sub-category-type-asset-inventory/sub-sub-category-type-asset-inventory.component';
import { RootCategoryFoodInventoryComponent } from './demo/pages/menu-page/food-inventory-category-dailogs/root-category-food-inventory/root-category-food-inventory.component';
import { SubCategoryFoodInventoryComponent } from './demo/pages/menu-page/food-inventory-category-dailogs/sub-category-food-inventory/sub-category-food-inventory.component';
import { SubSubCategoryFoodInventoryComponent } from './demo/pages/menu-page/food-inventory-category-dailogs/sub-sub-category-food-inventory/sub-sub-category-food-inventory.component';
import { SubSubCategoryTypeFoodInventoryComponent } from './demo/pages/menu-page/food-inventory-category-dailogs/sub-sub-category-type-food-inventory/sub-sub-category-type-food-inventory.component';
import { AddBrandFoodInvnetoryComponent } from './demo/pages/menu-page/add-brand-food-invnetory/add-brand-food-invnetory.component';
import { ViewIngredientFoodInventoryComponent } from './demo/pages/menu-page/view-ingredient-food-inventory/view-ingredient-food-inventory.component';
import { ViewItemsFoodInventoryComponent } from './demo/pages/menu-page/view-items-food-inventory/view-items-food-inventory.component';
import { ViewEquipmentAssetInventoryComponent } from './demo/pages/menu-page/view-equipment-asset-inventory/view-equipment-asset-inventory.component';
import { AddRootMenuComponent } from './PopoversList/add-root-menu/add-root-menu.component';
import { AddCateMenuComponent } from './PopoversList/add-cate-menu/add-cate-menu.component';
import { AddSubcatMenuComponent } from './PopoversList/add-subcat-menu/add-subcat-menu.component';
import { AddChildChildComponent } from './PopoversList/add-child-child/add-child-child.component';
import { AddCcMenuComponent } from './PopoversList/add-cc-menu/add-cc-menu.component';
import { UpdateAttributeGroupComponent } from './demo/pages/menu-page/update-attribute-group/update-attribute-group.component';
// import { UpdateAttributeGroupSetComponent } from './demo/pages/menu-page/update-attribute-group-set/update-attribute-group-set.component';
import { AddRootLiquorPopComponent } from './PopoversList/Liquor-pop-up/add-root-liquor-pop/add-root-liquor-pop.component';
import { AddVarientPopComponent } from './PopoversList/Liquor-pop-up/add-varient-pop/add-varient-pop.component';
import { AddSubVarientPopComponent } from './PopoversList/Liquor-pop-up/add-sub-varient-pop/add-sub-varient-pop.component';
import { AddSuperChildPopComponent } from './PopoversList/Liquor-pop-up/add-super-child-pop/add-super-child-pop.component';
import { RootCategoryBeverageComponent } from './demo/pages/menu-page/beverages-category-dailogs/root-category-beverage/root-category-beverage.component';
import { SubCategoryBeverageComponent } from './demo/pages/menu-page/beverages-category-dailogs/sub-category-beverage/sub-category-beverage.component';
import { SubSubCategoryBeverageComponent } from './demo/pages/menu-page/beverages-category-dailogs/sub-sub-category-beverage/sub-sub-category-beverage.component';
import { SubSubCategoryTypeBeverageComponent } from './demo/pages/menu-page/beverages-category-dailogs/sub-sub-category-type-beverage/sub-sub-category-type-beverage.component';
import { RootCategoryRetailFoodComponent } from './demo/pages/menu-page/retail-food-category-dailogs/root-category-retail-food/root-category-retail-food.component';
import { SubCategoryRetailFoodComponent } from './demo/pages/menu-page/retail-food-category-dailogs/sub-category-retail-food/sub-category-retail-food.component';
import { SubSubCategoryRetailFoodComponent } from './demo/pages/menu-page/retail-food-category-dailogs/sub-sub-category-retail-food/sub-sub-category-retail-food.component';
import { SubSubCategoryTypeRetailFoodComponent } from './demo/pages/menu-page/retail-food-category-dailogs/sub-sub-category-type-retail-food/sub-sub-category-type-retail-food.component';
import { ViewBeverageBrandComponent } from './demo/pages/menu-page/view-beverage-brand/view-beverage-brand.component';
import { UpdateBeverageBrandComponent } from './demo/pages/menu-page/update-beverage-brand/update-beverage-brand.component';
import { ViewBeverageProductComponent } from './demo/pages/menu-page/view-beverage-product/view-beverage-product.component';
import { UpdateRetailFoodBrandComponent } from './demo/pages/menu-page/update-retail-food-brand/update-retail-food-brand.component';
import { ViewBeverageComponent } from './demo/pages/menu-page/view-beverage/view-beverage.component';
import { AddMoreInfoComponent } from './PopoversList/Establish-related-popups/add-more-info/add-more-info.component';
import { AddServicesChipsComponent } from './PopoversList/Establish-related-popups/add-services-chips/add-services-chips.component';
import { AddRulesChipsComponent } from './PopoversList/Establish-related-popups/add-rules-chips/add-rules-chips.component';
import { AddAmbienceChipsComponent } from './PopoversList/Establish-related-popups/add-ambience-chips/add-ambience-chips.component';
import { ViewTemplateCardComponent } from './demo/pages/notice-page/view-template-card/view-template-card.component';
import { ViewCircularNoticeComponent } from './demo/pages/notice-page/view-circular-notice/view-circular-notice.component';
import { ViewCircularTemplateComponent } from './demo/pages/notice-page/view-circular-template/view-circular-template.component';
import { NoticePopupComponent } from './demo/pages/notice-page/notice-popup/notice-popup.component';
import { CircularPopUpComponent } from './demo/pages/notice-page/circular-pop-up/circular-pop-up.component';
import { NewDescussionComponent } from './demo/pages/notice-page/new-descussion/new-descussion.component';
import { AddBillcounterPopupComponent } from './PopoversList/Layout-related-popups/add-billcounter-popup/add-billcounter-popup.component';
import { AddChairPopupComponent } from './PopoversList/Layout-related-popups/add-chair-popup/add-chair-popup.component';
import { AddKitchenPopupComponent } from './PopoversList/Layout-related-popups/add-kitchen-popup/add-kitchen-popup.component';
import { CreateVdoAlbumComponent } from './PopoversList/Media-related-Popups/create-vdo-album/create-vdo-album.component';
import { StatusPopComponent } from './PopoversList/Restaurants-related-popups/status-pop/status-pop.component';
import { GenerateWebsiteComponent } from './PopoversList/Restaurants-related-popups/generate-website/generate-website.component';
import { AddChipMoreinfoComponent } from './PopoversList/Establish-related-popups/add-chip-moreinfo/add-chip-moreinfo.component';
import { AddChipServicesComponent } from './PopoversList/Establish-related-popups/add-chip-services/add-chip-services.component';
import { AddChipRulesComponent } from './PopoversList/Establish-related-popups/add-chip-rules/add-chip-rules.component';
import { AddChipAmbiencesComponent } from './PopoversList/Establish-related-popups/add-chip-ambiences/add-chip-ambiences.component';
import { QrcodenoComponent } from './PopoversList/Restaurants-related-popups/qrcodeno/qrcodeno.component';
import { ViewRolesComponent } from './demo/pages/users-roles/view-roles/view-roles.component';
import { AddAssetsComponent } from './demo/pages/users-roles/add-assets/add-assets.component';
import { ViewUsersComponent } from './demo/pages/users-roles/view-users/view-users.component';
import { ViewPosRoleComponent } from './demo/pages/users-roles/view-pos-role/view-pos-role.component';
import { SelectRoleComponent } from './demo/pages/users-roles/select-role/select-role.component';
import { PosSelectRoleComponent } from './demo/pages/users-roles/pos-select-role/pos-select-role.component';
import { SelectRoleOfRoleComponent } from './demo/pages/users-roles/select-role-of-role/select-role-of-role.component';
// import { PayAllSalaryComponent } from './demo/pages/salary/pay-all-salary/pay-all-salary.component';
// import { ViewAllHistoryComponent } from './demo/pages/salary/view-all-history/view-all-history.component';
// import { ViewSalaryComponent } from './demo/pages/salary/view-salary/view-salary.component';
// import { ChargesAddAddOnsComponent } from './demo/pages/charges-home/charges-add-add-ons/charges-add-add-ons/charges-add-add-ons.component';
import { AddProductPopupComponent} from './demo/pages/charges-home/add-product-popup/add-product-popup.component';
import { AddIntegrationPopupComponent } from './demo/pages/charges-home/add-integration-popup/add-integration-popup.component';
import { AddAddonPopupComponent } from './demo/pages/charges-home/add-addon-popup/add-addon-popup.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { RoleAddCatComponent } from './demo/pages/users-roles/role-add-cat/role-add-cat.component';
import { AddSubCatRoleComponent } from './demo/pages/users-roles/add-sub-cat-role/add-sub-cat-role.component';
import { AddCatRoleNameComponent } from './demo/pages/users-roles/add-cat-role-name/add-cat-role-name.component';
import { AddSubSubCatRoleComponent } from './demo/pages/users-roles/add-sub-sub-cat-role/add-sub-sub-cat-role.component';
import { ColSetComponent } from './demo/pages/Restaurtant/restaurant-home/CinemaPopUps/col-set/col-set.component';
import{ ViewLiquorAttributeComponent}from './demo/pages/menu-page/view-liquor-attribute/view-liquor-attribute.component'
import { AddBeverageBrandComponent } from './demo/pages/menu-page/add-beverage-brand/add-beverage-brand.component';
import { AddRetailfoodBrandComponent } from './demo/pages/menu-page/add-retailfood-brand/add-retailfood-brand.component';

import{ViewRetailbeveragesBrandComponent} from './demo/pages/beverage-retailfood/view-retailbeverages-brand/view-retailbeverages-brand.component'
import{ViewRetailbeveragesProductComponent} from './demo/pages/beverage-retailfood/view-retailbeverages-product/view-retailbeverages-product.component'
import { RetailBeveragesBrandComponent } from './demo/pages/beverage-retailfood/retail-beverages-brand/retail-beverages-brand.component';
import{UpdateRetailbeveragesBrandComponent}from'./demo/pages/beverage-retailfood/update-retailbeverages-brand/update-retailbeverages-brand.component';
import { LogoutComponent } from './theme/layout/logout/logout.component';
import { AddRootQuestionTypeComponent } from './PopoversList/add-root-question-type/add-root-question-type.component';
import { AddCategoryQueTypeComponent } from './PopoversList/add-category-que-type/add-category-que-type.component';
import { AddChildCategoryQueTypeComponent } from './PopoversList/add-child-category-que-type/add-child-category-que-type.component';
import { AddChildchildCategoryQueTypeComponent } from './PopoversList/add-childchild-category-que-type/add-childchild-category-que-type.component';

// Tree Module
import {TreeModule} from 'primeng/tree';






  


@NgModule({
  declarations: [
    NewDescussionComponent,
    AddKitchenPopupComponent,
    AppComponent,
    NoticePopupComponent,
    AdminComponent,
    AuthComponent,
    NavigationComponent,
    NavContentComponent,
    NavGroupComponent,
    NavCollapseComponent,
    NavItemComponent,
    NavBarComponent,
    NavLeftComponent,
    NavSearchComponent,
    NavRightComponent,
    ConfigurationComponent,
    ToggleFullScreenDirective,
    EstAddCatComponent,
    SubCatComponent,
    AttrCatComponent,
    CreateAlbumComponent,
    AddLayoutComponent,
    OpenTableComponent,
    AddEquipmentComponent,
    SendPaymentLinkComponent,
    DeleteProductAndAddOnsComponent,
    IntegrationsPosOrdersComponent,
    ViewProductAndAddOnsComponent,
    ViewIntegrationsPosOrdersComponent,
    AddAttributeRadioButtonComponent,
    CircularPopUpComponent ,
    RolesViewComponent,
    LoginComponent,
    AccessDeniedComponent,
    AddCuisinesComponent,
    AddTagComponent,
    AddVarientContentComponent,
    AddVarientPopoverComponent,
    AddChairPopupComponent,
    AddDishPopoverComponent,

    AddRootDrinkComponent,

    AddDrinkVarientComponent,
    AddDrinkSubvarientComponent,
    AddSubSubVarientComponent,
    ViewCuisineComponent,
    ViewCategoryComponent,
    ViewdishComponent,
    ViewVarientComponent,
    ViewAddOnComponent,
    AddRevenueComponent,
    AddRestoTypeComponent,
    AddEstCatNameComponent,
    AddCatoNameComponent,
    AddSubCatComponent,
    AddSubSubCatComponent,
    AddLiquorAttributeGroupComponent,
    AddLiquorAttributeSetComponent,
    AddAttributeCheckboxComponent,
    ViewLiquorAttributeSetComponent,
    RootCategoryAssetInventoryComponent,
    SubCategoryAssetInventoryComponent,
    SubSubCategoryAssetInventoryComponent,
    SubSubCategoryTypeAssetInventoryComponent,
    RootCategoryFoodInventoryComponent,
    SubCategoryFoodInventoryComponent,
    SubSubCategoryFoodInventoryComponent,
    SubSubCategoryTypeFoodInventoryComponent,
    AddBrandFoodInvnetoryComponent,
    ViewIngredientFoodInventoryComponent,
    ViewItemsFoodInventoryComponent,
    ViewEquipmentAssetInventoryComponent,
    AddRootMenuComponent,
    AddCateMenuComponent,
    AddSubcatMenuComponent,
    AddChildChildComponent,
    AddCcMenuComponent,
    UpdateAttributeGroupComponent,
    // UpdateAttributeGroupSetComponent,
    AddRootLiquorPopComponent,
    AddVarientPopComponent,
    AddSubVarientPopComponent,
    AddSuperChildPopComponent,

    RootCategoryBeverageComponent,
    SubCategoryBeverageComponent,
    SubSubCategoryBeverageComponent,
    SubSubCategoryTypeBeverageComponent,
    RootCategoryRetailFoodComponent,
    SubCategoryRetailFoodComponent,
    SubSubCategoryRetailFoodComponent,
    SubSubCategoryTypeRetailFoodComponent,
    ViewBeverageBrandComponent,
    UpdateBeverageBrandComponent,
    ViewBeverageProductComponent,
    UpdateRetailFoodBrandComponent,
    ViewBeverageComponent,
    AddMoreInfoComponent,
    AddServicesChipsComponent,
    AddRulesChipsComponent,
    AddAmbienceChipsComponent,
    ViewTemplateCardComponent,
    ViewCircularNoticeComponent,
    ViewCircularTemplateComponent,
    AddBillcounterPopupComponent,
    CreateVdoAlbumComponent,
    StatusPopComponent,
    GenerateWebsiteComponent,
    AddChipMoreinfoComponent,
    AddChipServicesComponent,
    AddChipRulesComponent,
    AddChipAmbiencesComponent,
    // ChargesAddAddOnsComponent
    QrcodenoComponent,
    ViewRolesComponent,
    AddAssetsComponent,
    ViewUsersComponent,
  
    ViewPosRoleComponent,
    SelectRoleComponent,
    SelectRoleOfRoleComponent,
    PosSelectRoleComponent,
   AddProductPopupComponent,
   AddIntegrationPopupComponent ,
   AddAddonPopupComponent,
   RoleAddCatComponent,
   AddSubCatRoleComponent ,
   AddCatRoleNameComponent,
   AddSubSubCatRoleComponent,
   ColSetComponent,
ViewLiquorAttributeComponent,
AddIngredientsComponent,
AddItemsComponent,
AddBeverageBrandComponent,
AddRetailfoodBrandComponent,
ViewRetailbeveragesBrandComponent,
ViewRetailbeveragesProductComponent,
RetailBeveragesBrandComponent,
UpdateRetailbeveragesBrandComponent,
LogoutComponent,
AddRootQuestionTypeComponent,
    AddCategoryQueTypeComponent,
    AddChildCategoryQueTypeComponent,
    AddChildchildCategoryQueTypeComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbButtonsModule,
    NgbTabsetModule,
    MaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    DialogModule,
    FormsModule,
    MultiSelectModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgxLoadingModule.forRoot({}),

    FileUploadModule,
    TreeModule
   
    

  ],
  providers: [NavigationItem],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [  
    ViewPosRoleComponent,ViewUsersComponent,AddAssetsComponent,ViewRolesComponent,ViewTemplateCardComponent,ViewCircularNoticeComponent,ViewCircularTemplateComponent,EstAddCatComponent, SubCatComponent, AttrCatComponent, CreateAlbumComponent,
    AddLayoutComponent, OpenTableComponent, AddEquipmentComponent, NewDescussionComponent, SendPaymentLinkComponent,
    DeleteProductAndAddOnsComponent, IntegrationsPosOrdersComponent, ViewProductAndAddOnsComponent,
    ViewIntegrationsPosOrdersComponent, AddAttributeRadioButtonComponent,
    RolesViewComponent, AddCuisinesComponent, AddVarientContentComponent, AddVarientPopoverComponent,
    AddTagComponent, AddDishPopoverComponent, AddRootDrinkComponent, AddDrinkVarientComponent,
    AddDrinkSubvarientComponent, AddSubSubVarientComponent, ViewCuisineComponent,NoticePopupComponent,
    ViewCategoryComponent, ViewdishComponent, ViewVarientComponent, ViewAddOnComponent,
    AddRevenueComponent, AddRestoTypeComponent, AddCatoNameComponent, AddSubCatComponent, AddSubSubCatComponent,
    AddLiquorAttributeGroupComponent, AddLiquorAttributeSetComponent, AddAttributeCheckboxComponent,
    ViewLiquorAttributeSetComponent, RootCategoryAssetInventoryComponent, SubCategoryAssetInventoryComponent,
    SubSubCategoryAssetInventoryComponent, SubSubCategoryTypeAssetInventoryComponent,
    RootCategoryFoodInventoryComponent, SubCategoryFoodInventoryComponent,
    SubSubCategoryFoodInventoryComponent, SubSubCategoryTypeFoodInventoryComponent, AddBrandFoodInvnetoryComponent,
    ViewIngredientFoodInventoryComponent, ViewItemsFoodInventoryComponent, ViewEquipmentAssetInventoryComponent,
    AddCateMenuComponent, AddRootMenuComponent, AddSubcatMenuComponent, AddCcMenuComponent, UpdateAttributeGroupComponent,
    // UpdateAttributeGroupSetComponent,
     AddRootLiquorPopComponent, AddVarientPopComponent, AddSubVarientPopComponent, AddSuperChildPopComponent,
    RootCategoryBeverageComponent,
    CircularPopUpComponent ,
    NoticePopupComponent ,
    SubCategoryBeverageComponent,
    SubSubCategoryBeverageComponent,
    SubSubCategoryTypeBeverageComponent,
    RootCategoryRetailFoodComponent,
    SubCategoryRetailFoodComponent,
    SubSubCategoryRetailFoodComponent,
    SubSubCategoryTypeRetailFoodComponent,
    ViewBeverageBrandComponent,
    UpdateBeverageBrandComponent,
    ViewBeverageProductComponent,
    UpdateRetailFoodBrandComponent,
    ViewBeverageComponent,
    AddMoreInfoComponent,
    AddRulesChipsComponent,
    AddServicesChipsComponent,
    AddAmbienceChipsComponent,
    NoticePopupComponent,
    CircularPopUpComponent,
    AddBillcounterPopupComponent,
    AddChairPopupComponent,
    AddKitchenPopupComponent ,
    CreateVdoAlbumComponent,
    GenerateWebsiteComponent,
    AddChipAmbiencesComponent,
    AddChipMoreinfoComponent,
    AddChipRulesComponent,
    AddChipServicesComponent,
    QrcodenoComponent,
    SelectRoleComponent,
    SelectRoleOfRoleComponent,
    PosSelectRoleComponent,
    AddProductPopupComponent,
    AddIntegrationPopupComponent ,
    AddAddonPopupComponent,
    RoleAddCatComponent,
    AddSubCatRoleComponent ,
    AddCatRoleNameComponent,
    AddSubSubCatRoleComponent,
    ColSetComponent,
    ViewLiquorAttributeComponent,
    AddIngredientsComponent,
    AddItemsComponent,
    AddBeverageBrandComponent,
    AddRetailfoodBrandComponent,
    ViewRetailbeveragesBrandComponent,
    ViewRetailbeveragesProductComponent,
    RetailBeveragesBrandComponent,
UpdateRetailbeveragesBrandComponent,AddRootQuestionTypeComponent,
AddCategoryQueTypeComponent,
AddChildCategoryQueTypeComponent,
AddChildchildCategoryQueTypeComponent
    //AddBillcounterPopupComponent
  ]
})
export class AppModule { }

