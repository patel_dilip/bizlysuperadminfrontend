import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
     

       const allowedRoles = next.data.allowedRoles;
       const isAuthorised = this.auth.isAuthorized(allowedRoles)


       if(!isAuthorised){
         this.router.navigate(['access-denied'])

         let token = localStorage.getItem('token')
         if(token == '' || token== null){
           this.router.navigate(['login'])
         }
       }
    return isAuthorised
    
  }
  
}