import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router) { }

  isAuthorized(allowedRoles: string[]): boolean {
    if(allowedRoles == null || allowedRoles.length == 0){
      console.log('here without login');
      
      return true;
    }


    const token = localStorage.getItem('token')

    if(token == '' || token == null){
      this.router.navigate(['/login'])
      return false;
    }

    if(!allowedRoles.includes(token)){
      this.router.navigate(['access-denied'])
    }

    return allowedRoles.includes(token)
  }


 
}
