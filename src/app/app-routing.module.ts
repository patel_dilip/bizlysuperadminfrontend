import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './theme/layout/admin/admin.component';
import { AuthComponent } from './theme/layout/auth/auth.component';
import { MenuPageComponent } from './demo/pages/menu-page/menu-page.component';
import { AddMenuCatComponent } from './demo/pages/menu-page/addFood-cat/add-menu-cat/add-menu-cat.component';
import { LoginComponent } from './theme/layout/login/login.component';
import { LogoutComponent } from './theme/layout/logout/logout.component';
import { AuthGuard } from './auth/auth.guard';
import { AccessDeniedComponent } from './demo/pages/access-denied/access-denied.component';
import { TblDatatableComponent } from './demo/pages/tables/tbl-datatable/tbl-datatable.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'restaurants',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'access-denied',
    component: AccessDeniedComponent
  },
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    data: {
      allowedRoles: ['admin', 'bizly', 'user']
    },
    children: [
      // {
      //   path: '',
      //   redirectTo: 'restaurants',
      //   pathMatch: 'full'
      // },
      {
        path: 'inventory',
        loadChildren: () => import('./demo/pages/inventory/inventory.module').then(module => module.InventoryModule)
      },
      {
        path: 'beverages_and_retailfood',
        loadChildren: () => import('./demo/pages/beverage-retailfood/beverage-retailfood.module').then(module => module.BeverageRetailfoodModule)
      },
      {
        path: 'cards',
        loadChildren: () => import('./demo/pages/card/card.module').then(module => module.CardModule)

      },
      {
        path: 'tables',
        loadChildren: () => import('./demo/pages/tables/tbl-datatable/tbl-datatable.module').then(module => module.TblDatatableModule)
      },
      {
        path: 'menu',
        // component: MenuPageComponent,
        loadChildren: () => import('./demo/pages/menu-page/menu-page.module').then(module => module.MenuPageModule)
        // children: [
        //   {
        //     path: 'add-food',
        //     component: AddMenuCatComponent
        //   }

        // ]
      },
      // {

      // },
      // {
      //   path: 'dashboard',
      //   loadChildren: () => import('./demo/pages/sample-page/sample-page.module').then(module => module.SamplePageModule)
      // },
      {
        path: 'restaurants',
        loadChildren: () => import('./demo/pages/Restaurtant/restaurant-home/restaurant-home.module').then(module => module.RestaurantHomeModule)
      }, {
        path: 'integrations',
        loadChildren: () => import('./demo/pages/integration-page/integration/integration.module').then(module => module.IntegrationModule)
      }, {
        path: 'pos-orders',
        loadChildren: () => import('./demo/pages/pos-orders/pos-orders.module').then(module => module.PosOrdersModule)
      }, {
        path: 'feedback',
        loadChildren: () => import('./demo/pages/feedback/feedback.module').then(module => module.FeedbackModule)
      }, {
        path: 'customer-orders',
        loadChildren: () => import('./demo/pages/customer-orders/customer-orders.module').then(module => module.CustomerOrdersModule)
      },
      // bizly employee 
      {
        path: 'employee',
        loadChildren: () => import('./demo/pages/Employee/employee.module').then(module => module.EmployeeModule)
      },
      // ,{
      //   path:'users-roles',
      //   loadChildren: () => import('./demo/pages/users-roles/users-roles.module').then(module =>module.UsersRolesModule)
      // }, 
      {
        path: 'users-roles',
        loadChildren: () => import('./demo/pages/users-roles/users-roles.module').then(module => module.UsersRolesModule)
      },
      {
        path: 'users',
        loadChildren: () => import('./demo/pages/users-page/users-page.module').then(module => module.UsersPageModule)
      }, {
        path: 'notice-page',
        loadChildren: () => import('./demo/pages/notice-page/notice-page.module').then(module => module.NoticePageModule)
      }, {
        path: 'charges',
        loadChildren: () => import('./demo/pages/charges-home/charges-home.module').then(module => module.ChargesHomeModule)
      },
      {
        path: 'marchant-Buy-product',
        loadChildren: () => import('./demo/pages/marchantbuyproduct/marchantbuyproduct.module').then(module => module.MarchantbuyproductModule)
      }
    ]
  },

  {
    path: '',
    component: AuthComponent,
    children: []
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }
