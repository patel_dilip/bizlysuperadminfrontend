import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubSubVarientComponent } from './add-sub-sub-varient.component';

describe('AddSubSubVarientComponent', () => {
  let component: AddSubSubVarientComponent;
  let fixture: ComponentFixture<AddSubSubVarientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubSubVarientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubSubVarientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
