import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MainService } from 'src/app/_services/main.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-sub-sub-varient',
  templateUrl: './add-sub-sub-varient.component.html',
  styleUrls: ['./add-sub-sub-varient.component.scss']
})
export class AddSubSubVarientComponent implements OnInit {

  addSubSubVarientForm: FormGroup;
  DrinkTypes: any;
  VarientsArray: any
  subVarientArray: any

  selectRootDrinkId: any;
  selectVarientId: any;
  selectSubVarientId: any
  liquorCategoryIDs: any

  constructor(private fb: FormBuilder,
    private addMenuFoodsService: AddMenuFoodsService,
    private mainServ: MainService,
    private matDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data) {
      console.log(data);
      this.liquorCategoryIDs = data
  //  this.getRootTypes();

  }

  ngOnInit() {
    this.addSubSubVarientForm = this.fb.group({
//      rootdrink: ['', Validators.required],
  //    varientName: ['', Validators.required],
    //  subVarientName: ['', Validators.required],
    liquorSubSubVarientTypeName: ['',[Validators.required, Validators.pattern('^[ a-zA-Z]+$')]]
      })
  }

  // // getting all root drink types   
  // getRootTypes() {

  //   this.addMenuFoodsService.getAllRootDrinks().subscribe(data => {
  //     this.DrinkTypes = data['data']

  //     console.log('drinktypes', this.DrinkTypes);

  //   })
  // }

  // // ************FILETER DROPDOWN LIST***********************

  // // ***********ROOT DRINK FILETER*************
  // onRootDrinkSelection(evt) {

  //   // this.getRootTypes();
  //   console.log(evt.target.selectedIndex);
  //   console.log(this.DrinkTypes);
  //   let a = evt.target.selectedIndex

  //   // find drink root object fliter
  //   let singleDrinkobj = this.DrinkTypes[a - 1]
  //   console.log('object', singleDrinkobj
  //   );

  //   // selected drink root id
  //   this.selectRootDrinkId = singleDrinkobj['_id']
  //   console.log('id of selected root drink', this.selectRootDrinkId);


  //   // find liquor varient object flier
  //   this.VarientsArray = singleDrinkobj['liquorVarients']
  //   console.log('varients', this.VarientsArray);

  // }



  // //***********Varient FILETER CODE*************
  // onVarientSelection(evt) {
  //   let varientIdx = evt.target.selectedIndex

  //   let singleVarientObj = this.VarientsArray[varientIdx - 1]

  //   console.log('single varient object', singleVarientObj);

  //   // seleted varient id
  //   this.selectVarientId = singleVarientObj['_id']
  //   console.log('selected varient id', this.selectVarientId);

  //   // subVarient Array

  //   this.subVarientArray = singleVarientObj['liquorSubVarients']


  // }

  // // ****************FIlter SUb varient Type********************
  // onSubVarientSelection(evt){
  //   console.log('eve ',evt.target.selectedIndex);

  //   let sub = evt.target.selectedIndex

  //   let singlesubVarientObj = this.subVarientArray[sub-1]

  //   console.log('single sub varient object', singlesubVarientObj);

  //   // selected sub varient id
  //   this.selectSubVarientId = singlesubVarientObj['_id']
  //   console.log('single sub sub varient id', this.selectSubVarientId);
    
    
    
  // }



  //*********FINAL ADD SUB SUB DRINK NAME ************/
  onAddSubVarient() {

    var liquorSubVarientName = this.addSubSubVarientForm.controls['liquorSubSubVarientTypeName'].value

    var obj = {
      "liquorSubSubVarientTypeName": liquorSubVarientName
    }

    this.addMenuFoodsService.putSubVarientDrink(this.selectRootDrinkId, this.selectVarientId, obj)
      .subscribe(addsubvarientRes => {
        if (addsubvarientRes['sucess']) {
          Swal.fire('sub varient added', '', 'success')
        }
        else {
          Swal.fire('Fail Adding Sub Varient', '', 'warning')
        }
      })

  }

  onAddSubSubDrink(){

    // /let liquorSubSubVarientType = this.addSubSubVarientForm.controls['liquorSubSubVarientTypeName'].value

  //   var addSubSubObj ={
  //     "liquorSubSubVarientType": liquorSubSubVarientType
  // }

    this.addMenuFoodsService.putSubSubDrink(this.liquorCategoryIDs.rootDrinkID, this.liquorCategoryIDs.liquorSubVarient, this.addSubSubVarientForm.value)
    .subscribe(addsubsubRes=>{
      if(addsubsubRes['sucess']){
        Swal.fire('sub sub varient added', '', 'success')
      }
      else {
        Swal.fire('Fail Adding Sub Sub', '', 'warning')
      }
    })
  }
}
