import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCcMenuComponent } from './add-cc-menu.component';

describe('AddCcMenuComponent', () => {
  let component: AddCcMenuComponent;
  let fixture: ComponentFixture<AddCcMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCcMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCcMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
