import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';

@Component({
  selector: 'app-add-cc-menu',
  templateUrl: './add-cc-menu.component.html',
  styleUrls: ['./add-cc-menu.component.scss']
})
export class AddCcMenuComponent implements OnInit {

  MenuChildChildForm: FormGroup
  constructor(private fb: FormBuilder, 
    @Inject(MAT_DIALOG_DATA) public data,
    private restServ: RestoService) { 
    this.getForm();
  }

  ngOnInit() {
  }

  getForm(){
    this.MenuChildChildForm = this.fb.group({
      childChildCategoryName: [''],
      isFilterable: false,
      isSearchable: false,
      priority: []

    })
  }

  onSave(){
    let root_id = this.data['root']
    let child_id = this.data['child']

    console.log('root and child', root_id, child_id)
    console.log('form data', this.MenuChildChildForm.value);
    
    this.restServ.putSuperChildMenu(root_id,child_id,this.MenuChildChildForm.value).subscribe(res=>{
     console.log('resposne', res);
     
      
    })
  }

}