import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCatoNameComponent } from './add-cato-name.component';

describe('AddCatoNameComponent', () => {
  let component: AddCatoNameComponent;
  let fixture: ComponentFixture<AddCatoNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCatoNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCatoNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
