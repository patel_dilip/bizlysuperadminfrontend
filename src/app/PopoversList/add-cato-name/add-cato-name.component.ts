import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { FormGroupName, FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-cato-name',
  templateUrl: './add-cato-name.component.html',
  styleUrls: ['./add-cato-name.component.scss']
})
export class AddCatoNameComponent implements OnInit {
  estRootName: any;

  rootId: any

  categoryAddForm: FormGroup

  addCategoryObj

  PrioArr: string[] = ['1','2','3'];

  constructor(@Inject(MAT_DIALOG_DATA) public root_id, private restServ: RestoService, private fb: FormBuilder) {

      console.log('root id', root_id);

      // get all root category
      this.getRootCat();
      this.getForm();
      
     }

  ngOnInit() {
  }

getRootCat(){
  this.restServ.getRootCategory().subscribe(data=>{
    this.estRootName = data['data']

    console.log('estRoot', this.estRootName);
    
    
  })
  
}

getForm(){
  this.categoryAddForm = this.fb.group({
    rootCat: ['', Validators.required],
    category: ['', Validators.required ],
    isFilterable: [false],
    isSearchable: [false],
    priority: []
  })
}

onRootSelect(root){
  console.log('root', root);
  
}

onSave(){

  
  let _id = this.categoryAddForm.controls['rootCat'].value
  var categoryObj = {
    categoryName: this.categoryAddForm.controls['category'].value,
    isFilterable: this.categoryAddForm.controls['isFilterable'].value,
    isSearchable: this.categoryAddForm.controls['isSearchable'].value,
    priority: this.categoryAddForm.controls['priority'].value

  }
  this.addCategoryObj = categoryObj
  console.log('Form VALUE', this.categoryAddForm.value)

  this.restServ.postCategory(this.root_id, categoryObj).subscribe(res=>{
    if(res['sucess']){
      Swal.fire('Categroy Added Succesfully','','success')
    }
    else{
      Swal.fire('Failed to Add Category','','warning')
    }
  })


}

}
