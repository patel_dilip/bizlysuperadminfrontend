import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-sub-cat',
  templateUrl: './add-sub-cat.component.html',
  styleUrls: ['./add-sub-cat.component.scss']
})
export class AddSubCatComponent implements OnInit {

  subCategoryForm: FormGroup
  constructor(@Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder,
    private restoServ: RestoService) {
    console.log('ids for root and cat', data);
    this.getForm()
  }

  ngOnInit() {
  }

  getForm() {
    this.subCategoryForm = this.fb.group({
      childCategoryName: ["", Validators.required],
      isFilterable: false,
      isSearchable: false,
      priority: ['']
    })
  }

  onSave() {
    console.log(this.subCategoryForm.value)
    console.log('root', this.data['root']);
    console.log('cat', this.data['cat']);
    let rid = this.data['root']
    let cid = this.data['cat']
    let addSubObj = this.subCategoryForm.value

// post sub category

this.restoServ.postSubCategory(rid,cid,addSubObj).subscribe(res=>{
  if(res['sucess']){
    Swal.fire('Sub Catgory Added','','success')
  }
  else{
    Swal.fire('Failed to Add','','warning')
  }
})

  }

}
