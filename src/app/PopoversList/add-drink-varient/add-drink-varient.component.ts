import { Component, OnInit, Inject } from '@angular/core';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-add-drink-varient',
  templateUrl: './add-drink-varient.component.html',
  styleUrls: ['./add-drink-varient.component.scss']
})
export class AddDrinkVarientComponent implements OnInit {
  VarientDrinkForm: FormGroup
  DrinkTypes: any
rootDrinkID: any
  // **********CONSTRUTOR***************************
  constructor( private addMenuFoodsService: AddMenuFoodsService, private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data  ) {
      console.log(data);
      this.rootDrinkID = data
    //this.getRootTypes()
   }

  ngOnInit() {
    
    this.VarientDrinkForm = this.fb.group({
     // rootDrinkName: ['', Validators.required],
      varientName: ['',[Validators.required, Validators.pattern('^[ a-zA-Z]+$')]]
    })
  }

  //  // getting all root drink types   
  //  getRootTypes(){
    
  //   this.addMenuFoodsService.getAllRootDrinks().subscribe(data=>{
  //     this.DrinkTypes = data['data']

  //     console.log('drinktypes',this.DrinkTypes);
      
  //   })
  // }

  // *****************ADD VARIENT NAME HIT API*************************
  onAddVarientDrink(){
    console.log('add varient name', this.VarientDrinkForm.value);

    //let drinkType_id = this.VarientDrinkForm.controls['rootDrinkName'].value
    let liquorVarientName = this.VarientDrinkForm.controls['varientName'].value
    var data = {
      "liquorVarientName": liquorVarientName
  }
    console.log('liqour nam', liquorVarientName);
    

    this.addMenuFoodsService.putVarientDrink(this.rootDrinkID, data).subscribe(addVarientResponse=>{
      if(addVarientResponse['sucess']){
        Swal.fire('varient is added','','success')
      }
    })
    
  }
}
