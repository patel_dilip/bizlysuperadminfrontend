import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDrinkVarientComponent } from './add-drink-varient.component';

describe('AddDrinkVarientComponent', () => {
  let component: AddDrinkVarientComponent;
  let fixture: ComponentFixture<AddDrinkVarientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDrinkVarientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDrinkVarientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
