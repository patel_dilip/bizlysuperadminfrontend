import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FeedbackService } from 'src/app/_services/_feedbackServices/feedback.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-child-category-que-type',
  templateUrl: './add-child-category-que-type.component.html',
  styleUrls: ['./add-child-category-que-type.component.scss']
})
export class AddChildCategoryQueTypeComponent implements OnInit {
  addChildCategoryTypeFrom: FormGroup;
  childCategoryIDs: any

  constructor(private fb: FormBuilder,
    private feedbackServise: FeedbackService,
    private matDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data) {
      console.log(data);
      this.childCategoryIDs= data}

  ngOnInit() {

    this.addChildCategoryTypeFrom = this.fb.group({
      // rootdrink: ['', Validators.required],
      // varientName: ['', Validators.required],
       childCategoryName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]]
     })
  }
//*********FINAL ADD SUB VARIENT NAME ************/
onAddSubCategory() {

  var childCategoryName = this.addChildCategoryTypeFrom.controls['childCategoryName'].value

  var obj = {
    "categoryName": childCategoryName
  }

  this.feedbackServise.putsubCategoryType(this.childCategoryIDs.rootQueID, this.childCategoryIDs.categorytypeID, obj)
    .subscribe(addsubcategoryRes => {
      if (addsubcategoryRes['success']) {
        Swal.fire('sub category added', '', 'success')
      }
      else {
        Swal.fire('Fail Adding Sub category', '', 'warning')
      }
    })

}
}
