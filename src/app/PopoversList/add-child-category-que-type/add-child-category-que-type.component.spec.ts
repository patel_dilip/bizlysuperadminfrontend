import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChildCategoryQueTypeComponent } from './add-child-category-que-type.component';

describe('AddChildCategoryQueTypeComponent', () => {
  let component: AddChildCategoryQueTypeComponent;
  let fixture: ComponentFixture<AddChildCategoryQueTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChildCategoryQueTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChildCategoryQueTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
