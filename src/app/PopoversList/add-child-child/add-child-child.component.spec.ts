import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChildChildComponent } from './add-child-child.component';

describe('AddChildChildComponent', () => {
  let component: AddChildChildComponent;
  let fixture: ComponentFixture<AddChildChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChildChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChildChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
