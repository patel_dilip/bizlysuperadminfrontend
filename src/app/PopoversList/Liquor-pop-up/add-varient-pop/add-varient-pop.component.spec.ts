import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVarientPopComponent } from './add-varient-pop.component';

describe('AddVarientPopComponent', () => {
  let component: AddVarientPopComponent;
  let fixture: ComponentFixture<AddVarientPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVarientPopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVarientPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
