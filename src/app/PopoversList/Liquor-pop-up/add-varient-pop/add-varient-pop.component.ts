import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-varient-pop',
  templateUrl: './add-varient-pop.component.html',
  styleUrls: ['./add-varient-pop.component.scss']
})
export class AddVarientPopComponent implements OnInit {
  addliquorVarient: FormGroup
  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private restServ: RestoService) { 
    this.getForm()
  }

  ngOnInit() {
  }

  getForm(){
    this.addliquorVarient = this.fb.group({
      categoryName: [''],
      isFilterable: false,
      isSearchable: false,
      priority:[]
    })
  }

  onSave(){
    console.log('add liquour varient ', this.addliquorVarient.value);
    
    this.restServ.putAddVarientResto(this.data, this.addliquorVarient.value).subscribe(res=>{
      console.log('response', res);
      if(res['sucess']){
        Swal.fire('Drink Varient Added','','success')
      }
      else{
        Swal.fire('Failed to Add','','warning')
      }
  
    })


  }

}
