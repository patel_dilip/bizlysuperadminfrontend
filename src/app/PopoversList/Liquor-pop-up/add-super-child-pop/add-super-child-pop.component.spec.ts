import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSuperChildPopComponent } from './add-super-child-pop.component';

describe('AddSuperChildPopComponent', () => {
  let component: AddSuperChildPopComponent;
  let fixture: ComponentFixture<AddSuperChildPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSuperChildPopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSuperChildPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
