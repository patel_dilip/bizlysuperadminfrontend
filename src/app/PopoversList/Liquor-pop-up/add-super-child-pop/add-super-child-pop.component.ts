import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-super-child-pop',
  templateUrl: './add-super-child-pop.component.html',
  styleUrls: ['./add-super-child-pop.component.scss']
})
export class AddSuperChildPopComponent implements OnInit {
  SuperChildLiquor: FormGroup
  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private restServ: RestoService) { 
    this.getForm()
  }

  ngOnInit() {
  }

  getForm(){
    this.SuperChildLiquor = this.fb.group({
      childChildCategoryName: [''],
      isFilterable: [false],
      isSearchable: [false],
      priority: []
    })
  }

  onSave(){
    console.log('data super child', this.SuperChildLiquor.value);

    let rid = this.data['root']
    let childid = this.data['child']

    this.restServ.putAddSuperChildLiquorResto(rid,childid,this.SuperChildLiquor.value).subscribe(res=>{
      console.log('response', res);
      if(res['sucess']){
        Swal.fire('Sub Sub Drink Added','','success')
      }
      else{
        Swal.fire('Failed to Add','','warning')
      }
    })
    
  }
}