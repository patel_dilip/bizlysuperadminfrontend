import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRootLiquorPopComponent } from './add-root-liquor-pop.component';

describe('AddRootLiquorPopComponent', () => {
  let component: AddRootLiquorPopComponent;
  let fixture: ComponentFixture<AddRootLiquorPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRootLiquorPopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRootLiquorPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
