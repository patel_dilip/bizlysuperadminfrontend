import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RestaurantHomeModule } from 'src/app/demo/pages/Restaurtant/restaurant-home/restaurant-home.module';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-root-liquor-pop',
  templateUrl: './add-root-liquor-pop.component.html',
  styleUrls: ['./add-root-liquor-pop.component.scss']
})
export class AddRootLiquorPopComponent implements OnInit {
  addliquorRoot: FormGroup
  userid
  constructor(private fb: FormBuilder, private restServ: RestoService) { 

     // ************user id localstorage ******************
     let userdata = JSON.parse(localStorage.getItem('loginUser'))
     this.userid = userdata['_id'];
    this.getForm()
  }

  ngOnInit() {
  }

  getForm(){
    this.addliquorRoot = this.fb.group({
      rootCategoryName: [''],
      isFilterable: [false],
      isSearchable: [false],
      priority:[],
      userid: this.userid
    })
  }

  onSave(){
    console.log('root menu', this.addliquorRoot.value);


    // restoservice

    this.restServ.postAddRootLiquorResto(this.addliquorRoot.value).subscribe(res=>{
      console.log('response', res);
      if(res['sucess']){
        Swal.fire('Root Drink Added','','success')
      }
      else{
        Swal.fire('Failed to Add','','warning')
      }
    })

    
  }
}
