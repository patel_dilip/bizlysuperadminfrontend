import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-sub-varient-pop',
  templateUrl: './add-sub-varient-pop.component.html',
  styleUrls: ['./add-sub-varient-pop.component.scss']
})
export class AddSubVarientPopComponent implements OnInit {
  addLiquorSubVarient: FormGroup
  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private restServ: RestoService) { 
    this.getForm()
  }

  ngOnInit() {
  }

  getForm(){
    this.addLiquorSubVarient = this.fb.group({
      childCategoryName: [''],
      isFilterable: [false],
      isSearchable: [false],
      priority: []
    })
  }

  onSave(){
console.log('drink sub varient value', this.addLiquorSubVarient.value);
    let rid = this.data['root']
    let catid = this.data['cat']

    console.log('rid', rid);
    console.log('catid', catid);
    
    
    this.restServ.putAddSubVarientResto(rid,catid,this.addLiquorSubVarient.value).subscribe(res=>{
      console.log('response add sub cat drink', res);
      if(res['sucess']){
        Swal.fire('Sub Varient Added','','success')
      }
      else{
        Swal.fire('Failed to Add','','warning')
      }
    })
}
}