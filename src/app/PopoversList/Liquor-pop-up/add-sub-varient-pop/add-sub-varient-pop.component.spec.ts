import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubVarientPopComponent } from './add-sub-varient-pop.component';

describe('AddSubVarientPopComponent', () => {
  let component: AddSubVarientPopComponent;
  let fixture: ComponentFixture<AddSubVarientPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubVarientPopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubVarientPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
