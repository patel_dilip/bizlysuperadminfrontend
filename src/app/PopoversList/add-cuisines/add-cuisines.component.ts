import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

export interface country {
  name: string;
}

const URL = environment.base_Url + 'cuisine/uploadphotos'

@Component({
  selector: 'app-add-cuisines',
  templateUrl: './add-cuisines.component.html',
  styleUrls: ['./add-cuisines.component.scss']
})
export class AddCuisinesComponent implements OnInit {
  addCuisinesForm: FormGroup
  cuisineCtrl: FormControl
  selectedAlbums = []
  cuisines = []
  tempCuisines = []
  uploader: FileUploader;
  response: string;
  multiResponse = []
  loading = false;
  selectedCuisinesIds=[];
  userid

  CountryCtrl = new FormControl('',Validators.required);
 
  
  allStatesCity = []
  allCities = []
  country_json=[];
  state=[];
  city=[];
active_cuisines=[]

  state_list: any;

  constructor(private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService
    , private http: HttpClient, private router: Router) {
      this.active_cuisines=[]
    this.addMenuFoodsService.getCuisines().subscribe(data => {
      this.cuisines = data['data']
      this.cuisines.forEach(element => {
        if(element.status==true){
this.active_cuisines.push(element) 
        }
      });
      this.cuisines=this.active_cuisines
      this.tempCuisines = data['data']
      console.log(data['data']);
      console.log(this.cuisines);
    })
    this.country_state_city()

// ************user id localstorage ******************
    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];


  }
country_state_city(){
  console.log("hey");

  this.addMenuFoodsService.country_state_city().subscribe(data => {
// console.log("============", data);

  console.log(data.country_json);
    this.country_json = data.country_json


    for (var i = 0; i < data.country_json.length - 1; i++)
    //console.log("=========================");
    
      console.log(data.country_json[i].name);

  })
}
changeCountry(count) {
  console.log(count);
  this.state = this.country_json.find(state => state.name == count).states
 
  this.state_list=this.state
  this.state=Object.keys(this.state)
 // console.log(Object.keys(this.state));
  // console.log(this.state['Maharashtra']);
  

  // console.log(this.villege);

}
changeState(count){
  this.city=this.state_list[count]
}
  ngOnInit() {

    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'cuisines'

    });



    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
     
     
       
        this.response = JSON.parse(response)
        console.log('response', this.response['cuisinePhotoUrls']);
       
        this.multiResponse.push(this.response['cuisinePhotoUrls'][0])
        console.log('multi response', this.multiResponse);
        
      // alert('File uploaded successfully');
     
      this.loading = false
    };

    
    this.addCuisines()

  }

  setLoading(){
    this.loading = true
  }

 
  // header upload images

  // onUpload(){

  //   let imgUpload = this.addCuisinesForm.controls['images'].value

  //   console.log('images upload object', imgUpload);

  //   this.http.post(URL, ).subscribe(res=>{
  //     console.log('response for it', res);

  //   })
  // }

  //add cuisines form 
  addCuisines() {
    this.addCuisinesForm = this.fb.group({
      cuisineName: ['', [Validators.required, Validators.pattern('^[0-9a-zA-Z ]+$')]],
      country:['',Validators.required],
      state: ['',Validators.required],
      city: ['',Validators.required],
      combinationOfCusines: [''],
      images: []
    })
  }

  applyFilterCuisine(event) {
    this.cuisines = this.tempCuisines
    console.log(event.target.value);
    if (event.target.value == "") {
      this.cuisines = this.tempCuisines
    } else {
      var result = this.cuisines.filter(function (item) {
        return JSON.stringify(item).toLowerCase().includes(event.target.value);
      })
      console.log(result);
      this.cuisines = result
    }
  }


  onCheckCuisine(s) {
    if (this.cuisines.includes(s)) {
      this.cuisines.splice(this.cuisines.indexOf(s), 1)
      for( var i = 0; i < this.tempCuisines.length; i++){ 
        if ( this.tempCuisines[i]._id === s._id) {
          this.tempCuisines.splice(i, 1); 
        }
      }
      if (this.selectedAlbums.includes(s.cuisineName)) {
        alert("Already exists in selection")
      } else {
        this.selectedAlbums.push(s)
        this.selectedCuisinesIds.push(s._id)
      }
    }
    console.log(this.selectedAlbums);
    console.log(this.selectedCuisinesIds)

  }
  checkCuisineExist(event){
    this.addMenuFoodsService.checkCuisineExist(event.target.value.toLowerCase()).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        Swal.fire("Cuisine Name Already Exist..!","", "warning")
        event.target.value=""
        this.addCuisinesForm.patchValue({
          cuisineName:''
        })
      }

    })
  }


  // ON REMOVE PUB MORE INFOS
  removePubs(s) {
    // full object of cuisines
    if (this.selectedAlbums.includes(s)) {
      this.selectedAlbums.splice(this.selectedAlbums.indexOf(s), 1)
      this.cuisines.push(s);
     
      console.log('selected Cuisines', this.selectedAlbums)
    }

    // only single key of cuisne as id
    if (this.selectedCuisinesIds.includes(s._id)) {
      this.selectedCuisinesIds.splice(this.selectedCuisinesIds.indexOf(s._id), 1)
      console.log('selected Cuisines', this.selectedCuisinesIds);
    }
  }

  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }


  urls = [];
  onSelectFile(event) {

    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          console.log('target result', event.target.result);
          this.urls.push(event.target.result);
          console.log('url', this.urls)
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }

  
// **************************Final add cuisines************
  onAddCuisnes() {
    this.addCuisinesForm.patchValue({
      combinationOfCusines: this.selectedCuisinesIds,
      images: this.multiResponse
    })
    console.log(this.addCuisinesForm.value);

    let addCuisinesObject = {
      "cuisineName": this.addCuisinesForm.controls['cuisineName'].value,
      "country":this.addCuisinesForm.controls['country'].value,
      "state":this.addCuisinesForm.controls['state'].value,
      "city":this.addCuisinesForm.controls['city'].value,
      "combinationOfCusines": this.selectedCuisinesIds,
      "images": this.multiResponse,
      "userid": this.userid
    }
    this.addMenuFoodsService.postCuisines(addCuisinesObject).subscribe(addCuisinesResponse=>{
      console.log('ADD Cusines resposne', addCuisinesResponse);
      if(addCuisinesResponse['sucess']){
        Swal.fire('Cuisine Added Successfully','','success')
        
        // this.router.navigate(['/menu'])
      }
      else{
        Swal.fire('Failed to Add Cuisines','Something went wrong','warning')
      }
      
    })

  }




}



