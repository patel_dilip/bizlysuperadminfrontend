import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';

@Component({
  selector: 'app-add-root-menu',
  templateUrl: './add-root-menu.component.html',
  styleUrls: ['./add-root-menu.component.scss']
})
export class AddRootMenuComponent implements OnInit {

  rootMenuForm: FormGroup
  userid
  constructor(private fb: FormBuilder, private restServ: RestoService) { 
    this.getForm()

    // ************user id localstorage ******************
    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];
   
   
    
  }

  getForm(){
    this.rootMenuForm = this.fb.group({
      rootCategoryName: [''],
      isFilterable: false,
      isSearchable: false,
      priority: [],
      userid: ['']
    })
  }

  ngOnInit() {
  }

  onSave(){
    // rootCategoryName"s"isFilterable
    // false
    // isSearchable
    // false
    // userid

    this.rootMenuForm.patchValue({
      userid: this.userid
    })

    console.log('root menu', this.rootMenuForm.value);


    // post add root menu
    this.restServ.postRootMenu(this.rootMenuForm.value).subscribe(res=>{
      console.log('response', res);
      
    })
    
  }

}
