import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRootMenuComponent } from './add-root-menu.component';

describe('AddRootMenuComponent', () => {
  let component: AddRootMenuComponent;
  let fixture: ComponentFixture<AddRootMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRootMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRootMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
