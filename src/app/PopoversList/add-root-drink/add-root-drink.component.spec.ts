import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRootDrinkComponent } from './add-root-drink.component';

describe('AddRootDrinkComponent', () => {
  let component: AddRootDrinkComponent;
  let fixture: ComponentFixture<AddRootDrinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRootDrinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRootDrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
