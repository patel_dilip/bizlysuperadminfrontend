import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-root-drink',
  templateUrl: './add-root-drink.component.html',
  styleUrls: ['./add-root-drink.component.scss']
})
export class AddRootDrinkComponent implements OnInit {

  RootDrinkForm: FormGroup
  userid
  constructor(private fb: FormBuilder, private addMenuServ: AddMenuFoodsService) { 
    
// ************user id localstorage ******************
let userdata = JSON.parse(localStorage.getItem('loginUser'))
this.userid = userdata['_id'];
  }

  ngOnInit() {
    this.RootDrinkForm = this.fb.group({
      rootdrinkName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]]
    })
  }


  // **************add root drink api hit****************

  onAddRoot(){
    
    let addRootObject = {
      "rootDrinkType": this.RootDrinkForm.controls['rootdrinkName'].value,
      "userid": this.userid
    }

    // post api hit
    this.addMenuServ.postRootDrink(addRootObject).subscribe(addRootObject=>{
      if(addRootObject['sucess']){
        Swal.fire('Drink Added Successfully','','success')
      }
      else{
        Swal.fire('Fail Adding Drink','','warning')
      }
    })

    
  }

}
