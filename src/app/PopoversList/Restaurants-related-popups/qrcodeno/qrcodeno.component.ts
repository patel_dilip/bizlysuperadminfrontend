import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-qrcodeno',
  templateUrl: './qrcodeno.component.html',
  styleUrls: ['./qrcodeno.component.scss']
})
export class QrcodenoComponent implements OnInit {

  localLayout: any= []

  qrForm: FormGroup
  constructor(@Inject(MAT_DIALOG_DATA)public data, private fb: FormBuilder) { 
  

    this.localLayout =data

    console.log('layout arr', this.localLayout);

    
  }

  ngOnInit() {
    this.qrForm = this.fb.group({
      quantity: ['']
    })
  }

  onQuantity(){

    let qty = this.qrForm.controls['quantity'].value

    console.log('quatity code',qty)
    this.localLayout.qrcode = qty

    console.log('localLayout', this.localLayout);
    
  }

}
