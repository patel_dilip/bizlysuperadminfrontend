import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrcodenoComponent } from './qrcodeno.component';

describe('QrcodenoComponent', () => {
  let component: QrcodenoComponent;
  let fixture: ComponentFixture<QrcodenoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrcodenoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrcodenoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
