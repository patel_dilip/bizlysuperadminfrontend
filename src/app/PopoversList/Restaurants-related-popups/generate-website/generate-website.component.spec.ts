import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateWebsiteComponent } from './generate-website.component';

describe('GenerateWebsiteComponent', () => {
  let component: GenerateWebsiteComponent;
  let fixture: ComponentFixture<GenerateWebsiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateWebsiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateWebsiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
