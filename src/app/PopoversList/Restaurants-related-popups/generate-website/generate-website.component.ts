import { Component, OnInit, ɵSWITCH_TEMPLATE_REF_FACTORY__POST_R3__ } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-generate-website',
  templateUrl: './generate-website.component.html',
  styleUrls: ['./generate-website.component.scss']
})
export class GenerateWebsiteComponent implements OnInit {

  websiteForm: FormGroup
  isdivHide = false
  generatedWeb:any=[]
  choice =''

  saveWebName: any
  isUpdate: boolean= true;
  constructor(private fb: FormBuilder, private restServ: RestoService) {
    
    this.websiteForm = this.fb.group({
      websitename: ['', Validators.required]
    })
  }

  ngOnInit() {
  }

  onTryDiff(){
    this.isdivHide = false
    // this.websiteForm.reset()
  }

  onsubmitWeb(){
    console.log(
      'website name', this.websiteForm.value
    );
    
   let trimName = this.websiteForm.controls['websitename'].value
  

    let obj = {
      websitename: 'www.bizly.in/'+trimName.replace(/\s/g, "")
    }
    
    console.log('web obj', obj);
    
    this.restServ.postwebsiteName(obj).subscribe(res=>{
      console.log('response web name', res);

      this.generatedWeb = obj
      this.isdivHide = true

      if(res['success']){

       
        this.choice = 'success'

        this.saveWebName = {
          webid: res['id'],
          websitename: this.generatedWeb.websitename
        }
      }
      else{
      
        this.choice = 'failure'

        this.saveWebName = {
          webid: '',
          websitename: this.generatedWeb.websitename
        }
      }
      
    })
  }


  onChangeWebName(){

   
    let id = this.saveWebName.webid;

    let trimName = this.websiteForm.controls['websitename'].value


    let webObj = {
      websitename: 'www.bizly.in/'+trimName.replace(/\s/g, "")
    }

    this.restServ.putChangeWebName(id, webObj).subscribe(res=>{
      if(res['success']){
        console.log('successfully changed!', res);
        this.generatedWeb = webObj
        this.isdivHide = true

        this.saveWebName={
          webid: id,
          websitename: this.generatedWeb.websitename
        }
      }
      else{
        console.log('failed to update',res);
        Swal.fire('Failed to Updata','','warning')
      }
    })
  }

  onUpdate(){
    this.isdivHide = false
    this.isUpdate = false
  }
}
