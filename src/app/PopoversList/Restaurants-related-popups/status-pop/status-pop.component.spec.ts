import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusPopComponent } from './status-pop.component';

describe('StatusPopComponent', () => {
  let component: StatusPopComponent;
  let fixture: ComponentFixture<StatusPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusPopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
