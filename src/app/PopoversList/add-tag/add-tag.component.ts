import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

const URL = environment.base_Url + 'cuisine/uploadphotos'

@Component({
  selector: 'app-add-tag',
  templateUrl: './add-tag.component.html',
  styleUrls: ['./add-tag.component.scss']
})
export class AddTagComponent implements OnInit {
  addTagsForm: FormGroup
  cuisineCtrl: FormControl
  selectedTags = []
  Tags = []

  uploader: FileUploader;
  response: string;
  multiResponse = []
  loading = false;
  selectedTagIds = [];
  userid

  constructor(private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService
    , private http: HttpClient, private router: Router) {

    this.addTagsForm = this.fb.group({
      tagName: ['', [Validators.required, Validators.pattern('^[a-zA-Z ]+$')]]
    })

    // FETCH USER ID FROM LOCAL STORAGE

    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];

  }

  ngOnInit() {

  }



  // **************************Final add Tags************

  onAddTag() {

    let addTagObject = {
      "tagName": this.addTagsForm.controls['tagName'].value,
      "userid": this.userid
    }

    console.log('add tag object', addTagObject);

    // hiTTING POST API FOR ADD TAG

    this.addMenuFoodsService.postTags(addTagObject).subscribe(addTagResposne=>{
      console.log('add tag response', addTagResposne);

      // sweet alert msg
    if(addTagResposne['sucess']){
      Swal.fire('Tag Added Successfully','','success')
    }
   else{
    Swal.fire('Failed to Add','','warning')
   }
      
    })

    

  }


}



