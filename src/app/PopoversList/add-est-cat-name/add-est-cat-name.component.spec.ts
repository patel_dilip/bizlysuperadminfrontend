import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEstCatNameComponent } from './add-est-cat-name.component';

describe('AddEstCatNameComponent', () => {
  let component: AddEstCatNameComponent;
  let fixture: ComponentFixture<AddEstCatNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEstCatNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEstCatNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
