import { Component, OnInit, Inject } from '@angular/core';
import Swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { FeedbackService } from 'src/app/_services/_feedbackServices/feedback.service';

@Component({
  selector: 'app-add-childchild-category-que-type',
  templateUrl: './add-childchild-category-que-type.component.html',
  styleUrls: ['./add-childchild-category-que-type.component.scss']
})
export class AddChildchildCategoryQueTypeComponent implements OnInit {
  addSubSubCategoryForm: FormGroup;
  childchildCategoryIDs: any;

  constructor(private fb: FormBuilder,
    private feedbackService: FeedbackService,
       private matDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data) {
      console.log(data);
      this.childchildCategoryIDs = data
  //  this.getRootTypes();

  }

  ngOnInit() {
    this.addSubSubCategoryForm = this.fb.group({
      childchildCategoryName: ['',[Validators.required, Validators.pattern('^[ a-zA-Z]+$')]]
      })
  }

  



  //*********FINAL ADD SUB SUB DRINK NAME ************/
 

  onAddSubSubCategory(){

    let childCategoryName = this.addSubSubCategoryForm.controls['childchildCategoryName'].value

    var addSubSubObj ={
      "childCategoryName": childCategoryName
  }

    this.feedbackService.putSubSubCategorytype(this.childchildCategoryIDs.rootQueID, this.childchildCategoryIDs.categorytypeID, addSubSubObj)
    .subscribe(addsubsubRes=>{
      if(addsubsubRes['success']){
        Swal.fire('sub sub category added', '', 'success')
      }
      else {
        Swal.fire('Fail Adding Sub Sub category', '', 'warning')
      }
    })
  }
}