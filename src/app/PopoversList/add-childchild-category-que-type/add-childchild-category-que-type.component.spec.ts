import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChildchildCategoryQueTypeComponent } from './add-childchild-category-que-type.component';

describe('AddChildchildCategoryQueTypeComponent', () => {
  let component: AddChildchildCategoryQueTypeComponent;
  let fixture: ComponentFixture<AddChildchildCategoryQueTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChildchildCategoryQueTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChildchildCategoryQueTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
