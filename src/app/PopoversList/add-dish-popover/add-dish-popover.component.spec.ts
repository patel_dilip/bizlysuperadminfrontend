import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDishPopoverComponent } from './add-dish-popover.component';

describe('AddDishPopoverComponent', () => {
  let component: AddDishPopoverComponent;
  let fixture: ComponentFixture<AddDishPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDishPopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDishPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
