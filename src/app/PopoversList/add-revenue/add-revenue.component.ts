import { Component, OnInit } from '@angular/core';
import {  FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-revenue',
  templateUrl: './add-revenue.component.html',
  styleUrls: ['./add-revenue.component.scss']
})
export class AddRevenueComponent implements OnInit {

  revenueForm: FormGroup
  fromValue: any;
  ToValue:any
  isEqualMsg: boolean = true;
  isLesser: boolean = true;
  isAdd: boolean = true;

  constructor(private fb: FormBuilder, private restoServ: RestoService) { 
    this.revenueForm = this.fb.group({
      fromRevenue: ['',[ Validators.required, Validators.pattern("^[0-9,]+$")]],
      toRevenue: ['',[ Validators.required, Validators.pattern("^[0-9,]+$")]]
    })
  }

  ngOnInit() {
  }



  setfromValue(evt){
    let val = this.revenueForm.controls['fromRevenue'].value
   
    

    // this.revenueForm.patchValue({
    //   fromRevenue: parseInt(val).toLocaleString()
    // })

   

    console.log('patch value', this.revenueForm.controls['fromRevenue'].value);
    this.fromValue = this.revenueForm.controls['fromRevenue'].value

    if(this.ToValue){
      if(this.ToValue<this.fromValue){
        this.revenueForm.controls['fromRevenue'].reset()
        this.isAdd = true
      }
      else{
        this.isAdd = false
      }
    }

  }

  setToValue(evt){
    let val = this.revenueForm.controls['toRevenue'].value
   
    // this.revenueForm.patchValue({
    //   toRevenue: parseInt(val).toLocaleString()
    // })

    console.log('patch value', this.revenueForm.controls['toRevenue'].value);

    this.ToValue = this.revenueForm.controls['toRevenue'].value
  
    if(this.fromValue){
      if(this.ToValue<this.fromValue){
        this.isAdd = true
      }
      else{
        this.isAdd = false
      }
    }
  }

 
  onAddRevenue(){

    let from = this.revenueForm.controls['fromRevenue'].value
    let to = this.revenueForm.controls['toRevenue'].value

    // from = parseInt(from).toLocaleString
    // to = parseInt(to).toLocaleString
    let restoRevenue = {"from": from, "to": to}
    
    var restoObj ={
      restoRevenue: restoRevenue
    }

    console.log('revenue', restoObj);
     
    if(this.fromValue){
      if(this.ToValue<this.fromValue){
        this.revenueForm.controls['toRevenue'].reset()
        this.isAdd = true
      }else{
        this.isAdd = false
        this.restoServ.postAddRevenue(restoObj).subscribe(addRevenueRes=>{
          if(addRevenueRes['sucess']) {
            Swal.fire('Revenue Added Successfully','','success')
          }
          else{
            Swal.fire('Something Went Wrong','','warning')
          }
        })
      }
    }
    
   
  }
}
