import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-sub-cat',
  templateUrl: './sub-cat.component.html',
  styleUrls: ['./sub-cat.component.scss']
})
export class SubCatComponent implements OnInit {

  subCategory: FormGroup
  CatArr: string[] = ['Cigar Bar','Indoor Bar','Irish Bar', 'Outdoor Bar']
  PrioArr: string[] = ['1','2','3'];
  filteredOptions: Observable<string[]>;
  // filteredOptions1: Observable<string[]>;
  myControl = new FormControl();
  constructor(private fb: FormBuilder, private mainService: MainService) { }

  ngOnInit() {
    this.getForm();


    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );

  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.CatArr.filter(option => option.toLowerCase().includes(filterValue));
  }

  getForm(){
    this.subCategory = this.fb.group({
      sub_cat_name: [],
      filterable: [''],
      searchable: [''],
      priority: ['']
    })
  }

  onSave(){

    this.subCategory.patchValue({
      sub_cat_name: this.myControl.value
    })
    console.log(
      'sub category values', this.subCategory.value
    );

    this.mainService.popSubCategData = this.subCategory.value
    
  }

}
