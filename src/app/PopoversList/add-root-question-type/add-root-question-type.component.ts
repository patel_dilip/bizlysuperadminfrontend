import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';
import { FeedbackService } from 'src/app/_services/_feedbackServices/feedback.service';

@Component({
  selector: 'app-add-root-question-type',
  templateUrl: './add-root-question-type.component.html',
  styleUrls: ['./add-root-question-type.component.scss']
})
export class AddRootQuestionTypeComponent implements OnInit {

  RootQuestionTypeForm: FormGroup
  userid
  constructor(private fb: FormBuilder, private feedbackService: FeedbackService) { 
    


  }

  ngOnInit() {
    this.RootQuestionTypeForm = this.fb.group({
      rootquestiontypeName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]]
    })
  }


  // **************add root question type api hit****************

  onAddRoot(){
    
    let addRootQueObject = {
      "rootQuestionType": this.RootQuestionTypeForm.controls['rootquestiontypeName'].value,
      "userid": this.userid
    }

    // post api hit
    this.feedbackService.postRootQuestionType(addRootQueObject).subscribe(addRootQueObject=>{
      if(addRootQueObject['success']){
        Swal.fire('RootQuestion Type Added Successfully','','success')
      }
      else{
        Swal.fire('Fail Adding Question','','warning')
      }
    })

    
  }



}
