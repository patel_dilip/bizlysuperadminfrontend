import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRootQuestionTypeComponent } from './add-root-question-type.component';

describe('AddRootQuestionTypeComponent', () => {
  let component: AddRootQuestionTypeComponent;
  let fixture: ComponentFixture<AddRootQuestionTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRootQuestionTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRootQuestionTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
