import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRestoTypeComponent } from './add-resto-type.component';

describe('AddRestoTypeComponent', () => {
  let component: AddRestoTypeComponent;
  let fixture: ComponentFixture<AddRestoTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRestoTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRestoTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
