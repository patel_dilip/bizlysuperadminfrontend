import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-resto-type',
  templateUrl: './add-resto-type.component.html',
  styleUrls: ['./add-resto-type.component.scss']
})
export class AddRestoTypeComponent implements OnInit {

  restoType: FormGroup
  userid
  constructor(private fb: FormBuilder, private restoServ: RestoService) {

    // ************user id localstorage ******************
    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];

    this.restoType = this.fb.group({
      restaurantTypeName: ['', Validators.required]
    })
  }

  ngOnInit() {
  }

  onAddRestoType() {

    var restotypeObj = {
      "restaurantTypeName": this.restoType.controls['restaurantTypeName'].value,
      "userid": this.userid
    }

    console.log('revenue', restotypeObj);

    this.restoServ.postAddRestoType(restotypeObj).subscribe(addRestoRes => {
      if (addRestoRes['sucess']) {
        Swal.fire('Restaurant Type Added Successfully', '', 'success')
      }
      else {
        Swal.fire('Something Went Wrong', '', 'warning')
      }
    })
  }

}
