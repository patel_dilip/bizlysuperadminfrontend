import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MainService } from 'src/app/_services/main.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-drink-subvarient',
  templateUrl: './add-drink-subvarient.component.html',
  styleUrls: ['./add-drink-subvarient.component.scss']
})
export class AddDrinkSubvarientComponent implements OnInit {

  addSubVarientForm: FormGroup;
  DrinkTypes: any;
  VarientsArray: any
  selectRootDrinkId: any;
  selectVarientId: any;
  liquorCategoryIDs: any

  constructor(private fb: FormBuilder,
    private addMenuFoodsService: AddMenuFoodsService,
    private mainServ: MainService,
    private matDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data) {
      console.log(data);
      this.liquorCategoryIDs= data
      
  //  this.getRootTypes();

  }

  ngOnInit() {
    this.addSubVarientForm = this.fb.group({
     // rootdrink: ['', Validators.required],
     // varientName: ['', Validators.required],
      subVarientName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]]
    })
  }

  // // getting all root drink types   
  // getRootTypes() {

  //   this.addMenuFoodsService.getAllRootDrinks().subscribe(data => {
  //     this.DrinkTypes = data['data']

  //     console.log('drinktypes', this.DrinkTypes);

  //   })
  // }

  // // ************FILETER DROPDOWN LIST***********************

  // // ROOT DRINK FILETER
  // onRootDrinkSelection(evt) {

  //   // this.getRootTypes();
  //   console.log(evt.target.selectedIndex);
  //   console.log(this.DrinkTypes);
  //   let a = evt.target.selectedIndex

  //   // find drink root object fliter
  //   let singleDrinkobj = this.DrinkTypes[a - 1]
  //   console.log('object', singleDrinkobj
  //   );

  //   // selected drink root id
  //   this.selectRootDrinkId = singleDrinkobj['_id']
  //   console.log('id of selected root drink', this.selectRootDrinkId);


  //   // find liquor varient object flier
  //   this.VarientsArray = singleDrinkobj['liquorVarients']
  //   console.log('varients', this.VarientsArray);

  // }



  // // Varient FILETER CODE
  // onVarientSelection(evt) {
  //   let varientIdx = evt.target.selectedIndex

  //   let singleVarientObj = this.VarientsArray[varientIdx - 1]

  //   console.log('single varient object', singleVarientObj);

  //   // seleted varient id
  //   this.selectVarientId = singleVarientObj['_id']
  //   console.log('selected varient id', this.selectVarientId);


  // }



  //*********FINAL ADD SUB VARIENT NAME ************/
  onAddSubVarient() {

    var liquorSubVarientName = this.addSubVarientForm.controls['subVarientName'].value

    var obj = {
      "liquorSubVarientName": liquorSubVarientName
    }

    this.addMenuFoodsService.putSubVarientDrink(this.liquorCategoryIDs.rootDrinkID, this.liquorCategoryIDs.liquorVarient, obj)
      .subscribe(addsubvarientRes => {
        if (addsubvarientRes['sucess']) {
          Swal.fire('sub varient added', '', 'success')
        }
        else {
          Swal.fire('Fail Adding Sub Varient', '', 'warning')
        }
      })

  }
}
