import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDrinkSubvarientComponent } from './add-drink-subvarient.component';

describe('AddDrinkSubvarientComponent', () => {
  let component: AddDrinkSubvarientComponent;
  let fixture: ComponentFixture<AddDrinkSubvarientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDrinkSubvarientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDrinkSubvarientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
