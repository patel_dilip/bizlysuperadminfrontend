import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AddVarientContentComponent } from 'src/app/PopoversList/add-varient-content/add-varient-content.component';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

const URL = environment.base_Url + 'varient/uploadphotos'


@Component({
  selector: 'app-add-varient-popover',
  templateUrl: './add-varient-popover.component.html',
  styleUrls: ['./add-varient-popover.component.scss']
})
export class AddVarientPopoverComponent implements OnInit {

  addVarientsForm: FormGroup
  cuisineCtrl: FormControl

  selecteddishes = []
  selectedvarients = []
  varientContents = []
  tempVarientContent = []
  uploader: FileUploader;
  response: string
  multiResponse = []
  loading = false;
  loginUserID: any
  add: any;

  constructor(private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService, private matDialog: MatDialog,
    private menuService: AddMenuFoodsService, private router: Router, public dialogRef: MatDialogRef<AddVarientPopoverComponent>) {

    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);

    //get all varient content
    this.getVarientContent()


  }

  ngOnInit() {
    this.addVarient()

    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'varients'

    });
    //file upload response with url
    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);


      this.response = JSON.parse(response)
      console.log('response', this.response['varientPhotoUrls']);

      this.multiResponse.push(this.response['varientPhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      this.loading = false
    };


  }


  setLoading() {
    this.loading = true
  }

  //get varient content
  getVarientContent() {
    // this.loading = true
    this.menuService.getVarientContent().subscribe(data => {
      console.log(data);
      if (data['sucess'] == true) {
        this.loading = false
        this.varientContents = data['data']
        this.tempVarientContent = data['data']
      }

    })
  }

  //add varient form 
  addVarient() {
    this.addVarientsForm = this.fb.group({
      varientName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      varientContent: [''],
      images: [],
      userid: [''],
      varientType:this.fb.array([])
      
    })
    this.addVarientType()
  }

  applyFilterVarientContent(event) {
    this.varientContents = this.tempVarientContent
    console.log(event.target.value);
    if (event.target.value == "") {
      this.varientContents = this.tempVarientContent
    } else {
      var result = this.varientContents.filter(function (item) {
        return JSON.stringify(item).toLowerCase().includes(event.target.value);
      })
      console.log(result);
      this.varientContents = result
    }
  }


  //on select chips add to selected varient
  //remove chips from varient array
  onCheckvarients(s) {
    console.log(s);

    if (this.varientContents.includes(s)) {
      this.varientContents.splice(this.varientContents.indexOf(s), 1)
    
      for( var i = 0; i < this.tempVarientContent.length; i++){ 
        if ( this.tempVarientContent[i]._id === s._id) {
          this.tempVarientContent.splice(i, 1); 
        }
      }
      if (this.selectedvarients.includes(s.varient)) {
        alert("Already exists in selection")
      } else {
        this.selectedvarients.push(s)
      }
    }
    console.log(this.selectedvarients);

  }


  // remove selected chips and add to  push to varients array
  removevarients(s) {
    if (this.selectedvarients.includes(s)) {
      this.selectedvarients.splice(this.selectedvarients.indexOf(s), 1)
      this.varientContents.push(s);
      
    }
  }



  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }

  //add varient content pop-up
  openAddVarientDialog(): void {

    console.log('opened Add cuisine dialog');

    const dialogRef = this.matDialog.open(AddVarientContentComponent, {
      width: '540px',
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      this.getVarientContent()
      console.log('The dialog was closed');

    });

  }

  //final add varient
  onAddAlbum() {
    // this.loading = true
    //get selected varient contents ids 
    let varientContentIDS = []
    this.selectedvarients.forEach(element => {
      varientContentIDS.push(element._id)
    });

    //patch all value to final object
    this.addVarientsForm.patchValue({
      varientContent: varientContentIDS,
      images: this.multiResponse,
      userid: this.loginUserID
    })
    console.log(this.addVarientsForm.value);

    this.menuService.postVarient(this.addVarientsForm.value).subscribe(res => {
      this.loading = false
      console.log("add varient", res);
      if (res['sucess'] == true) {

        Swal.fire('Varient Added Successfully', '', 'success')
        this.dialogRef.close();
      }
      else {
        Swal.fire('Failed to Add Vareint', 'Something went wrong', 'warning')
      }
    })

  }
  RemoveVarientType(i){
    console.log(i);
    this.add.removeAt(i);
    console.log(this.add.value);
    
    
  }
  getControls() {
    return (this.addVarientsForm.get('varientType') as FormArray).controls;
  }
addVarientType(){
  this.add = this.addVarientsForm.get('varientType') as FormArray;
    this.add.push(this.fb.group({
      varientName_type: [''],
     type: ['']
    }))
    console.log(this.add.value);
    
}
  onNoClick(): void {
    this.dialogRef.close();
  }
}
