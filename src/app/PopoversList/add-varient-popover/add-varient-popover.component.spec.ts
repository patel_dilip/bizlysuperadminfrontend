import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVarientPopoverComponent } from './add-varient-popover.component';

describe('AddVarientPopoverComponent', () => {
  let component: AddVarientPopoverComponent;
  let fixture: ComponentFixture<AddVarientPopoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVarientPopoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVarientPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
