import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-add-layout',
  templateUrl: './add-layout.component.html',
  styleUrls: ['./add-layout.component.scss']
})
export class AddLayoutComponent implements OnInit {

  layoutForm: FormGroup
  Layouts= [{title:'Bar',qrcode: 0, qrstatus: false, qrcodeArr: [],  table: [], chairs: [], walls:[], doors: [], billing: [], NumOfTables: []},{title:'Lawn',qrcode: 0, qrstatus: false, qrcodeArr: [], table: [], chairs: [], walls:[], doors: [], billing: [], NumOfTables: []},{title:'Rooftop',qrcode: 0, qrstatus: false, qrcodeArr: [], table: [], chairs: [], walls:[], doors: [], billing: [], NumOfTables: []},
  {title:'Garden',qrcode: 0, qrstatus: false, qrcodeArr: [], table: [], chairs: [], walls:[], doors: [], billing: [], NumOfTables: []},{title:'Indoor',qrcode: 0, qrstatus: false, qrcodeArr: [], table: [], chairs: [], walls:[], doors: [], billing: [], NumOfTables: []},{title:'Outdoor',qrcode: 0, qrstatus: false, qrcodeArr: [], table: [], chairs: [], walls:[], doors: [], billing: [], NumOfTables: []},{title:'Court',qrcode: 0, qrstatus: false, qrcodeArr: [], table: [], chairs: [], walls:[], doors: [], billing: [], NumOfTables: []},
  {title:'Lounge',qrcode: 0, qrstatus: false, qrcodeArr: [], table: [], chairs: [], walls:[], doors: [], billing: [], NumOfTables: []},{title:'Dine-In',qrcode: 0, qrstatus: false, qrcodeArr: [], table: [], chairs: [], walls:[], doors: [], billing: [], NumOfTables: []},{title:'Buffet',qrcode: 0, qrstatus: false, qrcodeArr: [], table: [], chairs: [], walls:[], doors: [], billing: [], NumOfTables: []}]
  selectedLayouts
  localdata
  AddLayoutComponent
  layoutTitle
  titlexists = false
  constructor(@Inject(MAT_DIALOG_DATA) public data,private fb: FormBuilder, private mainService: MainService) { 

    // if(this.mainService.layoutName!=undefined){
    //   this.selectedLayouts =this.mainService.layoutName;
     
    // }

    
    this.localdata = <object>data
    // console.log('data ', this.selectedLayouts)

  

    console.log('selected', this.localdata);
    
  }

  ngOnInit() {

    this.layoutForm = this.fb.group({
      layout_name: ['']
    })
  }

  addLayout(){
    console.log('value',this.layoutForm.value);

   this.layoutTitle = this.layoutForm.controls['layout_name'].value
    
  }

  onChkLayouts(s){
    if(this.localdata.includes(s)){
      alert('Alreday Exists');
      
    }else{
      this.localdata.push(s)
    }
  
    
    
  }

onRemove(s){
  this.localdata.splice(this.localdata.IndexOf(s),1)
  this.Layouts.push(s)
}



}
