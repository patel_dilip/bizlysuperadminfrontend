import { Component, OnInit, Inject } from '@angular/core';
import { MainService } from 'src/app/_services/main.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-open-table',
  templateUrl: './open-table.component.html',
  styleUrls: ['./open-table.component.scss']
})
export class OpenTableComponent implements OnInit {
  createTableForm: FormGroup

selectedTables = []
currenttab;
bg = 'blue'
tableObj: Object
  constructor(@Inject(MAT_DIALOG_DATA)public data,
  private mainService: MainService,
  private fb: FormBuilder) { 
   
    console.log("TABLE",data)

    this.createTableForm = this.fb.group({
      capacity: ['', Validators.required],
      shape: ['', Validators.required],
      tablename: ['', Validators.required],
      // quantity: ['']
    })
    
  }

  ngOnInit() {
  }

  onTabClick(){
    alert('table clicked')
  }


  onAddTable(){
    console.log(this.createTableForm.value);

    var obj = { 
      tablename: this.createTableForm.controls['tablename'].value,
      capacity: this.createTableForm.controls['capacity'].value,
      shape: this.createTableForm.controls['shape'].value,
      // quantity: this.createTableForm.controls['quantity'].value
    }

    console.log('table', this.data.table.push(obj))
    console.log('table', this.data)
  }

}
