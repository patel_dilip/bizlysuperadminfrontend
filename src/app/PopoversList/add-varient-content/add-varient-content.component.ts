import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-varient-content',
  templateUrl: './add-varient-content.component.html',
  styleUrls: ['./add-varient-content.component.scss']
})
export class AddVarientContentComponent implements OnInit {
  varientContentForm: FormGroup
  loginUserID: any
  constructor(public dialogRef: MatDialogRef<AddVarientContentComponent>, private fb: FormBuilder,
    private menuService: AddMenuFoodsService) {
      //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);

  }

  ngOnInit() {
    this.varientContentForm = this.fb.group({
      contentName: ['', [Validators.required,Validators.pattern('^[ a-zA-Z0]+$')]],
      userid: ['']
    })
  }

  // add final varient content 
  addvarientContent() {
  
    this.varientContentForm.patchValue({
      userid: this.loginUserID
    })
    //console.log(this.varientContentForm.value);

    this.menuService.postVarientContent(this.varientContentForm.value).subscribe(res => {
      console.log(res);
      if (res['sucess'] == true) {
        Swal.fire('Varient Content Added Successfully', '', 'success')
        this.dialogRef.close();
      }
      else {
        Swal.fire('Failed to Add Varient Content', 'Something went wrong', 'warning')
      }

    })


  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
