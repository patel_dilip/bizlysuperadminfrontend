import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVarientContentComponent } from './add-varient-content.component';

describe('AddVarientContentComponent', () => {
  let component: AddVarientContentComponent;
  let fixture: ComponentFixture<AddVarientContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVarientContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVarientContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
