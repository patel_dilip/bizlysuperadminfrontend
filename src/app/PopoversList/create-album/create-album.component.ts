import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { MAT_DIALOG_DATA } from '@angular/material';
import { element } from 'protractor';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-create-album',
  templateUrl: './create-album.component.html',
  styleUrls: ['./create-album.component.scss']
})
export class CreateAlbumComponent implements OnInit {

  addCategory: FormGroup
 
  selectedAlbums= []
 commonTitles = []
  selectedEstablishments = []
  FilteredList=[]
  elementArr = [] 
 
  constructor(private fb: FormBuilder, private mainService: MainService, @Inject(MAT_DIALOG_DATA)public data) { 
    
   console.log('data in album', data);

  //  moreinfo
    data.moreinfo.forEach(ele => {     
        this.selectedEstablishments.push(ele)     
    });

  //   // services
    data.service.forEach(ele => {     
      this.selectedEstablishments.push(ele)     
  });

  //   // ambiences
    data.ambiences.forEach(ele => {     
      this.selectedEstablishments.push(ele)     
  });

  this.selectedEstablishments.forEach(element=>{
    if(!this.commonTitles.includes(element.title)){
      this.commonTitles.push(element.title)
    }
  })

  console.log("common titles",this.commonTitles);
  
// more info
  this.commonTitles.forEach(element=>{

    this.elementArr =[]
    this.selectedEstablishments.forEach(ele=>{
      if('moreinfo' in ele){
        if(element==ele.title){
          this.elementArr.push(ele.moreinfo)
        }
      }

      if('service' in ele){
        if(element==ele.title){
          this.elementArr.push(ele.service)
        }
      }

      if('ambiences' in ele){
        if(element==ele.title){
          this.elementArr.push(ele.ambiences)
        }
      }
     
    })
    
    this.FilteredList.push({title:element,value:this.elementArr})
  })


  // this.selectedEstablishments.forEach(element => {
  
  // });

    console.log('selected Estalbishment', this.selectedEstablishments);
    console.log('Filtered List', this.FilteredList);
    
  //more info
 
  

  if(this.mainService.selectedAlbums!=undefined){
    this.selectedAlbums =this.mainService.selectedAlbums;
   
  }



  }

  ngOnInit() { 
  

  }

  

 

  // CheckedList
  onCreateAlbum(obj,value){
    // if(obj.value.includes(value)){
    //  obj.value.splice(obj.value.indexOf(value),1)

    
      if(this.selectedAlbums.includes(value)){
        Swal.fire('Already Exist!!','','warning')
      }
      else{
        this.selectedAlbums.push(value)
      }
    // }
  }


  // ON REMOVE PUB MORE INFOS
  removePubs(s){
    if(this.selectedAlbums.includes(s)){
      this.selectedAlbums.splice(this.selectedAlbums.indexOf(s), 1)      
      
      // this.FilteredList.forEach(element => {
      //   if(element.value.includes(s)){
      //     element.value.push(s)
      //   }
      // });
    }
  
  }


 
  onAddAlbum(){
    this.mainService.popMediaAddData = this.selectedAlbums;
    console.log('the selected array', this.selectedAlbums);
    
  }
  

 

}
