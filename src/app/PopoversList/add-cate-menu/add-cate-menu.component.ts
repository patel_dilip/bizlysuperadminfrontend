import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';

@Component({
  selector: 'app-add-cate-menu',
  templateUrl: './add-cate-menu.component.html',
  styleUrls: ['./add-cate-menu.component.scss']
})
export class AddCateMenuComponent implements OnInit {
  MenuCatForm: FormGroup
  rootCategoryId
  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public root_id, private restServ: RestoService) { 

    this.getForm()
  }

  ngOnInit() {
  }

  getForm(){
    this.MenuCatForm = this.fb.group({
      categoryName: [''],
      isFilterable: false,
      isSearchable: false,
      priority: []
    })
  }

  onSave(){
   var addMenuCatObj = this.MenuCatForm.value

   this.rootCategoryId = this.root_id
   console.log('root category', this.rootCategoryId)

   this.restServ.putCategoryMenu(this.rootCategoryId, addMenuCatObj).subscribe(res=>{
     console.log('response',res);
     
   })
  }

}
