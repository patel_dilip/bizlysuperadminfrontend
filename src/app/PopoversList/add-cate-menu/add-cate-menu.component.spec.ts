import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCateMenuComponent } from './add-cate-menu.component';

describe('AddCateMenuComponent', () => {
  let component: AddCateMenuComponent;
  let fixture: ComponentFixture<AddCateMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCateMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCateMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
