import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';

@Component({
  selector: 'app-add-chip-ambiences',
  templateUrl: './add-chip-ambiences.component.html',
  styleUrls: ['./add-chip-ambiences.component.scss']
})
export class AddChipAmbiencesComponent implements OnInit {

  ambiChipsForm: FormGroup
  ambience= []


  constructor(@Inject(MAT_DIALOG_DATA)private data, 
              private fb: FormBuilder,
              private restServ: RestoService) { 

    console.log('item id',data._id);
    console.log('item title',data.title);
    console.log('item rules', data.ambiences)

    if(data.ambiences!=undefined || data.ambiences!=null){
      this.ambience = data.ambiences
    }

   

    
    this.ambiChipsForm = this.fb.group({
      chip: ['',Validators.required]    

    })
    
  }

  ngOnInit() {

    
  }

  add(){

    let a = this.ambiChipsForm.controls['chip'].value

    if(this.ambience.includes(a)){
      Swal.fire('Already added: '+a)
    }
    else{
    this.ambience.push(a)
    }
    console.log('ambience info', this.ambience);
    this.ambiChipsForm.controls['chip'].reset()
    

      
  }

  remove(a){
   
      this.ambience.splice(this.ambience.indexOf(a),1)

      console.log('rules Info Values', this.ambience);
      
    
    
  }

  submit(){

    let obj = {
      title: this.data.title,
      ambiences: this.ambience
    }

    
    
    this.restServ.putchipstoAmbience(this.data._id, obj ).subscribe(res=>{
      console.log('respo', res);
      
    })
  }

}
