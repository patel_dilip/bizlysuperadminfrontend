import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChipAmbiencesComponent } from './add-chip-ambiences.component';

describe('AddChipAmbiencesComponent', () => {
  let component: AddChipAmbiencesComponent;
  let fixture: ComponentFixture<AddChipAmbiencesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChipAmbiencesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChipAmbiencesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
