import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-more-info',
  templateUrl: './add-more-info.component.html',
  styleUrls: ['./add-more-info.component.scss']
})
export class AddMoreInfoComponent implements OnInit {
  moreInfo : FormGroup
  elementArr= [];
  moreInfoobj: any
  isSubmitted: boolean = true;
  titleChk: any= [];
  constructor(private fb: FormBuilder, private restServ: RestoService,@Inject(MAT_DIALOG_DATA)public data) { 
    this.getForm()

    if(data!=undefined){
      data.forEach(element => {
        this.titleChk.push(element.title)
        
      });
    }
   
  }

  ngOnInit() {
  }

  dupliChk(){
    let a = this.moreInfo.controls['title'].value
    if(this.titleChk.includes(a)){
      Swal.fire('Already Existed!!! ',a)
      this.moreInfo.controls['title'].reset()
    }
  }

  getForm(){
    this.moreInfo = this.fb.group({
     title: ['', [Validators.required, Validators.minLength(3), Validators.pattern('^[ a-zA-Z0-9.]+$')]],
     moreInfoArr: ['', Validators.required]
    })
  }
  onSave(){
   let cuisine = this.moreInfo.controls['moreInfoArr'].value
   
   this.elementArr.push(cuisine)

   console.log('cuisines array', this.elementArr);

   if(this.elementArr.length!=0){
     this.isSubmitted = false
   }

   this.moreInfo.controls['moreInfoArr'].reset()
 
  }

  onSubmit(){

    this.moreInfoobj = {
      title: this.moreInfo.controls['title'].value,
      moreinfo: this.elementArr
    }
    console.log(
      'MORE INFO VALUE', this.moreInfoobj
    );

    this.restServ.postMoreInfo(this.moreInfoobj).subscribe(res=>{
      console.log('respoose',res);
      
    })

    this.restServ.getAllMoreInfos().subscribe(data=>{
      console.log('response data', data);
      
    })


  }



}
