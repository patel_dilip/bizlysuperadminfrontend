import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMoreInfoComponent } from './add-more-info.component';

describe('AddMoreInfoComponent', () => {
  let component: AddMoreInfoComponent;
  let fixture: ComponentFixture<AddMoreInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMoreInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMoreInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
