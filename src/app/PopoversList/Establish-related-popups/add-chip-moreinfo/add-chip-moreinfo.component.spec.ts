import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChipMoreinfoComponent } from './add-chip-moreinfo.component';

describe('AddChipMoreinfoComponent', () => {
  let component: AddChipMoreinfoComponent;
  let fixture: ComponentFixture<AddChipMoreinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChipMoreinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChipMoreinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
