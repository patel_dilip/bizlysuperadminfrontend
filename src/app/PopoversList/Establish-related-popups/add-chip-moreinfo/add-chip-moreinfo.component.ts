import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-chip-moreinfo',
  templateUrl: './add-chip-moreinfo.component.html',
  styleUrls: ['./add-chip-moreinfo.component.scss']
})
export class AddChipMoreinfoComponent implements OnInit {

  morechipsForm: FormGroup
  moreinfo=[]


  constructor(@Inject(MAT_DIALOG_DATA)private data, 
              private fb: FormBuilder,
              private restServ: RestoService) { 

    console.log('item id',data._id);
    console.log('item title',data.title);
    console.log('item moreinfo', data.moreinfo)

    if(data.moreinfo!=undefined || data.moreinfo!=null){
      this.moreinfo = data.moreinfo
    }

   

    
    this.morechipsForm = this.fb.group({
      chip: ['',Validators.required]    

    })
    
  }

  ngOnInit() {

    
  }

  add(){

    let a = this.morechipsForm.controls['chip'].value

    if(this.moreinfo.length==0){
      this.moreinfo.push(a)
    }
  
    if(this.moreinfo.includes(a)){
      Swal.fire('Already added: '+a)
      
    }
    else{
    this.moreinfo.push(a)
    }
    console.log('more info', this.moreinfo);
    this.morechipsForm.controls['chip'].reset()
    

      
  }

  remove(a){
   
      this.moreinfo.splice(this.moreinfo.indexOf(a),1)

      console.log('More Info Values', this.moreinfo);
      
    
    
  }

  submit(){

    let obj = {
      title: this.data.title,
      moreinfo: this.moreinfo
    }
    this.restServ.putchipstoMore(this.data._id, obj ).subscribe(res=>{
      console.log('respo', res);
      
    })
  }

}
