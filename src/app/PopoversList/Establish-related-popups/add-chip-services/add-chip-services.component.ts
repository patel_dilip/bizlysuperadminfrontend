import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-chip-services',
  templateUrl: './add-chip-services.component.html',
  styleUrls: ['./add-chip-services.component.scss']
})
export class AddChipServicesComponent implements OnInit {

  servChipsForm: FormGroup
  services= []


  constructor(@Inject(MAT_DIALOG_DATA)private data, 
              private fb: FormBuilder,
              private restServ: RestoService) { 

    console.log('item id',data._id);
    console.log('item title',data.title);
    console.log('item services', data.services)

    if(data.services!=undefined || data.services!=null){
      this.services = data.services
    }

   

    
    this.servChipsForm = this.fb.group({
      chip: ['',Validators.required]    

    })
    
  }

  ngOnInit() {

    
  }

  add(){

    let a = this.servChipsForm.controls['chip'].value

    if(this.services.includes(a)){
      Swal.fire('Already added: '+a)
    }
    else{
    this.services.push(a)
    }
    console.log('services info', this.services);
    this.servChipsForm.controls['chip'].reset()
    

      
  }

  remove(a){
   
      this.services.splice(this.services.indexOf(a),1)

      console.log('Services Info Values', this.services);
      
    
    
  }

  submit(){

    let obj = {
      title: this.data.title,
      services: this.services
    }
    this.restServ.putchipstoService(this.data._id, obj ).subscribe(res=>{
      console.log('respo', res);
      
    })
  }

}
