import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChipServicesComponent } from './add-chip-services.component';

describe('AddChipServicesComponent', () => {
  let component: AddChipServicesComponent;
  let fixture: ComponentFixture<AddChipServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChipServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChipServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
