import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddServicesChipsComponent } from './add-services-chips.component';

describe('AddServicesChipsComponent', () => {
  let component: AddServicesChipsComponent;
  let fixture: ComponentFixture<AddServicesChipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddServicesChipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddServicesChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
