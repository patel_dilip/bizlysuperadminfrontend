import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-services-chips',
  templateUrl: './add-services-chips.component.html',
  styleUrls: ['./add-services-chips.component.scss']
})
export class AddServicesChipsComponent implements OnInit {
  serviceInfo : FormGroup
  elementArr= [];
  servInfoObj: any
  isSubmitted: boolean = true;
  titleChk: any = [];
  constructor(private fb: FormBuilder, private restServ: RestoService, @Inject(MAT_DIALOG_DATA)public data) { 
    this.getForm()

    if(data!=undefined){
      data.forEach(element => {
        this.titleChk.push(element.title)      
      });
    }
   

    console.log('titles array', this.titleChk)
  }

  ngOnInit() {
  }

  dupliChk(){
    let a = this.serviceInfo.controls['title'].value
    
    if(this.titleChk.includes(a)){
      Swal.fire('Already Exist!! ', a)
      this.serviceInfo.controls['title'].reset()
    }
  }

  getForm(){
    this.serviceInfo = this.fb.group({
     title: ['',Validators.required],
     serviceArr: ['', Validators.required] 
    })
  }
  onSave(){
   let service = this.serviceInfo.controls['serviceArr'].value
  
   this.elementArr.push(service)

   console.log('services array', this.elementArr);

   if(this.elementArr.length!=0){
    this.isSubmitted = false
  }


   this.serviceInfo.controls['serviceArr'].reset()
 
  }

  onSubmit(){

    this.servInfoObj = {
      title: this.serviceInfo.controls['title'].value,
      services: this.elementArr
    }
    console.log(
      'Services INFO VALUE', this.servInfoObj
    );

    this.restServ.postServicesInfo(this.servInfoObj).subscribe(res=>{
      console.log('respoose',res);
      
    })

    // this.restServ.getAllServicesInfo().subscribe(data=>{
    //   console.log('response data', data);
      
    // })


  }

}
