import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import Swal from 'sweetalert2';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';

@Component({
  selector: 'app-add-chip-rules',
  templateUrl: './add-chip-rules.component.html',
  styleUrls: ['./add-chip-rules.component.scss']
})
export class AddChipRulesComponent implements OnInit {

    ruleChipsForm: FormGroup
    rules= []


    constructor(@Inject(MAT_DIALOG_DATA)private data, 
                private fb: FormBuilder,
                private restServ: RestoService) { 

      console.log('item id',data._id);
      console.log('item title',data.title);
      console.log('item rules', data.ruleandregulations)

      if(data.ruleandregulations!=undefined || data.ruleandregulations!=null){
        this.rules = data.ruleandregulations
      }

    

      
      this.ruleChipsForm = this.fb.group({
        chip: ['',Validators.required]    

      })
      
    }

    ngOnInit() {

      
    }

    add(){

      let a = this.ruleChipsForm.controls['chip'].value

      if(this.rules.includes(a)){
        Swal.fire('Already added: '+a)
      }
      else{
      this.rules.push(a)
      }
      console.log('rules info', this.rules);
      this.ruleChipsForm.controls['chip'].reset()
      

        
    }

    remove(a){
    
        this.rules.splice(this.rules.indexOf(a),1)

        console.log('rules Info Values', this.rules);
        
      
      
    }

    submit(){

      let obj = {
        title: this.data.title,
        ruleandregulations: this.rules
      }

      console.log('');
      
      this.restServ.putchipstoRules(this.data._id, obj ).subscribe(res=>{
        console.log('respo', res);
        
      })
    }

  }
