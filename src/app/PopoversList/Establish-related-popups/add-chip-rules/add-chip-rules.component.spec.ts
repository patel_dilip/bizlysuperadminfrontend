import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChipRulesComponent } from './add-chip-rules.component';

describe('AddChipRulesComponent', () => {
  let component: AddChipRulesComponent;
  let fixture: ComponentFixture<AddChipRulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChipRulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChipRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
