import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-ambience-chips',
  templateUrl: './add-ambience-chips.component.html',
  styleUrls: ['./add-ambience-chips.component.scss']
})
export class AddAmbienceChipsComponent implements OnInit {

  ambienceInfo : FormGroup
  elementArr= [];
  ambienceObj: any
  isSubmitted: boolean = true;
  titlesChk = []
  constructor(private fb: FormBuilder, private restServ: RestoService,@Inject(MAT_DIALOG_DATA)public data) { 
    this.getForm()

    console.log('data ',data)

    if(data!=undefined){
      data.forEach(element => {
        this.titlesChk.push(element.title)
      });
    }

   

    console.log('titles', this.titlesChk);
    
  }

  ngOnInit() {
  }

  getForm(){
    this.ambienceInfo = this.fb.group({
     title: ['', Validators.required],
     ambienceArr: ['', Validators.required] 
    })
  }
  onSave(){
   let ambience = this.ambienceInfo.controls['ambienceArr'].value
  
   this.elementArr.push(ambience)

   console.log('services array', this.elementArr);


  if(this.elementArr.length!=0){
    this.isSubmitted = false
  }

   this.ambienceInfo.controls['ambienceArr'].reset()
 
  }

  dupliChk(){

    let a  = this.ambienceInfo.controls['title'].value
    if(this.titlesChk.includes(a)){
      Swal.fire('Title Alreday Exists!!! '+a)
      this.ambienceInfo.controls['title'].reset()
    }
    console.log('hello duplicate check');
    
  }

  onSubmit(){

    this.ambienceObj = {
      title: this.ambienceInfo.controls['title'].value,
      ambiences: this.elementArr
    }
    console.log(
      'Services INFO VALUE', this.ambienceObj
    );

    this.restServ.postAmbienceInfo(this.ambienceObj).subscribe(res=>{
      console.log('respoose ambience',res);
      
    })

    // this.restServ.getAllMoreInfos().subscribe(data=>{
    //   console.log('response data', data);
      
    // })


  }

}
