import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAmbienceChipsComponent } from './add-ambience-chips.component';

describe('AddAmbienceChipsComponent', () => {
  let component: AddAmbienceChipsComponent;
  let fixture: ComponentFixture<AddAmbienceChipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAmbienceChipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAmbienceChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
