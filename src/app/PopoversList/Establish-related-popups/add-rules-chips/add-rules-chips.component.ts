import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { inject } from '@angular/core/testing';
import { MAT_DIALOG_DATA } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-rules-chips',
  templateUrl: './add-rules-chips.component.html',
  styleUrls: ['./add-rules-chips.component.scss']
})
export class AddRulesChipsComponent implements OnInit {

  rulesInfo : FormGroup
  elementArr= [];
  rulesObj: any
  isSubmitted: boolean = true;
  titleChk: any = [];
  constructor(private fb: FormBuilder, 
    private restServ: RestoService,
    @Inject(MAT_DIALOG_DATA)public data) { 
    this.getForm()

    if(data!=undefined){

      data.forEach(element => {
        this.titleChk.push(element.title)      
      });
    }


    console.log('titles array', this.titleChk);
    
  }

  ngOnInit() {
  }

  dupliChk(){

    console.log('hello ooo');
    
    
    let a = this.rulesInfo.controls['title'].value
    if(this.titleChk.includes(a)){
      Swal.fire('Already Exists!! ',a)
      this.rulesInfo.controls['title'].reset()
    }
  }

  getForm(){
    this.rulesInfo = this.fb.group({
     title: ['', Validators.required],
     rulesArr: ['',Validators.required] 
    })
  }
  onSave(){
   let rule = this.rulesInfo.controls['rulesArr'].value
  
   this.elementArr.push(rule)

   console.log('Rules array', this.elementArr);


   if(this.elementArr.length!=0){
    this.isSubmitted = false
  }


   this.rulesInfo.controls['rulesArr'].reset()

 
  }

  onSubmit(){

    this.rulesObj = {
      title: this.rulesInfo.controls['title'].value,
      ruleandregulations: this.elementArr
    }
    console.log(
      'MORE INFO VALUE', this.rulesObj
    );

    this.restServ.postRulesInfo(this.rulesObj).subscribe(res=>{
      console.log('respoose rules',res);
      
    })

    // this.restServ.getAllMoreInfos().subscribe(data=>{
    //   console.log('response data', data);
      
    // })


  }

}
