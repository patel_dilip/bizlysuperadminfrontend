import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRulesChipsComponent } from './add-rules-chips.component';

describe('AddRulesChipsComponent', () => {
  let component: AddRulesChipsComponent;
  let fixture: ComponentFixture<AddRulesChipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRulesChipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRulesChipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
