import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-sub-sub-cat',
  templateUrl: './add-sub-sub-cat.component.html',
  styleUrls: ['./add-sub-sub-cat.component.scss']
})
export class AddSubSubCatComponent implements OnInit {

  subsubForm: FormGroup
  constructor(@Inject(MAT_DIALOG_DATA) public data,
  private restoServ: RestoService,
  private fb: FormBuilder) {
    
    console.log('root', data['root']);
    console.log('child', data['child']);
    this.getForm()
   }

  ngOnInit() {
  }

  getForm(){
    this.subsubForm = this.fb.group({
     childChildCategoryName: ['', Validators.required],
     isFilterable: false,
     isSearchable: false,
     priority: ['']
  })
  }

  onSave(){
    let rid = this.data['root']
    let childid = this.data['child']
    var addsubsubObj = this.subsubForm.value

    this.restoServ.postSubSubCategory(rid,childid,addsubsubObj).subscribe(res=>{
      if(res['sucess']){
        Swal.fire('Sub Sub Child Added','','success')
      }
      else{
        Swal.fire('Failed to Add',"",'warning')
      }
    })
  }

}
