import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubSubCatComponent } from './add-sub-sub-cat.component';

describe('AddSubSubCatComponent', () => {
  let component: AddSubSubCatComponent;
  let fixture: ComponentFixture<AddSubSubCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubSubCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubSubCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
