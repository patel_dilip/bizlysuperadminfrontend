import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';

@Component({
  selector: 'app-add-subcat-menu',
  templateUrl: './add-subcat-menu.component.html',
  styleUrls: ['./add-subcat-menu.component.scss']
})
export class AddSubcatMenuComponent implements OnInit {
  MenuSubCatForm: FormGroup
  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data, private restServ: RestoService) { 
    this.getForm();
  }

  ngOnInit() {
  }

  getForm(){
    this.MenuSubCatForm = this.fb.group({
      childCategoryName: [''],
      isFilterable: false,
      isSearchable: false,
      priority: []
    })
  }

  onSave(){
    let rid = this.data['root']
    let cid = this.data['cat']

    var addSubCatObj = this.MenuSubCatForm.value

    this.restServ.putSubCategoryMenu(rid,cid,addSubCatObj).subscribe(res=>{
      console.log('response',res)
    })

  }

}
