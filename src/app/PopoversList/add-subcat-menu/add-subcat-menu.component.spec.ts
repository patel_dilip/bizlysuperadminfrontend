import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubcatMenuComponent } from './add-subcat-menu.component';

describe('AddSubcatMenuComponent', () => {
  let component: AddSubcatMenuComponent;
  let fixture: ComponentFixture<AddSubcatMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubcatMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubcatMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
