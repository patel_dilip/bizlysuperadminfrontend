import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-attr-cat',
  templateUrl: './attr-cat.component.html',
  styleUrls: ['./attr-cat.component.scss']
})
export class AttrCatComponent implements OnInit {

  attrForm: FormGroup
  CatArr: string[] = ['ABC','DEF','PQR', 'XYZ']
  PrioArr: string[] = ['1','2','3'];
  filteredOptions: Observable<string[]>;
  // filteredOptions1: Observable<string[]>;
  myControl = new FormControl();

  constructor(private fb: FormBuilder, private mainService: MainService) { }

  ngOnInit() {

    this.getForm()

    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.CatArr.filter(option => option.toLowerCase().includes(filterValue));
  }

  getForm(){
    this.attrForm = this.fb.group({
      attribute: [''],
      filterable: [''],
      searchable: [''],
      priority: ['']
    })
  }

  onSave(){

    this.attrForm.patchValue({
      attribute:  this.myControl.value
    })
    console.log(
      "this.attributes", this.attrForm.value
    );
    
    this.mainService.popAtrrCategData = this.attrForm.value
  }

}
