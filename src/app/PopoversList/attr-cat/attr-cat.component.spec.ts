import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttrCatComponent } from './attr-cat.component';

describe('AttrCatComponent', () => {
  let component: AttrCatComponent;
  let fixture: ComponentFixture<AttrCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttrCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttrCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
