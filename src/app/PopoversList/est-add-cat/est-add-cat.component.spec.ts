import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstAddCatComponent } from './est-add-cat.component';

describe('EstAddCatComponent', () => {
  let component: EstAddCatComponent;
  let fixture: ComponentFixture<EstAddCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstAddCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstAddCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
