import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-est-add-cat',
  templateUrl: './est-add-cat.component.html',
  styleUrls: ['./est-add-cat.component.scss']
})
export class EstAddCatComponent implements OnInit {

  userid;
  addCategory: FormGroup
  CatArr: string[] = ['Bar','Food court','Lounge', 'Cinema']
  PrioArr: string[] = ['1','2','3'];
  filteredOptions: Observable<string[]>;
  // filteredOptions1: Observable<string[]>;
  myControl = new FormControl();
  myControl1 = new FormControl();


  constructor(private fb: FormBuilder, private mainService: MainService,
    private serv: RestoService) { 
    // ************user id localstorage ******************
    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];
   
  }

  ngOnInit() {
    this.getForm();

    this.filteredOptions = this.myControl.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );

    
    // this.filteredOptions1 = this.myControl1.valueChanges
    // .pipe(
    //   startWith(''),
    //   map(value => this._filter1(value))
    // );




  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.CatArr.filter(option => option.toLowerCase().includes(filterValue));
  }
  
  // private _filter1(value: string): string[] {
  //   const filterValue1 = value.toLowerCase();

  //   return this.PrioArr.filter(option => option.toLowerCase().includes(filterValue1));
  // }

  getForm() {
    this.addCategory = this.fb.group({
      rootCategoryName: ["", Validators.required],
      isFilterable: [false],
      isSearchable: [false],
      priority:[""],
      userid: this.userid
  })

    
  }

  onSelect(v){
    
  }
  onSave() {

    // this.addCategory.patchValue({
    //   category_name: this.myControl.value
    // })
    console.table(this.addCategory.value);
    
    
    // this.mainService.popAddCategData = this.addCategory.value
    
    this.serv.postRootCategory(this.addCategory.value).subscribe(res=>{
      console.log('response', res);
      
      if(res['sucess']){
        Swal.fire('Root Added Succesfully','','success')
      }
      else{
        Swal.fire('Failed to Add')
      }
    })

  }

}
