import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FeedbackService } from 'src/app/_services/_feedbackServices/feedback.service';


@Component({
  selector: 'app-add-category-que-type',
  templateUrl: './add-category-que-type.component.html',
  styleUrls: ['./add-category-que-type.component.scss']
})
export class AddCategoryQueTypeComponent implements OnInit {

  CategoryQueTypeForm: FormGroup;
  categorytypeID: any

  constructor(private feedbackService: FeedbackService, private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data) { 
      console.log(data);
      this.categorytypeID= data
    }

  ngOnInit() {
    this.CategoryQueTypeForm = this.fb.group({
      
      categoryName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]]
    })
  }

    // *****************ADD VARIENT NAME HIT API*************************
    onAddCategoryType() {
      console.log('add category name', this.CategoryQueTypeForm.value);
  
      
    let addcatQueObject = {
      "rootCategoryName": this.CategoryQueTypeForm.controls['categoryName'].value,
      "categorytypeID": this.categorytypeID
    }
      
      console.log('questionTypeName data', addcatQueObject);

  
  
      this.feedbackService.putCategoryType(this.categorytypeID, addcatQueObject).subscribe(addCategoryResponse => {
        if (addCategoryResponse['success']) {
          Swal.fire('category is added', '', 'success')
        }
      })
  
    }
  }
  


