import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCategoryQueTypeComponent } from './add-category-que-type.component';

describe('AddCategoryQueTypeComponent', () => {
  let component: AddCategoryQueTypeComponent;
  let fixture: ComponentFixture<AddCategoryQueTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCategoryQueTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCategoryQueTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
