import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-vdo-album',
  templateUrl: './create-vdo-album.component.html',
  styleUrls: ['./create-vdo-album.component.scss']
})
export class CreateVdoAlbumComponent implements OnInit {

  addCategory: FormGroup
  barMoreInfo;
  pubMoreInfo;



  selectedAlbums= []
  commonTitles = []
  selectedEstablishments = []
  FilteredList=[]
  elementArr = [] 
  constructor(private fb: FormBuilder, private mainService: MainService, @Inject(MAT_DIALOG_DATA)public data) { 
    
    console.log('data in album', data);

    
  //  moreinfo
  data.moreinfo.forEach(ele => {     
    this.selectedEstablishments.push(ele)     
});

//   // services
data.service.forEach(ele => {     
  this.selectedEstablishments.push(ele)     
});

//   // ambiences
data.ambiences.forEach(ele => {     
  this.selectedEstablishments.push(ele)     
});


this.selectedEstablishments.forEach(element=>{
  if(!this.commonTitles.includes(element.title)){
    this.commonTitles.push(element.title)
  }
})

console.log("common titles",this.commonTitles);

// more info
  this.commonTitles.forEach(element=>{

    this.elementArr =[]
    this.selectedEstablishments.forEach(ele=>{
      if('moreinfo' in ele){
        if(element==ele.title){
          this.elementArr.push(ele.moreinfo)
        }
      }

      if('service' in ele){
        if(element==ele.title){
          this.elementArr.push(ele.service)
        }
      }

      if('ambiences' in ele){
        if(element==ele.title){
          this.elementArr.push(ele.ambiences)
        }
      }
     
    })
    
    this.FilteredList.push({title:element,value:this.elementArr})
  })


  console.log('selected Estalbishment', this.selectedEstablishments);
  console.log('Filtered List', this.FilteredList);


  if(this.mainService.selectedVideoAlbums!=undefined){
    this.selectedAlbums =this.mainService.selectedVideoAlbums;
   
  }

   
  //more info
  this.barMoreInfo = ['Live Entertainment', 'Live Sports', 'Indoor Seating', 'Service Cocktails',
  'Free Parking', 'Live Music', 'Sports', 'Outdoor Seating', 'Mocktails', 'Paid Parking', 'Live Karoake']


  //more info pubs

  this.pubMoreInfo = ['PUB', 'Entertainment','Discos','Drinks','Dance','Lights']


  // if(this.mainService.popVideoAddData!=undefined){
  //   this.selectedAlbums =this.mainService.popVideoAddData;
   
  // }
  }

  ngOnInit() { 
  

  }

  // CheckedList
  onCreateAlbum(obj,value){
    // if(obj.value.includes(value)){
    //  obj.value.splice(obj.value.indexOf(value),1)

    
      if(this.selectedAlbums.includes(value)){
        Swal.fire('Already Exist!!','','warning')
      }
      else{
        this.selectedAlbums.push(value)
      }
    // }
  }




  // ON REMOVE PUB MORE INFOS
  removePubs(s){
    if(this.selectedAlbums.includes(s)){
      this.selectedAlbums.splice(this.selectedAlbums.indexOf(s), 1)      
      this.pubMoreInfo.push(s);
    }
  
  }


 
  onAddAlbum(){
    this.mainService.popVideoAddData = this.selectedAlbums;
    console.log('the selected array', this.selectedAlbums);
    
  }
  

 

}
