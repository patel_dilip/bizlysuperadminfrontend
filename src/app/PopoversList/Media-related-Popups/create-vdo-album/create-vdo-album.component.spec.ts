import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateVdoAlbumComponent } from './create-vdo-album.component';

describe('CreateVdoAlbumComponent', () => {
  let component: CreateVdoAlbumComponent;
  let fixture: ComponentFixture<CreateVdoAlbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateVdoAlbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateVdoAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
