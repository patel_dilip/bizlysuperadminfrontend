import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { ɵKeyEventsPlugin } from '@angular/platform-browser';

@Component({
  selector: 'app-add-kitchen-popup',
  templateUrl: './add-kitchen-popup.component.html',
  styleUrls: ['./add-kitchen-popup.component.scss']
})
export class AddKitchenPopupComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA)public data) { 
   console.log('C ', this.data);
   

  }

  ngOnInit() {
  }

  onAddKitchen(){
    console.log('Kitchen', this.data.chairs.push("c"))
    console.log('Kitchen ', this.data)
  }

}
