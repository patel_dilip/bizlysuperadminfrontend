import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddKitchenPopupComponent } from './add-kitchen-popup.component';

describe('AddKitchenPopupComponent', () => {
  let component: AddKitchenPopupComponent;
  let fixture: ComponentFixture<AddKitchenPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddKitchenPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddKitchenPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
