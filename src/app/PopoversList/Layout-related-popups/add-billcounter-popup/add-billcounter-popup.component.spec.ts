import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBillcounterPopupComponent } from './add-billcounter-popup.component';

describe('AddBillcounterPopupComponent', () => {
  let component: AddBillcounterPopupComponent;
  let fixture: ComponentFixture<AddBillcounterPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBillcounterPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBillcounterPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
