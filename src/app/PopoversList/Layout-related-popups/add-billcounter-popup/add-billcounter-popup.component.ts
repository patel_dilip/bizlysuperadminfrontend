import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-add-billcounter-popup',
  templateUrl: './add-billcounter-popup.component.html',
  styleUrls: ['./add-billcounter-popup.component.scss']
})
export class AddBillcounterPopupComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA)public data) { 
    console.log('Bill Counter ', this.data);
    
   }
 
  ngOnInit() {
  }

}
