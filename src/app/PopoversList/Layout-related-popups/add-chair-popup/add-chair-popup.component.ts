import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-add-chair-popup',
  templateUrl: './add-chair-popup.component.html',
  styleUrls: ['./add-chair-popup.component.scss']
})
export class AddChairPopupComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA)public data) { 
    console.log('Chair ', this.data);
    
   }
 
  ngOnInit() {
  }

}
