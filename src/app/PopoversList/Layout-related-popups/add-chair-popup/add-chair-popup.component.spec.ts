import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChairPopupComponent } from './add-chair-popup.component';

describe('AddChairPopupComponent', () => {
  let component: AddChairPopupComponent;
  let fixture: ComponentFixture<AddChairPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChairPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChairPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
