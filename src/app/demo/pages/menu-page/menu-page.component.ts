import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-menu-page',
  templateUrl: './menu-page.component.html',
  styleUrls: ['./menu-page.component.scss']
})
export class MenuPageComponent implements OnInit {

  isLinear = false;
  foodFormGroup: FormGroup;
  liquorFormGroup: FormGroup;
  foodInventoryFormGroup: FormGroup;
  assestsInventoryFormGroup: FormGroup;

  foodList= ['Cuisine', 'Category','Menu', 'Add On', 'Varients']
  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.foodFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.liquorFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.foodInventoryFormGroup = this._formBuilder.group({
      foodCtrl: ['', Validators.required]
    });
    this.assestsInventoryFormGroup = this._formBuilder.group({
      assetsCtrl: ['', Validators.required]
    });
  }
}