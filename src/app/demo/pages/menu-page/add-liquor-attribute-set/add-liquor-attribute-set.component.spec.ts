import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLiquorAttributeSetComponent } from './add-liquor-attribute-set.component';

describe('AddLiquorAttributeSetComponent', () => {
  let component: AddLiquorAttributeSetComponent;
  let fixture: ComponentFixture<AddLiquorAttributeSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLiquorAttributeSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLiquorAttributeSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
