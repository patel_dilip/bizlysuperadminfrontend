import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import Swal from 'sweetalert2';
import { AddLiquorAttributeGroupComponent } from '../add-liquor-attribute-group/add-liquor-attribute-group.component';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

export interface DialogData {
  attribute_set_name: string;
  attributeType: string;
}
@Component({
  selector: 'app-add-liquor-attribute-set',
  templateUrl: './add-liquor-attribute-set.component.html',
  styleUrls: ['./add-liquor-attribute-set.component.scss']
})
export class AddLiquorAttributeSetComponent implements OnInit {
  addLiquorAttributeGroupForm: FormGroup
  panelOpenState = false;
  loginUserID: any
  allLiquorAttributeGroup: ['']
  attributeKey: any
  allAttributeGroup = [];
  equipmentAttributeJSONDATA = [];
  allUnassigned: any;
  group = []

  constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService,
    public dialogRef: MatDialogRef<AddLiquorAttributeSetComponent>, public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data) {
    //  this.getAllAttributeGroup()
    this.getAllAttribute();
    this.attributeKey = data
    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID)


  }
  drop(event: CdkDragDrop<any>) {

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);

    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      //  console.log("Index>>>>",event.currentIndex);

    }
    console.log(event);

    // console.log("c>>>>",this.todo);
    // console.log("B>>>>",this.done); 

  }
  getAllAttribute() {
    this.menuService.getAllLiquorAttribute().subscribe(data => {
      console.log("all attribute", data);
      data['data'].forEach(element => {
        element.attributeCode = "A" + element.attributeCode
      });
      let allAttributes = data['data'].filter(element => element.attributeType == "liquor")
      this.allUnassigned = allAttributes.filter(ele => ele['isAssign'] == false)

      var equipmentAttribute = data['data'].filter(element => element.attributeType == "equipment")
      equipmentAttribute.forEach(element => {
        var optionLabels = []
        element.responseType.options.forEach(ele => {
          optionLabels.push(ele.optionLable)
        });
        var d = new Date(element.updatedAt)
        var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
        let equipmentObj = {}
        equipmentObj = {
          "attributeCode": element.attributeCode,
          "attributeName": element.attributeName,
          "responseType": element.responseType.elementName,
          "fieldValue": element.responseType.fieldValue,
          "options": optionLabels.toString(),
          "isSearchable": element.isSearchable,
          "isFilterable": element.isFilterable,
          "isAssign": element.isAssign,
          "status": element.status,
          "addedBy": element.addedBy.userName,
          "updatedBy": element.updatedBy.userName,
          "updatedAt": date
        }
        this.equipmentAttributeJSONDATA.push(equipmentObj)
      });
      console.log("retail food attribute export", this.equipmentAttributeJSONDATA);
    })
  }

  ngOnInit() {
    this.addLiquorAttributeGroupForm = this.fb.group({
      attributeSetName: [''],
      assignGroups: [],
      userid: this.loginUserID,
      attributeType: this.attributeKey
    })
  }

  addAttributeGroup(): void {
    const dialogRef = this.dialog.open(AddLiquorAttributeGroupComponent, {
      data: { attributeType: 'liquor', attribute_set_name: this.addLiquorAttributeGroupForm.controls.attributeSetName.value },
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      // this.animal = result;
      this.getAllAttribute()
      console.log(">", this.addLiquorAttributeGroupForm.controls.attributeSetName.value);

      this.menuService.getLiquorAttributeGroupwithSetName(this.addLiquorAttributeGroupForm.controls.attributeSetName.value,'liquor').subscribe(data => {
        console.log("All liquor attribute group", data);
        if (data['sucess'] == true) {
          data['data'].forEach(element => {
            this.group.push(element)
          });

        }        // this.allLiquorAttributeGroup = data['data'].filter(ele=>ele.attributeType==this.attributeKey)
      })
      // this.getAllAttributeGroup()
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  // getAllAttributeGroup(){
  //   this.menuService.getLiquorAttributeGroup().subscribe(data => {
  //       console.log("All liquor attribute group", data);

  //       this.allAttributeGroup = data['data'].filter(ele=>ele.attributeType=="equipment")
  //     })
  //   }

  addNewSet() {
    console.log(this.addLiquorAttributeGroupForm.value);
    this.menuService.postAttriubteSet(this.addLiquorAttributeGroupForm.value).subscribe(res => {
      console.log(res);
      if (res['sucess']) {
        Swal.fire('Attribute Set Added Successfully', '', 'success')
        this.dialogRef.close();
      }
      else {
        Swal.fire('Fail to Add Attribute Set', 'Something Went Wrong', 'warning')
      }
    })
  }
}
