import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.scss']
})
export class ViewProductComponent implements OnInit {
  
  addedBy: any
  count: number;
  filteredArr = [];
  set = [];
  attributeData: Object;
  attribute_setlist = [];
  attribute=[];
  inputBoxPlaceholder: any;
  radiobutton: boolean;
  yesorNo: boolean;
  inputbox: boolean;
  checkbox: boolean;
  radioOptions: any;
  checkBoxOptions: any;
  filtered=[];
  // this.filtered=[];
  constructor(public dialogRef: MatDialogRef<ViewProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private menuService: AddMenuFoodsService) {
    console.log(data);
    this.filtered=data['attributeSet_response']

    let COBJ = {
      id: data['varient']['id'],
      string: this.data['varient']['varient'],
      attributeType: "liquor"
    }
    this.menuService.getattributeDatafromCategories(COBJ).subscribe(data => {
      this.count = 0
      console.log(data);
      this.attributeData = data
      this.set = this.attributeData['attributeSets']
      //  this.attribute_setlist=this.attributeData['attributeSets']
      this.attributeData['attributeSets'].forEach(element => {

        this.attribute_setlist.push(element['attributeSetName'])
      });
      console.log("list>", this.attribute_setlist);
     
      console.log();
      this.attributeData['groupAttribute'].forEach(element => {
        console.log(element['assignAttributes']);
        this.attribute = this.attribute.concat(element['assignAttributes'])
      });
      console.log(this.attribute, ">");
      this.filteredArr = this.attribute.reduce((acc, current) => {
        const x = acc.find(item => item['_id'] === current['_id']);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      console.log(this.filteredArr);
      // this.onClickResponse();
      let result=this.filteredArr.filter(value => this.filtered.includes(value))
      if(result.length>0){
        this.filtered.concat(result)
       
      }
     console.log( this.filteredArr.filter(value => this.filtered.includes(value)));
     
        
    })
   this.addedBy = data['addedBy'].userName
  //
  }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  onClickResponse(i) {

    // this.filteredArr.forEach((element, index) => {

    //  if(i==index-1){
    // console.log(this.this.filtered[i].responseType.elementName);
    if (this.filteredArr[i].responseType.elementName == "Input Box") {
      this.inputBoxPlaceholder =this.filtered[i].responseType.fieldValue
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true
      this.checkbox = false
    }
    if (this.filtered[i].responseType.elementName == "Yes / No") {

      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filtered[i].responseType.elementName == "Radio Button") {
      this.radioOptions =this.filtered[i].responseType.options
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filtered[i].responseType.elementName == "Check Box") {
      this.checkBoxOptions =this.filtered[i].responseType.options
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = true

    }
    // }

  }
}