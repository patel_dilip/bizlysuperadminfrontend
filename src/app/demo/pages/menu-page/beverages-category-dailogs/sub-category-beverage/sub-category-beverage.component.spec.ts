import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubCategoryBeverageComponent } from './sub-category-beverage.component';

describe('SubCategoryBeverageComponent', () => {
  let component: SubCategoryBeverageComponent;
  let fixture: ComponentFixture<SubCategoryBeverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubCategoryBeverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubCategoryBeverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
