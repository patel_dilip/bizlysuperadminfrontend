import { Component, OnInit, Inject } from '@angular/core';
  import { FormGroup, FormBuilder, Validators } from '@angular/forms';
  import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
  import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
  import Swal from 'sweetalert2';
@Component({
  selector: 'app-root-category-beverage',
  templateUrl: './root-category-beverage.component.html',
  styleUrls: ['./root-category-beverage.component.scss']
})
export class RootCategoryBeverageComponent implements OnInit {

    
    addCategoryForm: FormGroup;
    loginUserID:any
    beverageType:any
    constructor(public dialogRef: MatDialogRef<RootCategoryBeverageComponent>,
      private fb: FormBuilder, private menuService: AddMenuFoodsService,
      @Inject(MAT_DIALOG_DATA) public data) {
        console.log("beverage type",data);
        this.beverageType = data
         //get login user details
         console.log(">>>>>>>>>>>>>",localStorage.getItem('loginUser'));
         
      let loginUser = JSON.parse(localStorage.getItem('loginUser'))
      this.loginUserID = loginUser._id
      console.log(this.loginUserID)
       }
  
    ngOnInit() {
      this.addCategoryForm = this.fb.group({
        rootCategoryName:['', Validators.required],
        userid: this.loginUserID,
        beverageType: this.beverageType
      })
    }
    onNoClick(): void {
      this.dialogRef.close();
    }
    addCategory(){
      console.log(this.addCategoryForm.value);
      this.menuService.addRootCategoryBeverage(this.addCategoryForm.value).subscribe(res=>{
        console.log(res);
        if (res['sucess'] == true) {
    
          Swal.fire('Root Category Added Successfully', '', 'success')
          this.dialogRef.close();
        }
        else {
          Swal.fire('Failed to Add', 'Something went wrong', 'warning')
        }
      })
    }
  }
  