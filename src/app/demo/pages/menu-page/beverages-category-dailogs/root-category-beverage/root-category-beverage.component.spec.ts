import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootCategoryBeverageComponent } from './root-category-beverage.component';

describe('RootCategoryBeverageComponent', () => {
  let component: RootCategoryBeverageComponent;
  let fixture: ComponentFixture<RootCategoryBeverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RootCategoryBeverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootCategoryBeverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
