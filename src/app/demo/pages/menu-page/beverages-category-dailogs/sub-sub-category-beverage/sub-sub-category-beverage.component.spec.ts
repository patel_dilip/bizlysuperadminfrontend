import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSubCategoryBeverageComponent } from './sub-sub-category-beverage.component';

describe('SubSubCategoryBeverageComponent', () => {
  let component: SubSubCategoryBeverageComponent;
  let fixture: ComponentFixture<SubSubCategoryBeverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSubCategoryBeverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSubCategoryBeverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
