import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sub-sub-category-beverage',
  templateUrl: './sub-sub-category-beverage.component.html',
  styleUrls: ['./sub-sub-category-beverage.component.scss']
})
export class SubSubCategoryBeverageComponent implements OnInit {

  addCategoryForm: FormGroup;
  categoryIDs: any
  loginUserID: any
  constructor(public dialogRef: MatDialogRef<SubSubCategoryBeverageComponent>,
    private fb: FormBuilder, private menuService: AddMenuFoodsService,
    @Inject(MAT_DIALOG_DATA) public data) {
    console.log(data);
    this.categoryIDs = data

    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID)
  }

  ngOnInit() {
    this.addCategoryForm = this.fb.group({
      subSubCategoryName: ['', Validators.required],
      userid: this.loginUserID
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  addCategory() {
    console.log(this.categoryIDs);

    console.log(this.addCategoryForm.value);
    this.menuService.addSubSubCategoryBeverages(this.categoryIDs.rootCategoryID, this.categoryIDs.subCategoryID, this.addCategoryForm.value)
      .subscribe(res => {
        console.log(res);
        if (res['sucess'] == true) {

          Swal.fire('Sub Sub Category Added Successfully', '', 'success')
          this.dialogRef.close();
        }
        else {
          Swal.fire('Failed to Add', 'Something went wrong', 'warning')
        }
      })
  }
}
