import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSubCategoryTypeBeverageComponent } from './sub-sub-category-type-beverage.component';

describe('SubSubCategoryTypeBeverageComponent', () => {
  let component: SubSubCategoryTypeBeverageComponent;
  let fixture: ComponentFixture<SubSubCategoryTypeBeverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSubCategoryTypeBeverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSubCategoryTypeBeverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
