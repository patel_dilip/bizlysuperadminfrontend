import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sub-sub-category-type-beverage',
  templateUrl: './sub-sub-category-type-beverage.component.html',
  styleUrls: ['./sub-sub-category-type-beverage.component.scss']
})
export class SubSubCategoryTypeBeverageComponent implements OnInit {

  addCategoryForm: FormGroup;
    categoryID: any
    loginUserID: any
  
    constructor(public dialogRef: MatDialogRef<SubSubCategoryTypeBeverageComponent>,
      private fb: FormBuilder, private menuService: AddMenuFoodsService,
      @Inject(MAT_DIALOG_DATA) public data) {
      console.log(data);
      this.categoryID = data
  
         //get login user details
         let loginUser = JSON.parse(localStorage.getItem('loginUser'))
         this.loginUserID = loginUser._id
         console.log(this.loginUserID)
    }
  
    ngOnInit() {
      this.addCategoryForm = this.fb.group({
        subSubCategoryTypeName: ['', Validators.required],
        userid: this.loginUserID
      })
    }
    onNoClick(): void {
      this.dialogRef.close();
    }
    addCategory() {
      console.log(this.categoryID);
  
      console.log(this.addCategoryForm.value);
      this.menuService.addSubSubCategoryTypeBeverages(this.categoryID.rootCategoryID,this.categoryID.subsubCategortID, this.addCategoryForm.value).subscribe(res => {
        console.log(res);
        if (res['sucess'] == true) {
  
          Swal.fire('Sub Sub Category Type Added Successfully', '', 'success')
          this.dialogRef.close();
        }
        else {
          Swal.fire('Failed to Add', 'Something went wrong', 'warning')
        }
      })
    }
  }
  