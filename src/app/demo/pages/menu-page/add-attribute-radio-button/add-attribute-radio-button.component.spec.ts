import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAttributeRadioButtonComponent } from './add-attribute-radio-button.component';

describe('AddAttributeRadioButtonComponent', () => {
  let component: AddAttributeRadioButtonComponent;
  let fixture: ComponentFixture<AddAttributeRadioButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAttributeRadioButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAttributeRadioButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
