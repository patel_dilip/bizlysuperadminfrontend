import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';

const URL = environment.base_Url + 'attribute/uploadphoto'

@Component({
  selector: 'app-add-attribute-radio-button',
  templateUrl: './add-attribute-radio-button.component.html',
  styleUrls: ['./add-attribute-radio-button.component.scss']
})
export class AddAttributeRadioButtonComponent implements OnInit {
  addFormGroup: FormGroup
  uploader: FileUploader
  response: string;
  loading = false
  imageurl: any
  radioButtons:any
  constructor(public dialogRef: MatDialogRef<AddAttributeRadioButtonComponent>,
    private fb: FormBuilder) { 
    //   this.radioButtons = this.fb.group({
    //     optionLable: ['', Validators.required],
    //     associateImage: ['']
    //   })
    //   this.radioButtonForm.push(this.radioButtons)
     }

  ngOnInit() {

    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'attributeoption'

    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      // this.response = JSON.parse(response)
      // console.log('response', this.response);

      // this.imageurl = (this.response['cuisinePhotoUrls'][0])
      // console.log(this.imageurl);
      this.radioButtons.patchValue({
        associateImage: response
      })
      this.loading = false
    }


    this.addFormGroup = this.fb.group({
      radioButton: this.fb.array([]),
    });
  }

  // SETLOADING CALL FOR LOADER
  setLoading() {
    this.loading = true
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get radioButtonForm() {
    return this.addFormGroup.controls.radioButton as FormArray
  }

  addRadioButton() {
    this.radioButtons = this.fb.group({
      optionLable: ['', Validators.required],
      associateImage: ['']
    })
  
    this.radioButtonForm.push(this.radioButtons)
  }

  onSubmit() {
    console.log(this.addFormGroup.value);

  }
}
