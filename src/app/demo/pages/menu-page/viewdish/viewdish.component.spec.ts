import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewdishComponent } from './viewdish.component';

describe('ViewdishComponent', () => {
  let component: ViewdishComponent;
  let fixture: ComponentFixture<ViewdishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewdishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewdishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
