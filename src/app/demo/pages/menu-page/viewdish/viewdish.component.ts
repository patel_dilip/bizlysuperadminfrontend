import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-viewdish',
  templateUrl: './viewdish.component.html',
  styleUrls: ['./viewdish.component.scss']
})
export class ViewdishComponent implements OnInit {

  addedBy:any
  constructor( public dialogRef: MatDialogRef<ViewdishComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { 
      console.log(data);
  this.addedBy=data['addedBy'].userName
    }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}