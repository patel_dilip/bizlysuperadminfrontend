import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { Observable } from 'rxjs';
import { map, startWith, filter } from 'rxjs/operators';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { element } from 'protractor';

export interface country {
  name: string;
}

const URL = environment.base_Url + 'cuisine/uploadphotos'

@Component({
  selector: 'app-add-menu-cat',
  templateUrl: './add-menu-cat.component.html',
  styleUrls: ['./add-menu-cat.component.scss']
})


export class AddMenuCatComponent {
  addCuisinesForm: FormGroup
  cuisineCtrl: FormControl
  selectedAlbums = []
  cuisines = []
  tempCuisines = []
  uploader: FileUploader;
  response: string;
  multiResponse = []
  loading = false;
  selectedCuisinesIds = [];
  userid;
  // clicked:false
 
  filteredCountry: Observable<country[]>;
 
  allCountryStateCity = []
  allStatesCity = []
  allCities = []
  cusi= [];
  country_json=[];
  state=[];
  city=[];
  state_list: any;
  cusines_data=[];
  active_cuisines=[]

  constructor(private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService
    , private http: HttpClient, private router: Router) {
      this.country_state_city()
    this.addMenuFoodsService.getCuisines().subscribe(data => {
      this.cuisines = data['data']
      this.tempCuisines = data['data']
      console.log(data['data']);
      console.log(this.cuisines);
      this.active_cuisines=[]
      this.cuisines.forEach(element => {
      this.cusines_data.push({cuisineName:element['cuisineName'],_id:element['_id']})
        if(element.status==true){
          this.active_cuisines.push(element)
        }
      });

      console.log(this.cusines_data);
      this.cuisines=this.cusines_data
      


    })

    // FETCH USER ID FROM LOCAL STORAGE

    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];

    
  }
  checkCuisineExist(event){
    this.addMenuFoodsService.checkCuisineExist(event.target.value.toLowerCase()).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        // this.clicked=false
        Swal.fire("Cuisine Name Already Exist..!","", "warning")
        event.target.value=""
        this.addCuisinesForm.patchValue({
          cuisineName:''
        })
      }

    })
  }
country_state_city(){
  console.log("hey");

  this.addMenuFoodsService.country_state_city().subscribe(data => {
// console.log("============", data);

  console.log(data.country_json);
    this.country_json = data.country_json


    for (var i = 0; i < data.country_json.length - 1; i++)
    //console.log("=========================");
    
      console.log(data.country_json[i].name);

  })
}
changeCountry(count) {
  console.log(count);
  this.state = this.country_json.find(state => state.name == count).states
 
  this.state_list=this.state
  this.state=Object.keys(this.state)
 // console.log(Object.keys(this.state));
  // console.log(this.state['Maharashtra']);
  

  // console.log(this.villege);

}
changeState(count){
  this.city=this.state_list[count]
}
  ngOnInit() {

    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'cuisines'

    });



    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);



      this.response = JSON.parse(response)
      console.log('response', this.response['cuisinePhotoUrls']);

      this.multiResponse.push(this.response['cuisinePhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      // alert('File uploaded successfully');

      this.loading = false
    };


    this.addCuisines()

  }

  setLoading() {
    this.loading = true
  }



 

  //add cuisines form 
  addCuisines() {
    this.addCuisinesForm = this.fb.group({
      cuisineName: ['', [Validators.required, Validators.pattern('^[0-9a-zA-Z ]+$')]],
   //   originLocation: ['', Validators.required],
      combinationOfCusines: [''],
      country:['',Validators.required],
      state: ['',Validators.required],
      city: ['',Validators.required],
      images: []
    })
  }


  applyFilterCuisine(event) {
    this.cuisines = []
    this.cuisines = this.cusines_data
   // this.cuisines = this.tempCuisines['cuisineName']
    console.log(event.target.value);
    if (event.target.value == "") {
      this.cuisines = this.cusines_data
    } else {
      const val = event.target.value.trim().toLowerCase();
    const filter = this.cusines_data.filter(function (d) {
      //console.log(d);
      let result
      console.log("ele", d);        
      result = d['cuisineName'].toLowerCase().indexOf(val) !== -1 
  
      return result
    });

    //console.log(filter);
    this.cuisines = filter;
    }
     
   console.log(">>>", this.cuisines);
   
    
  }


  onCheckCuisine(s) {
    console.log(s);
    
    if (this.cuisines.includes(s)) {
      this.cuisines.splice(this.cuisines.indexOf(s), 1)
      console.log(this.cuisines);
      for( var i = 0; i < this.tempCuisines.length; i++){ 
        if ( this.tempCuisines[i]._id === s._id) {
          this.tempCuisines.splice(i, 1); 
        }
      }
      //console.log(this.tempCuisines);
      
      if (this.selectedAlbums.includes(s.cuisineName)) {
        alert("Already exists in selection")
      } else {
        this.selectedAlbums.push(s)
        this.selectedCuisinesIds.push(s._id)
      }
    }
    console.log(this.selectedAlbums);
    console.log(this.selectedCuisinesIds)

  }


  // ON REMOVE PUB MORE INFOS
  removePubs(s) {
    console.log(s);
    
    // full object of cuisines
    if (this.selectedAlbums.includes(s)) {
      this.selectedAlbums.splice(this.selectedAlbums.indexOf(s), 1)
      this.cuisines.push(s);
      console.log(this.cuisines);
    //  this.tempCuisines  = this.cuisines
      //  console.log(this.tempCuisines);
        
      console.log('selected Cuisines', this.selectedAlbums)
    }

    // only single key of cuisne as id
    if (this.selectedCuisinesIds.includes(s._id)) {
      this.selectedCuisinesIds.splice(this.selectedCuisinesIds.indexOf(s._id), 1)
      console.log('selected Cuisines', this.selectedCuisinesIds);


    }


  }


  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }

  urls = [];
  onSelectFile(event) {

    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          console.log('target result', event.target.result);
          this.urls.push(event.target.result);
          console.log('url', this.urls)
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }


  // **************************Final add cuisines************
  onAddCuisnes() {
    this.addCuisinesForm.patchValue({
      combinationOfCusines: this.selectedCuisinesIds,
      images: this.multiResponse
    })
    console.log(this.addCuisinesForm.value);

    // let addCuisinesObject = this.addCuisinesForm.value

    let addCuisinesObject = {
      "cuisineName": this.addCuisinesForm.controls['cuisineName'].value,
      "country":this.addCuisinesForm.controls['country'].value,
      "state":this.addCuisinesForm.controls['state'].value,
      "city":this.addCuisinesForm.controls['city'].value,
      "combinationOfCusines": this.selectedCuisinesIds,
      "images": this.multiResponse,
      "userid": this.userid
    }
    console.log(addCuisinesObject);
    
    this.addMenuFoodsService.postCuisines(addCuisinesObject).subscribe(addCuisinesResponse => {
      console.log('ADD Cusines resposne', addCuisinesResponse);
      if (addCuisinesResponse['sucess']) {
        Swal.fire('Cuisine Added Successfully', '', 'success')
        // **Resetting form and other values**
        // this.addCuisinesForm.reset()
        // this.selectedAlbums = [];
        // this.multiResponse = [];
        this.router.navigate(['/menu'])
      }
      else {
        Swal.fire('Failed to Add Cuisines', 'Something went wrong', 'warning')
      }

    })

  }




}



