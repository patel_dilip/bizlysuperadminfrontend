import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMenuCatComponent } from './add-menu-cat.component';

describe('AddMenuCatComponent', () => {
  let component: AddMenuCatComponent;
  let fixture: ComponentFixture<AddMenuCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMenuCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMenuCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
