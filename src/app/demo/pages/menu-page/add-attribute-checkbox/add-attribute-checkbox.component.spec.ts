import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAttributeCheckboxComponent } from './add-attribute-checkbox.component';

describe('AddAttributeCheckboxComponent', () => {
  let component: AddAttributeCheckboxComponent;
  let fixture: ComponentFixture<AddAttributeCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAttributeCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAttributeCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
