import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';

const URL = environment.base_Url + 'attribute/uploadphoto'

@Component({
  selector: 'app-add-attribute-checkbox',
  templateUrl: './add-attribute-checkbox.component.html',
  styleUrls: ['./add-attribute-checkbox.component.scss']
})
export class AddAttributeCheckboxComponent implements OnInit {
  addFormGroup: FormGroup
  uploader: FileUploader
  response: string;
  loading = false
  imageurl: any
  checkBoxButtons: any
  constructor(public dialogRef: MatDialogRef<AddAttributeCheckboxComponent>,
    private fb: FormBuilder) { }

  ngOnInit() {
    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'attributeoption'
    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      // this.response = JSON.parse(response)
      // console.log('response', this.response);

      // this.imageurl = (this.response['cuisinePhotoUrls'][0])
      // console.log(this.imageurl);
      this.checkBoxButtons.patchValue({
        associateImage: response
      })
      this.loading = false
    }


    this.addFormGroup = this.fb.group({
      checkBoxButton: this.fb.array([]),
    });
  }

  // SETLOADING CALL FOR LOADER
  setLoading() {
    this.loading = true
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get checkBoxButtonForm() {
    return this.addFormGroup.controls.checkBoxButton as FormArray
  }

  addcheckBoxButton() {
    this.checkBoxButtons = this.fb.group({
      optionLable: [''],
      associateImage: ['']
    })
    this.checkBoxButtonForm.push(this.checkBoxButtons)
  }

  onSubmit() {
    console.log(this.addFormGroup.value);

  }
}
