import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-beverage-product',
  templateUrl: './view-beverage-product.component.html',
  styleUrls: ['./view-beverage-product.component.scss']
})
export class ViewBeverageProductComponent implements OnInit {
 
  constructor(public dialogRef: MatDialogRef<ViewBeverageProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    console.log(data);

  }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
