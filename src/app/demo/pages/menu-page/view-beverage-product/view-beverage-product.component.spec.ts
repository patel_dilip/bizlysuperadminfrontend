import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBeverageProductComponent } from './view-beverage-product.component';

describe('ViewBeverageProductComponent', () => {
  let component: ViewBeverageProductComponent;
  let fixture: ComponentFixture<ViewBeverageProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBeverageProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBeverageProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
