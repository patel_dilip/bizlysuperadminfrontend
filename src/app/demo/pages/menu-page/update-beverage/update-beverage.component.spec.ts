import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBeverageComponent } from './update-beverage.component';

describe('UpdateBeverageComponent', () => {
  let component: UpdateBeverageComponent;
  let fixture: ComponentFixture<UpdateBeverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBeverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBeverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
