import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

export interface country {
  name: string;
}
const URL = environment.base_Url + 'cuisine/uploadphotos'

@Component({
  selector: 'app-update-cuisine',
  templateUrl: './update-cuisine.component.html',
  styleUrls: ['./update-cuisine.component.scss']
})
export class UpdateCuisineComponent implements OnInit {

  addCuisinesForm: FormGroup
  cuisineCtrl: FormControl
  selectedAlbums = []
  cuisines = []
  tempCuisines = []
  uploader: FileUploader;
  response: string;
  multiResponse = []
  loading = false;

  userid;

  cuisineID: any
  cuisineObject: any
  states: any;
  country_json: any ;
  state_list: any;
  cities: any;
  cusines_data=[];

  constructor(private menuService: AddMenuFoodsService, private router: Router, private fb: FormBuilder) {
    this.country_state_city()
      //get the cusine object for update
    this.cuisineObject = this.menuService.editObject

    console.log("edit obj", this.cuisineObject);
    if (this.cuisineObject == undefined) {
      this.router.navigateByUrl("/menu")
    }

   // this.changeCountry(this.cuisineObject.country)
    //get All Cuisines
    this.menuService.getCuisines().subscribe(data => {
      this.cuisines = data['data']

      for( var i = 0; i < this.cuisines.length; i++){
        for( var j = 0; j< this.cuisineObject.combinationOfCusines.length; j++){
          if ( this.cuisines[i]._id === this.cuisineObject.combinationOfCusines[j]._id) {
            this.cuisines.splice(i, 1); 
            this.cuisines.forEach(element => {
              if(element.status==true){
              this.cusines_data.push({cuisineName:element['cuisineName'],_id:element['_id']})
              }
              });
              console.log(this.cusines_data);
              this.cuisines=this.cusines_data
          }
        } 
      }
      console.log(this.cuisines);

    })

    // FETCH USER ID FROM LOCAL STORAGE
    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];
   
    //patch value to form
   
  }
  country_state_city(){
    return new Promise((resolve, reject) => {
    this.menuService.country_state_city().subscribe(data => {
    console.log(data.country_json);
      this.country_json = data.country_json
      resolve(this.country_json)
    })
    }).then(id => {
    this.country_json = id
    this.changeCountry(this.cuisineObject.country)
    // this.cuisineObject.country =''
    })
  }
  changeCountry(count) {
    this.states = this.country_json.find(state => state.name == count).states
    console.log(count);
    this.state_list=this.states
    this.states=Object.keys(this.states)
    let country = this.country_json.findIndex(x => x.name === "India")
console.log("selected country", country);

console.log("this is change country function", count);
this.changeState(this.cuisineObject.state)
// this.cuisineObject.state = ''
//this.changeState(this.userData.personal_details.state)

  }
  changeState(count){
    this.cities=this.state_list[count]
  }
  /*
changeCountry(count) {
let country = this.country_json.findIndex(x => x.name === "India")
console.log("selected country", country);

console.log("this is change country function", count);

this.state = this.country_json.find(state => state.name == count).states
console.log(count);
this.state_list = this.state
this.state = Object.keys(this.state)
//this.changeState(this.userData.personal_details.state)
console.log("this is country json value", this.country_json);
this.changeState(this.userData.personal_details.state)
this.userData.personal_details.state = ''
}
changeState(count) {
this.city = this.state_list[count]
console.log(this.PersonalDetailsForm.value);

}*/

  ngOnInit() {

    this.addCuisinesForm = this.fb.group({
      cuisineName: [this.cuisineObject.cuisineName, [Validators.required, Validators.pattern('^[0-9a-zA-Z ]+$')]],
     country:['',Validators.required],
     state: ['',Validators.required],
     city: ['',Validators.required],
      combinationOfCusines: [''],
      images: [],
      _id: [''],
      userid: [''],
      updatedBy:['']
    })
    this.addCuisines()
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'cuisines'

    });
  // this.addCuisinesForm.controls['cuisineName'].disable();


    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);



      this.response = JSON.parse(response)
      console.log('response', this.response['cuisinePhotoUrls']);

      this.multiResponse.push(this.response['cuisinePhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      // alert('File uploaded successfully');

      this.loading = false
    };




  }

  setLoading() {
    this.loading = true
  }

  

    //add cuisines form 
  addCuisines() {
    this.addCuisinesForm.patchValue({
      cuisineName: this.cuisineObject.cuisineName,
     
      country:this.cuisineObject.country,
      state: this.cuisineObject.state,
      city:this.cuisineObject.city,
      _id: this.cuisineObject._id
    })
   
    this.selectedAlbums = this.cuisineObject.combinationOfCusines
    this.multiResponse = this.cuisineObject.images
    this.cuisineID = this.cuisineObject._id
   
  }


  applyFilterCuisine(event) {
    this.cuisines = []
    this.cuisines = this.cusines_data
   // this.cuisines = this.tempCuisines['cuisineName']
    console.log(event.target.value);
    if (event.target.value == "") {
      this.cuisines = this.cusines_data
    } else {
      const val = event.target.value.trim().toLowerCase();
    const filter = this.cusines_data.filter(function (d) {
      //console.log(d);
      let result
      console.log("ele", d);        
      result = d['cuisineName'].toLowerCase().indexOf(val) !== -1 
  
      return result
    });

    //console.log(filter);
    this.cuisines = filter;
    }
     
   console.log(">>>", this.cuisines);
   
    
  }

  onCheckCuisine(s) {
    if (this.cuisines.includes(s)) {
      this.cuisines.splice(this.cuisines.indexOf(s), 1)
    
      for( var i = 0; i < this.tempCuisines.length; i++){ 
        if ( this.tempCuisines[i]._id === s._id) {
          this.tempCuisines.splice(i, 1); 
        }
      }
      if (this.selectedAlbums.includes(s)) {
        alert("Already exists in selection")
      } else {
        this.selectedAlbums.push(s)

      }
    }
    console.log(this.selectedAlbums);


  }


  // ON REMOVE PUB MORE INFOS
  removePubs(s) {
    // full object of cuisines
    if (this.selectedAlbums.includes(s)) {
      this.selectedAlbums.splice(this.selectedAlbums.indexOf(s), 1)
      this.cuisines.push(s);
    
      console.log('selected Cuisines', this.selectedAlbums)
    }
  }

  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }

  // **************************Final Update cuisines************
  onAddCuisnes() {
    console.log(this.selectedAlbums);

    let selectedCuisinesIds = []
    this.selectedAlbums.forEach(element => {
      selectedCuisinesIds.push(element._id)
    });

    this.addCuisinesForm.patchValue({
      combinationOfCusines: selectedCuisinesIds,
      images: this.multiResponse,
      userid: this.userid,
      updatedBy:this.userid,
    // cuisineName:this.cuisineName
      
    })
   // this.addCuisinesForm.controls['cuisineName']=this.cuisineObject.cuisineName
    console.log(this.addCuisinesForm.value);

    this.menuService.updateCuisines(this.cuisineID, this.addCuisinesForm.value).subscribe(res => {
      console.log(res);
      
      if (res['sucess']) {
            Swal.fire('Cuisine Updated Successfully', '', 'success')
            // **Resetting form and other values**
           // this.addCuisinesForm.reset()
            this.selectedAlbums = [];
            this.multiResponse = [];
            this.router.navigate(['/menu'])
          }
          else {
            Swal.fire('Failed to Update Cuisines', 'Something went wrong', 'warning')
          }
    })

  
  }




}




