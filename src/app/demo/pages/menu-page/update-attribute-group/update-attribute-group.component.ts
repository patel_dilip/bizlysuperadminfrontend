import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { element } from 'protractor';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-attribute-group',
  templateUrl: './update-attribute-group.component.html',
  styleUrls: ['./update-attribute-group.component.scss']
})
export class UpdateAttributeGroupComponent implements OnInit {

  addLiquorAttributeGroupForm: FormGroup
  loginUserID: any

  allAttributes: []
  attributeGroupKey: any
  attributesetObj: any;
  constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService,
    public dialogRef: MatDialogRef<UpdateAttributeGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    this.attributeGroupKey = data.attributeType
    console.log("attribute group key", data);

    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID)

    this.menuService.getAllLiquorAttribute().subscribe(data => {
      console.log(data['data']);
      this.allAttributes = data['data'].filter(ele => ele.attributeType == this.attributeGroupKey && ele['isAssign'] == false)

      console.log("filter attributes", this.allAttributes);


    })
  }

  ngOnInit() {
    this.addLiquorAttributeGroupForm = this.fb.group({
      groupName: [''],
      assignAttributes: [],
      userid: this.loginUserID,
      attributeType: this.attributeGroupKey
    })
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  addNewGroup() {
    console.log(this.addLiquorAttributeGroupForm.value);
    this.menuService.postAttributeGroup(this.addLiquorAttributeGroupForm.value).subscribe(res => {
      console.log(res);
      if (res['sucess']) {
        Swal.fire('Attribute Group Updated Successfully', '', 'success')
        this.dialogRef.close();
      }
      else {
        Swal.fire('Fail to Update Attribute Group', 'Something Went Wrong', 'warning')
      }
    })
  }
}
