import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVarientsComponent } from './add-varients.component';

describe('AddVarientsComponent', () => {
  let component: AddVarientsComponent;
  let fixture: ComponentFixture<AddVarientsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVarientsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVarientsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
