import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-add-on',
  templateUrl: './view-add-on.component.html',
  styleUrls: ['./view-add-on.component.scss']
})
export class ViewAddOnComponent implements OnInit {

  addedBy:any
  constructor( public dialogRef: MatDialogRef<ViewAddOnComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { 
      console.log(data);
    this.addedBy=data['addedBy'].userName
    let i=0
    data['adonType'].forEach(element => {
      if(element.adonName_type=='' && element.type==''){
        data['adonType'].splice(i,1);
      }
      i++
    });
    }
   

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}