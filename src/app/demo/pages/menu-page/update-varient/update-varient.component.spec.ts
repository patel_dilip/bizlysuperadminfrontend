import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateVarientComponent } from './update-varient.component';

describe('UpdateVarientComponent', () => {
  let component: UpdateVarientComponent;
  let fixture: ComponentFixture<UpdateVarientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateVarientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateVarientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
