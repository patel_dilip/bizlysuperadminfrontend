import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-category',
  templateUrl: './view-category.component.html',
  styleUrls: ['./view-category.component.scss']
})
export class ViewCategoryComponent implements OnInit {
  addedBy:any
  constructor( public dialogRef: MatDialogRef<ViewCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { 
      console.log(data);
      
     // this.addedBy=data['addedBy'].userName
    }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}

