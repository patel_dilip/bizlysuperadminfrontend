import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLiquorAttributeGroupComponent } from './add-liquor-attribute-group.component';

describe('AddLiquorAttributeGroupComponent', () => {
  let component: AddLiquorAttributeGroupComponent;
  let fixture: ComponentFixture<AddLiquorAttributeGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLiquorAttributeGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLiquorAttributeGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
