import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { element } from 'protractor';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-liquor-attribute-group',
  templateUrl: './add-liquor-attribute-group.component.html',
  styleUrls: ['./add-liquor-attribute-group.component.scss']
})
export class AddLiquorAttributeGroupComponent implements OnInit {
  addLiquorAttributeGroupForm: FormGroup
  loginUserID: any

  allAttributes: []
  attributeGroupKey:any
  clicked:boolean;
  constructor(private fb: FormBuilder,private route:Router, private menuService: AddMenuFoodsService,
    public dialogRef: MatDialogRef<AddLiquorAttributeGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {

     // this.attributeGroupKey = data
      console.log("attribute group key",data);
      
    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID)

    this.menuService.getAllLiquorAttribute().subscribe(data => {
      console.log(data['data']);
      this.allAttributes = data['data'].filter(ele => ele.attributeType==this.attributeGroupKey && ele['isAssign'] == false)

      console.log("filter attributes", this.allAttributes);


    })
  }

  ngOnInit() {
    this.addLiquorAttributeGroupForm = this.fb.group({
      groupName: ['', Validators.required],
      assignAttributes: [[]],
      userid: this.loginUserID,
      attributeType: this.data['attributeType'],
      attribute_set_name:this.data['attribute_set_name']
    })
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  addNewGroup() {
    // this.addLiquorAttributeGroupForm.controls.assignAttributes.value=[l]
    console.log(this.addLiquorAttributeGroupForm.value);
    this.menuService.postAttributeGroup(this.addLiquorAttributeGroupForm.value).subscribe(res => {
      console.log(">>>>>>>>>>>..",res);
      if (res['sucess']) {
        this.dialogRef.close();
        this.clicked=false
        Swal.fire('Attribute Group Added Successfully', '', 'success')
      //  this.dialogRef.close();
      this.route.navigate['/menu']
        
      }
      else {
        Swal.fire('Fail to Add Attribute Group', 'Something Went Wrong', 'warning')
      }
    })
  }
  checkGroupExist(event){
    this.menuService.checkAttributeGroupExists(event.target.value.toLowerCase(),this.data['attribute_set_name']).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        Swal.fire("Attribute Group Already Exist..!","", "warning")
        event.target.value=""
        this.addLiquorAttributeGroupForm.patchValue({
          groupName:''
        })
      }

    })
  }
}
