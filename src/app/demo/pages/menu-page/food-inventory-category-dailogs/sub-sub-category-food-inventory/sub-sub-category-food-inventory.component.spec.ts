import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSubCategoryFoodInventoryComponent } from './sub-sub-category-food-inventory.component';

describe('SubSubCategoryFoodInventoryComponent', () => {
  let component: SubSubCategoryFoodInventoryComponent;
  let fixture: ComponentFixture<SubSubCategoryFoodInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSubCategoryFoodInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSubCategoryFoodInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
