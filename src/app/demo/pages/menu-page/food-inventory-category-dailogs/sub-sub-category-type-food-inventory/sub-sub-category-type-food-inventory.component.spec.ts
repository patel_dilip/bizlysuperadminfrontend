import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSubCategoryTypeFoodInventoryComponent } from './sub-sub-category-type-food-inventory.component';

describe('SubSubCategoryTypeFoodInventoryComponent', () => {
  let component: SubSubCategoryTypeFoodInventoryComponent;
  let fixture: ComponentFixture<SubSubCategoryTypeFoodInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSubCategoryTypeFoodInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSubCategoryTypeFoodInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
