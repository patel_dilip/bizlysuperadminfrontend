import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubCategoryFoodInventoryComponent } from './sub-category-food-inventory.component';

describe('SubCategoryFoodInventoryComponent', () => {
  let component: SubCategoryFoodInventoryComponent;
  let fixture: ComponentFixture<SubCategoryFoodInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubCategoryFoodInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubCategoryFoodInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
