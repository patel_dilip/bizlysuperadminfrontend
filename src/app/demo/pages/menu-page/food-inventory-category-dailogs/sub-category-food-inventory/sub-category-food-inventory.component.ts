import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sub-category-food-inventory',
  templateUrl: './sub-category-food-inventory.component.html',
  styleUrls: ['./sub-category-food-inventory.component.scss']
})
export class SubCategoryFoodInventoryComponent implements OnInit {

  addCategoryForm: FormGroup;
  rootCategoryID: any
  loginUserID:any

  constructor(public dialogRef: MatDialogRef<SubCategoryFoodInventoryComponent>,
    private fb: FormBuilder, private menuService: AddMenuFoodsService,
    @Inject(MAT_DIALOG_DATA) public data) {
    console.log(data);
    this.rootCategoryID = data

        //get login user details
        let loginUser = JSON.parse(localStorage.getItem('loginUser'))
        this.loginUserID = loginUser._id
        console.log(this.loginUserID)
  }

  ngOnInit() {
    this.addCategoryForm = this.fb.group({
      subCategoryName: ['', Validators.required],
      userid: this.loginUserID
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  addCategory() {
    console.log(this.rootCategoryID);

    console.log(this.addCategoryForm.value);
    this.menuService.addSubCategoryFoodInventory(this.rootCategoryID, this.addCategoryForm.value).subscribe(res => {
      console.log(res);
      if (res['sucess'] == true) {

        Swal.fire('Sub Category Added Successfully', '', 'success')
        this.dialogRef.close();
      }
      else {
        Swal.fire('Failed to Add', 'Something went wrong', 'warning')
      }
    })
  }
}
