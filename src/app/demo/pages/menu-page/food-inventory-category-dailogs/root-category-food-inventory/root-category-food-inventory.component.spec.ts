import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootCategoryFoodInventoryComponent } from './root-category-food-inventory.component';

describe('RootCategoryFoodInventoryComponent', () => {
  let component: RootCategoryFoodInventoryComponent;
  let fixture: ComponentFixture<RootCategoryFoodInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RootCategoryFoodInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootCategoryFoodInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
