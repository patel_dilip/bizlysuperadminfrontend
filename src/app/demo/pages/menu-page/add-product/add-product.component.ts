import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { textChangeRangeIsUnchanged } from 'typescript';
import { MatDialog } from '@angular/material';
import { AddRootDrinkComponent } from 'src/app/PopoversList/add-root-drink/add-root-drink.component';
import { AddDrinkVarientComponent } from 'src/app/PopoversList/add-drink-varient/add-drink-varient.component';
import { AddDrinkSubvarientComponent } from 'src/app/PopoversList/add-drink-subvarient/add-drink-subvarient.component';
import { AddSubSubVarientComponent } from 'src/app/PopoversList/add-sub-sub-varient/add-sub-sub-varient.component';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { Router } from '@angular/router';
import { variable } from '@angular/compiler/src/output/output_ast';
import { resolve } from 'dns';

const URL = environment.base_Url + 'product/uploadphotos'

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  countries: string[] = ['India', 'America', 'England'];
  DrinkTypes: string[] = ['Wine', 'Beer', 'Vodka'];
  liquorVarients: string[] = ['Red Wine', 'Wine']
  addProductForm: FormGroup
  allDrinks: any
  allBrands=[]
  allLiquorAttributesSet: any
  disableBrandDropdown = true
  assignAttributes=[]
  uploader: FileUploader;
  response: string
  multiResponse = []
  loading = false;
  loginUserID: any
  attributeData:any;
  varient_name:{};
  count: number;
  countbrand: number;
  attribute=[];
  radiobutton = false
  yesorNo = false
  inputbox = false
  checkbox = false
inputBoxPlaceholder:any
radioOptions:any
checkBoxOptions: any
  filteredArr= [];
  set=[];
  attribute_setlist=[];
  varientType: any;
  root: any;
  subVarient: any;
  subsubVarient: any;

  constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService,
     private matDialog: MatDialog, private router: Router) {


    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);

    this.getAllDrinks()
    this.getAllLiquorAttributeSet()
    // this.getAllBrand()
  }

  ngOnInit() {
    this.addProduct()

    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'product'

    });
    //file upload response with url
    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);


      this.response = JSON.parse(response)
      console.log('response', this.response['adonPhotoUrls']);

      this.multiResponse.push(this.response['adonPhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      this.loading = false
    };
  }


  setLoading() {
    this.loading = true
  }

  getAllDrinks() {
    this.menuService.getAllRootDrinks().subscribe(data => {
      console.log(data);
      this.allDrinks = data['data']
      console.log(this.allDrinks);

    })
  }

  //get all liquor attribute set
  getAllLiquorAttributeSet() {
    this.menuService.getAllLiquorAttributeSets().subscribe(data => {
      console.log("All liquor attribute sets", data);
      this.allLiquorAttributesSet = data['data'].filter(ele=>ele.attributeType=="liquor")
    })
  }


  // getAllBrand() {
  //   this.menuService.getAllBrand().subscribe(data => {
  //     console.log(data);
  //     this.allBrands = data['data']
  //     console.log(this.allBrands);

  //   })
  // }

  addProduct() {
    this.addProductForm = this.fb.group({
      productName: ['', [Validators.required, Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      brand: ['', Validators.required],
      varient:[{}, Validators.required],
      productImages: [''],
      userid: this.loginUserID,
      attribute_set: [],
      root: {},
      subVarient: {},
      subsubVarient: {},
      varientType: {},
      attributeSet_response:[]
      
    })
  }




  

  //open tree node and close
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
        
      });
    }
  }

  // **********open root drink dialog**************
  
  openRootDialog() {
    const dialogRef = this.matDialog.open(AddRootDrinkComponent, {
      width: '460px',
      disableClose: true,
      
      // height: '200px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log('After dialog closed');
      setTimeout(()=>{    //<<<---    using ()=> syntax
        this.getAllDrinks()
   }, 2000);
    })
  }

  // ********OPEN VARIENT DRINK DIALOG***************

  openVarientDialog(rootDrinkID) {
    const dialogRef = this.matDialog.open(AddDrinkVarientComponent, {
      width: '460px',
      disableClose: true,
      data: rootDrinkID,
      // height: '300px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'after closing varient drink pop'
      );
      setTimeout(()=>{    //<<<---    using ()=> syntax
        this.getAllDrinks()
   }, 2000);
    })

  }

  //************open sub varient drink dialog box**************

  openSubVarientDialog(rootDrinkID, liquorVarient) {
    const dialogRef = this.matDialog.open(AddDrinkSubvarientComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootDrinkID": rootDrinkID, "liquorVarient": liquorVarient },
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(()=>{    //<<<---    using ()=> syntax
        this.getAllDrinks()
   }, 2000);
    })
  }

  // **************OPEN SUB SUB VARIENT DIALOG************
  openSubSubVarient(rootDrinkID, liquorVarient, liquorSubVarient) {
    const dialogRef = this.matDialog.open(AddSubSubVarientComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootDrinkID": rootDrinkID, "liquorVarient": liquorVarient, "liquorSubVarient": liquorSubVarient }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(()=>{    //<<<---    using ()=> syntax
        this.getAllDrinks()
   }, 2000);
    })
  }


  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }



  onAddAlbum() {
    console.log("varient:", this.varient_name);
    
    this.addProductForm.patchValue({
      productImages: this.multiResponse,
      varient:this.varient_name,
     root :this.root,
     subVarient :this.subVarient,
     subsubVarient :this.subsubVarient,
      varientType:this.varientType,
      attributeSet_response: this.filteredArr,

    })
    console.log("value",this.addProductForm.value);
    console.log("aaaa>>>",this.filteredArr);
    

    this.menuService.addLiquorProduct(this.addProductForm.value).subscribe(res=>{
      console.log(res);
      if (res['success'] == true) {

        Swal.fire('Product Added Successfully', '', 'success')
        this.router.navigate(['/menu'])
      }
      else {
        Swal.fire('Failed to Add Product', 'Something went wrong', 'warning')
      }
    })
  }
  checkLiquorProductExist(event){
    this.menuService.checkLiquorProductExist(event.target.value.toLowerCase()).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        Swal.fire("Product Name Already Exist..!","", "warning")
        event.target.value=""
        this.addProductForm.patchValue({
          productName:''
        })
      }

    })
  }
getRadiovalue(id, name){
    // fa
    this.allBrands=[]
    this.filteredArr=[]
   this.set=[]
   this.attribute_setlist=[]
    this.varient_name={varient:name, id:id};
    console.log(id,">>", this.varient_name);

    let categoryOBJ = {
       id: id,
      string:name
  }
  let obj=
      {
        id: id,
    string:name,
  attributeType:"liquor"
 
  }
  console.log("hi");
  console.log(categoryOBJ)
  // await new Promise(resolve => {
  this.menuService.getBrandsByCategory(categoryOBJ).subscribe(data => {
    this.countbrand=0

    console.log(data);
    this.allBrands = data['data']
                
      this.disableBrandDropdown = false
         this.allBrands = data['data']    
  })
// console.log(";en",this.allBrands)
  
    this.menuService.getattributeDatafromCategories(obj).subscribe(data => {
      this.count=0
    console.log(data);
    this.attributeData = data
   this.set= this.attributeData['attributeSets']
  //  this.attribute_setlist=this.attributeData['attributeSets']
  this.attributeData['attributeSets'].forEach(element => {
    
    this.attribute_setlist.push(element['attributeSetName'])
  });
  console.log("list>",this.attribute_setlist);
  this.addProductForm.patchValue({
    attribute_set:this.attribute_setlist
  })
  console.log();
  this.attributeData['groupAttribute'].forEach(element => {
    console.log(element ['assignAttributes']);
    this.attribute=this.attribute.concat(element ['assignAttributes'])
  }); 
  console.log(this.attribute,">");
  this.filteredArr = this.attribute.reduce((acc, current) => {
    const x = acc.find(item => item['_id'] === current['_id']);
    if (!x) {
      return acc.concat([current]);
    } else {
      return acc;
    }
  }, []);
  console.log(this.filteredArr);
  // this.onClickResponse();
  
  })  
  
// });
setTimeout(()=> {
              
if(this.allBrands.length==0 && this.set.length==0){
    Swal.fire('Liquor brand and Attribute not available', '', 'warning')
  }
  else if(this.set.length==0){      
    Swal.fire('Attribute not available', '', 'warning')
  } else if(this.allBrands.length==0){
    Swal.fire('Brands not available', '', 'warning')
  
  }
}, 1000);

  }
  onClickResponse(i) {
    
    // this.filteredArr.forEach((element, index) => {
      
  //  if(i==index-1){
  //  console.log(this.filteredArr[i].responseType.elementName);
    if (this.filteredArr[i].responseType.elementName == "Input Box") {
      this.inputBoxPlaceholder=this.filteredArr[i].responseType.fieldValue
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true
      this.checkbox = false
    }
    if (this.filteredArr[i].responseType.elementName == "Yes / No") {
     
      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filteredArr[i].responseType.elementName == "Radio Button") {
      this.radioOptions = this.filteredArr[i].responseType.options
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filteredArr[i].responseType.elementName == "Check Box") {
      this.checkBoxOptions = this.filteredArr[i].responseType.options
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = true

    }
    // }

  }
  getRadio(r?:any,sub?:any,subsub?:any, type?:any){
console.log(r,">",sub,">",subsub,">",type);
this.root={root:r.rootDrinkType, id:r._id}
      this.subVarient={subVarient:sub.liquorVarientName, id:sub._id}
      this.subsubVarient={subsubVarient:subsub.liquorSubVarientName, id:subsub._id}
      this.varientType={varientType:type.liquorSubSubVarientTypeName, id:type._id}
      console.log(this.root,">",this.subVarient,">",this.subsubVarient,">",this.varientType);
  }
  assignSpecificationByCheckbox = (object) => {
    object.value = object.responseType.options;
  }

}
