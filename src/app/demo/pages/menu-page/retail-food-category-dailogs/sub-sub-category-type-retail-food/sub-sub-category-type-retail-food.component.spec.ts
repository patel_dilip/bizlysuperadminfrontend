import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSubCategoryTypeRetailFoodComponent } from './sub-sub-category-type-retail-food.component';

describe('SubSubCategoryTypeRetailFoodComponent', () => {
  let component: SubSubCategoryTypeRetailFoodComponent;
  let fixture: ComponentFixture<SubSubCategoryTypeRetailFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSubCategoryTypeRetailFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSubCategoryTypeRetailFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
