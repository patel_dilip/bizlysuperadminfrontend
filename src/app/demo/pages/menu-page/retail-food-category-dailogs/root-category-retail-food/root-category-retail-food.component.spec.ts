import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootCategoryRetailFoodComponent } from './root-category-retail-food.component';

describe('RootCategoryRetailFoodComponent', () => {
  let component: RootCategoryRetailFoodComponent;
  let fixture: ComponentFixture<RootCategoryRetailFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RootCategoryRetailFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootCategoryRetailFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
