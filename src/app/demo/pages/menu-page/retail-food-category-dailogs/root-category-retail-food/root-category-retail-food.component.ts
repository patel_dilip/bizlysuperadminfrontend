import { Component, OnInit } from '@angular/core';
  import { FormGroup, FormBuilder, Validators } from '@angular/forms';
  import { MatDialogRef } from '@angular/material';
  import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
  import Swal from 'sweetalert2';

@Component({
  selector: 'app-root-category-retail-food',
  templateUrl: './root-category-retail-food.component.html',
  styleUrls: ['./root-category-retail-food.component.scss']
})
export class RootCategoryRetailFoodComponent implements OnInit {

  
    addCategoryForm: FormGroup;
    loginUserID:any
    constructor(public dialogRef: MatDialogRef<RootCategoryRetailFoodComponent>,
      private fb: FormBuilder, private menuService: AddMenuFoodsService) {
         //get login user details
      let loginUser = JSON.parse(localStorage.getItem('loginUser'))
      this.loginUserID = loginUser._id
      console.log(this.loginUserID)
       }
  
    ngOnInit() {
      this.addCategoryForm = this.fb.group({
        rootCategoryName:['', Validators.required],
        userid: this.loginUserID
      })
    }
    onNoClick(): void {
      this.dialogRef.close();
    }
    addCategory(){
      console.log(this.addCategoryForm.value);
      this.menuService.addRootCategoryRetailFood(this.addCategoryForm.value).subscribe(res=>{
        console.log(res);
        if (res['sucess'] == true) {
    
          Swal.fire('Root Category Added Successfully', '', 'success')
          this.dialogRef.close();
        }
        else {
          Swal.fire('Failed to Add', 'Something went wrong', 'warning')
        }
      })
    }
  }
  