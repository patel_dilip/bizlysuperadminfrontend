import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSubCategoryRetailFoodComponent } from './sub-sub-category-retail-food.component';

describe('SubSubCategoryRetailFoodComponent', () => {
  let component: SubSubCategoryRetailFoodComponent;
  let fixture: ComponentFixture<SubSubCategoryRetailFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSubCategoryRetailFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSubCategoryRetailFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
