import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubCategoryRetailFoodComponent } from './sub-category-retail-food.component';

describe('SubCategoryRetailFoodComponent', () => {
  let component: SubCategoryRetailFoodComponent;
  let fixture: ComponentFixture<SubCategoryRetailFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubCategoryRetailFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubCategoryRetailFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
