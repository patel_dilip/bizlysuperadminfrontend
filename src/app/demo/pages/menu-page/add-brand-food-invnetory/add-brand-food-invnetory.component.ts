import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-brand-food-invnetory',
  templateUrl: './add-brand-food-invnetory.component.html',
  styleUrls: ['./add-brand-food-invnetory.component.scss']
})
export class AddBrandFoodInvnetoryComponent implements OnInit {
  
  brandForm: FormGroup
  loginUserID: any
  constructor(public dialogRef: MatDialogRef<AddBrandFoodInvnetoryComponent>, private fb: FormBuilder,
    private menuService: AddMenuFoodsService) {
      //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);

  }

  ngOnInit() {
    this.brandForm = this.fb.group({
      BrandName: ['', Validators.required],
      userid: this.loginUserID
    })
  }

  // add final Brand
  addBrand() {
  
    console.log(this.brandForm.value);

    this.menuService.addBrandFoodInventory(this.brandForm.value).subscribe(res => {
      console.log(res);
      if (res['sucess'] == true) {
        Swal.fire('Brand Added Successfully', '', 'success')
        this.dialogRef.close();
      }
      else {
        Swal.fire('Inventory brand already exists', '', 'warning')
        this.brandForm.reset()
      }

    })

  }

  

  onNoClick(): void {
    this.dialogRef.close();
  }
}
