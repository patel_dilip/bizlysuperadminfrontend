import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBrandFoodInvnetoryComponent } from './add-brand-food-invnetory.component';

describe('AddBrandFoodInvnetoryComponent', () => {
  let component: AddBrandFoodInvnetoryComponent;
  let fixture: ComponentFixture<AddBrandFoodInvnetoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBrandFoodInvnetoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBrandFoodInvnetoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
