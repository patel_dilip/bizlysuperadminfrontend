
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { textChangeRangeIsUnchanged } from 'typescript';
import { MatDialog } from '@angular/material';
import { AddRootDrinkComponent } from 'src/app/PopoversList/add-root-drink/add-root-drink.component';
import { AddDrinkVarientComponent } from 'src/app/PopoversList/add-drink-varient/add-drink-varient.component';
import { AddDrinkSubvarientComponent } from 'src/app/PopoversList/add-drink-subvarient/add-drink-subvarient.component';
import { AddSubSubVarientComponent } from 'src/app/PopoversList/add-sub-sub-varient/add-sub-sub-varient.component';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { Router } from '@angular/router';
import { element } from 'protractor';
import { filter } from 'rxjs/operators';

const URL = environment.base_Url + 'product/uploadphotos'


@Component({
  selector: 'app-update-product-liquor',
  templateUrl: './update-product-liquor.component.html',
  styleUrls: ['./update-product-liquor.component.scss']
})
export class UpdateProductLiquorComponent implements OnInit {

  addProductForm: FormGroup
  allDrinks: any
  allBrands = []
  allLiquorAttributesSet: any
  disableBrandDropdown = true
  varient: any;
  uploader: FileUploader;
  response: string
  multiResponse = []
  loading = false;
  loginUserID: any
  productObject: any
  productID: any
  varient_name: {};
  count: number;
  countbrand: number;
  attribute = [];
  radiobutton = false
  yesorNo = false
  inputbox = false
  checkbox = false
  inputBoxPlaceholder: any
  radioOptions: any
  checkBoxOptions: any
  filteredArr = [];
  set = [];
  attributeData: Object;
  attribute_setlist = [];
  root: any;
  subVarient: any;
  subsubVarient: any;
  varientType: any;
  filtered=[];
  rootcategory: any;
  subcategory: any;
  subsubcategory: any;
  typecategory: any;
  constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService,
    private matDialog: MatDialog, private router: Router) {
     
    //get the product object for update

    this.productObject = this.menuService.editObject
    console.log(this.productObject);

    if (this.productObject == undefined) {
      this.router.navigateByUrl("/menu")
    }
    if(this.productObject['root']!=undefined){
      this.rootcategory=this.productObject['root']['root']
      }
      if(this.productObject['subVarient']!=undefined){
      this.subcategory=this.productObject['subVarient']['subVarient'];
      }
      if(this.productObject['subsubVarient']!=undefined){
    this.subsubcategory=this.productObject['subsubVarient']['subsubVarient'];
      }
      if(this.productObject['varientType']!=undefined){
    this.typecategory=this.productObject['varientType']['varientType'];
      }
    let OBJ = {
      id: this.productObject['varient']['id'],
      string: this.productObject['varient']['varient']
    }
    let COBJ = {
      id: this.productObject['varient']['id'],
      string: this.productObject['varient']['varient'],
      attributeType: "liquor"
    }

    console.log(OBJ);
    this.root = this.productObject['root']
    this.subVarient = this.productObject['subVarient']
    this.subsubVarient = this.productObject['subsubVarient']
    this.varientType = this.productObject['varientType']
    this.menuService.getBrandsByCategory(OBJ).subscribe(data => {
      this.countbrand = 0
      console.log("d>>>", data);
      this.allBrands = data['data']
      this.disableBrandDropdown = false
      this.allBrands = data['data']
    })
    this.menuService.getattributeDatafromCategories(COBJ).subscribe(data => {
      this.count = 0
      console.log(data);
      this.attributeData = data
      this.set = this.attributeData['attributeSets']
      //  this.attribute_setlist=this.attributeData['attributeSets']
      this.attributeData['attributeSets'].forEach(element => {

        this.attribute_setlist.push(element['attributeSetName'])
      });
      console.log("list>", this.attribute_setlist);
      this.addProductForm.patchValue({
        attribute_set: this.attribute_setlist
      })
      console.log();
      this.attributeData['groupAttribute'].forEach(element => {
        console.log(element['assignAttributes']);
        this.attribute = this.attribute.concat(element['assignAttributes'])
      });
      console.log(this.attribute, ">");
      this.filteredArr = this.attribute.reduce((acc, current) => {
        const x = acc.find(item => item['_id'] === current['_id']);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      console.log(this.filteredArr);
    this.filtered=this.productObject['attributeSet_response']
 
    //  this.productObject['attributeSet_response'].forEach(element => {
        let result=this.filteredArr.filter(value => this.filtered.includes(value))
        console.log(result);
        
        if(result.length>0){
          this.filtered.concat(result)
         
        }
        console.log("replece array:=", this.filtered);

      });
      
    // })


    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);


    this.getAllDrinks()
    this.getAllLiquorAttributeSet()
    // this.getAllBrand()

    this.addProduct()
    console.log("API call");
    this.menuService.getBrandsByCategory(OBJ).subscribe(data => {
      this.countbrand = 0
      console.log("d>>>", data);
      this.allBrands = data['data']
      this.disableBrandDropdown = false
      this.allBrands = data['data']
    })

    this.addProductForm.patchValue({
      productName: this.productObject.productName,
      brand: this.productObject.brand['_id'],
      varient: this.productObject.varient,
      attribute_set: this.productObject['attribute_set'],
      root: this.productObject['root'],
      subVarient: this.productObject['subVarient'],
      subsubVarient: this.productObject['subsubVarient'],
      varientType: this.productObject['varientType'],
      attributeSet_response:this.productObject['attributeSet_response']
    })
    this.multiResponse = this.productObject.productImages
    this.productID = this.productObject._id
    this.varient = this.productObject['varient']
    console.log("varient<<", this.varient);


  }

  ngOnInit() {
    
    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'product'

    });
    //file upload response with url
    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);


      this.response = JSON.parse(response)
      console.log('response', this.response['adonPhotoUrls']);

      this.multiResponse.push(this.response['adonPhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      this.loading = false
    };
  }


  setLoading() {
    this.loading = true
  }

  getAllDrinks() {
    this.menuService.getAllRootDrinks().subscribe(data => {
      console.log(data);
      this.allDrinks = data['data']
      console.log(this.allDrinks);

    })
  }

  //get all liquor attribute set
  getAllLiquorAttributeSet() {
    this.menuService.getAllLiquorAttributeSets().subscribe(data => {
      console.log("All liquor attribute sets", data);
      this.allLiquorAttributesSet = data['data'].filter(ele => ele.attributeType == "liquor")
    })
  }


  getAllBrand() {
    this.menuService.getAllBrand().subscribe(data => {
      console.log(data);
      this.allBrands = data['data']
      console.log(this.allBrands);

    })
    if (this.allBrands.length == 0) {
      Swal.fire("Liquor brand not available", "", "warning")
    }
  }

  addProduct() {
    this.addProductForm = this.fb.group({
      productName: ['', [Validators.required, Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      brand: ['', Validators.required],
      varient: [''],
      productImages: [''],
      userid: this.loginUserID,
      attribute_set: [],
      root: {},
      subVarient: {},
      subsubVarient: {},
      varientType: {},
      attributeSet_response:[]
    })
  }

  //open tree node and close
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  // **********open root drink dialog**************

  openRootDialog() {
    const dialogRef = this.matDialog.open(AddRootDrinkComponent, {
      width: '460px',
      disableClose: true,

      // height: '200px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log('After dialog closed');
      this.getAllDrinks()
    })
  }

  // ********OPEN VARIENT DRINK DIALOG***************

  openVarientDialog(rootDrinkID) {
    const dialogRef = this.matDialog.open(AddDrinkVarientComponent, {
      width: '460px',
      disableClose: true,
      data: rootDrinkID,
      // height: '300px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'after closing varient drink pop'
      );
      this.getAllDrinks()
    })

  }

  //************open sub varient drink dialog box**************

  openSubVarientDialog(rootDrinkID, liquorVarient) {
    const dialogRef = this.matDialog.open(AddDrinkSubvarientComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootDrinkID": rootDrinkID, "liquorVarient": liquorVarient },
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllDrinks()
    })
  }

  // **************OPEN SUB SUB VARIENT DIALOG************
  openSubSubVarient(rootDrinkID, liquorVarient, liquorSubVarient) {
    const dialogRef = this.matDialog.open(AddSubSubVarientComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootDrinkID": rootDrinkID, "liquorVarient": liquorVarient, "liquorSubVarient": liquorSubVarient }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllDrinks()
    })
  }


  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }

  onAddAlbum() {
    // const attribute = this.allLiquorAttributesSet.find(element => element.attributeSetName==this.addProductForm.controls.attribute.value);
    // console.log(attribute);
    // const brand = this.allBrands.find(element => element.brandName == this.addProductForm.controls.brand.value)
    // console.log(brand);

    this.addProductForm.patchValue({
      // attribute:attribute._id,
      root :this.root,
      subVarient :this.subVarient,
      subsubVarient :this.subsubVarient,
       varientType:this.varientType,
      brand: this.addProductForm.controls.brand.value,
      productImages: this.multiResponse,
      attributeSet_response:this.filtered
    })
    console.log(this.addProductForm.value);
    this.menuService.updateProductLiquor(this.productID, this.addProductForm.value).subscribe(res => {
      console.log(res);
      if (res['success'] == true) {

        Swal.fire('Product Updated Successfully', '', 'success')
        this.router.navigate(['/menu'])
      }
      else {
        Swal.fire('Failed to Update Product', 'Something went wrong', 'warning')
      }
    })
  }


  getRadiovalue(id, name) {
    // fa
    this.allBrands = []
    this.filteredArr = []
    this.set = []
    this.attribute_setlist = []
    this.varient_name = { varient: name, id: id };
    console.log(id, ">>", this.varient_name);

    let categoryOBJ = {
      id: id,
      string: name
    }
    let obj =
    {
      id: id,
      string: name,
      attributeType: "liquor"

    }
    console.log("hi");
    console.log(categoryOBJ)
    // await new Promise(resolve => {
    this.menuService.getBrandsByCategory(categoryOBJ).subscribe(data => {
      this.countbrand = 0

      console.log(data);
      this.allBrands = data['data']

      this.disableBrandDropdown = false
      this.allBrands = data['data']
    })
    // console.log(";en",this.allBrands)

    this.menuService.getattributeDatafromCategories(obj).subscribe(data => {
      this.count = 0
      console.log(data);
      this.attributeData = data
      this.set = this.attributeData['attributeSets']
      //  this.attribute_setlist=this.attributeData['attributeSets']
      this.attributeData['attributeSets'].forEach(element => {

        this.attribute_setlist.push(element['attributeSetName'])
      });
      console.log("list>", this.attribute_setlist);
      this.addProductForm.patchValue({
        attribute_set: this.attribute_setlist
      })
      console.log();
      this.attributeData['groupAttribute'].forEach(element => {
        console.log(element['assignAttributes']);
        this.attribute = this.attribute.concat(element['assignAttributes'])
      });
      console.log(this.attribute, ">");
      this.filteredArr = this.attribute.reduce((acc, current) => {
        const x = acc.find(item => item['_id'] === current['_id']);
        if (!x) {
          return acc.concat([current]);
        } else {
          return acc;
        }
      }, []);
      console.log(this.filteredArr);
      // this.onClickResponse();

    })

    // });
    setTimeout(() => {

      if (this.allBrands.length == 0 && this.set.length == 0) {
        Swal.fire('Liquor brand and Attribute not available', '', 'warning')
      }
      else if (this.set.length == 0) {
        Swal.fire('Attribute not available', '', 'warning')
      } else if (this.allBrands.length == 0) {
        Swal.fire('Brands not available', '', 'warning')

      }
    }, 1000);

  }
  onClickResponse(i) {

    // this.filteredArr.forEach((element, index) => {

    //  if(i==index-1){
   // console.log(this.filteredArr[i].responseType.elementName);
    if (this.filtered[i].responseType.elementName == "Input Box") {
      this.inputBoxPlaceholder = this.filtered[i].responseType.fieldValue
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true
      this.checkbox = false
    }
    if (this.filtered[i].responseType.elementName == "Yes / No") {

      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filtered[i].responseType.elementName == "Radio Button") {
      this.radioOptions = this.filtered[i].responseType.options
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filtered[i].responseType.elementName == "Check Box") {
      this.checkBoxOptions = this.filtered[i].responseType.options
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = true

    }
    // }

  }
  getRadio(r?: any, sub?: any, subsub?: any, type?: any) {
    this.rootcategory=''
    this.subcategory=''
    this.subsubcategory=''
    this.typecategory=''
    console.log(r, ">", sub, ">", subsub, ">", type);
    this.root={root:r.rootDrinkType, id:r._id}
      this.subVarient={subVarient:sub.liquorVarientName, id:sub._id}
      this.subsubVarient={subsubVarient:subsub.liquorSubVarientName, id:subsub._id}
      this.varientType={varientType:type.liquorSubSubVarientTypeName, id:type._id}
    console.log(this.root, ">", this.subVarient, ">", this.subsubVarient, ">", this.varientType);
  }

}
