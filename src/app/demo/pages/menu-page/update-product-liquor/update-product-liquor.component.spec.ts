import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateProductLiquorComponent } from './update-product-liquor.component';

describe('UpdateProductLiquorComponent', () => {
  let component: UpdateProductLiquorComponent;
  let fixture: ComponentFixture<UpdateProductLiquorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateProductLiquorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateProductLiquorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
