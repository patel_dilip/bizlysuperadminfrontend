import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-cuisine',
  templateUrl: './view-cuisine.component.html',
  styleUrls: ['./view-cuisine.component.scss']
})
export class ViewCuisineComponent implements OnInit {
addedBy:any
  constructor( public dialogRef: MatDialogRef<ViewCuisineComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { 
      console.log(data);
   this.addedBy=data['addedBy'].userName
    }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
