import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEquipmentAssetInventoryComponent } from './view-equipment-asset-inventory.component';

describe('ViewEquipmentAssetInventoryComponent', () => {
  let component: ViewEquipmentAssetInventoryComponent;
  let fixture: ComponentFixture<ViewEquipmentAssetInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEquipmentAssetInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEquipmentAssetInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
