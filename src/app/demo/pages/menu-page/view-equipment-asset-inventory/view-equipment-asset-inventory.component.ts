import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';

@Component({
  selector: 'app-view-equipment-asset-inventory',
  templateUrl: './view-equipment-asset-inventory.component.html',
  styleUrls: ['./view-equipment-asset-inventory.component.scss']
})
export class ViewEquipmentAssetInventoryComponent implements OnInit {
  count: number;
  filteredArr = [];
  set = [];
  attributeData: Object;
  attribute_setlist = [];
  attribute=[];
  inputBoxPlaceholder: any;
  radiobutton: boolean;
  yesorNo: boolean;
  inputbox: boolean;
  checkbox: boolean;
  radioOptions: any;
  checkBoxOptions: any;
  filtered: any;
 
  constructor( public dialogRef: MatDialogRef<ViewEquipmentAssetInventoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private menuservice:AddMenuFoodsService) { 
      console.log(data);
      let obj=
      {
        id: data.category['id'],
    string:data.category['category'],
  attributeType:"equipment"
 
  }
  this.filtered=data['attributeSet_response']
  console.log(obj);
  
 
  this.menuservice.getattributeDatafromotherCategories(obj).subscribe(data => {
  
    console.log(data);
    this.attributeData = data
   this.set= this.attributeData['attributeSets']
  //  this.attribute_setlist=this.attributeData['attributeSets']
  this.attributeData['attributeSets'].forEach(element => {
    
    this.attribute_setlist.push(element['attributeSetName'])
  });
  console.log("list>",this.attribute_setlist);
  
  console.log();
  this.attributeData['groupAttribute'].forEach(element => {
    console.log(element ['assignAttributes']);
    this.attribute=this.attribute.concat(element ['assignAttributes'])
  }); 
  console.log(this.attribute,">");
  this.filteredArr = this.attribute.reduce((acc, current) => {
    const x = acc.find(item => item['_id'] === current['_id']);
    if (!x) {
      return acc.concat([current]);
    } else {
      return acc;
    }
  }, []);
  console.log(this.filteredArr);
  // this.onClickResponse();
  
  })  
 
    }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  onClickResponse(i) {

    // this.filteredArr.forEach((element, index) => {

    //  if(i==index-1){
    console.log(this.filteredArr[i].responseType.elementName);
    if (this.filteredArr[i].responseType.elementName == "Input Box") {
      this.inputBoxPlaceholder = this.filteredArr[i].responseType.fieldValue
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true
      this.checkbox = false
    }
    if (this.filteredArr[i].responseType.elementName == "Yes / No") {

      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filteredArr[i].responseType.elementName == "Radio Button") {
      this.radioOptions = this.filteredArr[i].responseType.options
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filteredArr[i].responseType.elementName == "Check Box") {
      this.checkBoxOptions = this.filteredArr[i].responseType.options
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = true

    }
    // }

  }
}
