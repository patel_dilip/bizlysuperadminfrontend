import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRetailfoodBrandComponent } from './add-retailfood-brand.component';

describe('AddRetailfoodBrandComponent', () => {
  let component: AddRetailfoodBrandComponent;
  let fixture: ComponentFixture<AddRetailfoodBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRetailfoodBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRetailfoodBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
