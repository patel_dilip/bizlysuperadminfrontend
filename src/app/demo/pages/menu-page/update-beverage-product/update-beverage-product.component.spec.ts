import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBeverageProductComponent } from './update-beverage-product.component';

describe('UpdateBeverageProductComponent', () => {
  let component: UpdateBeverageProductComponent;
  let fixture: ComponentFixture<UpdateBeverageProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBeverageProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBeverageProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
