import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { element } from 'protractor';
import { MatDialog } from '@angular/material';
import { RootCategoryBeverageComponent } from '../beverages-category-dailogs/root-category-beverage/root-category-beverage.component';
import { SubCategoryBeverageComponent } from '../beverages-category-dailogs/sub-category-beverage/sub-category-beverage.component';
import { SubSubCategoryBeverageComponent } from '../beverages-category-dailogs/sub-sub-category-beverage/sub-sub-category-beverage.component';
import { SubSubCategoryTypeBeverageComponent } from '../beverages-category-dailogs/sub-sub-category-type-beverage/sub-sub-category-type-beverage.component';
import { AddBeverageBrandComponent } from '../add-beverage-brand/add-beverage-brand.component';

const URL = environment.base_Url + 'beverageproduct/uploadphoto'

@Component({
  selector: 'app-update-beverage-product',
  templateUrl: './update-beverage-product.component.html',
  styleUrls: ['./update-beverage-product.component.scss']
})
export class UpdateBeverageProductComponent implements OnInit {

  addProductForm: FormGroup

  attributes = []
  units = ['500 ML', '1 Liter', '1.5 Liter']
  brands = []
  allRetailBeveragesCategories: any
  loginUserID: any
  uploader: FileUploader
  response: string;
productIMG: any
  productObj: any
  productID: any
  constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService, 
    private router: Router, private dialog: MatDialog) {

    //get the product object for update
    this.productObj = this.menuService.editObject

    console.log("edit obj", this.productObj);
    if (this.productObj == undefined) {
      this.router.navigateByUrl("/menu")
    }

    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);

    this.getAllBeveragesCategories()
    this.getAllBeverageBrands()
    this.getAllAttributeSet()
    this.addproductFormfunction()

    this.addProductForm.patchValue({
      productName: this.productObj.productName,
      rootCategory:this.productObj.rootCategory,
      varient: this.productObj.varient,
      subVarient: this.productObj.subVarient,
      type: this.productObj.type,
      attributeSet: this.productObj.attributeSet.attributeSetName,
      unit: this.productObj.unit,
      associatedBrand: this.productObj.associatedBrand.brandName,
      productImage: this.productObj.productImage,
    })
    this.productIMG = this.productObj.productImage
    this.productID = this.productObj._id
  }

  ngOnInit() {
    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'beverageproduct'

    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.addProductForm.patchValue({
        productImage: response
      })
      this.productIMG = response
    }
   
  }

  addproductFormfunction() {
    this.addProductForm = this.fb.group({
      productName: ['', Validators.required],
      rootCategory: [''],
      varient: [''],
      subVarient: [''],
      type: [''],
      attributeSet: ['', Validators.required],
      unit: ['', Validators.required],
      associatedBrand: ['', Validators.required],
      productImage: [''],
      userid: this.loginUserID
    })
  }

  subVarientfunction(rootCategory, subCategory, subSubCategory) {
    console.log(rootCategory, subCategory, subSubCategory);
    this.addProductForm.patchValue({
      rootCategory: rootCategory,
      varient: subCategory,
      subVarient: subSubCategory
    })
  }

  getAllBeveragesCategories() {
    this.menuService.getAllBeverageCategories().subscribe(data => {
      console.log("All Beverages Categories", data);
      if (data['sucess'] == false) {
        console.log("Beverages categories data not found");
      } else {
        this.allRetailBeveragesCategories = data['data'].filter(element => element.beverageType == "retailBeverage")
      }
    })
  }

  //*************************OPEN ROOT CATEGORY IN BEVERAGE ********************** */
  openBeveragesRootCategoryDailog(beverageType) {
    const dialogRef = this.dialog.open(RootCategoryBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: beverageType
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }

  //*************************OPEN SUB CATEGORY IN BEVERAGE ********************** */
  openBeveragesSubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }

  //*************************OPEN SUB SUB CATEGORY IN BEVERAGE ********************** */
  openBeveragesSubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN BEVERAGE ********************** */
  openBeveragesSubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }


  //get all beverage brands
  getAllBeverageBrands() {
    this.menuService.getAllBeverageBrands().subscribe(data => {
      console.log("All Beverage Brands", data);
      this.brands = data['data']

    })
  }

   //popup for add brand Beverages
   addBrand(): void {
    const dialogRef = this.dialog.open(AddBeverageBrandComponent, {
      disableClose: true,
      width:'60%',
      height:'80%',
     });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getAllBeverageBrands()
    });
  }

  //get all attribute set
  getAllAttributeSet() {
    this.menuService.getAllLiquorAttributeSets().subscribe(data => {
      console.log("All liquor attribute sets", data);
      this.attributes = data['data'].filter(ele => ele.attributeType == "beverage")
    })
  }

  //open tree node and close
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  onAddProduct() {
  const brand = this.brands.find(ele => ele.brandName == this.addProductForm.controls.associatedBrand.value)
  const attributeSet = this.attributes.find(ele => ele.attributeSetName == this.addProductForm.controls.attributeSet.value)
  
  this.addProductForm.patchValue({
    attributeSet: attributeSet._id,
    associatedBrand: brand._id
  })
  
  console.log(this.addProductForm.value);
    this.menuService.updateProductBeverage(this.productID, this.addProductForm.value).subscribe(res => {
      console.log(res);

      if (res['success'] == true) {

        Swal.fire('Product Updated Successfully', '', 'success')
        this.router.navigate(['menu'])
      }
      else {
        Swal.fire('Failed to Update Product', 'Something went wrong', 'warning')
      }
    })
  }
}
