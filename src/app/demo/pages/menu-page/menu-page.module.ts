import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuPageComponent } from './menu-page.component';
import { Routes, RouterModule } from '@angular/router';
import { AddMenuCatComponent } from './addFood-cat/add-menu-cat/add-menu-cat.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { MaterialModule } from '../../../material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AddCategoryComponent } from './add-category/add-category.component';
import { AddDishesComponent } from './add-dishes/add-dishes.component';
import { AddAddOnComponent } from './add-add-on/add-add-on.component';
import { AddVarientsComponent } from './add-varients/add-varients.component'
import { SharedModule } from '../../../theme/shared/shared.module';
import {DataTablesModule} from 'angular-datatables';
import { ViewCuisineComponent } from './view-cuisine/view-cuisine.component';
import { ViewCategoryComponent } from './view-category/view-category.component';
import { ViewdishComponent } from './viewdish/viewdish.component';
import { ViewVarientComponent } from './view-varient/view-varient.component';
import { ViewAddOnComponent } from './view-add-on/view-add-on.component';
import { AddBrandComponent } from './add-brand/add-brand.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ViewBrandComponent } from './view-brand/view-brand.component';
import { ViewProductComponent } from './view-product/view-product.component';
//import { ViewLiquorAttributeComponent } from './view-liquor-attribute/view-liquor-attribute.component';
//import { AddIngredientsComponent } from './add-ingredients/add-ingredients.component';
//import { AddItemsComponent } from './add-items/add-items.component';
import{AddAttributeComponent}from './add-attribute/add-attribute.component'

import { AddEquipmentAttributesComponent } from './add-equipment-attributes/add-equipment-attributes.component';
import { CreateAttributeSetComponent } from './create-attribute-set/create-attribute-set.component';
import { FileUploadModule } from "ng2-file-upload";
import { NgxLoadingModule } from 'ngx-loading';

import { AddCuisinesComponent } from '../../../PopoversList/add-cuisines/add-cuisines.component';
import { UpdateAddonsComponent } from './update-addons/update-addons.component';
import { UpdateCategoryComponent } from './update-category/update-category.component';
import { UpdateCuisineComponent } from './update-cuisine/update-cuisine.component';
import { UpdateDishComponent } from './update-dish/update-dish.component';
import { UpdateVarientComponent } from './update-varient/update-varient.component';
import { SelectModule } from 'ng-select';
//import { AddBeverageAttributeComponent } from './add-beverage-attribute/add-beverage-attribute.component';
// { AddBeverageBrandComponent } from './add-beverage-brand/add-beverage-brand.component';
//import { AddBeverageProductComponent } from './add-beverage-product/add-beverage-product.component';
import { AddRetailfoodAttributeComponent } from './add-retailfood-attribute/add-retailfood-attribute.component';
//import { AddRetailfoodBrandComponent } from './add-retailfood-brand/add-retailfood-brand.component';
//import { AddRetailfoodProductComponent } from './add-retailfood-product/add-retailfood-product.component';
import { ViewLiquorAttributeSetComponent } from './view-liquor-attribute-set/view-liquor-attribute-set.component';
import { UpdateBrandComponent } from './update-brand/update-brand.component';
import { UpdateProductLiquorComponent } from './update-product-liquor/update-product-liquor.component';
import { NgbPopoverModule, NgbTooltipModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
//import { AddBeveragesComponent } from './add-beverages/add-beverages.component';
import { UpdateBeverageProductComponent } from './update-beverage-product/update-beverage-product.component';
import { UpdateRetailFoodProductComponent } from './update-retail-food-product/update-retail-food-product.component';
import { UpdateBeverageComponent } from './update-beverage/update-beverage.component';
import{UpdateAttributeComponent} from './update-attribute/update-attribute.component'
//import { AddLiquorAttributeSetComponent } from './add-liquor-attribute-set/add-liquor-attribute-set.component';
import { AttributesetComponent } from './attributeset/attributeset.component';
import { UpdateAttributeGroupSetComponent } from './update-attribute-group-set/update-attribute-group-set.component';

const routes: Routes = [
  {
    path: '',
    component: MenuPageComponent,
    children: [
      {
        path: '',
        component: MenuListComponent
      },
      {
        path: 'add-food',
        component: AddMenuCatComponent
      },
      {
        path: 'add-category',
        component: AddCategoryComponent
      },
      {
        path: 'add-dishes',
        component: AddDishesComponent
      },
      {
        path: 'add-add-on',
        component: AddAddOnComponent
      },
      {
        path: 'add-varients',
        component: AddVarientsComponent
      },
    
      {
        path: 'add-brand',
        component:AddBrandComponent
      },{
        path: 'add-product',
        component:AddProductComponent
      },{
        path: 'view-brand',
        component: ViewBrandComponent
      },{
        path: 'view-product',
        component: ViewProductComponent
      },{
        path:'add-equipment-attributes',
        component: AddEquipmentAttributesComponent
      },{
        path: 'add-attribute/:key',
        component:AddAttributeComponent
      },
      {
        path: 'add-attributeset/:key',
        component:AttributesetComponent
      },

      {
        path:'create-attribute-set',
        component: CreateAttributeSetComponent
      },
      {
        path: 'update-attribute',
        component: UpdateAttributeComponent
      },
      {
        path: 'update-cuisine',
        component: UpdateCuisineComponent
      },{
        path: 'update-category',
        component: UpdateCategoryComponent
      },
      {
        path: 'update-dish',
        component: UpdateDishComponent
      },
      {
        path: 'update-addon',
        component: UpdateAddonsComponent
      },
      {
        path: 'update-varient',
        component: UpdateVarientComponent
      },
      {
        path: 'update-attributeset',
        component: UpdateAttributeGroupSetComponent
      },
     
      
      // {
      //   path: 'add-retailfood-attribute',
      //   component:  AddRetailfoodAttributeComponent
      // },
     
     
      {
        path: 'update-brand-liquor',
        component:UpdateBrandComponent
      },{
        path: 'update-product-liquor',
        component: UpdateProductLiquorComponent
      },
      {
        path: 'update-beverage-product',
        component: UpdateBeverageProductComponent
      },
      {
        path: 'update-retail-food-product',
        component: UpdateRetailFoodProductComponent
      },
      {
        path: 'update-beverage',
        component: UpdateBeverageComponent
      }
    ]
  }
]

@NgModule({
  declarations: [AddMenuCatComponent, UpdateAttributeGroupSetComponent, AttributesetComponent , MenuPageComponent, MenuListComponent, UpdateAttributeComponent, AddAttributeComponent ,AddCategoryComponent, AddDishesComponent, AddAddOnComponent, AddVarientsComponent,  AddBrandComponent, AddProductComponent, ViewBrandComponent, ViewProductComponent, AddEquipmentAttributesComponent, CreateAttributeSetComponent, UpdateAddonsComponent, UpdateCategoryComponent, UpdateCuisineComponent, UpdateDishComponent, UpdateVarientComponent, AddRetailfoodAttributeComponent, UpdateBrandComponent, UpdateProductLiquorComponent, UpdateBeverageProductComponent, UpdateRetailFoodProductComponent, UpdateBeverageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule,
    FileUploadModule,
    SelectModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    NgxLoadingModule.forRoot({}),
    NgbPopoverModule,
    NgbTooltipModule,
    NgbTabsetModule
    
  ]
})
export class MenuPageModule {


}
