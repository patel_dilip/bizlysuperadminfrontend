import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ViewCategoryComponent } from '../view-category/view-category.component';

@Component({
  selector: 'app-view-varient',
  templateUrl: './view-varient.component.html',
  styleUrls: ['./view-varient.component.scss']
})
export class ViewVarientComponent implements OnInit {
  addedBy:any
  constructor( public dialogRef: MatDialogRef<ViewCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { 
      console.log(data);
      this.addedBy=data['varientType'].userName
      let i=0
      data['varientType'].forEach(element => {
        if(element.varientName_type=='' && element.type==''){
          data['varientType'].splice(i,1);
        }
        i++
      });
    }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}