import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewVarientComponent } from './view-varient.component';

describe('ViewVarientComponent', () => {
  let component: ViewVarientComponent;
  let fixture: ComponentFixture<ViewVarientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewVarientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewVarientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
