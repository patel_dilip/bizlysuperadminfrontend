import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRetailFoodBrandComponent } from './update-retail-food-brand.component';

describe('UpdateRetailFoodBrandComponent', () => {
  let component: UpdateRetailFoodBrandComponent;
  let fixture: ComponentFixture<UpdateRetailFoodBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateRetailFoodBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRetailFoodBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
