import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { RootCategoryRetailFoodComponent } from '../retail-food-category-dailogs/root-category-retail-food/root-category-retail-food.component';
import { SubCategoryRetailFoodComponent } from '../retail-food-category-dailogs/sub-category-retail-food/sub-category-retail-food.component';
import { SubSubCategoryRetailFoodComponent } from '../retail-food-category-dailogs/sub-sub-category-retail-food/sub-sub-category-retail-food.component';
import { SubSubCategoryTypeRetailFoodComponent } from '../retail-food-category-dailogs/sub-sub-category-type-retail-food/sub-sub-category-type-retail-food.component';

const URL = environment.base_Url + 'retailfoodbrand/uploadphoto'


@Component({
  selector: 'app-update-retail-food-brand',
  templateUrl: './update-retail-food-brand.component.html',
  styleUrls: ['./update-retail-food-brand.component.scss']
})
export class UpdateRetailFoodBrandComponent implements OnInit {

 
    addBrandForm: FormGroup
    countries: any
    uploader: FileUploader
    response: string;
    loginUserID: any
    brandObject: any
    brandImage: any
    brandID: any
  allRetailFoodCategories=[];
  dialog: any;
  count: number;
  subsubFlag: any;
  typeFlag: number;
  subFlag: number;
  categoryType=[];
  id: any;
  retailFoodtree=[];
  countbrand: number;
  allBrands: any;
  disableBrandDropdown: boolean;
  attributeData: Object;
  set: any;
  attribute_setlist: any;
  attribute: any;
  filteredArr: any;
  ti=0;
  si=0;
  ssi=0;
    constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<UpdateRetailFoodBrandComponent>,
      private mainService: MainService, private menuService: AddMenuFoodsService, 
      @Inject(MAT_DIALOG_DATA) public data) {
  
        console.log(data);
        
  
      //get login user details
      let loginUser = JSON.parse(localStorage.getItem('loginUser'))
      this.loginUserID = loginUser._id
      console.log(this.loginUserID);
  this.categoryType=data.categoryType
  
      this.getAllCountries()
      this.addBrandFormFunction()
      this.getAllRetailFoodCategories()
  
  
      this.addBrandForm.patchValue({
        brandName: data.brandName,
        country: data.country,
        brandLogo: data.brandLogo,
        categoryType:data.categoryType
      })
      this.brandImage = data.brandLogo
      this.brandID = data._id
      
    }
  
    ngOnInit() {
  
  
      //  NG FILE UPLOADER CODE
      this.uploader = new FileUploader({
        url: URL,
        itemAlias: 'retailfoodbrand'
  
      });
  
      this.response = '';
  
      this.uploader.response.subscribe(res => this.response = res);
  
      this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
  
        this.addBrandForm.patchValue({
          brandLogo: response
        })
        this.brandImage = response
      }
    }
  
    addBrandFormFunction() {
      this.addBrandForm = this.fb.group({
        brandName: ['', Validators.required],
        country: ['', Validators.required],
        brandLogo: [''],
        userid: this.loginUserID,
        categoryType:[]
      })
    }
  
    // getting all countries name
    getAllCountries() {
      this.mainService.getAllDialCode().subscribe(countryCode => {
        console.log('countries', countryCode);
  
        // Array object for countries
        this.countries = countryCode;
  
      })
    }
  
    //add brand 
    addBrand() {
      this.addBrandForm.patchValue({
        categoryType:this.categoryType
      })
      console.log("add Brand", this.addBrandForm.value);
      this.menuService.updateRetailFoodBrand(this.brandID,this.addBrandForm.value).subscribe(res => {
        console.log(res);
        if (res['success'] == true) {
  
          Swal.fire('Brand Updated Successfully', '', 'success')
          this.dialogRef.close();
        }
        else {
          Swal.fire('Failed to Update Brand', 'Something went wrong', 'warning')
        }
      })
    }
    caretClick() {
      var toggler = document.getElementsByClassName("caret");
      var i;
      for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function () {
          this.parentElement.querySelector(".nested").classList.toggle("active");
          this.classList.toggle("caret-down");
        });
      }
    }
    getAllRetailFoodCategories() {
      this.menuService.getAllRetailFoodCategories().subscribe(data => {
        console.log("All Retail Food categories", data);
        this.allRetailFoodCategories = data['data']
        this.allRetailFoodCategories.forEach(element => {
          this.categoryType.forEach(ele=>{
          if(element['_id']==ele['_id'])
          element=ele
          
          })
           this.retailFoodtree.push(element)
        });
    this.allRetailFoodCategories=this.retailFoodtree
    console.log("asset inventory:", this.allRetailFoodCategories);
    
      })
    }
  
  //*************************OPEN ROOT CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  //*************************OPEN SUB CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodSubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  //*************************OPEN SUB SUB CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodSubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN RETAIL FOOD ********************** */
  openRetailFoodSubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }
  RetailfoodchangeFlag(data, event, root_level, rootindex?: any, subcategoryIndex?: any, subsubcategoryINdex?: any, typeIndex?: any) {
    //console.log( ">>>" , data, ">>>" , event.checked);
    this.count=0
    console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
    if (root_level == 'root') {
      this.allRetailFoodCategories.forEach((element, index) => {
        if (index == rootindex) {
          element.showChildren = event.checked
          if (
            this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
            &&
            this.allRetailFoodCategories[index].subCategories.length > 0
          ) {
            this.allRetailFoodCategories[index].subCategories.forEach((element, subcategoryIndex = index) => {
              element.showChildren = event.checked;
              if (
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
                  element.showChildren = event.checked;
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, subSubCategoryTypeIndex = index) => {
                      element.showChildren = event.checked;
                    })
                  }
  
  
                })
              }
            })
          }
        }
      });
      //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
    }
    else if (root_level == 'sub') {
      this.allRetailFoodCategories.forEach((Rootelement, index) => {
        if (index == rootindex) {
          // Rootelement.showChildren = event.checked
          if (
            this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
            &&
            this.allRetailFoodCategories[index].subCategories.length > 0
          ) {
            this.allRetailFoodCategories[index].subCategories.forEach((subelement, subindex) => {
              if (subindex == subcategoryIndex) {
                subelement.showChildren = event.checked;
                if (
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                  &&
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((subsubelement, subsubcategoryINdex = index) => {
                    subsubelement.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
  
                      this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((typeelement, subSubCategoryTypeIndex = index) => {
                        typeelement.showChildren = event.checked;
                      })
                    }
  
  
                  })
                }
              }
  
            })
  
          }
  
  
        }
      });
      //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
    } else if (root_level == 'subsub') {
      this.allRetailFoodCategories.forEach((element, index) => {
        if (index == rootindex) {
          // element.showChildren = event.checked
          if (
            this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
            &&
            this.allRetailFoodCategories[index].subCategories.length > 0
          ) {
            this.allRetailFoodCategories[index].subCategories.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                //             element.showChildren = event.checked;
                if (
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                  &&
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      element.showChildren = event.checked;
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, subSubCategoryTypeIndex = index) => {
                          element.showChildren = event.checked;
                        })
                      }
                    }
  
                  })
                }
                // }
              }
            })
          }
  
  
        }
      });
      //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
    } else if (root_level == 'type') {
      this.allRetailFoodCategories.forEach((element, index) => {
        if (index == rootindex) {
          if (
            this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
            &&
            this.allRetailFoodCategories[index].subCategories.length > 0
          ) {
            this.allRetailFoodCategories[index].subCategories.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                if (
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                  &&
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, childIndex) => {
                          if (typeIndex == childIndex)
                            element.showChildren = event.checked;
                        })
                      }
  
  
                    }
                  })
                }
                // }
              }
            })
          }
  
  
        }
      });
      //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
    }
    ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////
  
    this.allRetailFoodCategories.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailFoodCategories[index].subCategories.length > 0
        ) {
          this.allRetailFoodCategories[index].subCategories.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.allRetailFoodCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
  
                this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {
                  // if(subsubINdex==0){
                  //   this.subsubFlag=0
                  // }
                  if (subsubINdex == subsubcategoryINdex) {
                    if (subsubelement.showChildren == true) {
                      this.subsubFlag++
                    }
                    //console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length);
                    //console.log(">>>", this.subsubFlag);
  
  
                    if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length == this.subsubFlag) {
                      subelement.showChildren = true
                    } else {
                      subelement.showChildren = false;
                    }
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      // //console.log(element);
                      this.typeFlag = 0
                      this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {
                        if (typeIndex == childIndex) {
                          typeelement.showChildren = event.checked;
                        }
                        if (typeelement.showChildren == true) {
                          this.typeFlag++
                        }
                        // //console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length);
                        // //console.log(">>>", this.typeFlag);
  
  
                        if (this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length == this.typeFlag) {
                          subsubelement.showChildren = true
                        } else {
                          subsubelement.showChildren = false;
                        }
  
                      })
                    }
  
  
                  }
                })
              }
              // }
            }
          })
        }
  
  
      }
    });
    //subsub
    this.subsubFlag = 0
  
    this.allRetailFoodCategories.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailFoodCategories[index].subCategories.length > 0
        ) {
          this.allRetailFoodCategories[index].subCategories.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
  
              if (
                this.allRetailFoodCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
  
                this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {
  
                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  // //console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length);
                  // //console.log(">>>", this.subsubFlag);
  
  
                  if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    // this.typeFlag=0
                    this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {
  
  
  
                    })
                  }
                })
              }
  
            }
          })
        }
  
  
      }
    });
  
    //////subflag
    this.subFlag = 0
    this.subsubFlag = 0
  
    this.allRetailFoodCategories.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailFoodCategories[index].subCategories.length > 0
        ) {
          this.allRetailFoodCategories[index].subCategories.forEach((subelement, subindex) => {
            // if(subindex==subcategoryIndex){
            if (subelement.showChildren == true) {
              this.subFlag++
            }
            console.log("<<<", this.allRetailFoodCategories[index].subCategories.length);
            console.log(">>>", this.subFlag);
  
  
            if (this.allRetailFoodCategories[index].subCategories.length == this.subFlag) {
              Rootelement.showChildren = true
            } else {
              Rootelement.showChildren = false;
            }
  
            if (
              this.allRetailFoodCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
              &&
              this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
  
              this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {
  
  
                if (
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {
  
  
  
                  })
                }
              })
            }
  
            // }
          })
        }
  
  
      }
    });
    if (this.allRetailFoodCategories[rootindex].showChildren == false) {
      // element.showChildren = event.checked
      if (
        this.allRetailFoodCategories[rootindex].hasOwnProperty('subCategories')
        &&
        this.allRetailFoodCategories[rootindex].subCategories.length > 0
      ) {
        this.allRetailFoodCategories[rootindex].subCategories.forEach((element, subindex) => {
         if( element.showChildren == false){
          if (
            this.allRetailFoodCategories[rootindex].subCategories[subindex].hasOwnProperty('subSubCategories')
            &&
            this.allRetailFoodCategories[rootindex].subCategories[subindex].subSubCategories.length > 0
          ) {
            // tslint:disable-next-line: max-line-length
            this.allRetailFoodCategories[rootindex].subCategories[subindex].subSubCategories.forEach((element, subsubINdex) => {
              if( element.showChildren == false){
              if (
                // tslint:disable-next-line: max-line-length
                this.allRetailFoodCategories[rootindex].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                &&
                // tslint:disable-next-line: max-line-length
                this.allRetailFoodCategories[rootindex].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                //console.log(element);
                this.allRetailFoodCategories[rootindex].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((element, liquorSubSubVarientTypeIndex) => {
                if( element.showChildren == true)
                {               
                  this.count=1;
                  return false
                }
                })
              }
    
    
            }
            else{
              this.count=1;
              return false;
            }
            })
          }
        }
        if(element.showChildren==true){
        this.count=1;
        return false;
        }
        })
      }
    }
    else{
      this.count=1
    }
    
    console.log(this.count);
    let id
    if(this.count==1){
    if(this.categoryType.length==0){
    this.categoryType.push(this.allRetailFoodCategories[rootindex])
    }else{
    this.categoryType.forEach(element => {
    if(element['_id']==this.allRetailFoodCategories[rootindex]['_id']){
      element=this.allRetailFoodCategories[rootindex]
      this.id=element['_id'];
    }
    });
    if(this.id!=this.allRetailFoodCategories[rootindex]['_id']){
    this.categoryType.push(this.allRetailFoodCategories[rootindex])
    }
    }
    }
    if(this.count==0){  
    let i=0;
    this.categoryType.forEach(element => {
    
    if(element['_id']==this.allRetailFoodCategories[rootindex]['_id']){
    this.categoryType.splice(i, 1)
    }else{
     console.log("not match");
     
    }
    i++;
    });
    }
    console.log(this.categoryType);
    this.intermediateRetailFood(data, event, root_level, rootindex = rootindex, subcategoryIndex = subcategoryIndex, subsubcategoryINdex = subsubcategoryINdex, typeIndex = typeIndex)
    
   
  }
  intermediateRetailFood(data, event, root_level, rootindex, subcategoryIndex, subsubcategoryINdex, typeIndex?: any) {
    ///////////////////////////////////////// intermediate ////////////////////////////////////
    console.log(data, "<", event, "<", root_level, "<", rootindex, "<", subcategoryIndex, "<", subsubcategoryINdex, "<", typeIndex);
    // this.ri = 0
    // this.si = 0
    // this.ssi = 0
    
      this.ti = 0
  
   
      this.allRetailFoodCategories.forEach((Rootelement, index) => {
  
        if (index == rootindex) {
         
          console.log("sub length:", this.allRetailFoodCategories[rootindex].subCategories.length);
  
          if (
            this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
            &&
            this.allRetailFoodCategories[index].subCategories.length > 0
          ) {
            this.si=0
            this.allRetailFoodCategories[index].subCategories.forEach((subelement, subindex) => {
              // debugger;
              if (subelement['showChildren'] == true) {
                this.si++
              }
              if (subindex == subcategoryIndex) {
               
                if (
                  this.allRetailFoodCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
                  &&
                  this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
  // this.ssi=0
  // debugger;
  this.ssi=0
  
  
                  this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {
                    if (subsubelement['showChildren'] == true) {
                      this.ssi++
                    }
                        
                    if (subsubINdex == subsubcategoryINdex) {
  
                      // ////console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length);
                      // ////console.log(">>>", this.subsubFlag);
                     
  
  
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        // ////console.log(element);
                        // this.typeFlag=0
                        this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {
                          // if(typeIndex==childIndex){
                          if (typeelement['showChildren'] == true) {
                            this.ti++
                          }
                          console.log(this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length, "type>>", this.ti);
  
                          if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > this.ti) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subsubelement.inter = true;
                          } else if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length == this.ti) {
  
                            subsubelement.inter = false;
                            subsubelement.showChildren = true;
                          }
                        //  if (this.ti== 0) {
                        //   subsubelement.inter = false;
                        //   subsubelement.showChildren = false;
                        // }
  
                          if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > this.ssi) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subelement.inter = true;
                          } else if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length == this.ssi) {
  
                            subelement.inter = false;
                            subelement.showChildren = true;
                          }
                          
                          if (this.allRetailFoodCategories[index].subCategories.length > this.si) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subelement.inter = true;
                          } else if (this.allRetailFoodCategories[index].subCategories.length == this.si) {
                            Rootelement.inter = false;
                            Rootelement.showChildren = true;
                          }
                        
                          //  }
                        })
                        if (this.ti== 0) {
                          subsubelement.inter = false;
                          subsubelement.showChildren = false;
                        }
                      }
                      
                     
                            }
                            console.log(this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length, "subsub>>", this.ssi);
  
                            if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > this.ssi) {
                              // Rootelement.inter=true;
                              // subelement.inter=true;
                              subelement.inter = true;
                            } else if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length == this.ssi) {
        
                              subelement.inter = false;
                              subelement.showChildren = true;
                            } 
                           
                           
                  })
                  if (this.ssi== 0 && this.ti==0) {
                      subelement.inter = false;
                      subelement.showChildren = false;
                    }
                }
  
            
          
                
                console.log(this.allRetailFoodCategories[index].subCategories.length, "sub>>", this.si);
  
               
                 }
                 if (this.allRetailFoodCategories[index].subCategories.length > this.si) {
                  // Rootelement.inter=true;
                  // subelement.inter=true;
                  Rootelement.inter = true;
                } else if (this.allRetailFoodCategories[index].subCategories.length == this.si) {
  
                  Rootelement.inter = false;
                  Rootelement.showChildren = true;
                } 
               
             })
             if (this.ssi== 0 && this.ti==0 && this.si==0) {
              Rootelement.inter = false;
              Rootelement.showChildren = false;
            }
          }
  
  
        }
      });
   
  }
  
  onNoClick(){
    this.dialogRef.close()
  }
  }
  