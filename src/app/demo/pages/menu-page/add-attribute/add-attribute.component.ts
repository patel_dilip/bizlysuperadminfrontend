import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AddMenuFoodsService } from '../../../../_services/_menuServices/add-menu-foods.service';
import { MatDialog } from '@angular/material';
import { AddAttributeRadioButtonComponent } from '../../menu-page/add-attribute-radio-button/add-attribute-radio-button.component';
import { AddAttributeCheckboxComponent } from '../../menu-page/add-attribute-checkbox/add-attribute-checkbox.component';
import { environment } from '../../../../../environments/environment';
import { FileUploader } from 'ng2-file-upload';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-attribute',
  templateUrl: './add-attribute.component.html',
  styleUrls: ['./add-attribute.component.scss']
})
export class AddAttributeComponent implements OnInit {
  addAttributeForm: FormGroup
  SelectResponses: string[] = ['Input Box', 'Yes / No', 'Radio Button', 'Check Box']
  radiobutton = false
  yesorNo = false
  inputbox = false
  checkbox = false
  uploader: FileUploader
  response: string;
  radioOptions = []
  checkBoxOptions = []
  loading = false
  autoTrue = true
  path_name:string
  loginUserID: any
  attributeKey:any
  constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService,
    private dialog: MatDialog, private router: Router, private route:ActivatedRoute) {
      this.route.params.subscribe(params=>{
        this.attributeKey=params['key']
        console.log("attribute key",this.attributeKey);
        if(this.attributeKey=="liquor"){
         this.path_name="Menu Management"
        }else if(this.attributeKey=="equipment"){
          this.path_name="Inventory Management"  
    
        }else if(this.attributeKey=="beverage"){
          this.path_name="Beverages And Retail Food"
        } else if(this.attributeKey=="retailFood" || this.attributeKey=='retailBeverages'){
          this.path_name="Beverages And Retail Food"
        }
        
      })
    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID)
  }

  ngOnInit() {

    this.addActivity()

  }

  // SETLOADING CALL FOR LOADER
  setLoading() {
    this.loading = true
  }

  //reactive form 
  addActivity() {
    this.addAttributeForm = this.fb.group({
      attributeName: ['', [Validators.required, Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      selectResponse: ['', Validators.required],
      display_name :['', [Validators.required, Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      inputBox: [''],
      radioButton: [''],
      yesNo: [''],
      checkBoxButton: [''],
      searchable: [true],
      filterable: [true]
     
    })
  } 

  //on click respone dropdown respective response input open
  onClickResponse(response) {
    console.log(response);
    if (response == "Input Box") {
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true
      this.checkbox = false
    }
    if (response == "Yes / No") {
      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false
      this.checkbox = false
    }
    if (response == "Radio Button") {
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = false
    }
    if (response == "Check Box") {
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = true

    }

  }

  //In radio button reponse add new radio buttons
  addOption(): void {
    const dialogRef = this.dialog.open(AddAttributeRadioButtonComponent, {
      width: '350px',
      disableClose: true,
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result == undefined || result == [] || result == null) {

      } else {
        result.radioButton.forEach(element => {
          this.radioOptions.push(element)
        });
        this.addAttributeForm.controls.radioButton.reset()
        console.log(this.radioOptions);
      }
      // this.animal = result;
    });
  }

  //In Checkbox button reponse add new Checkbox buttons
  addCheckBox(): void {
    const dialogRef = this.dialog.open(AddAttributeCheckboxComponent, {
      width: '350px',
      disableClose: true,
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result == undefined || result == [] || result == null) {

      } else {
        result.checkBoxButton.forEach(element => {
          this.checkBoxOptions.push(element)
        });
        this.addAttributeForm.controls.checkBoxButton.reset()
        console.log(this.checkBoxOptions);
      }
      // this.animal = result;
    });
  }
  //final create json object
  onAddAlbum() {

    let value = this.addAttributeForm.controls.selectResponse.value
    let fieldValue
    let options

    switch (value) {
      case "Input Box":
        fieldValue = this.addAttributeForm.controls.inputBox.value
        options = [
          {
            "optionLabel": "",
            "associateImage": ""
          }
        ]
        break
      case "Yes / No":
        fieldValue = this.addAttributeForm.controls.selectResponse.value
        options = [
          {
            "optionLable": "",
            "associateImage": ""
          }
        ]
        break
      case "Radio Button":
        options = this.radioOptions
        fieldValue = ""
        break
      case "Check Box":
        options = this.checkBoxOptions
        fieldValue = ""
        break
    }

    let attributeobj = {
      "display_name":this.addAttributeForm.controls.display_name.value,
      "attributeName": this.addAttributeForm.controls.attributeName.value,
      "responseType": {
        "elementName": this.addAttributeForm.controls.selectResponse.value,
        "fieldValue": fieldValue,
        "options": options,
      },
      "isSearchable": this.addAttributeForm.controls.searchable.value,
      "isFilterable": this.addAttributeForm.controls.filterable.value,
      "userid": this.loginUserID,
      "attributeType": this.attributeKey
    }
    console.log(attributeobj);

    //    console.log(this.addAttributeForm.value);
    this.menuService.postLiquorAttribute(attributeobj).subscribe(res=>{
      console.log(res);
      if(res['sucess']){
        Swal.fire('Attribute Added Successfully','','success')
        if(this.attributeKey=="equipment"){
          this.router.navigate(['inventory/equipment-attributes'])  
        }else if(this.attributeKey=="liquor"){
        this.router.navigate(['menu'])
        }
        else if(this.attributeKey=="beverage"){
          this.router.navigate(['beverages_and_retailfood'])
        }
        else if(this.attributeKey=="retailFood" || this.attributeKey== "retailBeverages"){
          this.router.navigate(['beverages_and_retailfood'])
        }
      }
      else {
        Swal.fire('Fail to Add Attribute','Something Went Wrong','warning')
      }
    })
  }

  backButton(){
    if(this.attributeKey=="liquor"){
      this.router.navigate(['menu'])
    }else if(this.attributeKey=="equipment"){
      this.router.navigate(['inventory/equipment-attributes'])  

    }else if(this.attributeKey=="beverage"){
      this.router.navigate(['beverages_and_retailfood'])
    } else if(this.attributeKey=="retailFood" || this.attributeKey=="retailBeverages"){
      this.router.navigate(['beverages_and_retailfood'])
    }
  }

  checkAttributeExist(event){
    this.menuService.checkAttributeExist(event.target.value.toLowerCase(),this.attributeKey).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        Swal.fire("Attribute Name Already Exist..!","", "warning")
        event.target.value=""
        this.addAttributeForm.patchValue({
          attributeName:''
        })
      }

    })
  }
}
