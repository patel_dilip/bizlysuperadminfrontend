import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AddAttributeCheckboxComponent } from '../add-attribute-checkbox/add-attribute-checkbox.component';
import { AddAttributeRadioButtonComponent } from '../add-attribute-radio-button/add-attribute-radio-button.component';
import { FileUploader } from 'ng2-file-upload';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-retailfood-attribute',
  templateUrl: './add-retailfood-attribute.component.html',
  styleUrls: ['./add-retailfood-attribute.component.scss']
})
export class AddRetailfoodAttributeComponent implements OnInit {
  addAttributeForm: FormGroup
  SelectResponses: string[] = ['Input Box', 'Yes / No', 'Radio Button', 'Check Box']
  radiobutton = false
  yesorNo = false
  inputbox = false
  checkbox = false
  uploader: FileUploader
  response: string;
  radioOptions = []
  checkBoxOptions = []
  loading = false
autoTrue=true
  constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService,
     private dialog: MatDialog, private router: Router) {

  }

  ngOnInit() {



    this.addActivity()

  }

  // SETLOADING CALL FOR LOADER
  setLoading() {
    this.loading = true
  }

  addActivity() {
    this.addAttributeForm = this.fb.group({
      attributeName: ['',Validators.required],
      selectResponse: ['',Validators.required],
      inputBox: [''],
      radioButton: [''],
      yesNo: [''],
      checkBoxButton: [''],
      searchable:[false],
      filterable:[false]
    })
  }


  onClickResponse(response) {
    console.log(response);
    if (response == "Input Box") {
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true
      this.checkbox = false
    }
    if (response == "Yes / No") {
      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false
      this.checkbox = false
    }
    if (response == "Radio Button") {
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = false
    }
    if (response == "Check Box") {
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = true

    }

  }

  addOption(): void {
    const dialogRef = this.dialog.open(AddAttributeRadioButtonComponent, {
      width: '350px',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result == undefined || result == [] || result == null) {

      } else {
        result.radioButton.forEach(element => {
          this.radioOptions.push(element)
        });
        this.addAttributeForm.controls.radioButton.reset()
        console.log(this.radioOptions);
      }
      // this.animal = result;
    });
  }

  addCheckBox(): void {
    const dialogRef = this.dialog.open(AddAttributeCheckboxComponent, {
      width: '350px',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result == undefined || result == [] || result == null) {

      } else {
        result.checkBoxButton.forEach(element => {
          this.checkBoxOptions.push(element)
        });
        this.addAttributeForm.controls.checkBoxButton.reset()
        console.log(this.checkBoxOptions);
      }
      // this.animal = result;
    });
  }

  onAddAlbum() {

    let value = this.addAttributeForm.controls.selectResponse.value
    let fieldValue
    let options

    switch (value) {
      case "Input Box":
        fieldValue = this.addAttributeForm.controls.inputBox.value
        options = [
          {
            "optionLabel": "",
            "associateImage": ""
          }
        ]
        break
      case "Yes / No":
        fieldValue = this.addAttributeForm.controls.selectResponse.value
        options = [
          {
            "optionLable": "",
            "associateImage": ""
          }
        ]
        break
      case "Radio Button":
        options = this.radioOptions
        fieldValue = ""
        break
      case "Check Box":
        options = this.checkBoxOptions
        fieldValue = ""
        break
    }

    let attributeobj = {
      "attributeName": this.addAttributeForm.controls.attributeName.value,
      "responseType": {
        "elementName": this.addAttributeForm.controls.selectResponse.value,
        "fieldValue": fieldValue,
        "options": options,
        "searchable":this.addAttributeForm.controls.searchable.value,
        "filterable": this.addAttributeForm.controls.filterable.value

      }
    }
    console.log(attributeobj);

    console.log(this.addAttributeForm.value);
    // this.menuService.postLiquorAttribute(attributeobj).subscribe(res=>{
    //   console.log(res);
    //   if(res['sucess']){
    //     Swal.fire('Attribute Added Successfully','','success')
    //     this.router.navigate(['menu'])
    //   }
    //   else {
    //     Swal.fire('Fail to Add Attribute','Something Went Wrong','warning')
    //   }
    // })
  }




}
