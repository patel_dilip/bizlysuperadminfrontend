import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRetailfoodAttributeComponent } from './add-retailfood-attribute.component';

describe('AddRetailfoodAttributeComponent', () => {
  let component: AddRetailfoodAttributeComponent;
  let fixture: ComponentFixture<AddRetailfoodAttributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRetailfoodAttributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRetailfoodAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
