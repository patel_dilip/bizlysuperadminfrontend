import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialog } from '@angular/material';
import { AddCuisinesComponent } from 'src/app/PopoversList/add-cuisines/add-cuisines.component';
import { AddTagComponent } from 'src/app/PopoversList/add-tag/add-tag.component';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { element } from 'protractor';


const URL = environment.base_Url + 'category/uploadphotos'

@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.scss']
})
export class UpdateCategoryComponent implements OnInit {

  addCategoryForm: FormGroup
  cuisineCtrl: FormControl
  selectedAlbums = []
  selectedTags = []
  cuisines = []
  tempCuisines = []
  tags = []
  tempTags = []
  uploader: FileUploader
  response: string;
  multiResponse = [];
  userid
  loading = false;
  categoryObject
  categoryId: any
  cusines_data=[];
  // **********CONSTRUCTOR*************************

  constructor(private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService,
    private matDialog: MatDialog,
    private router: Router) {

    //get the cusine object for update
    this.categoryObject = this.addMenuFoodsService.editObject

    console.log("edit obj", this.categoryObject);
    if (this.categoryObject == undefined) {
      this.router.navigateByUrl("/menu")
    }


    //  get cusines FUNCITON CALL

    this.getCuisines()
    // get tags api FUNCITON CALL
    this.getTags()

    // FETCH USER ID FROM LOCAL STORAGE

    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];

    this.addCategory()
    this.addCategoryForm.patchValue({
      categoryName: this.categoryObject.categoryName
    })
    this.selectedAlbums = this.categoryObject.cuisines
    this.selectedTags = this.categoryObject.tags
    this.multiResponse = this.categoryObject.images
    this.categoryId = this.categoryObject._id
  }

  ngOnInit() {

    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'categories'

    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.response = JSON.parse(response)
      console.log('response', this.response);

      this.multiResponse.push(this.response['cuisinePhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      this.loading = false
    }



  }
  // SETLOADING CALL FOR LOADER
  setLoading() {
    this.loading = true
  }

  // get CUISINES API FUNCTION DEFINED
  getCuisines() {this.cusines_data=[]
    this.addMenuFoodsService.getCuisines().subscribe(data => {
      this.cuisines = data['data']
      for (var i = 0; i < this.cuisines.length; i++) {
        for (var j = 0; j < this.categoryObject.cuisines.length; j++) {
          if (this.cuisines[i]._id === this.categoryObject.cuisines[j]._id) {
            this.cuisines.splice(i, 1);
           // this.tempCuisines = this.cuisines
           
             // this.cuisines=this.cusines_data
          }
         
        }
      }
      this.cuisines.forEach(element => {
        if(element.status==true){
        this.cusines_data.push({cuisineName:element['cuisineName'],_id:element['_id']})
       } });
     this.cuisines = this.cusines_data
    })
  }

  // get TAGS API FUNCTION DEFINED
  getTags() {
    this.addMenuFoodsService.getTags().subscribe(data => {
      this.tags = data['data']

      for (var i = 0; i < this.tags.length; i++) {
        for (var j = 0; j < this.categoryObject.tags.length; j++) {
          if (this.tags[i]._id === this.categoryObject.tags[j]._id) {
            this.tags.splice(i, 1);
            this.tempTags = this.tags
          }
        }
      }

      //this.tempTags = data['data']
      console.log('tags', this.tags);

    })
  }


  applyFilterCuisine(event) {
    this.cuisines = []
    this.cuisines = this.cusines_data
   // this.cuisines = this.tempCuisines['cuisineName']
    console.log(event.target.value);
    if (event.target.value == "") {
      this.cuisines = this.cusines_data
    } else {
      const val = event.target.value.trim().toLowerCase();
    const filter = this.cusines_data.filter(function (d) {
      //console.log(d);
      let result
      console.log("ele", d);        
      result = d['cuisineName'].toLowerCase().indexOf(val) !== -1 
  
      return result
    });

    //console.log(filter);
    this.cuisines = filter;
    }
     
   console.log(">>>", this.cuisines);
   
    
  }

  applyFilterTags(event) {
    this.tags = this.tempTags
    console.log(event.target.value);
    
    if (event.target.value == "") {
      this.tags = this.tempTags
    } else {
      const val = event.target.value.trim().toLowerCase();
      const filter = this.tags.filter(function (d) {
        //console.log(d);
        let result
        console.log("ele", d);        
        result = d['tagName'].toLowerCase().indexOf(val) !== -1 
    
        return result
      });
  
      //console.log(filter);
      this.tags = filter;
      }
       
     console.log(">>>", this.tags);
         }
  


  // ******Open dialog box for add new cuines****

  openAddCuisinesDialog(): void {

    console.log('opened Add cuisine dialog');

    const dialogRef = this.matDialog.open(AddCuisinesComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // GET CUISINES FUNCITON CALL to get fresh data after addition
      this.getCuisines()
    });

  }

  //  *************open dialog add tags***************
  // AddTagComponent
  openAddTagDialog(): void {

    console.log('opened Add cuisine dialog');

    const dialogRef = this.matDialog.open(AddTagComponent, {
      width: '400px',
      height: '400px',
      disableClose: true,
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      // GET TAGS FUNCITON CALL to get fresh data after addition
      this.getTags()

    });

  }

  //add cuisines form 
  addCategory() {
    this.addCategoryForm = this.fb.group({
      categoryName: ['', [Validators.required, Validators.pattern('^[0-9a-zA-Z ]+$')]],
      cuisines: [''],
      tags: [''],
      categoryImages: [],
      updatedBy: ['']
    })
  }


  // after selection of cuisines

  onCheckCuisine(s) {
    if (this.cuisines.includes(s)) {
      this.cuisines.splice(this.cuisines.indexOf(s), 1)

      for (var i = 0; i < this.tempCuisines.length; i++) {
        if (this.tempCuisines[i]._id === s._id) {
          this.tempCuisines.splice(i, 1);
        }
      }
      if (this.selectedAlbums.includes(s.cuisine)) {
        alert("Already exists in selection")
      } else {
        this.selectedAlbums.push(s)

      }
    }
    console.log(this.selectedAlbums);


  }


  // ON REMOVE PUB MORE INFOS
  removePubs(s) {
    // full object of cuisines
    if (this.selectedAlbums.includes(s)) {
      this.selectedAlbums.splice(this.selectedAlbums.indexOf(s), 1)
      this.cuisines.push(s);

      console.log('selected Cuisines', this.selectedAlbums)
    }



  }

  // check tags push 
  onCheckTags(s) {
    console.log(s.tagName);

    if (this.tags.includes(s)) {
      this.tags.splice(this.tags.indexOf(s), 1)

      for (var i = 0; i < this.tempTags.length; i++) {
        if (this.tempTags[i]._id === s._id) {
          this.tempTags.splice(i, 1);
        }
      }
      if (this.selectedTags.includes(s)) {
        alert("Already exists in selection")
      } else {
        this.selectedTags.push(s)


      }
    }
    console.log(this.selectedTags);

  }


  // ON REMOVE PUB MORE INFOS
  removeTags(s) {
    if (this.selectedTags.includes(s)) {
      this.selectedTags.splice(this.selectedTags.indexOf(s), 1)
      this.tags.push(s);

    }

  }


  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }



  // **************FINAL POST ADD METHOD CATEGORY********************
  onAddCategory() {

    // ************addCategory Object**************
    let selectedCuisinesIds = []
    let selectedTagsIds = []
    this.selectedAlbums.forEach(element => {
      selectedCuisinesIds.push(element._id)
    })
    this.selectedTags.forEach(element => {
      selectedTagsIds.push(element._id)
    })
    let addCategoryObject = {
      "categoryName": this.addCategoryForm.controls['categoryName'].value,
      "cuisines": selectedCuisinesIds,
      "tags": selectedTagsIds,
      "images": this.multiResponse,
      "userid": this.userid,
      "updatedBy": this.userid
    }

    console.log('addCategoryObject', addCategoryObject);


    // POST ADD CATEGORY API 

    this.addMenuFoodsService.updateCategory(this.categoryId, addCategoryObject).subscribe(addCategoryResponse => {
      console.log('add category response', addCategoryResponse);
      this.loading = false
      if (addCategoryResponse['sucess']) {
        Swal.fire('Category Update Successfully', '', 'success')
        this.router.navigate(['/menu'])
      }
      else {
        Swal.fire('Failed to Update Category', 'Something went wrong', 'warning')
      }
    })

  }




}
