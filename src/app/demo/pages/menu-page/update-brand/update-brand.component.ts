
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialog } from '@angular/material';
import { AddRootDrinkComponent } from 'src/app/PopoversList/add-root-drink/add-root-drink.component';
import { AddDrinkVarientComponent } from 'src/app/PopoversList/add-drink-varient/add-drink-varient.component';
import { AddDrinkSubvarientComponent } from 'src/app/PopoversList/add-drink-subvarient/add-drink-subvarient.component';
import { element } from 'protractor';
import { AddSubSubVarientComponent } from 'src/app/PopoversList/add-sub-sub-varient/add-sub-sub-varient.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-brand',
  templateUrl: './update-brand.component.html',
  styleUrls: ['./update-brand.component.scss']
})
export class UpdateBrandComponent implements OnInit {

  countries: any
  // filter variables
  DrinkTypes: any;
  VarientsArray: any
  liquorVarients: any;
  subVarients: any;
  addBrandForm: FormGroup
  dataArray: any;           // all root type liquor
  subsubVarientArray: any;
  userid
  brandObj: any
  brandID: any
  category_data: any;
  categoryType=[];
  DrinkTree=[];
  subsubFlag=0;
  typeFlag=0;
  subFlag=0;
  count=0;
  id: any;
  ti=0;
  si=0;
  ssi=0;



  constructor(private fb: FormBuilder,
    private addMenuFoodsService: AddMenuFoodsService,
    private mainServ: MainService,
    private matDialog: MatDialog,
    private router: Router,
    private dialog: MatDialog) {

    //get the brand object for update
    this.brandObj = this.addMenuFoodsService.editObject
    
    console.log("edit obj", this.brandObj);
    if (this.brandObj == undefined) {
      this.router.navigateByUrl("/menu")
    }
    this.categoryType=this.brandObj['categoryType']
    this.getRootTypes();

    // ************user id localstorage ******************
    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];


    this.addBrand()
    this.addBrandForm.patchValue({
      brandName: this.brandObj.brandName,
      country: this.brandObj.country,
      categoryType:this.brandObj.categoryType
    })
    this.brandID = this.brandObj._id
  }

  ngOnInit() {
    this.getAllCountries()


  }
  getRootTypes() {

    this.addMenuFoodsService.getAllRootDrinks().subscribe(data => {
      this.DrinkTypes = data['data']

      console.log('drinktypes', this.DrinkTypes);
      this.DrinkTypes.forEach(element => {
        this.categoryType.forEach(ele=>{
        if(element['_id']==ele['_id'])
        element=ele
        
        })
         this.DrinkTree.push(element)
      });
this.DrinkTypes=this.DrinkTree
console.log(this.categoryType);

    })
  }

  // getting all countries name
  getAllCountries() {
    this.mainServ.getAllDialCode().subscribe(countryCode => {
      console.log('countries', countryCode);

      // Array object for countries
      this.countries = countryCode;

    })
    console.log();

  }

  addBrand() {
    this.addBrandForm = this.fb.group({
      brandName: ['', [Validators.required, Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      country: ['', [Validators.required]],
      categoryType:[],
      userid: this.userid
    })
  }

  // *****************************  Liquor tree *******************
  //open tree node and close
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  // **********open root drink dialog**************

  openRootDialog() {
    const dialogRef = this.dialog.open(AddRootDrinkComponent, {
      width: '460px',
      disableClose: true,
      // height: '200px'
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log('After dialog closed');
      this.getRootTypes()
    })
  }

  // ********OPEN VARIENT DRINK DIALOG***************

  openVarientDialog(rootDrinkID) {
    const dialogRef = this.dialog.open(AddDrinkVarientComponent, {
      width: '460px',
      disableClose: true,
      data: rootDrinkID
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'after closing varient drink pop'
      );
      this.getRootTypes()
    })

  }

  //************open sub varient drink dialog box**************

  openSubVarientDialog(rootDrinkID, liquorVarient) {
    const dialogRef = this.dialog.open(AddDrinkSubvarientComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootDrinkID": rootDrinkID, "liquorVarient": liquorVarient }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getRootTypes()
    })
  }

  // **************OPEN SUB SUB VARIENT DIALOG************
  openSubSubVarient(rootDrinkID, liquorVarient, liquorSubVarient) {
    const dialogRef = this.dialog.open(AddSubSubVarientComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootDrinkID": rootDrinkID, "liquorVarient": liquorVarient, "liquorSubVarient": liquorSubVarient }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getRootTypes()
    })
  }

  onAddAlbum() {
    console.log(this.addBrandForm.value);

  }


 

  // *****************Adding Final BRAND ALCOHOL**********
  onAddBrand() {

    console.log('json object', this.addBrandForm.value);
this.addBrandForm.patchValue({
  categoryType:this.categoryType

})
console.log('json object', this.addBrandForm.value);
    this.addMenuFoodsService.updateLiquorBrand(this.brandID, this.addBrandForm.value).subscribe(resAddBrand => {
      if (resAddBrand['sucess']) {
        Swal.fire('Brand Updated Successfully', '', 'success')
        this.router.navigate(['menu'])
      }
      else {
        Swal.fire('Fail to Add Update', 'Something Went Wrong', 'warning')
      }
    })

  }

  changeFlag(data, event, root_level, rootindex?: any, subcategoryIndex?: any, subsubcategoryINdex?: any, typeIndex?: any) {
    //console.log( ">>>" , data, ">>>" , event.checked);
    this.count=0
    console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
    if (root_level == 'root') {
      this.DrinkTypes.forEach((element, index) => {
        if (index == rootindex) {
          element.showChildren = event.checked
          if (
            this.DrinkTypes[index].hasOwnProperty('liquorVarients')
            &&
            this.DrinkTypes[index].liquorVarients.length > 0
          ) {
            this.DrinkTypes[index].liquorVarients.forEach((element, subcategoryIndex = index) => {
              element.showChildren = event.checked;
              if (
                this.DrinkTypes[index].liquorVarients[subcategoryIndex].hasOwnProperty('liquorSubVarients')
                &&
                this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.forEach((element, subsubcategoryINdex = index) => {
                  element.showChildren = event.checked;
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                      element.showChildren = event.checked;
                    })
                  }


                })
              }
            })
          }
        }
      });
      //console.log('this.DrinkTypes >', this.DrinkTypes)
    }
    else if (root_level == 'sub') {
      this.DrinkTypes.forEach((Rootelement, index) => {
        if (index == rootindex) {
          // Rootelement.showChildren = event.checked
          if (
            this.DrinkTypes[index].hasOwnProperty('liquorVarients')
            &&
            this.DrinkTypes[index].liquorVarients.length > 0
          ) {
            this.DrinkTypes[index].liquorVarients.forEach((subelement, subindex) => {
              if (subindex == subcategoryIndex) {
                subelement.showChildren = event.checked;
                if (
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].hasOwnProperty('liquorSubVarients')
                  &&
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.forEach((subsubelement, subsubcategoryINdex = index) => {
                    subsubelement.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length

                      this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.forEach((typeelement, liquorSubSubVarientTypeIndex = index) => {
                        typeelement.showChildren = event.checked;
                      })
                    }


                  })
                }
              }

            })

          }


        }
      });
      //console.log('this.DrinkTypes >', this.DrinkTypes)
    } else if (root_level == 'subsub') {
      this.DrinkTypes.forEach((element, index) => {
        if (index == rootindex) {
          // element.showChildren = event.checked
          if (
            this.DrinkTypes[index].hasOwnProperty('liquorVarients')
            &&
            this.DrinkTypes[index].liquorVarients.length > 0
          ) {
            this.DrinkTypes[index].liquorVarients.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                //             element.showChildren = event.checked;
                if (
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].hasOwnProperty('liquorSubVarients')
                  &&
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      element.showChildren = event.checked;
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                          element.showChildren = event.checked;
                        })
                      }
                    }

                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.DrinkTypes >', this.DrinkTypes)
    } else if (root_level == 'type') {
      this.DrinkTypes.forEach((element, index) => {
        if (index == rootindex) {
          if (
            this.DrinkTypes[index].hasOwnProperty('liquorVarients')
            &&
            this.DrinkTypes[index].liquorVarients.length > 0
          ) {
            this.DrinkTypes[index].liquorVarients.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                if (
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].hasOwnProperty('liquorSubVarients')
                  &&
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.forEach((element, childIndex) => {
                          if (typeIndex == childIndex)
                            element.showChildren = event.checked;
                        })
                      }


                    }
                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.DrinkTypes >', this.DrinkTypes)
    }
    ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////

    this.DrinkTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.DrinkTypes[index].hasOwnProperty('liquorVarients')
          &&
          this.DrinkTypes[index].liquorVarients.length > 0
        ) {
          this.DrinkTypes[index].liquorVarients.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.DrinkTypes[index].liquorVarients[subindex].hasOwnProperty('liquorSubVarients')
                &&
                this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.forEach((subsubelement, subsubINdex) => {
                  // if(subsubINdex==0){
                  //   this.subsubFlag=0
                  // }
                  if (subsubINdex == subsubcategoryINdex) {
                    if (subsubelement.showChildren == true) {
                      this.subsubFlag++
                    }
                    //console.log("<<<", this.DrinkTypes[index].subCategories[subcategoryIndex].liquorSubVarients.length);
                    //console.log(">>>", this.subsubFlag);


                    if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length == this.subsubFlag) {
                      subelement.showChildren = true
                    } else {
                      subelement.showChildren = false;
                    }
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].hasOwnProperty('liquorSubSubVarientType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      // //console.log(element);
                      this.typeFlag = 0
                      this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.forEach((typeelement, childIndex) => {
                        // if (typeIndex == childIndex) {
                        //   typeelement.showChildren = event.checked;
                        // }
                        if (typeelement.showChildren == true) {
                          this.typeFlag++
                        }
                        console.log("<<<", this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length);
                        console.log(">>>", this.typeFlag);


                        if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length == this.typeFlag) {
                          subsubelement.showChildren = true
                        } else {
                          subsubelement.showChildren = false;
                        }

                      })
                    }


                  }
                })
              }
              // }
            }
          })
        }


      }
    });
    //subsub
    this.subsubFlag = 0

    this.DrinkTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.DrinkTypes[index].hasOwnProperty('liquorVarients')
          &&
          this.DrinkTypes[index].liquorVarients.length > 0
        ) {
          this.DrinkTypes[index].liquorVarients.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {

              if (
                this.DrinkTypes[index].liquorVarients[subindex].hasOwnProperty('liquorSubVarients')
                &&
                this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.forEach((subsubelement, subsubINdex) => {

                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  // //console.log("<<<", this.DrinkTypes[index].subCategories[subcategoryIndex].liquorSubVarients.length);
                  // //console.log(">>>", this.subsubFlag);


                  if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].hasOwnProperty('liquorSubSubVarientType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    // this.typeFlag=0
                    this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.forEach((typeelement, childIndex) => {



                    })
                  }
                })
              }

            }
          })
        }


      }
    });

    //////subflag
    this.subFlag = 0
    this.subsubFlag = 0

    this.DrinkTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.DrinkTypes[index].hasOwnProperty('liquorVarients')
          &&
          this.DrinkTypes[index].liquorVarients.length > 0
        ) {
          this.DrinkTypes[index].liquorVarients.forEach((subelement, subindex) => {
            // if(subindex==subcategoryIndex){
            if (subelement.showChildren == true) {
              this.subFlag++
            }
            // console.log("<<<", this.DrinkTypes[index].liquorVarients.length);
            // console.log(">>>", this.subFlag);


            if (this.DrinkTypes[index].liquorVarients.length == this.subFlag) {
              Rootelement.showChildren = true
            } else {
              Rootelement.showChildren = false;
            }

            if (
              this.DrinkTypes[index].liquorVarients[subindex].hasOwnProperty('liquorSubVarients')
              &&
              this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.forEach((subsubelement, subsubINdex) => {


                if (
                  // tslint:disable-next-line: max-line-length
                  this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].hasOwnProperty('liquorSubSubVarientType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.forEach((typeelement, childIndex) => {



                  })
                }
              })
            }

            // }
          })
        }


      }
    });
    console.log(this.DrinkTypes[rootindex]);
  //   if (this.categoryType.length == 0)  {
  //     if (this.DrinkTypes[rootindex].showChildren == true) {
  //       this.categoryType.push(this.DrinkTypes[rootindex])
  //     }
  //   }
  //  else{
  //   if (this.DrinkTypes[rootindex].showChildren == true) {
  //     this.categoryType.push(this.DrinkTypes[rootindex])
  //   }
  //  }
  // 
  if (this.DrinkTypes[rootindex].showChildren == false) {
    // element.showChildren = event.checked
    if (
      this.DrinkTypes[rootindex].hasOwnProperty('liquorVarients')
      &&
      this.DrinkTypes[rootindex].liquorVarients.length > 0
    ) {
      this.DrinkTypes[rootindex].liquorVarients.forEach((element, subindex) => {
       if( element.showChildren == false){
        if (
          this.DrinkTypes[rootindex].liquorVarients[subindex].hasOwnProperty('liquorSubVarients')
          &&
          this.DrinkTypes[rootindex].liquorVarients[subindex].liquorSubVarients.length > 0
        ) {
          // tslint:disable-next-line: max-line-length
          this.DrinkTypes[rootindex].liquorVarients[subindex].liquorSubVarients.forEach((element, subsubINdex) => {
            if( element.showChildren == false){
            if (
              // tslint:disable-next-line: max-line-length
              this.DrinkTypes[rootindex].liquorVarients[subindex].liquorSubVarients[subsubINdex].hasOwnProperty('liquorSubSubVarientType')
              &&
              // tslint:disable-next-line: max-line-length
              this.DrinkTypes[rootindex].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              //console.log(element);
              this.DrinkTypes[rootindex].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.forEach((element, liquorSubSubVarientTypeIndex) => {
              if( element.showChildren == true)
              {               
                this.count=1;
                return false
              }
              })
            }


          }
          else{
            this.count=1;
            return false;
          }
          })
        }
      }
      if(element.showChildren==true){
      this.count=1;
      return false;
      }
      })
    }
  }
  else{
    this.count=1
  }
 
console.log(this.count);
let id
if(this.count==1){
if(this.categoryType.length==0){
this.categoryType.push(this.DrinkTypes[rootindex])
}else{
this.categoryType.forEach(element => {
  if(element['_id']==this.DrinkTypes[rootindex]['_id']){
    element=this.DrinkTypes[rootindex]
    this.id=element['_id'];
  }
});
if(this.id!=this.DrinkTypes[rootindex]['_id']){
  this.categoryType.push(this.DrinkTypes[rootindex])
}
}
}
if(this.count==0){
let i=0;
this.categoryType.forEach(element => {
 
 if(element['_id']==this.DrinkTypes[rootindex]['_id']){
  this.categoryType.splice(i, 1)
 }else{
   console.log("not match");
   
 }
 i++;
});
}
console.log(this.categoryType);
this.intermediate(data, event, root_level, rootindex = rootindex, subcategoryIndex = subcategoryIndex, subsubcategoryINdex = subsubcategoryINdex, typeIndex = typeIndex)

     }
       //////////////////////////////////////interminate ////////////////////////////////////////////
   intermediate(data, event, root_level, rootindex, subcategoryIndex, subsubcategoryINdex, typeIndex?: any) {
    ///////////////////////////////////////// intermediate ////////////////////////////////////
    console.log(data, "<", event, "<", root_level, "<", rootindex, "<", subcategoryIndex, "<", subsubcategoryINdex, "<", typeIndex);
    // this.ri = 0
    // this.si = 0
    // this.ssi = 0
    
      this.ti = 0
  
   
      this.DrinkTypes.forEach((Rootelement, index) => {

        if (index == rootindex) {
         
          console.log("sub length:", this.DrinkTypes[rootindex].liquorVarients.length);
  
          if (
            this.DrinkTypes[index].hasOwnProperty('liquorVarients')
            &&
            this.DrinkTypes[index].liquorVarients.length > 0
          ) {
            this.si=0
            this.DrinkTypes[index].liquorVarients.forEach((subelement, subindex) => {
              // debugger;
              if (subelement['showChildren'] == true) {
                this.si++
              }
              if (subindex == subcategoryIndex) {
               
                if (
                  this.DrinkTypes[index].liquorVarients[subindex].hasOwnProperty('liquorSubVarients')
                  &&
                  this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
  // this.ssi=0
  // debugger;
  this.ssi=0

  
                  this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.forEach((subsubelement, subsubINdex) => {
                    if (subsubelement['showChildren'] == true) {
                      this.ssi++
                    }
                        
                    if (subsubINdex == subsubcategoryINdex) {
 
                      // ////console.log("<<<", this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.length);
                      // ////console.log(">>>", this.subsubFlag);
                     
  
  
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].hasOwnProperty('liquorSubSubVarientType')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        // ////console.log(element);
                        // this.typeFlag=0
                        this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.forEach((typeelement, childIndex) => {
                          // if(typeIndex==childIndex){
                          if (typeelement['showChildren'] == true) {
                            this.ti++
                          }
                          console.log(this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length, "type>>", this.ti);
  
                          if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > this.ti) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subsubelement.inter = true;
                          } else if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length == this.ti) {
  
                            subsubelement.inter = false;
                            subsubelement.showChildren = true;
                          }
                        //  if (this.ti== 0) {
                        //   subsubelement.inter = false;
                        //   subsubelement.showChildren = false;
                        // }
  
                          if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > this.ssi) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subelement.inter = true;
                          } else if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length == this.ssi) {
  
                            subelement.inter = false;
                            subelement.showChildren = true;
                          }
                          
                          if (this.DrinkTypes[index].liquorVarients.length > this.si) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subelement.inter = true;
                          } else if (this.DrinkTypes[index].liquorVarients.length == this.si) {
                            Rootelement.inter = false;
                            Rootelement.showChildren = true;
                          }
                        
                          //  }
                        })
                        if (this.ti== 0) {
                          subsubelement.inter = false;
                          subsubelement.showChildren = false;
                        }
                      }
                      
                     
                            }
                            console.log(this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length, "subsub>>", this.ssi);
  
                            if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > this.ssi) {
                              // Rootelement.inter=true;
                              // subelement.inter=true;
                              subelement.inter = true;
                            } else if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length == this.ssi) {
        
                              subelement.inter = false;
                              subelement.showChildren = true;
                            } 
                           
                           
                  })
                  if (this.ssi== 0 && this.ti==0) {
                      subelement.inter = false;
                      subelement.showChildren = false;
                    }
                }
  
            
          
                
                console.log(this.DrinkTypes[index].liquorVarients.length, "sub>>", this.si);
  
               
                 }
                 if (this.DrinkTypes[index].liquorVarients.length > this.si) {
                  // Rootelement.inter=true;
                  // subelement.inter=true;
                  Rootelement.inter = true;
                } else if (this.DrinkTypes[index].liquorVarients.length == this.si) {
  
                  Rootelement.inter = false;
                  Rootelement.showChildren = true;
                } 
               
             })
             if (this.ssi== 0 && this.ti==0 && this.si==0) {
              Rootelement.inter = false;
              Rootelement.showChildren = false;
            }
          }
  
  
        }
      });
   
}

}
