import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-create-attribute-set',
  templateUrl: './create-attribute-set.component.html',
  styleUrls: ['./create-attribute-set.component.scss']
})
export class CreateAttributeSetComponent implements OnInit {
  createAttributeSetForm : FormGroup
  units:string[]=['Gram',"Kg"]
    constructor(public dialogRef: MatDialogRef<CreateAttributeSetComponent>,
      private fb:FormBuilder) { }
  
    ngOnInit() {
      this.createAttributeSetForm=this.fb.group({
        attributeSetName:[''],
        basedOn:['']
  
      })
    }
    onNoClick(): void {
      this.dialogRef.close();
    }
    createAttributeSet(){
      console.log(this.createAttributeSetForm.value);
      
    }
  }
  