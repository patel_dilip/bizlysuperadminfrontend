import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAttributeSetComponent } from './create-attribute-set.component';

describe('CreateAttributeSetComponent', () => {
  let component: CreateAttributeSetComponent;
  let fixture: ComponentFixture<CreateAttributeSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAttributeSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAttributeSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
