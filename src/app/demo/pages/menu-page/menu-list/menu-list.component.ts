import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, MatSort, MatDialog, matTooltipAnimations, MatTabChangeEvent } from '@angular/material';
import { MainService } from 'src/app/_services/main.service';
import * as _ from "lodash";
import { AddIngredientsComponent } from '../add-ingredients/add-ingredients.component';
import { AddItemsComponent } from '../add-items/add-items.component';
import { AddEquipmentComponent } from '../add-equipment/add-equipment.component';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { ViewCuisineComponent } from '../view-cuisine/view-cuisine.component';
import { ViewCategoryComponent } from '../view-category/view-category.component';
import { ViewdishComponent } from '../viewdish/viewdish.component';
import { ViewAddOnComponent } from '../view-add-on/view-add-on.component';
import { ViewVarientComponent } from '../view-varient/view-varient.component';
import { Router } from '@angular/router';
import { AddLiquorAttributeGroupComponent } from '../add-liquor-attribute-group/add-liquor-attribute-group.component';
import { AddLiquorAttributeSetComponent } from '../add-liquor-attribute-set/add-liquor-attribute-set.component';
import { AddBeverageBrandComponent } from '../add-beverage-brand/add-beverage-brand.component';
import { AddRetailfoodBrandComponent } from '../add-retailfood-brand/add-retailfood-brand.component';
import { AddRootDrinkComponent } from 'src/app/PopoversList/add-root-drink/add-root-drink.component';
import { AddDrinkVarientComponent } from 'src/app/PopoversList/add-drink-varient/add-drink-varient.component';
import { AddDrinkSubvarientComponent } from 'src/app/PopoversList/add-drink-subvarient/add-drink-subvarient.component';
import { AddSubSubVarientComponent } from 'src/app/PopoversList/add-sub-sub-varient/add-sub-sub-varient.component';
import { ViewBrandComponent } from '../view-brand/view-brand.component';
import { ViewProductComponent } from '../view-product/view-product.component';
import { ViewLiquorAttributeComponent } from '../view-liquor-attribute/view-liquor-attribute.component';
import Swal from 'sweetalert2';
import { ViewLiquorAttributeSetComponent } from '../view-liquor-attribute-set/view-liquor-attribute-set.component';
import { RootCategoryAssetInventoryComponent } from '../asset-Inventory-category-dailogs/root-category-asset-inventory/root-category-asset-inventory.component';
import { SubSubCategoryTypeAssetInventoryComponent } from '../asset-Inventory-category-dailogs/sub-sub-category-type-asset-inventory/sub-sub-category-type-asset-inventory.component';
import { SubSubCategoryAssetInventoryComponent } from '../asset-Inventory-category-dailogs/sub-sub-category-asset-inventory/sub-sub-category-asset-inventory.component';
import { SubCategoryAssetInventoryComponent } from '../asset-Inventory-category-dailogs/sub-category-asset-inventory/sub-category-asset-inventory.component';
import { RootCategoryFoodInventoryComponent } from '../food-inventory-category-dailogs/root-category-food-inventory/root-category-food-inventory.component';
import { SubCategoryFoodInventoryComponent } from '../food-inventory-category-dailogs/sub-category-food-inventory/sub-category-food-inventory.component';
import { SubSubCategoryFoodInventoryComponent } from '../food-inventory-category-dailogs/sub-sub-category-food-inventory/sub-sub-category-food-inventory.component';
import { SubSubCategoryTypeFoodInventoryComponent } from '../food-inventory-category-dailogs/sub-sub-category-type-food-inventory/sub-sub-category-type-food-inventory.component';
import { ViewIngredientFoodInventoryComponent } from '../view-ingredient-food-inventory/view-ingredient-food-inventory.component';
import { ViewEquipmentAssetInventoryComponent } from '../view-equipment-asset-inventory/view-equipment-asset-inventory.component';
import { ViewItemsFoodInventoryComponent } from '../view-items-food-inventory/view-items-food-inventory.component';
import { element } from 'protractor';
import { UpdateAttributeGroupSetComponent } from '../update-attribute-group-set/update-attribute-group-set.component';
import { UpdateAttributeGroupComponent } from '../update-attribute-group/update-attribute-group.component';
import { RootCategoryBeverageComponent } from '../beverages-category-dailogs/root-category-beverage/root-category-beverage.component';
import { SubCategoryBeverageComponent } from '../beverages-category-dailogs/sub-category-beverage/sub-category-beverage.component';
import { SubSubCategoryBeverageComponent } from '../beverages-category-dailogs/sub-sub-category-beverage/sub-sub-category-beverage.component';
import { SubSubCategoryTypeBeverageComponent } from '../beverages-category-dailogs/sub-sub-category-type-beverage/sub-sub-category-type-beverage.component';
import { RootCategoryRetailFoodComponent } from '../retail-food-category-dailogs/root-category-retail-food/root-category-retail-food.component';
import { SubCategoryRetailFoodComponent } from '../retail-food-category-dailogs/sub-category-retail-food/sub-category-retail-food.component';
import { SubSubCategoryRetailFoodComponent } from '../retail-food-category-dailogs/sub-sub-category-retail-food/sub-sub-category-retail-food.component';
import { SubSubCategoryTypeRetailFoodComponent } from '../retail-food-category-dailogs/sub-sub-category-type-retail-food/sub-sub-category-type-retail-food.component';
import { ViewBeverageBrandComponent } from '../view-beverage-brand/view-beverage-brand.component';
import { UpdateBeverageBrandComponent } from '../update-beverage-brand/update-beverage-brand.component';
import { ViewBeverageProductComponent } from '../view-beverage-product/view-beverage-product.component';
import { UpdateRetailFoodBrandComponent } from '../update-retail-food-brand/update-retail-food-brand.component';
import { ViewBeverageComponent } from '../view-beverage/view-beverage.component';
import { forkJoin } from 'rxjs';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { LoginService } from 'src/app/_services/adminLogin/login.service';
import { map, subscribeOn, startWith, filter } from 'rxjs/operators';


// ******************************************* Food ************************************************
export interface PeriodicElement {
  cuisineCode: string;
  cuisinename: string;
  country: string;
  state: string;
  city: string;
  combinationOfCusines: string;
  updatedAt: string;
  addedBy: string
}
export interface CategoryElement {
  categoryCode: string;
  categoryName: string;
  cuisines: string;
  tags: string;
  updatedAt: string;
  addedBy: string

}
export interface DishElement {
  dishCode: string;
  dishName: string;
  cuisines: string;
  varients: string;
  updatedAt: string;
  addedBy: string;
  veg_nonveg: string;
  unit: string;
}

export interface addOnElement {
  adonCode: string;
  adonName: string;
  dishes: string;
  updatedAt: string;
  addedBy: string;
  veg_nonveg: string;
  unit: string;
}

export interface varientsElement {
  varientCode: string;
  varientName: string;
  updatedAt: string;
  addedBy: string;
  varientContent: string;
}


// *************************************** Liquor *********************************************

export interface liquorBrandElement {
  brandCode: string;
  brandName: string;
  country: string;
  rootDrinkType: string;
  liquorVarient: string;
  addedBy: string;
  updatedAt: string;
  status: boolean
}

export interface liquorProductElement {
  productCode: string;
  productName: string;
  varient: string;
  brand: string;
  attribute: string;
  addedby: string;
  updatedAt: string;
  status: boolean;
}

export interface liquorAttributeElement {
  attributeCode: string;
  attributeName: string;
  responseType: string;
  searchable: boolean;
  filterable: boolean;
  status: boolean;
  addedBy: string;
  updatedAt: string;
}

export interface liquorAttributeSetsElement {
  attributeSetCode: string;
  attributeSetName: string;
  assignGroups: string;
  status: string
  addedBy: string;
  updatedAt: string;
}
// ****************************************** Food Inventory ***************************************

export interface ingredientElement {
  ingredientName: string;
  ingredientCode: string;
  ingredientcategory: string;
  unit: string;
  status: string;
  addedBy: string;
  updatedAt: string
}

export interface inputElement {
  itemCode: string;
  itemName: string;
  brand: string;
  ingredient: string;
  quantity: string;
  addedBy: string;
  updatedAt: string;
}

// ********************************** Asset Inventory *************************************

export interface equipmentElement {
  equipmentCode: string;
  equipmentName: string;
  category: string;
  assignattributeSet: string;
  addedBy: string;
  updatedAt: string;
}


// *************************************** Beverages *************************************

export interface beverageElement {
  beverageCode: string;
  beverageName: string;
  unit: string;
  veg_nonveg: string;
  rootCategory: string;
  subVarient: string;
  varients: string;
  type: string;
  addedBy: string;
  updatedAt: string;
}

export interface beverageBrandElement {
  brandCode: string;
  brandName: string;
  country: string;
  addedBy: string;
  updatedAt: string;
  status: boolean;
}

export interface beverageProductElement {
  productCode: string;
  productName: string;
  varient: string;
  associatedBrand: string;
  addedBy: string;
  updatedAt: string;
  status: boolean;
}

// ************************************* Retail Food *****************************************

export interface retailFoodAttributeElement {
  _id: string;
  attributeName: string;
  responseType: string;
  searchable: boolean;
  filterable: boolean;
  status: boolean;
  addedBy: string;
  updatedAt: string;
}




@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss']
})
export class MenuListComponent implements OnInit {
  brandfilter = { brand: [], country: [], root: [], addedby: [], status: [] }
  productfilter = { brand: [], root: [], addedby: [], status: [] }
  cusinesfilter = { country1: [], cuisine1: [], status: [] }
  categoryfilter = { cuisine: [], tag: [], addedby: [], status: [] }
  dishfilter = { Egg: true, NonVeg: true, SeaFood: true, Veg: true, all: true, addedby: [], cuisine: [], status: [] }
  addonfilter = { Egg: true, NonVeg: true, SeaFood: true, Veg: true, all: true, addedby: [], dish: [], status: [] }
  varientfilter = { addedby: [], dish: [], status: [] }
  arrtibutefilter = { filter: [], search: [], addedby: [], status: [] }
  arrtibuteSetfilter = { addedby: [], status: [] }


  Egg: Boolean
  NonVeg: Boolean
  SeaFood: Boolean
  Veg: Boolean

  //********************************************  Food  ************************************/

  //cusines table
  displayedColumns: string[] = ['cuisineCode', 'cuisineName', 'country', 'state', 'city', 'combinationOfCusines', 'updatedAt', 'addedBy', 'status', 'action'];
  dataSource: MatTableDataSource<PeriodicElement>;
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("sort", { static: true }) sort: MatSort;

  //category table
  displayedColumnsCategory: string[] = ['categoryCode', 'categoryName', 'cuisines', 'tags', 'updatedAt', 'addedBy', 'status', 'action'];
  dataSourceCategory: MatTableDataSource<CategoryElement>;
  @ViewChild("categorypaginator", { static: true }) categorypaginator: MatPaginator;
  @ViewChild("categorysort", { static: true }) categorysort: MatSort;

  //dishes table
  displayedColumnsDishes: string[] = ['dishCode', 'dishName', 'cuisines', 'varients', 'veg_nonveg', 'unit', 'updatedAt', 'addedBy', 'status', 'action'];
  dataSourceDish: MatTableDataSource<DishElement>;
  @ViewChild("dishpaginator", { static: true }) dishpaginator: MatPaginator;
  @ViewChild("dishsort", { static: true }) dishsort: MatSort;

  //add on table
  displayedColumnsAddOn: string[] = ['adonCode', 'adonName', 'dishes', 'veg_nonveg', 'unit', 'updatedAt', 'addedBy', 'status', 'action'];
  dataSourceAddOn: MatTableDataSource<addOnElement>;
  @ViewChild("addonpaginator", { static: true }) addonpaginator: MatPaginator;
  @ViewChild("addonsort", { static: true }) addonsort: MatSort;

  //varients table
  displayColumnsVarientas: string[] = ['varientCode', 'varientName', 'varientContent', 'updatedAt', 'addedBy', 'status', 'action'];
  dataSourceVarients: MatTableDataSource<varientsElement>;
  @ViewChild("varientpaginator", { static: true }) varientpaginator: MatPaginator;
  @ViewChild("varientsort", { static: true }) varientsort: MatSort;


  // ************************************* Liquor **************************************8

  //liquorBrand_DATA
  displayColumnsliquorBrand: string[] = ['brandCode', 'brandName', 'country', 'rootDrinkType', 'updatedAt', 'addedBy', 'status', 'action'];
  dataSourceLiquorBrand: MatTableDataSource<liquorBrandElement>;
  @ViewChild("liquorBrandpaginator", { static: true }) liquorBrandpaginator: MatPaginator;
  @ViewChild("liquorBrandsort", { static: true }) liquorBrandsort: MatSort;

  //liquorProduct_DATA
  displayColumnsliquorProduct: string[] = ['productCode', 'productName', 'varient', 'brand', 'attribute', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceLiquorProduct: MatTableDataSource<liquorProductElement>;
  @ViewChild("liquorProductpaginator", { static: true }) liquorProductpaginator: MatPaginator;
  @ViewChild("liquorProductsort", { static: true }) liquorProductsort: MatSort;

  //liquorAttribute_DATA: liquorAttributeElement
  displayColumnsliquorAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceLiquorAttribute: MatTableDataSource<liquorAttributeElement>
  @ViewChild("liquorAttributepaginator", { static: true }) liquorAttributepaginator: MatPaginator;
  @ViewChild("liquorAttributesort", { static: true }) liquorAttributesort: MatSort;


  //liquorAttributeSetsElement
  displayColumnsLiquorAttributeSet: string[] = ['attributeSetCode', 'attributeSetName', 'assignGroups', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceLiquorAttributeSet: MatTableDataSource<liquorAttributeSetsElement>
  @ViewChild("liquorAttributeSetpaginator", { static: true }) liquorAttributeSetpaginator: MatPaginator;
  @ViewChild("liquorAttributeSetSort", { static: true }) liquorAttributeSetSort: MatSort


  //********************************************  Food Inventory **************************************/

  //ingredient_DATA: ingredientElement
  displayColumnsingredient: string[] = ['ingredientCode', 'ingredientName', 'ingredientcategory', 'unit', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceIngredient: MatTableDataSource<ingredientElement>
  @ViewChild("ingredientpaginator", { static: true }) ingredientpaginator: MatPaginator;
  @ViewChild("ingredientsort", { static: true }) ingredientsort: MatSort;

  //input_DATA: inputElement
  displayColumnsInputData: string[] = ['itemCode', 'itemName', 'brand', 'ingredient', 'quantity', 'updatedAt', 'addedBy', 'action']
  dataSourceInput: MatTableDataSource<inputElement>
  @ViewChild("inputpaginator", { static: true }) inputpaginator: MatPaginator;
  @ViewChild("inputsort", { static: true }) inputsort: MatSort;


  // ****************************************** Asset Inventory *****************************************
  //equipment_DATA: equipmentElement
  displayColumnsequipment: string[] = ['equipmentCode', 'equipmentName', 'category', 'assignattributeSet', 'updatedAt', 'addedBy', 'action']
  dataSourceEquipment: MatTableDataSource<equipmentElement>
  @ViewChild("equipmentpaginator", { static: true }) equipmentpaginator: MatPaginator;
  @ViewChild("equipmentsort", { static: true }) equipmentsort: MatSort;


  // ***************************************** Beverages *******************************************

  //beverageDATA
  displayColumnsBeverage: string[] = ['beverageCode', 'beverageName', 'varients', 'subVarient', 'type', 'unit', 'veg_nonveg', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceBeverage: MatTableDataSource<beverageElement>
  @ViewChild("beveragepaginator", { static: true }) beveragepaginator: MatPaginator;
  @ViewChild("beverageSort", { static: true }) beverageSort: MatSort

  //beverageAttributeElement
  displayColumnsBeverageAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceBeverageAttribute: MatTableDataSource<liquorAttributeElement>
  @ViewChild("beverageAttributepaginator", { static: true }) beverageAttributepaginator: MatPaginator;
  @ViewChild("beverageAttributesort", { static: true }) beverageAttributesort: MatSort;


  //beverages attribute sets
  displayColumnsBeverageAttributeSet: string[] = ['attributeSetCode', 'attributeSetName', 'assignGroups', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceBeverageAttributeSet: MatTableDataSource<liquorAttributeSetsElement>
  @ViewChild("BeverageAttributeSetpaginator", { static: true }) BeverageAttributeSetpaginator: MatPaginator;
  @ViewChild("BeverageAttributeSetSort", { static: true }) BeverageAttributeSetSort: MatSort


  // beverage brands
  displayColumnsBeverageBrands: string[] = ['brandCode', 'brandName', 'country', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceBeverageBrands: MatTableDataSource<beverageBrandElement>
  @ViewChild("beverageBrandspaginator", { static: true }) beverageBrandspaginator: MatPaginator;
  @ViewChild("beverageBrandsSort", { static: true }) beverageBrandsSort: MatSort

  // beverage products
  displayColumnsBeverageProducts: string[] = ['productCode', 'productName', 'varient', 'associatedBrand', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceBeverageProduct: MatTableDataSource<beverageProductElement>
  @ViewChild("beverageProductspaginator", { static: true }) beverageProductspaginator: MatPaginator;
  @ViewChild("beverageProductsSort", { static: true }) beverageProductsSort: MatSort


  // ******************************************* Retail Food *****************************************

  //retailFoodAttributeElement
  displayColumnsretailFoodAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceRetailFoodAttribute: MatTableDataSource<liquorAttributeElement>
  @ViewChild("retailFoodAttributepaginator", { static: true }) retailFoodAttributepaginator: MatPaginator;
  @ViewChild("retailFoodAttributesort", { static: true }) retailFoodAttributesort: MatSort;


  //retail food attribute sets
  displayColumnsRetailFoodAttributeSet: string[] = ['attributeSetCode', 'attributeSetName', 'assignGroups', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceRetailFoodAttributeSet: MatTableDataSource<liquorAttributeSetsElement>
  @ViewChild("RetailFoodAttributeSetpaginator", { static: true }) RetailFoodAttributeSetpaginator: MatPaginator;
  @ViewChild("RetailFoodAttributeSetSort", { static: true }) RetailFoodAttributeSetSort: MatSort

  // retail food brands
  displayColumnsRetailFoodBrands: string[] = ['brandCode', 'brandName', 'country', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceRetailFoodBrands: MatTableDataSource<beverageBrandElement>
  @ViewChild("retailFoodBrandspaginator", { static: true }) retailFoodBrandspaginator: MatPaginator;
  @ViewChild("retailFoodBrandsSort", { static: true }) retailFoodBrandsSort: MatSort

  // retail food products
  displayColumnsRetailFoodProducts: string[] = ['productCode', 'productName', 'varient', 'associatedBrand', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceRetailFoodProduct: MatTableDataSource<beverageProductElement>
  @ViewChild("retailFoodProductspaginator", { static: true }) retailFoodProductspaginator: MatPaginator;
  @ViewChild("retailFoodProductsSort", { static: true }) retailFoodProductsSort: MatSort


  firstLevel: any

  //liquor > categories > show categories
  liquorSize = 12
  showLiquorCategories = false

  //ingredient > Categories > show categories
  size = 12
  showIngredientCategories = false

  // asset inventory > categories > show categories
  inputsize = 12
  showInputCategories = false

  //beverages > categories > show categories
  beverageSize = 12
  showBeverageCategories = false

  //retail food > categories > show categories
  retailFoodSize = 12
  showRetailFoodCategories = false


  showCuisineTable = false
  showCategoryTable = false
  showDishTable = false
  showAddonTable = false
  showVarientTable = false

  //un assigned attribute 
  allLiquorAttribute = []
  allBeverageUnassignedAttribute = []
  allRetailFoodUnassignedAttribue = []


  // attribute groups
  allLiquorAttributeGroup = []
  allBeverageAttributeGroups = []
  allRetailFoodAttributeGroups = []


  allDrinks: any
  allAssetInventoryCategory: any
  allFoodInventoryCategory: any
  allInHouseBeveragesCategories: any
  allRetailBeveragesCategories: any
  allRetailFoodCategories: any

  activeTab: any
  activeTabBeverage: any
  activeTabRetailFood: any

  cuisineCount = 0
  dishesCount = 0
  categoryCount = 0
  addonCount = 0
  varientCount = 0
  liquorBrandCount = 0
  liquorProductCount = 0
  liquorAttributeCount = 0
  assetInventoryAttributeCount = 0
  beverageProductCount = 0
  beverageBrandCount = 0
  retailFoodProductCount = 0
  retailFoodBrandCount = 0
  all: any
  cuisine: any
  category: any
  dish: any
  addon: any
  varient: any
  foodAllCount: number
  alld = true
  // allMenuData: any

  storeData: any;
  fileUploaded: File;
  worksheet: any;
  jsonData: any;
  csvData: any;
  userid: any

  date: any

  //export excel file 
  cusineExportJSONDATA = []
  categoryExportJSONDATA = []
  dishExportJSONDATA = []
  addonExportJSONDATA = []
  varientExportJSONDATA = []
  liquorBrandJSONDATA = []
  liquorProductJSONDATA = []
  liquorAttributeJSONDATA = []
  beverageAttributeJSONDATA = []
  retailFoodAttributeJSONDATA = []
  liquorAttributeSetJSONDATA = []
  beverageAttributeSetJSONDATA = []
  retailFoodAttributeSetJSONDATA = []
  ingredientJSONDATA = []
  itemsJSONDATA = []
  equipmentJSONDATA = []
  beverageJSONDATA = []
  beverageProductJSONDATA = []
  beveraeBrandJSONDATA = []
  retailFoodProductJSONDATA = []
  retailFoodBrandJSONDATA = []
  noData: any;
  noData1: any;
  noData2: any;
  noData3: any;
  noData4: any;
  noData5: any;
  noData6: any;
  noData7: any;
  noData8: any;
  noData9: any;
  noData10: any;
  noData11: any;
  noData12: any;
  noData13: any;
  noData14: any;
  noData15: any;
  noData16: any;
  noData17: any;
  noData18: any;
  retail_food_product: any;
  cuisine_data: any;
  category_data: any;
  dish_data: any;
  varients_data: any;
  addon_data: any;
  varient_data: any;
  liquorbrand_data: any;
  liquorproduct_data: any;
  liquorattribute_data: any;
  liquorattributeset_data: any
  beverage_data: any;
  beverageproduct_data: any;
  beverageBrand_data: any;
  beverageattributeset_data: any
  retailfoodattributeset_data: any;
  beverageattribute_data: any;
  retailFoodattribute_data: any;
  retailfoodbrand_data: any;
  noData19: any;
  noData20: any;
  rootDrinkType = [];
  allCountryStateCity: any;
  filteredCountry: any;
  CountryCtrl: any;
  country_json: any;
  state: any;
  state_list: any;
  country = false;
  status = false;
  statuscategory = false;
  cusinesflag = false;
  tagflag = false;
  Cusines_list = []
  tag_list = []
  tagslist = [];
  cusinesDishflag = false;
  statusDish = false;
  vegflag = false;
  checked = true
  v = [];
  addonDishflag = false;
  statusaddon = false;
  vegaddonflag = false;
  v1 = [];
  Rootdrinkcategory = []

  Dishes = []
  brandcountry = false;
  brandcategory = false;
  brandstatus = false;
  brand = false; Brands = []
  productcategory = false;
  productstatus = false;
  brandp = false;
  searchflag = false;
  attrstatus = false;
  filterflag = false;
  set = false;
  admin = [];
  cuser = false;
  catuser = false;
  dishuser = false;
  addonuser = false;
  liquorbranduser = false;
  liquorproductuser = false;
  liquorattruser = false
  rootFlag = true;
  rootindex = 0
  // rootCount=0;
  constructor(private _formBuilder: FormBuilder, private main: MainService, private router: Router,
    public dialog: MatDialog, private menuService: AddMenuFoodsService) {
    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];
    this.getadmin();
    this.getAllDrinks()
    this.getTags();
    this.getAllCategories();
    this.getAllCuisines();
    this.getAllDishes();
    this.getAllAddons();
    this.getAllVarients()
    this.getAllLiquorAttribute();
    this.getAllLiquorAttributeSet();
    this.getAllLiquorProducts();
    this.getAllLiquorbrand();
    // this.getAllApis()
    // this.main.getAllEstablishment().subscribe(data => {
    //   ////console.log(data);
    //   this.firstLevel = data
    // })
    /*country */
    this.country_state_city()

    var d = new Date()
    this.date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();

  }

  country_state_city() {
    console.log("hey");

    this.menuService.country_state_city().subscribe(data => {
      // console.log("============", data);

      console.log(data.country_json);
      this.country_json = data.country_json


      for (var i = 0; i < data.country_json.length - 1; i++)
        //console.log("=========================");

        console.log(data.country_json[i].name);

    })
  }
  ngOnInit() {
    let index = localStorage.getItem('userTabLocation')
    if (index == undefined) {
      let index = 0; // get stored number or zero if there is nothing stored
      this.activeTab = index; // with tabGroup being the MatTabGroup accessed through ViewChild

    } else {
      // let index = localStorage.getItem('userTabLocation') || 0; // get stored number or zero if there is nothing stored
      this.activeTab = index; // with tabGroup being the MatTabGroup accessed through ViewChild

    }

    let beverageTabIndex = localStorage.getItem('beverageTabLocation')
    if (beverageTabIndex == undefined) {
      this.activeTabBeverage = 0
    } else {
      this.activeTabBeverage = beverageTabIndex
    }


    let retailFoodTabIndex = localStorage.getItem('retailFoodMenuManagement')
    if (retailFoodTabIndex == undefined) {
      this.activeTabRetailFood = 0
    } else {
      this.activeTabRetailFood = retailFoodTabIndex
    }
  }

  // getAllApis() {
  //   ////console.log("in fork join");

  //   forkJoin(
  //     {
  //       allCuisines: this.menuService.getCuisines(),
  //       allCategory: this.menuService.gelAllCategory(),
  //       allDishes: this.menuService.getAllDishes(),
  //       allAddons: this.menuService.getAllAddons(),
  //       allVarient: this.menuService.getAllvarients(),
  //       allLiquorBrand: this.menuService.getAllBrand(),
  //       allLiquorProduct: this.menuService.getAllLiquorProducts(),
  //       allAttributes: this.menuService.getAllLiquorAttribute(),
  //       allAttributeGroups: this.menuService.getLiquorAttributeGroup(),
  //       allAttributeSets: this.menuService.getAllLiquorAttributeSets(),
  //       // allFoodInventoryIngrdient: this.menuService.getAllIngredientFoodInventory(),
  //       // allFoodInventoryItems: this.menuService.getAllItemsFoodInventory(),
  //       // allAssetInventoryEquipments: this.menuService.getAllEquipmentAssetInventory(),
  //       // allBeverages: this.menuService.getAllBeverages(),
  //       // allBeveragesBrands: this.menuService.getAllBeverageBrands(),
  //       // allBeveragesProducts: this.menuService.getAllBeverageProducts(),
  //       // allRetailFoodBrands: this.menuService.getAllRetailFoodBrands(),
  //       // allRetailFoodProducts: this.menuService.getAllRetailFoodProducts()
  //     }
  //   ).subscribe(result => {
  //     ////console.log("fork Join", result);
  //     this.allMenuData = result

  //     //get all tables data function call
  //     this.getAllCuisines()
  //     this.getAllCategories()
  //     this.getAllDishes()
  //     this.getAllAddons()
  //     this.getAllVarients()
  //     this.getAllLiquorbrand()
  //     this.getAllLiquorProducts()
  //     this.getAllLiquorAttribute()
  //     this.getAllLiquorAttributeGroup()
  //     this.getAllLiquorAttributeSet()
  //    // this.getAllIngredientFoodInventory()
  // //     this.getAllItemsFoodInventory()
  // //     this.getAllEquipmentAssetInventory()
  // //     this.getAllBeverageBrands()
  // //     // this.getAllBeverageProducts()
  // //     this.getAllRetailFoodBrands()
  // // //    this.getAllRetailFoodProducts()
  // //     this.getAllBeverages()


  //     this.foodAllCount = 0

  //     // this.foodAllCount = this.allMenuData.allCuisines['data'].length + this.allMenuData.allCategory['data'].length + this.allMenuData.allDishes['data'].length +
  //     //   this.allMenuData.allAddons['data'].length + this.allMenuData.allVarient['data'].length;
  //     //   //console.log("end");

  //     //console.log("total>",this.foodAllCount);

  //   })
  // }
  ngAfterViewInit() {

  }
  //Tabs On navigate from tab back to same tab
  handleMatTabChange(event) {
    ////console.log(event);
    localStorage.setItem('userTabLocation', event);
  }

  handleMatTabChangeMenuManagementBeverage(event) {
    localStorage.setItem('beverageTabLocation', event)
  }

  handleMatTabChangeRetailFood(event) {
    localStorage.setItem('retailFoodMenuManagement', event)
  }

  // import excel file and convert it to json object
  uploadedFile(event) {
    ////console.log("upload file");

    this.fileUploaded = event.target.files[0];
    this.readExcel();
  }
  readExcel() {
    let readFile = new FileReader();
    ////console.log(readFile);

    readFile.onload = (e) => {

      this.storeData = readFile.result;
      var data = new Uint8Array(this.storeData);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });

      var first_sheet_name = workbook.SheetNames[0];
      this.worksheet = workbook.Sheets[first_sheet_name];
      this.jsonData = XLSX.utils.sheet_to_json(this.worksheet, { raw: false });
      this.jsonData.map(ele => (ele.userid = this.userid))
      //   this.jsonData = JSON.stringify(this.jsonData);
      ////console.log(this.jsonData);
    }
    readFile.readAsArrayBuffer(this.fileUploaded);


  }


  //get All Cuisines -menu food
  getAllCuisines() {
    // let data=[]
    // // data['data']=[]
    // //console.log( this.allMenuData.allCuisines);
    this.menuService.getCuisines().subscribe(data => {
      //  ? data = this.allMenuData.allCuisines
      //console.log("all cuisines", data);
      this.cuisine_data = []

      if (data['sucess'] == false) {
        this.cuisineCount = 0
        ////console.log("Cuisines Data Not Found");
        this.noData = 0
      } else {
        //console.log("length>>",data['data']);
        this.cuisine_data = data['data']
        this.cuisineCount = data['data'].length
        this.cuisine_data.forEach(element => {
          element.cuisineCode = "C" + element.cuisineCode
        });
        this.dataSource = new MatTableDataSource(this.cuisine_data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.noData = this.dataSource.connect().pipe(map(data => data.length === 0));
        data['data'].forEach(element => {
          //////console.log(element);
          this.Cusines_list.push(element['cuisineName'])
          let combinationOfCusines = []
          element.combinationOfCusines.forEach(ele => {
            combinationOfCusines.push(ele.cuisineName)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let cusinesObj = {}
          cusinesObj = {
            "cuisineCode": element.cuisineCode,
            "cuisineName": element.cuisineName,
            "country": element.country,
            "city": element.city,
            "state": element.state,
            "combinationOfCusines": combinationOfCusines.toString(),
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          // ////console.log(cusinesObj);
          this.cusineExportJSONDATA.push(cusinesObj)
        });
        ////console.log("cusineExport", this.cusineExportJSONDATA);
      }
    })
    // this.allMenuData.allCuisines=[]
  }

  // export cuisiens excel 
  readAsCSVCuisine() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Cuisine Code", "Cuisine Name", "Country", "State", "City", "Combination Of Cusines", "Added By", "Updated By"]
    };
    new ngxCsv(this.cusineExportJSONDATA, 'Cuisines_Export_' + this.date, options);
  }

  //get All Category -menu food
  getAllCategories() {
    this.menuService.gelAllCategory().subscribe(data => {
      this.category_data = data['data']
      if (data['sucess'] == false) {
        this.categoryCount = 0
        this.noData1 = 0
        ////console.log("Category Data Not Found");
      } else {
        this.categoryCount = data['data'].length
        data['data'].forEach(element => {
          element.categoryCode = "C" + element.categoryCode
        });
        this.dataSourceCategory = new MatTableDataSource(data['data']);
        this.dataSourceCategory.paginator = this.categorypaginator;
        this.dataSourceCategory.sort = this.categorysort;
        this.noData1 = this.dataSourceCategory.connect().pipe(map(data => data.length === 0));

        data['data'].forEach(element => {
          let cuisines = []
          element.cuisines.forEach(ele => {
            cuisines.push(ele.cuisineName)
          });
          let tags = []
          element.tags.forEach(e => {
            tags.push(e.tagName)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let categoryObj = {}
          categoryObj = {
            "categoryCode": element.categoryCode,
            "categoryName": element.categoryName,
            "cuisines": cuisines.toString(),
            "tags": tags.toString(),
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.categoryExportJSONDATA.push(categoryObj)
        });
        ////console.log("category export", this.categoryExportJSONDATA);

      }
    })
  }

  // export category excel 
  readAsCSVCategory() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Category Code", "Category Name", "Cuisines", "Tags", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.categoryExportJSONDATA, 'Category_Export_' + this.date, options);
  }

  //get All dish- menu food
  getAllDishes() {
    this.menuService.getAllDishes().subscribe(data => {
      ////console.log("all dishes", data);

      if (data['sucess'] == false) {
        this.dishesCount = 0
        this.noData2 = 0
        ////console.log("Dishes Data Not Found");
      } else {
        this.dish_data = data['data']
        this.dishesCount = data['data'].length
        data['data'].forEach(element => {
          this.Dishes.push(element['dishName'])
          element.dishCode = "D" + element.dishCode
        });
        this.dataSourceDish = new MatTableDataSource(data['data']);
        this.dataSourceDish.paginator = this.dishpaginator;
        this.dataSourceDish.sort = this.dishsort;
        this.noData2 = this.dataSourceDish.connect().pipe(map(data => data.length === 0));

        data['data'].forEach(element => {
          let cuisines = []
          element.cuisines.forEach(ele => {
            cuisines.push(ele.cuisineName)
          });
          let varients = []
          element.varients.forEach(e => {
            varients.push(e.varientName)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let dishObj = {}
          dishObj = {
            "dishCode": element.dishCode,
            "dishName": element.dishName,
            "cuisines": cuisines.toString(),
            "varients": varients.toString(),
            "veg_nonveg": element.veg_nonveg,
            "unit": element.unit,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.dishExportJSONDATA.push(dishObj)
        });
        ////console.log("dish export", this.dishExportJSONDATA);

      }
    })
  }


  // export dish excel 
  readAsCSVDish() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Dish Code", "Dish Name", "Cuisines", "Varients", "Veg Nonveg", "Unit", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.dishExportJSONDATA, 'Dish_Export_' + this.date, options);
  }

  //get all addons -menu food
  getAllAddons() {
    this.menuService.getAllAddons().subscribe(data => {
      if (data['sucess'] == false) {
        this.addonCount = 0
        this.noData3 = 0
        ////console.log("Add ons Data Not Found");
      } else {
        this.addon_data = data['data']
        this.addonCount = data['data'].length
        data['data'].forEach(element => {
          element.adonCode = "A" + element.adonCode
        });
        this.dataSourceAddOn = new MatTableDataSource(data['data']);
        this.dataSourceAddOn.paginator = this.addonpaginator;
        this.dataSourceAddOn.sort = this.addonsort;
        this.noData3 = this.dataSourceAddOn.connect().pipe(map(data => data.length === 0));
        data['data'].forEach(element => {
          let dishes = []
          element.dishes.forEach(ele => {
            dishes.push(ele.dishName)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let addonObj = {}
          addonObj = {
            "adonCode": element.adonCode,
            "adonName": element.adonName,
            "dishes": dishes.toString(),
            "veg_nonveg": element.veg_nonveg,
            "unit": element.unit,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.addonExportJSONDATA.push(addonObj)
        });
        ////console.log("Add on export", this.addonExportJSONDATA);

      }
    })
  }

  // export addon excel 
  readAsCSVAddon() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Addon Code", "Addon Name", "Dishes", "Veg Nonveg", "Unit", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.addonExportJSONDATA, 'Addon_Export_' + this.date, options);
  }

  // get All varients -menu food
  getAllVarients() {
    this.menuService.getAllvarients().subscribe(data => {

      if (data['sucess'] == false) {
        this.varientCount = 0
        this.noData4 = 0
        ////console.log("Varient Data Not Found");
      } else {
        this.varientCount = data['data'].length
        this.varient_data = data['data']
        data['data'].forEach(element => {
          element.varientCode = "V" + element.varientCode
        });
        this.dataSourceVarients = new MatTableDataSource(data['data']);
        this.dataSourceVarients.paginator = this.varientpaginator;
        this.dataSourceVarients.sort = this.varientsort;
        this.noData4 = this.dataSourceVarients.connect().pipe(map(data => data.length === 0));
        data['data'].forEach(element => {
          var varientContent = []
          element.varientContent.forEach(ele => {
            varientContent.push(ele.contentName)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let varientObj = {}
          varientObj = {
            "varientCode": element.varientCode,
            "varientName": element.varientName,
            "varientContent": varientContent.toString(),
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.varientExportJSONDATA.push(varientObj)
        });
        ////console.log("vareintExport", this.varientExportJSONDATA);
      }
    })
  }

  // export varient excel 
  readAsCSVVarient() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Varient Code", "Varient Name", "Varient Content", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.varientExportJSONDATA, 'Varient_Export_' + this.date, options);
  }

  //get all liquor brand
  getAllLiquorbrand() {
    this.menuService.getAllBrand().subscribe(data => {
      ////console.log("all liquor brand", data);
      if (data['sucess'] == false) {
        this.liquorBrandCount = 0
        this.noData5 = 0
        ////console.log("Liquor Brand Data Not Found");
      } else {
        this.liquorbrand_data = data['data']
        this.liquorBrandCount = data['data'].length
        data['data'].forEach(element => {
          this.Brands.push(element['brandName'])


          element.brandCode = "B" + element.brandCode

        });
        data['data'].forEach(element => {
          this.rootDrinkType = []
          element.categoryType.forEach(ele => {
            this.rootDrinkType.push(ele.rootDrinkType)
          });
          element.rootDrinkType = this.rootDrinkType
        });
        this.dataSourceLiquorBrand = new MatTableDataSource(data['data']);
        this.dataSourceLiquorBrand.paginator = this.liquorBrandpaginator;
        this.dataSourceLiquorBrand.sort = this.liquorBrandsort;
        this.noData5 = this.dataSourceLiquorBrand.connect().pipe(map(data => data.length === 0));
        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var brandObj = {}
          brandObj = {
            "brandCode": element.brandCode,
            "brandName": element.brandName,
            "country": element.country,
            "rootDrinkType": element.rootDrinkType,
            "liquorVarient": element.liquorVarient,
            "liquorSubVarient": element.liquorSubVarient,
            "liquorSuVarientType": element.liquorSuVarientType,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.liquorBrandJSONDATA.push(brandObj)
        });
        ////console.log("liquor brand export", this.liquorBrandJSONDATA);

      }
    })
  }

  // export liquor brand excel 
  readAsCSVLiquorBrand() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Brand Code", "Brand Name", "Country", "Root Drink Type", "Liquor Varient", "Liquor Sub Varient", "Liquor Sub Varient Type", "Status", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.liquorBrandJSONDATA, 'Liquor_Brand_Export_' + this.date, options);
  }

  //get all liquor products
  getAllLiquorProducts() {
    let alliquorProducts = []
    this.menuService.getAllLiquorProducts().subscribe(data => {
      console.log("all liquor products", data);
      if (data['success'] == false) {
        this.noData6 = 0
      }
      if (data['success'] == true) {
        this.liquorProductCount = data['data'].length
        this.liquorproduct_data = data['data']

        data['data'].forEach(async element => {
          // ////console.log(element['brand']._id);
          element.productCode = "P" + element.productCode
          element.varient = element.varient



          // await this.menuService.getSingleLiquorBrand(element['brand']._id).subscribe(async brandData => {
          //   // ////console.log(brandData['data'].liquorVarient);
          //   Object.assign(element, { view"varient": brandData['data'].liquorVarient })
          //   //  ////console.log(element);
          //   alliquorProducts.push(element)                
          //   // ////console.log(alliquorProducts);
          this.dataSourceLiquorProduct = new MatTableDataSource(this.liquorproduct_data);
          this.dataSourceLiquorProduct.paginator = this.liquorProductpaginator;
          this.dataSourceLiquorProduct.sort = this.liquorProductsort;
          this.noData6 = this.dataSourceLiquorProduct.connect().pipe(map(data => data.length === 0));
          data['data'].forEach(element => {
            var d = new Date(element.updatedAt)
            var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
            var productObj = {}
            productObj = {
              "productCode": element.productCode,
              "productName": element.productName,
              "varient": element.varient,
              "root": element.root,
              "subVarient": element.subVarient,
              "subsubVarient": element.subsubVarient,
              "varientType": element.varientType,
              "brand": element.brand.brandName,
              "attribute": element.attribute_set,
              "status": element.status,
              "addedBy": element.addedBy.userName,
              "updatedBy": element.updatedBy.userName,
              "updatedAt": date
            }
            this.liquorProductJSONDATA.push(productObj)
          });
          ////console.log("liquor product export", this.liquorProductJSONDATA);

        })
        // });
      } else {
        this.liquorProductCount = 0
        ////console.log("Liquor Product Data Not Found");
      }
    })
  }

  // export liquor product excel 
  readAsCSVLiquorProduct() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Product Code", "Product Name", "Varient", "Brand", "Attribute Set", "Status", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.liquorProductJSONDATA, 'Liquor_Product_Export_' + this.date, options);
  }

  //get all attributes
  getAllLiquorAttribute() {
    this.menuService.getAllLiquorAttribute().subscribe(data => {
      ////console.log("all liquor attribute", data);

      if (data['sucess'] == false) {
        this.liquorAttributeCount = 0
        this.assetInventoryAttributeCount = 0
        ////console.log("Attribute Data Not Found");
        this.noData7 = 0
        this.noData19 = 0
        this.noData15 = 0
      } else {
        // LIQUOR
        this.liquorattribute_data = data['data']
        data['data'].forEach(element => {
          element.attributeCode = "A" + element.attributeCode
        });
        let allAttributes = data['data'].filter(element => element.attributeType == "liquor")
        this.liquorattribute_data = data['data'].filter(element => element.attributeType == "liquor")
        this.dataSourceLiquorAttribute = new MatTableDataSource(allAttributes);
        this.dataSourceLiquorAttribute.paginator = this.liquorAttributepaginator;
        this.dataSourceLiquorAttribute.sort = this.liquorAttributesort;
        this.noData7 = this.dataSourceLiquorAttribute.connect().pipe(map(data => data.length === 0));
        this.allLiquorAttribute = allAttributes.filter(ele => ele['isAssign'] == false)
        this.liquorAttributeCount = allAttributes.length
        // BEVERAGE
        this.dataSourceBeverageAttribute = new MatTableDataSource(data['data'].filter(element => element.attributeType == "beverage"))
        this.beverageattribute_data = data['data'].filter(element => element.attributeType == "beverage")

        this.dataSourceBeverageAttribute.paginator = this.beverageAttributepaginator;
        this.dataSourceBeverageAttribute.sort = this.beverageAttributesort;
        this.noData15 = this.dataSourceBeverageAttribute.connect().pipe(map(data => data.length === 0));
        this.allBeverageUnassignedAttribute = data['data'].filter(element => element.attributeType == "beverage" && element.isAssign == false)

        // RETAIL FOOD
        this.dataSourceRetailFoodAttribute = new MatTableDataSource(data['data'].filter(element => element.attributeType == "retailFood"))
        this.retailFoodattribute_data = data['data'].filter(element => element.attributeType == "retailFood")
        this.noData19 = this.dataSourceRetailFoodAttribute.connect().pipe(map(data => data.length === 0));
        this.dataSourceRetailFoodAttribute.paginator = this.retailFoodAttributepaginator;
        this.dataSourceRetailFoodAttribute.sort = this.retailFoodAttributesort;

        this.allRetailFoodUnassignedAttribue = data['data'].filter(element => element.attributeType == "retailFood" && element.isAssign == false)

        //Asset Inventory
        this.assetInventoryAttributeCount = data['data'].filter(element => element.attributeType == "equipment").length


        var liquorAttribute = data['data'].filter(element => element.attributeType == "liquor")
        liquorAttribute.forEach(element => {
          var optionLabels = []
          element.responseType.options.forEach(ele => {
            optionLabels.push(ele.optionLable)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let liquorObj = {}
          liquorObj = {
            "attributeCode": element.attributeCode,
            "attributeName": element.attributeName,
            "responseType": element.responseType.elementName,
            "fieldValue": element.responseType.fieldValue,
            "options": optionLabels.toString(),
            "isSearchable": element.isSearchable,
            "isFilterable": element.isFilterable,
            "isAssign": element.isAssign,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.liquorAttributeJSONDATA.push(liquorObj)
        });
        ////console.log("liquor attribute export", this.liquorAttributeJSONDATA);

        var beverageAttribute = data['data'].filter(element => element.attributeType == "beverage")
        beverageAttribute.forEach(element => {
          var optionLabels = []
          element.responseType.options.forEach(ele => {
            optionLabels.push(ele.optionLable)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let beverageObj = {}
          beverageObj = {
            "attributeCode": element.attributeCode,
            "attributeName": element.attributeName,
            "responseType": element.responseType.elementName,
            "fieldValue": element.responseType.fieldValue,
            "options": optionLabels.toString(),
            "isSearchable": element.isSearchable,
            "isFilterable": element.isFilterable,
            "isAssign": element.isAssign,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.beverageAttributeJSONDATA.push(beverageObj)
        });
        ////console.log("beverage attribute export", this.beverageAttributeJSONDATA);


        var retailFoodAttribute = data['data'].filter(element => element.attributeType == "retailFood")
        retailFoodAttribute.forEach(element => {
          var optionLabels = []
          element.responseType.options.forEach(ele => {
            optionLabels.push(ele.optionLable)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let retailFoodObj = {}
          retailFoodObj = {
            "attributeCode": element.attributeCode,
            "attributeName": element.attributeName,
            "responseType": element.responseType.elementName,
            "fieldValue": element.responseType.fieldValue,
            "options": optionLabels.toString(),
            "isSearchable": element.isSearchable,
            "isFilterable": element.isFilterable,
            "isAssign": element.isAssign,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.retailFoodAttributeJSONDATA.push(retailFoodObj)
        });
        ////console.log("retail food attribute export", this.retailFoodAttributeJSONDATA);

      }
    })
  }

  // export liquor attribute excel 
  readAsCSVLiquorAttribute() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Attribute Code", "Attribute Name", "Response Type", "Field Value", "Options", "Searchable", "Filterable", "Assign", "Status", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.liquorAttributeJSONDATA, 'Liquor_Attributes_Export_' + this.date, options);

  }

  // export beverage attribute excel 
  readAsCSVBeverageAttribute() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Attribute Code", "Attribute Name", "Response Type", "Field Value", "Options", "Searchable", "Filterable", "Assign", "Status", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.beverageAttributeJSONDATA, 'Beverage_Attribute_Export_' + this.date, options);
  }

  // export retail food attribute excel 
  readAsCSVRetailFoodAttribute() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Attribute Code", "Attribute Name", "Response Type", "Field Value", "Options", "Searchable", "Filterable", "Assign", "Status", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.retailFoodAttributeJSONDATA, 'Retail_Food_Attribute_Export_' + this.date, options);
  }


  //get all attribute group
  // getAllLiquorAttributeGroup() {
  //   let data = this.allMenuData.allAttributeGroups
  //   ////console.log("All liquor attribute group", data);
  //   if (data['sucess'] == false) {
  //     ////console.log("Attribute group Data Not Found");
  //   } else {
  //     // LIQUOR
  //     this.allLiquorAttributeGroup = data['data'].filter(ele => ele.attributeType == "liquor")

  //     // BEVERAGE
  //     this.allBeverageAttributeGroups = data['data'].filter(ele => ele.attributeType == "beverage")

  //     // RETAIL FOOD
  //     this.allRetailFoodAttributeGroups = data['data'].filter(ele => ele.attributeType == "retailFood")
  //   }
  // }

  // //get all attribute set
  getAllLiquorAttributeSet() {
    this.menuService.getAllLiquorAttributeSets().subscribe(data => {
      ////console.log("All liquor attribute sets", data);
      if (data['sucess'] == false) {
        ////console.log("Attribute Set Data Not Found");
        this.noData8 = 0
        this.noData20 = 0
      } else {

        // LIQUOR
        data['data'].forEach(element => {
          element.attributeSetCode = "AS" + element.attributeSetCode
        });
        this.dataSourceLiquorAttributeSet = new MatTableDataSource(data['data'].filter(ele => ele.attributeType == "liquor"))
        this.liquorattributeset_data = this.dataSourceLiquorAttributeSet.data
        this.dataSourceLiquorAttributeSet.paginator = this.liquorAttributeSetpaginator
        this.dataSourceLiquorAttributeSet.sort = this.liquorAttributeSetSort
        this.noData8 = this.dataSourceLiquorAttributeSet.connect().pipe(map(data => data.length === 0));

        // BEVERAGE
        this.dataSourceBeverageAttributeSet = new MatTableDataSource(data['data'].filter(ele => ele.attributeType == "beverage"))
        this.beverageattributeset_data = this.dataSourceBeverageAttributeSet.data
        this.dataSourceBeverageAttributeSet.paginator = this.BeverageAttributeSetpaginator
        this.dataSourceBeverageAttributeSet.sort = this.BeverageAttributeSetSort
        this.noData16 = this.dataSourceBeverageAttributeSet.connect().pipe(map(data => data.length === 0));

        // RETAIL FOOD
        this.dataSourceRetailFoodAttributeSet = new MatTableDataSource(data['data'].filter(ele => ele.attributeType == "retailFood"))
        this.retailfoodattributeset_data = this.dataSourceRetailFoodAttributeSet.data
        this.noData20 = this.dataSourceRetailFoodAttributeSet.connect().pipe(map(data => data.length === 0));
        this.dataSourceRetailFoodAttributeSet.paginator = this.RetailFoodAttributeSetpaginator
        this.dataSourceRetailFoodAttributeSet.sort = this.RetailFoodAttributeSetSort

        var liquorAttributeSet = data['data'].filter(ele => ele.attributeType == "liquor")
        liquorAttributeSet.forEach(element => {
          var assigngGroup = []
          element.assignGroups.forEach(ele => {
            assigngGroup.push(ele.groupName)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var liquorObj = {}
          liquorObj = {
            "attributeSetCode": element.attributeSetCode,
            "attributeSetName": element.attributeSetName,
            "assignGroups": assigngGroup.toString(),
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.liquorAttributeSetJSONDATA.push(liquorObj)
        });
        ////console.log("liquor attribute set export", this.liquorAttributeSetJSONDATA);


        var beverageAttributeSet = data['data'].filter(ele => ele.attributeType == "beverage")
        ////console.log(">>>>>>>>>>>>>>>>>>>", beverageAttributeSet);
        var assigngGroup = []
        beverageAttributeSet.forEach(element => {

          element.assignGroups.forEach(ele => {
            assigngGroup.push(ele.groupName)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var beverageObj = {}
          beverageObj = {
            "attributeSetCode": element.attributeSetCode,
            "attributeSetName": element.attributeSetName,
            "assignGroups": assigngGroup.toString(),
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.beverageAttributeSetJSONDATA.push(beverageObj)
        });
        ////console.log("beverage attribute set export", this.beverageAttributeSetJSONDATA);


        var retailFoodAttributeSet = data['data'].filter(ele => ele.attributeType == "retailFood")
        retailFoodAttributeSet.forEach(element => {
          var assigngGroup = []
          element.assignGroups.forEach(ele => {
            assigngGroup.push(ele.groupName)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var retailFoodObj = {}
          retailFoodObj = {
            "attributeSetCode": element.attributeSetCode,
            "attributeSetName": element.attributeSetName,
            "assignGroups": assigngGroup.toString(),
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.retailFoodAttributeSetJSONDATA.push(retailFoodObj)
        });
        ////console.log("retail food attribute set export", this.retailFoodAttributeSetJSONDATA);
      }
    })
  }

  // export liquor attribute set excel 
  readAsCSVLiquorAttributeSet() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["attributeSetCode", "attributeSetName", "assignGroups", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.liquorAttributeSetJSONDATA, 'Liquor_Attribute_Set_Export_' + this.date, options);
  }

  // export beverage attribute set excel 
  readAsCSVBeverageAttributeSet() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["attributeSetCode", "attributeSetName", "assignGroups", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.beverageAttributeSetJSONDATA, 'Beverage_Attribute_Set_Export_' + this.date, options);
  }

  // export retail food attribute set excel 
  readAsCSVRetailFoodAttributeSet() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["attributeSetCode", "attributeSetName", "assignGroups", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.retailFoodAttributeSetJSONDATA, 'Retail_Food_Attribute_Set_Export_' + this.date, options);
  }


  // // get all ingredients food inventory
  // getAllIngredientFoodInventory() {
  //   let data = this.allMenuData.allFoodInventoryIngrdient
  //   ////console.log("All Ingredient food inventory", data);
  //   if (data['sucess'] == false) {
  //     ////console.log("Food Inventroy Ingredient Data Not Found");

  //   } else {
  //     data['data'].forEach(element => {
  //       element.ingredientCode = "I" + element.ingredientCode
  //     });
  //     this.dataSourceIngredient = new MatTableDataSource(data['data']);
  //     this.dataSourceIngredient.paginator = this.ingredientpaginator;
  //     this.dataSourceIngredient.sort = this.ingredientsort;
  //     this.noData9 = this.dataSourceIngredient.connect().pipe(map(data => data.length === 0));

  //     data['data'].forEach(element => {
  //       var d = new Date(element.updatedAt)
  //       var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  //       var ingredientObj = {}
  //       ingredientObj = {
  //         "ingredientCode": element.ingredientCode,
  //         "ingredientName": element.ingredientName,
  //         "ingredientcategory": element.ingredientcategory,
  //         "unit": element.unit,
  //         "status": element.status,
  //         "addedBy": element.addedBy.userName,
  //         "updatedBy": element.updatedBy.userName,
  //         "updatedAt": date
  //       }
  //       this.ingredientJSONDATA.push(ingredientObj)
  //     });
  //     ////console.log("ingredient export", this.ingredientJSONDATA);
  //   }
  // }

  // // export ingredient excel 
  // readAsCSVIngredient() {
  //   var options = {
  //     fieldSeparator: ',',
  //     quoteStrings: '"',
  //     decimalseparator: '.',
  //     showLabels: true,
  //     //  showTitle: true,
  //     //  title: 'Your title',
  //     useBom: true,
  //     noDownload: false,
  //     headers: ["ingredientCode", "ingredientName", "ingredientcategory", "unit", "status", "addedBy", "updatedBy", "updatedAt"]
  //   };
  //   new ngxCsv(this.ingredientJSONDATA, 'Ingredient_Export_' + this.date, options);
  // }

  // // get all items food inventory
  // getAllItemsFoodInventory() {
  //   let data = this.allMenuData.allFoodInventoryItems
  //   ////console.log("All Items Food Inventory", data);
  //   if (data['sucess'] == false) {
  //     ////console.log("Food Inventory Items Data Not Found");
  //   } else {
  //     data['data'].forEach(element => {
  //       element.itemCode = "I" + element.itemCode
  //     });
  //     this.dataSourceInput = new MatTableDataSource(data['data']);
  //     this.dataSourceInput.paginator = this.inputpaginator;
  //     this.dataSourceInput.sort = this.inputsort;
  //     this.noData10 = this.dataSourceInput.connect().pipe(map(data => data.length === 0));

  //     data['data'].forEach(element => {
  //       var d = new Date(element.updatedAt)
  //       var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  //       var itemtObj = {}
  //       itemtObj = {
  //         "itemCode": element.itemCode,
  //         "itemName": element.itemName,
  //         "brand": element.brand.BrandName,
  //         "ingredient": element.ingredient.ingredientName,
  //         "quantity": element.quantity,
  //         "status": element.status,
  //         "addedBy": element.addedBy.userName,
  //         "updatedBy": element.updatedBy.userName,
  //         "updatedAt": date
  //       }
  //       this.itemsJSONDATA.push(itemtObj)
  //     });
  //     ////console.log("Item export", this.itemsJSONDATA);
  //   }
  // }

  // // export items excel 
  // readAsCSVItems() {
  //   var options = {
  //     fieldSeparator: ',',
  //     quoteStrings: '"',
  //     decimalseparator: '.',
  //     showLabels: true,
  //     //  showTitle: true,
  //     //  title: 'Your title',
  //     useBom: true,
  //     noDownload: false,
  //     headers: ["itemCode", "itemName", "brand", "ingredient", "quantity", "status", "addedBy", "updatedBy", "updatedAt"]
  //   };
  //   new ngxCsv(this.itemsJSONDATA, 'Items_Export_' + this.date, options);
  // }


  // // get all equipment asset inventory
  // getAllEquipmentAssetInventory() {
  //   let data = this.allMenuData.allAssetInventoryEquipments
  //   ////console.log("All Equipment Asset Inventory", data);
  //   if (data['success'] == false) {
  //     ////console.log("Asset Inventory Equipment Data Not Found");

  //   } else {
  //     data['data'].forEach(element => {
  //       element.equipmentCode = "E" + element.equipmentCode
  //     });
  //     this.dataSourceEquipment = new MatTableDataSource(data['data'])
  //     this.dataSourceEquipment.paginator = this.equipmentpaginator;
  //     this.dataSourceEquipment.sort = this.equipmentsort;


  //     data['data'].forEach(element => {
  //       var d = new Date(element.updatedAt)
  //       var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  //       var equipmentObj = {}
  //       equipmentObj = {
  //         "equipmentCode": element.equipmentCode,
  //         "equipmentName": element.equipmentName,
  //         "category": element.category,
  //         "assignattributeSet": element.assignattributeSet.attributeSetName,
  //         "addedBy": element.addedBy.userName,
  //         "updatedBy": element.updatedBy.userName,
  //         "updatedAt": date
  //       }
  //       this.equipmentJSONDATA.push(equipmentObj)
  //     });
  //     ////console.log("Equipment export", this.equipmentJSONDATA);
  //   }
  // //  this.noData11 = this.dataSourceEquipment.connect().pipe(map(data => data.length === 0));
  // }

  // // export equipment excel 
  // readAsCSVEquipment() {
  //   var options = {
  //     fieldSeparator: ',',
  //     quoteStrings: '"',
  //     decimalseparator: '.',
  //     showLabels: true,
  //     //  showTitle: true,
  //     //  title: 'Your title',
  //     useBom: true,
  //     noDownload: false,
  //     headers: ["equipmentCode", "equipmentName", "category", "assignattributeSet", "addedBy", "updatedBy", "updatedAt"]
  //   };
  //   new ngxCsv(this.equipmentJSONDATA, 'Equipment_Export_' + this.date, options);
  // }

  // //get all beverages
  // getAllBeverages() {
  //   let data = this.allMenuData.allBeverages
  //   ////console.log("All Beverages", data);
  //   if (data['success'] == false) {
  //     ////console.log("Beverages Data Not Found");
  //     this.noData12 = 0
  //   } else {
  //     this.beverage_data=data['data']
  //     data['data'].forEach(element => {
  //       element.beverageCode = "B" + element.beverageCode
  //     });
  //     this.dataSourceBeverage = new MatTableDataSource(data['data'])
  //     this.dataSourceBeverage.paginator = this.beveragepaginator;
  //     this.dataSourceBeverage.sort = this.beverageSort;
  //     this.noData12 = this.dataSourceBeverage.connect().pipe(map(data => data.length === 0));

  //     data['data'].forEach(element => {
  //       var d = new Date(element.updatedAt)
  //       var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  //       var beverageObj = {}
  //       beverageObj = {
  //         "beverageCode": element.beverageCode,
  //         "beverageName": element.beverageName,
  //         "rootCategory": element.rootCategory,
  //         "varients": element.varients,
  //         "subVarient": element.subVarient,
  //         "type": element.type,
  //         "status": element.status,
  //         "addedBy": element.addedBy.userName,
  //         "updatedBy": element.updatedBy.userName,
  //         "updatedAt": date
  //       }
  //       this.beverageJSONDATA.push(beverageObj)
  //     });
  //     ////console.log("beverage export", this.beverageJSONDATA);
  //   }
  // }

  // // export beverage excel 
  // readAsCSVBeverage() {
  //   var options = {
  //     fieldSeparator: ',',
  //     quoteStrings: '"',
  //     decimalseparator: '.',
  //     showLabels: true,
  //     //  showTitle: true,
  //     //  title: 'Your title',
  //     useBom: true,
  //     noDownload: false,
  //     headers: ["beverageCode", "beverageName", "rootCategory", "varients", "subVarient", "type", "status", "addedBy", "updatedBy", "updatedAt"]
  //   };
  //   new ngxCsv(this.beverageJSONDATA, 'Beverage_Export_' + this.date, options);
  // }

  // //get all beverage brands
  // getAllBeverageBrands() {
  //   let data = this.allMenuData.allBeveragesBrands
  //   ////console.log("All Beverage Brands", data);
  //   if (data['success'] == false) {
  //     this.beverageBrandCount = 0
  //     this.noData14 = 0
  //     ////console.log("Beverage Brand Data Not Found");
  //   } else {
  //     this.beverageBrand_data=data['data']
  //     this.beverageBrandCount = data['data'].length
  //     data['data'].forEach(element => {
  //       element.brandCode = "B" + element.brandCode
  //     });
  //     this.dataSourceBeverageBrands = new MatTableDataSource(data['data'])
  //     this.dataSourceBeverageBrands.paginator = this.beverageBrandspaginator;
  //     this.dataSourceBeverageBrands.sort = this.beverageBrandsSort;
  //     this.noData14 = this.dataSourceBeverageBrands.connect().pipe(map(data => data.length === 0));

  //     data['data'].forEach(element => {
  //       var d = new Date(element.updatedAt)
  //       var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  //       var brandObj = {}
  //       brandObj = {
  //         "brandCode": element.brandCode,
  //         "brandName": element.brandName,
  //         "country": element.country,
  //         "status": element.status,
  //         "addedBy": element.addedBy.userName,
  //         "updatedBy": element.updatedBy.userName,
  //         "updatedAt": date
  //       }
  //       this.beveraeBrandJSONDATA.push(brandObj)
  //     });
  //     ////console.log("beverage brand export", this.beveraeBrandJSONDATA);
  //   }
  // }

  // // export beverage brand excel 
  // readAsCSVBeverageBrand() {
  //   var options = {
  //     fieldSeparator: ',',
  //     quoteStrings: '"',
  //     decimalseparator: '.',
  //     showLabels: true,
  //     //  showTitle: true,
  //     //  title: 'Your title',
  //     useBom: true,
  //     noDownload: false,
  //     headers: ["brandCode", "brandName", "country", "status", "addedBy", "updatedBy", "updatedAt"]
  //   };
  //   new ngxCsv(this.beveraeBrandJSONDATA, 'Beverage_Brand_Export_' + this.date, options);
  // }

  // //get all beverage prouct
  // // getAllBeverageProducts() {
  // //   let data = this.allMenuData.allBeveragesProducts
  // //   ////console.log("All Beverage Products", data);
  // //   if (data['success'] == false) {
  // //     this.beverageProductCount = 0
  // //     this.noData13 = 0
  // //     ////console.log("Beverage product Data Not Found");
  // //   } else {
  // //    this.beverageproduct_data=data['data']
  // //     this.beverageProductCount = data['data'].length
  // //     data['data'].forEach(element => {
  // //       element.productCode = "P" + element.productCode
  // //     });
  // //     this.dataSourceBeverageProduct = new MatTableDataSource(data['data'])
  // //     this.dataSourceBeverageProduct.paginator = this.beverageProductspaginator
  // //     this.dataSourceBeverageProduct.sort = this.beverageProductsSort
  // //     this.noData13 = this.dataSourceBeverageProduct.connect().pipe(map(data => data.length === 0));

  // //     data['data'].forEach(element => {
  // //       var d = new Date(element.updatedAt)
  // //       var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  // //       var productObj = {}
  // //       productObj = {
  // //         "productCode": element.productCode,
  // //         "productName": element.productName,
  // //         "rootCategory": element.rootCategory,
  // //         "varient": element.varient,
  // //         "subVarient": element.subVarient,
  // //         "type": element.type,
  // //         "attributeSet": element.attributeSet.attributeSetName,
  // //         "associatedBrand": element.associatedBrand.brandName,
  // //         "unit": element.unit,
  // //         "status": element.status,
  // //         "addedBy": element.addedBy.userName,
  // //         "updatedBy": element.updatedBy.userName,
  // //         "updatedAt": date
  // //       }
  // //       this.beverageProductJSONDATA.push(productObj)
  // //     });
  // //     ////console.log("beverage product export", this.beverageProductJSONDATA);
  // //   }
  // // }

  // // export beverage product excel 
  // readAsCSVBeverageProduct() {
  //   var options = {
  //     fieldSeparator: ',',
  //     quoteStrings: '"',
  //     decimalseparator: '.',
  //     showLabels: true,
  //     //  showTitle: true,
  //     //  title: 'Your title',
  //     useBom: true,
  //     noDownload: false,
  //     headers: ["productCode", "productName", "rootCategory", "varient", "subVarient", "type", "attributeSet", "associatedBrand", "unit", "status", "addedBy", "updatedBy", "updatedAt"]
  //   };
  //   new ngxCsv(this.beverageProductJSONDATA, 'Beverage_Product_Export_' + this.date, options);
  // }

  // //get all retail food brands
  // getAllRetailFoodBrands() {
  //   let data = this.allMenuData.allRetailFoodBrands
  //   ////console.log("All Retail Food Brands", data);
  //   if (data['success'] == false) {
  //     this.retailFoodBrandCount = 0
  //     this.noData18=0
  //     ////console.log("Retail Food Brand Data Not Found");
  //   } else {
  //     this.retailfoodbrand_data=data['data']

  //     this.retailFoodBrandCount = data['data'].length
  //     data['data'].forEach(element => {
  //       element.brandCode = "B" + element.brandCode
  //     });
  //     this.dataSourceRetailFoodBrands = new MatTableDataSource(data['data'])
  //     this.dataSourceRetailFoodBrands.paginator = this.retailFoodBrandspaginator
  //     this.dataSourceRetailFoodBrands.sort = this.retailFoodBrandsSort
  //     this.noData18 = this.dataSourceRetailFoodBrands.connect().pipe(map(data => data.length === 0));
  //     data['data'].forEach(element => {
  //       var d = new Date(element.updatedAt)
  //       var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  //       var brandObj = {}
  //       brandObj = {
  //         "brandCode": element.brandCode,
  //         "brandName": element.brandName,
  //         "country": element.country,
  //         "status": element.status,
  //         "addedBy": element.addedBy.userName,
  //         "updatedBy": element.updatedBy.userName,
  //         "updatedAt": date
  //       }
  //       this.retailFoodBrandJSONDATA.push(brandObj)
  //     });
  //     ////console.log("retail food brand export", this.retailFoodBrandJSONDATA);
  //   }
  // }


  // // export retail food brand excel 
  // readAsCSVRetailFoodBrand() {
  //   var options = {
  //     fieldSeparator: ',',
  //     quoteStrings: '"',
  //     decimalseparator: '.',
  //     showLabels: true,
  //     //  showTitle: true,
  //     //  title: 'Your title',
  //     useBom: true,
  //     noDownload: false,
  //     headers: ["brandCode", "brandName", "country", "status", "addedBy", "updatedBy", "updatedAt"]
  //   };
  //   new ngxCsv(this.retailFoodBrandJSONDATA, 'Retail_Food_Brand_Export_' + this.date, options);
  // }



  // // get all retail food products
  // // getAllRetailFoodProducts() {
  // //   let data = this.allMenuData.allRetailFoodProducts
  // //   ////console.log("All Retail food Products", data);
  // //   this.retail_food_product = data['data']
  // //   if (data['success'] == false) {
  // //     this.retailFoodProductCount = 0
  // //     this.noData17=0
  // //     ////console.log("Retail Food Product Data Not Found");
  // //   } else {
  // //     this.retailFoodProductCount = data['data'].length
  // //     data['data'].forEach(element => {
  // //       element.productCode = "P" + element.productCode
  // //     });
  // //     this.dataSourceRetailFoodProduct = new MatTableDataSource(data['data'])

  // //     this.dataSourceRetailFoodProduct.paginator = this.retailFoodProductspaginator
  // //     this.dataSourceRetailFoodProduct.sort = this.retailFoodProductsSort
  // //     this.noData17 = this.dataSourceRetailFoodProduct.connect().pipe(map(data => data.length === 0));

  // //     data['data'].forEach(element => {
  // //       var d = new Date(element.updatedAt)
  // //       var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  // //       var productObj = {}
  // //       productObj = {
  // //         "productCode": element.productCode,
  // //         "productName": element.productName,
  // //         "rootCategory": element.rootCategory,
  // //         "varient": element.varient,
  // //         "subVarient": element.subVarient,
  // //         "type": element.type,
  // //         "attributeSet": element.attributeSet.attributeSetName,
  // //         "associatedBrand": element.associatedBrand.brandName,
  // //         "unit": element.unit,
  // //         "status": element.status,
  // //         "addedBy": element.addedBy.userName,
  // //         "updatedBy": element.updatedBy.userName,
  // //         "updatedAt": date
  // //       }
  // //       this.retailFoodProductJSONDATA.push(productObj)
  // //     });
  // //     ////console.log("retail food product export", this.retailFoodProductJSONDATA);
  // //   }
  // // }

  // // export retailfood product excel 
  // readAsCSVRetailFoodProduct() {
  //   var options = {
  //     fieldSeparator: ',',
  //     quoteStrings: '"',
  //     decimalseparator: '.',
  //     showLabels: true,
  //     //  showTitle: true,
  //     //  title: 'Your title',
  //     useBom: true,
  //     noDownload: false,
  //     headers: ["productCode", "productName", "rootCategory", "varient", "subVarient", "type", "attributeSet", "associatedBrand", "unit", "status", "addedBy", "updatedBy", "updatedAt"]
  //   };
  //   new ngxCsv(this.retailFoodProductJSONDATA, 'Retail_Food_Product_Export_' + this.date, options);
  // }


  //change filterable status of liquor attribute
  liquorAttributeFilterable(element, event) {
    ////console.log(event.checked);


    ////console.log(element._id);

    this.menuService.updateLiquorAttributeIsFilterable(element._id, { "isFilterable": event.checked }).subscribe(res => {
      ////console.log(res);
      if (res['sucess'] == true) {

        if (event.checked) {

          Swal.fire('Filterable Status Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Filterable Status Deactivated Successfully ', '', 'warning')
        }
      }
      else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
    })

  }

  //change searchable status of liquor attribute
  liquorAttributeSearchable(element, event) {
    ////console.log(event.checked);
    ////console.log(element._id);

    this.menuService.updateLiquorAttributeIsSearchable(element._id, { "isSearchable": event.checked }).subscribe(res => {
      ////console.log(res);
      if (res['sucess'] == true) {

        if (event.checked) {

          Swal.fire('Searchable status Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Searchable status Deactivated Successfully ', '', 'warning')
        }
      }
      else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
    })

  }



  //////////////////////////////////////////// Foods filter select tables Checkbox /////////////////////////////

  // on select 'all' checkbox all tables will display
  foodsCheckbox(event) {
    this.all = document.getElementById("all") as HTMLInputElement
    this.cuisine = document.getElementById("cuisine") as HTMLInputElement
    this.category = document.getElementById("category") as HTMLInputElement
    this.dish = document.getElementById("dish") as HTMLInputElement
    this.addon = document.getElementById("addon") as HTMLInputElement
    this.varient = document.getElementById("varient") as HTMLInputElement

    this.cuisine.checked = false
    this.category.checked = false
    this.dish.checked = false
    this.addon.checked = false
    this.varient.checked = false

    let eventVal = event.target.checked
    if (eventVal == true) {
      this.showCuisineTable = false
      this.showCategoryTable = false
      this.showDishTable = false
      this.showAddonTable = false
      this.showVarientTable = false
    } else {
      this.all.checked = true
    }
  }

  // on select cuisine checkbox cuisine table display and check other tables selected 
  foodsCuisineCheckbox(event) {
    ////console.log(event.target.checked);
    let eventVal = event.target.checked
    if (eventVal == true) {
      this.showCuisineTable = false
    } else {
      this.showCuisineTable = true
    }
    this.all = document.getElementById("all") as HTMLInputElement
    this.cuisine = document.getElementById("cuisine") as HTMLInputElement
    this.category = document.getElementById("category") as HTMLInputElement
    this.dish = document.getElementById("dish") as HTMLInputElement
    this.addon = document.getElementById("addon") as HTMLInputElement
    this.varient = document.getElementById("varient") as HTMLInputElement

    if (this.category.checked == true) {
      this.showCategoryTable = false
    } else {
      this.showCategoryTable = true
    }

    if (this.dish.checked == true) {
      this.showDishTable = false
    } else {
      this.showDishTable = true
    }

    if (this.addon.checked == true) {
      this.showAddonTable = false
    } else {
      this.showAddonTable = true
    }

    if (this.varient.checked == true) {
      this.showVarientTable = false
    } else {
      this.showVarientTable = true
    }

    //if all the checkbox of tables are selected then uncheck all and only show 'all' checkbox checked
    if (this.cuisine.checked == true && this.category.checked == true && this.dish.checked == true && this.addon.checked == true && this.varient.checked == true) {
      this.all.checked = true
      this.cuisine.checked = false
      this.category.checked = false
      this.dish.checked = false
      this.addon.checked = false
      this.varient.checked = false
    } else {
      this.all.checked = false
    }
  }

  // on select category checkbox category table display and check other tables selected 
  foodsCategoryCheckbox(event) {
    ////console.log(event.target.checked);
    let eventVal = event.target.checked
    if (eventVal == true) {
      this.showCategoryTable = false
    } else {
      this.showCategoryTable = true
    }

    this.all = document.getElementById("all") as HTMLInputElement
    this.cuisine = document.getElementById("cuisine") as HTMLInputElement
    this.category = document.getElementById("category") as HTMLInputElement
    this.dish = document.getElementById("dish") as HTMLInputElement
    this.addon = document.getElementById("addon") as HTMLInputElement
    this.varient = document.getElementById("varient") as HTMLInputElement

    if (this.cuisine.checked == true) {
      this.showCuisineTable = false
    } else {
      this.showCuisineTable = true
    }

    if (this.dish.checked == true) {
      this.showDishTable = false
    } else {
      this.showDishTable = true
    }

    if (this.addon.checked == true) {
      this.showAddonTable = false
    } else {
      this.showAddonTable = true
    }

    if (this.varient.checked == true) {
      this.showVarientTable = false
    } else {
      this.showVarientTable = true
    }

    //if all the checkbox of tables are selected then uncheck all and only show 'all' checkbox checked
    if (this.cuisine.checked == true && this.category.checked == true && this.dish.checked == true && this.addon.checked == true && this.varient.checked == true) {
      this.all.checked = true
      this.cuisine.checked = false
      this.category.checked = false
      this.dish.checked = false
      this.addon.checked = false
      this.varient.checked = false
    } else {
      this.all.checked = false
    }
  }

  // on select dish checkbox dish table display and check other tables selected 
  foodsDishCheckbox(event) {
    ////console.log(event.target.checked);
    let eventVal = event.target.checked
    if (eventVal == true) {
      this.showDishTable = false
    } else {
      this.showDishTable = true
    }

    this.all = document.getElementById("all") as HTMLInputElement
    this.cuisine = document.getElementById("cuisine") as HTMLInputElement
    this.category = document.getElementById("category") as HTMLInputElement
    this.dish = document.getElementById("dish") as HTMLInputElement
    this.addon = document.getElementById("addon") as HTMLInputElement
    this.varient = document.getElementById("varient") as HTMLInputElement

    if (this.cuisine.checked == true) {
      this.showCuisineTable = false
    } else {
      this.showCuisineTable = true
    }

    if (this.category.checked == true) {
      this.showCategoryTable = false
    } else {
      this.showCategoryTable = true
    }

    if (this.addon.checked == true) {
      this.showAddonTable = false
    } else {
      this.showAddonTable = true
    }

    if (this.varient.checked == true) {
      this.showVarientTable = false
    } else {
      this.showVarientTable = true
    }

    //if all the checkbox of tables are selected then uncheck all and only show 'all' checkbox checked
    if (this.cuisine.checked == true && this.category.checked == true && this.dish.checked == true && this.addon.checked == true && this.varient.checked == true) {
      this.all.checked = true
      this.cuisine.checked = false
      this.category.checked = false
      this.dish.checked = false
      this.addon.checked = false
      this.varient.checked = false
    } else {
      this.all.checked = false
    }
  }

  // on select addon checkbox addon table display and check other tables selected 
  foodsAddonCheckbox(event) {
    ////console.log(event.target.checked);
    let eventVal = event.target.checked
    if (eventVal == true) {
      this.showAddonTable = false
    } else {
      this.showAddonTable = true
    }

    this.all = document.getElementById("all") as HTMLInputElement
    this.cuisine = document.getElementById("cuisine") as HTMLInputElement
    this.category = document.getElementById("category") as HTMLInputElement
    this.dish = document.getElementById("dish") as HTMLInputElement
    this.addon = document.getElementById("addon") as HTMLInputElement
    this.varient = document.getElementById("varient") as HTMLInputElement

    if (this.cuisine.checked == true) {
      this.showCuisineTable = false
    } else {
      this.showCuisineTable = true
    }

    if (this.category.checked == true) {
      this.showCategoryTable = false
    } else {
      this.showCategoryTable = true
    }

    if (this.dish.checked == true) {
      this.showDishTable = false
    } else {
      this.showDishTable = true
    }

    if (this.varient.checked == true) {
      this.showVarientTable = false
    } else {
      this.showVarientTable = true
    }

    //if all the checkbox of tables are selected then uncheck all and only show 'all' checkbox checked
    if (this.cuisine.checked == true && this.category.checked == true && this.dish.checked == true && this.addon.checked == true && this.varient.checked == true) {
      this.all.checked = true
      this.cuisine.checked = false
      this.category.checked = false
      this.dish.checked = false
      this.addon.checked = false
      this.varient.checked = false
    } else {
      this.all.checked = false
    }
  }


  // on select varient checkbox varient table display and check other tables selected 
  foodsVarientCheckbox(event) {
    ////console.log(event.target.checked);
    let eventVal = event.target.checked
    if (eventVal == true) {
      this.showVarientTable = false
    } else {
      this.showVarientTable = true
    }

    this.all = document.getElementById("all") as HTMLInputElement
    this.cuisine = document.getElementById("cuisine") as HTMLInputElement
    this.category = document.getElementById("category") as HTMLInputElement
    this.dish = document.getElementById("dish") as HTMLInputElement
    this.addon = document.getElementById("addon") as HTMLInputElement
    this.varient = document.getElementById("varient") as HTMLInputElement

    if (this.cuisine.checked == true) {
      this.showCuisineTable = false
    } else {
      this.showCuisineTable = true
    }

    if (this.category.checked == true) {
      this.showCategoryTable = false
    } else {
      this.showCategoryTable = true
    }

    if (this.dish.checked == true) {
      this.showDishTable = false
    } else {
      this.showDishTable = true
    }

    if (this.addon.checked == true) {
      this.showAddonTable = false
    } else {
      this.showAddonTable = true
    }

    //if all the checkbox of tables are selected then uncheck all and only show 'all' checkbox checked
    if (this.cuisine.checked == true && this.category.checked == true && this.dish.checked == true && this.addon.checked == true && this.varient.checked == true) {
      this.all.checked = true
      this.cuisine.checked = false
      this.category.checked = false
      this.dish.checked = false
      this.addon.checked = false
      this.varient.checked = false
    } else {
      this.all.checked = false
    }
  }


  //view single cuisine
  viewCuisine(row): void {
    ////console.log(row);
    const dialogRef = this.dialog.open(ViewCuisineComponent, {
      data: row,
      disableClose: true,
      width: '60%',
      height: '80%',
    });

    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  //view single category
  viewCategory(obj): void {
    ////console.log(obj);
    const dialogRef = this.dialog.open(ViewCategoryComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });

    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  //view single dish
  viewDish(obj): void {
    ////console.log(obj);
    const dialogRef = this.dialog.open(ViewdishComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });

    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  //view single addons
  viewAddon(obj): void {
    ////console.log(obj);
    const dialogRef = this.dialog.open(ViewAddOnComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });

    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  //view single varient
  viewVarient(obj): void {
    ////console.log(obj);
    const dialogRef = this.dialog.open(ViewVarientComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  //view single liquor brand
  viewBrand(obj): void {
    ////console.log(obj);
    const dialogRef = this.dialog.open(ViewBrandComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  //view single liquor product
  viewProduct(obj): void {
    ////console.log(obj);
    const dialogRef = this.dialog.open(ViewProductComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  //view single liquor attribute
  viewLiquorAttribute(obj): void {
    ////console.log(obj);
    const dialogRef = this.dialog.open(ViewLiquorAttributeComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  //add attribute group popup
  // addLiquorAttributeGroup(keyType): void {
  //   ////console.log(keyType, "attribute group key");

  //   const dialogRef = this.dialog.open(AddLiquorAttributeGroupComponent, {
  //     data: keyType,
  //     disableClose: true,
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     ////console.log('The dialog was closed');
  //     // this.animal = result;
  //     this.getAllApis()
  //   });
  // }

  //add attribute set popup
  addLiquorAttributeSet(keyType): void {
    const dialogRef = this.dialog.open(AddLiquorAttributeSetComponent, {
      data: keyType,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      this.getAllLiquorAttributeSet()
    });
  }

  //view Attribute set popup
  viewLiquorAttributeSet(obj): void {
    const dialogRef = this.dialog.open(ViewLiquorAttributeSetComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  // view ingredient food inventory
  viewIngredientFoodinventory(obj): void {
    const dialogRef = this.dialog.open(ViewIngredientFoodInventoryComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  //view items food inventory
  viewItemsFoodInventory(obj): void {
    const dialogRef = this.dialog.open(ViewItemsFoodInventoryComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  // view equipment asset inventory
  viewEquipmentAssetEquipment(obj): void {
    const dialogRef = this.dialog.open(ViewEquipmentAssetInventoryComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }


  // view beverage brand
  viewBeverageBrand(obj): void {
    const dialogRef = this.dialog.open(ViewBeverageBrandComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
    });
  }

  // view beverage products
  viewBeverageProduct(obj): void {
    const dialogRef = this.dialog.open(ViewBeverageProductComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
    });
  }

  // view beverage
  viewBeverage(obj): void {
    const dialogRef = this.dialog.open(ViewBeverageComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
    });
  }

  //update cuisine 
  EditCuisine(obj) {
    ////console.log(obj);
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-cuisine")
  }

  editCategory(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-category")
  }

  editDish(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-dish")
  }

  editAddon(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-addon")
  }

  editVarient(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-varient")
  }

  editBrandLiquor(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-brand-liquor")
  }

  editProductLiquor(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-product-liquor")
  }


  editIngredientFoodInventory(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-ingredient-food-inventory")
  }

  editItemFoodInventory(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-item-food-inventory")
  }

  editEquipmentAssetInventory(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-equipment-asset-inventory")
  }

  editAttribute(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-attribute")
  }

  editAttributeSet(obj) {
    //console.log(obj);

    this.menuService.editObject = obj
    //console.log("",this.menuService.editObject);

    this.router.navigate(['/menu/update-attributeset'])
    // // const dialogRef = this.dialog.open(UpdateAttributeGroupSetComponent, {
    //   data: obj,
    //   disableClose: true,


    // });
    // //console.log(obj);
    // dialogRef.afterClosed().subscribe(result => {
    //   ////console.log('The dialog was closed');
    //   // this.animal = result;
    //   this.getAllApis()
    // });
  }


  editAttributeGroup(obj) {

    const dialogRef = this.dialog.open(UpdateAttributeGroupComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;

    });
  }


  // editBeverageBrand(obj): void {
  //   const dialogRef = this.dialog.open(UpdateBeverageBrandComponent, {
  //     data: obj,
  //     disableClose: true,
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     ////console.log('The dialog was closed');
  //     this.getAllApis()
  //   });
  // }

  // editBeverageProduct(obj) {
  //   this.menuService.editObject = obj
  //   this.router.navigateByUrl("/menu/update-beverage-product")
  // }

  // editRetailFoodProduct(obj) {
  //   this.menuService.editObject = obj
  //   this.router.navigateByUrl("/menu/update-retail-food-product")
  // }

  // editRetailFoodBrand(obj): void {
  //   const dialogRef = this.dialog.open(UpdateRetailFoodBrandComponent, {
  //     data: obj,
  //     disableClose: true,
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     ////console.log('The dialog was closed');
  //     this.getAllApis()
  //   });
  // }

  // editBeverage(obj) {
  //   this.menuService.editObject = obj
  //   this.router.navigateByUrl("/menu/update-beverage")
  // }



  //delete liquor brand
  // deleteBrandLiquor(obj) {

  //   ////console.log("delete", obj);
  //   this.menuService.deleteLiquorBrand(obj._id).subscribe(res => {
  //     ////console.log(res);
  //     if (res['sucess'] == true) {
  //       Swal.fire('Brand Delete Successfully', '', 'success')
  //       this.getAllApis()
  //     }
  //     else {
  //       Swal.fire('Failed to Delete Brand', 'Something went wrong', 'warning')
  //     }
  //   })
  // }

  // //delete liquor product
  // deleteProductLiquor(obj) {
  //   ////console.log("delete", obj);
  //   this.menuService.deleteLiquorProduct(obj._id).subscribe(res => {
  //     ////console.log(res);
  //     if (res['success'] == true) {
  //       Swal.fire('Product Delete Successfully', '', 'success')
  //       this.getAllApis()
  //     }
  //     else {
  //       Swal.fire('Failed to Delete Product', 'Something went wrong', 'warning')
  //     }
  //   })
  // }

  // //delete liquor attribute
  // deleteLiquorAttribute(obj) {
  //   ////console.log("delete", obj);
  //   this.menuService.deleteAttribute(obj._id).subscribe(res => {
  //     ////console.log(res);
  //     if (res['sucess'] == true) {
  //       Swal.fire('Attribute Delete Successfully', '', 'success')
  //       this.getAllApis()
  //     }
  //     else {
  //       Swal.fire('Failed to Delete Attribute', 'Something went wrong', 'warning')
  //     }
  //   })
  // }

  // //delete liquor attribute set
  // deleteLiquorAttributeSet(obj) {
  //   ////console.log("delete", obj);
  //   this.menuService.deleteAttributeSet(obj._id).subscribe(res => {
  //     ////console.log(res);
  //     if (res['sucess'] == true) {
  //       Swal.fire('Attribute Set Delete Successfully', '', 'success')
  //       this.getAllApis()
  //     }
  //     else {
  //       Swal.fire('Failed to Delete Attribute Set', 'Something went wrong', 'warning')
  //     }
  //   })
  // }


  // //delete ingredient food inventory
  // deleteIngredientFoodInventory(obj) {
  //   ////console.log("delete", obj);
  //   this.menuService.deleteIngredientFoodInventory(obj._id).subscribe(res => {
  //     ////console.log(res);
  //     if (res['sucess'] == true) {
  //       Swal.fire('Ingredient Delete Successfully', '', 'success')
  //       this.getAllApis()
  //     }
  //     else {
  //       Swal.fire('Failed to Delete Ingredient', 'Something went wrong', 'warning')
  //     }
  //   })
  // }

  // //delete item food inventory
  // deleteItemFoodInventory(obj) {
  //   ////console.log("delete", obj);
  //   this.menuService.deleteItemFoodInventory(obj._id).subscribe(res => {
  //     ////console.log(res);
  //     if (res['sucess'] == true) {
  //       Swal.fire('Item Delete Successfully', '', 'success')
  //       this.getAllApis()
  //     }
  //     else {
  //       Swal.fire('Failed to Delete Item', 'Something went wrong', 'warning')
  //     }
  //   })
  // }


  // // Delete equipment asset inventory
  // deleteEquipmentAssetInventory(obj) {
  //   ////console.log("delete", obj);
  //   this.menuService.deleteEquipmentAssetInventory(obj._id).subscribe(res => {
  //     ////console.log(res);
  //     if (res['success'] == true) {
  //       Swal.fire('Equipment Delete Successfully', '', 'success')
  //       this.getAllApis()
  //     }
  //     else {
  //       Swal.fire('Failed to Delete Equipment', 'Something went wrong', 'warning')
  //     }
  //   })
  // }

  /////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Soft Delete ///////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////

  // Soft Delete Brand Liquor
  liquorBrandSoftDelete(element, event) {
    ////console.log(event.checked);
    ////console.log(element._id);
    this.menuService.softDeleteLiquorBrand(element._id, { "status": event.checked }).subscribe(res => {
      ////console.log(res);

      if (res['sucess'] == true) {
        if (event.checked) {

          Swal.fire('Brand Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Brand Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllLiquorbrand();
    })
    // this.getAllLiquorbrand();
  }

  // Soft Delete Product Liquor
  liquorProductSoftDelete(element, event) {
    ////console.log(event.checked);
    ////console.log(element._id);
    this.menuService.softDeleteLiquorProduct(element._id, { "status": event.checked }).subscribe(res => {
      ////console.log(res);

      if (res['success'] == true) {
        if (event.checked) {

          Swal.fire('Product Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Product Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllLiquorProducts();
    })

  }

  // SOFT DELETE  ATTRIBUTE
  softDeleteAttribute(element, event) {
    ////console.log(event.checked);
    ////console.log(element._id);
    this.menuService.softDeleteAttribute(element._id, { "status": event.checked }).subscribe(res => {
      ////console.log(res);

      if (res['sucess'] == true) {
        if (event.checked) {

          Swal.fire('Attribute Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Attribute Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllLiquorAttribute()
    })

  }

  // SOFT DELETE ATTRIBUTE SET
  softDeleteAttributeSet(element, event) {
    ////console.log(event.checked);
    ////console.log(element._id);
    this.menuService.softDeleteAttributeSet(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['sucess'] == true) {
        if (event.checked) {

          Swal.fire('Attribute set Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Attribute set Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllLiquorAttributeSet()
    })

  }

  // SOFT DELETE INGREDIENT 
  // softDeleteIngrdient(element, event) {
  //   ////console.log(event.checked);
  //   ////console.log(element._id);
  //   this.menuService.softDeleteIngredient(element._id, { "status": event.checked }).subscribe(res => {
  //     ////console.log(res);

  //     if (res['sucess'] == true) {
  //       if (event.checked) {

  //         Swal.fire('Material Activated Successfully', '', 'success')
  //       }
  //       else {

  //         Swal.fire('Material Deactivated Successfully ', '', 'warning')
  //       }
  //     } else {
  //       Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
  //     }
  //   })
  // }

  // // SOFT DELETE BEVERAGE
  // softDeleteBeverage(element, event) {
  //   ////console.log(event.checked);
  //   ////console.log(element._id);
  //   this.menuService.softDeleteBeverage(element._id, { "status": event.checked }).subscribe(res => {
  //     ////console.log(res);

  //     if (res['success'] == true) {
  //       if (event.checked) {

  //         Swal.fire('Beverage Activated Successfully', '', 'success')
  //       }
  //       else {

  //         Swal.fire('Beverage Deactivated Successfully ', '', 'warning')
  //       }
  //     } else {
  //       Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
  //     }
  //   })
  // }

  // //SOFT DELETE BEVERAGE PRODUCT
  // softDeleteBeverageProduct(element, event) {
  //   ////console.log(event.checked);
  //   ////console.log(element._id);
  //   this.menuService.softDeleteBeverageProduct(element._id, { "status": event.checked }).subscribe(res => {
  //     ////console.log(res);

  //     if (res['success'] == true) {
  //       if (event.checked) {

  //         Swal.fire('Beverage Product Activated Successfully', '', 'success')
  //       }
  //       else {

  //         Swal.fire('Beverage Product Deactivated Successfully ', '', 'warning')
  //       }
  //     } else {
  //       Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
  //     }
  //   })
  // }

  // //SOFT DELETE BEVERAGE BRAND
  // softDeleteBeverageBrand(element, event) {
  //   ////console.log(event.checked);
  //   ////console.log(element._id);
  //   this.menuService.softDeleteBeverageBrand(element._id, { "status": event.checked }).subscribe(res => {
  //     ////console.log(res);

  //     if (res['success'] == true) {
  //       if (event.checked) {

  //         Swal.fire('Beverage Brand Activated Successfully', '', 'success')
  //       }
  //       else {

  //         Swal.fire('Beverage Brand Deactivated Successfully ', '', 'warning')
  //       }
  //     } else {
  //       Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
  //     }
  //   })
  // }


  // // SOFT DELETE RATAIL FOOD PRODUCT
  // softDeleteRetailFoodProduct(element, event) {
  //   ////console.log(event.checked);
  //   ////console.log(element._id);
  //   this.menuService.softDeleteRetailFoodProduct(element._id, { "status": event.checked }).subscribe(res => {
  //     ////console.log(res);

  //     if (res['success'] == true) {
  //       if (event.checked) {

  //         Swal.fire('Product Activated Successfully', '', 'success')
  //       }
  //       else {

  //         Swal.fire('Product Deactivated Successfully ', '', 'warning')
  //       }
  //     } else {
  //       Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
  //     }
  //   })
  // }

  // // SOFT DELETE RETAIL FOOD BRAND
  // softDeleteRetailFoodBrand(element, event) {
  //   ////console.log(event.checked);
  //   ////console.log(element._id);
  //   this.menuService.softDeleteRetailFoodBrand(element._id, { "status": event.checked }).subscribe(res => {
  //     ////console.log(res);

  //     if (res['success'] == true) {
  //       if (event.checked) {

  //         Swal.fire('Brand Activated Successfully', '', 'success')
  //       }
  //       else {

  //         Swal.fire('Brand Deactivated Successfully ', '', 'warning')
  //       }
  //     } else {

  //       Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
  //     }
  //   })
  // }

  // //pop up for add ingredient in Food Inventory
  // addingredients(): void {
  //   const dialogRef = this.dialog.open(AddIngredientsComponent, {
  //     width: '400px',
  //     disableClose: true,
  //     // data: {name: this.name, animal: this.animal}
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     ////console.log('The dialog was closed');
  //     this.getAllApis()
  //   });
  // }

  // //popup for add items in  > Food Inventory
  // addItems(): void {
  //   const dialogRef = this.dialog.open(AddItemsComponent, {
  //     width: '400px',
  //     disableClose: true,
  //     // data: {name: this.name, animal: this.animal}
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     ////console.log('The dialog was closed');
  //     this.getAllApis()
  //   });
  // }

  // //popup for add equipment in > Asset Inventory
  // openAddEquipment(): void {
  //   const dialogRef = this.dialog.open(AddEquipmentComponent, {
  //     width: '400px',
  //     disableClose: true,

  //     // data: {name: this.name, animal: this.animal}
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     ////console.log('The dialog was closed');
  //     this.getAllApis()
  //   });
  // }

  // //popup for add brand Beverages
  // openBeverageAddBrand(): void {
  //   const dialogRef = this.dialog.open(AddBeverageBrandComponent, {
  //     // width: '400px',
  //     disableClose: true,
  //     // data: {name: this.name, animal: this.animal}
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     ////console.log('The dialog was closed');
  //     this.getAllApis()
  //   });
  // }

  // //popup for add brand Retail food
  // openpopupRetailFoodAddBrand(): void {
  //   const dialogRef = this.dialog.open(AddRetailfoodBrandComponent, {
  //     disableClose: true,
  //   });
  //   dialogRef.afterClosed().subscribe(result => {
  //     ////console.log('The dialog was closed');
  //     this.getAllApis()
  //   });
  // }

  // //show and hide liquor categories tree
  liquorCategory(event) {
    ////console.log(event.srcElement.checked);
    if (event.srcElement.checked == true) {
      this.liquorSize = 8
      this.showLiquorCategories = true
    } else {
      this.liquorSize = 12
      this.showLiquorCategories = false

    }
  }

  // show and hide food inventory categories tree
  ingredientCategory(event) {
    ////console.log(event.srcElement.checked);
    if (event.srcElement.checked == true) {
      this.size = 8
      this.showIngredientCategories = true
    } else {
      this.size = 12
      this.showIngredientCategories = false

    }
  }

  //show and hide asset inventory categories tree
  inputCategory(event) {
    ////console.log(event.srcElement.checked);
    if (event.srcElement.checked == true) {
      this.inputsize = 8
      this.showInputCategories = true
    } else {
      this.inputsize = 12
      this.showInputCategories = false

    }
  }

  //show and hide beverages categories tree
  BeverageCategory(event) {
    ////console.log(event.srcElement.checked);
    if (event.srcElement.checked == true) {
      this.beverageSize = 8
      this.showBeverageCategories = true
    } else {
      this.beverageSize = 12
      this.showBeverageCategories = false
    }
  }

  //show and hide retail food categories tree
  retailFoodCategories(event) {
    ////console.log(event.srcElement.checked);
    if (event.srcElement.checked == true) {
      this.retailFoodSize = 8
      this.showRetailFoodCategories = true
    } else {
      this.retailFoodSize = 12
      this.showRetailFoodCategories = false
    }
  }



  ////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////// All Data Table Filters //////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////


  applyFilter(event) {
    this.dataSource.data = this.cuisine_data
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSource.data.filter(function (d) {
      ////console.log(d);
      let result
      let combination: any
      let combinationOfCusines: any
      combination = d['combinationOfCusines']
      if (combination.length > 0) {
        for (let j = 0; j < combination.length; j++) {
          combinationOfCusines = combination[j]['cuisineName'].toLowerCase().indexOf(val) !== -1
          if (combinationOfCusines == true) {
            break;
          }
        }
      }
      result = d['cuisineName'].toLowerCase().indexOf(val) !== -1 || d['cuisineCode'].toLowerCase().indexOf(val) !== -1 || d['country'].toLowerCase().indexOf(val) !== -1 || d['state'].toLowerCase().indexOf(val) !== -1 || d['city'].toLowerCase().indexOf(val) !== -1 || combinationOfCusines

      return result
    });

    ////console.log(filter);
    this.dataSource.data = filter;

  }
  applyFilterCategory(event) {
    this.dataSourceCategory.data = this.category_data
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceCategory.data.filter(function (d) {
      let result
      let tagslist: any
      let cuisine_list: any
      tagslist = d['tags']
      cuisine_list = d['cuisines']
      let cusine
      let tags
      if (tagslist.length > 0) {

        for (let j = 0; j < tagslist.length; j++) {
          tags = tagslist[j]['tagName'].toLowerCase().indexOf(val) !== -1
          if (tags == true) {
            break;
          }
        }
      }
      if (cuisine_list.length > 0) {

        for (let i = 0; i < cuisine_list.length; i++) {
          //console.log(cuisine_list[i]);

          cusine = cuisine_list[i]['cuisineName'].toLowerCase().indexOf(val) !== -1
          if (cusine == true) {
            break;
          }
        }
      }

      result = d['categoryName'].toLowerCase().indexOf(val) !== -1 || d['categoryCode'].toLowerCase().indexOf(val) !== -1 || cusine || tags
      return result
    });
    this.dataSourceCategory.data = filter;

  }
  applyFilterdish(event) {

    this.dataSourceDish.data = this.dish_data
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceDish.data.filter(function (d) {
      let result

      let varients_list: any
      let cuisine_list: any

      varients_list = d['varients']
      cuisine_list = d['cuisines']
      let varient
      let cusine
      ////console.log(varients_list);

      if (varients_list.length > 0) {

        for (let j = 0; j < varients_list.length; j++) {
          varient = varients_list[j]['varientName'].toLowerCase().indexOf(val) !== -1
          if (varient == true) {
            break;
          }
        }
      }
      if (cuisine_list.length > 0) {

        for (let i = 0; i < cuisine_list.length; i++) {
          //console.log(cuisine_list[i]);

          cusine = cuisine_list[i]['cuisineName'].toLowerCase().indexOf(val) !== -1
          if (cusine == true) {
            break;
          }
        }
      }


      result = d['dishName'].toLowerCase().indexOf(val) !== -1 || d['veg_nonveg'].toLowerCase().indexOf(val) !== -1 || d['dishCode'].toLowerCase().indexOf(val) !== -1 || varient || cusine


      return result

    });
    this.dataSourceDish.data = filter;
  }
  applyFilterAddOn(event) {

    this.dataSourceAddOn.data = this.addon_data
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceAddOn.data.filter(function (d) {
      ////console.log(d);
      let result
      let associatedDish: any
      associatedDish = d['dishes']
      let dish
      if (associatedDish.length > 0) {

        for (let j = 0; j < associatedDish.length; j++) {
          dish = associatedDish[j]['dishName'].toLowerCase().indexOf(val) !== -1
          if (dish == true) {
            break;
          }
        }
      }

      result = dish || d['adonName'].toLowerCase().indexOf(val) !== -1 || d['adonCode'].toLowerCase().indexOf(val) !== -1 || d['veg_nonveg'].toLowerCase().indexOf(val) !== -1

      return result
    });

    ////console.log(filter);
    this.dataSourceAddOn.data = filter;
  }
  applyFilterVarient(event) {
    ////console.log('HII', this.varient_data);
    ////console.log(event);
    this.dataSourceVarients.data = this.varient_data
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceVarients.data.filter(function (d) {
      ////console.log(d);
      let result
      let varientContent: any
      varientContent = d['varientContent']
      let contentName;
      if (varientContent.length > 0) {

        for (let j = 0; j < varientContent.length; j++) {
          contentName = varientContent[j]['contentName'].toLowerCase().indexOf(val) !== -1
          if (contentName == true) {
            break;
          }
        }
      }
      result = contentName || d['varientCode'].toLowerCase().indexOf(val) !== -1 || d['varientName'].toLowerCase().indexOf(val) !== -1

      return result
    });

    ////console.log(filter);
    this.dataSourceVarients.data = filter;
  }

  applyFilterLiquorBrand(event) {
    this.dataSourceLiquorBrand.data = this.liquorbrand_data
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceLiquorBrand.data.filter(function (d) {
      console.log(d);
      let result
      let root
      if (d['rootDrinkType'].length > 0) {

        for (let j = 0; j < d['rootDrinkType'].length; j++) {
          root = d['rootDrinkType'][j].toLowerCase().indexOf(val) !== -1
          if (root == true) {
            break;
          }
        }
      }
      result = root || d['brandName'].toLowerCase().indexOf(val) !== -1 || d['country'].toLowerCase().indexOf(val) !== -1 || d['brandCode'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceLiquorBrand.data = filter;
  }

  applyFilterLiquorProduct(event) {
    this.dataSourceLiquorProduct.data = this.liquorproduct_data
    ////console.log( this.dataSourceLiquorProduct.data);
    // debugger
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceLiquorProduct.data.filter(function (d) {
      console.log(d);
      //attribute_set
      let result
      let set;
      if (d['attribute_set'] != null) {

        for (let j = 0; j < d['attribute_set'].length; j++) {
          set = d['attribute_set'][j].toLowerCase().indexOf(val) !== -1
          if (set == true) {
            break;
          }
        }
      }
      if (d['root'] != undefined && d['subVarient'] != undefined && d['subsubVarient'] != undefined && d['varientType'] != undefined) {
        result = d['root']['root'].toLowerCase().indexOf(val) !== -1 || d['subVarient']['subVarient'].toLowerCase().indexOf(val) !== -1 || d['subsubVarient']['subsubVarient'].toLowerCase().indexOf(val) !== -1 || d['productCode'].toLowerCase().indexOf(val) !== -1 || d['brand']['brandName'].toLowerCase().indexOf(val) !== -1 || d['productName'].toLowerCase().indexOf(val) !== -1 || d['varientType']['varientType'].toLowerCase().indexOf(val) !== -1 || set
        return result
      }
      // else if (d['root'] == undefined) {
      //   result = d['productCode'].toLowerCase().indexOf(val) !== -1 || d['brand']['brandName'].toLowerCase().indexOf(val) !== -1 || d['productName'].toLowerCase().indexOf(val) !== -1 || set
      //   return result
      // }
       else if (d['subVarient'] == undefined) {
        result = d['root']['root'].toLowerCase().indexOf(val) !== -1 || d['productCode'].toLowerCase().indexOf(val) !== -1 || d['brand']['brandName'].toLowerCase().indexOf(val) !== -1 || d['productName'].toLowerCase().indexOf(val) !== -1 || set
        return result
      } else if (d['subsubVarient'] == undefined) {
        result = d['root']['root'].toLowerCase().indexOf(val) !== -1 || d['subVarient']['subVarient'].toLowerCase().indexOf(val) !== -1 || d['productCode'].toLowerCase().indexOf(val) !== -1 || d['brand']['brandName'].toLowerCase().indexOf(val) !== -1 || d['productName'].toLowerCase().indexOf(val) !== -1 || set
        return result
      }
       else if (d['varientType'] == undefined) {
        result = d['root']['root'].toLowerCase().indexOf(val) !== -1 || d['subVarient']['subVarient'].toLowerCase().indexOf(val) !== -1 || d['subsubVarient']['subsubVarient'].toLowerCase().indexOf(val) !== -1 || d['productCode'].toLowerCase().indexOf(val) !== -1 || d['brand']['brandName'].toLowerCase().indexOf(val) !== -1 || d['productName'].toLowerCase().indexOf(val) !== -1 || set
        return result
      }
    });


    ////console.log(filter);
    this.dataSourceLiquorProduct.data = filter;

  }

  applyFilterLiquorAttribute(event) {
    this.dataSourceLiquorAttribute.data = this.liquorattribute_data
    ////console.log( this.dataSourceLiquorAttribute.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceLiquorAttribute.data.filter(function (d) {
      ////console.log(d);
      let result

      result = d['attributeCode'].toLowerCase().indexOf(val) !== -1 || d['attributeName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceLiquorAttribute.data = filter;
  }

  applyFilterIngredientFoodInventory(filterValue: string) {
    this.dataSourceIngredient.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceIngredient.paginator) {
      this.dataSourceIngredient.paginator.firstPage();
    }
  }

  applyFilterInputFoodInventory(filterValue: string) {
    this.dataSourceInput.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceInput.paginator) {
      this.dataSourceInput.paginator.firstPage();
    }
  }

  applyFilterEquipment(filterValue: string) {
    this.dataSourceEquipment.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceEquipment.paginator) {
      this.dataSourceEquipment.paginator.firstPage();
    }
  }

  applyFilterBeverage(event) {
    this.dataSourceBeverage.data = this.beverage_data
    ////console.log( this.dataSourceBeverage.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceBeverage.data.filter(function (d) {
      ////console.log(d);
      let result

      result = d['beverageName'].toLowerCase().indexOf(val) !== -1 || d['beverageCode'].toLowerCase().indexOf(val) !== -1 || d['type'].toLowerCase().indexOf(val) !== -1 || d['subVarient'].toLowerCase().indexOf(val) !== -1 || d['varients'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceBeverage.data = filter;
  }

  applyFilterLiquorAttributeSet(event) {
    this.dataSourceLiquorAttributeSet.data = this.liquorattributeset_data
    ////console.log( this.dataSourceLiquorAttributeSet.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceLiquorAttributeSet.data.filter(function (d) {
      ////console.log(d);
      let result

      result = d['attributeSetCode'].toLowerCase().indexOf(val) !== -1 || d['attributeSetName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceLiquorAttributeSet.data = filter;
  }

  applyFilterBeverageAttributeSet(event) {
    this.dataSourceBeverageAttributeSet.data = this.beverageattributeset_data
    ////console.log(this.beverageattributeset_data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceBeverageAttributeSet.data.filter(function (d) {
      ////console.log(d);
      let result

      result = d['attributeSetCode'].toLowerCase().indexOf(val) !== -1 || d['attributeSetName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceBeverageAttributeSet.data = filter;
  }

  applyFilterretailFoodAttributeSet(event) {
    this.dataSourceRetailFoodAttributeSet.data = this.retailfoodattributeset_data
    // ////console.log(this.beverageattributeset_data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceRetailFoodAttributeSet.data.filter(function (d) {
      ////console.log(d);
      let result

      result = d['attributeSetCode'].toLowerCase().indexOf(val) !== -1 || d['attributeSetName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceRetailFoodAttributeSet.data = filter;
  }

  applyFilterBeverageBrand(event) {
    this.dataSourceBeverageBrands.data = this.beverageBrand_data
    ////console.log( this.dataSourceBeverageBrands.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceBeverageBrands.data.filter(function (d) {
      ////console.log(d);
      let result

      result = d['country'].toLowerCase().indexOf(val) !== -1 || d['brandCode'].toLowerCase().indexOf(val) !== -1 || d['brandName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceBeverageBrands.data = filter;
  }

  applyFilterBeveragesAttribute(event) {
    this.dataSourceBeverageAttribute.data = this.beverageattribute_data
    ////console.log( this.dataSourceBeverageAttribute.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceBeverageAttribute.data.filter(function (d) {
      ////console.log(d);
      let result

      result = d['attributeCode'].toLowerCase().indexOf(val) !== -1 || d['attributeName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceBeverageAttribute.data = filter;
  }

  applyFilterBeverageProducts(event) {
    this.dataSourceBeverageProduct.data = this.beverageproduct_data
    ////console.log( this.dataSourceBeverageProduct.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceBeverageProduct.data.filter(function (d) {
      ////console.log(d);
      let result

      result = d['productCode'].toLowerCase().indexOf(val) !== -1 || d['productName'].toLowerCase().indexOf(val) !== -1 || d['associatedBrand']['brandName'].toLowerCase().indexOf(val) !== -1 || d['varient'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceBeverageProduct.data = filter;

  }

  applyFilterRetailFoodBrand(event) {
    this.dataSourceRetailFoodBrands.data = this.retailfoodbrand_data
    ////console.log( this.dataSourceRetailFoodBrands.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceRetailFoodBrands.data.filter(function (d) {
      ////console.log(d);
      let result

      result = d['brandCode'].toLowerCase().indexOf(val) !== -1 || d['brandName'].toLowerCase().indexOf(val) !== -1 || d['country'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceRetailFoodBrands.data = filter;
  }


  applyFilterRetailFoodProduct(event) {
    ////console.log('HII', this.retail_food_product);
    ////console.log(event);
    this.dataSourceRetailFoodProduct.data = this.retail_food_product
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceRetailFoodProduct.data.filter(function (d) {
      return d['productName'].toLowerCase().indexOf(val) !== -1 || d['productCode'].toLowerCase().indexOf(val) !== -1 || d['varient'].toLowerCase().indexOf(val) !== -1 || d['associatedBrand']['brandName'].toLowerCase().indexOf(val) !== -1
    });

    ////console.log(filter);
    this.dataSourceRetailFoodProduct.data = filter;
  }

  applyFilterRetailFoodAttribute(event) {
    this.dataSourceRetailFoodAttribute.data = this.retailFoodattribute_data
    ////console.log( this.dataSourceRetailFoodAttribute.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceRetailFoodAttribute.data.filter(function (d) {
      ////console.log(d);
      let result

      result = d['attributeCode'].toLowerCase().indexOf(val) !== -1 || d['attributeName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    ////console.log(filter);
    this.dataSourceRetailFoodAttribute.data = filter;


  }

  ////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////// ///////////////    LIQUOR CATEGORY TREE //////////////////////////////  ///////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////


  getAllDrinks() {
    this.menuService.getAllRootDrinks().subscribe(data => {
      console.log(data);
      this.allDrinks = data['data']
      console.log(this.allDrinks);

      this.allDrinks.forEach((element, index) => {
        this.Rootdrinkcategory.push(element['rootDrinkType'])

        element.edit = false
        if (
          this.allDrinks[index].hasOwnProperty('liquorVarients')
          &&
          this.allDrinks[index].liquorVarients.length > 0
        ) {
          this.allDrinks[index].liquorVarients.forEach((element, subcategoryIndex = index) => {
            element.edit = false;
            if (
              this.allDrinks[index].liquorVarients[subcategoryIndex].hasOwnProperty('liquorSubVarients')
              &&
              this.allDrinks[index].liquorVarients[subcategoryIndex].liquorSubVarients.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.allDrinks[index].liquorVarients[subcategoryIndex].liquorSubVarients.forEach((element, subsubcategoryINdex = index) => {
                element.edit = false;
                if (
                  // tslint:disable-next-line: max-line-length
                  this.allDrinks[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.allDrinks[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  //console.log(element);
                  this.allDrinks[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                    element.edit = false;
                  })
                }


              })
            }
          })
        }



      });
      console.log(this.allDrinks);

    })
  }

  // *****************************  Liquor tree *******************
  //open tree node and close
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  // **********open root drink dialog**************

  openRootDialog() {
    const dialogRef = this.dialog.open(AddRootDrinkComponent, {
      width: '460px',
      disableClose: true,
      // height: '200px'
    })
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('After dialog closed');
      setTimeout(() => {
        this.getAllDrinks()
      }, 2000)
    })
  }

  // ********OPEN VARIENT DRINK DIALOG***************

  openVarientDialog(rootDrinkID) {
    const dialogRef = this.dialog.open(AddDrinkVarientComponent, {
      width: '460px',
      disableClose: true,
      data: rootDrinkID
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.getAllDrinks()
      }, 2000)
    })

  }

  //************open sub varient drink dialog box**************

  openSubVarientDialog(rootDrinkID, liquorVarient) {
    const dialogRef = this.dialog.open(AddDrinkSubvarientComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootDrinkID": rootDrinkID, "liquorVarient": liquorVarient }
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.getAllDrinks()
      }, 2000)
    })
  }

  // **************OPEN SUB SUB VARIENT DIALOG************
  openSubSubVarient(rootDrinkID, liquorVarient, liquorSubVarient) {
    const dialogRef = this.dialog.open(AddSubSubVarientComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootDrinkID": rootDrinkID, "liquorVarient": liquorVarient, "liquorSubVarient": liquorSubVarient }
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.getAllDrinks()
      }, 2000)
    })
  }



  /////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////// ASSET INVENTORY CATEGORY TREE //////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////


  getAllAssetInventoryCategory() {
    this.menuService.getAllAssetInventoryCategories().subscribe(data => {
      ////console.log("all asset inventory categories", data);
      this.allAssetInventoryCategory = data['data']
    })
  }
  // *********************** OPEN ROOT CATEGORY IN ASSET INVENTORY *************************8
  openInventoryRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,

      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB CATEGORY IN ASSET INVENTORY ********************** */
  openInventorySubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB SUB CATEGORY IN ASSET INVENTORY ********************** */
  openInventorySubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }

  //*************************OPEN SUB SUB CATEGORY TYPE IN ASSET INVENTORY ********************** */
  openInventorySubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }






  ///////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////// FOOD INVENTORY CATEGORY TREE ////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////



  getAllFoodInventoryCategory() {
    this.menuService.getAllFoodInventoryCategories().subscribe(data => {
      ////console.log("all food inventory categories", data);
      this.allFoodInventoryCategory = data['data']
    })
  }

  //*************************OPEN ROOT CATEGORY IN FOOD INVENTORY ********************** */
  openFoodInventoryRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryFoodInventoryComponent, {
      width: '460px',
      disableClose: true,
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }
  //*************************OPEN SUB CATEGORY IN FOOD INVENTORY ********************** */
  openFoodInventorySubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryFoodInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }
  //*************************OPEN SUB SUB CATEGORY IN FOOD INVENTORY ********************** */
  openFoodInventorySubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryFoodInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN FOOD INVENTORY ********************** */
  openFoodInventorySubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeFoodInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }



  /////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////// BEVERAGE CATEGORY TREE ////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////


  getAllBeveragesCategories() {
    this.menuService.getAllBeverageCategories().subscribe(data => {
      ////console.log("All Beverages Categories", data);
      if (data['sucess'] == false) {
        ////console.log("Beverages categories data not found");
      } else {
        this.allInHouseBeveragesCategories = data['data'].filter(element => element.beverageType == "inHouseBeverage")
        this.allRetailBeveragesCategories = data['data'].filter(element => element.beverageType == "retailBeverage")
      }
    })
  }

  //*************************OPEN ROOT CATEGORY IN BEVERAGE ********************** */
  openBeveragesRootCategoryDailog(beverageType) {
    const dialogRef = this.dialog.open(RootCategoryBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: beverageType
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }

  //*************************OPEN SUB CATEGORY IN BEVERAGE ********************** */
  openBeveragesSubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }

  //*************************OPEN SUB SUB CATEGORY IN BEVERAGE ********************** */
  openBeveragesSubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN BEVERAGE ********************** */
  openBeveragesSubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }




  ////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// RETAIL FOOD CATEGORY TREE //////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////


  getAllRetailFoodCategories() {
    this.menuService.getAllRetailFoodCategories().subscribe(data => {
      ////console.log("All Retail Food categories", data);
      this.allRetailFoodCategories = data['data']
    })
  }


  //*************************OPEN ROOT CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  //*************************OPEN SUB CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodSubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  //*************************OPEN SUB SUB CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodSubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN RETAIL FOOD ********************** */
  openRetailFoodSubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  //*************************Change status of cuisines ********************** */
  changeStatusCuisines(id, event) {

    this.menuService.softDeleteCusines(id, { "status": event.checked }).subscribe(Response => {
      if (Response['success']) {

      //console.log(Response);

        if (event.checked) {
          //console.log("hI");
          Swal.fire('Cuisines Activated Successfully', '', 'success')
          // this.getAllCuisines();

        }
        else {
          Swal.fire('Cuisines Deactivated Successfully ', '', 'warning')
          // this.getAllCuisines();
        }
        this.getAllCuisines();
      }

      //////console.log(Response);

    })

  }
  //*************************Change status of category ********************** */
  changeStatusCategory(id, event) {

    this.menuService.softDeleteCategory(id, { "status": event.checked }).subscribe(Response => {
      //console.log(Response);

      if (Response['sucess']) {
        if (event.checked) {

          Swal.fire('Category Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Category Deactivated Successfully ', '', 'warning')
        }
      }
      this.getAllCategories();
    })
   
  }

  //*************************Change status of Dish ********************** */
  changeStatusDish(id, event) {

    this.menuService.softDeleteDish(id, { "status": event.checked }).subscribe(Response => {
      //console.log(Response);

      if (Response['success']) {
        if (event.checked) {

          Swal.fire('Dish Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Dish Deactivated Successfully ', '', 'warning')
        }
      }
      this.getAllDishes();
      //////console.log(Response);

    })

  }

  //*************************Change status of Addon ********************** */
  changeStatusAddon(id, event) {

    this.menuService.softDeleteAddon(id, { "status": event.checked }).subscribe(Response => {
      if (Response['sucess']) {
        if (event.checked) {

          Swal.fire('Addon Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Addon Deactivated Successfully ', '', 'warning')
        }
      }
      this.getAllAddons();
      //////console.log(Response);

    })

  }
  //*************************Change status of cuisines ********************** */
  changeStatusVarient(id, event) {

    this.menuService.softDeleteVarient(id, { "status": event.checked }).subscribe(Response => {
      if (Response['success']) {
        if (event.checked) {

          Swal.fire('Varient Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Varient Deactivated Successfully ', '', 'warning')
        }
        this.getAllVarients();
      }
    })
  }
  filterstatuscusines(val) {
    console.log(val);

    this.dataSource.data = this.cuisine_data
    const filter = this.dataSource.data.filter(function (d) {
      console.log(d);
      let result
      result = d['status'] == val
      return result
    });
    this.dataSource.data = filter;
  }
  filterCusinieslist(cusines) {
    let data = []
    this.cusinesflag = true
    console.log(cusines);
    this.dataSourceCategory.data = this.category_data

    for (let i = 0; i < cusines.length; i++) {
      let filter = this.category_data.filter(function (d) {
        console.log(d);
        let result
        let cuisine_list = d['cuisines']
        let cusine
        if (cuisine_list.length > 0) {

          for (let j = 0; j < cuisine_list.length; j++) {
            //console.log(cuisine_list[i]);

            cusine = cuisine_list[j]['cuisineName'] == cusines[i]
            if (cusine == true) {
              break;
            }

          }
          result = cusine
        }

        // result = d['cuisines']== cusines[i] 
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceCategory.data = data
    this.dataSourceCategory.data = _.uniq(this.dataSourceCategory.data, '_id')
    if (data.length == 0) {
      this.dataSourceCategory.data = []

    }
    if (cusines.length == 0) {
      this.dataSourceCategory.data = this.category_data

    }

  }
  filtercountrylist(country) {
    this.country = true
    console.log(country);
    this.dataSource.data = this.cuisine_data
    let data = []
    for (let i = 0; i < country.length; i++) {
      let filter = this.cuisine_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['country'] == country[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSource.data = data

    if (data.length == 0) {
      this.dataSource.data = []

    }

    if (country.length == 0) {
      this.dataSource.data = this.cuisine_data

    }
  }
  showhide(name) {
    console.log(name);

    if (name == "country") {
      this.country = true
      this.status = false
      this.cuser = false
    }
    else if (name == "user") {
      this.country = false
      this.status = false
      this.cuser = true
    }
    else if (name == "status") {
      this.filterstatuscusines(true)
      this.status = true
      this.country = false
      this.cuser = false

    }

  }
  showhideCategory(name) {
    if (name == "cuisines") {
      this.cusinesflag = true
      this.statuscategory = false
      this.tagflag = false
      this.catuser = false
    } else if (name == "catuser") {
      this.cusinesflag = false
      this.statuscategory = false
      this.tagflag = false
      this.catuser = true

    }
    else if (name == "status") {
      this.filterstatusCategory(true)
      this.cusinesflag = false
      this.statuscategory = true
      this.tagflag = false
      this.catuser = false

    }
    else if (name == 'tag') {
      this.statuscategory = false;
      this.cusinesflag = false
      this.tagflag = true
      this.catuser = false

    }
  }

  showhideDish(name) {
    if (name == "cuisines") {
      this.cusinesDishflag = true
      this.statusDish = false
      this.vegflag = false
      this.dishuser = false
    } else if (name == "dishuser") {
      this.cusinesDishflag = false
      this.statusDish = false
      this.vegflag = false
      this.dishuser = true

    }
    else if (name == "status") {
      this.filterstatusDish(true)
      this.cusinesDishflag = false
      this.statusDish = true
      this.vegflag = false
      this.dishuser = false

    }
    else if (name == 'veg') {
      this.statusDish = false;
      this.cusinesDishflag = false
      this.vegflag = true
      this.dishuser = false

    }
  }
  //////////////////////////////////////////////////////show hide liquor brand filter
  showhideBrand(name) {
    if (name == "country") {
      this.brandcategory = false
      this.brandcountry = true
      this.brandstatus = false
      this.brand = false
      this.liquorbranduser = false
    }
    else if (name == "status") {
      this.filterstatusBrand(true)
      this.brandcategory = false
      this.brandcountry = false
      this.brandstatus = true
      this.brand = false
      this.liquorbranduser = false


    }
    else if (name == 'category') {
      this.brandcategory = true
      this.brandcountry = false
      this.brandstatus = false
      this.liquorbranduser = false
      this.brand = false
    }
    else if (name == 'brand') {
      this.brandcategory = false
      this.brandcountry = false
      this.brandstatus = false
      this.brand = true
      this.liquorbranduser = false
    }
    else if (name == 'user') {
      this.brandcategory = false
      this.brandcountry = false
      this.brandstatus = false
      this.brand = false
      this.liquorbranduser = true

    }
  }

  ////////////////////////////////////////////// show hide addon menu ///////////////////////////
  showhideAddon(name) {
    if (name == "dish") {
      this.addonDishflag = true
      this.statusaddon = false
      this.vegaddonflag = false
      this.addonuser = false
    }
    else if (name == "status") {
      this.filterstatusAddon(true)
      this.addonDishflag = false
      this.statusaddon = true
      this.vegaddonflag = false
      this.addonuser = false

    }
    else if (name == "useraddon") {
      this.addonDishflag = false
      this.statusaddon = false
      this.vegaddonflag = false
      this.addonuser = true
    }
    else if (name == 'veg') {
      this.statusaddon = false;
      this.addonDishflag = false
      this.vegaddonflag = true
      this.addonuser = false

    }
  }

  ////////////////////////////////////////////////////////////////////////////////categoryFilter///////////////////////////////////////
  filterstatusCategory(val) {
    console.log(val);

    this.dataSourceCategory.data = this.category_data
    const filter = this.dataSourceCategory.data.filter(function (d) {
      console.log(d);
      let result
      result = d['status'] == val
      return result
    });
    this.dataSourceCategory.data = filter;
  }
  /////////////////////////////////////////////////////////gettag///////////////////////////////////
  getTags() {
    this.menuService.getTags().subscribe(data => {
      this.tagslist = data['data']

      console.log('tags', this.tagslist);
      this.tagslist.forEach(element => {
        this.tag_list.push(element['tagName'])
      });
      console.log(this.tag_list);

    })
  }
  //////////////////////////////////////////////////////filter by tags in category /////////////////////////////////////////
  filtertaglist(tag) {
    let data = []
    this.tagflag = true
    console.log(tag);
    this.dataSourceCategory.data = this.category_data

    for (let i = 0; i < tag.length; i++) {
      let filter = this.category_data.filter(function (d) {
        console.log(d);
        let result
        let taglist = d['tags']
        let cusine
        if (taglist.length > 0) {

          for (let j = 0; j < taglist.length; j++) {
            //console.log(cuisine_list[i]);

            cusine = taglist[j]['tagName'] == tag[i]
            if (cusine == true) {
              break;
            }

          }
          result = cusine
        }

        // result = d['cuisines']== cusines[i] 
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceCategory.data = data
    this.dataSourceCategory.data = _.uniq(this.dataSourceCategory.data, '_id')

    if (tag.length == 0) {
      this.dataSourceCategory.data = this.category_data

    }
    if (data.length == 0) {
      this.dataSourceCategory.data = []

    }

  }
  /////////////////////////////////// filter status dish ///////////////////////////////////////////////////////////
  filterstatusDish(val) {
    console.log(val);

    this.dataSourceDish.data = this.dish_data
    const filter = this.dataSourceDish.data.filter(function (d) {
      console.log(d);
      let result
      result = d['status'] == val
      return result
    });
    this.dataSourceDish.data = filter;
  }
  /////////////////////////////////////////////// filter cusines list in dish ////////////////////////////////////
  filterCusinieslistindish(cusines) {
    let data = []
    this.cusinesDishflag = true
    console.log(cusines);
    this.dataSourceDish.data = this.dish_data

    for (let i = 0; i < cusines.length; i++) {
      let filter = this.dish_data.filter(function (d) {
        // console.log(d);
        let result
        let cuisine_list = d['cuisines']
        let cusine
        if (cuisine_list.length > 0) {

          for (let j = 0; j < cuisine_list.length; j++) {
            //console.log(cuisine_list[i]);

            cusine = cuisine_list[j]['cuisineName'] == cusines[i]
            if (cusine == true) {
              break;
            }

          }
          result = cusine
        }

        // result = d['cuisines']== cusines[i] 
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceDish.data = data
    this.dataSourceDish.data = _.uniq(this.dataSourceDish.data, '_id');
    if (data.length == 0) {
      this.dataSourceDish.data = []
    }
    if (cusines.length == 0) {
      this.dataSourceDish.data = this.dish_data
    }

  }
  ///////////////////////////////// checkbox filter for veg non veg ////////////////////////////////////////////
  change(e, value) {
    //console.log(e, value);

    if (e == true) {
      this.v.push(value)
    } else {
      this.v = this.v.filter(e => e !== value);
    }
    console.log(this.v);
    let veg = this.v

    this.vegflag = true
    // console.log(country);
    this.dataSourceDish.data = this.dish_data
    let data = []
    for (let i = 0; i < veg.length; i++) {
      let filter = this.dish_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['veg_nonveg'] == veg[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      // console.log(data);
    }
    this.dataSourceDish.data = data

    if (data.length == 0) {
      this.dataSourceDish.data = []

    }
    if (veg.length == 0) {
      this.dataSourceDish.data = this.dish_data
    }



  }

  /////////////////////////////////////////// addon veg nonveg filter /////////////////////////////
  changeAddon(e, value) {
    //console.log(e, value);

    if (e == true) {
      this.v1.push(value)
    } else {
      this.v1 = this.v1.filter(e => e !== value);
    }
    console.log(this.v1);
    let veg = this.v1

    this.vegflag = true
    // console.log(country);
    this.dataSourceAddOn.data = this.addon_data
    let data = []
    for (let i = 0; i < veg.length; i++) {
      let filter = this.addon_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['veg_nonveg'] == veg[i]
        return result
      });
      // console.log(filter);
      data = data.concat(filter)
      // console.log(data);
    }
    this.dataSourceAddOn.data = data

    if (data.length == 0) {
      this.dataSourceAddOn.data = []

    }
    if (veg.length == 0) {
      this.dataSourceAddOn.data = this.addon_data
    }



  }
  //////////////////////filterstatus Addon ///////////////////////////////
  filterstatusAddon(val) {
    console.log(val);

    this.dataSourceAddOn.data = this.addon_data
    const filter = this.dataSourceAddOn.data.filter(function (d) {
      console.log(d);
      let result
      result = d['status'] == val
      return result
    });
    this.dataSourceAddOn.data = filter;
  }
  //////////////////////////////////////////////////////////filter dishesh in addons ///////////////////////////
  filterdishlist(dish) {
    let data = []
    this.addonDishflag = true
    console.log(dish);
    this.dataSourceAddOn.data = this.addon_data

    for (let i = 0; i < dish.length; i++) {
      let filter = this.addon_data.filter(function (d) {
        console.log(d);
        let result
        let cuisine_list = d['dishes']
        let cusine
        if (cuisine_list.length > 0) {

          for (let j = 0; j < cuisine_list.length; j++) {
            //console.log(cuisine_list[i]);

            cusine = cuisine_list[j]['dishName'] == dish[i]
            if (cusine == true) {
              break;
            }

          }
          result = cusine
        }

        // result = d['cuisines']== cusines[i] 
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceAddOn.data = data
    this.dataSourceAddOn.data = _.uniq(this.dataSourceAddOn.data, '_id')
    if (data.length == 0) {
      this.dataSourceAddOn.data = []

    }
    if (dish.length == 0) {
      this.dataSourceAddOn.data = this.dish_data

    }
  }
  /////////////////////////////////filter brand status ////////////////////////////////////
  filterstatusBrand(val) {
    console.log(val);

    this.dataSourceLiquorBrand.data = this.liquorbrand_data
    const filter = this.dataSourceLiquorBrand.data.filter(function (d) {
      console.log(d);
      let result
      result = d['status'] == val
      return result
    });
    this.dataSourceLiquorBrand.data = filter;
  }
  ///////////////////////////////filter country for liquor brnad
  filtercountrylistforBrand(country) {
    this.brandcountry = true
    console.log(country);
    this.dataSourceLiquorBrand.data = this.liquorbrand_data
    let data = []
    for (let i = 0; i < country.length; i++) {
      let filter = this.liquorbrand_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['country'] == country[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceLiquorBrand.data = data

    if (data.length == 0) {
      this.dataSourceLiquorBrand.data = []

    }
    if (country.length == 0) {
      this.dataSourceLiquorBrand.data = this.liquorbrand_data

    }
  }

  ///////////////////////////////////////// filter brand ////////////////////////////////////////////////////////
  filterBrand(brand) {
    this.brand = true
    console.log(brand);
    this.dataSourceLiquorBrand.data = this.liquorbrand_data
    let data = []
    for (let i = 0; i < brand.length; i++) {
      let filter = this.liquorbrand_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['brandName'] == brand[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceLiquorBrand.data = data

    if (data.length == 0) {
      this.dataSourceLiquorBrand.data = []

    }

    if (brand.length == 0) {
      this.dataSourceLiquorBrand.data = this.liquorbrand_data

    }

  }
  /////////////////////////////////////////////////// filter root category ////////////////////////////
  filterRootcategory(category) {

    let data = []
    this.brandcategory = true
    console.log(category);
    this.dataSourceLiquorBrand.data = this.liquorbrand_data

    for (let i = 0; i < category.length; i++) {
      let filter = this.liquorbrand_data.filter(function (d) {
        console.log(d);
        let result
        let cuisine_list = d['rootDrinkType']
        let cusine
        if (cuisine_list.length > 0) {

          for (let j = 0; j < cuisine_list.length; j++) {
            //console.log(cuisine_list[i]);

            cusine = cuisine_list[j] == category[i]
            if (cusine == true) {
              break;
            }

          }
          result = cusine
        }

        // result = d['cuisines']== cusines[i] 
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceLiquorBrand.data = data
    this.dataSourceLiquorBrand.data = _.uniq(this.dataSourceLiquorBrand.data, '_id')
    if (data.length == 0) {
      this.dataSourceLiquorBrand.data = []
    }
    if (category.length == 0) {
      this.dataSourceLiquorBrand.data = this.liquorbrand_data
    }
  }
  //////////////////////////////////////////////////////////////////// show hide product filter menu ////////////////////////////
  showhideBrandProduct(name) {

    if (name == "status") {
      this.filterstatusProduct(true)
      this.productcategory = false
      this.productstatus = true
      this.brandp = false
      this.liquorproductuser = false
    }
    else if (name == 'category') {
      this.productcategory = true
      this.productstatus = false
      this.brandp = false
      this.liquorproductuser = false

    }
    else if (name == 'brand') {
      this.productcategory = false
      this.productstatus = false
      this.brandp = true
      this.liquorproductuser = false

    }
    else if (name == 'user') {
      this.productcategory = false
      this.productstatus = false
      this.brandp = false
      this.liquorproductuser = true

    }
  }
  ////////////////////////////////////////////////////////////////////// filter status product /////////////////////////////////////////////////////////
  filterstatusProduct(val) {
    console.log(val);

    this.dataSourceLiquorBrand.data = this.liquorproduct_data
    const filter = this.dataSourceLiquorProduct.data.filter(function (d) {
      console.log(d);
      let result
      result = d['status'] == val
      return result
    });
    this.dataSourceLiquorProduct.data = filter;
  }
  ///////////////////////////////////////// filter brand ////////////////////////////////////////////////////////
  filterBrandinProduct(brand) {
    this.brandp = true
    console.log(brand);
    this.dataSourceLiquorProduct.data = this.liquorproduct_data
    let data = []
    for (let i = 0; i < brand.length; i++) {
      let filter = this.liquorproduct_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['brand']['brandName'] == brand[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceLiquorProduct.data = data

    if (data.length == 0) {
      this.dataSourceLiquorProduct.data = []

    }

    if (brand.length == 0) {
      this.dataSourceLiquorProduct.data = this.liquorproduct_data

    }

  }
  /////////////////////////////////////////////////// filter root category ////////////////////////////
  filterRootcategoryinProduct(category) {

    let data = []
    this.productcategory = true
    console.log(category);
    this.dataSourceLiquorProduct.data = this.liquorproduct_data

    for (let i = 0; i < category.length; i++) {
      let filter = this.liquorproduct_data.filter(function (d) {
        console.log(d);
        let result

        let cusine

        cusine = d['root'] == category[i]
        result = cusine


        // result = d['cuisines']== cusines[i] 
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceLiquorProduct.data = data
    this.dataSourceLiquorProduct.data = _.uniq(this.dataSourceLiquorProduct.data, '_id')
    if (data.length == 0) {
      this.dataSourceLiquorProduct.data = []
    }
    if (category.length == 0) {
      this.dataSourceLiquorProduct.data = this.liquorproduct_data
    }
  }
  ////////////////////////////////////////////// show hide attribute list /////////////////////////////////////////////////////////////
  showhideAttribute(name) {
    if (name == "attribute") {
      this.searchflag = false
      this.attrstatus = false
      this.filterflag = false
      this.set = true
      this.liquorattruser = false

    }
    else if (name == "status") {
      this.filterstatusAttribute(true)
      this.searchflag = false
      this.attrstatus = true
      this.filterflag = false
      this.set = false
      this.liquorattruser = false

    }
    else if (name == 'search') {
      this.searchAttribute(true)
      this.searchflag = true
      this.attrstatus = false
      this.filterflag = false
      this.set = false
      this.liquorattruser = false

    }
    else if (name == 'filter') {
      this.filterAttribute(true)
      this.searchflag = false
      this.attrstatus = false
      this.filterflag = true
      this.set = false
      this.liquorattruser = false

    }
    else if (name == 'user') {
      this.searchflag = false
      this.attrstatus = false
      this.filterflag = false
      this.set = false
      this.liquorattruser = true

    }
  }
  ///////////////////////////////////////////////////// filter status attibute ////////////////////////
  filterstatusAttribute(val) {
    console.log(val);

    this.dataSourceLiquorAttribute.data = this.liquorattribute_data
    const filter = this.dataSourceLiquorAttribute.data.filter(function (d) {
      console.log(d);
      let result
      result = d['status'] == val
      return result
    });
    this.dataSourceLiquorAttribute.data = filter;
  }
  searchAttribute(val) {
    console.log(val);

    this.dataSourceLiquorAttribute.data = this.liquorattribute_data
    const filter = this.dataSourceLiquorAttribute.data.filter(function (d) {
      console.log(d);
      let result
      result = d['isSearchable'] == val
      return result
    });
    this.dataSourceLiquorAttribute.data = filter;
  }
  filterAttribute(val) {
    console.log(val);

    this.dataSourceLiquorAttribute.data = this.liquorattribute_data
    const filter = this.dataSourceLiquorAttribute.data.filter(function (d) {
      console.log(d);
      let result
      result = d['isFilterable'] == val
      return result
    });
    this.dataSourceLiquorAttribute.data = filter;
  }
  getadmin() {
    this.menuService.getAdmin().subscribe(data => {
      console.log("users", data);
      data['data'].forEach(element => {
        this.admin.push(element['userName'])
      });
      console.log(this.admin);

    })
  }
  filterAddedBy(user) {
    this.cuser = true
    console.log(user);
    this.dataSource.data = this.cuisine_data
    let data = []
    for (let i = 0; i < user.length; i++) {
      let filter = this.cuisine_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['addedBy']['userName'] == user[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSource.data = data

    if (data.length == 0) {
      this.dataSource.data = []

    }
    if (user.length == 0) {
      this.dataSource.data = this.cuisine_data

    }
  }

  ////////////////////////////////////////////////// added by in category /////////////////////////////////////////
  filterAddedByInCategory(user) {
    this.catuser = true
    console.log(user);
    this.dataSourceCategory.data = this.category_data
    let data = []
    for (let i = 0; i < user.length; i++) {
      let filter = this.category_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['addedBy']['userName'] == user[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceCategory.data = data

    if (data.length == 0) {
      this.dataSourceCategory.data = []

    }
    if (user.length == 0) {
      this.dataSourceCategory.data = this.category_data

    }
  }

  ////////////////////////////////////////////////// added by in category /////////////////////////////////////////
  filterAddedByInDish(user) {
    this.dishuser = true
    console.log(user);
    this.dataSourceDish.data = this.dish_data
    let data = []
    for (let i = 0; i < user.length; i++) {
      let filter = this.dish_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['addedBy']['userName'] == user[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceDish.data = data

    if (data.length == 0) {
      this.dataSourceDish.data = []

    }
    if (user.length == 0) {
      this.dataSourceDish.data = this.dish_data

    }
  }
  filterAddedByInAddon(user) {
    this.addonuser = true
    console.log(user);
    this.dataSourceAddOn.data = this.addon_data
    let data = []
    for (let i = 0; i < user.length; i++) {
      let filter = this.addon_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['addedBy']['userName'] == user[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceAddOn.data = data

    if (data.length == 0) {
      this.dataSourceAddOn.data = []

    }
    if (user.length == 0) {
      this.dataSourceAddOn.data = this.addon_data

    }
  }

  ///////////////////////////////////////////////// filter added by in liquor brand ////////////////////////////////////////
  filterAddedByInBrand(user) {
    this.liquorbranduser = true
    console.log(user);
    this.dataSourceLiquorBrand.data = this.liquorbrand_data
    let data = []
    for (let i = 0; i < user.length; i++) {
      let filter = this.liquorbrand_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['addedBy']['userName'] == user[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceLiquorBrand.data = data

    if (data.length == 0) {
      this.dataSourceLiquorBrand.data = []

    }
    if (user.length == 0) {
      this.dataSourceLiquorBrand.data = this.liquorbrand_data

    }
  }
  ///////////////////////////////////////////////// filter added by in liquor product ////////////////////////////////////////
  filterAddedByInProduct(user) {
    this.liquorproductuser = true
    console.log(user);
    this.dataSourceLiquorProduct.data = this.liquorproduct_data
    let data = []
    for (let i = 0; i < user.length; i++) {
      let filter = this.liquorproduct_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['addedBy']['userName'] == user[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceLiquorProduct.data = data

    if (data.length == 0) {
      this.dataSourceLiquorProduct.data = []

    }
    if (user.length == 0) {
      this.dataSourceLiquorProduct.data = this.liquorproduct_data

    }
  }
  ///////////////////////////////////////////////// filter added by in liquor product ////////////////////////////////////////
  filterAddedByInAttribute(user) {
    this.liquorattruser = true
    console.log(user);
    this.dataSourceLiquorAttribute.data = this.liquorattribute_data
    let data = []
    for (let i = 0; i < user.length; i++) {
      let filter = this.liquorattribute_data.filter(function (d) {
        // console.log(d);
        let result
        result = d['addedBy']['userName'] == user[i]
        return result
      });
      console.log(filter);
      data = data.concat(filter)
      console.log(data);
    }
    this.dataSourceLiquorAttribute.data = data

    if (data.length == 0) {
      this.dataSourceLiquorAttribute.data = []

    }
    if (user.length == 0) {
      this.dataSourceLiquorAttribute.data = this.liquorattribute_data

    }
  }
  filtervalue(obj) {
    console.log(obj);
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);
    console.log("cusines Filter:", obj);
    if (Object.keys(obj).length === 0) {
      this.dataSource.data = this.cuisine_data
    } else {
      let cuisine_filter = this.cuisine_data
      this.dataSource.data = this.cuisine_data
      console.log(cuisine_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('country1') && obj['country1'].length > 0) {
        filter = cuisine_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['country1'].includes(d['country'])
          return result
        });
        cuisine_filter = filter


      }


      if (obj.hasOwnProperty('cuisine1') && obj['cuisine1'].length > 0) {
        filter = cuisine_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['cuisine1'].includes(d['addedBy']['userName'])
          return result
        });
        cuisine_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = cuisine_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
      }
      console.log(filter);
      data = filter
      console.log(data);
      this.dataSource = new MatTableDataSource<any>(data);
      this.noData = this.dataSource.connect().pipe(map(data => data.length === 0));
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    }
  }
  resetcusinesfilter() {

    this.dataSource = new MatTableDataSource<any>(this.cuisine_data);
    this.noData = this.dataSource.connect().pipe(map(data => data.length === 0));
    this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
  }
  ////////////////////////////////// multi filter category //////////////////////////////////////////////////////
  filterCategoryvalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceCategory.data = this.category_data
    } else {
      let category_filter = this.category_data
      this.dataSource.data = this.category_data
      console.log(category_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('cuisine') && obj['cuisine'].length > 0) {
        filter = category_filter.filter(function (d) {
          console.log(d);
          let cuisine_list = d['cuisines']
          let cusine
          let result
          if (cuisine_list.length > 0) {

            for (let j = 0; j < cuisine_list.length; j++) {
              //console.log(cuisine_list[i]);

              result = obj['cuisine'].includes(cuisine_list[j]['cuisineName'])
              if (result == true) {
                break;
              }
            }
          }
          return result
        });
        category_filter = filter


      }
      if (obj.hasOwnProperty('tag') && obj['tag'].length > 0) {
        filter = category_filter.filter(function (d) {
          console.log(d);
          let tag_list = d['tags']
          let cusine
          let result
          if (tag_list.length > 0) {

            for (let j = 0; j < tag_list.length; j++) {
              //console.log(cuisine_list[i]);

              result = obj['tag'].includes(tag_list[j]['tagName'])
              if (result == true) {
                break;
              }
            }
          }
          return result
        });
        category_filter = filter


      }
      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = category_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        category_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = category_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
      }
      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceCategory = new MatTableDataSource<any>(data);
      this.noData1 = this.dataSourceCategory.connect().pipe(map(data => data.length === 0));
      this.dataSourceCategory.paginator = this.categorypaginator;
      this.dataSourceCategory.sort = this.categorysort;

    }
  }
  resetCategoryfilter() {
    this.dataSourceCategory = new MatTableDataSource<any>(this.category_data);
    this.noData1= this.dataSourceCategory.connect().pipe(map(data => data.length === 0));
    this.dataSourceCategory.paginator = this.categorypaginator;
      this.dataSourceCategory.sort = this.categorysort;

  }
  resetDishfilter() {
    this.dataSourceDish = new MatTableDataSource<any>(this.dish_data);
      this.noData2 = this.dataSourceDish.connect().pipe(map(data => data.length === 0));
      this.dataSourceDish.paginator = this.dishpaginator;
        this.dataSourceDish.sort = this.dishsort;
    setTimeout(() => {
    this.dishfilter.Veg = true
    this.dishfilter.SeaFood = true
    this.dishfilter.Egg = true
    this.dishfilter.NonVeg = true
    this.dishfilter.all = true
  },20)
  }
  selectAllinDish(event) {
    console.log(event.checked);

    if (event.checked == true) {
      this.dishfilter.Veg = true
      this.dishfilter.SeaFood = true
      this.dishfilter.Egg = true
      this.dishfilter.NonVeg = true
    } else {
      this.dishfilter.Veg = false
      this.dishfilter.SeaFood = false
      this.dishfilter.Egg = false
      this.dishfilter.NonVeg = false
    }



  }
  filterDishvalue(obj) {

    if (this.dishfilter.Veg== false || this.dishfilter.SeaFood == false || this.dishfilter.Egg == false || this.dishfilter.NonVeg == false) {
     
      this.dishfilter.all = false
    }
    else if (this.dishfilter.Veg== true && this.dishfilter.SeaFood == true && this.dishfilter.Egg == true && this.dishfilter.NonVeg == true) {
     
      this.dishfilter.all = true
    }
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceDish.data = this.dish_data
    } else {
      let dish_filter = this.dish_data
      this.dataSourceDish.data = this.dish_data
      console.log(dish_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('cuisine') && obj['cuisine'].length > 0) {
        filter = dish_filter.filter(function (d) {
          //  console.log(d);
          let cuisine_list = d['cuisines']
          let cusine
          let result
          if (cuisine_list.length > 0) {

            for (let j = 0; j < cuisine_list.length; j++) {
              //console.log(cuisine_list[i]);

              result = obj['cuisine'].includes(cuisine_list[j]['cuisineName'])
              if (result == true) {
                break;
              }
            }
          }
          return result
        });
        dish_filter = filter


      }

      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = dish_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        dish_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = dish_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        dish_filter = filter
      }
      this.v1 = []
      if (obj.hasOwnProperty('Veg') || obj.hasOwnProperty('NonVeg') || obj.hasOwnProperty('SeaFood') || obj.hasOwnProperty('Egg') || obj.hasOwnProperty('all')) {
        if (obj.hasOwnProperty('all') && obj['all'] == true) {
          this.v1 = ['Veg', 'Egg', 'Non-Veg', 'Sea Food']
        } else {
          this.v1 = []
        }
        if (obj.hasOwnProperty('Veg') && obj['Veg'] == true) {
          this.v1.push('Veg')
        }
        else if (obj.hasOwnProperty('Veg') && obj['Veg'] == false) {
          // this.v1 = this.v1.filter(e => e !== 'Veg');
          // console.log(this.v1);

          delete obj['Veg']


        }

        if (obj.hasOwnProperty('NonVeg') && obj['NonVeg'] == true) {
          this.v1.push('Non-Veg')
        }
        else if (obj.hasOwnProperty('NonVeg') && obj['NonVeg'] == false) {
          // this.v1 = this.v1.filter(e => e !== 'NonVeg');
          // console.log(this.v1);
          delete obj['NonVeg']


        }
        if (obj.hasOwnProperty('Egg') && obj['Egg'] == true) {
          this.v1.push('Egg')
        }
        else if (obj.hasOwnProperty('Egg') && obj['Egg'] == false) {
          delete obj['Egg']

        }
        if (obj.hasOwnProperty('SeaFood') && obj['SeaFood'] == true) {
          this.v1.push('Sea Food')
        }
        else if (obj.hasOwnProperty('SeaFood') && obj['SeaFood'] == false) {
          delete obj['SeaFood']


        }
        this.v1 = _.union(this.v1);
        console.log(this.v1, "><");
        let filter_veg = this.v1
        filter = dish_filter.filter(function (d) {
          // console.log(d);
          let result
          result = filter_veg.includes(d['veg_nonveg'])
          return result
        });
      }
      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceDish = new MatTableDataSource<any>(data);
      this.noData2 = this.dataSourceDish.connect().pipe(map(data => data.length === 0));
      this.dataSourceDish.paginator = this.dishpaginator;
      this.dataSourceDish.sort = this.dishsort;
    }
  }
  selectAllinAddon(event) {
    console.log(event.checked);

    if (event.checked == true) {
      this.addonfilter.Veg = true
      this.addonfilter.SeaFood = true
      this.addonfilter.Egg = true
      this.addonfilter.NonVeg = true
    } else {
      this.addonfilter.Veg = false
      this.addonfilter.SeaFood = false
      this.addonfilter.Egg = false
      this.addonfilter.NonVeg = false
    }
  }
  filterAddonvalue(obj) {
    if (this.addonfilter.Veg== false || this.addonfilter.SeaFood == false || this.addonfilter.Egg == false || this.addonfilter.NonVeg == false) {
     
      this.addonfilter.all = false
    }
    else if (this.addonfilter.Veg== true && this.addonfilter.SeaFood == true && this.addonfilter.Egg == true && this.addonfilter.NonVeg == true) {
     
      this.addonfilter.all = true
    }
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceAddOn.data = this.addon_data
    } else {
      let addon_filter = this.addon_data
      this.dataSourceAddOn.data = this.addon_data
      console.log(addon_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('dish') && obj['dish'].length > 0) {
        filter = addon_filter.filter(function (d) {
          console.log(d);
          let cuisine_list = d['dishes']
          let cusine
          let result
          if (cuisine_list.length > 0) {

            for (let j = 0; j < cuisine_list.length; j++) {
              //console.log(cuisine_list[i]);

              result = obj['dish'].includes(cuisine_list[j]['dishName'])
              if (result == true) {
                break;
              }
            }
          }
          return result
        });
        addon_filter = filter


      }

      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = addon_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        addon_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = addon_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        addon_filter = filter
      }
      this.v1 = []

      if (obj.hasOwnProperty('Veg') || obj.hasOwnProperty('NonVeg') || obj.hasOwnProperty('SeaFood') || obj.hasOwnProperty('Egg') || obj.hasOwnProperty('all')) {
        if (obj.hasOwnProperty('all') && obj['all'] == true) {
          this.v1 = ['Veg', 'Egg', 'Non-Veg', 'Sea Food']
        } else {
          this.v1 = []
        }
        if (obj.hasOwnProperty('Veg') && obj['Veg'] == true) {
          this.v1.push('Veg')
        }
        else if (obj.hasOwnProperty('Veg') && obj['Veg'] == false) {
          // this.v1 = this.v1.filter(e => e !== 'Veg');
          // console.log(this.v1);

          delete obj['Veg']


        }

        if (obj.hasOwnProperty('NonVeg') && obj['NonVeg'] == true) {
          this.v1.push('Non-Veg')
        }
        else if (obj.hasOwnProperty('NonVeg') && obj['NonVeg'] == false) {
          // this.v1 = this.v1.filter(e => e !== 'NonVeg');
          // console.log(this.v1);
          delete obj['NonVeg']


        }
        if (obj.hasOwnProperty('Egg') && obj['Egg'] == true) {
          this.v1.push('Egg')
        }
        else if (obj.hasOwnProperty('Egg') && obj['Egg'] == false) {
          delete obj['Egg']

        }
        if (obj.hasOwnProperty('SeaFood') && obj['SeaFood'] == true) {
          this.v1.push('Sea Food')
        }
        else if (obj.hasOwnProperty('SeaFood') && obj['SeaFood'] == false) {
          delete obj['SeaFood']


        }
        this.v1 = _.union(this.v1);
        console.log(this.v1, "><");
        let filter_veg = this.v1
        filter = addon_filter.filter(function (d) {
          // console.log(d);
          let result
          result = filter_veg.includes(d['veg_nonveg'])
          return result
        });
      }
      console.log(filter);
      data = filter
      console.log(data);
      // this.dataSourceAddOn.data = data
      this.dataSourceAddOn = new MatTableDataSource<any>(data);
      this.noData3 = this.dataSourceAddOn.connect().pipe(map(data => data.length === 0));
      this.dataSourceAddOn.paginator = this.addonpaginator;
      this.dataSourceAddOn.sort = this.addonsort;

    }
  }
  filterBrandvalue(obj) {
    // ;
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceLiquorBrand.data = this.liquorbrand_data
    } else {
      let brand_filter = this.liquorbrand_data
      this.dataSourceLiquorBrand.data = this.liquorbrand_data
      console.log(brand_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('brand') && obj['brand'].length > 0) {
        filter = brand_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['brand'].includes(d['brandName'])

          return result
        });
        brand_filter = filter


      }
      if (obj.hasOwnProperty('country') && (obj['country'].length > 0)) {
        filter = brand_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['country'].includes(d['country'])
          return result
        });
        brand_filter = filter
      }


      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = brand_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        brand_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = brand_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        brand_filter = filter
      }
      if (obj.hasOwnProperty('root') && obj['root'].length > 0) {
        filter = brand_filter.filter(function (d) {
          console.log(d);
          let root_list = d['rootDrinkType']
          let cusine
          let result
          if (root_list.length > 0) {

            for (let j = 0; j < root_list.length; j++) {
              //console.log(cuisine_list[i]);

              result = obj['root'].includes(root_list[j])
              if (result == true) {
                break;
              }
            }
          }
          return result
        });
        brand_filter = filter


      }

      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceLiquorBrand = new MatTableDataSource<any>(data);
      this.noData5 = this.dataSourceLiquorBrand.connect().pipe(map(data => data.length === 0));
      this.dataSourceLiquorBrand.paginator = this.liquorBrandpaginator;
      this.dataSourceLiquorBrand.sort = this.liquorBrandsort;
    }
  }
  filterProductvalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceLiquorProduct.data = this.liquorproduct_data
    } else {
      let product_filter = this.liquorproduct_data
      this.dataSourceLiquorProduct.data = this.liquorproduct_data
      console.log(product_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('brand') && obj['brand'].length > 0) {
        filter = product_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['brand'].includes(d['brand']['brandName'])

          return result
        });
        product_filter = filter


      }
      if (obj.hasOwnProperty('country') && obj['country'].length > 0) {
        filter = product_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['country'].includes(d['country'])
          return result
        });
        product_filter = filter
      }


      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = product_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        product_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = product_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        product_filter = filter
      }

      if (obj.hasOwnProperty('root') && obj['root'].length > 0) {
        filter = product_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['root'].includes(d['root']['root'])
          return result
        });
        product_filter = filter
      }

      console.log(filter);
      data = filter
      console.log(data);
      // this.dataSourceLiquorProduct.data = data
      this.dataSourceLiquorProduct = new MatTableDataSource<any>(data);
      this.noData6 = this.dataSourceLiquorProduct.connect().pipe(map(data => data.length === 0));
      this.dataSourceLiquorProduct.paginator = this.liquorProductpaginator;
        this.dataSourceLiquorProduct.sort = this.liquorProductsort;

    }

  }
  filterAttributevalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceLiquorAttribute.data = this.liquorattribute_data
    } else {
      let attribute_filter = this.liquorattribute_data
      this.dataSourceLiquorAttribute.data = this.liquorattribute_data
      console.log(attribute_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('filter') && obj['filter'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['filter'].includes(d['isFilterable'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('search') && obj['search'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['search'].includes(d['isSearchable'])
          return result
        });
        attribute_filter = filter
      }


      console.log(filter);
      data = filter
      console.log(data);
      // this.dataSourceLiquorAttribute.data = data
      this.dataSourceLiquorAttribute = new MatTableDataSource<any>(data);
      this.noData7 = this.dataSourceLiquorAttribute.connect().pipe(map(data => data.length === 0));
      this.dataSourceLiquorAttribute.paginator = this.liquorAttributepaginator;
        this.dataSourceLiquorAttribute.sort = this.liquorAttributesort;

    }


  }
  filterAttributeSetvalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceLiquorAttributeSet.data = this.liquorattributeset_data
    } else {
      let attributeSet_filter = this.liquorattributeset_data
      this.dataSourceLiquorAttributeSet.data = this.liquorattributeset_data
      console.log(attributeSet_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = attributeSet_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        attributeSet_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = attributeSet_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        attributeSet_filter = filter
      }

      console.log(filter);
      data = filter
      console.log(data);
      // this.dataSourceLiquorAttributeSet.data = data
      this.dataSourceLiquorAttributeSet = new MatTableDataSource<any>(data);
      this.noData8 = this.dataSourceLiquorAttributeSet.connect().pipe(map(data => data.length === 0));
      this.dataSourceLiquorAttributeSet.paginator = this.liquorAttributeSetpaginator;
      this.dataSourceLiquorAttributeSet.sort = this.liquorAttributeSetSort;

    }


  }
  filterVarientvalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceVarients.data = this.varient_data
    } else {
      let varient_filter = this.varient_data
      this.dataSourceVarients.data = this.varient_data
      console.log(varient_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = varient_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        varient_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = varient_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        varient_filter = filter
      }

      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceVarients = new MatTableDataSource<any>(data);
      this.noData4 = this.dataSourceVarients.connect().pipe(map(data => data.length === 0));
      this.dataSourceVarients.paginator = this.varientpaginator;
        this.dataSourceVarients.sort = this.varientsort;

    }


  }

  resetAddonfilter() {
    this.dataSourceAddOn = new MatTableDataSource<any>(this.addon_data);
    this.noData3 = this.dataSourceAddOn.connect().pipe(map(data => data.length === 0));
    this.dataSourceAddOn.paginator = this.addonpaginator;
      this.dataSourceAddOn.sort = this.addonsort;
    setTimeout(() => {
    this.addonfilter.Veg = true
    this.addonfilter.SeaFood = true
    this.addonfilter.Egg = true
    this.addonfilter.NonVeg = true 
    this.addonfilter.all=true},20) 
  }
  resetProductfilter() {
    this.dataSourceLiquorProduct = new MatTableDataSource<any>(this.liquorproduct_data);
    this.noData6 = this.dataSourceLiquorProduct.connect().pipe(map(data => data.length === 0));
    this.dataSourceLiquorProduct.paginator = this.liquorProductpaginator;
      this.dataSourceLiquorProduct.sort = this.liquorProductsort
  }
  resetBrandfilter() {
    this.dataSourceLiquorBrand = new MatTableDataSource<any>(this.liquorbrand_data);
    this.noData5 = this.dataSourceLiquorBrand.connect().pipe(map(data => data.length === 0));
    this.dataSourceLiquorBrand.paginator = this.liquorBrandpaginator;
      this.dataSourceLiquorBrand.sort = this.liquorBrandsort;
  }
  resetAttributefilter() {
    this.dataSourceLiquorBrand = new MatTableDataSource<any>(this.liquorattribute_data);
    this.noData7 = this.dataSourceLiquorBrand.connect().pipe(map(data => data.length === 0));
    this.dataSourceLiquorBrand.paginator = this.liquorAttributepaginator;
      this.dataSourceLiquorBrand.sort = this.liquorAttributesort;
  }
  resetAttributeSetfilter() {
    // this.dataSourceLiquorAttributeSet = this.liquorattributeset_data
    this.dataSourceLiquorAttributeSet = new MatTableDataSource<any>(this.liquorattributeset_data);
    this.noData8 = this.dataSourceLiquorAttributeSet.connect().pipe(map(data => data.length === 0));
    this.dataSourceLiquorAttributeSet.paginator = this.liquorAttributeSetpaginator;
      this.dataSourceLiquorAttributeSet.sort = this.liquorAttributeSetSort;
  }
  resetVarientfilter() {
    this.dataSourceVarients = new MatTableDataSource<any>(this.varient_data);
      this.noData4 = this.dataSourceVarients.connect().pipe(map(data => data.length === 0));
      this.dataSourceVarients.paginator = this.varientpaginator;
        this.dataSourceVarients.sort = this.varientsort;
  }
  updateTreeroot(root_i) {
    this.allDrinks[root_i].edit = true
  }
  updateLiquorTree(id, root, i) {
    console.log(id, root);
    let obj = {
      rootDrinkType: root
    }
    this.menuService.UpdateLiquorTreeRoot(id, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Root updated successfully", "", "success")
      this.getAllDrinks();

      this.getAllLiquorAttributeSet()
      this.getAllLiquorbrand()
      this.getAllLiquorbrand();
      this.allDrinks[i].edit = false
    })


  }
  cancelRoot(root_i) {
    this.allDrinks[root_i].edit = false
  }
  updateTreeVarient(root_i, varient_i) {
    this.allDrinks[root_i]['liquorVarients'][varient_i].edit = true
  }
  updateLiquorVarientTree(id, idv, varient, root_i, varient_i) {
    console.log(id, idv, varient, root_i, varient_i);
    let obj = {
      liquorVarientName: varient
    }
    this.menuService.UpdateLiquorTreeVarient(id, idv, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Varient updated successfully", "", "success")
      this.getAllDrinks();
      this.getAllLiquorAttributeSet()
      this.getAllLiquorbrand()
      this.getAllLiquorbrand();
      this.allDrinks[root_i]['liquorVarients'][varient_i].edit = false


    })

  }
  cancelVarient(root_i, varient_i) {
    this.allDrinks[root_i]['liquorVarients'][varient_i].edit = false
  }
  updateTreeSubVarient(root_i, varient_i, subVarient_i) {
    this.allDrinks[root_i]['liquorVarients'][varient_i]['liquorSubVarients'][subVarient_i].edit = true
  }
  updateLiquorsubVarientTree(id, idv, idsv, subvarient, root_i, varient_i, subvarient_i) {
    let obj = {
      liquorSubVarientName: subvarient
    }
    this.menuService.UpdateLiquorTreeVarientsub(id, idv, idsv, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Sub Varient updated successfully", "", "success")
      this.getAllDrinks();

      this.getAllLiquorAttributeSet()
      this.getAllLiquorbrand()
      this.getAllLiquorbrand();
      this.allDrinks[root_i]['liquorVarients'][varient_i]['liquorSubVarients'][subvarient_i].edit = false


    })

  }
  cancelSubVarient(root_i, varient_i, subvarient_i) {
    this.allDrinks[root_i]['liquorVarients'][varient_i]['liquorSubVarients'][subvarient_i].edit = false
  }
  updateTreeSubSubVarient(root_i, varient_i, subVarient_i, type_i) {
    console.log(root_i, varient_i, subVarient_i, type_i);
    this.allDrinks[root_i]['liquorVarients'][varient_i]['liquorSubVarients'][subVarient_i]['liquorSubSubVarientType'][type_i].edit = true
  }
  updateLiquorsubsubVarientTree(id, idv, idsv, idtype, type, root_i, varient_i, subVarient_i, type_i) {
    let obj = {
      liquorSubSubVarientTypeName: type
    }
    this.menuService.UpdateLiquorTreeVarientsubsubtype(id, idv, idsv, idtype, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Sub Varient updated successfully", "", "success")
      this.getAllDrinks();

      this.getAllLiquorAttributeSet()
      this.getAllLiquorbrand()
      this.getAllLiquorbrand();
      this.allDrinks[root_i]['liquorVarients'][varient_i]['liquorSubVarients'][subVarient_i]['liquorSubSubVarientType'][type_i].edit = false


    })

  }
  cancelSubSubVarient(root_i, varient_i, subvarient_i, type_i) {
    this.allDrinks[root_i]['liquorVarients'][varient_i]['liquorSubVarients'][subvarient_i]['liquorSubSubVarientType'][type_i].edit = false
  }
}