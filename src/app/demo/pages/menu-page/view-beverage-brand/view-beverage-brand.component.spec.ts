import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBeverageBrandComponent } from './view-beverage-brand.component';

describe('ViewBeverageBrandComponent', () => {
  let component: ViewBeverageBrandComponent;
  let fixture: ComponentFixture<ViewBeverageBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewBeverageBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewBeverageBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
