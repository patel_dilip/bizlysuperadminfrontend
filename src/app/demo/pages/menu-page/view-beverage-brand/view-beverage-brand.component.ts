import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';

@Component({
  selector: 'app-view-beverage-brand',
  templateUrl: './view-beverage-brand.component.html',
  styleUrls: ['./view-beverage-brand.component.scss']
})
export class ViewBeverageBrandComponent implements OnInit {
  allRetailFoodCategories=[];
  
  categoryType=[];
  retailFoodtree=[];


  constructor(public dialogRef: MatDialogRef<ViewBeverageBrandComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private menuService: AddMenuFoodsService) {
    console.log(data);
    this.getAllRetailFoodCategories()
    this.categoryType=data.categoryType

  }
   //open tree node and close
 caretClick() {
  var toggler = document.getElementsByClassName("caret");
  var i;
  for (i = 0; i < toggler.length; i++) {
    toggler[i].addEventListener("click", function () {
      this.parentElement.querySelector(".nested").classList.toggle("active");
      this.classList.toggle("caret-down");
    });
  }
}

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  getAllRetailFoodCategories() {
    this.menuService.getAllRetailFoodCategories().subscribe(data => {
      console.log("All Retail Food categories", data);
      this.allRetailFoodCategories = data['data']
      this.allRetailFoodCategories.forEach(element => {
        this.categoryType.forEach(ele=>{
        if(element['_id']==ele['_id'])
        element=ele
        
        })
         this.retailFoodtree.push(element)
      });
  this.allRetailFoodCategories=this.retailFoodtree
  console.log("asset inventory:", this.allRetailFoodCategories);
  
    })
  }
}