import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-liquor-attribute',
  templateUrl: './view-liquor-attribute.component.html',
  styleUrls: ['./view-liquor-attribute.component.scss']
})
export class ViewLiquorAttributeComponent implements OnInit {

  addedBy: any
  response: any

  radiobutton = false
  yesorNo = false
  inputbox = false
  checkbox = false
inputBoxPlaceholder:any
radioOptions:any
checkBoxOptions: any
  constructor(public dialogRef: MatDialogRef<ViewLiquorAttributeComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {

    console.log(data);
    this.response= data.responseType
    this.onClickResponse()
    //  this.addedBy = data['addedBy'].userName
  }

  //on click respone dropdown respective response input open
  onClickResponse() {
    console.log(this.response.elementName);
    if (this.response.elementName == "Input Box") {
      this.inputBoxPlaceholder=this.response.fieldValue
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true
      this.checkbox = false
    }
    if (this.response.elementName == "Yes / No") {
     
      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false
      this.checkbox = false
    }
    if (this.response.elementName == "Radio Button") {
      this.radioOptions = this.response.options
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = false
    }
    if (this.response.elementName == "Check Box") {
      this.checkBoxOptions = this.response.options
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = true

    }

  }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}