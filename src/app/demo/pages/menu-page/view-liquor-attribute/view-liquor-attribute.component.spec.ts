import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLiquorAttributeComponent } from './view-liquor-attribute.component';

describe('ViewLiquorAttributeComponent', () => {
  let component: ViewLiquorAttributeComponent;
  let fixture: ComponentFixture<ViewLiquorAttributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLiquorAttributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLiquorAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
