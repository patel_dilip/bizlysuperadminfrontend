import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RootCategoryAssetInventoryComponent } from './root-category-asset-inventory.component';

describe('RootCategoryAssetInventoryComponent', () => {
  let component: RootCategoryAssetInventoryComponent;
  let fixture: ComponentFixture<RootCategoryAssetInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RootCategoryAssetInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RootCategoryAssetInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
