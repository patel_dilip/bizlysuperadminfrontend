import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSubCategoryTypeAssetInventoryComponent } from './sub-sub-category-type-asset-inventory.component';

describe('SubSubCategoryTypeAssetInventoryComponent', () => {
  let component: SubSubCategoryTypeAssetInventoryComponent;
  let fixture: ComponentFixture<SubSubCategoryTypeAssetInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSubCategoryTypeAssetInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSubCategoryTypeAssetInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
