import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubCategoryAssetInventoryComponent } from './sub-category-asset-inventory.component';

describe('SubCategoryAssetInventoryComponent', () => {
  let component: SubCategoryAssetInventoryComponent;
  let fixture: ComponentFixture<SubCategoryAssetInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubCategoryAssetInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubCategoryAssetInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
