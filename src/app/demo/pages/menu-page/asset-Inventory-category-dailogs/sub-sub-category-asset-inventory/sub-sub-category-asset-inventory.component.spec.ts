import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubSubCategoryAssetInventoryComponent } from './sub-sub-category-asset-inventory.component';

describe('SubSubCategoryAssetInventoryComponent', () => {
  let component: SubSubCategoryAssetInventoryComponent;
  let fixture: ComponentFixture<SubSubCategoryAssetInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubSubCategoryAssetInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubSubCategoryAssetInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
