import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material';
import { AddCuisinesComponent } from 'src/app/PopoversList/add-cuisines/add-cuisines.component';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AddVarientPopoverComponent } from 'src/app/PopoversList/add-varient-popover/add-varient-popover.component';

const URL = environment.base_Url + 'dish/uploadphotos'
@Component({
  selector: 'app-update-dish',
  templateUrl: './update-dish.component.html',
  styleUrls: ['./update-dish.component.scss']
})
export class UpdateDishComponent implements OnInit {

  dishUnits: string[] = ['Kilogram', 'Grams', 'Per Plate'];
  addDishForm: FormGroup
  cuisineCtrl: FormControl
  selectedAlbums = []
  selectedvarients = []
  uploader: FileUploader;
  response: string
  multiResponse = []
  cuisines = []
  tempCuisines = []
  varients = []
  tempVariens = []
  loginUserID: any
  loading = false;
  dishObject: any
  dishID: any
  cusines_data=[];
 // cusines_data=[];
  constructor(private fb: FormBuilder, private matDialog: MatDialog,
    private menuService: AddMenuFoodsService, private router: Router) {

    //get the dish object for update
    this.dishObject = this.menuService.editObject

    console.log("edit obj", this.dishObject);
    if (this.dishObject == undefined) {
      this.router.navigateByUrl("/menu")
    }

    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID)

    //get all cusines and varients funtion
    this.getAllCuisines()
    this.getAllVarient()

    this.addDishes()
    this.addDishForm.patchValue({
      dishName: this.dishObject.dishName,
      unit: this.dishObject.unit,
      veg_nonveg: this.dishObject.veg_nonveg
    })
    this.selectedAlbums = this.dishObject.cuisines
    this.selectedvarients = this.dishObject.varients
    this.multiResponse = this.dishObject.images
    this.dishID = this.dishObject._id
  }

  ngOnInit() {


    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'dishes'
    });
    //file upload response with url
    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.response = JSON.parse(response)
      console.log('response', this.response['dishPhotoUrls']);

      this.multiResponse.push(this.response['dishPhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      this.loading = false
    };

  }


  setLoading() {
    this.loading = true
  }

  //get all cuisines list
  getAllCuisines() {
  this.cusines_data=[]
    this.menuService.getCuisines().subscribe(data => {
      console.log("cuisines", data);
      this.cuisines = data['data']
      for (var i = 0; i < this.cuisines.length; i++) {
        for (var j = 0; j < this.dishObject.cuisines.length; j++) {
          if (this.cuisines[i]._id === this.dishObject.cuisines[j]._id) {
            this.cuisines.splice(i, 1);
          
          }
        }
      }
      this.cuisines.forEach(element => {
        if(element.status==true){
        this.cusines_data.push({cuisineName:element['cuisineName'],_id:element['_id']})
        }  });
        console.log(this.cusines_data);
        this.cuisines=this.cusines_data
    })
  }

  //get all vareints list
  getAllVarient() {
    this.tempVariens=[]
    this.menuService.getAllvarients().subscribe(data => {
      console.log("varients", data);
      this.varients = data['data']

      for (var i = 0; i < this.varients.length; i++) {
        for (var j = 0; j < this.dishObject.varients.length; j++) {
          if (this.varients[i]._id === this.dishObject.varients[j]._id ) {
            this.varients.splice(i, 1);

            // this.tempVariens = this.varients
          }
        }
      }
this.varients.forEach(element => {
  if(element.status==true){
this.tempVariens.push(element)
  }
  this.varients=this.tempVariens
});
     // this.tempVariens = data['data']
    })
  }

  //add cuisines form 
  addDishes() {
    this.addDishForm = this.fb.group({
      dishName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z0-9]+$')]],
      unit: ['', Validators.required],
      veg_nonveg: ['', Validators.required],
      cuisines: [''],
      varients: [''],
      images: [],
      userid: [''],
      updatedBy: ['']
    })
  }


  applyFilterCuisine(event) {
    this.cuisines = []
    this.cuisines = this.cusines_data
   // this.cuisines = this.tempCuisines['cuisineName']
    console.log(event.target.value);
    if (event.target.value == "") {
      this.cuisines = this.cusines_data
    } else {
      const val = event.target.value.trim().toLowerCase();
    const filter = this.cusines_data.filter(function (d) {
      //console.log(d);
      let result
      console.log("ele", d);        
      result = d['cuisineName'].toLowerCase().indexOf(val) !== -1 
  
      return result
    });

    //console.log(filter);
    this.cuisines = filter;
    }
     
   console.log(">>>", this.cuisines);
   
    
  }



  applyFilterVarient(event) {
    // varientName
    this.varients = this.tempVariens
    console.log(event.target.value);
    if (event.target.value == "") {
      this.varients = this.tempVariens
    } else {
      
      const val = event.target.value.trim().toLowerCase();
    const filter = this.varients.filter(function (d) {
      //console.log(d);
      let result
      console.log("ele", d);        
      result = d['varientName'].toLowerCase().indexOf(val) !== -1 
  
      return result
    });

    //console.log(filter);
    this.varients = filter;
    
    }
  }

  //on select cuisine chip add to selectedAlbums array and remove from cuisines
  onCheckCuisine(s) {
    if (this.cuisines.includes(s)) {
      this.cuisines.splice(this.cuisines.indexOf(s), 1)
     
      for( var i = 0; i < this.tempCuisines.length; i++){ 
        if ( this.tempCuisines[i]._id === s._id) {
          this.tempCuisines.splice(i, 1); 
        }
      }
      if (this.selectedAlbums.includes(s.cuisine)) {
        alert("Already exists in selection")
      } else {
        this.selectedAlbums.push(s)
      }
    }
    console.log(this.selectedAlbums);

  }


  // remove from selected cuisine array
  removePubs(s) {
    if (this.selectedAlbums.includes(s)) {
      this.selectedAlbums.splice(this.selectedAlbums.indexOf(s), 1)
      this.cuisines.push(s);
      
    }

  }

  //on select varient chip add to selectedvarients array and remove from varients
  onCheckvarients(s) {
    console.log(s);

    if (this.varients.includes(s)) {
      this.varients.splice(this.varients.indexOf(s), 1)
    
      for( var i = 0; i < this.tempVariens.length; i++){ 
        if ( this.tempVariens[i]._id === s._id) {
          this.tempVariens.splice(i, 1); 
        }
      }
      if (this.selectedvarients.includes(s.varient)) {
        alert("Already exists in selection")
      } else {
        this.selectedvarients.push(s)
      }
    }
    console.log(this.selectedvarients);

  }


  // remove from selected varients array
  removevarients(s) {
    if (this.selectedvarients.includes(s)) {
      this.selectedvarients.splice(this.selectedvarients.indexOf(s), 1)
      this.varients.push(s);
    
    }

  }

  openAddCuisineDialog(): void {
    const dialogRef = this.matDialog.open(AddCuisinesComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,

    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getAllCuisines()
    });

  }

  openAddVarientDialog(): void {
    const dialogRef = this.matDialog.open(AddVarientPopoverComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,

    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getAllVarient()
    });

  }


  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }

  //add final dish
  onAddAlbum() {
    this.loading = true
    //get selected cuisine ids and varient ids
    let cuisineIDS = []
    let varientIDS = []
    this.selectedAlbums.forEach(element => {
      cuisineIDS.push(element._id)
    })
    this.selectedvarients.forEach(element => {
      varientIDS.push(element._id)
    })

    //patch all value to final object
    this.addDishForm.patchValue({
      cuisines: cuisineIDS,
      varients: varientIDS,
      images: this.multiResponse,
      userid: this.loginUserID,
      updatedBy: this.loginUserID
    })
    console.log(this.addDishForm.value);
    this.menuService.updateDish( this.dishID,this.addDishForm.value).subscribe(res => {
      this.loading = false
      console.log("add dish", res);
      if (res['sucess'] == true) {

        Swal.fire('Dish Updated Successfully', '', 'success')
        this.router.navigate(['/menu'])
      }
      else {
        Swal.fire('Failed to Update Dish', 'Something went wrong', 'warning')
      }
    })
  }

}

