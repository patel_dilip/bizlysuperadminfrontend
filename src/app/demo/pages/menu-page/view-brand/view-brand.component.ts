import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';

@Component({
  selector: 'app-view-brand',
  templateUrl: './view-brand.component.html',
  styleUrls: ['./view-brand.component.scss']
})
export class ViewBrandComponent implements OnInit {
  addedBy: any
  addMenuFoodsService: any;
  DrinkTypes: any;
  categoryType= [];
  DrinkTree=[];
  constructor(private menu:AddMenuFoodsService,public dialogRef: MatDialogRef<ViewBrandComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    console.log(data);
    this.addedBy = data['addedBy'].userName
    this.categoryType=data['categoryType']
    this.getRootTypes()
  }
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }
  getRootTypes() {

    this.menu.getAllRootDrinks().subscribe(data => {
      this.DrinkTypes = data['data']

      console.log('drinktypes', this.DrinkTypes);
      this.DrinkTypes.forEach(element => {
        this.categoryType.forEach(ele=>{
        if(element['_id']==ele['_id'])
        element=ele
        
        })
         this.DrinkTree.push(element)
      });
this.DrinkTypes=this.DrinkTree
console.log(this.categoryType);

    })
  }
  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}