import { Component, OnInit } from '@angular/core';
  import { FormGroup, FormBuilder, Validators } from '@angular/forms';
  import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
  import { FileUploader } from 'ng2-file-upload';
  import { environment } from 'src/environments/environment';
  import Swal from 'sweetalert2';
  import { Router } from '@angular/router';
  import { element } from 'protractor';
import { MatDialog } from '@angular/material';
import { AddRetailfoodBrandComponent } from '../add-retailfood-brand/add-retailfood-brand.component';
import { SubSubCategoryTypeRetailFoodComponent } from '../retail-food-category-dailogs/sub-sub-category-type-retail-food/sub-sub-category-type-retail-food.component';
import { RootCategoryRetailFoodComponent } from '../retail-food-category-dailogs/root-category-retail-food/root-category-retail-food.component';
import { SubCategoryRetailFoodComponent } from '../retail-food-category-dailogs/sub-category-retail-food/sub-category-retail-food.component';
import { SubSubCategoryRetailFoodComponent } from '../retail-food-category-dailogs/sub-sub-category-retail-food/sub-sub-category-retail-food.component';
  
  const URL = environment.base_Url + 'retailfoodproduct/uploadphoto'
  

@Component({
  selector: 'app-update-retail-food-product',
  templateUrl: './update-retail-food-product.component.html',
  styleUrls: ['./update-retail-food-product.component.scss']
})
export class UpdateRetailFoodProductComponent implements OnInit {

  addProductForm: FormGroup
  
    attributes = []
    units = ['Gram',"Kg"]
    brands = []
    allRetailBeveragesCategories: any
    loginUserID: any
    uploader: FileUploader
    response: string;
  productIMG: any
    productObj: any
    productID: any
  countbrand: number;
  allBrands=[];
  disableBrandDropdown: boolean;
  count: number;
  attributeData: Object;
  set=[]
  attribute_setlist=[]
  attribute=[]
  filteredArr=[]
  inputBoxPlaceholder: any;
  radiobutton: boolean;
  yesorNo: boolean;
  inputbox: boolean;
  checkbox: boolean;
  radioOptions: any;
  checkBoxOptions: any;
  rootCategory: any;
  varient: any;
  subVarient: any;
  type: any;
  category: { category: any; id: any; };
  cat: any;
  rootcategory: any;
  subcategory: any;
  subsubcategory: any;
  typecategory: any;
    constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService, 
      private router: Router , private dialog: MatDialog) {
  
      //get the product object for update
      this.productObj = this.menuService.editObject
  
      console.log("edit obj", this.productObj);
      if (this.productObj == undefined) {
        this.router.navigateByUrl("/beverages_and_retailfood")
      }
      if(this.productObj['rootCategory']!=undefined){
        this.rootcategory=this.productObj['rootCategory']['rootCategory']
        }
        if(this.productObj['varient']!=undefined){
        this.subcategory=this.productObj['varient']['varient'];
        }
        if(this.productObj['subVarient']!=undefined){
      this.subsubcategory=this.productObj['subVarient']['subVarient'];
        }
        if(this.productObj['type']!=undefined){
      this.typecategory=this.productObj['type']['type'];
        }
     
  
      //get login user details
      let loginUser = JSON.parse(localStorage.getItem('loginUser'))
      this.loginUserID = loginUser._id
      console.log(this.loginUserID);
      // this.cat=this.productObj['category']['category']
  let categoryOBJ = {
    id: this.productObj['category']['id'],
   string:this.productObj['category']['category']
}
let obj=
   {
     id: this.productObj['category']['id'],
     string:this.productObj['category']['category'],
attributeType:"retailfood"

}
console.log("hi");
console.log(categoryOBJ)
// await new Promise(resolve => {
this.menuService.getBrandsByCategoryinretailFood(categoryOBJ).subscribe(data => {
 this.countbrand=0
this.allBrands=[]
 console.log("Brand>",data);
 this.allBrands = data['data']
             
   this.disableBrandDropdown = false
      this.allBrands = data['data']    
})
// console.log(";en",this.allBrands)

 this.menuService.getattributeDatafromotherCategories(obj).subscribe(data => {
   this.count=0
   this.set=[]
 console.log("attr>",data);
 this.attributeData = data
this.set= this.attributeData['attributeSets']
//  this.attribute_setlist=this.attributeData['attributeSets']
this.attributeData['attributeSets'].forEach(element => {
 
 this.attribute_setlist.push(element['attributeSetName'])
});
console.log("list>",this.attribute_setlist);
// this.addProductForm.patchValue({
//   attribute_set:this.attribute_setlist
// })
console.log();
this.attributeData['groupAttribute'].forEach(element => {
 console.log(element ['assignAttributes']);
 this.attribute=this.attribute.concat(element ['assignAttributes'])
}); 
console.log(this.attribute,">");
this.filteredArr = this.attribute.reduce((acc, current) => {
 const x = acc.find(item => item['_id'] === current['_id']);
 if (!x) {
   return acc.concat([current]);
 } else {
   return acc;
 }
}, []);
console.log(this.filteredArr);
// this.onClickResponse();

})  
  
      this.getAllRetailFoodCategories()
      this.getAllRetailFoodBrands()
      this.getAllAttributeSet()
      this.addproductFormfunction()
  
      this.addProductForm.patchValue({
        productName: this.productObj.productName,
        rootCategory:this.productObj.rootCategory,
        varient: this.productObj.varient,
        subVarient: this.productObj.subVarient,
        type: this.productObj.type,
        attributeSet: this.productObj.attributeSet,
        unit: this.productObj.unit,
        category:this.productObj['category'],
        associatedBrand: this.productObj.associatedBrand['_id'],
        productImage: this.productObj.productImage,
      })
      this.productIMG = this.productObj.productImage
      this.productID = this.productObj._id
      console.log( ">",this.addProductForm.value);
      
    }
  
    ngOnInit() {
      //  NG FILE UPLOADER CODE
      this.uploader = new FileUploader({
        url: URL,
        itemAlias: 'beverageproduct'
  
      });
  
      this.response = '';
  
      this.uploader.response.subscribe(res => this.response = res);
  
      this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
  
        this.addProductForm.patchValue({
          productImage: response
        })
        this.productIMG = response
      }
     
    }
  
    addproductFormfunction() {
      this.addProductForm = this.fb.group({
        productName: ['', Validators.required],
        rootCategory: {},
        varient: {},
        subVarient: {},
        type: {},
        attributeSet: [],
        unit: ['', Validators.required],
        associatedBrand: ['', Validators.required],
        productImage: [''],
        category:{},
        userid: this.loginUserID
      })
    }
  
  
    getAllRetailFoodCategories() {
      this.menuService.getAllRetailFoodCategories().subscribe(data => {
        console.log("All Retail food Categories", data);
        if (data['sucess'] == false) {
          console.log("Retail Food categories data not found");
        } else {
          this.allRetailBeveragesCategories = data['data']
        }
      })
    }

  //*************************OPEN ROOT CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  //*************************OPEN SUB CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodSubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  //*************************OPEN SUB SUB CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodSubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN RETAIL FOOD ********************** */
  openRetailFoodSubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  
  
    //get all beverage brands
    getAllRetailFoodBrands() {
      this.menuService.getAllRetailFoodBrands().subscribe(data => {
        console.log("All Retail Food Brands", data);
        this.brands = data['data']
      
      })
    }
  
  
    //get all attribute set
    getAllAttributeSet() {
      this.menuService.getAllLiquorAttributeSets().subscribe(data => {
        console.log("All attribute sets", data);
      this.attributes = data['data'].filter(ele => ele.attributeType == "retailFood")
      })
    }
  
     //open tree node and close
     caretClick() {
      var toggler = document.getElementsByClassName("caret");
      var i;
      for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function () {
          this.parentElement.querySelector(".nested").classList.toggle("active");
          this.classList.toggle("caret-down");
        });
      }
    }
  
   
  //popup for add brand Retail food
  openpopupRetailFoodAddBrand(): void {
    const dialogRef = this.dialog.open(AddRetailfoodBrandComponent, {
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
     this.getAllRetailFoodBrands()
    });
  }
    onAddProduct() {
    const brand = this.brands.find(ele => ele.brandName == this.addProductForm.controls.associatedBrand.value)
    const attributeSet = this.attributes.find(ele => ele.attributeSetName == this.addProductForm.controls.attributeSet.value)
    
    this.addProductForm.patchValue({
    //  attributeSet: attributeSet,
      // associatedBrand: brand._i
    })
    
    console.log(this.addProductForm.value);
      this.menuService.updateRetailFoodProduct(this.productID, this.addProductForm.value).subscribe(res => {
        console.log(res);
  
        if (res['success'] == true) {
  
          Swal.fire('Product Updated Successfully', '', 'success')
          this.router.navigate(['beverages_and_retailfood'])
        }
        else {
          Swal.fire('Failed to Update Product', 'Something went wrong', 'warning')
        }
      })
    }
    onClickResponse(i) {

      // this.filteredArr.forEach((element, index) => {
  
      //  if(i==index-1){
      // console.log(this.filteredArr[i].responseType.elementName);
      if (this.filteredArr[i].responseType.elementName == "Input Box") {
        this.inputBoxPlaceholder = this.filteredArr[i].responseType.fieldValue
        this.radiobutton = false
        this.yesorNo = false
        this.inputbox = true
        this.checkbox = false
      }
      if (this.filteredArr[i].responseType.elementName == "Yes / No") {
  
        this.radiobutton = false
        this.yesorNo = true
        this.inputbox = false
        this.checkbox = false
      }
      if (this.filteredArr[i].responseType.elementName == "Radio Button") {
        this.radioOptions = this.filteredArr[i].responseType.options
        this.radiobutton = true
        this.yesorNo = false
        this.inputbox = false
        this.checkbox = false
      }
      if (this.filteredArr[i].responseType.elementName == "Check Box") {
        this.checkBoxOptions = this.filteredArr[i].responseType.options
        this.radiobutton = false
        this.yesorNo = false
        this.inputbox = false
        this.checkbox = true
  
      }
      // }
  
    }
    getroot(r?:any,sub?:any,subsub?:any, type?:any){
      this.rootcategory=''
    this.subcategory=''
  this.subsubcategory=''
  this.typecategory=''
        this.rootCategory={rootCategory:r['rootCategoryName'],id:r['_id']}
      if(sub!=undefined){
      this.varient={varient:sub['subCategoryName'],id:sub['_id']}
      }
      if(subsub!=undefined){
      this.subVarient={subVarient:subsub['subSubCategoryName'],id:subsub['_id']}
      }
      if(type!=undefined){
      this.type={type:type['subSubCategoryTypeName'],id:type['_id']}
      }
        console.log(r,">", sub,">", subsub,">", type);
        
         this.addProductForm.patchValue({
        rootCategory:this.rootCategory,
        varient: this.varient, 
        subVarient:  this.subVarient,
        type:this.type,
        category:this.category
      })
    }
    getRadiovalue(id, name){
      // fa
      this.allBrands=[]
      this.filteredArr=[]
     this.set=[]
     this.attribute_setlist=[]
      this.category={category:name, id:id};
      console.log(id,">>", this.category);
  
      let categoryOBJ = {
         id: id,
        string:name
    }
    let obj=
        {
          id: id,
      string:name,
    attributeType:"retailfood"
   
    }
    console.log("hi");
    console.log(categoryOBJ)
    // await new Promise(resolve => {
    this.menuService.getBrandsByCategoryinretailFood(categoryOBJ).subscribe(data => {
      this.countbrand=0
  // this.allBrands=[]
      console.log("Brand>",data);
      this.allBrands = data['data']
                  
        this.disableBrandDropdown = false
           this.allBrands = data['data']    
    })
  // console.log(";en",this.allBrands)
    
      this.menuService.getattributeDatafromotherCategories(obj).subscribe(data => {
        // this.set=[]
        this.count=0
      console.log("attr>",data);
      this.attributeData = data
     this.set= this.attributeData['attributeSets']
    //  this.attribute_setlist=this.attributeData['attributeSets']
    this.attributeData['attributeSets'].forEach(element => {
      
      this.attribute_setlist.push(element['attributeSetName'])
    });
    console.log("list>",this.attribute_setlist);
    // this.addProductForm.patchValue({
    //   attribute_set:this.attribute_setlist
    // })
    console.log();
    this.attributeData['groupAttribute'].forEach(element => {
      console.log(element['assignAttributes']);
      this.attribute=this.attribute.concat(element ['assignAttributes'])
    }); 
    console.log(this.attribute,">");
    this.filteredArr = this.attribute.reduce((acc, current) => {
      const x = acc.find(item => item['_id'] === current['_id']);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
    console.log(this.filteredArr);
    // this.onClickResponse();
    
    })  
    
  // });
  setTimeout(()=> {
                
  if(this.allBrands.length==0 && this.set.length==0){
      Swal.fire('Brand and Attribute not available', '', 'warning')
    }
    else if(this.set.length==0){      
      Swal.fire('Attribute not available', '', 'warning')
    } else if(this.allBrands.length==0){
      Swal.fire('Brands not available', '', 'warning')
    
    }
  }, 3000);
  
    }
  }
  