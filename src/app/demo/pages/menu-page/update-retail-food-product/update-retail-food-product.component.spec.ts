import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRetailFoodProductComponent } from './update-retail-food-product.component';

describe('UpdateRetailFoodProductComponent', () => {
  let component: UpdateRetailFoodProductComponent;
  let fixture: ComponentFixture<UpdateRetailFoodProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateRetailFoodProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRetailFoodProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
