import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { AddBrandFoodInvnetoryComponent } from '../add-brand-food-invnetory/add-brand-food-invnetory.component';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AddIngredientsComponent } from '../add-ingredients/add-ingredients.component';

@Component({
  selector: 'app-add-items',
  templateUrl: './add-items.component.html',
  styleUrls: ['./add-items.component.scss']
})
export class AddItemsComponent implements OnInit {
  additemForm: FormGroup
  quantity: string[] = ['Liter-Mililiter',"Kg-Gram"]
  loginUserID: any
  allIngredient: any
  allBrand: any
  add: any;
  constructor(public dialogRef: MatDialogRef<AddItemsComponent>,
    private fb: FormBuilder, private dialog: MatDialog,
    private menuService: AddMenuFoodsService, private router: Router) {

    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);

    this.getAllBrand()
    this.getAllIngredient()
  }

  ngOnInit() {
    this.additemForm = this.fb.group({
      itemName: ['', [Validators.required, Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      brand: ['', [Validators.required]],
      ingredient: ['', [Validators.required]],
      measurement: ['', [Validators.required]],
      quantity:this.fb.array([]),
      userid: this.loginUserID
    })
    this.addQuantityList()

  }
  convertintograms(inkg,i){
  
    let g=inkg*1000
    this.additemForm.controls.quantity['controls'][i].patchValue({
      gram:g
    })
   
  }
  convertintoKg(gram,i){
    console.log(this.additemForm.get('quantity').value[i]);
    let kg=gram/1000
    this.additemForm.controls.quantity['controls'][i].patchValue({
      kg:kg
    })
    



  }
  RemoveQuantityList(i){
    console.log(i);
    this.add.removeAt(i);
    console.log(this.add.value);
    
    
  }
addQuantityList(){
  this.add = this.additemForm.get('quantity') as FormArray;
    this.add.push(this.fb.group({
      kg: [''],
     gram: ['']
    }))
    console.log(this.add.value);
    
}
  getAllBrand() {
    this.menuService.getAllBrandFoodInventory().subscribe(data => {
      console.log(data);
      this.allBrand = data['data']
    })
  }

  getAllIngredient() {
    this.menuService.getAllIngredientFoodInventory().subscribe(data => {

      console.log(data);
      this.allIngredient = data['data']
    })
  }

  // add brand pop up
  addBrandFoodInventory() {
    const dialogRef = this.dialog.open(AddBrandFoodInvnetoryComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      // this.getAllFoodInventoryCategory()
      this.getAllBrand()
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  additems() {
    console.log(this.additemForm.value);
    this.menuService.addItemFoodInventory(this.additemForm.value).subscribe(res => {
      console.log(res);

      if (res['sucess'] == true) {

        Swal.fire('Item Added Successfully', '', 'success')
        this.dialogRef.close();
      }
      else {
        Swal.fire('Failed to Add Item', 'Something went wrong', 'warning')
      }
    })
  }
  checkItemExist(event){
    console.log("hi");
    
    this.menuService.checkInventoryItemExist(event.target.value.toLowerCase()).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        Swal.fire("Item Name Already Exist..!","", "warning")
        event.target.value=""
        this.additemForm.patchValue({
          itemName:''
        })
      }

    })
  }
  
  addingredients(): void {
    const dialogRef = this.dialog.open(AddIngredientsComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getAllIngredient()
      }, 2000);

    });
  }
}
