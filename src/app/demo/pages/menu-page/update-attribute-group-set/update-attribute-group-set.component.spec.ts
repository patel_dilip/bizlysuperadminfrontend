import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAttributeGroupSetComponent } from './update-attribute-group-set.component';

describe('UpdateAttributeGroupSetComponent', () => {
  let component: UpdateAttributeGroupSetComponent;
  let fixture: ComponentFixture<UpdateAttributeGroupSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAttributeGroupSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAttributeGroupSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
