import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import Swal from 'sweetalert2';
import { Router, RoutesRecognized } from '@angular/router';
import { AddLiquorAttributeGroupComponent } from '../add-liquor-attribute-group/add-liquor-attribute-group.component';
import { transferArrayItem, moveItemInArray, CdkDragDrop } from '@angular/cdk/drag-drop';
import { AddSubSubVarientComponent } from 'src/app/PopoversList/add-sub-sub-varient/add-sub-sub-varient.component';
import { AddDrinkSubvarientComponent } from 'src/app/PopoversList/add-drink-subvarient/add-drink-subvarient.component';
import { AddDrinkVarientComponent } from 'src/app/PopoversList/add-drink-varient/add-drink-varient.component';
import { AddRootDrinkComponent } from 'src/app/PopoversList/add-root-drink/add-root-drink.component';
import { SubCategoryBeverageComponent } from '../beverages-category-dailogs/sub-category-beverage/sub-category-beverage.component';
import { RootCategoryAssetInventoryComponent } from '../asset-Inventory-category-dailogs/root-category-asset-inventory/root-category-asset-inventory.component';
import { SubCategoryAssetInventoryComponent } from '../asset-Inventory-category-dailogs/sub-category-asset-inventory/sub-category-asset-inventory.component';
import { SubSubCategoryAssetInventoryComponent } from '../asset-Inventory-category-dailogs/sub-sub-category-asset-inventory/sub-sub-category-asset-inventory.component';
import { SubSubCategoryTypeAssetInventoryComponent } from '../asset-Inventory-category-dailogs/sub-sub-category-type-asset-inventory/sub-sub-category-type-asset-inventory.component';
import { SubSubCategoryTypeBeverageComponent } from '../beverages-category-dailogs/sub-sub-category-type-beverage/sub-sub-category-type-beverage.component';
import { SubSubCategoryBeverageComponent } from '../beverages-category-dailogs/sub-sub-category-beverage/sub-sub-category-beverage.component';
import { RootCategoryBeverageComponent } from '../beverages-category-dailogs/root-category-beverage/root-category-beverage.component';
import { RootCategoryRetailFoodComponent } from '../retail-food-category-dailogs/root-category-retail-food/root-category-retail-food.component';
import { SubCategoryRetailFoodComponent } from '../retail-food-category-dailogs/sub-category-retail-food/sub-category-retail-food.component';
import { SubSubCategoryRetailFoodComponent } from '../retail-food-category-dailogs/sub-sub-category-retail-food/sub-sub-category-retail-food.component';
import { SubSubCategoryTypeRetailFoodComponent } from '../retail-food-category-dailogs/sub-sub-category-type-retail-food/sub-sub-category-type-retail-food.component';
import { pairwise, filter } from 'rxjs/operators';

@Component({
  selector: 'app-update-attribute-group-set',
  templateUrl: './update-attribute-group-set.component.html',
  styleUrls: ['./update-attribute-group-set.component.scss']
})
export class UpdateAttributeGroupSetComponent implements OnInit {
  addLiquorAttributeGroupForm: FormGroup
 
  loginUserID: any
  allLiquorAttributeGroup: ['']
  attributeKey: any
  attributeSetID: any
  data: any;
  allUnassigned: any;
  group_id: any;
  group=[];
  groupdata=[];
  categoryType=[];
  DrinkTypes=[];
  subsubFlag=0;
  typeFlag=0;
  subFlag=0;
  DrinkTree=[];
  id:any;
  count=0;
  attribute=[];
  attribute_set=[];
  equipmentAttributeJSONDATA=[];
  allInHouseBeveragesCategories=[];
  allAssetInventoryCategory=[];
  allRetailBeveragesCategories=[];
  path_name: string;
  Inventorytree=[];
  allRetailFoodCategories: any;
  retailFoodtree=[];
  Beveragestree=[];
  ti=0;
  si=0;
  ssi=0;
  disabledflag:any
  disabled_id: any;
  attributesetObj: any;
  constructor(private fb: FormBuilder,private route:Router,private dialog:MatDialog,private menuService: AddMenuFoodsService) {
    this.route.events.pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
    .subscribe((events: RoutesRecognized[]) => {
      console.log('previous url=========', events[0].urlAfterRedirects);
       localStorage.setItem('lastUrl', events[0].urlAfterRedirects)
      //console.log('current url', events[1].urlAfterRedirects);

    });
this.attributesetObj=menuService.editObject

    console.log(menuService.editObject);
    if(this.attributesetObj==undefined ){
      route.navigate['/menu']
    }else{
   
    

   
   
   
    
    if(this.attributeKey=="liquor"){
      this.path_name="Menu Management"
     }else if(this.attributeKey=="equipment"){
       this.path_name="Inventory Management"  
 
     }else if(this.attributeKey=="beverage"){
       this.path_name="Beverages And Retail Food"
     } else if(this.attributeKey=="retailfood" || this.attributeKey=='retailbeverages'){
       this.path_name="Beverages And Retail Food"
     }
    let id=[]
      // this.attribute=[]
      this.attributesetObj['assignGroups'].forEach(element => {
       id.push(element['_id'])
       
       });
       console.log("Group id",id);
       for(let i=0; i<id.length;i++){
this.menuService.getSingleAttributeGroup(id[i]).subscribe(data=>{
  console.log("Attribute:",data['data']['assignAttributes']);
  this.attribute_set = this.attribute_set.concat(data['data']['assignAttributes']); 

  
})
console.log(">",this.attribute_set);

this.getAllAttribute();
this.getAllBeveragesCategories()
       }
       this.menuService.getLiquorAttributeGroupwithSetName(this.menuService.editObject['attributeSetName'], this.attributeKey).subscribe(data => {
        console.log("All liquor attribute group", data);
        if (data['sucess'] == true) {
this.group_id=[]
this.group=[]

          data['data'].forEach(element => {
            this.group.push(element)
            this.group_id.push(element['_id'])
          });
          console.log(this.group_id);
          console.log("DATA", this.allUnassigned);
          
          
          this.addLiquorAttributeGroupForm.patchValue({
      assignGroups: this.group_id,
      categoryType:this.menuService.editObject.categoryType

          })
          console.log(this.addLiquorAttributeGroupForm.value);
          

        }        // this.allLiquorAttributeGroup = data['data'].filter(ele=>ele.attributeType==this.attributeKey)
      })
       // this.getAllAttribute();
    
    this.data=menuService.editObject
    this.categoryType=this.data['categoryType']
    this.getRootTypes();
    this.getAllAssetInventoryCategory()
    this.getAllBeveragesCategories()
    this.getAllRetailFoodCategories();
    this.attributeKey = menuService.editObject['attributeType']
    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID)
this.disbled()
    // this.menuService.getLiquorAttributeGroup().subscribe(data => {
    //   console.log("All liquor attribute group", data);
    //   this.allLiquorAttributeGroup = data['data'].filter(ele => ele.attributeType == this.attributeKey)
    // })


    this.addAttributeSet()
    // this.allUnassigned.forEach(element => {
     
      // menuService.getLiquorAttributeGroupwithSetName(id)
    // });
   

    this.addLiquorAttributeGroupForm.patchValue({
      attributeSetName: this.data.attributeSetName,
      assignGroups: this.data.assignGroups
    })
    this.attributeSetID = this.data._id
    this.group=this.data.assignGroups
  }
  }
   // **********open root drink dialog**************

   openRootDialog() {
    const dialogRef = this.dialog.open(AddRootDrinkComponent, {
      width: '460px',
      // height: '200px'
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log('After dialog closed');
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getRootTypes()
      }, 2000);

    })
  }

  // ********OPEN VARIENT DRINK DIALOG***************

  openVarientDialog(rootDrinkID) {
    const dialogRef = this.dialog.open(AddDrinkVarientComponent, {
      width: '460px',
      data: rootDrinkID,
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'after closing varient drink pop'
      );
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getRootTypes()
      }, 2000);
    })

  }

  //************open sub varient drink dialog box**************

  openSubVarientDialog(rootDrinkID, liquorVarient) {
    const dialogRef = this.dialog.open(AddDrinkSubvarientComponent, {
      width: '460px',
      data: { "rootDrinkID": rootDrinkID, "liquorVarient": liquorVarient },
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getRootTypes()
      }, 2000);
    })
  }

  // **************OPEN SUB SUB VARIENT DIALOG************
  openSubSubVarient(rootDrinkID, liquorVarient, liquorSubVarient) {
    const dialogRef = this.dialog.open(AddSubSubVarientComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootDrinkID": rootDrinkID, "liquorVarient": liquorVarient, "liquorSubVarient": liquorSubVarient }
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getRootTypes()
      }, 2000);
    })
  }
  addAttributeGroup(): void {
    
    const dialogRef = this.dialog.open(AddLiquorAttributeGroupComponent, {
      data: { attributeType: 'liquor', attribute_set_name: this.addLiquorAttributeGroupForm.controls.attributeSetName.value },
      disableClose: true,
    });
   

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
     this.getAllAttribute()
      console.log(">", this.addLiquorAttributeGroupForm.controls.attributeSetName.value);
      let attributesetname=this.addLiquorAttributeGroupForm.controls.attributeSetName.value
      this.menuService.getLiquorAttributeGroupwithSetName(attributesetname.tolowercase(),this.attributeKey.tolowercase()).subscribe(data => {
        console.log("All liquor attribute group", data);
        if (data['sucess'] == true) {
this.group_id=[]
this.group=[]

          data['data'].forEach(element => {
            this.group.push(element)
            this.group_id.push(element['_id'])
          });
          this.addLiquorAttributeGroupForm.patchValue({
      assignGroups: this.group_id,

          })
          console.log(this.addLiquorAttributeGroupForm.value);
          

        }        // this.allLiquorAttributeGroup = data['data'].filter(ele=>ele.attributeType==this.attributeKey)
      })
      // this.getAllAttributeGroup()
    });
  }
  drop(event: CdkDragDrop<any>) {

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);

    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      //  console.log("Index>>>>",event.currentIndex);

    }
    console.log(this.group);
  
// console.log(this.group);


    // console.log("c>>>>",this.todo);
    // console.log("B>>>>",this.done); 

  }
  ngOnInit() {

  }
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }
  getRootTypes() {

    this.menuService.getAllRootDrinks().subscribe(data => {
      this.DrinkTypes = data['data']

      console.log('drinktypes', this.DrinkTypes);
      
      this.DrinkTypes.forEach(element => {
        this.categoryType.forEach(ele=>{
        if(element['_id']==ele['_id'])
        element=ele
        
        })
         this.DrinkTree.push(element)
      });
this.DrinkTypes=this.DrinkTree
console.log(this.categoryType);
    })
  }

  addAttributeSet() {
    this.addLiquorAttributeGroupForm = this.fb.group({
      attributeSetName: [''],
      assignGroups: [],
      userid: this.loginUserID,
      attributeType: this.attributeKey,
      categoryType:['']
    })
  }
  getAllAttribute() {
    this.menuService.getAllLiquorAttribute().subscribe(data => {
      console.log("all attribute", data);
      data['data'].forEach(element => {
        element.attributeCode = "A" + element.attributeCode
      });if(this.attributeKey=='liquor'){
      let allAttributes = data['data'].filter(element => element.attributeType == "liquor")
      this.allUnassigned = allAttributes.filter(ele => ele['isAssign'] == false  && ele.status==true)
      console.log(this.allUnassigned);
    }else if(this.attributeKey=='equipment'){
      let allAttributes = data['data'].filter(element => element.attributeType == "equipment")
      this.allUnassigned = allAttributes.filter(ele => ele['isAssign'] == false  && ele.status==true)
      console.log(this.allUnassigned);
    }
    else if(this.attributeKey=='beverage'){
      let allAttributes = data['data'].filter(element => element.attributeType == "beverage")
      this.allUnassigned = allAttributes.filter(ele => ele['isAssign'] == false  && ele.status==true)
      console.log(this.allUnassigned);
    }
    else if(this.attributeKey=='retailfood'){
      let allAttributes = data['data'].filter(element => element.attributeType == "retailfood")
      this.allUnassigned = allAttributes.filter(ele => ele['isAssign'] == false  && ele.status==true)
      console.log(this.allUnassigned);
    }
    else if(this.attributeKey=='retailbeverages'){
      let allAttributes = data['data'].filter(element => element.attributeType == "retailbeverages")
      this.allUnassigned = allAttributes.filter(ele => ele['isAssign'] == false  && ele.status==true)
      console.log(this.allUnassigned);
    }
  
    let ids = new Set(this.attribute_set.map(({ _id }) => _id));

    this.allUnassigned = this.allUnassigned.filter(({ _id }) => !ids.has(_id));
         
              console.log("atr>",this.allUnassigned);
              console.log("ser>", this.attribute_set);
    

      var equipmentAttribute = data['data'].filter(element => element.attributeType == "equipment")
      equipmentAttribute.forEach(element => {
        var optionLabels = []
        element.responseType.options.forEach(ele => {
          optionLabels.push(ele.optionLable)
        });
        var d = new Date(element.updatedAt)
        var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
        let equipmentObj = {}
        equipmentObj = {
          "attributeCode": element.attributeCode,
          "attributeName": element.attributeName,
          "responseType": element.responseType.elementName,
          "fieldValue": element.responseType.fieldValue,
          "options": optionLabels.toString(),
          "isSearchable": element.isSearchable,
          "isFilterable": element.isFilterable,
          "isAssign": element.isAssign,
          "status": element.status,
          "addedBy": element.addedBy.userName,
          "updatedBy": element.updatedBy.userName,
          "updatedAt": date
        }
        this.equipmentAttributeJSONDATA.push(equipmentObj)
      });
      console.log("retail food attribute export", this.equipmentAttributeJSONDATA);
    })
  }

  addNewSet() {
    this.addLiquorAttributeGroupForm.patchValue({
      categoryType:this.categoryType
    })
    console.log(this.addLiquorAttributeGroupForm.value);
    this.menuService.updateAttributeSet(this.attributeSetID,this.addLiquorAttributeGroupForm.value).subscribe(res => {
      console.log(res);
      if (res['sucess']) {
        Swal.fire('Attribute Set Updated Successfully', '', 'success')
        if(this.attributeKey=="equipment"){
          this.route.navigate(['inventory/equipment-attributes'])  
        }else if(this.attributeKey=="liquor"){
        this.route.navigate(['menu'])
        }
        else if(this.attributeKey=="beverage"){
          this.route.navigate(['beverages_and_retailfood'])
        }
        else if(this.attributeKey=="retailfood" || this.attributeKey== "retailbeverages"){
          this.route.navigate(['beverages_and_retailfood'])
        }
        this.group_id=[]
        this.groupdata=this.group
  //this.groupdata=this.group
  this.groupdata.forEach(element => {
    let id=[]
    

   // console.log(element['assignAttributes'])
    if(element['assignAttributes']!=[]){
    id=[]
      element['assignAttributes'].forEach(ele => {
  
          id.push(ele['_id'])
          element['assignAttributes']=id
      });
    }
    console.log("data>>",element,"id>>", element['_id']);
    
    this.menuService.updateGroup(element['_id'], element).subscribe(data=>{
      console.log(data);
      
    })
  });
       
            }
      else {
        Swal.fire('Fail to Update Attribute Set', 'Something Went Wrong', 'warning')
      }
    })
  }
  changeFlag(_id,data, event, root_level, rootindex?: any, subcategoryIndex?: any, subsubcategoryINdex?: any, typeIndex?: any) {
    //console.log( ">>>" , data, ">>>" , event.checked);
    this.count=0
    console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
    if (root_level == 'root') {
      this.DrinkTypes.forEach((element, index) => {
        if (index == rootindex) {
          element.showChildren = event.checked
          if (
            this.DrinkTypes[index].hasOwnProperty('liquorVarients')
            &&
            this.DrinkTypes[index].liquorVarients.length > 0
          ) {
            this.DrinkTypes[index].liquorVarients.forEach((element, subcategoryIndex = index) => {
              element.showChildren = event.checked;
              if (
                this.DrinkTypes[index].liquorVarients[subcategoryIndex].hasOwnProperty('liquorSubVarients')
                &&
                this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.forEach((element, subsubcategoryINdex = index) => {
                  element.showChildren = event.checked;
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                      element.showChildren = event.checked;
                    })
                  }


                })
              }
            })
          }
        }
      });
      //console.log('this.DrinkTypes >', this.DrinkTypes)
    }
    else if (root_level == 'sub') {
      this.DrinkTypes.forEach((Rootelement, index) => {
        if (index == rootindex) {
          // Rootelement.showChildren = event.checked
          if (
            this.DrinkTypes[index].hasOwnProperty('liquorVarients')
            &&
            this.DrinkTypes[index].liquorVarients.length > 0
          ) {
            this.DrinkTypes[index].liquorVarients.forEach((subelement, subindex) => {
              if (subindex == subcategoryIndex) {
                subelement.showChildren = event.checked;
                if (
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].hasOwnProperty('liquorSubVarients')
                  &&
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.forEach((subsubelement, subsubcategoryINdex = index) => {
                    subsubelement.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length

                      this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.forEach((typeelement, liquorSubSubVarientTypeIndex = index) => {
                        typeelement.showChildren = event.checked;
                      })
                    }


                  })
                }
              }

            })

          }


        }
      });
      //console.log('this.DrinkTypes >', this.DrinkTypes)
    } else if (root_level == 'subsub') {
      this.DrinkTypes.forEach((element, index) => {
        if (index == rootindex) {
          // element.showChildren = event.checked
          if (
            this.DrinkTypes[index].hasOwnProperty('liquorVarients')
            &&
            this.DrinkTypes[index].liquorVarients.length > 0
          ) {
            this.DrinkTypes[index].liquorVarients.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                //             element.showChildren = event.checked;
                if (
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].hasOwnProperty('liquorSubVarients')
                  &&
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      element.showChildren = event.checked;
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                          element.showChildren = event.checked;
                        })
                      }
                    }

                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.DrinkTypes >', this.DrinkTypes)
    } else if (root_level == 'type') {
      this.DrinkTypes.forEach((element, index) => {
        if (index == rootindex) {
          if (
            this.DrinkTypes[index].hasOwnProperty('liquorVarients')
            &&
            this.DrinkTypes[index].liquorVarients.length > 0
          ) {
            this.DrinkTypes[index].liquorVarients.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                if (
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].hasOwnProperty('liquorSubVarients')
                  &&
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.forEach((element, childIndex) => {
                          if (typeIndex == childIndex)
                            element.showChildren = event.checked;
                        })
                      }


                    }
                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.DrinkTypes >', this.DrinkTypes)
    }
    // debugger;
    ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////

    this.DrinkTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.DrinkTypes[index].hasOwnProperty('liquorVarients')
          &&
          this.DrinkTypes[index].liquorVarients.length > 0
        ) {
          this.DrinkTypes[index].liquorVarients.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.DrinkTypes[index].liquorVarients[subindex].hasOwnProperty('liquorSubVarients')
                &&
                this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.forEach((subsubelement, subsubINdex) => {
                  // if(subsubINdex==0){
                  //   this.subsubFlag=0
                  // }
                  if (subsubINdex == subsubcategoryINdex) {
                    if (subsubelement.showChildren == true) {
                      this.subsubFlag++
                    }
                    //console.log("<<<", this.DrinkTypes[index].subCategories[subcategoryIndex].liquorSubVarients.length);
                    //console.log(">>>", this.subsubFlag);


                    if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length == this.subsubFlag) {
                      subelement.showChildren = true
                    } else {
                      subelement.showChildren = false;
                    }
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].hasOwnProperty('liquorSubSubVarientType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      // //console.log(element);
                      this.typeFlag = 0
                      this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.forEach((typeelement, childIndex) => {
                        // if (typeIndex == childIndex) {
                        //   typeelement.showChildren = event.checked;
                        // }
                        if (typeelement.showChildren == true) {
                          this.typeFlag++
                        }
                        console.log("<<<", this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length);
                        console.log(">>>", this.typeFlag);


                        if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length == this.typeFlag) {
                          subsubelement.showChildren = true
                        } else {
                          subsubelement.showChildren = false;
                        }

                      })
                    }


                  }
                })
              }
              // }
            }
          })
        }


      }
    });
    //subsub
    this.subsubFlag = 0

    this.DrinkTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.DrinkTypes[index].hasOwnProperty('liquorVarients')
          &&
          this.DrinkTypes[index].liquorVarients.length > 0
        ) {
          this.DrinkTypes[index].liquorVarients.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {

              if (
                this.DrinkTypes[index].liquorVarients[subindex].hasOwnProperty('liquorSubVarients')
                &&
                this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.forEach((subsubelement, subsubINdex) => {

                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  // //console.log("<<<", this.DrinkTypes[index].subCategories[subcategoryIndex].liquorSubVarients.length);
                  // //console.log(">>>", this.subsubFlag);


                  if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].hasOwnProperty('liquorSubSubVarientType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    // this.typeFlag=0
                    this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.forEach((typeelement, childIndex) => {



                    })
                  }
                })
              }

            }
          })
        }


      }
    });

    //////subflag
    this.subFlag = 0
    this.subsubFlag = 0

    this.DrinkTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.DrinkTypes[index].hasOwnProperty('liquorVarients')
          &&
          this.DrinkTypes[index].liquorVarients.length > 0
        ) {
          this.DrinkTypes[index].liquorVarients.forEach((subelement, subindex) => {
            // if(subindex==subcategoryIndex){
            if (subelement.showChildren == true) {
              this.subFlag++
            }
            // console.log("<<<", this.DrinkTypes[index].liquorVarients.length);
            // console.log(">>>", this.subFlag);


            if (this.DrinkTypes[index].liquorVarients.length == this.subFlag) {
              Rootelement.showChildren = true
            } else {
              Rootelement.showChildren = false;
            }

            if (
              this.DrinkTypes[index].liquorVarients[subindex].hasOwnProperty('liquorSubVarients')
              &&
              this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.forEach((subsubelement, subsubINdex) => {


                if (
                  // tslint:disable-next-line: max-line-length
                  this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].hasOwnProperty('liquorSubSubVarientType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.forEach((typeelement, childIndex) => {



                  })
                }
              })
            }

            // }
          })
        }


      }
    });
    console.log(this.DrinkTypes[rootindex]);
  //   if (this.categoryType.length == 0)  {
  //     if (this.DrinkTypes[rootindex].showChildren == true) {
  //       this.categoryType.push(this.DrinkTypes[rootindex])
  //     }
  //   }
  //  else{
  //   if (this.DrinkTypes[rootindex].showChildren == true) {
  //     this.categoryType.push(this.DrinkTypes[rootindex])
  //   }
  //  }
  // 
  if (this.DrinkTypes[rootindex].showChildren == false) {
    // element.showChildren = event.checked
    if (
      this.DrinkTypes[rootindex].hasOwnProperty('liquorVarients')
      &&
      this.DrinkTypes[rootindex].liquorVarients.length > 0
    ) {
      this.DrinkTypes[rootindex].liquorVarients.forEach((element, subindex) => {
       if( element.showChildren == false){
        if (
          this.DrinkTypes[rootindex].liquorVarients[subindex].hasOwnProperty('liquorSubVarients')
          &&
          this.DrinkTypes[rootindex].liquorVarients[subindex].liquorSubVarients.length > 0
        ) {
          // tslint:disable-next-line: max-line-length
          this.DrinkTypes[rootindex].liquorVarients[subindex].liquorSubVarients.forEach((element, subsubINdex) => {
            if( element.showChildren == false){
            if (
              // tslint:disable-next-line: max-line-length
              this.DrinkTypes[rootindex].liquorVarients[subindex].liquorSubVarients[subsubINdex].hasOwnProperty('liquorSubSubVarientType')
              &&
              // tslint:disable-next-line: max-line-length
              this.DrinkTypes[rootindex].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              //console.log(element);
              this.DrinkTypes[rootindex].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.forEach((element, liquorSubSubVarientTypeIndex) => {
              if( element.showChildren == true)
              {               
                this.count=1;
                return false
              }
              })
            }


          }
          else{
            this.count=1;
            return false;
          }
          })
        }
      }
      if(element.showChildren==true){
      this.count=1;
      return false;
      }
      })
    }
  }
  else{
    this.count=1
  }
 
console.log(this.count);
let id
if(this.count==1){
if(this.categoryType.length==0){
this.categoryType.push(this.DrinkTypes[rootindex])
}else{
this.categoryType.forEach(element => {
  if(element['_id']==this.DrinkTypes[rootindex]['_id']){
    element=this.DrinkTypes[rootindex]
    this.id=element['_id'];
  }
});
if(this.id!=this.DrinkTypes[rootindex]['_id']){
  this.categoryType.push(this.DrinkTypes[rootindex])
}
}
}
if(this.count==0){
let i=0;
this.categoryType.forEach(element => {
 
 if(element['_id']==this.DrinkTypes[rootindex]['_id']){
  this.categoryType.splice(i, 1)
 }else{
   console.log("not match");
   
 }
 i++;
});
}
console.log(this.categoryType);

this.intermediate(data, event, root_level, rootindex = rootindex, subcategoryIndex = subcategoryIndex, subsubcategoryINdex = subsubcategoryINdex, typeIndex = typeIndex)
this.DisabledTree(_id, event)
}
     intermediate(data, event, root_level, rootindex, subcategoryIndex, subsubcategoryINdex, typeIndex?: any) {
      ///////////////////////////////////////// intermediate ////////////////////////////////////
      console.log(data, "<", event, "<", root_level, "<", rootindex, "<", subcategoryIndex, "<", subsubcategoryINdex, "<", typeIndex);
      // this.ri = 0
      // this.si = 0
      // this.ssi = 0
      
        this.ti = 0
    
     
        this.DrinkTypes.forEach((Rootelement, index) => {
  
          if (index == rootindex) {
           
            console.log("sub length:", this.DrinkTypes[rootindex].liquorVarients.length);
    
            if (
              this.DrinkTypes[index].hasOwnProperty('liquorVarients')
              &&
              this.DrinkTypes[index].liquorVarients.length > 0
            ) {
              this.si=0
              this.DrinkTypes[index].liquorVarients.forEach((subelement, subindex) => {
                // debugger;
                if (subelement['showChildren'] == true) {
                  this.si++
                }
                if (subindex == subcategoryIndex) {
                 
                  if (
                    this.DrinkTypes[index].liquorVarients[subindex].hasOwnProperty('liquorSubVarients')
                    &&
                    this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
    // this.ssi=0
    // debugger;
    this.ssi=0
  
    
                    this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.forEach((subsubelement, subsubINdex) => {
                      if (subsubelement['showChildren'] == true) {
                        this.ssi++
                      }
                          
                      if (subsubINdex == subsubcategoryINdex) {
   
                        // ////console.log("<<<", this.DrinkTypes[index].liquorVarients[subcategoryIndex].liquorSubVarients.length);
                        // ////console.log(">>>", this.subsubFlag);
                       
    
    
                        if (
                          // tslint:disable-next-line: max-line-length
                          this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].hasOwnProperty('liquorSubSubVarientType')
                          &&
                          // tslint:disable-next-line: max-line-length
                          this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > 0
                        ) {
                          // tslint:disable-next-line: max-line-length
                          // ////console.log(element);
                          // this.typeFlag=0
                          this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.forEach((typeelement, childIndex) => {
                            // if(typeIndex==childIndex){
                            if (typeelement['showChildren'] == true) {
                              this.ti++
                            }
                            console.log(this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length, "type>>", this.ti);
    
                            if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length > this.ti) {
                              // Rootelement.inter=true;
                              // subelement.inter=true;
                              subsubelement.inter = true;
                            } else if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients[subsubINdex].liquorSubSubVarientType.length == this.ti) {
    
                              subsubelement.inter = false;
                              subsubelement.showChildren = true;
                            }
                          //  if (this.ti== 0) {
                          //   subsubelement.inter = false;
                          //   subsubelement.showChildren = false;
                          // }
    
                            if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > this.ssi) {
                              // Rootelement.inter=true;
                              // subelement.inter=true;
                              subelement.inter = true;
                            } else if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length == this.ssi) {
    
                              subelement.inter = false;
                              subelement.showChildren = true;
                            }
                            
                            if (this.DrinkTypes[index].liquorVarients.length > this.si) {
                              // Rootelement.inter=true;
                              // subelement.inter=true;
                              subelement.inter = true;
                            } else if (this.DrinkTypes[index].liquorVarients.length == this.si) {
                              Rootelement.inter = false;
                              Rootelement.showChildren = true;
                            }
                          
                            //  }
                          })
                          if (this.ti== 0) {
                            subsubelement.inter = false;
                            subsubelement.showChildren = false;
                          }
                        }
                        
                       
                              }
                              console.log(this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length, "subsub>>", this.ssi);
    
                              if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length > this.ssi) {
                                // Rootelement.inter=true;
                                // subelement.inter=true;
                                subelement.inter = true;
                              } else if (this.DrinkTypes[index].liquorVarients[subindex].liquorSubVarients.length == this.ssi) {
          
                                subelement.inter = false;
                                subelement.showChildren = true;
                              } 
                             
                             
                    })
                    if (this.ssi== 0 && this.ti==0) {
                        subelement.inter = false;
                        subelement.showChildren = false;
                      }
                  }
    
              
            
                  
                  console.log(this.DrinkTypes[index].liquorVarients.length, "sub>>", this.si);
    
                 
                   }
                   if (this.DrinkTypes[index].liquorVarients.length > this.si) {
                    // Rootelement.inter=true;
                    // subelement.inter=true;
                    Rootelement.inter = true;
                  } else if (this.DrinkTypes[index].liquorVarients.length == this.si) {
    
                    Rootelement.inter = false;
                    Rootelement.showChildren = true;
                  } 
                 
               })
               if (this.ssi== 0 && this.ti==0 && this.si==0) {
                Rootelement.inter = false;
                Rootelement.showChildren = false;
              }
            }
    
    
          }
        });
     
  }
  
     removeAttribute(group,i,item){
      console.log("item", item);
        // console.log(this.todo1.indexOf(event));
   
    group.splice(i, 1);
    this.allUnassigned.push(item)
   // this.getAllAttribute();
       }
       deleteGroup(id, j, data){
        console.log(id);
        this.menuService.deleteGroup(id).subscribe(data=>{
          console.log(data);
          this.group.splice(j,1)
          
          // this.getAllAttribute();
        })
        data.forEach(element => {
          this.allUnassigned.push(element)
        });
        
        
      }
        /////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////// ASSET INVENTORY CATEGORY TREE //////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////


  getAllAssetInventoryCategory() {
    console.log(this.categoryType);
    
    this.menuService.getAllAssetInventoryCategories().subscribe(data => {
      console.log("all asset inventory categories", data);
      this.allAssetInventoryCategory = data['data']
      console.log('drinktypes', this.DrinkTypes);
      this.allAssetInventoryCategory.forEach(element => {
        this.categoryType.forEach(ele=>{
        if(element['_id']==ele['_id'])
        element=ele
        
        })
         this.Inventorytree.push(element)
      });
this.allAssetInventoryCategory=this.Inventorytree
console.log(this.categoryType);
      
    })
    // this.loadSuitofAssetInventory()
  }
  // *********************** OPEN ROOT CATEGORY IN ASSET INVENTORY *************************8
  openInventoryRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,

      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB CATEGORY IN ASSET INVENTORY ********************** */
  openInventorySubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB SUB CATEGORY IN ASSET INVENTORY ********************** */
  openInventorySubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }

  //*************************OPEN SUB SUB CATEGORY TYPE IN ASSET INVENTORY ********************** */
  openInventorySubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }

///
 
getAllBeveragesCategories() {
  this.menuService.getAllBeverageCategories().subscribe(data => {
    console.log("All Beverages Categories", data);
    if (data['sucess'] == false) {
      //console.log("Beverages categories data not found");
    } else {
                    this.allRetailBeveragesCategories = data['data'].filter(element => element.beverageType == "retailBeverage")
                   this.Beveragestree=[];
                    this.allRetailBeveragesCategories.forEach(element => {
                      this.categoryType.forEach(ele=>{
                        if(element['_id']==ele['_id'])
                        element=ele
                        })
                         this.Beveragestree.push(element)
                    });
               this.allRetailBeveragesCategories=this.Beveragestree
    }
  })

}
// getAllBeveragesCategories() {
//   this.menuService.getAllBeverageCategories().subscribe(data => {
//     console.log("All Beverages Categories", data);
//     if (data['sucess'] == false) {
//       //console.log("Beverages categories data not found");
//     } else {
//       // this.allInHouseBeveragesCategories = data['data'].filter(element => element.beverageType == "inHouseBeverage")
//       // console.log("In House>>>", this.allInHouseBeveragesCategories);

//       this.allRetailBeveragesCategories = data['data'].filter(element => element.beverageType == "retailbeverages")
//       this.allRetailBeveragesCategories.forEach(element => {
//         this.categoryType.forEach(ele=>{
//         if(element['_id']==ele['_id'])
//         element=ele
        
//         })
//          this.Beveragestree.push(element)
//       });
// this.allRetailBeveragesCategories=this.Beveragestree
//       //console.log("Retail Beverage>>>", this.allInHouseBeveragesCategories);
//       // this.loadSuit();

//     }


//   })

// }
openBeveragesSubSubCategoryTypeDialog(rootCategoryID, subsubCategortyID) {
  const dialogRef = this.dialog.open(SubSubCategoryTypeBeverageComponent, {
    width: '460px',
    disableClose: true,
    data: { "rootCategoryID": rootCategoryID, "subsubCategortyID": subsubCategortyID }
  })
  dialogRef.afterClosed().subscribe(result => {
    this.getAllBeveragesCategories()
  })
}
openBeveragesSubSubCategoryDialog(rootCategoryID, subCategoryID) {
  const dialogRef = this.dialog.open(SubSubCategoryBeverageComponent, {
    width: '460px',
    disableClose: true,
    data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
  })
  dialogRef.afterClosed().subscribe(result => {
    this.getAllBeveragesCategories()
  })
}
openBeveragesRootCategoryDailog(beverageType) {
  const dialogRef = this.dialog.open(RootCategoryBeverageComponent, {
    width: '460px',
    disableClose: true,
    data: beverageType
  })
  dialogRef.afterClosed().subscribe(result => {
    this.getAllBeveragesCategories()
  })
}
openBeveragesSubCategoryDialog(rootCategoryid) {
  const dialogRef = this.dialog.open(SubCategoryBeverageComponent, {
    width: '460px',
    disableClose: true,
    data: rootCategoryid
  })
  dialogRef.afterClosed().subscribe(result => {
    this.getAllBeveragesCategories()
  })
}
changeFlagEquipment(_id,data, event, root_level, rootindex?: any, subcategoryIndex?: any, subsubcategoryINdex?: any, typeIndex?: any) {
  //console.log( ">>>" , data, ">>>" , event.checked);
  this.count=0
  console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
  if (root_level == 'root') {
    this.allAssetInventoryCategory.forEach((element, index) => {
      if (index == rootindex) {
        element.showChildren = event.checked
        if (
          this.allAssetInventoryCategory[index].hasOwnProperty('subCategories')
          &&
          this.allAssetInventoryCategory[index].subCategories.length > 0
        ) {
          this.allAssetInventoryCategory[index].subCategories.forEach((element, subcategoryIndex = index) => {
            element.showChildren = event.checked;
            if (
              this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
              &&
              this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
                element.showChildren = event.checked;
                if (
                  // tslint:disable-next-line: max-line-length
                  this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  //console.log(element);
                  this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                    element.showChildren = event.checked;
                  })
                }


              })
            }
          })
        }
      }
    });
    //console.log('this.DrinkTypes >', this.DrinkTypes)
  }
  else if (root_level == 'sub') {
    this.allAssetInventoryCategory.forEach((Rootelement, index) => {
      if (index == rootindex) {
        // Rootelement.showChildren = event.checked
        if (
          this.allAssetInventoryCategory[index].hasOwnProperty('subCategories')
          &&
          this.allAssetInventoryCategory[index].subCategories.length > 0
        ) {
          this.allAssetInventoryCategory[index].subCategories.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              subelement.showChildren = event.checked;
              if (
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.forEach((subsubelement, subsubcategoryINdex = index) => {
                  subsubelement.showChildren = event.checked;
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length

                    this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((typeelement, liquorSubSubVarientTypeIndex = index) => {
                      typeelement.showChildren = event.checked;
                    })
                  }


                })
              }
            }

          })

        }


      }
    });
    //console.log('this.DrinkTypes >', this.DrinkTypes)
  } else if (root_level == 'subsub') {
    this.allAssetInventoryCategory.forEach((element, index) => {
      if (index == rootindex) {
        // element.showChildren = event.checked
        if (
          this.allAssetInventoryCategory[index].hasOwnProperty('subCategories')
          &&
          this.allAssetInventoryCategory[index].subCategories.length > 0
        ) {
          this.allAssetInventoryCategory[index].subCategories.forEach((element, subindex) => {
            if (subindex == subcategoryIndex) {
              //             element.showChildren = event.checked;
              if (
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubINdex) => {
                  if (subsubINdex == subsubcategoryINdex) {
                    element.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                        element.showChildren = event.checked;
                      })
                    }
                  }

                })
              }
              // }
            }
          })
        }


      }
    });
    //console.log('this.DrinkTypes >', this.DrinkTypes)
  } else if (root_level == 'type') {
    this.allAssetInventoryCategory.forEach((element, index) => {
      if (index == rootindex) {
        if (
          this.allAssetInventoryCategory[index].hasOwnProperty('subCategories')
          &&
          this.allAssetInventoryCategory[index].subCategories.length > 0
        ) {
          this.allAssetInventoryCategory[index].subCategories.forEach((element, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubINdex) => {
                  if (subsubINdex == subsubcategoryINdex) {
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.DrinkTypes[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, childIndex) => {
                        if (typeIndex == childIndex)
                          element.showChildren = event.checked;
                      })
                    }


                  }
                })
              }
              // }
            }
          })
        }


      }
    });
    //console.log('this.DrinkTypes >', this.DrinkTypes)
  }
  ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////

  this.allAssetInventoryCategory.forEach((Rootelement, index) => {
    if (index == rootindex) {
      if (
        this.allAssetInventoryCategory[index].hasOwnProperty('subCategories')
        &&
        this.allAssetInventoryCategory[index].subCategories.length > 0
      ) {
        this.allAssetInventoryCategory[index].subCategories.forEach((subelement, subindex) => {
          if (subindex == subcategoryIndex) {
            if (
              this.allAssetInventoryCategory[index].subCategories[subindex].hasOwnProperty('subSubCategories')
              &&
              this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {
                // if(subsubINdex==0){
                //   this.subsubFlag=0
                // }
                if (subsubINdex == subsubcategoryINdex) {
                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  //console.log("<<<", this.DrinkTypes[index].subCategories[subcategoryIndex].liquorSubVarients.length);
                  //console.log(">>>", this.subsubFlag);


                  if (this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    this.typeFlag = 0
                    this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {
                      // if (typeIndex == childIndex) {
                      //   typeelement.showChildren = event.checked;
                      // }
                      if (typeelement.showChildren == true) {
                        this.typeFlag++
                      }
                      console.log("<<<", this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length);
                      console.log(">>>", this.typeFlag);


                      if (this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length == this.typeFlag) {
                        subsubelement.showChildren = true
                      } else {
                        subsubelement.showChildren = false;
                      }

                    })
                  }


                }
              })
            }
            // }
          }
        })
      }


    }
  });
  //subsub
  this.subsubFlag = 0

  this.allAssetInventoryCategory.forEach((Rootelement, index) => {
    if (index == rootindex) {
      if (
        this.allAssetInventoryCategory[index].hasOwnProperty('subCategories')
        &&
        this.allAssetInventoryCategory[index].subCategories.length > 0
      ) {
        this.allAssetInventoryCategory[index].subCategories.forEach((subelement, subindex) => {
          if (subindex == subcategoryIndex) {

            if (
              this.allAssetInventoryCategory[index].subCategories[subindex].hasOwnProperty('subSubCategories')
              &&
              this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {

                if (subsubelement.showChildren == true) {
                  this.subsubFlag++
                }
                // //console.log("<<<", this.DrinkTypes[index].subCategories[subcategoryIndex].liquorSubVarients.length);
                // //console.log(">>>", this.subsubFlag);


                if (this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length == this.subsubFlag) {
                  subelement.showChildren = true
                } else {
                  subelement.showChildren = false;
                }
                if (
                  // tslint:disable-next-line: max-line-length
                  this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {



                  })
                }
              })
            }

          }
        })
      }


    }
  });

  //////subflag
  this.subFlag = 0
  this.subsubFlag = 0

  this.allAssetInventoryCategory.forEach((Rootelement, index) => {
    if (index == rootindex) {
      if (
        this.allAssetInventoryCategory[index].hasOwnProperty('subCategories')
        &&
        this.allAssetInventoryCategory[index].subCategories.length > 0
      ) {
        this.allAssetInventoryCategory[index].subCategories.forEach((subelement, subindex) => {
          // if(subindex==subcategoryIndex){
          if (subelement.showChildren == true) {
            this.subFlag++
          }
          // console.log("<<<", this.DrinkTypes[index].liquorVarients.length);
          // console.log(">>>", this.subFlag);


          if (this.allAssetInventoryCategory[index].subCategories.length == this.subFlag) {
            Rootelement.showChildren = true
          } else {
            Rootelement.showChildren = false;
          }

          if (
            this.allAssetInventoryCategory[index].subCategories[subindex].hasOwnProperty('subSubCategories')
            &&
            this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length > 0
          ) {
            // tslint:disable-next-line: max-line-length

            this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {


              if (
                // tslint:disable-next-line: max-line-length
                this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                &&
                // tslint:disable-next-line: max-line-length
                this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                // //console.log(element);
                // this.typeFlag=0
                this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {



                })
              }
            })
          }

          // }
        })
      }


    }
  });
  console.log(this.allAssetInventoryCategory[rootindex]);
if (this.allAssetInventoryCategory[rootindex].showChildren == false) {
  // element.showChildren = event.checked
  if (
    this.allAssetInventoryCategory[rootindex].hasOwnProperty('subCategories')
    &&
    this.allAssetInventoryCategory[rootindex].subCategories.length > 0
  ) {
    this.allAssetInventoryCategory[rootindex].subCategories.forEach((element, subindex) => {
     if( element.showChildren == false){
      if (
        this.allAssetInventoryCategory[rootindex].subCategories[subindex].hasOwnProperty('subSubCategories')
        &&
        this.allAssetInventoryCategory[rootindex].subCategories[subindex].subSubCategories.length > 0
      ) {
        // tslint:disable-next-line: max-line-length
        this.allAssetInventoryCategory[rootindex].subCategories[subindex].subSubCategories.forEach((element, subsubINdex) => {
          if( element.showChildren == false){
          if (
            // tslint:disable-next-line: max-line-length
            this.allAssetInventoryCategory[rootindex].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
            &&
            // tslint:disable-next-line: max-line-length
            this.allAssetInventoryCategory[rootindex].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
          ) {
            // tslint:disable-next-line: max-line-length
            //console.log(element);
            this.allAssetInventoryCategory[rootindex].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((element, liquorSubSubVarientTypeIndex) => {
            if( element.showChildren == true)
            {               
              this.count=1;
              return false
            }
            })
          }


        }
        else{
          this.count=1;
          return false;
        }
        })
      }
    }
    if(element.showChildren==true){
    this.count=1;
    return false;
    }
    })
  }
}
else{
  this.count=1
}

console.log(this.count);
let id
if(this.count==1){
if(this.categoryType.length==0){
this.categoryType.push(this.allAssetInventoryCategory[rootindex])
}else{
this.categoryType.forEach(element => {
if(element['_id']==this.allAssetInventoryCategory[rootindex]['_id']){
  element=this.allAssetInventoryCategory[rootindex]
  this.id=element['_id'];
}
});
if(this.id!=this.allAssetInventoryCategory[rootindex]['_id']){
this.categoryType.push(this.allAssetInventoryCategory[rootindex])
}
}
}
if(this.count==0){  
let i=0;
this.categoryType.forEach(element => {

if(element['_id']==this.allAssetInventoryCategory[rootindex]['_id']){
this.categoryType.splice(i, 1)
}else{
 console.log("not match");
 
}
i++;
});
}
console.log(this.categoryType);
this.intermediateEquipment(data, event, root_level, rootindex = rootindex, subcategoryIndex = subcategoryIndex, subsubcategoryINdex = subsubcategoryINdex, typeIndex = typeIndex)
this.DisabledTree(_id,event)

  }
   //////////////////////////////////////////////// interminate for equipment//////////////////////////////////////////////////
 
   intermediateEquipment(data, event, root_level, rootindex, subcategoryIndex, subsubcategoryINdex, typeIndex?: any) {
    ///////////////////////////////////////// intermediate ////////////////////////////////////
    console.log(data, "<", event, "<", root_level, "<", rootindex, "<", subcategoryIndex, "<", subsubcategoryINdex, "<", typeIndex);
    // this.ri = 0
    // this.si = 0
    // this.ssi = 0
    
      this.ti = 0
  
   
      this.allAssetInventoryCategory.forEach((Rootelement, index) => {

        if (index == rootindex) {
         
          console.log("sub length:", this.allAssetInventoryCategory[rootindex].subCategories.length);
  
          if (
            this.allAssetInventoryCategory[index].hasOwnProperty('subCategories')
            &&
            this.allAssetInventoryCategory[index].subCategories.length > 0
          ) {
            this.si=0
            this.allAssetInventoryCategory[index].subCategories.forEach((subelement, subindex) => {
              // debugger;
              if (subelement['showChildren'] == true) {
                this.si++
              }
              if (subindex == subcategoryIndex) {
               
                if (
                  this.allAssetInventoryCategory[index].subCategories[subindex].hasOwnProperty('subSubCategories')
                  &&
                  this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
  // this.ssi=0
  // debugger;
  this.ssi=0

  
                  this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {
                    if (subsubelement['showChildren'] == true) {
                      this.ssi++
                    }
                        
                    if (subsubINdex == subsubcategoryINdex) {
 
                      // ////console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length);
                      // ////console.log(">>>", this.subsubFlag);
                     
  
  
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        // ////console.log(element);
                        // this.typeFlag=0
                        this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {
                          // if(typeIndex==childIndex){
                          if (typeelement['showChildren'] == true) {
                            this.ti++
                          }
                          console.log(this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length, "type>>", this.ti);
  
                          if (this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > this.ti) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subsubelement.inter = true;
                          } else if (this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length == this.ti) {
  
                            subsubelement.inter = false;
                            subsubelement.showChildren = true;
                          }
                        //  if (this.ti== 0) {
                        //   subsubelement.inter = false;
                        //   subsubelement.showChildren = false;
                        // }
  
                          if (this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length > this.ssi) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subelement.inter = true;
                          } else if (this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length == this.ssi) {
  
                            subelement.inter = false;
                            subelement.showChildren = true;
                          }
                          
                          if (this.allAssetInventoryCategory[index].subCategories.length > this.si) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subelement.inter = true;
                          } else if (this.allAssetInventoryCategory[index].subCategories.length == this.si) {
                            Rootelement.inter = false;
                            Rootelement.showChildren = true;
                          }
                        
                          //  }
                        })
                        if (this.ti== 0) {
                          subsubelement.inter = false;
                          subsubelement.showChildren = false;
                        }
                      }
                      
                     
                            }
                            console.log(this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length, "subsub>>", this.ssi);
  
                            if (this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length > this.ssi) {
                              // Rootelement.inter=true;
                              // subelement.inter=true;
                              subelement.inter = true;
                            } else if (this.allAssetInventoryCategory[index].subCategories[subindex].subSubCategories.length == this.ssi) {
        
                              subelement.inter = false;
                              subelement.showChildren = true;
                            } 
                           
                           
                  })
                  if (this.ssi== 0 && this.ti==0) {
                      subelement.inter = false;
                      subelement.showChildren = false;
                    }
                }
  
            
          
                
                console.log(this.allAssetInventoryCategory[index].subCategories.length, "sub>>", this.si);
  
               
                 }
                 if (this.allAssetInventoryCategory[index].subCategories.length > this.si) {
                  // Rootelement.inter=true;
                  // subelement.inter=true;
                  Rootelement.inter = true;
                } else if (this.allAssetInventoryCategory[index].subCategories.length == this.si) {
  
                  Rootelement.inter = false;
                  Rootelement.showChildren = true;
                } 
               
             })
             if (this.ssi== 0 && this.ti==0 && this.si==0) {
              Rootelement.inter = false;
              Rootelement.showChildren = false;
            }
          }
  
  
        }
      });
   
}
 
beveargeschangeFlag(_id, data, event, root_level, rootindex?: any, subcategoryIndex?: any, subsubcategoryINdex?: any, typeIndex?: any) {
  //console.log( ">>>" , data, ">>>" , event.checked);
  this.count=0
  console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
  if (root_level == 'root') {
    this.allRetailBeveragesCategories.forEach((element, index) => {
      if (index == rootindex) {
        element.showChildren = event.checked
        if (
          this.allRetailBeveragesCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailBeveragesCategories[index].subCategories.length > 0
        ) {
          this.allRetailBeveragesCategories[index].subCategories.forEach((element, subcategoryIndex = index) => {
            element.showChildren = event.checked;
            if (
              this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
              &&
              this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
                element.showChildren = event.checked;
                if (
                  // tslint:disable-next-line: max-line-length
                  this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  //console.log(element);
                  this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, subSubCategoryTypeIndex = index) => {
                    element.showChildren = event.checked;
                  })
                }


              })
            }
          })
        }
      }
    });
    //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
  }
  else if (root_level == 'sub') {
    this.allRetailBeveragesCategories.forEach((Rootelement, index) => {
      if (index == rootindex) {
        // Rootelement.showChildren = event.checked
        if (
          this.allRetailBeveragesCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailBeveragesCategories[index].subCategories.length > 0
        ) {
          this.allRetailBeveragesCategories[index].subCategories.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              subelement.showChildren = event.checked;
              if (
                this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((subsubelement, subsubcategoryINdex = index) => {
                  subsubelement.showChildren = event.checked;
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length

                    this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((typeelement, subSubCategoryTypeIndex = index) => {
                      typeelement.showChildren = event.checked;
                    })
                  }


                })
              }
            }

          })

        }


      }
    });
    //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
  } else if (root_level == 'subsub') {
    this.allRetailBeveragesCategories.forEach((element, index) => {
      if (index == rootindex) {
        // element.showChildren = event.checked
        if (
          this.allRetailBeveragesCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailBeveragesCategories[index].subCategories.length > 0
        ) {
          this.allRetailBeveragesCategories[index].subCategories.forEach((element, subindex) => {
            if (subindex == subcategoryIndex) {
              //             element.showChildren = event.checked;
              if (
                this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubINdex) => {
                  if (subsubINdex == subsubcategoryINdex) {
                    element.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, subSubCategoryTypeIndex = index) => {
                        element.showChildren = event.checked;
                      })
                    }
                  }

                })
              }
              // }
            }
          })
        }


      }
    });
    //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
  } else if (root_level == 'type') {
    this.allRetailBeveragesCategories.forEach((element, index) => {
      if (index == rootindex) {
        if (
          this.allRetailBeveragesCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailBeveragesCategories[index].subCategories.length > 0
        ) {
          this.allRetailBeveragesCategories[index].subCategories.forEach((element, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubINdex) => {
                  if (subsubINdex == subsubcategoryINdex) {
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, childIndex) => {
                        if (typeIndex == childIndex)
                          element.showChildren = event.checked;
                      })
                    }


                  }
                })
              }
              // }
            }
          })
        }


      }
    });
    //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
  }
  ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////

  this.allRetailBeveragesCategories.forEach((Rootelement, index) => {
    if (index == rootindex) {
      if (
        this.allRetailBeveragesCategories[index].hasOwnProperty('subCategories')
        &&
        this.allRetailBeveragesCategories[index].subCategories.length > 0
      ) {
        this.allRetailBeveragesCategories[index].subCategories.forEach((subelement, subindex) => {
          if (subindex == subcategoryIndex) {
            if (
              this.allRetailBeveragesCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
              &&
              this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {
                // if(subsubINdex==0){
                //   this.subsubFlag=0
                // }
                if (subsubINdex == subsubcategoryINdex) {
                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  //console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length);
                  //console.log(">>>", this.subsubFlag);


                  if (this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    this.typeFlag = 0
                    this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {
                      if (typeIndex == childIndex) {
                        typeelement.showChildren = event.checked;
                      }
                      if (typeelement.showChildren == true) {
                        this.typeFlag++
                      }
                      // //console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length);
                      // //console.log(">>>", this.typeFlag);


                      if (this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length == this.typeFlag) {
                        subsubelement.showChildren = true
                      } else {
                        subsubelement.showChildren = false;
                      }

                    })
                  }


                }
              })
            }
            // }
          }
        })
      }


    }
  });
  //subsub
  this.subsubFlag = 0

  this.allRetailBeveragesCategories.forEach((Rootelement, index) => {
    if (index == rootindex) {
      if (
        this.allRetailBeveragesCategories[index].hasOwnProperty('subCategories')
        &&
        this.allRetailBeveragesCategories[index].subCategories.length > 0
      ) {
        this.allRetailBeveragesCategories[index].subCategories.forEach((subelement, subindex) => {
          if (subindex == subcategoryIndex) {

            if (
              this.allRetailBeveragesCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
              &&
              this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {

                if (subsubelement.showChildren == true) {
                  this.subsubFlag++
                }
                // //console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length);
                // //console.log(">>>", this.subsubFlag);


                if (this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length == this.subsubFlag) {
                  subelement.showChildren = true
                } else {
                  subelement.showChildren = false;
                }
                if (
                  // tslint:disable-next-line: max-line-length
                  this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {



                  })
                }
              })
            }

          }
        })
      }


    }
  });

  //////subflag
  this.subFlag = 0
  this.subsubFlag = 0

  this.allRetailBeveragesCategories.forEach((Rootelement, index) => {
    if (index == rootindex) {
      if (
        this.allRetailBeveragesCategories[index].hasOwnProperty('subCategories')
        &&
        this.allRetailBeveragesCategories[index].subCategories.length > 0
      ) {
        this.allRetailBeveragesCategories[index].subCategories.forEach((subelement, subindex) => {
          // if(subindex==subcategoryIndex){
          if (subelement.showChildren == true) {
            this.subFlag++
          }
          console.log("<<<", this.allRetailBeveragesCategories[index].subCategories.length);
          console.log(">>>", this.subFlag);


          if (this.allRetailBeveragesCategories[index].subCategories.length == this.subFlag) {
            Rootelement.showChildren = true
          } else {
            Rootelement.showChildren = false;
          }

          if (
            this.allRetailBeveragesCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
            &&
            this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length > 0
          ) {
            // tslint:disable-next-line: max-line-length

            this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {


              if (
                // tslint:disable-next-line: max-line-length
                this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                &&
                // tslint:disable-next-line: max-line-length
                this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                // //console.log(element);
                // this.typeFlag=0
                this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {



                })
              }
            })
          }

          // }
        })
      }


    }
  });
  if (this.allRetailBeveragesCategories[rootindex].showChildren == false) {
    // element.showChildren = event.checked
    if (
      this.allRetailBeveragesCategories[rootindex].hasOwnProperty('subCategories')
      &&
      this.allRetailBeveragesCategories[rootindex].subCategories.length > 0
    ) {
      this.allRetailBeveragesCategories[rootindex].subCategories.forEach((element, subindex) => {
       if( element.showChildren == false){
        if (
          this.allRetailBeveragesCategories[rootindex].subCategories[subindex].hasOwnProperty('subSubCategories')
          &&
          this.allRetailBeveragesCategories[rootindex].subCategories[subindex].subSubCategories.length > 0
        ) {
          // tslint:disable-next-line: max-line-length
          this.allRetailBeveragesCategories[rootindex].subCategories[subindex].subSubCategories.forEach((element, subsubINdex) => {
            if( element.showChildren == false){
            if (
              // tslint:disable-next-line: max-line-length
              this.allRetailBeveragesCategories[rootindex].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
              &&
              // tslint:disable-next-line: max-line-length
              this.allRetailBeveragesCategories[rootindex].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              //console.log(element);
              this.allRetailBeveragesCategories[rootindex].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((element, liquorSubSubVarientTypeIndex) => {
              if( element.showChildren == true)
              {               
                this.count=1;
                return false
              }
              })
            }
  
  
          }
          else{
            this.count=1;
            return false;
          }
          })
        }
      }
      if(element.showChildren==true){
      this.count=1;
      return false;
      }
      })
    }
  }
  else{
    this.count=1
  }
  
  console.log(this.count);
  let id
  if(this.count==1){
  if(this.categoryType.length==0){
  this.categoryType.push(this.allRetailBeveragesCategories[rootindex])
  }else{
  this.categoryType.forEach(element => {
  if(element['_id']==this.allRetailBeveragesCategories[rootindex]['_id']){
    element=this.allRetailBeveragesCategories[rootindex]
    this.id=element['_id'];
  }
  });
  if(this.id!=this.allRetailBeveragesCategories[rootindex]['_id']){
  this.categoryType.push(this.allRetailBeveragesCategories[rootindex])
  }
  }
  }
  if(this.count==0){  
  let i=0;
  this.categoryType.forEach(element => {
  
  if(element['_id']==this.allRetailBeveragesCategories[rootindex]['_id']){
  this.categoryType.splice(i, 1)
  }else{
   console.log("not match");
   
  }
  i++;
  });
  }
  console.log(this.categoryType);
  
   

  this.intermediateBeverages(data, event, root_level, rootindex = rootindex, subcategoryIndex = subcategoryIndex, subsubcategoryINdex = subsubcategoryINdex, typeIndex = typeIndex)
  this.DisabledTree(_id,event)
}

/////////////////////////////////// interminate for beverages tree ////////////////////////////
intermediateBeverages(data, event, root_level, rootindex, subcategoryIndex, subsubcategoryINdex, typeIndex?: any) {
  ///////////////////////////////////////// intermediate ////////////////////////////////////
  console.log(data, "<", event, "<", root_level, "<", rootindex, "<", subcategoryIndex, "<", subsubcategoryINdex, "<", typeIndex);
  // this.ri = 0
  // this.si = 0
  // this.ssi = 0
  
    this.ti = 0

 
    this.allRetailBeveragesCategories.forEach((Rootelement, index) => {

      if (index == rootindex) {
       
        console.log("sub length:", this.allRetailBeveragesCategories[rootindex].subCategories.length);

        if (
          this.allRetailBeveragesCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailBeveragesCategories[index].subCategories.length > 0
        ) {
          this.si=0
          this.allRetailBeveragesCategories[index].subCategories.forEach((subelement, subindex) => {
            // debugger;
            if (subelement['showChildren'] == true) {
              this.si++
            }
            if (subindex == subcategoryIndex) {
             
              if (
                this.allRetailBeveragesCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
// this.ssi=0
// debugger;
this.ssi=0


                this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {
                  if (subsubelement['showChildren'] == true) {
                    this.ssi++
                  }
                      
                  if (subsubINdex == subsubcategoryINdex) {

                    // ////console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length);
                    // ////console.log(">>>", this.subsubFlag);
                   


                    if (
                      // tslint:disable-next-line: max-line-length
                      this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      // ////console.log(element);
                      // this.typeFlag=0
                      this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {
                        // if(typeIndex==childIndex){
                        if (typeelement['showChildren'] == true) {
                          this.ti++
                        }
                        console.log(this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length, "type>>", this.ti);

                        if (this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > this.ti) {
                          // Rootelement.inter=true;
                          // subelement.inter=true;
                          subsubelement.inter = true;
                        } else if (this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length == this.ti) {

                          subsubelement.inter = false;
                          subsubelement.showChildren = true;
                        }
                      //  if (this.ti== 0) {
                      //   subsubelement.inter = false;
                      //   subsubelement.showChildren = false;
                      // }

                        if (this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length > this.ssi) {
                          // Rootelement.inter=true;
                          // subelement.inter=true;
                          subelement.inter = true;
                        } else if (this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length == this.ssi) {

                          subelement.inter = false;
                          subelement.showChildren = true;
                        }
                        
                        if (this.allRetailBeveragesCategories[index].subCategories.length > this.si) {
                          // Rootelement.inter=true;
                          // subelement.inter=true;
                          subelement.inter = true;
                        } else if (this.allRetailBeveragesCategories[index].subCategories.length == this.si) {
                          Rootelement.inter = false;
                          Rootelement.showChildren = true;
                        }
                      
                        //  }
                      })
                      if (this.ti== 0) {
                        subsubelement.inter = false;
                        subsubelement.showChildren = false;
                      }
                    }
                    
                   
                          }
                          console.log(this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length, "subsub>>", this.ssi);

                          if (this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length > this.ssi) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subelement.inter = true;
                          } else if (this.allRetailBeveragesCategories[index].subCategories[subindex].subSubCategories.length == this.ssi) {
      
                            subelement.inter = false;
                            subelement.showChildren = true;
                          } 
                         
                         
                })
                if (this.ssi== 0 && this.ti==0) {
                    subelement.inter = false;
                    subelement.showChildren = false;
                  }
              }

          
        
              
              console.log(this.allRetailBeveragesCategories[index].subCategories.length, "sub>>", this.si);

             
               }
               if (this.allRetailBeveragesCategories[index].subCategories.length > this.si) {
                // Rootelement.inter=true;
                // subelement.inter=true;
                Rootelement.inter = true;
              } else if (this.allRetailBeveragesCategories[index].subCategories.length == this.si) {

                Rootelement.inter = false;
                Rootelement.showChildren = true;
              } 
             
           })
           if (this.ssi== 0 && this.ti==0 && this.si==0) {
            Rootelement.inter = false;
            Rootelement.showChildren = false;
          }
        }


      }
    });
 
}





////////////////////////////////////////////////////////////////////////////////////////////////////
 /////////////////////////////////////////////////////////////////////////////////////////////////////////
 /////////////////////////////////////// RETAIL FOOD CATEGORY TREE //////////////////////////////////////
 //////////////////////////////////////////////////////////////////////////////////////////////////////////
 /////////////////////////////////////////////////////////////////////////////////////////////////////////


 getAllRetailFoodCategories() {
  this.menuService.getAllRetailFoodCategories().subscribe(data => {
    console.log("All Retail Food categories", data);
    this.allRetailFoodCategories = data['data']
    this.allRetailFoodCategories.forEach(element => {
      this.categoryType.forEach(ele=>{
      if(element['_id']==ele['_id'])
      element=ele
      
      })
       this.retailFoodtree.push(element)
    });
this.allRetailFoodCategories=this.retailFoodtree
console.log("asset inventory:", this.allRetailFoodCategories);

  })
}


//*************************OPEN ROOT CATEGORY IN RETAIL FOOD ********************** */
openRetailFoodRootCategoryDailog() {
  const dialogRef = this.dialog.open(RootCategoryRetailFoodComponent, {
    width: '460px',
    disableClose: true,
  })
  dialogRef.afterClosed().subscribe(result => {
    this.getAllRetailFoodCategories()
  })
}

//*************************OPEN SUB CATEGORY IN RETAIL FOOD ********************** */
openRetailFoodSubCategoryDialog(rootCategoryid) {
  const dialogRef = this.dialog.open(SubCategoryRetailFoodComponent, {
    width: '460px',
    disableClose: true,
    data: rootCategoryid
  })
  dialogRef.afterClosed().subscribe(result => {
    this.getAllRetailFoodCategories()
  })
}

//*************************OPEN SUB SUB CATEGORY IN RETAIL FOOD ********************** */
openRetailFoodSubSubCategoryDialog(rootCategoryID, subCategoryID) {
  const dialogRef = this.dialog.open(SubSubCategoryRetailFoodComponent, {
    width: '460px',
    disableClose: true,
    data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
  })
  dialogRef.afterClosed().subscribe(result => {
    this.getAllRetailFoodCategories()
  })
}


//*************************OPEN SUB SUB CATEGORY TYPE IN RETAIL FOOD ********************** */
openRetailFoodSubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
  const dialogRef = this.dialog.open(SubSubCategoryTypeRetailFoodComponent, {
    width: '460px',
    disableClose: true,
    data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
  })
  dialogRef.afterClosed().subscribe(result => {
    this.getAllRetailFoodCategories()
  })
}
DisabledTree(_id, event){
  if(event.checked==true){
  this.disabledflag++
  }else{
    this.disabledflag=0
  }
  this.disabled_id=_id
}
RetailfoodchangeFlag(_id,data, event, root_level, rootindex?: any, subcategoryIndex?: any, subsubcategoryINdex?: any, typeIndex?: any) {
  //console.log( ">>>" , data, ">>>" , event.checked);
  this.count=0
  console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
  if (root_level == 'root') {
    this.allRetailFoodCategories.forEach((element, index) => {
      if (index == rootindex) {
        element.showChildren = event.checked
        if (
          this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailFoodCategories[index].subCategories.length > 0
        ) {
          this.allRetailFoodCategories[index].subCategories.forEach((element, subcategoryIndex = index) => {
            element.showChildren = event.checked;
            if (
              this.allRetailFoodCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
              &&
              this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
                element.showChildren = event.checked;
                if (
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  //console.log(element);
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, subSubCategoryTypeIndex = index) => {
                    element.showChildren = event.checked;
                  })
                }


              })
            }
          })
        }
      }
    });
    //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
  }
  else if (root_level == 'sub') {
    this.allRetailFoodCategories.forEach((Rootelement, index) => {
      if (index == rootindex) {
        // Rootelement.showChildren = event.checked
        if (
          this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailFoodCategories[index].subCategories.length > 0
        ) {
          this.allRetailFoodCategories[index].subCategories.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              subelement.showChildren = event.checked;
              if (
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((subsubelement, subsubcategoryINdex = index) => {
                  subsubelement.showChildren = event.checked;
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length

                    this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((typeelement, subSubCategoryTypeIndex = index) => {
                      typeelement.showChildren = event.checked;
                    })
                  }


                })
              }
            }

          })

        }


      }
    });
    //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
  } else if (root_level == 'subsub') {
    this.allRetailFoodCategories.forEach((element, index) => {
      if (index == rootindex) {
        // element.showChildren = event.checked
        if (
          this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailFoodCategories[index].subCategories.length > 0
        ) {
          this.allRetailFoodCategories[index].subCategories.forEach((element, subindex) => {
            if (subindex == subcategoryIndex) {
              //             element.showChildren = event.checked;
              if (
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubINdex) => {
                  if (subsubINdex == subsubcategoryINdex) {
                    element.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, subSubCategoryTypeIndex = index) => {
                        element.showChildren = event.checked;
                      })
                    }
                  }

                })
              }
              // }
            }
          })
        }


      }
    });
    //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
  } else if (root_level == 'type') {
    this.allRetailFoodCategories.forEach((element, index) => {
      if (index == rootindex) {
        if (
          this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailFoodCategories[index].subCategories.length > 0
        ) {
          this.allRetailFoodCategories[index].subCategories.forEach((element, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubINdex) => {
                  if (subsubINdex == subsubcategoryINdex) {
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, childIndex) => {
                        if (typeIndex == childIndex)
                          element.showChildren = event.checked;
                      })
                    }


                  }
                })
              }
              // }
            }
          })
        }


      }
    });
    //console.log('this.allRetailBeveragesCategories >', this.allRetailBeveragesCategories)
  }
  ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////

  this.allRetailFoodCategories.forEach((Rootelement, index) => {
    if (index == rootindex) {
      if (
        this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
        &&
        this.allRetailFoodCategories[index].subCategories.length > 0
      ) {
        this.allRetailFoodCategories[index].subCategories.forEach((subelement, subindex) => {
          if (subindex == subcategoryIndex) {
            if (
              this.allRetailFoodCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
              &&
              this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {
                // if(subsubINdex==0){
                //   this.subsubFlag=0
                // }
                if (subsubINdex == subsubcategoryINdex) {
                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  //console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length);
                  //console.log(">>>", this.subsubFlag);


                  if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    this.typeFlag = 0
                    this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {
                      if (typeIndex == childIndex) {
                        typeelement.showChildren = event.checked;
                      }
                      if (typeelement.showChildren == true) {
                        this.typeFlag++
                      }
                      // //console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length);
                      // //console.log(">>>", this.typeFlag);


                      if (this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length == this.typeFlag) {
                        subsubelement.showChildren = true
                      } else {
                        subsubelement.showChildren = false;
                      }

                    })
                  }


                }
              })
            }
            // }
          }
        })
      }


    }
  });
  //subsub
  this.subsubFlag = 0

  this.allRetailFoodCategories.forEach((Rootelement, index) => {
    if (index == rootindex) {
      if (
        this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
        &&
        this.allRetailFoodCategories[index].subCategories.length > 0
      ) {
        this.allRetailFoodCategories[index].subCategories.forEach((subelement, subindex) => {
          if (subindex == subcategoryIndex) {

            if (
              this.allRetailFoodCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
              &&
              this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {

                if (subsubelement.showChildren == true) {
                  this.subsubFlag++
                }
                // //console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length);
                // //console.log(">>>", this.subsubFlag);


                if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length == this.subsubFlag) {
                  subelement.showChildren = true
                } else {
                  subelement.showChildren = false;
                }
                if (
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {



                  })
                }
              })
            }

          }
        })
      }


    }
  });

  //////subflag
  this.subFlag = 0
  this.subsubFlag = 0

  this.allRetailFoodCategories.forEach((Rootelement, index) => {
    if (index == rootindex) {
      if (
        this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
        &&
        this.allRetailFoodCategories[index].subCategories.length > 0
      ) {
        this.allRetailFoodCategories[index].subCategories.forEach((subelement, subindex) => {
          // if(subindex==subcategoryIndex){
          if (subelement.showChildren == true) {
            this.subFlag++
          }
          console.log("<<<", this.allRetailFoodCategories[index].subCategories.length);
          console.log(">>>", this.subFlag);


          if (this.allRetailFoodCategories[index].subCategories.length == this.subFlag) {
            Rootelement.showChildren = true
          } else {
            Rootelement.showChildren = false;
          }

          if (
            this.allRetailFoodCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
            &&
            this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > 0
          ) {
            // tslint:disable-next-line: max-line-length

            this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {


              if (
                // tslint:disable-next-line: max-line-length
                this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                &&
                // tslint:disable-next-line: max-line-length
                this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                // //console.log(element);
                // this.typeFlag=0
                this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {



                })
              }
            })
          }

          // }
        })
      }


    }
  });
  if (this.allRetailFoodCategories[rootindex].showChildren == false) {
    // element.showChildren = event.checked
    if (
      this.allRetailFoodCategories[rootindex].hasOwnProperty('subCategories')
      &&
      this.allRetailFoodCategories[rootindex].subCategories.length > 0
    ) {
      this.allRetailBeveragesCategories[rootindex].subCategories.forEach((element, subindex) => {
       if( element.showChildren == false){
        if (
          this.allRetailFoodCategories[rootindex].subCategories[subindex].hasOwnProperty('subSubCategories')
          &&
          this.allRetailFoodCategories[rootindex].subCategories[subindex].subSubCategories.length > 0
        ) {
          // tslint:disable-next-line: max-line-length
          this.allRetailFoodCategories[rootindex].subCategories[subindex].subSubCategories.forEach((element, subsubINdex) => {
            if( element.showChildren == false){
            if (
              // tslint:disable-next-line: max-line-length
              this.allRetailFoodCategories[rootindex].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
              &&
              // tslint:disable-next-line: max-line-length
              this.allRetailFoodCategories[rootindex].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              //console.log(element);
              this.allRetailFoodCategories[rootindex].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((element, liquorSubSubVarientTypeIndex) => {
              if( element.showChildren == true)
              {               
                this.count=1;
                return false
              }
              })
            }
  
  
          }
          else{
            this.count=1;
            return false;
          }
          })
        }
      }
      if(element.showChildren==true){
      this.count=1;
      return false;
      }
      })
    }
  }
  else{
    this.count=1
  }
  
  console.log(this.count);
  let id
  if(this.count==1){
  if(this.categoryType.length==0){
  this.categoryType.push(this.allRetailFoodCategories[rootindex])
  }else{
  this.categoryType.forEach(element => {
  if(element['_id']==this.allRetailFoodCategories[rootindex]['_id']){
    element=this.allRetailFoodCategories[rootindex]
    this.id=element['_id'];
  }
  });
  if(this.id!=this.allRetailFoodCategories[rootindex]['_id']){
  this.categoryType.push(this.allRetailFoodCategories[rootindex])
  }
  }
  }
  if(this.count==0){  
  let i=0;
  this.categoryType.forEach(element => {
  
  if(element['_id']==this.allRetailFoodCategories[rootindex]['_id']){
  this.categoryType.splice(i, 1)
  }else{
   console.log("not match");
   
  }
  i++;
  });
  }
  console.log(this.categoryType);
  this.intermediateRetailFood(data, event, root_level, rootindex = rootindex, subcategoryIndex = subcategoryIndex, subsubcategoryINdex = subsubcategoryINdex, typeIndex = typeIndex)
  this.DisabledTree(_id,event)
 
}
intermediateRetailFood(data, event, root_level, rootindex, subcategoryIndex, subsubcategoryINdex, typeIndex?: any) {
  ///////////////////////////////////////// intermediate ////////////////////////////////////
  console.log(data, "<", event, "<", root_level, "<", rootindex, "<", subcategoryIndex, "<", subsubcategoryINdex, "<", typeIndex);
  // this.ri = 0
  // this.si = 0
  // this.ssi = 0
  
    this.ti = 0

 
    this.allRetailFoodCategories.forEach((Rootelement, index) => {

      if (index == rootindex) {
       
        console.log("sub length:", this.allRetailFoodCategories[rootindex].subCategories.length);

        if (
          this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailFoodCategories[index].subCategories.length > 0
        ) {
          this.si=0
          this.allRetailFoodCategories[index].subCategories.forEach((subelement, subindex) => {
            // debugger;
            if (subelement['showChildren'] == true) {
              this.si++
            }
            if (subindex == subcategoryIndex) {
             
              if (
                this.allRetailFoodCategories[index].subCategories[subindex].hasOwnProperty('subSubCategories')
                &&
                this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
// this.ssi=0
// debugger;
this.ssi=0


                this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.forEach((subsubelement, subsubINdex) => {
                  if (subsubelement['showChildren'] == true) {
                    this.ssi++
                  }
                      
                  if (subsubINdex == subsubcategoryINdex) {

                    // ////console.log("<<<", this.allRetailBeveragesCategories[index].subCategories[subcategoryIndex].subSubCategories.length);
                    // ////console.log(">>>", this.subsubFlag);
                   


                    if (
                      // tslint:disable-next-line: max-line-length
                      this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].hasOwnProperty('subSubCategoryType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      // ////console.log(element);
                      // this.typeFlag=0
                      this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.forEach((typeelement, childIndex) => {
                        // if(typeIndex==childIndex){
                        if (typeelement['showChildren'] == true) {
                          this.ti++
                        }
                        console.log(this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length, "type>>", this.ti);

                        if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length > this.ti) {
                          // Rootelement.inter=true;
                          // subelement.inter=true;
                          subsubelement.inter = true;
                        } else if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories[subsubINdex].subSubCategoryType.length == this.ti) {

                          subsubelement.inter = false;
                          subsubelement.showChildren = true;
                        }
                      //  if (this.ti== 0) {
                      //   subsubelement.inter = false;
                      //   subsubelement.showChildren = false;
                      // }

                        if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > this.ssi) {
                          // Rootelement.inter=true;
                          // subelement.inter=true;
                          subelement.inter = true;
                        } else if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length == this.ssi) {

                          subelement.inter = false;
                          subelement.showChildren = true;
                        }
                        
                        if (this.allRetailFoodCategories[index].subCategories.length > this.si) {
                          // Rootelement.inter=true;
                          // subelement.inter=true;
                          subelement.inter = true;
                        } else if (this.allRetailFoodCategories[index].subCategories.length == this.si) {
                          Rootelement.inter = false;
                          Rootelement.showChildren = true;
                        }
                      
                        //  }
                      })
                      if (this.ti== 0) {
                        subsubelement.inter = false;
                        subsubelement.showChildren = false;
                      }
                    }
                    
                   
                          }
                          console.log(this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length, "subsub>>", this.ssi);

                          if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length > this.ssi) {
                            // Rootelement.inter=true;
                            // subelement.inter=true;
                            subelement.inter = true;
                          } else if (this.allRetailFoodCategories[index].subCategories[subindex].subSubCategories.length == this.ssi) {
      
                            subelement.inter = false;
                            subelement.showChildren = true;
                          } 
                         
                         
                })
                if (this.ssi== 0 && this.ti==0) {
                    subelement.inter = false;
                    subelement.showChildren = false;
                  }
              }

          
        
              
              console.log(this.allRetailFoodCategories[index].subCategories.length, "sub>>", this.si);

             
               }
               if (this.allRetailFoodCategories[index].subCategories.length > this.si) {
                // Rootelement.inter=true;
                // subelement.inter=true;
                Rootelement.inter = true;
              } else if (this.allRetailFoodCategories[index].subCategories.length == this.si) {

                Rootelement.inter = false;
                Rootelement.showChildren = true;
              } 
             
           })
           if (this.ssi== 0 && this.ti==0 && this.si==0) {
            Rootelement.inter = false;
            Rootelement.showChildren = false;
          }
        }


      }
    });
 
}

back(){
  if(this.attributeKey=="equipment"){
    this.route.navigate(['inventory/equipment-attributes'])  
  }else if(this.attributeKey=="liquor"){
  this.route.navigate(['menu'])
  }
  else if(this.attributeKey=="beverage"){
    this.route.navigate(['beverages_and_retailfood'])
  }
  else if(this.attributeKey=="retailfood" || this.attributeKey== "retailbeverages"){
    this.route.navigate(['beverages_and_retailfood'])
  }
}
disbled(){
  if(this.attributeKey=='liquor'){
  this.menuService.editObject.categoryType.forEach((element, index) => {
  if(element.showChildren==true){
  this.disabledflag=1
  this.disabled_id=element['_id']
  return false;
  }
  
  if (
  this.menuService.editObject.categoryType[index].hasOwnProperty('liquorVarients')
  &&
  this.menuService.editObject.categoryType[index].liquorVarients.length > 0
  ) {
  this.menuService.editObject.categoryType[index].liquorVarients.forEach((element, subcategoryIndex = index) => {
  if(element.showChildren==true){
  this.disabledflag=1
  this.disabled_id=element['_id']
  return false;
  }
  if (
  this.menuService.editObject.categoryType[index].liquorVarients[subcategoryIndex].hasOwnProperty('liquorSubVarients')
  &&
  this.menuService.editObject.categoryType[index].liquorVarients[subcategoryIndex].liquorSubVarients.length > 0
  ) {
  // tslint:disable-next-line: max-line-length
  this.menuService.editObject.categoryType[index].liquorVarients[subcategoryIndex].liquorSubVarients.forEach((element, subsubcategoryINdex = index) => {
  if(element.showChildren==true){
  this.disabledflag=1
  this.disabled_id=element['_id']
  return false;
  }
  if (
  // tslint:disable-next-line: max-line-length
  this.menuService.editObject.categoryType[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
  &&
  // tslint:disable-next-line: max-line-length
  this.menuService.editObject.categoryType[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.length > 0
  ) {
  // tslint:disable-next-line: max-line-length
  //console.log(element);
  this.menuService.editObject.categoryType[index].liquorVarients[subcategoryIndex].liquorSubVarients[subsubcategoryINdex].liquorSubSubVarientType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
  if(element.showChildren==true){
  this.disabledflag=1
  this.disabled_id=element['_id']
  return false;
  }
  })
  }
  
  
  })
  }
  })
  }
  
  });
}else {
  this.menuService.editObject.categoryType.forEach((element, index) => {
  
    if(element.showChildren==true){
      this.disabledflag=1
      this.disabled_id=element['_id']
      return false;
      }
      if (
        this.menuService.editObject.categoryType[index].hasOwnProperty('subCategories')
        &&
        this.menuService.editObject.categoryType[index].subCategories.length > 0
      ) {
        this.menuService.editObject.categoryType[index].subCategories.forEach((element, subcategoryIndex = index) => {
          if(element.showChildren==true){
            this.disabledflag=1
            this.disabled_id=element['_id']
            return false;
            }
          if (
            this.menuService.editObject.categoryType[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
            &&
            this.menuService.editObject.categoryType[index].subCategories[subcategoryIndex].subSubCategories.length > 0
          ) {
            // tslint:disable-next-line: max-line-length
            this.menuService.editObject.categoryType[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
              if(element.showChildren==true){
                this.disabledflag=1
                this.disabled_id=element['_id']
                return false;
                }
              if (
                // tslint:disable-next-line: max-line-length
                this.menuService.editObject.categoryType[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                &&
                // tslint:disable-next-line: max-line-length
                this.menuService.editObject.categoryType[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                //console.log(element);
                this.menuService.editObject.categoryType[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                  if(element.showChildren==true){
                    this.disabledflag=1
                    this.disabled_id=element['_id']
                    return false;
                    }
                })
              }


            })
          }
        })
      }
    
  });
}
  }

      }
