import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAddOnComponent } from './add-add-on.component';

describe('AddAddOnComponent', () => {
  let component: AddAddOnComponent;
  let fixture: ComponentFixture<AddAddOnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAddOnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAddOnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
