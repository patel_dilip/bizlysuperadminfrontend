import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { AddDishPopoverComponent } from 'src/app/PopoversList/add-dish-popover/add-dish-popover.component';


const URL = environment.base_Url + 'adon/uploadphotos'

@Component({
  selector: 'app-add-add-on',
  templateUrl: './add-add-on.component.html',
  styleUrls: ['./add-add-on.component.scss']
})
export class AddAddOnComponent implements OnInit {
  dishUnits: string[] = ['Kilogram', 'Grams', 'Unit'];
  addAddOnForm: FormGroup
  cuisineCtrl: FormControl
  selectedAlbums = []
  selecteddishes = []

  cuisines = []
  dishes = []
  tempDishes = []
  userid
  uploader: FileUploader;
  response: string
  multiResponse = []
  loading = false;
  add: FormArray;
  // ***************CONSTRUCTOR********************
  constructor(private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService,
    private menuService: AddMenuFoodsService, private router: Router, private matDialog: MatDialog) {
    //function call
    this.getAllDishes()



    // FETCH USER ID FROM LOCAL STORAGE

    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];


  }

  ngOnInit() {
    this.addDishes()


    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'adons'
    });
    //file upload response with url
    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.response = JSON.parse(response)
      console.log('response', this.response['adonPhotoUrls']);

      this.multiResponse.push(this.response['adonPhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      this.loading = false
    };

  }

  //on image upload loader start
  setLoading() {
    this.loading = true
  }


  //getAll dishes 
  getAllDishes() {
    this.dishes=[]
    this.menuService.getAllDishes().subscribe(data => {
      console.log(data);
      data['data'].forEach(element => {
        if(element.status){
this.dishes.push(element)
        }
      });
      
      this.tempDishes = data['data']
    })
  }

  //add cuisines form 
  addDishes() {
    this.addAddOnForm = this.fb.group({
      adonName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      unit: ['', Validators.required],
      veg_nonveg: ['', Validators.required],
      dishes: [''],
      images: [],
      userid: [''],
      adonType:this.fb.array([])
    })
    this.addAddonType()
  }
  RemoveAddonType(i){
    console.log(i);
    this.add.removeAt(i);
    console.log(this.add.value);
    
    
  }
  getControls() {
    return (this.addAddOnForm.get('adonType') as FormArray).controls;
  }
addAddonType(){
  this.add = this.addAddOnForm.get('adonType') as FormArray;
    this.add.push(this.fb.group({
      adonName_type: [''],
     type: ['']
    }))
    console.log(this.add.value);
    
}
  applyFilterDishes(event) {
    this.dishes = this.tempDishes
    console.log(event.target.value);
    if (event.target.value == "") {
      this.dishes = this.tempDishes
    } 
    else {
      const val = event.target.value.trim().toLowerCase();
    const filter = this.dishes.filter(function (d) {
    //console.log(d);
      let result
      console.log("ele", d);        
    result = d['dishName'].toLowerCase().indexOf(val) !== -1 
  
      return result
    });

    //console.log(filter);
    this.dishes = filter;
    
     }
  }


  onCheckdishes(s) {
    console.log(s);

    if (this.dishes.includes(s)) {
      this.dishes.splice(this.dishes.indexOf(s), 1)
     
      for( var i = 0; i < this.tempDishes.length; i++){ 
        if ( this.tempDishes[i]._id === s._id) {
          this.tempDishes.splice(i, 1); 
        }
      }
      if (this.selecteddishes.includes(s.dishName)) {
        alert("Already exists in selection")
      } else {
        this.selecteddishes.push(s)
      }
    }
    console.log(this.selecteddishes);

  }


  // ON REMOVE PUB MORE INFOS
  removedishes(s) {
    if (this.selecteddishes.includes(s)) {
      this.selecteddishes.splice(this.selecteddishes.indexOf(s), 1)
      this.dishes.push(s);
    
    }
  }

  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }

  //add dish pop over
  openAddDishesDialog(): void {
    const dialogRef = this.matDialog.open(AddDishPopoverComponent, {
      width: '540px',
      disableClose: true
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getAllDishes()
    });

  }


  //final add adon post
  onAddon() {
    this.loading = true
    let dishesIDS = []
    this.selecteddishes.forEach(element => {
      dishesIDS.push(element._id)
    });

    //patch all value to final object
    this.addAddOnForm.patchValue({
      dishes: dishesIDS,
      images: this.multiResponse,
      userid: this.userid
    })
    console.log(this.addAddOnForm.value);

    this.menuService.postAddOn(this.addAddOnForm.value).subscribe(res => {
      this.loading = false
      console.log(res);

      if (res['sucess'] == true) {

        Swal.fire('Add On Added Successfully', '', 'success')
        this.router.navigate(['/menu'])
      }
      else {
        Swal.fire('Failed to Add Add On', 'Something went wrong', 'warning')
      }
    })

  }

  checkAddonExist(event){
    this.addMenuFoodsService.checkAddonExist(event.target.value.toLowerCase()).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        Swal.fire("Addon Name Already Exist..!","", "warning")
        event.target.value=""
        this.addAddOnForm.patchValue({
          adonName:''
        })
      }

    })
  }



}
