import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewIngredientFoodInventoryComponent } from './view-ingredient-food-inventory.component';

describe('ViewIngredientFoodInventoryComponent', () => {
  let component: ViewIngredientFoodInventoryComponent;
  let fixture: ComponentFixture<ViewIngredientFoodInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewIngredientFoodInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewIngredientFoodInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
