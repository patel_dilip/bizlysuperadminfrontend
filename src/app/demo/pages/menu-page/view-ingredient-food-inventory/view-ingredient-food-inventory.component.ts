import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-ingredient-food-inventory',
  templateUrl: './view-ingredient-food-inventory.component.html',
  styleUrls: ['./view-ingredient-food-inventory.component.scss']
})
export class ViewIngredientFoodInventoryComponent implements OnInit {
 
  constructor( public dialogRef: MatDialogRef<ViewIngredientFoodInventoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { 
      console.log(data);
    
    }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}