import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialog } from '@angular/material';
import { AddAttributeRadioButtonComponent } from '../add-attribute-radio-button/add-attribute-radio-button.component';
import { AddAttributeCheckboxComponent } from '../add-attribute-checkbox/add-attribute-checkbox.component';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
@Component({
  selector: 'app-update-attribute',
  templateUrl: './update-attribute.component.html',
  styleUrls: ['./update-attribute.component.scss']
})
export class UpdateAttributeComponent implements OnInit {

  addAttributeForm: FormGroup
  SelectResponses: string[] = ['Input Box', 'Yes / No', 'Radio Button', 'Check Box']
  radiobutton = false
  yesorNo = false
  inputbox = false
  checkbox = false
  uploader: FileUploader
  response: string;
  radioOptions = []
  checkBoxOptions = []
  loading = false
  autoTrue = true
  loginUserID: any
  attributeObj: any
  attributeID: any
attributeKey:any
  path_name: string;

  constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService,
    private dialog: MatDialog, private router: Router) {

    //get the attribute object for update
    this.attributeObj = this.menuService.editObject

    console.log("edit obj", this.attributeObj);
    if (this.attributeObj == undefined) {
      this.router.navigateByUrl("/menu")
    }
    console.log(">>", this.attributeObj.attributeType);
    
    if(this.attributeObj.attributeType=="liquor"){
      this.path_name="Menu Management"
     }else if(this.attributeObj.attributeType=="equipment"){
       this.path_name="Inventory Management"  
 
     }else if(this.attributeObj.attributeType=="beverage"){
       this.path_name="Beverages And Retail Food"
     } else if(this.attributeObj.attributeType=="retailFood" || this.attributeObj.attributeType=="retailBeverages"){
       this.path_name="Beverages And Retail Food"
     }
    

    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID)


    this.addActivity()

    this.addAttributeForm.patchValue({
      attributeName: this.attributeObj.attributeName,
      display_name: this.attributeObj.display_name,
      selectResponse: this.attributeObj.responseType.elementName,
      inputBox: this.attributeObj.responseType.fieldValue,
      searchable: this.attributeObj.isSearchable,
      filterable: this.attributeObj.isFilterable
    })
    this.attributeKey = this.attributeObj.attributeType
    this.attributeID = this.attributeObj._id

    this.onClickResponse(this.attributeObj.responseType.elementName)
    if (this.attributeObj.responseType.elementName == "Radio Button") {
      this.radioOptions = this.attributeObj.responseType.options

    }
    if (this.attributeObj.responseType.elementName == "Check Box") {
      this.checkBoxOptions = this.attributeObj.responseType.options
    }
  }

  ngOnInit() {



  }

  // SETLOADING CALL FOR LOADER
  setLoading() {
    this.loading = true
  }

  //reactive form 
  addActivity() {
    this.addAttributeForm = this.fb.group({
      attributeName: ['', [Validators.required, Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      selectResponse: ['', Validators.required],
      display_name :['', [Validators.required, Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      inputBox: [''],
      radioButton: [''],
      yesNo: [''],
      checkBoxButton: [''],
      searchable: [false],
      filterable: [false]
     
    })
  }
  //on click respone dropdown respective response input open
  onClickResponse(response) {
    console.log(response);
    if (response == "Input Box") {
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true
      this.checkbox = false
    }
    if (response == "Yes / No") {
      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false
      this.checkbox = false
    }
    if (response == "Radio Button") {
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = false
    }
    if (response == "Check Box") {
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = true

    }

  }

  //In radio button reponse add new radio buttons
  addOption(): void {
    const dialogRef = this.dialog.open(AddAttributeRadioButtonComponent, {
      width: '350px',
      disableClose: true,
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result == undefined || result == [] || result == null) {

      } else {
        result.radioButton.forEach(element => {
          this.radioOptions.push(element)
        });
        this.addAttributeForm.controls.radioButton.reset()
        console.log(this.radioOptions);
      }
      // this.animal = result;
    });
  }

  //In Checkbox button reponse add new Checkbox buttons
  addCheckBox(): void {
    const dialogRef = this.dialog.open(AddAttributeCheckboxComponent, {
      width: '350px',
      disableClose: true,
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result == undefined || result == [] || result == null) {

      } else {
        result.checkBoxButton.forEach(element => {
          this.checkBoxOptions.push(element)
        });
        this.addAttributeForm.controls.checkBoxButton.reset()
        console.log(this.checkBoxOptions);
      }
      // this.animal = result;
    });
  }

  //remove option
  removeObject(option) {
    let res = this.addAttributeForm.controls.selectResponse.value
    if (res == "Radio Button") {
      this.radioOptions.splice(this.radioOptions.indexOf(option), 1)
    }
    if (res == "Check Box") {
      this.checkBoxOptions.splice(this.checkBoxOptions.indexOf(option), 1)
    }

  }


  //final create json object
  onAddAlbum() {
    this.loading = true
    let value = this.addAttributeForm.controls.selectResponse.value
    let fieldValue
    let options

    switch (value) {
      case "Input Box":
        fieldValue = this.addAttributeForm.controls.inputBox.value
        options = [
          {
            "optionLabel": "",
            "associateImage": ""
          }
        ]
        break
      case "Yes / No":
        fieldValue = this.addAttributeForm.controls.selectResponse.value
        options = [
          {
            "optionLable": "",
            "associateImage": ""
          }
        ]
        break
      case "Radio Button":
        options = this.radioOptions
        fieldValue = ""
        break
      case "Check Box":
        options = this.checkBoxOptions
        fieldValue = ""
        break
    }

    let attributeobj = {
      "attributeName": this.addAttributeForm.controls.attributeName.value,
      "responseType": {
        "elementName": this.addAttributeForm.controls.selectResponse.value,
        "fieldValue": fieldValue,
        "options": options,
      },
      "isSearchable": this.addAttributeForm.controls.searchable.value,
      "isFilterable": this.addAttributeForm.controls.filterable.value,
      "userid": this.loginUserID,
      "attributeType": this.attributeKey
      
    }
    console.log(attributeobj);

    //    console.log(this.addAttributeForm.value);
    this.menuService.updateAttribute(this.attributeID, attributeobj).subscribe(res => {
      console.log(res);
      this.loading = false
      if (res['sucess']) {
        Swal.fire('Attribute Updated Successfully', '', 'success')
        if(this.attributeObj.attributeType=="equipment"){
          this.router.navigate(['inventory/equipment-attributes'])  
        }else if(this.attributeObj.attributeType=="liquor"){
        this.router.navigate(['menu'])
        }
        else if(this.attributeObj.attributeType=="beverage"){
          this.router.navigate(['beverages_and_retailfood'])
        }
        else if(this.attributeObj.attributeType=="retailFood" || this.attributeObj.attributeType){
          this.router.navigate(['beverages_and_retailfood'])
        }

      }
      else {
        Swal.fire('Fail to Update Attribute', 'Something Went Wrong', 'warning')
      }
    })
  }

  back(){
    if(this.attributeObj.attributeType=="liquor"){
      this.router.navigate(['menu'])
    }else if(this.attributeObj.attributeType=="equipment"){
      this.router.navigate(['inventory/equipment-attributes'])  

    }else if(this.attributeObj.attributeType=="beverage"){
      this.router.navigate(['beverages_and_retailfood'])
    } else if(this.attributeObj.attributeType=="retailFood" || this.attributeObj.attributeType=="retailBeverages"){
      this.router.navigate(['beverages_and_retailfood'])
    }
  }


}
