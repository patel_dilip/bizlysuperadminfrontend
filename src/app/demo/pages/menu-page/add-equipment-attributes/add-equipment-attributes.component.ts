import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';

@Component({
  selector: 'app-add-equipment-attributes',
  templateUrl: './add-equipment-attributes.component.html',
  styleUrls: ['./add-equipment-attributes.component.scss']
})
export class AddEquipmentAttributesComponent implements OnInit {
  addAttributeForm: FormGroup
  SelectResponses:string[]=['Input Box', 'Yes / No', 'Radio Button']
  radiobutton=false
  yesorNo=false
  inputbox=false
    constructor(private fb: FormBuilder,  private addMenuFoodsService: AddMenuFoodsService) {
  
    }
  
    ngOnInit() {
      this.addActivity()
  
    }
  
    
    addActivity() {
      this.addAttributeForm = this.fb.group({
        attributeName: [''],
        selectResponse:[''],
        inputBox:[''],
        radioButton:[''],
        yesNo:[''],
        
      })
    }
  
  
    onClickResponse(response){
  console.log(response);
  if(response=="Input Box"){
    this.radiobutton=false
    this.yesorNo=false
    this.inputbox=true
    
  }
  if(response=="Yes / No"){
    this.radiobutton=false
    this.yesorNo=true
    this.inputbox=false
    
  }
  if(response=="Radio Button"){
    this.radiobutton=true
    this.yesorNo=false
    this.inputbox=false
   
  }
  
    }
  
    onAddAlbum() {
      console.log(this.addAttributeForm.value);
      
    }
  }
  