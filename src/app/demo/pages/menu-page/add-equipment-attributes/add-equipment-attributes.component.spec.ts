import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEquipmentAttributesComponent } from './add-equipment-attributes.component';

describe('AddEquipmentAttributesComponent', () => {
  let component: AddEquipmentAttributesComponent;
  let fixture: ComponentFixture<AddEquipmentAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEquipmentAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEquipmentAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
