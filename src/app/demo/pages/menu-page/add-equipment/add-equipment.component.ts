import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { RootCategoryAssetInventoryComponent } from '../asset-Inventory-category-dailogs/root-category-asset-inventory/root-category-asset-inventory.component';
import { SubCategoryAssetInventoryComponent } from '../asset-Inventory-category-dailogs/sub-category-asset-inventory/sub-category-asset-inventory.component';
import { SubSubCategoryAssetInventoryComponent } from '../asset-Inventory-category-dailogs/sub-sub-category-asset-inventory/sub-sub-category-asset-inventory.component';
import { SubSubCategoryTypeAssetInventoryComponent } from '../asset-Inventory-category-dailogs/sub-sub-category-type-asset-inventory/sub-sub-category-type-asset-inventory.component';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

const URL = environment.base_Url + 'equipment/uploadphotos'

@Component({
  selector: 'app-add-equipment',
  templateUrl: './add-equipment.component.html',
  styleUrls: ['./add-equipment.component.scss']
})
export class AddEquipmentComponent implements OnInit {
  addEquipmentForm: FormGroup
  units: string[] = ['Gram', "Kg"]
  loginUserID: any
  allAssetInventoryCategory: any
  allAttributesSet: any


  uploader: FileUploader;
  response: string
  multiResponse = []
  attributeData:any
  category= { };
  root: any;
  subcategory: any;
  subsubCategory: any;
  categoryType: any;
  attribute_setlist=[];
  set=[]
  filteredArr=[];
  attribute=[];
  inputBoxPlaceholder: any;
  radiobutton: boolean;
  yesorNo: boolean;
  inputbox: boolean;
  checkbox: boolean;
  radioOptions: any;
  checkBoxOptions: any;
// filtered=[]

  constructor(public dialogRef: MatDialogRef<AddEquipmentComponent>,
    private fb: FormBuilder, private dialog: MatDialog,
    private menuService: AddMenuFoodsService, private router: Router) {

    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);

    this.getAllAssetInventoryCategory()
    this.getAllAttributeSet()
    
  }

  ngOnInit() {
    this.addEquipmentForm = this.fb.group({
      equipmentName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      category: [{}, Validators.required],
      attribute_set:[],
      root: {},
      subcategory: {}, 
      subsubCategory: {},
      categoryType: {},
      image: [''],
      userid: this.loginUserID,
      attributeSet_response: [],


    })

    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'equipment'

    });
    //file upload response with url
    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);


      this.response = JSON.parse(response)['EquipmentUrls']
      console.log('response', this.response);

      // this.multiResponse.push(this.response)
      // console.log('multi response', this.multiResponse);


    };
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  //get all liquor attribute set
  getAllAttributeSet() {
    this.menuService.getAllLiquorAttributeSets().subscribe(data => {
      console.log("All attribute sets", data);
      this.allAttributesSet = data['data'].filter(ele => ele.attributeType == "equipment")
    })
  }

  //open tree node and close
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  getAllAssetInventoryCategory() {
    this.menuService.getAllAssetInventoryCategories().subscribe(data => {
      console.log("all asset inventory categories", data);
      this.allAssetInventoryCategory = data['data']
    })
  }

  // *********************** OPEN ROOT CATEGORY IN ASSET INVENTORY *************************8
  openInventoryRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB CATEGORY IN ASSET INVENTORY ********************** */
  openInventorySubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB SUB CATEGORY IN ASSET INVENTORY ********************** */
  openInventorySubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN ASSET INVENTORY ********************** */
  openInventorySubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }



  addequipment() {
    this.addEquipmentForm.patchValue({
      image: this.response,
      category:this.category
      
    })
   
    this.addEquipmentForm.patchValue({
      root: this.root,
      subcategory: this.subcategory, 
      subsubCategory: this.subsubCategory,
      categoryType: this.categoryType,
      attributeSet_response: this.filteredArr,

    })
    console.log(this.addEquipmentForm.value);
    this.menuService.addEquipmentAssetInventory(this.addEquipmentForm.value).subscribe(res => {
      console.log(res);

      if (res['success'] == true) {

        Swal.fire('Equipment Added Successfully', '', 'success')
        this.dialogRef.close();
      }
      else {
        Swal.fire('Failed to Add Equipment', 'Something went wrong', 'warning')
      }
    })
  }
 
  checkEquipmentExist(event){
    console.log("hi");
    
    this.menuService.checkLiquorEquipmentExist(event.target.value.toLowerCase()).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        Swal.fire("Item Name Already Exist..!","", "warning")
        event.target.value=""
        this.addEquipmentForm.patchValue({
          equipmentName:''
        })
      }

    })
  }
  getroot(r?:any,sub?:any,subsub?:any, type?:any){
    this.root={root:r['rootCategoryName'],id:r['_id']}
    if(sub!=undefined){
    this.subcategory={subcategory:sub['subCategoryName'],id:sub['_id']}
    }
    if(subsub!=undefined){
    this.subsubCategory={subsubCategory:subsub['subSubCategoryName'],id:subsub['_id']}
    }
    if(type!=undefined){
    this.categoryType={categoryType:type['subSubCategoryTypeName'],id:type['_id']}
    }
    console.log(r,">", sub,">", subsub,">", type);
    
  }
  getRadiovalue(id, name){
    // fa
   
    this.filteredArr=[]
   this.set=[]
   this.attribute_setlist=[]
    this.category={category:name, id:id};
    console.log(id,">>", this.category);

  let obj=
      {
        id: id,
    string:name,
  attributeType:"equipment"
 
  }
  console.log("hi");
  console.log(obj)
  // await new Promise(resolve => {

  
    this.menuService.getattributeDatafromotherCategories(obj).subscribe(data => {
  
    console.log(data);
    this.attributeData = data
   this.set= this.attributeData['attributeSets']
  //  this.attribute_setlist=this.attributeData['attributeSets']
  this.attributeData['attributeSets'].forEach(element => {
    
    this.attribute_setlist.push(element['attributeSetName'])
  });
  console.log("list>",this.attribute_setlist);
  this.addEquipmentForm.patchValue({
    attribute_set:this.attribute_setlist
  })
  console.log();
  this.attributeData['groupAttribute'].forEach(element => {
    console.log(element ['assignAttributes']);
    this.attribute=this.attribute.concat(element ['assignAttributes'])
    
  }); 
  console.log(this.attribute,">");
  this.filteredArr = this.attribute.reduce((acc, current) => {
    const x = acc.find(item => item['_id'] === current['_id']);
    if (!x) {
      return acc.concat([current]);
    } else {
      return acc;
    }
  }, []);
  console.log(this.filteredArr);
  // this.onClickResponse();
  
  })  
  
// });
setTimeout(()=> {
              

   if(this.set.length==0){      
    Swal.fire('Attribute not available', '', 'warning')
  } 
}, 1000);

  }
  onClickResponse(i) {
    
    // this.filteredArr.forEach((element, index) => {
      
  //  if(i==index-1){
    console.log(this.filteredArr[i].responseType.elementName);
    if (this.filteredArr[i].responseType.elementName == "Input Box") {
      this.inputBoxPlaceholder=this.filteredArr[i].responseType.fieldValue
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true
      this.checkbox = false
    }
    if (this.filteredArr[i].responseType.elementName == "Yes / No") {
     
      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filteredArr[i].responseType.elementName == "Radio Button") {
      this.radioOptions = this.filteredArr[i].responseType.options
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filteredArr[i].responseType.elementName == "Check Box") {
      this.checkBoxOptions = this.filteredArr[i].responseType.options
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = true

    }
    // }

  }
}
