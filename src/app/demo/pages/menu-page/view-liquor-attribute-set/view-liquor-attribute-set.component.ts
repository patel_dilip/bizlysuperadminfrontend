import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material';
import { ViewLiquorAttributeComponent } from '../view-liquor-attribute/view-liquor-attribute.component';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';

@Component({
  selector: 'app-view-liquor-attribute-set',
  templateUrl: './view-liquor-attribute-set.component.html',
  styleUrls: ['./view-liquor-attribute-set.component.scss']
})
export class ViewLiquorAttributeSetComponent implements OnInit {
assignedGroupsToSet=[]
  categoryType: any;
  DrinkTypes: any;
  DrinkTree=[];
  allRetailFoodCategories= [];
  retailFoodtree= [];
attributeKey:any
  allAssetInventoryCategory=[];
  Inventorytree=[];
  allRetailBeveragesCategories=[];
  Beveragestree=[];
  constructor(public dialogRef: MatDialogRef<ViewLiquorAttributeComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private menuService: AddMenuFoodsService) {
      console.log(data);
      console.log(data['assignGroups']);
      this.categoryType=data['categoryType']
      this.attributeKey=data['attributeType']
    this.getRootTypes();
    this.getAllBeveragesCategories()
    this.getAllAssetInventoryCategory()
    this.getAllRetailFoodCategories()
      data['assignGroups'].forEach(async element => {
        await  this.menuService.getSingleAttributeGroup(element._id).subscribe(data=>{
          console.log(data);
          this.assignedGroupsToSet.push(data['data'])
       
        })
        console.log(this.assignedGroupsToSet);
      });
     
     }

  ngOnInit() {
 
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  getRootTypes() {

    this.menuService.getAllRootDrinks().subscribe(data => {
      this.DrinkTypes = data['data']

      console.log('drinktypes', this.DrinkTypes);
      this.DrinkTypes.forEach(element => {
        this.categoryType.forEach(ele=>{
        if(element['_id']==ele['_id'])
        element=ele
        
        })
         this.DrinkTree.push(element)
      });
this.DrinkTypes=this.DrinkTree
console.log(this.categoryType);

    })
  }
  getAllAssetInventoryCategory() {
    console.log(this.categoryType);
    
    this.menuService.getAllAssetInventoryCategories().subscribe(data => {
      console.log("all asset inventory categories", data);
      this.allAssetInventoryCategory = data['data']
      console.log('drinktypes', this.DrinkTypes);
      this.allAssetInventoryCategory.forEach(element => {
        this.categoryType.forEach(ele=>{
        if(element['_id']==ele['_id'])
        element=ele
        
        })
         this.Inventorytree.push(element)
      });
this.allAssetInventoryCategory=this.Inventorytree
console.log(this.categoryType);
      
    })
    // this.loadSuitofAssetInventory()
  }
  getAllRetailFoodCategories() {
    this.menuService.getAllRetailFoodCategories().subscribe(data => {
      console.log("All Retail Food categories", data);
      this.allRetailFoodCategories = data['data']
      this.allRetailFoodCategories.forEach(element => {
        this.categoryType.forEach(ele=>{
        if(element['_id']==ele['_id'])
        element=ele
        
        })
         this.retailFoodtree.push(element)
      });
  this.allRetailFoodCategories=this.retailFoodtree
  console.log("asset inventory:", this.allRetailFoodCategories);
  
    })
  }
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }
  
  getAllBeveragesCategories() {
    this.menuService.getAllBeverageCategories().subscribe(data => {
      console.log("All Beverages Categories", data);
      if (data['sucess'] == false) {
        //console.log("Beverages categories data not found");
      } else {
                      this.allRetailBeveragesCategories = data['data'].filter(element => element.beverageType == "retailBeverage")
                     this.Beveragestree=[];
                      this.allRetailBeveragesCategories.forEach(element => {
                        this.categoryType.forEach(ele=>{
                          if(element['_id']==ele['_id'])
                          element=ele
                          })
                           this.Beveragestree.push(element)
                      });
                 this.allRetailBeveragesCategories=this.Beveragestree
      }
    })
  
  }
}
