import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLiquorAttributeSetComponent } from './view-liquor-attribute-set.component';

describe('ViewLiquorAttributeSetComponent', () => {
  let component: ViewLiquorAttributeSetComponent;
  let fixture: ComponentFixture<ViewLiquorAttributeSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLiquorAttributeSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLiquorAttributeSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
