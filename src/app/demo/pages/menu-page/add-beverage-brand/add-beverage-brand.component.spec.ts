import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBeverageBrandComponent } from './add-beverage-brand.component';

describe('AddBeverageBrandComponent', () => {
  let component: AddBeverageBrandComponent;
  let fixture: ComponentFixture<AddBeverageBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBeverageBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBeverageBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
