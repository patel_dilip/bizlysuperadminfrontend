import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';
import { MatDialogRef } from '@angular/material';

const URL = environment.base_Url + 'beveragebrand/uploadphoto'

@Component({
  selector: 'app-add-beverage-brand',
  templateUrl: './add-beverage-brand.component.html',
  styleUrls: ['./add-beverage-brand.component.scss']
})
export class AddBeverageBrandComponent implements OnInit {
  addBrandForm: FormGroup
  countries: any
  uploader: FileUploader
  response: string;
  loginUserID:any
  constructor(private fb: FormBuilder,public dialogRef: MatDialogRef<AddBeverageBrandComponent>,
    private mainService: MainService, private menuService: AddMenuFoodsService ) {

       //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);

    this.getAllCountries()
    this.addBrandFormFunction()
  }

  ngOnInit() {


    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'beveragebrand'

    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.addBrandForm.patchValue({
        brandLogo: response
      })
      
    }
  }

  addBrandFormFunction(){
    this.addBrandForm = this.fb.group({
      brandName: ['', Validators.required],
      country: ['', Validators.required],
      brandLogo: [''],
      userid:  this.loginUserID
    })
  }

  // getting all countries name
  getAllCountries() {
    this.mainService.getAllDialCode().subscribe(countryCode => {
      console.log('countries', countryCode);

      // Array object for countries
      this.countries = countryCode;

    })
  }

  //add brand 
  addBrand() {
    console.log("add Brand", this.addBrandForm.value);
    this.menuService.addBeverageBrand(this.addBrandForm.value).subscribe(res => {
      console.log(res);
      if (res['success'] == true) {

        Swal.fire('Brand Added Successfully', '', 'success')
        this.dialogRef.close();
      }
      else {
        Swal.fire('Failed to Add Brand', 'Something went wrong', 'warning')
      }
    })
  }
}
