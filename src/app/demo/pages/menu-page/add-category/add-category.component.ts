
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialog } from '@angular/material';
import { AddCuisinesComponent } from 'src/app/PopoversList/add-cuisines/add-cuisines.component';
import { AddTagComponent } from 'src/app/PopoversList/add-tag/add-tag.component';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { element } from 'protractor';


const URL = environment.base_Url + 'category/uploadphotos'

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

// clicked=false
  addCategoryForm: FormGroup
  cuisineCtrl: FormControl
  selectedAlbums = []
  selectedTags = []
  cuisines = []
  tempCuisines = []
  tags = []
  tempTags = []
  selectedCuisinesIds = [];
  selectedTagsIds = [];
  uploader: FileUploader
  response: string;
  multiResponse = [];
  userid
  loading = false;
  cusines_data=[];

  // **********CONSTRUCTOR*************************

  constructor(private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService,
    private matDialog: MatDialog,
    private router: Router) {

    //  get cusines FUNCITON CALL

    this.getCuisines()
    // get tags api FUNCITON CALL
    this.getTags()

    // FETCH USER ID FROM LOCAL STORAGE

    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];

  }

  ngOnInit() {

    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'categories'

    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);



      this.response = JSON.parse(response)
      console.log('response', this.response);

      this.multiResponse.push(this.response['cuisinePhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      this.loading = false
    }


    this.addCategory()

  }
  // SETLOADING CALL FOR LOADER
  setLoading() {
    this.loading = true
  }





  // get CUISINES API FUNCTION DEFINED
  getCuisines() {
    this.cusines_data=[]
    this.addMenuFoodsService.getCuisines().subscribe(data => {
      this.cuisines = data['data']
      this.tempCuisines = data['data']
      console.log(data['data']);
      console.log(this.cuisines);
      this.cuisines.forEach(element => {
        if(element.status==true){
      this.cusines_data.push({cuisineName:element['cuisineName'],_id:element['_id']})
        }
      });
      console.log(this.cusines_data);
      this.cuisines=this.cusines_data
      


    })
  }

  // get TAGS API FUNCTION DEFINED
  getTags() {
    this.addMenuFoodsService.getTags().subscribe(data => {
      this.tags = data['data']
      this.tempTags = data['data']
      console.log('tags', this.tags);
    })
  }
  applyFilterCuisine(event) {
    this.cuisines = []
    this.cuisines = this.cusines_data
   // this.cuisines = this.tempCuisines['cuisineName']
    console.log(event.target.value);
    if (event.target.value == "") {
      this.cuisines = this.cusines_data
    } else {
      const val = event.target.value.trim().toLowerCase();
    const filter = this.cusines_data.filter(function (d) {
      //console.log(d);
      let result
      console.log("ele", d);        
      result = d['cuisineName'].toLowerCase().indexOf(val) !== -1 
  
      return result
    });

    //console.log(filter);
    this.cuisines = filter;
    }
     
   console.log(">>>", this.cuisines);
   
    
  }

  applyFilterTags(event) {
    this.tags = this.tempTags
    console.log(event.target.value);
    
    if (event.target.value == "") {
      this.tags = this.tempTags
    } else {
      const val = event.target.value.trim().toLowerCase();
      const filter = this.tags.filter(function (d) {
        //console.log(d);
        let result
        console.log("ele", d);        
        result = d['tagName'].toLowerCase().indexOf(val) !== -1 
    
        return result
      });
  
      //console.log(filter);
      this.tags = filter;
      }
       
     console.log(">>>", this.tags);
         }
  


  // ******Open dialog box for add new cuines****

  openAddCuisinesDialog(): void {

    console.log('opened Add cuisine dialog');

    const dialogRef = this.matDialog.open(AddCuisinesComponent, {
      width:'60%',
      height:'80%',
      disableClose: true
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // GET CUISINES FUNCITON CALL to get fresh data after addition
      this.getCuisines()
    });

  }
  

  //  *************open dialog add tags***************
  // AddTagComponent
  openAddTagDialog(): void {

    console.log('opened Add cuisine dialog');

    const dialogRef = this.matDialog.open(AddTagComponent, {
      width:'60%',
      height:'80%',
      disableClose: true
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      // GET TAGS FUNCITON CALL to get fresh data after addition
      setTimeout(() => {
       this.getTags()
    }, 4000);

    });
    
  }

  //add cuisines form 
  addCategory() {
    this.addCategoryForm = this.fb.group({
      categoryName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z0-9]+$')]],
      cuisines: [''],
      tags: [''],
      categoryImages: []
    })
  }


  // after selection of cuisines

  onCheckCuisine(s) {
    if (this.cuisines.includes(s)) {
      this.cuisines.splice(this.cuisines.indexOf(s), 1)
      for( var i = 0; i < this.tempCuisines.length; i++){ 
        if ( this.tempCuisines[i]._id === s._id) {
          this.tempCuisines.splice(i, 1); 
        }
      }
      if (this.selectedAlbums.includes(s.cuisine)) {
        alert("Already exists in selection")
      } else {
        this.selectedAlbums.push(s)
        this.selectedCuisinesIds.push(s._id)
      }
    }
    console.log(this.selectedAlbums);
    console.log(this.selectedCuisinesIds)

  }


  // ON REMOVE PUB MORE INFOS
  removePubs(s) {
    // full object of cuisines
    if (this.selectedAlbums.includes(s)) {
      this.selectedAlbums.splice(this.selectedAlbums.indexOf(s), 1)
      this.cuisines.push(s);
     
      console.log('selected Cuisines', this.selectedAlbums)
    }

    // only single key of cuisne as id
    if (this.selectedCuisinesIds.includes(s._id)) {
      this.selectedCuisinesIds.splice(this.selectedCuisinesIds.indexOf(s._id), 1)
      console.log('selected Cuisines', this.selectedCuisinesIds);

    }

  }

  // check tags push 
  onCheckTags(s) {
    console.log(s.tagName);

    if (this.tags.includes(s)) {
      this.tags.splice(this.tags.indexOf(s), 1)
    
      for( var i = 0; i < this.tempTags.length; i++){ 
        if ( this.tempTags[i]._id === s._id) {
          this.tempTags.splice(i, 1); 
        }
      }
      if (this.selectedTags.includes(s)) {
        alert("Already exists in selection")
      } else {
        this.selectedTags.push(s)
        this.selectedTagsIds.push(s._id)


      }
    }
    console.log(this.selectedTags);
    console.log('selected tags id', this.selectedTagsIds);
  }


  // ON REMOVE PUB MORE INFOS
  removeTags(s) {
    if (this.selectedTags.includes(s)) {
      this.selectedTags.splice(this.selectedTags.indexOf(s), 1)
      this.tags.push(s);
     
    }

    if (this.selectedTagsIds.includes(s._id)) {
      this.selectedTagsIds.splice(this.selectedTagsIds.indexOf(s._id), 1)
      console.log('this seleected tags id', this.selectedTagsIds);

    }

  }





  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }

  
  urls = [];




  // **************FINAL POST ADD METHOD CATEGORY********************
  onAddCategory() {

    // ************addCategory Object**************

    let addCategoryObject = {
      "categoryName": this.addCategoryForm.controls['categoryName'].value,
      "cuisines": this.selectedCuisinesIds,
      "tags": this.selectedTagsIds,
      "images": this.multiResponse,
      "userid": this.userid
    }

    console.log('addCategoryObject', addCategoryObject);


    // POST ADD CATEGORY API 

    this.addMenuFoodsService.postCategory(addCategoryObject).subscribe(addCategoryResponse => {
      console.log('add category response', addCategoryResponse);

      if (addCategoryResponse['sucess']) {
        this.loading = false
        // this.clicked=false;
        Swal.fire('Category Added Successfully', '', 'success')
        this.router.navigate(['/menu'])
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }
    })

  }

  checkCategoryExist(event){
    this.addMenuFoodsService.checkCategoryExist(event.target.value.toLowerCase()).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        Swal.fire("Category Name Already Exist..!","", "warning")
        event.target.value=""
        this.addCategoryForm.patchValue({
          categoryName:''
        })
      }

    })
  }


}
