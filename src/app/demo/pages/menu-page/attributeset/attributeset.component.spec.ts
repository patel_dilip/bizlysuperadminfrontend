import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributesetComponent } from './attributeset.component';

describe('AttributesetComponent', () => {
  let component: AttributesetComponent;
  let fixture: ComponentFixture<AttributesetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttributesetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributesetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
