
  import { Component, OnInit, Inject } from '@angular/core';
  import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-beverage',
  templateUrl: './view-beverage.component.html',
  styleUrls: ['./view-beverage.component.scss']
})
export class ViewBeverageComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ViewBeverageComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
    console.log(data);

  }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}