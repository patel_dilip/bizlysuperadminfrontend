import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewItemsFoodInventoryComponent } from './view-items-food-inventory.component';

describe('ViewItemsFoodInventoryComponent', () => {
  let component: ViewItemsFoodInventoryComponent;
  let fixture: ComponentFixture<ViewItemsFoodInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewItemsFoodInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewItemsFoodInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
