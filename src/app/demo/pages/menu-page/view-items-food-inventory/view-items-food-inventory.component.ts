import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-view-items-food-inventory',
  templateUrl: './view-items-food-inventory.component.html',
  styleUrls: ['./view-items-food-inventory.component.scss']
})
export class ViewItemsFoodInventoryComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<ViewItemsFoodInventoryComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { 
      console.log(data);
      let i=0
    data['quantity'].forEach(element => {
      if(element.gram=='' && element.kg==''){
        data['quantity'].splice(i,1);
      }
      i++
    });
  
    }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
