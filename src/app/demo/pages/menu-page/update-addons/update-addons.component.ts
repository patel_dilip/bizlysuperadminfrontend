import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { AddDishPopoverComponent } from 'src/app/PopoversList/add-dish-popover/add-dish-popover.component';


const URL = environment.base_Url + 'adon/uploadphotos'

@Component({
  selector: 'app-update-addons',
  templateUrl: './update-addons.component.html',
  styleUrls: ['./update-addons.component.scss']
})
export class UpdateAddonsComponent implements OnInit {

  dishUnits: string[] = ['Kilogram', 'Grams', 'Unit'];
  addAddOnForm: FormGroup
  cuisineCtrl: FormControl

  selecteddishes = []

  cuisines = []
  dishes = []
  tempDishes = []
  userid
  uploader: FileUploader;
  response: string
  multiResponse = []
  loading = false;
  addonObject: any
  addOnID: any
  add: any;
  // ***************CONSTRUCTOR********************
  constructor(private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService,
    private menuService: AddMenuFoodsService, private router: Router, private matDialog: MatDialog) {
    //function call
    this.getAllDishes()

    //get the cusine object for update
    this.addonObject = this.menuService.editObject
    console.log("edit obj", this.addonObject);
    if (this.addonObject == undefined) {
      this.router.navigateByUrl("/menu")
    }

    
    // FETCH USER ID FROM LOCAL STORAGE

    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];
    this.addAddOn()
    this.addAddOnForm.patchValue({
      adonName: this.addonObject.adonName,
      unit: this.addonObject.unit,
      veg_nonveg: this.addonObject.veg_nonveg,
      adonType:this.addonObject.adonType
    })

    this.selecteddishes = this.addonObject.dishes
    this.multiResponse = this.addonObject.images
    this.addOnID = this.addonObject._id
  }

  ngOnInit() {



    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'adons'
    });
    //file upload response with url
    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.response = JSON.parse(response)
      console.log('response', this.response['adonPhotoUrls']);

      this.multiResponse.push(this.response['adonPhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      this.loading = false
    };

  }

  //on image upload loader start
  setLoading() {
    this.loading = true
  }


  //getAll dishes 
  getAllDishes() {
    this.menuService.getAllDishes().subscribe(data => {
      console.log(data);
      this.dishes = data['data']
this.tempDishes=[]
      for (var i = 0; i < this.dishes.length; i++) {
        for (var j = 0; j < this.addonObject.dishes.length; j++) {
          if (this.dishes[i]._id === this.addonObject.dishes[j]._id ) {
            this.dishes.splice(i, 1);
            //  this.tempDishes = this.dishes
          }

        }
              }
              this.dishes.forEach(element => {
                if(element.status==true){
              this.tempDishes.push(element)
                }

              });
              this.dishes=this.tempDishes
    //  this.tempDishes = data['data']
    })
  }

  //add addon form 
  addAddOn() {
    this.addAddOnForm = this.fb.group({
      adonName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      unit: ['',Validators.required],
      veg_nonveg: ['', Validators.required],
      dishes: [''],
      images: [],
      userid: [''],
      updatedBy: [''],
      adonType:this.fb.array([])
    })
   console.log(this.addonObject.adonType.length)
   for(let i=0; i< this.addonObject.adonType.length; i++){
     this.addAddonType()
   }
    // this.addAddOnForm.patchValue({adonType:this.add});
    
  }

  RemoveAddonType(i){
    console.log(i);
    this.add.removeAt(i);
    console.log(this.add.value);
    
    
  }
addAddonType(){
  this.add = this.addAddOnForm.get('adonType') as FormArray;
    this.add.push(this.fb.group({
      adonName_type: [''],
     type: ['']
    }))
    console.log(this.add.value);
    
}

  applyFilterDishes(event) {
    this.dishes = this.tempDishes
    console.log(event.target.value);
    if (event.target.value == "") {
      this.dishes = this.tempDishes
    } 
    else {
      const val = event.target.value.trim().toLowerCase();
    const filter = this.dishes.filter(function (d) {
    //console.log(d);
      let result
      console.log("ele", d);        
    result = d['dishName'].toLowerCase().indexOf(val) !== -1 
  
      return result
    });

    //console.log(filter);
    this.dishes = filter;
    
     }
  }


  onCheckdishes(s) {
    console.log(s);

    if (this.dishes.includes(s)) {
      this.dishes.splice(this.dishes.indexOf(s), 1)
     
      for( var i = 0; i < this.tempDishes.length; i++){ 
        if ( this.tempDishes[i]._id === s._id) {
          this.tempDishes.splice(i, 1); 
        }
      }
      if (this.selecteddishes.includes(s.dishName)) {
        alert("Already exists in selection")
      } else {
        this.selecteddishes.push(s)
      }
    }
    console.log(this.selecteddishes);

  }


  // ON REMOVE PUB MORE INFOS
  removedishes(s) {
    if (this.selecteddishes.includes(s)) {
      this.selecteddishes.splice(this.selecteddishes.indexOf(s), 1)
      this.dishes.push(s);
  
    }

  }
  //add dish pop over
  openAddDishesDialog(): void {
    const dialogRef = this.matDialog.open(AddDishPopoverComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.getAllDishes()
    });


  }


  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }

  //final add adon post
  onAddon() {
    this.loading = true
    let dishesIDS = []
    this.selecteddishes.forEach(element => {
      dishesIDS.push(element._id)
    });

    //patch all value to final object
    this.addAddOnForm.patchValue({
      dishes: dishesIDS,
      images: this.multiResponse,
      userid: this.userid,
      updatedBy: this.userid,
       adonType:this.addonObject.adonType
    })
    console.log(this.addAddOnForm.value);

    this.menuService.updateAddon(this.addOnID,this.addAddOnForm.value).subscribe(res => {
      this.loading = false
      console.log(res);

      if (res['sucess'] == true) {

        Swal.fire('Add On Updated Successfully', '', 'success')
        this.router.navigate(['/menu'])
      }
      else {
        Swal.fire('Failed to Update Add On', 'Something went wrong', 'warning')
      }
    })

  }


  getControls() {
    return (this.addAddOnForm.get('adonType') as FormArray).controls;
  }

}
