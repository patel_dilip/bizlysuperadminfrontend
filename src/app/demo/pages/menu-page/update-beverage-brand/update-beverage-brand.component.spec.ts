import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBeverageBrandComponent } from './update-beverage-brand.component';

describe('UpdateBeverageBrandComponent', () => {
  let component: UpdateBeverageBrandComponent;
  let fixture: ComponentFixture<UpdateBeverageBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBeverageBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBeverageBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
