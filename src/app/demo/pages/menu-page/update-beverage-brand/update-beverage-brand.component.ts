import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

const URL = environment.base_Url + 'beveragebrand/uploadphoto'

@Component({
  selector: 'app-update-beverage-brand',
  templateUrl: './update-beverage-brand.component.html',
  styleUrls: ['./update-beverage-brand.component.scss']
})
export class UpdateBeverageBrandComponent implements OnInit {

  addBrandForm: FormGroup
  countries: any
  uploader: FileUploader
  response: string;
  loginUserID: any
  brandObject: any
  brandImage: any
  brandID: any
  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<UpdateBeverageBrandComponent>,
    private mainService: MainService, private menuService: AddMenuFoodsService, 
    @Inject(MAT_DIALOG_DATA) public data) {

      console.log(data);
      

    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);

    this.getAllCountries()
    this.addBrandFormFunction()


    this.addBrandForm.patchValue({
      brandName: data.brandName,
      country: data.country,
      brandLogo: data.brandLogo
    })
    this.brandImage = data.brandLogo
    this.brandID = data._id
    
  }

  ngOnInit() {


    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'beveragebrand'

    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.addBrandForm.patchValue({
        brandLogo: response
      })
      this.brandImage = response
    }
  }

  addBrandFormFunction() {
    this.addBrandForm = this.fb.group({
      brandName: ['', Validators.required],
      country: ['', Validators.required],
      brandLogo: [''],
      userid: this.loginUserID
    })
  }

  // getting all countries name
  getAllCountries() {
    this.mainService.getAllDialCode().subscribe(countryCode => {
      console.log('countries', countryCode);

      // Array object for countries
      this.countries = countryCode;

    })
  }

  //add brand 
  addBrand() {
    console.log("add Brand", this.addBrandForm.value);
    this.menuService.updateBeverageBrand(this.brandID,this.addBrandForm.value).subscribe(res => {
      console.log(res);
      if (res['success'] == true) {

        Swal.fire('Brand Updated Successfully', '', 'success')
        this.dialogRef.close();
      }
      else {
        Swal.fire('Failed to Update Brand', 'Something went wrong', 'warning')
      }
    })
  }
}
