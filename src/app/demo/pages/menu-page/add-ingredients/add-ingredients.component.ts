import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { RootCategoryFoodInventoryComponent } from '../food-inventory-category-dailogs/root-category-food-inventory/root-category-food-inventory.component';
import { SubCategoryFoodInventoryComponent } from '../food-inventory-category-dailogs/sub-category-food-inventory/sub-category-food-inventory.component';
import { SubSubCategoryFoodInventoryComponent } from '../food-inventory-category-dailogs/sub-sub-category-food-inventory/sub-sub-category-food-inventory.component';
import { SubSubCategoryTypeFoodInventoryComponent } from '../food-inventory-category-dailogs/sub-sub-category-type-food-inventory/sub-sub-category-type-food-inventory.component';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { RootCategoryBeverageComponent } from '../beverages-category-dailogs/root-category-beverage/root-category-beverage.component';

@Component({
  selector: 'app-add-ingredients',
  templateUrl: './add-ingredients.component.html',
  styleUrls: ['./add-ingredients.component.scss']
})
export class AddIngredientsComponent implements OnInit {
addingredientForm : FormGroup
units:string[]=['Liter',"Kg"]
allFoodInventoryCategory:any
loginUserID:any

uploader: FileUploader;
  response: string
  multiResponse = []
  root: any;
  sub: any;
  subsub: any;
  type: any;
  category: any;

  
  constructor(public dialogRef: MatDialogRef<AddIngredientsComponent>,
    private fb:FormBuilder, private menuService: AddMenuFoodsService, 
    private dialog:MatDialog, private router: Router) {
      
      this.getAllFoodInventoryCategory()

       //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);
    
     }

  ngOnInit() {
    this.addingredientForm=this.fb.group({
      ingredientName:['',[Validators.required,Validators.pattern('^[ a-zA-Z]+$') ]],
      ingredientcategory:{},
      unit:['',[Validators.required]],
      userid: this.loginUserID,
      root:'',
      sub:'',
      subsub:'',
      type:''
    })
  } 
  onNoClick(): void {
    this.dialogRef.close();
  }

  getAllFoodInventoryCategory() {
    this.menuService.getAllFoodInventoryCategories().subscribe(data => {
      console.log("all food inventory categories", data);
      this.allFoodInventoryCategory = data['data']
    })
  }


  //*************************OPEN ROOT CATEGORY IN FOOD INVENTORY ********************** */
  openFoodInventoryRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryFoodInventoryComponent, {
      width: '460px',
      disableClose: true,

      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }
  //*************************OPEN SUB CATEGORY IN FOOD INVENTORY ********************** */
  openFoodInventorySubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryFoodInventoryComponent, {
      width: '460px',
      data: rootCategoryid,
      disableClose: true,
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }

  //*************************OPEN SUB SUB CATEGORY IN FOOD INVENTORY ********************** */
  openFoodInventorySubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryFoodInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN FOOD INVENTORY ********************** */
  openFoodInventorySubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeFoodInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }


   //open tree node and close
   caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }


  addingrdient(){
    this.addingredientForm.patchValue({
      sub:this.sub,
      subsub:this.subsub,
      root:this.root,
      type:this.type,
      ingredientcategory:this.category

    })
    // debugger;
  
    console.log(this.addingredientForm.value);
    this.menuService.addIngredientFoodInventory(this.addingredientForm.value).subscribe(res=>{
      console.log(res);
       if (res['sucess'] == true) {

        Swal.fire('Material Added Successfully', '', 'success')
        this.router.navigate(['/inventory'])
      }
      else {
        Swal.fire('Failed to Add Material', 'Something went wrong', 'warning')
      }
      
    })
  }
  checkIngrdientExist(event){
    console.log("hi");
    
    this.menuService.checkInventoryIngredientExist(event.target.value.toLowerCase()).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        Swal.fire("Ingredient Name Already Exist..!","", "warning")
        event.target.value=""
        this.addingredientForm.patchValue({
          ingredientName:''
        })
      }

    })
  }
  getRadiovalue(r?:any,sub?:any,subsub?:any, type?:any){
    console.log(r,"<", sub,"<", subsub ,"<",type);
    
    this.root={root:r['rootCategoryName'],id:r['_id']}
    if(sub!=undefined){
    this.sub={sub:sub['subCategoryName'],id:sub['_id']}
    }
    if(subsub!=undefined){
    this.subsub={subsub:subsub['subSubCategoryName'],id:subsub['_id']}
    }
    if(type!=undefined){
    this.type={type:type['subSubCategoryTypeName'],id:type['_id']}
    }
    console.log(r,">", sub,">", subsub,">", type);
    
  }
  getradio(id, category){
    
console.log(id, "<", category);
this.category={id:id,category:category}
  
  }
}
