import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';

@Component({
  selector: 'app-vu-chain',
  templateUrl: './vu-chain.component.html',
  styleUrls: ['./vu-chain.component.scss']
})
export class VuChainComponent implements OnInit {
data: any
 
  constructor(
    private router: Router,
    private restServ: RestoService
    ) 
    {
      this.data = this.restServ.view_update
      console.log(this.data)
      if(this.data == undefined){
        this.router.navigate(['/restaurants/chain-home'])
      }
     }

  ngOnInit() {
  }


  editCinema(type){
    this.restServ.chainEdit = {type: type, data: this.data}
   this.router.navigate(['/restaurants/update-chain'])
   
  }


}
