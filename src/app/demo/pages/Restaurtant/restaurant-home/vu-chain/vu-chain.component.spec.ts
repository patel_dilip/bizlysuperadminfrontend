import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VuChainComponent } from './vu-chain.component';

describe('VuChainComponent', () => {
  let component: VuChainComponent;
  let fixture: ComponentFixture<VuChainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VuChainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VuChainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
