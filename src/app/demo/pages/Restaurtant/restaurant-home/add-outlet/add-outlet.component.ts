import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestaurantCellRenderComponent } from '../restaurant-cell-render/restaurant-cell-render.component'
import Swal from 'sweetalert2';


@Component({
  selector: 'app-add-outlet',
  templateUrl: './add-outlet.component.html',
  styleUrls: ['./add-outlet.component.scss']
})
export class AddOutletComponent implements OnInit {

  orgId: any
  orgObject: any = []
  outletsArr: any = []
  orgSavedata: any = []

  outletDelete: FormGroup
  // column data name
  private gridApi
  private gridColumnApi


  // column data name

  columnDefs = [
    { headerName: 'Outlet Name', field: 'restoInfo.restoName', sortable: true, filter: true, sortingOrder: ["asc", "desc"], "cellRendererFramework": RestaurantCellRenderComponent },
    { headerName: 'Locality', field: 'restoInfo.restoLocality', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Contact', field: 'restoInfo.restoContactNo', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Pincode', field: 'restoInfo.restoPincode', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Infrastructure Type', field: 'outletInfo.infrastructure_type', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Price for Two', field: 'restoInfo.restoPriceForTwo', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Restaurants Type', field: 'restoInfo.restoType', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Address', field: 'restoInfo.restoAddress', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Website', field: 'restoInfo.restoWebsite', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Account Number', field: 'legalInfo.accountNo', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'IFSC Code', field: 'legalInfo.IFSCCode', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Account Type', field: 'legalInfo.accountType', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    // { headerName: 'GST Number', field: 'legalInfo.restoWebsite', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    // { headerName: 'FSSAI Number', field: 'legalInfo.restoWebsite', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    // { headerName: 'PAN Number', field: 'legalInfo.restoWebsite', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },


  ];
  rowData: any = [];
  isEdit: boolean = false;
  organizationForm: FormGroup
  userid: any;

  // ngPrime Table
  cols: any = [];
  outletTable: any = [];
  _selectedColumns: any[];
  selected1: any;


  constructor(private router: Router, private restServ: RestoService, private fb: FormBuilder) {
    this.orgSavedata = this.restServ.OrganizationObj

    if (this.orgSavedata == undefined) {
      this.router.navigate(['/restaurants'])
    }
    else {
      console.log('organization data', this.orgSavedata);
      this.orgId = this.orgSavedata.outlets_arr
      console.log("orgId", this.orgId);

      this.rowData = this.orgId
      this.tableData()
    }


    if (this.orgId.length !== 0) {
      console.log("Outlets Resides under oranization are", this.orgId);
    }
    else {
      console.log('No outlets found')
    }




    let user = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = user._id

    console.log("USERID :--", this.userid);


    this.organizationForm = this.fb.group({
      organizationCode: [''],
      organization_name: ['', Validators.required],
      representative_name: ['', Validators.required],
      org_email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      org_contact: ['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      outlets_arr: [[]],
      status: true,
      userid: ['']
    })


  }
  ngOnInit() {

    this.outletDelete = this.fb.group({
      outletid: ['']
    })

  }


  // NG PRIME DATATABLE
  tableData() {

   
    let outletTable = []

    this.orgId.forEach(element => {
      outletTable.push(
        {
          id: element._id,
          outletcode: element.outletInfo.outlet_code,
          name: element.restoInfo.restoName,
          contact: element.restoInfo.restoCountryCode+' - '+element.restoInfo.restoContactNo,
          locality: element.restoInfo.restoLocality,
          pincode: element.restoInfo.restoPincode
        }
      )
    });

    this.outletTable = outletTable

    this.cols = [
      { field: "outletcode", header: "Outlet Code" },
      { field: "name", header: "Outlet Name" },
      { field: "contact", header: "Contact" },
      { field: "locality", header: "Locality" },
      { field: "pincode", header: "Pincode" }     
    ]


    this._selectedColumns = this.cols
  }

   // functions ng prime

   onRestoView(id){
    this.orgId.forEach(element => {
      if(element._id == id){
        this.restServ.view_restaurant = element
        this.restServ.current_restoid = element._id
      }
    })
    this.router.navigate(['/restaurants/view-restaurant'])
  }

    // ON CHECKBOX CLICK
    onChk() {
      console.log("selected1", this.selected1);
    }
  
    // show hide columns
    @Input() get selectedColumns(): any[] {
      return this._selectedColumns;
    }
  
    set selectedColumns(val: any[]) {
      //restore original order
      this._selectedColumns = this.cols.filter((col) => val.includes(col));
  
      console.log("Set value of selected columns : ", this._selectedColumns);
    }

  // EMAIL VALIDATION KEY UP FUNCTION
  onEmailKeyup() {
    let abc = this.organizationForm.controls['org_email'].value
    this.organizationForm.patchValue({
      org_email: abc.trim().toLowerCase().replace(/\s/g, "")

    })


    console.log('Organization email', this.organizationForm.controls['org_email'].value);
  }

  deleteOutlet() {
    let outletid = this.outletDelete.controls.outletid.value
    this.restServ.deleteRestobyId(outletid).subscribe(res => {
      console.log('res', res);
      if (res['sucess']) {
        this.router.navigate(['/restaurants'])
      }

    })
  }

  deleteAllOutlet() {
    this.restServ.deleteAllResto().subscribe(res => {
      console.log("response delete all", res);
      if (res['success']) {
        console.log("All deleted");
        Swal.fire('All Outlets Deleted', '', 'success')
      }


    })
  }

  onAddOulet() {

    localStorage.removeItem('ambiences')
    localStorage.removeItem('services')
    localStorage.removeItem('moreinfo')

    this.router.navigate(['/restaurants/add-restaurant'])
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }


  onEdit() {
    this.isEdit = !this.isEdit

    this.organizationForm.patchValue({
      organizationCode: this.orgSavedata.organizationCode,
      organization_name: this.orgSavedata.organization_name,
      representative_name: this.orgSavedata.representative_name,
      org_email: this.orgSavedata.org_email,
      org_contact: this.orgSavedata.org_contact,
      outlets_arr: this.orgSavedata.outlets_arr,
      status: this.orgSavedata.status,
      userid: this.orgSavedata.userid
    })

    console.log('before update', this.organizationForm.value);

  }

  onUpdate() {
    this.isEdit = !this.isEdit

    console.log('after update', this.organizationForm.value);


    if ('_id' in this.orgSavedata) {
      this.restServ.updateOrg(this.orgSavedata._id, this.organizationForm.value).subscribe(res => {
        console.log('response', res);

      })
    } else {
      this.restServ.OrganizationObj = this.organizationForm.value
    }
    this.orgSavedata = this.organizationForm.value
  }

}








