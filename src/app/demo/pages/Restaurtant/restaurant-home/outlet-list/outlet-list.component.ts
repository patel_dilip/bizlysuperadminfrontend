import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { resolve } from 'url';


interface person {
  name;
  contact;
  city;
}

@Component({
  selector: 'app-outlet-list',
  templateUrl: './outlet-list.component.html',
  styleUrls: ['./outlet-list.component.scss']
})
export class OutletListComponent implements OnInit {

  organizationForm: FormGroup

  AllOrg: any = [];
  options: any = []
  filteredOptions: Observable<string[]>;
  isReadOnly: boolean = false
  existOrg: any;
  loopdata: any = []

  // dtResponsiveOptions: any = {};


  // ngPrime Table
  cols: any = [];
  outletTable: any = [];
  selected1: person;
  // jobs: any = [];
  // cities: any = [];
  // name: any;
  // contact: any;
  // city: any;
  _selectedColumns: any[];
  

// organizationCode
 

  rowData = [];
  onlyRestoInfo: any = [];
  onlyEstablish: any = [];
  onlyLegal: any = [];
  onlyMedia: any = [];
  onlyMenu: any = [];
  organization: any = [];
  orgcode: string;
  userid: any;
  tData: any;


  // Is Add Organization
  isAddOrg: boolean = false
  orgId: any;
  constructor(private fb: FormBuilder, private restServ: RestoService, private router: Router) {

    let user = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = user._id

    console.log("USERID :--", this.userid);
    

    this.organizationForm = this.fb.group({
      organizationCode: [''],
      organization_name: ['', Validators.required],
      representative_name: ['', Validators.required],
      org_email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      org_contact:['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      outlets_arr: [[]],
      status: true,
      userid: ['']
    }) 

    // Get All Organizations
    this.restServ.getAllOrg().subscribe(data => {
      console.log('get all organization', data['data']);
      this.tData = data
      this.AllOrg = data['data']

      console.log("tadata", this.tData);
      
      // **********creating code serially

    if(this.AllOrg.length == 0){
      let len = this.AllOrg.length + 1   //length if array is empty 0+1
      let str = len.toString()            // converting 1 to string '1'
      let size = 3                        // size of code
      this.orgcode = 'ORG' + str.padStart(size, '0')  // padding with 00 to left ex: 001
      console.log('org code', this.orgcode);
      
    }
    else{

      console.log("all org lenth",this.AllOrg.length);
      let index = this.AllOrg.length - 1

      let last_code = this.AllOrg[index].organizationCode
      console.log('last_code', last_code);
      let str = last_code.slice(3)
      console.log('string', str);
      let digit = parseInt(str)
      digit = digit + 1
      let str1 = digit.toString()
      let size = 3
      this.orgcode = 'ORG' + str1.padStart(size, '0')
      console.log('role code', this.orgcode);


    }



      this.rowData= this.AllOrg

      if(this.AllOrg !== undefined){
        this.AllOrg.forEach(element => {
          this.options.push(element.organization_name)
          console.log('each org naem', element.organization_name);
  
        });
      }
     
    })



    // Get ALL Restaurant API Call
    this.restServ.getAllRestaurant().subscribe(data => {

     console.log('loopdata data', data['data']);
     
      this.loopdata = data['data']

     if(this.loopdata !=='No restaurants available'){
      this.loopdata.forEach(element => {
        this.onlyRestoInfo.push(element.restoInfo)
        this.onlyEstablish.push(element.establishmentType)
        this.onlyLegal.push(element.legalInfo)
        this.onlyMedia.push(element.media)
        this.onlyMenu.push(element.menuManagement)
      });
      console.log('all data', this.loopdata)
      console.log('only resto info', this.onlyRestoInfo);
      console.log('only establish', this.onlyEstablish);
      console.log('only legal', this.onlyLegal);
      console.log('only media', this.onlyMedia);
      console.log('only menu', this.onlyMenu);


        // concat all arrays

        this.tableData()
  
   
     }
      

      // this.rowData = this.onlyRestoInfo
     


    })

   
  }

  ngOnInit() {
    this.filteredOptions = this.organizationForm.controls.organization_name.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );



  }

 
  // Add Organization
  addOrg(){
    this.isAddOrg = true
  }

  // Table data

  tableData(){

    console.log("dataTable loop data", this.onlyRestoInfo )
           //my code prme ng data


           let outletTable = []
            let orgName;
            let orgCode;
           this.loopdata.forEach(element => {

            this.AllOrg.forEach(ele => {
              ele.outlets_arr.forEach(outlet => {
                if(outlet._id == element._id){
                  orgName = ele.organization_name
                  orgCode = ele.organizationCode
                }
              });
            });

            let Gst
            let Pan
            let Fssai
            let cuisines = []
            //GST PAN FSSAI
            if(element.legalInfo.documents){
              element.legalInfo.documents.forEach(docit => {
                if(docit.documentName=='GST'){
                   Gst = docit.documentNumber
                }
    
                if(docit.documentName=='PAN Card'){
                   Pan = docit.documentNumber
                }
    
                if(docit.documentName=='FSSAI Certificate'){
                   Fssai = docit.documentNumber
                }
             });
            }

            // Cuisines
            if(element.menuManagement.cuisines){
              element.menuManagement.cuisines.forEach(cui => {
                cuisines.push(cui.city)
              });
            }
      


           outletTable.push(
             {
              name: element.restoInfo.restoName,
              out_code: element.outletInfo.outlet_code,
              locality: element.restoInfo.restoLocality,
              contact: element.restoInfo.restoCountryCode +'- '+ element.restoInfo.restoContactNo,
              pincode: element.restoInfo.restoPincode,             
              infrasture: element.outletInfo.infrastructure_type,
              pricefortwo: element.restoInfo.restoPriceForTwo,
              type: element.restoInfo.restoType,
              cuisines: cuisines,
              address: element.restoInfo.restoAddress,
              website: element.restoInfo.restoWebsite,
              organization: orgName,
              orgCode: orgCode,
              dateOnBoardQrServ: 'NA',
              product:'NA',
              chainid: element.outletInfo.chain_name,
              account_no: element.legalInfo.accountNo,
              ifsc_code: element.legalInfo.IFSCCode,
              account_type: element.legalInfo.accountType,
              gst: Gst,
              pan: Pan,
              fssai: Fssai,
              average_cart_value: 0,
              orders: 0,
              revenue:0,
              total_orders: 0
             }
           ) 


         
           });
           this.outletTable = outletTable
           console.log("Main data table", this.outletTable)
          this.cols = [
            { field: "orgCode", header: "Organization Code" },
            { field: "out_code", header: "Outlet Code" },
            { field: "name", header: "Outlet Name" },            
            { field: "organization", header: "Organization Name" },         
            { field: "locality", header: "Locality" },
            { field: "pincode", header: "Pincode" },
            { field: "contact", header: "Contact" },
            { field: "infrasture", header: "Infrastructure" },
            { field: "pricefortwo", header: "Price For 2" },
            { field: "type", header: "Restaurant Type" },
            { field: "cuisines", header: "Cuisines" },
            { field: "address", header: "Address" },
            { field: "website", header: "Restaurant Website" },
            { field: "dateOnBoardQrServ", header: "Date of Onboarding QR Service" },
            { field: "product", header: "Product" },
            { field: "chainid", header: "Chain Id" },
            { field: "account_no", header: "Account Number" },
            { field: "ifsc_code", header: "IFSC Code" },
            { field: "account_type", header: "Account Type" },
            { field: "gst", header: "GST Number" },
            { field: "pan", header: "PAN" },
            { field: "fssai", header: "FSSAI Certificate" },
            { field: "average_cart_value", header: "Average Cart Value" },
            { field: "orders", header: "Orders" },
            { field: "revenue", header: "Revenue" },
            { field: "total_orders", header: "Total Orders" }
          ];

          this._selectedColumns = this.cols;

          console.log("Selected Columns : ",this._selectedColumns);
          console.log("Json : ", this.outletTable);
      
  }

  // EMAIL VALIDATION KEY UP FUNCTION
  onEmailKeyup() {
    let abc = this.organizationForm.controls['org_email'].value
    this.organizationForm.patchValue({
      org_email: abc.trim().toLowerCase().replace(/\s/g, "")

    })


    console.log('Organization email', this.organizationForm.controls['org_email'].value);
  }

// fileter auto complete
  private _filter(value: string): string[] {

    if (value != undefined) {
      const filterValue = value.toLowerCase();

      return this.options.filter(option => option.toLowerCase().includes(filterValue));
    }

  }


  onSaveOrg() {

    this.organizationForm.patchValue({
      organizationCode: this.orgcode,
      userid: this.userid,
      outlets_arr: []
    })
    console.log('Saving org', this.organizationForm.value)

    this.restServ.OrganizationObj = this.organizationForm.value
    this.router.navigate(['/restaurants/add-outlet'])

  }

  // ON Proceed

  onProceed() {

    console.log('existed org', this.existOrg);
    // this.restServ.existOrganization

    this.restServ.OrganizationObj = this.existOrg
    this.router.navigate(['/restaurants/add-outlet'])
  }


  // SELECTED ORGANIZATIONS
  onSelectedOrg(item) {
    console.log('Selected Items', item);
    this.isReadOnly = true
    this.AllOrg.forEach(element => {


      if (item == element.organization_name) {
        console.log('found', element);

        this.existOrg = element
        this.organizationForm.patchValue({
          representative_name: element.representative_name,
          org_email: element.org_email,
          org_contact: element.org_contact,

        })

      }
    });
  }

  // On Change 
  onChange() {
    this.organizationForm.reset()
    this.isReadOnly = false
  }




  // ON DELETE
  onDeleteOrg() {
    // let delid = this.existOrg._id
    // this.restServ.deleteOrg(delid).subscribe(res=>{
    //   console.log('response after delete org',res);
    //   if(res['success']){
    //     this.organizationForm.reset()
    //     Swal.fire('Delete Successfully')
    //   }
    // })

    this.organizationForm.reset()
    this.restServ.OrganizationObj = undefined
    this.isReadOnly = false
  }

  // Delete all orgnaization
  deleteAll(){
    this.restServ.deleteAllOrg().subscribe(res=>{
      console.log('response',res);

      if(res['success']){
        Swal.fire('Deleted All','Successfully','success')
        this.onRefresh()
      }
      else{
        Swal.fire('Something went Wrong','','error')
      }
      
    })

   
  }


  // *******************************ON PAGE REFRESH************************************

  onRefresh(){
       // Get All Organizations
       this.restServ.getAllOrg().subscribe(data => {
        console.log('get all organization', data['data']);
        this.AllOrg = data['data']
  
         // **********creating code serially

    if(this.AllOrg.length == 0){
      let len = this.AllOrg.length + 1   //length if array is empty 0+1
      let str = len.toString()            // converting 1 to string '1'
      let size = 3                        // size of code
      this.orgcode = 'ORG' + str.padStart(size, '0')  // padding with 00 to left ex: 001
      console.log('org code', this.orgcode);
      
    }
    else{

      console.log("all org lenth",this.AllOrg.length);
      let index = this.AllOrg.length - 1

      let last_code = this.AllOrg[index].organizationCode
      console.log('last_code', last_code);
      let str = last_code.slice(3)
      console.log('string', str);
      let digit = parseInt(str)
      digit = digit + 1
      let str1 = digit.toString()
      let size = 3
      this.orgcode = 'ORG' + str1.padStart(size, '0')
      console.log('role code', this.orgcode);


    }
  
  
  
        this.rowData= this.AllOrg
  
        if(this.AllOrg !== undefined){
          this.AllOrg.forEach(element => {
            this.options.push(element.organization_name)
            console.log('each org naem', element.organization_name);
    
          });
        }

        if(this.AllOrg.length ==0){
          this.options = []
        }
       
      })
  
  
  
      // Get ALL Restaurant API Call
      this.restServ.getAllRestaurant().subscribe(data => {
  
       console.log('loopdata data', data['data']);
       
        this.loopdata = data['data']
  
       
          this.loopdata.forEach(element => {
            this.onlyRestoInfo.push(element.restoInfo)
            this.onlyEstablish.push(element.establishmentType)
            this.onlyLegal.push(element.legalInfo)
            this.onlyMedia.push(element.media)
            this.onlyMenu.push(element.menuManagement)
          });
          console.log('all data', this.loopdata)
          console.log('only resto info', this.onlyRestoInfo);
          console.log('only establish', this.onlyEstablish);
          console.log('only legal', this.onlyLegal);
          console.log('only media', this.onlyMedia);
          console.log('only menu', this.onlyMenu);
  
  
            // concat all arrays
  
  
        // this.rowData = this.onlyRestoInfo
        
       
  
      
  
  
      })

      this.tableData()
  }


  // functions ng prime

    // ON CHECKBOX CLICK
    onChk() {
      console.log("selected1", this.selected1);
    }
  
    // show hide columns
    @Input() get selectedColumns(): any[] {
      return this._selectedColumns;
    }
  
    set selectedColumns(val: any[]) {
      //restore original order
      this._selectedColumns = this.cols.filter((col) => val.includes(col));
  
      console.log("Set value of selected columns : ", this._selectedColumns);
    }




    //***********************NAVIGATE BY ORGANIZATION NAME******************************* */
    navOrgName(item){
      this.AllOrg.forEach(element => {


        if (item == element.organization_name) {
          console.log('found', element);
  
          this.existOrg = element         
          this.restServ.OrganizationObj = this.existOrg
          this.router.navigate(['/restaurants/add-outlet'])
        }
      });
    }

     //***********************NAVIGATE BY ORGANIZATION CODE******************************* */
     navOrgCode(item){
      this.AllOrg.forEach(element => {


        if (item == element.organizationCode) {
          console.log('found', element);
  
          this.existOrg = element         
          this.restServ.OrganizationObj = this.existOrg
          this.router.navigate(['/restaurants/add-outlet'])
        }
      });
    }

    // *******************NAVIGATE BY OUTLET NAME*******************************/
   
    navOutName(id){
      console.log('outname ', id)

      this.loopdata.forEach(element => {
        if(element.restoInfo.restoName == id){
          console.log("element resto", element)
          this.restServ.view_restaurant = element
          this.restServ.current_restoid = element._id
          this.router.navigate(['/restaurants/view-restaurant'])
        }
      });
    }



        // *******************NAVIGATE BY OUTLET ID*******************************/

    navOutCode(id){
      console.log('outlet code', id)
      this.loopdata.forEach(element => {
        if(element.restoInfo.restoName == id.name){
          console.log("element resto", element)
          this.restServ.view_restaurant = element
          this.restServ.current_restoid = element._id
          this.router.navigate(['/restaurants/view-restaurant'])
        }
      });
    }
}


