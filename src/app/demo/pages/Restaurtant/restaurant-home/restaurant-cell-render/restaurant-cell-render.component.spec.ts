import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestaurantCellRenderComponent } from './restaurant-cell-render.component';

describe('RestaurantCellRenderComponent', () => {
  let component: RestaurantCellRenderComponent;
  let fixture: ComponentFixture<RestaurantCellRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestaurantCellRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantCellRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
