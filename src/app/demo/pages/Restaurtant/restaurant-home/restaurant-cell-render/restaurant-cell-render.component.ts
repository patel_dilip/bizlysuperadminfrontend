import { Component, OnInit } from '@angular/core';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-restaurant-cell-render',
  templateUrl: './restaurant-cell-render.component.html',
  styleUrls: ['./restaurant-cell-render.component.scss']
})
export class RestaurantCellRenderComponent implements OnInit {

  title: any
  data: any
  constructor(
    private restServ: RestoService,
    private router: Router) { 


  }

  agInit(params){
    this.title= params.value
    this.data = params.data
  }

  ngOnInit() {
  }

  onRestoView(){
    console.log('show data', this.data);
    this.restServ.view_restaurant = this.data
    this.restServ.current_restoid = this.data._id
    this.router.navigate(['/restaurants/view-restaurant'])
  }

}
