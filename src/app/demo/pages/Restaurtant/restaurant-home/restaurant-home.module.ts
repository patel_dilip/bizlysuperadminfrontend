import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestaurantHomeComponent } from './restaurant-home.component';
import { Routes, RouterModule } from '@angular/router';
import { AddRestuarantComponent } from './Add-restuarants/add-restuarant/add-restuarant.component';
import { BreadcrumbModule } from 'src/app/theme/shared/components';
import { ListRestaurantsComponent } from './Add-restuarants/List-restaurants/list-restaurants/list-restaurants.component';
import { MaterialModule } from 'src/app/material/material.module';
import { ReactiveFormsModule } from '@angular/forms'
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/compiler/src/core';
import { EstbNavComponent } from 'src/app/Navbar/estb-nav/estb-nav.component';
import { SubCatComponent } from 'src/app/PopoversList/sub-cat/sub-cat.component';
import { FileUploadModule } from "ng2-file-upload";
import { AddCateMenuComponent } from 'src/app/PopoversList/add-cate-menu/add-cate-menu.component';

import { NgbTabsetModule, NgbTooltipModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';

// import { DataTablesModule } from 'angular-datatables';
// import { TblSearchingComponent } from '../../tables/tbl-datatable/tbl-searching/tbl-searching.component';
// import { TblDatatableComponent } from '../../tables/tbl-datatable/tbl-datatable.component';
import { ViewRestaurantComponent } from './view-restaurant/view-restaurant.component';
import { EditRestoInfoComponent } from './Edit-restaurants/edit-resto-info/edit-resto-info.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { OutletHomeComponent } from './outlet-home/outlet-home.component';
import { OutletListComponent } from './outlet-list/outlet-list.component';
import { AgGridModule } from 'ag-grid-angular';

import { AddOutletComponent } from './add-outlet/add-outlet.component';
import { AddMultiplexHomeComponent } from './add-multiplex-home/add-multiplex-home.component';
import { AddChainHomeComponent } from './add-chain-home/add-chain-home.component';
import { ChainHomeComponent } from './chain-home/chain-home.component'
// import { TreeModule } from 'angular-tree-component';
import { CinemaHomeComponent } from './cinema-home/cinema-home.component';
import { AddCinemaComponent } from './add-cinema/add-cinema.component';
import { ChainCellRenderComponent } from './chain-cell-render/chain-cell-render.component';
import { VuChainComponent } from './vu-chain/vu-chain.component';
import { CinemaCellRenderComponent } from './cinema-cell-render/cinema-cell-render.component';
import { ViewCinemaComponent } from './view-cinema/view-cinema.component';
import { RestaurantCellRenderComponent } from './restaurant-cell-render/restaurant-cell-render.component';
import { UpdateChainComponent } from './update-chain/update-chain.component';
import { UpdateCinemaComponent } from './update-cinema/update-cinema.component';
import { UpdateRestaurantComponent } from './update-restaurant/update-restaurant.component';
import { OrgCellRenderComponent } from './org-cell-render/org-cell-render.component';
import { AuthGuard } from 'src/app/auth/auth.guard';


// ngprime code
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { MultiSelectModule } from 'primeng/multiselect';
import { PopLayoutTabComponent } from './pop-layout-tab/pop-layout-tab.component'; 

// prime Tree
import {TreeModule} from 'primeng/tree';
import {TreeNode, ConfirmationService} from 'primeng/api';
import { PopTreeComponent } from './pop-tree/pop-tree.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ContextMenuModule} from 'primeng/contextmenu';
import { PopDelTreeComponent } from './pop-del-tree/pop-del-tree.component';
import {TabViewModule} from 'primeng/tabview';


const routes: Routes = [
  {
    path: '',
    component: RestaurantHomeComponent,
    canActivate: [AuthGuard],
    data: {
      allowedRoles: ['admin', 'bizly','user']
    },
    children: [
      {
        path: '',
        component: OutletListComponent
      },
      {
        path: 'add-outlet',
        component: AddOutletComponent
      },
      {
        path: 'outlet-home',
        component: OutletListComponent
      },
      {
        path: 'list-restaurant',
        component: ListRestaurantsComponent
      },
      {
        path: 'add-restaurant',
        component: AddRestuarantComponent
      },
      {
        path: 'view-restaurant',
        component: ViewRestaurantComponent
      },
      {
        path: 'edit-restoinfo',
        component: EditRestoInfoComponent
      },      
      {
        path: 'update-restaurant',
        component: UpdateRestaurantComponent
      },
     
      {
        path: 'add-chain',
        component: AddChainHomeComponent
      },
      {
        path: 'update-chain',
        component: UpdateChainComponent
      },
      {
        path: 'chain-home',
        component: ChainHomeComponent
      },
      {
        path: 'view-update-chain',
        component: VuChainComponent
      },
      
       {
        path: 'cinema-home',
        component: CinemaHomeComponent
      },
      {
        path: 'view-cinema',
        component: ViewCinemaComponent
      },
     {
       path: 'add-cinema',
       component: AddCinemaComponent
     },
     {
      path: 'update-cinema',
      component: UpdateCinemaComponent
    },
    ]
  }
];


@NgModule({
  declarations: [
    RestaurantHomeComponent,
    ViewRestaurantComponent,
    AddRestuarantComponent,
    ListRestaurantsComponent,
    EstbNavComponent,
    ViewRestaurantComponent,
    EditRestoInfoComponent,
    OutletHomeComponent,
    OutletListComponent,
    AddOutletComponent,
    AddMultiplexHomeComponent,
    AddChainHomeComponent,
    ChainHomeComponent,
    CinemaHomeComponent,
    AddCinemaComponent,
    ChainCellRenderComponent,
    VuChainComponent,
    CinemaCellRenderComponent,
    ViewCinemaComponent,
    RestaurantCellRenderComponent,
    UpdateChainComponent,
    UpdateCinemaComponent,
    UpdateRestaurantComponent,
    OrgCellRenderComponent,
    PopLayoutTabComponent,
    PopTreeComponent,
    PopDelTreeComponent 
 
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BreadcrumbModule,
    MaterialModule,
    ReactiveFormsModule,
    FileUploadModule,

    FormsModule,
    NgxSpinnerModule,
    NgSelectModule,
    NgbModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCvYEMMyNKpvtrKOztbpaUbK-GvfDK8EOs',
      libraries: ['places']
    }),
    AgGridModule.withComponents([
  
    ]),
    TreeModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    DialogModule,
    MultiSelectModule,
   ConfirmDialogModule,
   ContextMenuModule,
   TabViewModule
   
  ],

     
    entryComponents: [ChainCellRenderComponent, CinemaCellRenderComponent, 
      RestaurantCellRenderComponent, PopLayoutTabComponent, PopTreeComponent, PopDelTreeComponent ],

  schemas: [NO_ERRORS_SCHEMA],
  providers: [ConfirmationService]

})
export class RestaurantHomeModule { }
