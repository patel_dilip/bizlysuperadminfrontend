import { Component, OnInit, Injectable } from '@angular/core';
import { FormGroup, Validators, FormBuilder, Form, FormArray, FormControl } from '@angular/forms';
import { LiquorService } from 'src/app/_services/liquor.service';
import { EstablishmentTypeService } from 'src/app/_services/establishment-type.service';
import { MoreinfoService } from 'src/app/_services/moreinfo.service';
import { MainService } from 'src/app/_services/main.service';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { EstAddCatComponent } from 'src/app/PopoversList/est-add-cat/est-add-cat.component';
import { SubCatComponent } from 'src/app/PopoversList/sub-cat/sub-cat.component';
import { AttrCatComponent } from 'src/app/PopoversList/attr-cat/attr-cat.component';
import { CreateAlbumComponent } from 'src/app/PopoversList/create-album/create-album.component';
import { AddLayoutComponent } from 'src/app/PopoversList/add-layout/add-layout.component';
import { OpenTableComponent } from 'src/app/PopoversList/open-table/open-table.component';
import { AbstractJsEmitterVisitor } from '@angular/compiler/src/output/abstract_js_emitter';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { AddRevenueComponent } from 'src/app/PopoversList/add-revenue/add-revenue.component';
import { AddRestoTypeComponent } from 'src/app/PopoversList/add-resto-type/add-resto-type.component';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { AddCatoNameComponent } from 'src/app/PopoversList/add-cato-name/add-cato-name.component';
import { AddSubCatComponent } from 'src/app/PopoversList/add-sub-cat/add-sub-cat.component';
import { AddSubSubCatComponent } from 'src/app/PopoversList/add-sub-sub-cat/add-sub-sub-cat.component';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { ThrowStmt } from '@angular/compiler';
import { Router } from '@angular/router';
import { AddRootMenuComponent } from 'src/app/PopoversList/add-root-menu/add-root-menu.component';
import { AddCateMenuComponent } from 'src/app/PopoversList/add-cate-menu/add-cate-menu.component';
import { AddSubcatMenuComponent } from 'src/app/PopoversList/add-subcat-menu/add-subcat-menu.component';
import { AddChildChildComponent } from 'src/app/PopoversList/add-child-child/add-child-child.component';
import { AddCcMenuComponent } from 'src/app/PopoversList/add-cc-menu/add-cc-menu.component';
import { AddRootLiquorPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-root-liquor-pop/add-root-liquor-pop.component';
import { AddVarientPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-varient-pop/add-varient-pop.component';
import { AddSubVarientPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-sub-varient-pop/add-sub-varient-pop.component';
import { AddSuperChildPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-super-child-pop/add-super-child-pop.component';
import { AddServicesChipsComponent } from 'src/app/PopoversList/Establish-related-popups/add-services-chips/add-services-chips.component';
import { AddMoreInfoComponent } from 'src/app/PopoversList/Establish-related-popups/add-more-info/add-more-info.component';
import { AddRulesChipsComponent } from 'src/app/PopoversList/Establish-related-popups/add-rules-chips/add-rules-chips.component';
import { AddAmbienceChipsComponent } from 'src/app/PopoversList/Establish-related-popups/add-ambience-chips/add-ambience-chips.component';
import { AddBillcounterPopupComponent } from 'src/app/PopoversList/Layout-related-popups/add-billcounter-popup/add-billcounter-popup.component';
import { AddChairPopupComponent } from 'src/app/PopoversList/Layout-related-popups/add-chair-popup/add-chair-popup.component';
import { AddKitchenPopupComponent } from 'src/app/PopoversList/Layout-related-popups/add-kitchen-popup/add-kitchen-popup.component';
import { CreateVdoAlbumComponent } from 'src/app/PopoversList/Media-related-Popups/create-vdo-album/create-vdo-album.component';

// IMAGES UPLOAD URLS
const GstImg = environment.base_Url + "restaurantLegal/uploadgst";
const PanImg = environment.base_Url + "restaurantLegal/uploadpan";
const FssaiImg = environment.base_Url + "restaurantLegal/uploadfssai";
const OtherImg = environment.base_Url + "restaurantLegal/uploadother";
const LogoImg = environment.base_Url + "restaurant/uploadrestaurantlogo";
const BannerImg = environment.base_Url + "restaurant/uploadrestaurantbanner"
const ImagesAlbum = environment.base_Url + "restaurant/uploadrestoalbumimages"
const VideoAlbum = environment.base_Url + "restaurant/uploadrestoalbumvideos"

@Component({
  selector: 'app-edit-resto-info',
  templateUrl: './edit-resto-info.component.html',
  styleUrls: ['./edit-resto-info.component.scss']
})
export class EditRestoInfoComponent implements OnInit {

  isLinear = true;

  /// Fomrgroups Declarations
  RestaurantInfo: FormGroup;
  EstablishmentType: FormGroup;
  LegalForm: FormGroup;
  MediaForm: FormGroup;
  MenuMgmtForm: FormGroup;
  AllDialCodes: any
  Revenue: any

  isDine = false;
  isCloudKitchen = false;
  isDelievery = false;
  isSelfService = false;

  isMon = true;
  isTues = true;
  isWed = true;
  isThur = true;
  isFri = true;
  isSat = true;
  isSun = true;

  monAddHr = true
  tueAddHr = true
  wedAddHr = true
  thurAddHr = true
  friAddHr = true
  satAddHr = true
  sunAddHr = true
  dummy: any;
  Establishment: Object;
  popAdd: Object;
  popSub: Object;
  popAttr: Object;

  barMoreInfo;
  pubMoreInfo;
  serviceMoreInfo;
  rulesMoreInfo;
  placesMoreInfo;
  ambienceMoreInfo;

  selectedBars = [];
  selectedPubs = [];
  selectedServices: any = [];
  selectedRules = [];
  selectedPlaces = [];
  selectedAmbience = [];

  allCuisines: string[];
  selectedCuisines = [];

  firstLevel: Object;
  menuFirstlevel: Object;
  liquorLevel: Object;

  mondayOpenTime: any;
  mondayCloseTime: any;
  extramonOpenTime: any;
  extamonCloseTime: any;
  ischecked: any;
  docsCount: number = 0;

  albumNames = []
  menuNav: string;

  isAlbum: boolean = true;
  tabs = [];
  selected = new FormControl(0);
  tabNames: any;
  tablesSelected: any;
  currentTab: any;
  tableObj: any;
  restotype: string[];
  restaurants_types = [];
  restaurants_contact: any;
  finalRestObj: any
  finalLegalObj: any;
  finalMediaObj: any;
  cuisinesObj: any[];

  output: any = {};
  FinalEstObj: any
  finalMoreInfobj: any
  finalServicesobj: any;
  finalRulesobj: any;
  finalAmbienceObj: any;
  estobj: any
  cuisinesArr: string[];
  submenu: any
  liquormenu: any
  finalMenuMgmt: any
  objectvalue: { restoInfo: any; establishmentType: any; legalInfo: any; media: any; menuManagement: any; };
  FinalLayoutObj: any
  checkobj: any;
  allDrinks: any;
  estRootCategory: any;
  estCateName: string[];
  cateChildName: string[];
  cateChildChildName: string[];
  selectedRootType: any;
  categoryName: any;
  childCategory: any;
  subsubChild: any;
  DrinkTypes = [];
  account_types = ["Saving Account", "Current Account"]

  // Layout tab variables
  LayoutsArr = [{ title: "Indoor", table: [], chairs: [], walls: [], washroom: [], billing: [] }]
  activeLink = this.LayoutsArr[0]
  layoutCount = 0;

  // Legal Images URLs
  public gstUploader: FileUploader = new FileUploader({ url: GstImg, itemAlias: 'gstfile' });
  public panUploader: FileUploader = new FileUploader({ url: PanImg, itemAlias: 'panfile' });
  public fssaiUploader: FileUploader = new FileUploader({ url: FssaiImg, itemAlias: 'fssaifile' });
  public otherUploader: FileUploader = new FileUploader({ url: OtherImg, itemAlias: 'otherfile' });
  public logoUploader: FileUploader = new FileUploader({ url: LogoImg, itemAlias: 'restologo' });

  public BannerUploader: FileUploader = new FileUploader({ url: BannerImg, itemAlias: 'restobanner' });
  public ImgAlbumUploader: FileUploader = new FileUploader({ url: ImagesAlbum, itemAlias: 'restoalbumimages' });
  public VideoAlbumUploader: FileUploader = new FileUploader({ url: VideoAlbum, itemAlias: 'restoalbumvideos' });


  response: string;
  gstResponse: any;
  panResponse: any;
  fssaiResponse: any;
  otherResponse = [];
  isgstTrash: boolean = true;
  ispanTrash: boolean = true;
  isFssaiTrash: boolean = true;
  isOtherTrash: boolean = true;
  docs: any;
  otherdocsImages: any;
  estTreeArr = [];
  estKeepTrack = [];
  logoResponse: any;
  RestoId = '';
  BannerResponse = [];
  ImgAlbumResponse = [];
  selectedRootNames = [];
  selectedCatNames = [];
  selectedChildName = [];
  selectedChildChildName = [];
  AllRootMenu = [];
  selectedMenuRoot = [];
  selectedMenuCategory = [];
  selectedChildCategory = [];
  selectedMenucc = [];
  selectedRootDrinksName = [];
  selectedDrinkVarient = [];
  selectedDrinkSubVarient = [];
  selectedDrinkSuperChild = [];
  selectedCuisinesName = [];
  allMoreInfos: any;

  isAddTable = true
  isAddChair = true
  isBillCounter = true
  isKitchen = true
  dupChkServ = [];
  dupChkMoreInfo = [];
  selectedMoreInfos = [];
  dupChkRules = [];
  selectedRulesInfo = [];
  dupChkAmbience = [];
  selectedAmbienceInfo = [];
  createdVdoAlbum = [];
  VideoAlbumResponse = [];
  album_image: any = [];
  album_video: any = [];
  testObj: any;
  tableCount: any = 0;
  billCount: any = 0;
  wallcount: any = 0;
  washroomCount: any = 0;

  Resto: any;
  Establish: any;
  Legal: any;
  Media: any;
  Menu: any;
  Layout: any;
  // RestoId: any;


  constructor(private fb: FormBuilder,
    private liqourService: LiquorService,
    private establishService: EstablishmentTypeService,
    private moreinfo: MoreinfoService,
    private mainService: MainService,
    private matDialog: MatDialog,
    private restServ: RestoService,
    private http: HttpClient,
    private addMenuFoodsService: AddMenuFoodsService,
    private router: Router
  ) {

    // ///////////////////////////
    // Fetching Resto Id///////
    ///////////////////////////

    if (this.restServ.RestoId == undefined) {
      this.router.navigate(['/restaurants'])
    }
    else {
      console.log('only resto id', this.restServ.RestoId);
      this.RestoId = this.restServ.RestoId
    }

    // ///////////////////////////
    // fETCHING rESTO iNFO
    //////////////////////////////

    if (this.restServ.onlyRestoInfo == undefined) {
      this.router.navigate(['/restaurants'])
    }
    else {
      console.log('only resto info', this.restServ.onlyRestoInfo);
      this.Resto = this.restServ.onlyRestoInfo
      // this.Resto.restoStatus =false

    }



    // ///////////////////////////
    // fETCHING eSTABLISMENT INFO
    //////////////////////////////


    if (this.restServ.onlyEstablish == undefined) {
      this.router.navigate(['/restaurants'])
    }
    else {
      console.log('only estblisment info', this.restServ.onlyEstablish);
      this.Establish = this.restServ.onlyEstablish
      // this.Resto.restoStatus =false




    }


    // ///////////////////////////
    // fETCHING lEGAL iNFO
    //////////////////////////////

    if (this.restServ.onlyLegalInfo == undefined) {
      this.router.navigate(['/restaurants'])
    }
    else {
      console.log('only Legal info', this.restServ.onlyLegalInfo);
      this.Legal = this.restServ.onlyLegalInfo
      // this.Resto.restoStatus =false

    }

    // ///////////////////////////
    // fETCHING mEDIA iNFO
    //////////////////////////////

    if (this.restServ.onlyMedia == undefined) {
      this.router.navigate(['/restaurants'])
    }
    else {
      console.log('only Media info', this.restServ.onlyMedia);
      this.Media = this.restServ.onlyMedia
      // this.Resto.restoStatus =false

    }

    // ///////////////////////////
    // fETCHING MENU MGMT
    //////////////////////////////

    if (this.restServ.onlyMenu == undefined) {
      this.router.navigate(['/restaurants'])
    }
    else {
      console.log('only Menu info', this.restServ.onlyMenu);
      this.Menu = this.restServ.onlyMenu
      // this.Resto.restoStatus =false

    }

    // ///////////////////////////
    // fETCHING lAYOUT DETAILS
    //////////////////////////////

    if (this.restServ.onlyLayout == undefined) {
      this.router.navigate(['/restaurants'])
    }
    else {
      console.log('only Layout info', this.restServ.onlyLayout);
      this.Layout = this.restServ.onlyLayout
      // this.Resto.restoStatus =false

    }



    // call get all more infos

    this.getAllMoreInfos();
    // Images uploaders



    //  get all establishmetn tree

    this.getEstRootType()




    this.allCuisines = ['Spanish', 'Sri Lankan', 'Taiwanese', 'Tatar', 'Thai', 'Turkish', 'Tamil', 'Udupi', 'Ukrainian', 'Vietnamese', 'Yamal', 'Zambian', 'Zanzibari']

    // Restaurant's type
    this.restotype = ['Dine-In', 'Cloud Kitchen', 'Delievery', 'Self Service']

    //dummy
    this.dummy = 'Establishment'
    this.menuNav = 'Cuisines'

    //  Calling RestoTYpe service
    this.getAllRestoType();

    // Calling all Drinks service
    this.getRootDrinkTypes()

    // Calling Revenue service
    this.getRevenue();

    // get establishment root category

    this.getEstRootType();

    // GET ALL ROOT MENU
    this.getAllRestoMenu();
    // Calling Liquor service
    this.liqourService.getAllRootDrinks().subscribe(data => {
      console.log("Liquor types", data);

      this.liquorLevel = data
      // console.table("Liquor types",data);

    })


    // *********** Calling all establishmentypes service
    this.establishService.getAllEstablishmentTypes().subscribe(data => {
      console.log("establishment types array: ", data);

    })


    // *******Calling all cuisines API***********
    this.restServ.getAllCuisines().subscribe(data => {
      this.allCuisines = data['data']
      console.log('All cuisines form DB', this.allCuisines);

    })

    // *******Calling All More Info API*********
    this.restServ.getAllMoreInfo().subscribe(data => {

      this.barMoreInfo = data['data']
      console.log('more infos form db: ', this.barMoreInfo);

    })

    // ********Calling All Services API******


    this.getAllServicesChips()

    //****** Calling All Rules API******

    this.getAllRulesChips();

    // ******* Calling Ambience API***********
    this.getAllAmbienceChips();

    //Calling all Dial COdes form service
    this.mainService.getAllDialCode().subscribe(data => {
      console.log('All Dial Codes', data);

      this.AllDialCodes = data

    })



    /// fetcing establishmet data array
    this.mainService.getAllEstablishment().subscribe(data => {
      console.log('establishment', data);
      this.firstLevel = data;

    })

    //fetching menus form menu mgmt
    this.mainService.getAllMenu().subscribe(data => {
      console.log(
        'Menu', data
      );

      this.menuFirstlevel = data;
    })

  }
  ngOnInit() {
    this.RestaurantInfo = this.fb.group({

      // Restaurant Basic Information
      legal_name: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[ a-zA-Z0-9]+$')]],
      restaurant_name: ['', [Validators.required, Validators.pattern('^[ a-zA-Z0-9]+$')]],
      restaurant_country_code: ['', Validators.required],
      restaurant_contact: ['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      restaurant_email: ['', [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+[\.]+[a-z]{2,4}$")]],
      restaurant_address: ['', [Validators.required, Validators.pattern('^[ a-zA-Z0-9]+$')]],
      restaurant_locality: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      restaurant_pincode: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      restaurant_revenue: ['', Validators.required],
      restaurant_website: ['', Validators.required],
      representative_name: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      representative_contact: ['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      representative_email: ['', [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+[\.]+[a-z]{2,4}$")]],
      openMon: [''],
      openTue: [''],
      openWed: [''],
      openThur: [''],
      openFri: [''],
      openSat: [''],
      openSun: [''],

      extraopenMon: [''],
      extraopenTue: [''],
      extraopenWed: [''],
      extraopenThur: [''],
      extraopenFri: [''],
      extraopenSat: [''],
      extraopenSun: [''],

      closeMon: [''],
      closeTue: [''],
      closeWed: [''],
      closeThur: [''],
      closeFri: [''],
      closeSat: [''],
      closeSun: [''],

      extracloseMon: [''],
      extracloseTue: [''],
      extracloseWed: [''],
      extracloseThur: [''],
      extracloseFri: [''],
      extracloseSat: [''],
      extracloseSun: [''],

    });

    this.EstablishmentType = this.fb.group({

      // Restaurants Establishment type name
      select_establishment_type: ['', Validators.required],
      establishment_more_info: ['', Validators.required],
      establishment_services: ['', Validators.required],
      establishment_rules_and_regulations: ['', Validators.required],
      establishment_place: ['', Validators.required],
      establishment_ambience: ['', Validators.required],
      establishment_around_places: ['', Validators.required]
    });

    this.LegalForm = this.fb.group({

      //Banks detail []
      restaurant_bank_details: ['', Validators.required],

      // Bank Details Form Control Names
      restaurant_account_no: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(20), Validators.pattern('^[0-9]{7,20}$')]],
      restaurant_account_type: ['', Validators.required],
      restaurant_bank_name: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      restaurant_branch_name: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      restaurant_bank_city: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      restaurant_bank_address: ['', Validators.required],
      restaurant_bank_ifsccode: ['', [Validators.required, Validators.maxLength(11), Validators.minLength(3), Validators.pattern('^[A-Za-z]{4}0[A-Z0-9a-z]{6}$')]],
      restaurant_gst_no: ['', Validators.required],
      restaurant_gst_image: ['', Validators.required],
      restaurant_pan_no: ['', Validators.required],
      restaurant_pan_image: ['', Validators.required],
      restaurant_fssai_certificate: ['', Validators.required],
      restaurant_fssai_image: ['', Validators.required],
      restaurant_legal_document: this.fb.array([])

    });



    this.MediaForm = this.fb.group({

      // other Legal Document Controls Name
      restaurant_logo: ['', Validators.required],
      restaurant_banner: ['', Validators.required],
      restaurant_pictures: ['', Validators.required],
      restaurant_videos: ['', Validators.required]

    });

    this.MenuMgmtForm = this.fb.group({


      //Restaurant Food [] contro l NAME
      restaurant_add_food: ['', Validators.required],


      //Restaurant Menu Management
      restaurant_select_cuisine: ['', Validators.required],
      restaurant_select_menu: ['', Validators.required],

      //Restaurant Liquor 
      restaurant_add_liquor_product: ['', Validators.required]

    });

  

    // patchvalue for edit resto info form
    this.setRestoForm()

    // patchValue for edit legal info form
    this.setLegalInfo()

    // IMAGES UPLOADER RESOPONSE (inside ngOnInit)

    this.response = '';


    // ***GST UPLOADER
    // this.gstUploader.response.subscribe(res => this.response = res);

    this.gstUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      console.log('hello');
      
      this.gstResponse = response
      this.LegalForm.patchValue({
        restaurant_gst_image: this.gstResponse
      })
      console.log('final response', this.gstResponse);
      // this.gstResponse = this.gstResponse
      this.isgstTrash = false

    }


    //  **PAN UPLOAD
    this.panUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.panResponse = response
      console.log('response', this.panResponse);
      this.LegalForm.patchValue({
        restaurant_pan_image: this.panResponse
      })
      // console.log('final response', this.panResponse['gstFilePhotoUrl']);
      // this.panResponse = this.panResponse['gstFilePhotoUrl']
      this.ispanTrash = false
    }

    // ** FSSAI UPLOAD

    this.fssaiUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.fssaiResponse = response
      console.log('response', this.fssaiResponse);
      this.LegalForm.patchValue({
        restaurant_fssai_image: this.fssaiResponse
      })
      // console.log('final response', this.fssaiResponse['gstFilePhotoUrl']);
      // this.fssaiResponse = this.fssaiResponse['gstFilePhotoUrl']
      this.isFssaiTrash = false
    }

    // ** Other UPLOAD

    this.otherUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      if (this.otherResponse.length <= 4) {
        this.otherResponse.push(response)
        this.docs.patchValue({
          documentUrl: response
        })
      }



      console.log('response', this.otherResponse);

      // console.log('other response', this.otherResponse['gstFilePhotoUrl']);
      // this.docs.patchValue({
      //   documentUrl: this.otherResponse
      // })
      // this.otherResponse = this.otherResponse['gstFilePhotoUrl']
      // this.isOtherTrash = false

    }

    // **Other LOGO UPLOAD
    this.logoUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.logoResponse = response
      console.log('Logo Response', response);
      Swal.fire('Logo Uploaded', '', 'success')

    }

    // **Other BannerUploader UPLOAD
    this.BannerUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.BannerResponse.push(response)
      console.log(' Banner Response', this.BannerResponse);

    }

    // **Other ImgAlbumUploader UPLOAD
    this.ImgAlbumUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ImgAlbumResponse.push(response)

      console.log('Images Response', this.ImgAlbumResponse);

    }

    // Video collection empty



    // **Other LOGO UPLOAD
    this.VideoAlbumUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // this.VideoAlbumResponse = []
      console.log('FileUpload:uploaded:', item, status, response);
      this.VideoAlbumResponse.push(response)

      console.log(' Video Response', this.VideoAlbumResponse);

    }



  }


  //   // /uploa
    uploadWorks(){
     console.log('hellooooooooooooooo');
    //  this.gstUploader.uploadAll();
      }


  // set patch value for Resto
  setRestoForm() {
    this.RestaurantInfo = this.fb.group({

      // Restaurant Basic Information
      legal_name: this.Resto.restoLegalName,
      restaurant_name: this.Resto.restoName,
      restaurant_country_code: this.Resto.restoContactNo,
      restaurant_contact: this.Resto.restoContactNo,
      restaurant_email: this.Resto.restoEmail,
      restaurant_address: this.Resto.restoAddress,
      restaurant_locality: this.Resto.restoLocality,
      restaurant_pincode: this.Resto.restoPincode,
      restaurant_revenue: this.Resto.restoRevenue,
      restaurant_website: this.Resto.restoWebsite,
      representative_name: this.Resto.restoRepresentativeName,
      representative_contact: this.Resto.restoRepresentativeContactNo,
      representative_email: this.Resto.restoRepresentativeEmail,


      openMon: this.Resto.restoHours[0].openMon,
      openTue: this.Resto.restoHours[1].openTue,
      openWed: this.Resto.restoHours[2].openWed,
      openThur: this.Resto.restoHours[3].openThur,
      openFri: this.Resto.restoHours[4].openFri,
      openSat: this.Resto.restoHours[5].openSat,
      openSun: this.Resto.restoHours[6].openSun,

      extraopenMon: this.Resto.restoHours[0].extraopenMon,
      extraopenTue: this.Resto.restoHours[1].extraopenTue,
      extraopenWed: this.Resto.restoHours[2].extraopenWed,
      extraopenThur: this.Resto.restoHours[3].extraopenThur,
      extraopenFri: this.Resto.restoHours[4].extraopenFri,
      extraopenSat: this.Resto.restoHours[5].extraopenSat,
      extraopenSun: this.Resto.restoHours[6].extraopenSun,

      closeMon: this.Resto.restoHours[0].closeMon,
      closeTue: this.Resto.restoHours[1].closeTue,
      closeWed: this.Resto.restoHours[2].closeWed,
      closeThur: this.Resto.restoHours[3].closeThur,
      closeFri: this.Resto.restoHours[4].closeFri,
      closeSat: this.Resto.restoHours[5].closeSat,
      closeSun: this.Resto.restoHours[6].closeSun,

      extracloseMon: this.Resto.restoHours[0].extracloseMon,
      extracloseTue: this.Resto.restoHours[1].extracloseTue,
      extracloseWed: this.Resto.restoHours[2].extracloseWed,
      extracloseThur: this.Resto.restoHours[3].extracloseThur,
      extracloseFri: this.Resto.restoHours[4].extracloseFri,
      extracloseSat: this.Resto.restoHours[5].extracloseSat,
      extracloseSun: this.Resto.restoHours[6].extracloseSun,

    });
  }

  // set legal info values for Resto
  setLegalInfo() {

  //  Setting Images for GstResponse
    this.gstResponse = this.Legal.documents[0].documentUrl
    this.panResponse = this.Legal.documents[1].documentUrl
    this.fssaiResponse = this.Legal.documents[2].documentUrl 
      this.LegalForm.patchValue({

  // //       //Banks detail []
  // //       // restaurant_bank_details: ['', Validators.required],


  // // //       accountNo: 98745632101
  // // // accountType: "Saving Account"
  // // // bankName: "YES Bank"
  // // // branchName: "Karve Nagar"
  // // // city: "Pune"
  // // // address: "Yes Bank Karishma Society"
  // // // IFSCCode: "789BKD5"

  // //       // Bank Details Form Control Names
        restaurant_account_no: this.Legal.accountNo,
        restaurant_account_type: this.Legal.accountType,
        restaurant_bank_name: this.Legal.bankName,
        restaurant_branch_name: this.Legal.branchName,
        restaurant_bank_city: this.Legal.city,
        restaurant_bank_address: this.Legal.address,
        restaurant_bank_ifsccode: this.Legal.IFSCCode,
        restaurant_gst_no: this.Legal.documents[0].documentNumber,
        restaurant_gst_image: this.Legal.documents[0].documentUrl,
        restaurant_pan_no: this.Legal.documents[1].documentNumber,
        restaurant_pan_image: this.Legal.documents[1].documentUrl,
        restaurant_fssai_certificate: this.Legal.documents[2].documentNumber,
        restaurant_fssai_image: this.Legal.documents[2].documentUrl,
        restaurant_legal_document: this.fb.array([])

      });

    
   console.log('legal other documents', this.Legal);
   console.log('length of other docuemtns', this.Legal );
   


  }

  // on click over establishment
  estSelected() {
    // alert('selected establismeent')
    // console.log('select establsiment');
    this.isLinear = false
  }

  // banners successfully uploaded
  banSuccessUp() {
    Swal.fire('Banner Successfully Uploaded', '', 'success')
  }

  // banners successfully uploaded


  // empty image collection
  emptyImgCollection() {
    this.ImgAlbumResponse = []
    console.log('empty img collection');

  }

  // empty vdo collection for next
  emptyVdoCollection() {
    this.VideoAlbumResponse = []
    console.log('empty video collection')
  }

  // images media json creation

  imgMediaJson(title) {
    console.log('title', title);
    console.log('collection images', this.ImgAlbumResponse);

    this.album_image.push({ name: title, fileurl: this.ImgAlbumResponse })

    console.log('album_image', this.album_image)
    Swal.fire(title + ' Album Successfully Uploaded', '', 'success')
  }

  // videos media json creation

  vdoMediaJson(title) {

    this.album_video.push({ name: title, fileurl: this.VideoAlbumResponse })

    console.log('album videos json', this.album_video)

    Swal.fire(title + ' Album Successfully Uploaded', '', 'success')
  }


  onSaveMediaInfo() {
    let mediaObj = {
      resto_logo: this.logoResponse,
      resto_banner: this.BannerResponse,
      album_image: this.album_image,
      album_video: this.album_video
    }

    this.testObj = mediaObj
    console.log('media object ', mediaObj);
    // this.RestoId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.putMediaObjtoResto(this.RestoId, mediaObj).subscribe(res => {
      console.log('resposne after media uploade', res);
      if (res['sucess']) {
        Swal.fire('Media Uploaded Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Upload', '', 'warning')
      }

    })

  }

  //   // on get add extra documents
  // getAddDocs(){
  //   return this.LegalForm.controls.restaurant_legal_document as FormArray
  // }

  // on get all SERVICES ALL CHIPS
  getAllServicesChips() {
    this.restServ.getAllServicesInfo().subscribe(data => {
      this.serviceMoreInfo = data['data']
      console.log('services from db', this.serviceMoreInfo);

    })
  }

  // Geta all rules all chips
  getAllRulesChips() {

    this.restServ.getAllRulesInfo().subscribe(data => {
      this.rulesMoreInfo = data['data']
      // console.log('rules from db', data);

    })


  }

  // get all ambience all chips
  getAllAmbienceChips() {

    this.restServ.getAllAmbienceInfo().subscribe(data => {
      this.ambienceMoreInfo = data['data']
      console.log('ambience from db', this.ambienceMoreInfo);

    })
  }

  // on view add table controllers
  onViewTableCtrl() {
    this.isAddTable = false
    this.isAddChair = true
    this.isBillCounter = true
    this.isKitchen = true
  }

  // on view add chairs controllers
  onViewChairCtrl() {
    this.isAddTable = true
    this.isAddChair = false
    this.isBillCounter = true
    this.isKitchen = true
  }

  // on view add billing counter controllers
  onViewBillingCtrl() {
    this.isAddTable = true
    this.isAddChair = true
    this.isBillCounter = false
    this.isKitchen = true
  }

  // on view add kitchen controllers
  onViewKitchenCtrl() {
    this.isAddTable = true
    this.isAddChair = true
    this.isBillCounter = true
    this.isKitchen = false
  }

  // get all morinfos
  getAllMoreInfos() {
    this.restServ.getAllMoreInfos().subscribe(data => {
      console.log('get all more infos for chips data', data);
      this.allMoreInfos = data['data']
    })
  }
  // on Add More Info open dialog

  openAddMoreInfo() {
    const dialogRef = this.matDialog.open(AddMoreInfoComponent, {
      width: '540px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result of moreInfo', result);
      this.getAllMoreInfos();
    })
  }

  // on Add Services open dialog
  openAddServicesChips() {
    const dialogRef = this.matDialog.open(AddServicesChipsComponent, {
      width: '540px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllServicesChips()
    })
  }

  // on ADD rules open dialog
  openAddRulesChips() {
    const dialogRef = this.matDialog.open(AddRulesChipsComponent, {
      width: '540px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRulesChips()
    })
  }

  // on Add ambience open dialog
  openAddAmbienceChips() {
    const dialogRef = this.matDialog.open(AddAmbienceChipsComponent, {
      width: '540px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllAmbienceChips()
    })
  }
  // on Add dialog of Liquor

  openAddLiquorRoot() {
    const dialogRef = this.matDialog.open(AddRootLiquorPopComponent, {
      width: '540px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getRootDrinkTypes()
    })

  }

  // open add dialog liqour varient
  openAddLiquorVarient(root) {
    let root_id = root['_id']
    const dialogRef = this.matDialog.open(AddVarientPopComponent, {
      width: '540px',
      data: root_id
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('reslut', result);
      this.getRootDrinkTypes()
    })
  }

  // open add dialog SUB liquor varient
  openAddSubVarient(root, cat) {
    const dialogRef = this.matDialog.open(AddSubVarientPopComponent, {
      width: '540px',
      data: { root: root['_id'], cat: cat['_id'] }
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('reslut', result);
      this.getRootDrinkTypes()
    })
  }

  // open add dialog super liquor child
  openAddLiquorSuperChild(root, child) {
    const dialogRef = this.matDialog.open(AddSuperChildPopComponent, {
      width: '540px',
      data: { root: root._id, child: child._id }

    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('reslut', result);
      this.getRootDrinkTypes()
    })
  }
  // Add Bill Counter
  OnAddBillCounter(a) {

    const dialogRef = this.matDialog.open(AddBillcounterPopupComponent, {
      width: "540px",
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result after add bill counter', result);

    })
  }

  // open popup to add chair to layout
  onAddChairtoLayout(a) {
    const dialogRef = this.matDialog.open(AddChairPopupComponent, {
      width: "540px",
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result after add chair', result);

    })
  }

  // open popup for add kitchen
  onAddKitchentoLayout(a) {
    const dialogRef = this.matDialog.open(AddKitchenPopupComponent, {
      width: "540px",
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result after add kitchen', result);

      console.log('layout', this.LayoutsArr)
    })
  }

  // open add child child menu dialog

  openMenuChildChild(root, child) {


    const dialogRef = this.matDialog.open(AddCcMenuComponent,
      {
        width: "420px",
        data: { root: root._id, child: child._id }
      })
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRestoMenu()
    })
  }

  // open add sub child menu dialog
  openMenuChild(root, cat) {
    let root_id = root._id
    let cat_id = cat._id
    const dialogRef = this.matDialog.open(AddSubcatMenuComponent,
      {
        width: '420px',
        data: { 'root': root_id, 'cat': cat_id }
      })
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRestoMenu()
    })
  }

  // open add category menu dialog
  openMenuCate(root) {
    let root_id = root._id
    const dialogRef = this.matDialog.open(AddCateMenuComponent,
      {
        width: '420px',
        data: root_id
      })
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRestoMenu()
    })
  }
  // open add root menu dialog
  openAddRootMenu() {
    const dialogRef = this.matDialog.open(AddRootMenuComponent, {
      width: '420px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRestoMenu()
    })
  }

  // on FINAL SUBMIT
  onlayoutFinalSubmit() {
    // this.RestoId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.PutLayoutFinalSubmit(this.RestoId, this.LayoutsArr).subscribe(res => {
      if (res['sucess']) {
        Swal.fire('Restaurant Finally Submitted', '', 'success')
        this.router.navigate(['/restaurants'])
      }
      else {
        Swal.fire('Failed to Added', '', 'warning')
      }
    })
  }

  // ON SECLECT LIQ;UOR cATEGORY
  onSelectRootDrink(root) {
    console.log('root', root._id);
    if (!this.selectedRootDrinksName.includes(root.rootDrinkName)) {
      this.selectedRootDrinksName.push(root.rootDrinkName)
    }
    else {
      this.selectedRootDrinksName.splice(this.selectedRootDrinksName.indexOf(root.rootDrinkName), 1)
    }

    console.log('select root drinks name', this.selectedRootDrinksName);

  }

  // ON Select Liqour VARIENT
  onSelectDrinkVarient(root, cat) {
    console.log('root', root._id);
    console.log('cat', cat._id);

    if (!this.selectedDrinkVarient.includes(cat.VarientName)) {
      this.selectedDrinkVarient.push(cat.VarientName)
    }
    else {
      this.selectedDrinkVarient.splice(this.selectedDrinkVarient.indexOf(cat.VarientName), 1)
    }

    console.log('select root drinks name', this.selectedDrinkVarient);

  }

  // ON SELECT LIQOUR CHILD
  onSelectDrinkSubVarient(root, cat, child) {
    console.log('root', root._id);
    console.log('cat', cat._id);
    console.log('child', child._id);

    if (!this.selectedDrinkSubVarient.includes(child.SubVarientName)) {
      this.selectedDrinkSubVarient.push(child.SubVarientName)
    }
    else {
      this.selectedDrinkVarient.splice(this.selectedDrinkSubVarient.indexOf(child.SubVarientName), 1)
    }

    console.log('select root drinks name', this.selectedDrinkSubVarient);
  }

  // ON SELECT Liquor cc
  onSelectDrinkSubSub(root, cat, child, cc) {
    console.log('root', root._id);
    console.log('cat', cat._id);
    console.log('child', child._id);
    console.log('cc', cc);

    if (!this.selectedDrinkSuperChild.includes(cc.SubSubVarientName)) {
      this.selectedDrinkSuperChild.push(cc.SubSubVarientName)
    }
    else {
      this.selectedDrinkVarient.splice(this.selectedDrinkSuperChild.indexOf(cc.SubSubVarientName), 1)
    }

    console.log('select root drinks name', this.selectedDrinkSuperChild);

  }

  // ON SELECET MENU ROOT
  onSelectMenuRoot(root) {
    console.log('menu root id', root._id);

    if (!this.selectedMenuRoot.includes(root.rootCategoryName)) {
      this.selectedMenuRoot.push(root.rootCategoryName)
    }
    else {
      this.selectedMenuRoot.splice(this.selectedMenuRoot.indexOf(root.rootCategoryName), 1)
    }
    console.log('selected root menu', this.selectedMenuRoot);

  }
  // ON SELECT MENU CATEGORY
  onSelectMenuCat(root, cat) {
    console.log('menu root id', root._id)
    console.log('menu cat._id', cat._id);

    if (!this.selectedMenuCategory.includes(cat.categoryName)) {
      this.selectedMenuCategory.push(cat.categoryName)
    }
    else {
      this.selectedMenuCategory.splice(this.selectedMenuCategory.indexOf(cat.categoryName), 1)
    }

    console.log('Selected Menu Category', this.selectedMenuCategory);

  }

  // ON SELECT MENU CHILD
  onSelectMenuChild(root, cat, child) {
    console.log('menu root id', root._id)
    console.log('menu cat._id', cat._id);
    console.log('menu child._id', child._id);

    if (!this.selectedChildCategory.includes(child.childCategoryName)) {
      this.selectedChildCategory.push(child.childCategoryName)
    }
    else {
      this.selectedChildCategory.splice(this.selectedChildCategory.indexOf(child.childCategoryName), 1)
    }

    console.log("selected child category", this.selectedChildCategory);


  }

  // ON select Menu CC
  onSelectMenuCC(root, cat, child, cc) {
    console.log('menu root id', root._id)
    console.log('menu cat._id', cat._id);
    console.log('menu child._id', child._id);
    console.log('menu cc id', cc._id);

    if (!this.selectedMenucc.includes(cc.childChildCategoryName)) {
      this.selectedMenucc.push(cc.childChildCategoryName)
    }
    else {
      this.selectedMenucc.splice(this.selectedMenucc.indexOf(cc.childChildCategoryName), 1)
    }

    console.log('selected menu child child', this.selectedMenucc);


  }

  //  On sAVE MENU PAGE
  onMenuSave() {
    console.log("selected menu root", this.selectedMenuRoot)
    console.log("selected menu category", this.selectedMenuCategory)
    console.log("selected menu child", this.selectedChildCategory)
    console.log("selected menu root", this.selectedMenucc)


    var menuObj = {
      rootCategories: this.selectedMenuRoot,
      categories: this.selectedMenuCategory,
      childCategories: this.selectedChildCategory,
      childchildCategories: this.selectedMenucc
    }

    console.log('resto id', this.RestoId);
    console.log('menuObj', menuObj);

    // posting final menu
    this.restServ.putSelectedMenu(this.RestoId, menuObj).subscribe(res => {
      console.log('response', res);

    })


  }
  // On Select ROOT

  onSelectRoot(root, evt) {
    // console.log("root", root._id, evt);


    console.log('log', root.rootCategoryName);

    if (!this.selectedRootNames.includes(root.rootCategoryName)) {
      this.selectedRootNames.push(root.rootCategoryName)
    }
    else {
      this.selectedRootNames.splice(this.selectedRootNames.indexOf(root.rootCategoryName), 1)
    }

    console.log('Selected Root Names', this.selectedRootNames);
  }

  // on Select Category()
  onSelectCategory(root, cat) {
    console.log("root", root._id)
    console.log("cat", cat._id);


    if (!this.selectedCatNames.includes(cat.categoryName)) {
      this.selectedCatNames.push(cat.categoryName)
    }
    else {
      this.selectedCatNames.splice(this.selectedCatNames.indexOf(cat.categoryName, 1))
    }

    console.log('Selected Categories Names', this.selectedCatNames);


  }

  // On Select Child  
  onSelectChildCat(root, cat, child) {
    console.log("root", root._id)
    console.log("cat", cat._id);
    console.log("child", child._id);

    if (!this.selectedChildName.includes(child.childCategoryName)) {
      this.selectedChildName.push(child.childCategoryName)
    }
    else {
      this.selectedChildName.splice(this.selectedChildName.indexOf(child.childCategoryName), 1)
    }

    console.log('selected Childs names', this.selectedChildName)

  }


  // On Select Child Child
  onSelectChildChild(root, cat, child, cc) {
    console.log("root", root._id)
    console.log("cat", cat._id);
    console.log("child", child._id);
    console.log("cc", cc._id);

    if (!this.selectedChildChildName.includes(cc.childChildCategoryName)) {
      this.selectedChildChildName.push(cc.childChildCategoryName)
    }
    else {
      this.selectedChildChildName.splice(this.selectedChildChildName.indexOf(cc.childChildCategoryName), 1)

    }

    console.log('selected child child names', this.selectedChildChildName);





  }
  // Layout Function

  addLayout() {

    // this.LayoutsArr.push('new  '+this.layoutCount)
    // this.LayoutsArr.push({title: 'new', table: 'abc'})
    // console.log('Layout after Add', this.LayoutsArr)

    const dialogRef = this.matDialog.open(AddLayoutComponent, {
      width: '400px',
      data: this.LayoutsArr
    }
    )

    dialogRef.afterClosed().subscribe(result => {
      console.log('layout', this.LayoutsArr)
    })
  }

  deleteLayout(layout) {
    this.LayoutsArr.splice(this.LayoutsArr.indexOf(layout), 1)
    console.log('Layout after delete', this.LayoutsArr);

  }

  // openAddTableDialog(a){
  //   console.log('table',a)
  //   const dialogRef = this.matDialog.open(,
  //     width: '400px',
  //     )
  // }
  //get FORM ARRAY OF DOCUMENTS NAME

  get docFormArray() {
    return this.LegalForm.controls.restaurant_legal_document as FormArray;
  }

  // adding form to it

  addDocArray() {
    ++this.docsCount
    this.docs = this.fb.group({
      documentName: [''],
      documentNumber: [''],
      documentUrl: ['']
    })
    //  console.log(this.docs.value);

    if (this.docsCount <= 4) {
      this.docFormArray.push(this.docs)
    }
    else {
      Swal.fire('You have already Added 4 Docs', '', 'warning')
    }

  }

  //  Remove OTHER IMAGE

  removeother() {
    this.otherResponse = []
    this.isOtherTrash = true
  }
  // Remove GST IMAGE
  removegst() {
    this.gstResponse = ''
    this.isgstTrash = true
    // this.LegalForm.controls['restaurant_gst_no'].reset()
  }

  // Remove PAN IMAGE
  removePan() {
    this.panResponse = ''
    this.ispanTrash = true
    // this.LegalForm.controls['restaurant_pan_no'].reset()
  }

  // Remove Fssai Image
  removeFssai() {
    this.fssaiResponse = ''
    this.isFssaiTrash = true
    // this.LegalForm.controls['restaurant_fssai_certificate'].reset()
  }
  // On Check Root
  oncheckRoot(evt) {
    console.log('chk', evt);

  }

  // *******CARET CLCIK





  // ***********ESTABLISHMENT DROP DOWN CODE******************

  // onRootCATEGORYSelections

  onRootCategorySelection(evt) {
    let a = evt.target.selectedIndex

    let singlecategoryName = this.estRootCategory[a - 1]

    this.categoryName = singlecategoryName['categories']

    console.log('categories Names', this.categoryName);



  }

  // onCategorySelection

  onCategorySelection(evt) {
    let idx = evt.target.selectedIndex

    let sinsubCategory = this.categoryName[idx - 1]

    console.log('sing sub category', sinsubCategory);

    this.childCategory = sinsubCategory['childCategories']

  }

  // on selecTION CHILD

  onSelectChild(evt) {
    let a = evt.target.selectedIndex

    let singleChild = this.childCategory[a - 1]

    this.subsubChild = singleChild['childChildCategories']

    console.log('sub sub child', this.subsubChild);

  }


  // ***********end of ESTABLISHMENT DROP DOWN CODE******************

  // *******GET ESTABLISHMENT ROOT TYPE*********
  getEstRootType() {
    this.restServ.getRootCategory().subscribe(data => {
      this.estRootCategory = data['data']
      console.log('estRootCategory', this.estRootCategory);

    })
  }


  // *******************Get All Resto Menu***************
  getAllRestoMenu() {
    this.restServ.getAllRootMenu().subscribe(data => {
      this.AllRootMenu = data['data']
      console.log('all root menu', this.AllRootMenu);

    })
  }

  // *********LIQUOR RESTO INFO code ****************

  // getting all root drink types   
  getRootDrinkTypes() {

    this.restServ.getALlLiquorTree().subscribe(data => {
      console.log('get all drinks', data);
      this.DrinkTypes = data['data']
    })
  }
  // ****** click carrot function**********

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  // *******get resto type API

  getAllRestoType() {
    this.restServ.getAllRestoType().subscribe(data => {
      this.restotype = data['data']
    })
  }

  // get function REVENUE
  getRevenue() {
    this.restServ.getAllRevenue().subscribe(data => {
      this.Revenue = data['data']

    })
  }
  // ON SAVE LAYOUTS FORM
  onSaveLayouts() {

    this.FinalLayoutObj = {
      "layout": [
        {
          "tabname": "Rooftop",
          "tab_layouts": [
            {
              "table_name": "Table 1",
              "seating_capacity": "4",
              "assests": {
                "chairs": [
                  "chair 1",
                  "chair 2"
                ],
                "walls": [
                  "side",
                  "front"
                ],
                "doors": [
                  "enter",
                  "exit"
                ],
                "billing_counter": [
                  "counter 1",
                  "couter 2"
                ]
              }
            }
          ]
        }
      ]
    }


    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: this.estobj,
      legalInfo: this.finalLegalObj,
      media: this.finalMediaObj,
      menuManagement: this.finalMenuMgmt,
      layout: this.FinalLayoutObj
    }

    this.objectvalue = obj
    console.log('final obj is here', obj)

  }
  //ON SAVE MENU FORM
  onLiquorSave() {


    // this.finalMenuMgmt = {
    //   cuisines: this.cuisinesArr,
    //   menu: this.submenu,
    //   liquor: this.liquormenu
    // }

    // let obj = {
    //   restoInfo: this.finalRestObj,
    //   establishmentType: this.estobj,
    //   legalInfo: this.finalLegalObj,
    //   media: this.finalMediaObj,
    //   menuManagement: this.finalMenuMgmt
    // }

    // console.log("after menu tab completion", obj);


    // this.objectvalue = obj;

    console.log('selected ROOT', this.selectedRootDrinksName);
    console.log('selected VARIENT', this.selectedDrinkVarient);
    console.log('selected SUB VARIENT', this.selectedDrinkSubVarient);
    console.log('selected SUPER CHILD', this.selectedDrinkSuperChild);

    var liquorObj = {
      rootVarients: this.selectedRootDrinksName,
      Varients: this.selectedDrinkVarient,
      SubVarients: this.selectedDrinkSubVarient,
      SubSubVarients: this.selectedDrinkSuperChild
    }
    this.restServ.putSelectedLiquor(this.RestoId, liquorObj).subscribe(res => {
      console.log('Response Liquor object', res);
      if (res['sucess']) {
        Swal.fire('Added Liquor Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }
    })
  }


  //ON SAVE MEDIA FORM
  onSaveMediaForm() {

    let banner = ['abc', 'xyz', 'pqr']
    var img = [
      {
        name: 'parking',
        fileurl: ['abc', 'xyz', 'pqr']
      },
      {
        name: 'pub',
        fileurl: ['pqr', 'xyz', 'mno']
      }
    ]

    var vdo = [
      {
        name: 'indoor',
        fileurl: ['pqr', 'xyz', 'mno']
      },
      {
        name: 'rooftop',
        fileurl: ['abc', 'xyz', 'pqr']
      }
    ]

    this.finalMediaObj = {
      resto_logo: this.logoResponse,
      resto_banner: this.BannerResponse,
      album_image: [{
        name: this.albumNames[0],
        fileurl: this.ImgAlbumResponse
      }],
      // album_video: vdo
    }


    console.log(
      "MEDIA FORM", this.finalMediaObj
    );

    // this.restServ.PutMediaDataResto(this.RestoId,this.finalMediaObj).subscribe(res=>{
    //   console.log('response',res);

    // })

    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: this.estobj,
      legalInfo: this.finalLegalObj,
      media: this.finalMediaObj
    }



  }
  onSubmitMedia() {
    // this.RestoId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.PutMediaDataResto(this.RestoId, this.finalMediaObj).subscribe(res => {
      console.log(res);
      if (res['sucess']) {
        Swal.fire('Media Added Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Added', '', 'warning')
      }

    })
  }

  // ON SAVE BANK INFO
  onSaveBankInfo() {
    console.log('Bank Info Save', this.LegalForm.value);

    // this.LegalForm.patchValue({
    //   restaurant_gst_no: this.gstResponse,
    //   restaurant_pan_no: this.panResponse,
    //   restaurant_fssai_certificate: this.fssaiResponse

    // })


    this.finalLegalObj = this.LegalForm.value

    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: this.estobj,
      legalInfo: this.finalLegalObj
    }

    // console.log('json after legal', obj);

    var finalLegalFormObject = {
      "accountNo": this.LegalForm.controls['restaurant_account_no'].value,
      "accountType": this.LegalForm.controls['restaurant_account_type'].value,
      "bankName": this.LegalForm.controls['restaurant_bank_name'].value,
      "branchName": this.LegalForm.controls['restaurant_branch_name'].value,
      "city": this.LegalForm.controls['restaurant_bank_city'].value,
      "address": this.LegalForm.controls['restaurant_bank_address'].value,
      "IFSCCode": this.LegalForm.controls['restaurant_bank_ifsccode'].value,
      "documents": [{
        "documentName": "GST",
        "documentNumber": this.LegalForm.controls['restaurant_gst_no'].value,
        "documentUrl": this.LegalForm.controls['restaurant_gst_image'].value
      },
      {
        "documentName": "PAN",
        "documentNumber": this.LegalForm.controls['restaurant_pan_no'].value,
        "documentUrl": this.LegalForm.controls['restaurant_pan_image'].value
      },
      {
        "documentName": "FSSAI",
        "documentNumber": this.LegalForm.controls['restaurant_fssai_certificate'].value,
        "documentUrl": this.LegalForm.controls['restaurant_fssai_image'].value
      }],
      "otherDocuments": this.LegalForm.controls['restaurant_legal_document'].value
    }

    // console.log('only otherdocuments', this.LegalForm.controls['restaurant_legal_document'].value);


    console.log('resto legal form', finalLegalFormObject);

    // this.RestoId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.PutLegalDataResto(this.RestoId, finalLegalFormObject).subscribe(res => {
      console.log('Response', res);
      if (res['sucess']) {
        Swal.fire('Legal Data Added Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Added', '', 'warning')
      }

    })

    this.checkobj = finalLegalFormObject
  }


  //ON SAVE AMBIENCES
  onSaveAmbience() {

    // this.RestoId = '5e3c2bc9a588df0cb1f6d323' 
    console.log('ambience ifno', this.selectedAmbienceInfo)
    this.restServ.putAmbiencesToResto(this.RestoId, this.selectedAmbienceInfo).subscribe(res => {
      console.log('response after adding ambiences', res);
      if (res['sucess']) {
        Swal.fire('Ambiences Added Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }

    })
    this.finalAmbienceObj = [
      {
        ambience_name: 'Beach',
        selected_type: this.selectedAmbience
      },
      {
        ambience_name: 'Lawn',
        selected_type: this.selectedAmbience
      }
    ]
    this.estobj =
    {
      est: this.FinalEstObj,
      more_info: this.finalMoreInfobj,
      services: this.finalServicesobj,
      rules: this.finalRulesobj,
      ambience: this.finalAmbienceObj
    }

    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: this.estobj,

    }

    console.log('object final RULES AND REGULARIONT', obj);
  }

  //ON SAVE RULES & REGULATIONS
  onSaveRules() {


    // this.RestoId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.putRulesToResto(this.RestoId, this.selectedRulesInfo).subscribe(res => {
      console.log('resposne save rules to resto', res);
      if (res['sucess']) {
        Swal.fire('Rules added Successfully', '', 'success')
      }
      else {
        Swal.fire(
          'Failed to add', '', 'warning'
        )
      }

    })

    this.finalRulesobj = [
      {
        rule_name: 'Restriction',
        selected_type: this.selectedRules
      },
      {
        rule_name: 'Policy',
        selected_type: this.selectedRules
      }
    ]

    let estobj =
    {
      est: this.FinalEstObj,
      more_info: this.finalMoreInfobj,
      services: this.finalServicesobj,
      rules: this.finalRulesobj
    }

    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: estobj,

    }

    console.log('object final RULES AND REGULARIONT', obj);
  }
  // ON SAVE SERVICES
  onSaveServices() {

    // this.RestoId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.putServicesToResto(this.RestoId, this.selectedServices).subscribe(res => {
      console.log('Services saved to db status', res);
      if (res['sucess']) {
        Swal.fire('Services Added Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }

    })


    this.finalServicesobj = [
      {
        service_name: 'Bar',
        selected_type: this.selectedServices
      },
      {
        service: 'Pub',
        selected_type: this.selectedServices
      }
    ]
    let estobj =
    {
      est: this.FinalEstObj,
      more_info: this.finalMoreInfobj,
      services: this.finalServicesobj
    }

    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: estobj,

    }

    console.log('object final services', obj);


  }

  //ON SAVE MORE INFO
  onSaveMoreInfo() {

    // this.RestoId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.putMoreInfoToResto(this.RestoId, this.selectedMoreInfos).subscribe(res => {
      console.log('Response more Infos to db', res);
      if (res['sucess']) {
        Swal.fire('More Infos Added Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }
    })

    this.finalMoreInfobj = [
      {
        moreinfo_name: 'Bar',
        selected_type: this.selectedBars
      },
      {
        moreinfo_name: 'Pub',
        selected_type: this.selectedPubs
      }
    ]

    let estobj =
    {
      est: this.FinalEstObj,
      more_info: this.finalMoreInfobj
    }

    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: estobj,

    }


    console.log('show details', obj);

  }
  //ON SAVE ESTABLSIMENT TYPE
  onSaveEstType() {
    var estCategories = {
      rootCategories: this.selectedRootNames,
      categories: this.selectedCatNames,
      childCategories: this.selectedChildName,
      childchildCategories: this.selectedChildChildName
    }



    console.log('estCategories schema', estCategories);


    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: estCategories
    }

    console.log('obj after establsihment type', obj);

    // 5e40f82bf177a934fb971342
    // this.restServ.putEstablishType(this.RestoId, estCategories).subscribe(response =>{
    this.restServ.putEstablishType(this.RestoId, estCategories).subscribe(response => {
      console.log('establisment response', response);

      if (response['sucess']) {
        Swal.fire('Establishments Added', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }
    })



  }



  //ON SAVE CUSINE FORM
  onSaveCuisinesForm() {
    this.cuisinesObj = this.selectedCuisines
    let obj = {
      restoInfo: this.finalRestObj,
      legalInfo: this.finalLegalObj,
      media: this.finalMediaObj,
      cuisines: this.cuisinesObj
    }

    console.log(
      "Selected Cuisines", this.selectedCuisinesName
    );


    this.restServ.putSelectedCuisines(this.RestoId, this.selectedCuisinesName).subscribe(res => {
      console.log('response add selected cuisines', res);

      if (res['sucess']) {
        Swal.fire('Cuisines Added Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }
    })


  }

  // ON SAVE RESTO INFO
  onSaveRestoInfo() {

    // ADD RESTAURABTS contact with country code: 

    // this.restaurants_contact = this.RestaurantInfo.controls['restaurant_country_code'].value + this.RestaurantInfo.controls['restaurant_contact'].value

    // FINAL OBJECT FOR RESTO OBJECT
    var Restobj = {
      restoLegalName: this.RestaurantInfo.controls['legal_name'].value,
      restoName: this.RestaurantInfo.controls['restaurant_name'].value,
      // restaurant_contact: this.RestaurantInfo.controls['restaurant_contact'].value,
      restoContactNo: this.RestaurantInfo.controls['restaurant_contact'].value,
      restoEmail: this.RestaurantInfo.controls['restaurant_email'].value,
      restoAddress: this.RestaurantInfo.controls['restaurant_address'].value,
      restoLocality: this.RestaurantInfo.controls['restaurant_locality'].value,
      restoPincode: this.RestaurantInfo.controls['restaurant_pincode'].value,
      restoRevenue: this.RestaurantInfo.controls['restaurant_revenue'].value,
      restoType: this.restaurants_types,
      restoWebsite: this.RestaurantInfo.controls['restaurant_website'].value,
      restoRepresentativeName: this.RestaurantInfo.controls['representative_name'].value,
      restoRepresentativeContactNo: this.RestaurantInfo.controls['representative_contact'].value,
      restoRepresentativeEmail: this.RestaurantInfo.controls['representative_email'].value,
      // restoHours: this.RestaurantInfo.controls['representative_email'].value,

      restoHours: [
        {
          day: 'monday',
          open: this.RestaurantInfo.controls['openMon'].value,
          close: this.RestaurantInfo.controls['closeMon'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenMon'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseMon'].value

        },
        {
          day: 'tuesday',
          open: this.RestaurantInfo.controls['openTue'].value,
          close: this.RestaurantInfo.controls['closeTue'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenTue'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseTue'].value

        },
        {
          day: 'wednesday',
          open: this.RestaurantInfo.controls['openWed'].value,
          close: this.RestaurantInfo.controls['closeWed'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenWed'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseWed'].value

        },
        {
          day: 'thursday',
          open: this.RestaurantInfo.controls['openThur'].value,
          close: this.RestaurantInfo.controls['closeThur'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenThur'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseThur'].value

        },
        {
          day: 'friday',
          open: this.RestaurantInfo.controls['openFri'].value,
          close: this.RestaurantInfo.controls['closeFri'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenFri'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseFri'].value

        },
        {
          day: 'saturday',
          open: this.RestaurantInfo.controls['openSat'].value,
          close: this.RestaurantInfo.controls['closeSat'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenSat'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseSat'].value

        },
        {
          day: 'sunday',
          open: this.RestaurantInfo.controls['openSun'].value,
          close: this.RestaurantInfo.controls['closeSun'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenSun'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseSun'].value

        }
      ]
    }


    console.log('resto schema', Restobj);
    this.finalRestObj = Restobj

    // **********POST SERVICE FOR RESTO INFO***************



    this.restServ.putRestoInfo(this.RestoId, Restobj).subscribe(res => {
      console.log('response', res);

    })


  }


  addTab(t) {

    console.log(t);

    this.tabs = t;


    this.selected.setValue(this.tabs.length - 1);

  }

  removeTab(index: number) {
    this.tabs.splice(index, 1);
  }


  //ON CHECK CUISINES 

  onChkCuisines(s) {

    if (this.allCuisines.includes(s)) {
      this.allCuisines.splice(this.allCuisines.indexOf(s), 1)
      this.selectedCuisines.push(s)
      this.selectedCuisinesName.push(s.cuisineName)
    }
  }

  //ON REMOVE CUISINES

  removeCuisines(s) {
    if (this.selectedCuisines.includes(s)) {
      this.selectedCuisines.splice(this.selectedCuisines.indexOf(s), 1)
      this.selectedCuisinesName.splice(this.selectedCuisinesName.indexOf(s.cuisineName), 1)
      this.allCuisines.push(s)
    }
  }




  // after click on sameday 
  onSameAsMonday(chk) {

    console.log('checked vlaue', chk)
    if (this.monAddHr) {
      this.tueAddHr = true;
      this.wedAddHr = true;
      this.thurAddHr = true;
      this.friAddHr = true;
      this.satAddHr = true;
      this.sunAddHr = true;
    }

    // TIME VALUE SAME AS MONDAY

    this.mondayOpenTime = this.RestaurantInfo.controls['openMon'].value
    this.mondayCloseTime = this.RestaurantInfo.controls['closeMon'].value

    console.log('Extra open monday vlaue', this.RestaurantInfo.controls['extraopenMon'].value);
    this.extramonOpenTime = this.RestaurantInfo.controls['extraopenMon'].value
    this.extamonCloseTime = this.RestaurantInfo.controls['extracloseMon'].value


    console.log('tIMES values ', this.mondayOpenTime);

    this.RestaurantInfo.patchValue({
      openTue: this.mondayOpenTime,
      openWed: this.mondayOpenTime,
      openThur: this.mondayOpenTime,
      openFri: this.mondayOpenTime,
      openSat: this.mondayOpenTime,
      openSun: this.mondayOpenTime,

      extraopenTue: this.extramonOpenTime,
      extraopenWed: this.extramonOpenTime,
      extraopenThur: this.extramonOpenTime,
      extraopenFri: this.extramonOpenTime,
      extraopenSat: this.extramonOpenTime,
      extraopenSun: this.extramonOpenTime,


      closeTue: this.mondayCloseTime,
      closeWed: this.mondayCloseTime,
      closeThur: this.mondayCloseTime,
      closeFri: this.mondayCloseTime,
      closeSat: this.mondayCloseTime,
      closeSun: this.mondayCloseTime,

      extracloseTue: this.extamonCloseTime,
      extracloseWed: this.extamonCloseTime,
      extracloseThur: this.extamonCloseTime,
      extracloseFri: this.extamonCloseTime,
      extracloseSat: this.extamonCloseTime,
      extracloseSun: this.extamonCloseTime

    })




    //RADIO SAME AS MONDAY
    if (chk.target.checked) {

      this.isTues = this.isMon;
      this.isWed = this.isMon;
      this.isThur = this.isMon;
      this.isFri = this.isMon;
      this.isSat = this.isMon;
      this.isSun = this.isMon;
    }

    else {
      this.isTues = false;
      this.isWed = false;
      this.isThur = false;
      this.isFri = false;
      this.isSat = false;
      this.isSun = false;
    }

  }
  showtabName(t) {
    console.log('tabnaem', t);

    this.mainService.tabTableName = t
    console.log(
      'the TabName', this.mainService.tabTableName
    );

  }




  //******Open add Revenue Dialog

  openAddRevenueDialog() {

    const dialogRef = this.matDialog.open(AddRevenueComponent, {
      width: '400px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'After closing Dialog'
      );
      this.getRevenue();
    })
  }


  // ******************Open add Resto Type**************

  openRestoTypeDialog() {

    const dialogRef = this.matDialog.open(AddRestoTypeComponent, {
      width: '400px'
    })

    dialogRef.afterClosed().subscribe(addRestoRes => {

      console.log('after closing RestoTYpe Dialog');
      this.getAllRestoType()
    })
  }


  // Open add table dialog
  openTableDialog(a): void {

    console.log('a', a);

    const dialogRef = this.matDialog.open(OpenTableComponent, {
      width: '300px',
      data: a
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      console.log('layout', this.LayoutsArr)
    });

  }


  //  open add layout dialog

  openAddLayoutDilog(): void {
    const dialogRef = this.matDialog.open(AddLayoutComponent, {
      width: '540px',
      height: '440px',
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (this.mainService.layoutName !== undefined) {
        this.tabNames = this.mainService.layoutName
        this.addTab(this.tabNames);

        // this.animal = result;
        this.mainService.selectedAlbums = this.tabNames;
      }
    });

  }

  // open DIalog media only for images
  openMedialog() {
    const dialogRef = this.matDialog.open(CreateAlbumComponent, {
      width: '540px',

    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (this.mainService.popMediaAddData != undefined) {
        // this.albumNames.push(this.mainService.popMediaAddData);


        this.albumNames = this.mainService.popMediaAddData;

        this.isAlbum = false;
        console.table("ALUBM HERE", this.albumNames)
        this.mainService.selectedAlbums = this.albumNames;
      }


    });



  }



  // open video dialog album
  openVdoAlbum() {
    const dialogRef = this.matDialog.open(CreateVdoAlbumComponent, {
      width: '540px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result form videos create album', result);
      // this.createdVdoAlbum.push(result)
      // console.log('created video album', this.createdVdoAlbum);

      if (this.mainService.popVideoAddData != undefined) {
        // this.albumNames.push(this.mainService.popMediaAddData);


        this.createdVdoAlbum = this.mainService.popVideoAddData;

        this.isAlbum = false;
        console.table("ALUBM HERE", this.createdVdoAlbum)
        this.mainService.selectedVideoAlbums = this.createdVdoAlbum;
      }

    })
  }

  // open dialog add root category code

  openAddCateg(): void {
    const dialogRef = this.matDialog.open(EstAddCatComponent, {
      width: '540px',

    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.getEstRootType()
    });


  }

  // open category add category name

  openCategoryName(root) {

    console.log('current root', root);
    let root_id = root._id

    const dialogRef = this.matDialog.open(AddCatoNameComponent, {
      width: '540px',
      data: root_id
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('category name closed', result);
      this.getEstRootType()

    })

  }

  // AddSubCatComponent, AddSubSubCatComponent

  // open child category 
  openSubCategory(root, cat) {
    console.log('root details', root._id);

    console.log('category details', cat._id);

    const dialogRef = this.matDialog.open(AddSubCatComponent, {
      width: '540px',
      data: { 'root': root._id, "cat": cat._id }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.getEstRootType()
    })
  }


  // open child child category 
  openSubSubCategory(root, child) {

    // console.log('root', root._id);
    // console.log('child', child._id)

    const dialogRef = this.matDialog.open(AddSubSubCatComponent, {
      width: '540px',
      data: { 'root': root._id, 'child': child._id }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.getEstRootType()
    })
  }
  // /////////
  //open sub category
  // /////////
  opensubCateg(): void {
    const dialogRef = this.matDialog.open(SubCatComponent, {
      width: '580px',
      height: '440px',
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (this.mainService.popSubCategData !== undefined) {
        this.popSub = this.mainService.popSubCategData
        console.log('sub catergory data form popOver', this.popSub);
        console.table('sub catergory data form popOver', this.popSub);
      }
      // this.animal = result;
    });


  }

  // OPEN ATTRIBUTE CATEGORY

  openattr(): void {
    const dialogRef = this.matDialog.open(AttrCatComponent, {
      width: '580px',
      height: '480px',
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (this.mainService.popAtrrCategData !== undefined) {
        this.popAttr = this.mainService.popAtrrCategData
        console.log('attr catergory data form popOver', this.popAttr);
        console.table('attr data form popOver', this.popAttr);
      }
      // this.animal = result;
    });


  }



  onEstNext(next) {
    this.dummy = next
  }

  onMenuNext(next) {
    console.log(this.output);
    this.menuNav = next
  }

  // Open city Navbar 
  openCity(evt, cityName) {

    this.dummy = cityName
    console.log('dummt', this.dummy);

    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace("active", "");
    }
    // document.getElementById(cityName).style.display = "block";
    // evt.currentTarget.className += "active";
  }

  // // Get the element with id="defaultOpen" and click on it

  //GET BARMOREINFO SELECTIONS

  onCheckMat(e) {
    console.log(
      "event value here", e
    );

    if (this.barMoreInfo.includes(e)) {
      this.barMoreInfo.splice(this.barMoreInfo.indexOf(e), 1);
      this.selectedBars.push(e);
    }
    // this.ischecked = e.checked



  }

  // REMOVE BAR MORE INFOS
  removeBars(s) {
    if (this.selectedBars.includes(s)) {
      this.selectedBars.splice(this.selectedBars.indexOf(s), 1);
      this.barMoreInfo.push(s)
    }
  }

  // ON SELECTD MORE INFO PUBS
  onCheckPubs(s) {
    if (this.pubMoreInfo.includes(s)) {
      this.pubMoreInfo.splice(this.pubMoreInfo.indexOf(s), 1)
      this.selectedPubs.push(s);
    }
  }


  // ON REMOVE PUB MORE INFOS
  removePubs(s) {
    if (this.selectedPubs.includes(s)) {
      this.selectedPubs.splice(this.selectedPubs.indexOf(s), 1)
      this.pubMoreInfo.push(s);
    }
  }

  // ON MOREINFO chips
  onCheckMoreInfo(a, b) {
    if (!this.dupChkMoreInfo.includes(b)) {
      this.dupChkMoreInfo.push(b)
      this.selectedMoreInfos.push({ title: a, moreinfo: b })
      console.log('selected moreinfos', this.selectedMoreInfos)
    }
    else {
      alert('Already Added!!')
    }

  }

  // On Remove MoreInfo 
  removeMoreInfo(s) {
    if (this.selectedMoreInfos.includes(s)) {
      this.selectedMoreInfos.splice(this.selectedMoreInfos.indexOf(s), 1)
      this.dupChkMoreInfo.splice(this.dupChkMoreInfo.indexOf(s.moreinfo), 1)
    }
  }


  // on CHECK SERVICES

  onCheckService(a, b) {


    if (!this.dupChkServ.includes(b)) {
      this.dupChkServ.push(b)
      this.selectedServices.push({ title: a, service: b })
      console.log('selecected services', this.selectedServices)
    }
    else {
      alert('Already Added!!')
    }



  }

  // on REMOVE MORE INFO SERVICES

  removeServices(s) {
    if (this.selectedServices.includes(s)) {
      this.selectedServices.splice(this.selectedServices.indexOf(s), 1)
      this.dupChkServ.splice(this.dupChkServ.indexOf(s.service), 1)
    }

  }


  //on Check Rules more info
  onCheckRules(a, b) {

    if (!this.dupChkRules.includes(b)) {
      this.dupChkRules.push(b)
      this.selectedRulesInfo.push({ title: a, rule: b })
      console.log('selecected Rules', this.selectedRulesInfo)
    }
    else {
      alert('Already Added!!')
    }
  }

  // on Remove RUles from More Info
  removeRules(s) {
    if (this.selectedRulesInfo.includes(s)) {
      this.selectedRulesInfo.splice(this.selectedRulesInfo.indexOf(s), 1)
      this.dupChkRules.splice(this.dupChkRules.indexOf(s.rule), 1)
    }

    console.log('selected RULES', this.selectedRulesInfo)
  }


  // CHECK AMBIENCE MORE INFO

  onCheckAmbience(a, b) {


    if (!this.dupChkAmbience.includes(b)) {
      this.dupChkAmbience.push(b)
      this.selectedAmbienceInfo.push({ title: a, ambiences: b })
      console.log('selecected Ambiences', this.selectedAmbienceInfo)
    }
    else {
      alert('Already Added!!')
    }
  }

  /// remove AMABIEANCE MORE INFO
  removeAmbience(s) {
    // if (this.selectedAmbience.includes(s)) {
    //   this.selectedAmbience.splice(this.selectedAmbience.indexOf(s), 1)
    //   this.ambienceMoreInfo.push(s);
    // }
    if (this.selectedAmbienceInfo.includes(s)) {
      this.selectedAmbienceInfo.splice(this.selectedAmbienceInfo.indexOf(s), 1)
      this.dupChkAmbience.splice(this.dupChkAmbience.indexOf(s.ambiences), 1)
    }

    console.log('selected RULES', this.selectedAmbienceInfo)
  }




  createNewElement(id) {
    // First create a DIV element.
    var txtNewInputBox = document.createElement('div');

    // Then add the content (a new input box) of the element.
    txtNewInputBox.innerHTML = "+<input type='time' value='16:00:00' > to <input type='time' value='23:00:00' >";

    // Finally put it where it is supposed to appear.
    if (id == 'mon') {
      // document.getElementById("monAddHTML").appendChild(txtNewInputBox);
      this.monAddHr = true;
    }

    if (id == 'tue') {
      // document.getElementById("tueAddHTML").appendChild(txtNewInputBox);
      this.tueAddHr = true;
    }

    if (id == 'wed') {
      // document.getElementById("wedAddHtml").appendChild(txtNewInputBox);
      this.wedAddHr = true;
    }

    if (id == 'thur') {
      // document.getElementById("thurAddHtml").appendChild(txtNewInputBox);
      this.thurAddHr = true;
    }

    if (id == 'fri') {
      // document.getElementById("friAddHtml").appendChild(txtNewInputBox);
      this.friAddHr = true;
    }

    if (id == 'sat') {
      // document.getElementById("satAddHtml").appendChild(txtNewInputBox);
      this.satAddHr = true;
    }

    if (id == 'sun') {
      // document.getElementById("sunAddHtml").appendChild(txtNewInputBox);
      this.sunAddHr = true;
    }
    //sunAddHtml

    console.log('Id ', id);


  }

  removeExtraTime(id) {
    if (id == 'mon') {
      // document.getElementById("monAddHTML").appendChild(txtNewInputBox);
      this.monAddHr = false;
    }

    if (id == 'tue') {
      // document.getElementById("tueAddHTML").appendChild(txtNewInputBox);
      this.tueAddHr = false;
    }

    if (id == 'wed') {
      // document.getElementById("wedAddHtml").appendChild(txtNewInputBox);
      this.wedAddHr = false;
    }

    if (id == 'thur') {
      // document.getElementById("thurAddHtml").appendChild(txtNewInputBox);
      this.thurAddHr = false;
    }

    if (id == 'fri') {
      // document.getElementById("friAddHtml").appendChild(txtNewInputBox);
      this.friAddHr = false;
    }

    if (id == 'sat') {
      // document.getElementById("satAddHtml").appendChild(txtNewInputBox);
      this.satAddHr = false;
    }

    if (id == 'sun') {
      // document.getElementById("sunAddHtml").appendChild(txtNewInputBox);
      this.sunAddHr = false;
    }
    //su
  }

  // Resto Info on submit function

  OnRestoInfo() {
    console.log(
      'Resto Info on submit value: ', this.RestaurantInfo.value
    );

    console.table(this.RestaurantInfo.value);




    // Establishment on Submit 



  }

  onChkRStype(v, evt) {

    let status = evt.target.checked

    if (status) {
      this.restaurants_types.push(v);
      console.log('restaurants types', this.restaurants_types);

    }
    else {
      this.restaurants_types.splice(this.restaurants_types.indexOf(v), 1)
      console.log('restaurants types', this.restaurants_types);

      // this.array.splice(i,1)

      // array = ['a','b']

      // let i = this.array.indexof('b')
    }
  }


  // ********************88
  // ******************
  // WEEK RADIO BUTTON TIMES 

  onMonday(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isMon = true;
    }
    else {
      this.isMon = false;
    }

  }

  onTuesday(evt) {
    if (evt.checked) {
      this.isTues = true;
    }
    else {
      this.isTues = false;
    }
  }

  onWednesday(evt) {
    if (evt.checked) {
      this.isWed = true;
    }
    else {
      this.isWed = false;
    }
  }

  onThursday(evt) {
    if (evt.checked) {
      this.isThur = true;
    }
    else {
      this.isThur = false;
    }
  }


  onFriday(evt) {
    if (evt.checked) {
      this.isFri = true;
    }
    else {
      this.isFri = false;
    }
  }

  onSaturday(evt) {
    if (evt.checked) {
      this.isSat = true;
    }
    else {
      this.isSat = false;
    }
  }

  onSunday(evt) {
    if (evt.checked) {
      this.isSun = true;
    }
    else {
      this.isSun = false;
    }
  }


  // TABLE RELATED FUNCITONS

  onSaveLayout(a) {
    console.log('Layout json object', this.LayoutsArr);
    this.tableCount++
    var obj = {
      tablename: 'table' + this.tableCount,
      capacity: '',
      shape: 'rectangle',
      // quantity: this.createTableForm.controls['quantity'].value
    }
    console.log(a.table.push(obj));



  }

  // onSave bill counter
  onSaveBillCounter(a) {


    this.billCount++

    var obj = {
      billcounterName: 'billcounter' + this.billCount
    }

    console.log(a.billing.push(obj));
    console.log('Layout json object', this.LayoutsArr);

  }

  // on save wall

  onSaveWall(a) {


    this.wallcount++

    var obj = {
      wall: 'wall' + this.wallcount
    }

    console.log(a.walls.push(obj));

    console.log('Layout json object', this.LayoutsArr);

  }

  // on save washroom

  onSaveWashroom(a) {
    this.washroomCount++

    var obj = {
      washroom: 'washroom' + this.washroomCount
    }

    console.log(a.washroom.push(obj))
    console.log('Layout json ', this.LayoutsArr);

  }
}
