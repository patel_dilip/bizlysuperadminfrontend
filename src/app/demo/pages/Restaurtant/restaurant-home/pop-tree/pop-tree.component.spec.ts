import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopTreeComponent } from './pop-tree.component';

describe('PopTreeComponent', () => {
  let component: PopTreeComponent;
  let fixture: ComponentFixture<PopTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
