import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-pop-tree',
  templateUrl: './pop-tree.component.html',
  styleUrls: ['./pop-tree.component.scss']
})
export class PopTreeComponent implements OnInit {

  treeForm: FormGroup
  value
  constructor(private fb: FormBuilder,  @Inject(MAT_DIALOG_DATA) public data) { 

    
  }

  ngOnInit() {

    this.treeForm = this.fb.group({
      label: ['', Validators.required]
    })
  }

  addLabel(){
    this.value = this.treeForm.controls.label.value
    console.log(this.value)
  }

}
