import { Component, OnInit } from '@angular/core';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-cinema',
  templateUrl: './view-cinema.component.html',
  styleUrls: ['./view-cinema.component.scss']
})
export class ViewCinemaComponent implements OnInit {

  view_data: any
  constructor(
    private restServ: RestoService,
    private router: Router
  ) { 

    this.view_data = this.restServ.view_cinema

    if(this.view_data == undefined){
      this.router.navigate(['/restaurants/cinema-home'])
    }

  }

  ngOnInit() {
  }

  editCinema(type){

    this.restServ.update_cinema = {type: type, data: this.view_data}
    this.router.navigate(['/restaurants/update-cinema'])
  }

}
