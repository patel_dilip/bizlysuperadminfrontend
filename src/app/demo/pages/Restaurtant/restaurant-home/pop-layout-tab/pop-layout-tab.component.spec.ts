import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopLayoutTabComponent } from './pop-layout-tab.component';

describe('PopLayoutTabComponent', () => {
  let component: PopLayoutTabComponent;
  let fixture: ComponentFixture<PopLayoutTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopLayoutTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopLayoutTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
