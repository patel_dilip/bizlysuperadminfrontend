import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-pop-layout-tab',
  templateUrl: './pop-layout-tab.component.html',
  styleUrls: ['./pop-layout-tab.component.scss']
})
export class PopLayoutTabComponent implements OnInit {

  createTab: FormGroup

  constructor(private fb: FormBuilder) {
    this.createTab = this.fb.group({
      title: ['', Validators.required],
      ambiences: ['', Validators.required]
    })
   }

  ngOnInit() {
  }

}
