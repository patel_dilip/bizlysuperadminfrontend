import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-pop-del-tree',
  templateUrl: './pop-del-tree.component.html',
  styleUrls: ['./pop-del-tree.component.scss']
})
export class PopDelTreeComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA)public data) { }

  ngOnInit() {
  }

}
