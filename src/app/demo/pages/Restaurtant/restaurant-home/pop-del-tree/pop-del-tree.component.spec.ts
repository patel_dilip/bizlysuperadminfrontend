import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopDelTreeComponent } from './pop-del-tree.component';

describe('PopDelTreeComponent', () => {
  let component: PopDelTreeComponent;
  let fixture: ComponentFixture<PopDelTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopDelTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopDelTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
