import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import Swal from 'sweetalert2';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-cinema',
  templateUrl: './add-cinema.component.html',
  styleUrls: ['./add-cinema.component.scss']
})
export class AddCinemaComponent implements OnInit {

  cinemaInfo: FormGroup;
  // series types
  series_types: any = ['FIXED + SERIES', 'SERIES + FIXED', 'SERIES + FIXED + SERIES', 'FIXED + SERIES + FIXED']
  type_of_series: any = 'FIXED + SERIES';
  cinemcode: any;
  rowData: any;

  constructor(private fb: FormBuilder, private restoServ: RestoService, private router: Router) {
    this.cinemaInfo = this.fb.group({
      cinema_code: [''],
      displayName: [''],
      legalName: [''],
      Allscreens: this.fb.array([])
      // series_array: this.fb.array([])
    })

    this.restoServ.getAllCinema().subscribe(data => {
      console.log("All Cinemas", data['data']);


      this.rowData = data['data']

      // Generating unique cinema code
      if (this.rowData.length == 0) {
        let len = this.rowData.length + 1   //length if array is empty 0+1
        let str = len.toString()            // converting 1 to string '1'
        let size = 3                        // size of code
        this.cinemcode = 'MLX' + str.padStart(size, '0')  // padding with 00 to left ex: 001
        console.log('cinema code', this.cinemcode);

      }
      else {

        console.log("all org lenth", this.rowData.length);
        let index = this.rowData.length - 1

        let last_code = this.rowData[index].cinema_code
        console.log('last_code', last_code);
        let str = last_code.slice(3)
        console.log('string', str);
        let digit = parseInt(str)
        digit = digit + 1
        let str1 = digit.toString()
        let size = 3
        this.cinemcode = 'MLX' + str1.padStart(size, '0')
        console.log('Chian code', this.cinemcode);


      }




    })

  }

  ngOnInit() {
  }



  // *** Screen as FormArray

  screen(): FormArray {
    return this.cinemaInfo.get("Allscreens") as FormArray
  }

  // **** new Single Screen Formgroup

  newScreen(): FormGroup {
    return this.fb.group({
      screenNo: [''],
      screenName: [''],
      seatingCapacity: [Number],
      livecount: [Number],
      collections: [[]],
      generate: [false],
      series_array: this.fb.array([])
    })
  }

  // *** ADD new Screen
  addScreen() {
    this.screen().push(this.newScreen())



  }

  // **** Remove screen
  removeScreen(si) {
    this.screen().removeAt(si)
    console.log("Screen Array Object", this.cinemaInfo.value)
  }


  // *** Series formArray
  series(si): FormArray {
    return this.screen().at(si).get("series_array") as FormArray
  }

  // Series as formGroup
  newSeries(): FormGroup {
    return this.fb.group({

      seriesNo: [''],
      seriesType: ['FIXED + SERIES'],
      from1: [''],
      to1: [''],
      fixed1: [''],
      fixed2: [''],
      from2: [''],
      to2: [''],
      readOnly: [false]
    })

  }


  // ***first time add series
  firstaddSeries(si, screen) {
    let countValue = screen.controls.livecount.value
    if (countValue > 0) {

      this.series(si).push(this.newSeries())
    }
    else {
      Swal.fire('Seating Capacity is Invalid', '', 'warning')
    }


  }

  // ***add series
  addSeries(si, screen, ss) {
    let countValue = screen.controls.livecount.value
    let from1 = ss.controls.from1.value
    let to1 = ss.controls.to1.value
    let fixed1 = ss.controls.fixed1.value
    let fixed2 = ss.controls.fixed2.value
    let from2 = ss.controls.from2.value
    let to2 = ss.controls.to2.value
    let seriesType = ss.controls.seriesType.value
    let collections = screen.controls.collections.value


    // *****************************************Sample for all series type*********************
    if (seriesType == 'FIXED + SERIES') {
      let subtract = to1 - from1
      subtract = subtract + 1
      console.log('substract value', subtract);
      
      if (subtract > countValue) {
        console.log('subtract', subtract)
        // console.log('lenColl', lenColl)
        console.log('countvalue', countValue);

        Swal.fire('Series Count Out of Limit', '', 'warning')
       
      }
      else {

        for (let i = 1; i <= subtract; i++) {
          let figure = fixed1 + i
          collections.push(figure)
        }
        console.log('collections', collections);
        screen.controls.collections.setValue(collections)
        console.log('sCREEN.controls.collections', screen.controls.collections.value);
        console.log('Screen series array', screen.controls.series_array.value)
  
        screen.controls.livecount.setValue(countValue - subtract)
        countValue = countValue - subtract
        if (countValue > 0) {

          this.series(si).push(this.newSeries())
        }
        else {
          Swal.fire('Series Completed', '', 'success')
        }
        console.log('sCREEN.controls.collections', screen.controls.collections.value);
        ss.controls.readOnly.setValue(true)
        ss.controls['seriesType'].disable()
      }

    }
    // *****************************************closing Sample for all series type*********************
    if (seriesType == 'SERIES + FIXED') {
      let subtract = to1 - from1
      subtract = subtract + 1
      console.log('substract value', subtract);

      for (let i = 1; i <= subtract; i++) {
        let figure = i + fixed1
        collections.push(figure)
      }
      console.log('collections', collections);
      screen.controls.collections.setValue(collections)
      console.log('sCREEN.controls.collections', screen.controls.collections.value);
      console.log('Screen series array', screen.controls.series_array.value)


      let lenColl = collections.length
      if (lenColl > countValue) {
        Swal.fire('Series Count Out of Limit', '', 'warning')
        screen.controls.collections.setValue([])
      }
      else {
        countValue = countValue - lenColl
        if (countValue > 0) {

          this.series(si).push(this.newSeries())
        }
        else {
          Swal.fire('Series Completed', '', 'success')
        }

        ss.controls.readOnly.setValue(true)
        ss.controls['seriesType'].disable()
      }

    }
    if (seriesType == 'SERIES + FIXED + SERIES') {
      let subtract1 = to1 - from1
      subtract1 = subtract1 + 1
      console.log('substract value', subtract1);

      let subtract2 = to2 - from2
      subtract2 = subtract2 + 1
      console.log('substract value', subtract2);


      for (let i = 1; i <= subtract1; i++) {
        for (let j = 1; j <= subtract2; j++) {
          let figure = i + fixed1 + j
          collections.push(figure)
        }
      }

      console.log('collections', collections);
      screen.controls.collections.setValue(collections)
      console.log('sCREEN.controls.collections', screen.controls.collections.value);
      console.log('Screen series array', screen.controls.series_array.value)


      let lenColl = collections.length
      if (lenColl > countValue) {
        Swal.fire('Series Count Out of Limit', '', 'warning')
        screen.controls.collections.setValue([])
      }
      else {
        countValue = countValue - lenColl
        if (countValue > 0) {

          this.series(si).push(this.newSeries())
        }
        else {
          Swal.fire('Series Completed', '', 'success')
        }

        ss.controls.readOnly.setValue(true)
        ss.controls['seriesType'].disable()
      }

    }
    if (seriesType == 'FIXED + SERIES + FIXED') {

      let substract = to1 - from1
      substract = substract + 1

      for (let i = 1; i <= substract; i++) {
        let figure = fixed1 + i + fixed2
        collections.push(figure)
      }

      console.log('collections', collections);
      screen.controls.collections.setValue(collections)
      console.log('sCREEN.controls.collections', screen.controls.collections.value);

      let lenColl = collections.length
      if (lenColl > countValue) {
        Swal.fire('Series Count Out of Limit', '', 'warning')
        screen.controls.collections.setValue([])
      }
      else {
        countValue = countValue - lenColl
        if (countValue > 0) {

          this.series(si).push(this.newSeries())
        }
        else {
          Swal.fire('Series Completed', '', 'success')
        }

        ss.controls.readOnly.setValue(true)
        ss.controls['seriesType'].disable()
      }
    }

  }

  // *** remove series
  removeSeries(si, ssi) {
    this.series(si).removeAt(ssi);

    if (this.series(si).length == 0) {

    }
  }

  // **proceed

  onProceed() {
    console.log("Screen Array Object", this.cinemaInfo.value)



  }

  // setButton Disable

  setDisable(screen) {

    let countValue = screen.controls.livecount.value
    if (countValue > 0) {
      screen.controls.generate.setValue(true)
    }

  }


  seriesType(series, si) {
    this.type_of_series = series.controls.seriesType.value

    series.controls.from1.reset()
    series.controls.to1.reset()
    series.controls.fixed1.reset()
    series.controls.fixed2.reset()
    series.controls.from2.reset()
    series.controls.to2.reset()

    console.log('series length', this.series(si).length);

    let len = this.series(si).length

    for (let i = 0; i < len; i++) {
      // this.series(si).clear

      console.log('count', i);

    }

    console.log(this.cinemaInfo.value);


  }


  // ********SET READONLY to each series included inside each screen
  setReadOnly(ss) {
    // console.log('each ss', ss.controls);
    // ss.controls.readOnly.setValue(true)
    // ss.controls['seriesType'].disable()
  }


  // ****Setting Enable Generate button at top of each screen
  setEnable(screen, si) {
    if (this.series(si).length == 0) {
      screen.controls.generate.setValue(false)
    }

  }

  // *********setting live count for calculation
  setLiveCount(screen) {
    screen.controls.livecount.setValue(parseInt(screen.controls.seatingCapacity.value))
    console.log('live count', screen.controls.livecount.value);

  }


  // ***Final save cinema
  onSaveCinema() {

    let obj = {

      cinema_code: this.cinemcode,
      display_name: this.cinemaInfo.controls.displayName.value,
      legal_name: this.cinemaInfo.controls.legalName.value,
      screen_series: this.cinemaInfo.controls.Allscreens.value

    }
    this.restoServ.addCinema(obj).subscribe(res => {
      console.log('response', res);
      if (res['success']) {
        Swal.fire('Cinema Added Successfully', '', 'success')
        this.router.navigate(['restaurants/cinema-home'])
      }
      else {
        Swal.fire('Failed to Add Cinema', '', 'error')
      }

    })
  }



  // DELETE SERIES 
  deleteSeries(ss, screen, si) {
    let from1 = ss.controls.from1.value
    let to1 = ss.controls.to1.value
    let fixed1 = ss.controls.fixed1.value
    let fixed2 = ss.controls.fixed2.value
    let from2 = ss.controls.from2.value
    let to2 = ss.controls.to2.value
    let seriesType = ss.controls.seriesType.value

    if (seriesType == 'FIXED + SERIES') {
      let subtract = to1 - from1
      subtract = subtract + 1
      console.log('substract value', subtract);
      let remaining = screen.controls.livecount.value
      screen.controls.livecount.setValue(remaining + subtract)
      console.log('Collections after delete', screen.controls.collections.value)
      let existCollect = screen.controls.collections.value
      let delCollect = []
      for (let i = 1; i <= subtract; i++) {
        let figure = fixed1 + i
        delCollect.push(figure)
      }
      console.log('existing collection', existCollect)
      console.log('deleted collection', delCollect)
      let newCollect = existCollect.filter(x => !delCollect.includes(x))
      console.log('new collect', newCollect)
      screen.controls.collections.setValue(newCollect)
    }


    let countValue = screen.controls.livecount.value
    if (countValue > 0) {

      this.series(si).push(this.newSeries())
    }
    else {
      Swal.fire('Seating Capacity is Invalid', '', 'warning')
    }

    console.log("cinemainfoo", this.cinemaInfo.value )
  }
}
