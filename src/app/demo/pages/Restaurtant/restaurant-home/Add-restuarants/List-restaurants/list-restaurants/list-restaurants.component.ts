  import { AfterViewInit, Component, OnInit, ViewChild, ɵɵcontainerRefreshEnd } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ReferenceAst } from '@angular/compiler';
import { NgxSpinnerService } from "ngx-spinner";

export interface restaurantElements {
  restoName: string;
  restoAddress: string;
  restoPincode: string;
  restoCuisines: string;
  restoHours: string;
  restoLegalName: string;
  restoContactNo: string;
  restoStatus: boolean
  restoRepresentativeName: string;
  restoRepresentativeContactNo: string;
  restoRepresentativeEmail: string;
  restoRevenue: string;
  restoLocality: string
  action: string
}
@Component({
  selector: 'app-list-restaurants',
  templateUrl: './list-restaurants.component.html',
  styleUrls: ['./list-restaurants.component.scss']
})



export class ListRestaurantsComponent implements OnInit {

  dtExportButtonOptions: any = {};
  dtColumnsReorderOptions: any = {};
  dtResponsiveOptions: any = {};
  dtRowSelectOptions: any = {};
  dtRouterLinkOptions: any = {};


  // all restaurants array

  all_Restaurant: any = []

  // only restoINFO 1ST STEPPER
  restoInfo: any = []

  custom_resto_obj: any

  // displayedColumns: string[] = ['restoName', 'restoAddress','restoPincode', 'restoCuisines','restoHours','restoLegalName',
  //   'restoContactNo', 'restoStatus','restoRepresentativeName', 'restoRepresentativeContactNo',
  //   'restoRevenue', 'action'];

  displayedColumns: string[] = ['restoName', 'restoAddress','restoPincode', 'restoCuisines',
  'restoRepresentativeName', 'restoRepresentativeContactNo',
  'restoStatus','restoRevenue','restoLocality', 'action'];
    
  dataSource: MatTableDataSource<restaurantElements>;
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("sort", { static: true }) sort: MatSort
  noData: boolean = false;
  fullSingleResto: any= [];



  constructor(private restServ: RestoService, 
    private router: Router,
    private matDialog: MatDialog,
    private ngxSpinner: NgxSpinnerService) {

    
this.loading();
  this.getAllResto()

    // all the restInfo array
    console.log('all restoinfo array', this.restoInfo);


    console.log('show array', this.restoInfo);



  }

  ngOnInit() {

    this.dtExportButtonOptions = {
      ajax: 'fake-data/datatable-data.json',
      columns: [{
        title: 'Legal Name',
        data: 'name'
      }, {
        title: 'Restaurant Name',
        data: 'position'
      }, {
        title: 'Contact',
        data: 'office'
      }, {
        title: 'Email',
        data: 'age'
      }, {
        title: 'Address',
        data: 'date'
      }, {
        title: 'Pincode',
        data: 'salary'
      }, {
        title: 'Action',
        data: ''
      }],
      dom: 'Bfrtip',
      buttons: [
        'copy',
        'print',
        'excel',
        'csv'
      ]
    };
    this.dtColumnsReorderOptions = {
      ajax: 'fake-data/datatable-data.json',
      columns: [{
        title: 'Legal Name',
        data: 'name'
      }, {
        title: 'Restaurant Name',
        data: 'position'
      }, {
        title: 'Contact',
        data: 'office'
      }, {
        title: 'Email',
        data: 'age'
      }, {
        title: 'Address',
        data: 'date'
      }, {
        title: 'Pincode',
        data: 'salary'
      }, {
        title: 'Action',
        data: ''
      }],
      dom: 'Rt',
      colReorder: {
        order: [0, 3, 4, 2, 1, 5],
        fixedColumnsRight: 1
      },
      responsive: true
    };
    this.dtResponsiveOptions = {
      ajax: 'fake-data/datatable-data.json',
      columns: [{
        title: 'Legal Name',
        data: 'name'
      }, {
        title: 'Restaurant Name',
        data: 'position'
      }, {
        title: 'Contact',
        data: 'office'
      }, {
        title: 'Email',
        data: 'age'
      }, {
        title: 'Address',
        data: 'date'
      }, {
        title: 'Pincode',
        data: 'salary'
      }, {
        title: 'Action',
        data: ''
      }],
      responsive: true
    };
    this.dtRowSelectOptions = {
      ajax: 'fake-data/datatable-data.json',
      columns: [{
        title: 'Legal Name',
        data: 'name'
      }, {
        title: 'Restaurant Name',
        data: 'position'
      }, {
        title: 'Contact',
        data: 'office'
      }, {
        title: 'Email',
        data: 'age'
      }, {
        title: 'Address',
        data: 'date'
      }, {
        title: 'Pincode',
        data: 'salary'
      }, {
        title: 'Action',
        data: ''
      }],
      responsive: true,
      select: true
    };
    this.dtRouterLinkOptions = {
      ajax: 'fake-data/datatable-data.json',
      columns: [{
        title: 'Legal Name',
        data: 'name'
      }, {
        title: 'Restaurant Name',
        data: 'position'
      }, {
        title: 'Contact',
        data: 'office'
      }, {
        title: 'Email',
        data: 'age'
      }, {
        title: 'Address',
        data: 'date'
      }, {
        title: 'Pincode',
        data: 'salary'
      }, {
        title: 'Action',
        render: function (data: any, type: any, full: any) {
          return '<button class="btn btn-outline-primary btn-sm">View</button>';
        }
      }],
      responsive: true
    };



  }

  // /Loading fucntion
  loading(){
    this.ngxSpinner.show()
  }
  // Getting all RESTAURANTS
  getAllResto(){
    this.restServ.getAllRestaurant().subscribe(data => {
      console.log('data', data['data']);
      this.ngxSpinner.hide()
      this.all_Restaurant = data['data']
      if (data['data'] != []) {

        this.noData = true;
        // sort restoInfo from full data

        // let allrestoinfo = data['data']
        // this.all_Restaurant = data['data']
        this.all_Restaurant.forEach(element => {
          // console.log('elements:-', element);
          this.restoInfo.push(element.restoInfo)
          this.fullSingleResto.push(element)

          this.dataSource = new MatTableDataSource(this.fullSingleResto)
          this.dataSource.paginator = this.paginator
          this.dataSource.sort = this.sort
        });
      }
      else{
        this.noData = false
        
      }


    })
  }

  // /////////////////////////////////////////////////////////////
  //////////////////DATA TABLE FILTER////////////////////////////
  ///////////////////////////////////////////////////////////
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  // ///////////////////////////////////////////
  ////////////////////Navigations /////////////
  /////////////////////////////////////////////

  onViewRestaurant(singleResto) {
    this.router.navigate(['restaurants/view-restaurant'])
    console.log('data single', singleResto);

    console.log('index', this.restoInfo.indexOf(singleResto))
    let index = this.restoInfo.indexOf(singleResto)

    console.log('full table', this.all_Restaurant[index]);
    let singleFUllDetail = this.all_Restaurant[index]

    console.log('single Full table', singleFUllDetail);
    console.log('selected establisment', singleFUllDetail.establishmentType)
    console.log('selected legal', singleFUllDetail.legalInfo);
    console.log('selected Media', singleFUllDetail.media);
    console.log('selected Menu', singleFUllDetail.menuManagement);
    console.log('selected Layour', singleFUllDetail.layout);


    // legalInfo
    // media
    // menuManagement

    // console.log('id', singleFUllDetail._id)
    // this.restServ.singleViewRestaurant = singleFUllDetail._id
    // onlyRestoInfo: any
    // onlyEstablish: any
    // onlyLegalInfo: any
    // onlyMedia: any
    // onlyMenu: any
    // onlyLayout: any

    console.log('onlyResto')
    this.restServ.onlyRestoInfo = singleResto
    this.restServ.onlyEstablish = singleFUllDetail.establishmentType
    this.restServ.onlyLegalInfo = singleFUllDetail.legalInfo
    this.restServ.onlyMedia = singleFUllDetail.media
    this.restServ.onlyMenu = singleFUllDetail.menuManagement
    this.restServ.onlyLayout = singleFUllDetail.layout



  }



  onEditRestaurant(singleResto) {
    this.router.navigate(['restaurants/edit-restoinfo'])
    console.log('data single', singleResto);

    console.log('index', this.restoInfo.indexOf(singleResto))
    let index = this.restoInfo.indexOf(singleResto)

    console.log('full table', this.all_Restaurant[index]);
    let singleFUllDetail = this.all_Restaurant[index]

    console.log('single Full table', singleFUllDetail);
    console.log('Restaurant id', singleFUllDetail._id );
    
    console.log('selected establisment', singleFUllDetail.establishmentType)
    console.log('selected legal', singleFUllDetail.legalInfo);
    console.log('selected Media', singleFUllDetail.media);
    console.log('selected Menu', singleFUllDetail.menuManagement);
    console.log('selected Layour', singleFUllDetail.layout);



    console.log('onlyResto')
    this.restServ.onlyRestoInfo = singleResto
    this.restServ.onlyEstablish = singleFUllDetail.establishmentType
    this.restServ.onlyLegalInfo = singleFUllDetail.legalInfo
    this.restServ.onlyMedia = singleFUllDetail.media
    this.restServ.onlyMenu = singleFUllDetail.menuManagement
    this.restServ.onlyLayout = singleFUllDetail.layout
    this.restServ.RestoId = singleFUllDetail._id


  }


  // ON Change STATUS
  onChangeStatus(evt, id){

    
   
  console.log('event over checked', evt, id );
  
  var statusObj={
    status: evt
  }
  this.restServ.setRestaurantStatus(id,statusObj).subscribe(res=>{
    if(res['sucess']){
     if(evt){
       Swal.fire('Successfully Activated Restaurant','','success')
     }
     else{
       Swal.fire('Successfully Deactivated Restaurant','','warning')
     }
    }
    else{
      Swal.fire('Failed Something Went Wrong!!')
      location.reload();
    }
  })
  }
}
