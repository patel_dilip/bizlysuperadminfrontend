import { Component, OnInit, Injectable, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder, Form, FormArray, FormControl } from '@angular/forms';
import { LiquorService } from 'src/app/_services/liquor.service';
import { EstablishmentTypeService } from 'src/app/_services/establishment-type.service';
import { MoreinfoService } from 'src/app/_services/moreinfo.service';
import { MainService } from 'src/app/_services/main.service';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay, startWith } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { EstAddCatComponent } from 'src/app/PopoversList/est-add-cat/est-add-cat.component';
import { SubCatComponent } from 'src/app/PopoversList/sub-cat/sub-cat.component';
import { AttrCatComponent } from 'src/app/PopoversList/attr-cat/attr-cat.component';
import { CreateAlbumComponent } from 'src/app/PopoversList/create-album/create-album.component';
import { AddLayoutComponent } from 'src/app/PopoversList/add-layout/add-layout.component';
import { OpenTableComponent } from 'src/app/PopoversList/open-table/open-table.component';
import { AbstractJsEmitterVisitor } from '@angular/compiler/src/output/abstract_js_emitter';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { AddRevenueComponent } from 'src/app/PopoversList/add-revenue/add-revenue.component';
import { AddRestoTypeComponent } from 'src/app/PopoversList/add-resto-type/add-resto-type.component';
import Swal from 'sweetalert2';
import { HttpClient } from '@angular/common/http';
import { AddCatoNameComponent } from 'src/app/PopoversList/add-cato-name/add-cato-name.component';
import { AddSubCatComponent } from 'src/app/PopoversList/add-sub-cat/add-sub-cat.component';
import { AddSubSubCatComponent } from 'src/app/PopoversList/add-sub-sub-cat/add-sub-sub-cat.component';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { ThrowStmt } from '@angular/compiler';
import { Router } from '@angular/router';
import { AddRootMenuComponent } from 'src/app/PopoversList/add-root-menu/add-root-menu.component';
import { AddCateMenuComponent } from 'src/app/PopoversList/add-cate-menu/add-cate-menu.component';
import { AddSubcatMenuComponent } from 'src/app/PopoversList/add-subcat-menu/add-subcat-menu.component';
import { AddChildChildComponent } from 'src/app/PopoversList/add-child-child/add-child-child.component';
import { AddCcMenuComponent } from 'src/app/PopoversList/add-cc-menu/add-cc-menu.component';
import { AddRootLiquorPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-root-liquor-pop/add-root-liquor-pop.component';
import { AddVarientPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-varient-pop/add-varient-pop.component';
import { AddSubVarientPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-sub-varient-pop/add-sub-varient-pop.component';
import { AddSuperChildPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-super-child-pop/add-super-child-pop.component';
import { AddServicesChipsComponent } from 'src/app/PopoversList/Establish-related-popups/add-services-chips/add-services-chips.component';
import { AddMoreInfoComponent } from 'src/app/PopoversList/Establish-related-popups/add-more-info/add-more-info.component';
import { AddRulesChipsComponent } from 'src/app/PopoversList/Establish-related-popups/add-rules-chips/add-rules-chips.component';
import { AddAmbienceChipsComponent } from 'src/app/PopoversList/Establish-related-popups/add-ambience-chips/add-ambience-chips.component';
import { AddBillcounterPopupComponent } from 'src/app/PopoversList/Layout-related-popups/add-billcounter-popup/add-billcounter-popup.component';
import { AddChairPopupComponent } from 'src/app/PopoversList/Layout-related-popups/add-chair-popup/add-chair-popup.component';
import { AddKitchenPopupComponent } from 'src/app/PopoversList/Layout-related-popups/add-kitchen-popup/add-kitchen-popup.component';
import { CreateVdoAlbumComponent } from 'src/app/PopoversList/Media-related-Popups/create-vdo-album/create-vdo-album.component';
import { NgxSpinnerService } from "ngx-spinner";
import { GenerateWebsiteComponent } from 'src/app/PopoversList/Restaurants-related-popups/generate-website/generate-website.component';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { AddChipAmbiencesComponent } from 'src/app/PopoversList/Establish-related-popups/add-chip-ambiences/add-chip-ambiences.component';
import { AddChipMoreinfoComponent } from 'src/app/PopoversList/Establish-related-popups/add-chip-moreinfo/add-chip-moreinfo.component';
import { AddChipRulesComponent } from 'src/app/PopoversList/Establish-related-popups/add-chip-rules/add-chip-rules.component';
import { AddChipServicesComponent } from 'src/app/PopoversList/Establish-related-popups/add-chip-services/add-chip-services.component';
// import { } from 'googlemaps';
import { element } from 'protractor';
import { QrcodenoComponent } from 'src/app/PopoversList/Restaurants-related-popups/qrcodeno/qrcodeno.component';
import { PopLayoutTabComponent } from '../../pop-layout-tab/pop-layout-tab.component';
import { isNullOrUndefined } from 'util';
import { TreeNode } from 'primeng/api';
import { PopTreeComponent } from '../../pop-tree/pop-tree.component';
import { ConfirmationService } from 'primeng/api';
import { PopDelTreeComponent } from '../../pop-del-tree/pop-del-tree.component';

// IMAGES UPLOAD URLS
const GstImg = environment.base_Url + "restaurantLegal/uploadgst";
const PanImg = environment.base_Url + "restaurantLegal/uploadpan";
const FssaiImg = environment.base_Url + "restaurantLegal/uploadfssai";
const NocImg = environment.base_Url + "restaurantLegal/uploadnoc";
const LiquorLicImg = environment.base_Url + "restaurantLegal/uploadliquorlicense";
const EatingHouseImg = environment.base_Url + "restaurantLegal/uploadeatinghouselicense"
const MusicLicImg = environment.base_Url + "restaurantLegal/uploadmusiclicense";
const TrademarkImg = environment.base_Url + "restaurantLegal/uploadtrademarkforcafelicense";
const CECImg = environment.base_Url + "restaurantLegal/uploadcec";
const ShopsActImg = environment.base_Url + "restaurantLegal/uploadshopsandestablishmentact";
const SignageImg = environment.base_Url + "restaurantLegal/uploadsignagelicense";
const LiftImg = environment.base_Url + "restaurantLegal/uploadliftclearance"
const HealthTradeImg = environment.base_Url + "restaurantLegal/uploadhealthtradelicense"
// restaurantLegal/uploadnoc
// restaurantLegal/uploadliquorlicense
// restaurantLegal/uploadeatinghouselicense
// restaurantLegal/uploadmusiclicense
// restaurantLegal/uploadtrademarkforcafelicense
// restaurantLegal/uploadcec
// restaurantLegal/uploadshopsandestablishmentact
// restaurantLegal/uploadsignagelicense
// restaurantLegal/uploadliftclearance
// restaurantLegal/uploadhealthtradelicense


// MEDIA UPLOAD URLS
const OtherImg = environment.base_Url + "restaurantLegal/uploadother";
const LogoImg = environment.base_Url + "restaurant/uploadrestaurantlogo";
const BannerImg = environment.base_Url + "restaurant/uploadrestaurantbanner"
const ImagesAlbum = environment.base_Url + "restaurant/uploadrestoalbumimages"
const VideoAlbum = environment.base_Url + "restaurant/uploadrestoalbumvideos"

@Component({
  selector: 'app-add-restuarant',
  templateUrl: './add-restuarant.component.html',
  styleUrls: ['./add-restuarant.component.scss']
})


export class AddRestuarantComponent implements OnInit {
  // gaugeType = "semi";
  // gaugeValue = 28.3;
  // gaugeLabel = "Speed";
  // gaugeAppendText = "km/hr";

  lat: number = 18.4962;
  lng: number = 73.8120;
  zoom: number = 15;
  isLinear = true;

  // LAYOUT variables

  newHeight: any
  client: boolean = false
  rectArr: any = []

  alter: any = []
  i = 0


  // Google maps variables
  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  // zoom: number;

  // GOOGLE PLACES VARIABLES
  @Input() adressType: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('addresstext', { static: true }) addresstext: any;

  autocompleteInput: string;
  queryWait: boolean;


  // time report
  time = { hour: 13, minute: 30 };
  meridian = true;
  websiteId: any = '';
  webReadOnly: boolean = false;
  selectedDocs: any = [];
  nocResponse: any;
  liquorLic: any;
  eatingHouseResponse: any;
  musicLicResponse: any;
  trademarkResponse: any;
  cecResponse: any;
  shopActResponse: any;
  sinageResponse: any;
  LiftResponse: any;
  healthResponse: any;
  addmonHr: boolean = true;
  addtueHr: boolean = true;
  addwedHr: boolean = true;
  addthurHr: boolean = true;
  addfriHr: boolean = true;
  addsatHr: boolean = true;
  addsunHr: boolean = true;
  add: any;
  restaurantCountryCode: any;
  restaurantContact: any;
  respresentativeCoutryCode: any;
  respresentativeContact: any;
  gstStatus: any = false;
  gstName: any;
  panName: any;
  panStatus: boolean = false;
  fssaiName: any;
  fssaiStatus: boolean = false;
  nocName: any;
  nocStatus: boolean = false;
  LiqLicName: any;
  LiqLicStatus: boolean = false;
  eatLicName: any;
  eatLicStatus: boolean = false;
  musicLicName: any;
  musicLicStatus: boolean = false;
  trademarkName: any;
  trademarkStatus: boolean = false;
  cedName: any;
  cedStatus: boolean = false;
  shopActName: any;
  shopActStatus: boolean = false;
  sinageName: any;
  sinageStatus: boolean = false;
  liftLicName: any;
  liftLicStatus: boolean = false;
  healthLicStatus: boolean = false;
  healthLicName: any;
  logoHide: boolean = true;
  bannerHide: boolean = true;
  isQRshow: boolean = true;
  layoutQrChecks: any = [];
  selectSeries: any;
  lastSeriesArr: any;
  PocLayoutForm: FormGroup;
  qtyoftable: any;
  fstablecount: number = 0;
  tablesValue = 0;
  balance_table: number = 0;
  combine_count: number = 0;
  combinations: number = 0;
  total_combinations: number = 0;
  isReadOnly: boolean = false;
  OrganizationId: any;
  OrganizationObj: any;
  outletarr: any = [];
  isOutletKeys: any = true;
  docStatus: any = [];
  docAble: boolean = false;
  multiplexArr: any = [];
  chainArr: any = [];
  googleplace: any;
  selectedValue: string[];
  isOutValid: boolean = true;
  isSoloStand: boolean = true;
  isSoloFood: boolean = true;
  isChainStand: boolean = true;
  isChainFood: boolean = true;
  allOutlets: any[];
  outletcode: string;
  tab_touch: string;
  tab_touch1: any;
  isTreeEdit: boolean = false;
  estNode: any;
  estDelNode: any;
  ismenuTree: boolean = false;
  isEstExpand: boolean = false;
  isMenuExpand: boolean = false;
  albumAmbience: any = [];
  albumRules: any = [];
  albumService: any = [];
  albumMoreinfo: any = [];
  albumOtherChip: any = [];



  toggleMeridian() {
    this.meridian = !this.meridian;
  }

  /// Fomrgroups Declarations
  outletFormGrp: FormGroup;
  RestaurantInfo: FormGroup;
  EstablishmentType: FormGroup;
  LegalForm: FormGroup;
  MediaForm: FormGroup;
  MenuMgmtForm: FormGroup;
  LayoutForm: FormGroup;

  // ******* media other alubm
  otheralbum: FormGroup;

  AllDialCodes: any
  Revenue: any

  isDine = false;
  isCloudKitchen = false;
  isDelievery = false;
  isSelfService = false;

  isMon = true;
  isTues = false;
  isWed = false;
  isThur = false;
  isFri = false;
  isSat = false;
  isSun = false;

  monAddHr = false
  tueAddHr = false
  wedAddHr = false
  thurAddHr = false
  friAddHr = false
  satAddHr = false
  sunAddHr = false
  dummy: any;
  Establishment: Object;
  popAdd: Object;
  popSub: any;
  popAttr: any;

  barMoreInfo;
  pubMoreInfo;
  serviceMoreInfo;
  rulesMoreInfo;
  placesMoreInfo;
  ambienceMoreInfo;

  selectedBars = [];
  selectedPubs = [];
  selectedServices: any = [];
  selectedRules = [];
  selectedPlaces = [];
  selectedAmbience = [];

  allCuisines: string[];
  selectedCuisines = [];

  firstLevel: Object;
  menuFirstlevel: Object;
  liquorLevel: Object;

  mondayOpenTime: any;
  mondayCloseTime: any;
  extramonOpenTime: any;
  extamonCloseTime: any;
  ischecked: any;
  docsCount: number = 0;

  albumNames: any = []
  menuNav: string;

  isAlbum: boolean = true;
  tabs = [];
  selected = new FormControl(0);
  tabNames: any;
  tablesSelected: any;
  currentTab: any;
  tableObj: any;
  restotype: string[];
  restaurants_types = [];
  restaurants_contact: any;
  finalRestObj: any
  finalLegalObj: any;
  finalMediaObj: any;
  cuisinesObj: any[];

  output: any = {};
  FinalEstObj: any
  finalMoreInfobj: any
  finalServicesobj: any;
  finalRulesobj: any;
  finalAmbienceObj: any;
  estobj: any
  cuisinesArr: string[];
  submenu: any
  liquormenu: any
  finalMenuMgmt: any
  objectvalue: { restoInfo: any; establishmentType: any; legalInfo: any; media: any; menuManagement: any; };
  FinalLayoutObj: any
  checkobj: any;
  allDrinks: any;
  estRootCategory: any;
  estCateName: string[];
  cateChildName: string[];
  cateChildChildName: string[];
  selectedRootType: any;
  categoryName: any;
  childCategory: any;
  subsubChild: any;
  DrinkTypes = [];
  account_types = ["Saving Account", "Current Account"]

  // Layout tab variables
  LayoutsArr = []
  activeLink = this.LayoutsArr[0]
  layoutCount = 0;

  // Legal Images URLs
  public gstUploader: FileUploader = new FileUploader({ url: GstImg, itemAlias: 'gstfile' });
  public panUploader: FileUploader = new FileUploader({ url: PanImg, itemAlias: 'panfile' });
  public fssaiUploader: FileUploader = new FileUploader({ url: FssaiImg, itemAlias: 'fssaifile' });
  public nocUploader: FileUploader = new FileUploader({ url: NocImg, itemAlias: 'noc' });
  public LiquorLicUploader: FileUploader = new FileUploader({ url: LiquorLicImg, itemAlias: 'liquorlicense' });
  public EatingHouseUploader: FileUploader = new FileUploader({ url: EatingHouseImg, itemAlias: 'eatinghouselicense' });
  public MusicLicUploader: FileUploader = new FileUploader({ url: MusicLicImg, itemAlias: 'musiclicense' });
  public TrademarkUploader: FileUploader = new FileUploader({ url: TrademarkImg, itemAlias: 'trademarkforcafelicense' });
  public CECUploader: FileUploader = new FileUploader({ url: CECImg, itemAlias: 'cec' });
  public ShopsActUploader: FileUploader = new FileUploader({ url: ShopsActImg, itemAlias: 'shopsandestablishmentact' });
  public SignageUploader: FileUploader = new FileUploader({ url: SignageImg, itemAlias: 'signagelicense' });
  public LiftUploader: FileUploader = new FileUploader({ url: LiftImg, itemAlias: 'liftclearance' });
  public HealthTradeUploader: FileUploader = new FileUploader({ url: HealthTradeImg, itemAlias: 'healthtradelicense' });


  //   noc
  // liquorlicense
  // eatinghouselicense
  // musiclicense
  // trademarkforcafelicense
  // cec
  // shopsandestablishmentact
  // signagelicense
  // liftclearance
  // healthtradelicense


  public otherUploader: FileUploader = new FileUploader({ url: OtherImg, itemAlias: 'otherfile' });
  public logoUploader: FileUploader = new FileUploader({ url: LogoImg, itemAlias: 'restologo' });

  public BannerUploader: FileUploader = new FileUploader({ url: BannerImg, itemAlias: 'restobanner' });
  public ImgAlbumUploader: FileUploader = new FileUploader({ url: ImagesAlbum, itemAlias: 'restoalbumimages' });
  public VideoAlbumUploader: FileUploader = new FileUploader({ url: VideoAlbum, itemAlias: 'restoalbumvideos' });


  response: string;
  gstResponse: any;

  panResponse: any;
  fssaiResponse: any;
  otherResponse = [];
  isgstTrash: boolean = true;
  ispanTrash: boolean = true;
  isFssaiTrash: boolean = true;
  isOtherTrash: boolean = true;
  docs: any;
  otherdocsImages: any;
  estTreeArr = [];
  estKeepTrack = [];
  logoResponse: any;
  OutletId = '';
  BannerResponse = [];
  ImgAlbumResponse = [];
  selectedRootNames = [];
  selectedCatNames = [];
  selectedChildName = [];
  selectedChildChildName = [];
  AllRootMenu = [];
  selectedMenuRoot = [];
  selectedMenuCategory = [];
  selectedChildCategory = [];
  selectedMenucc = [];
  selectedRootDrinksName = [];
  selectedDrinkVarient = [];
  selectedDrinkSubVarient = [];
  selectedDrinkSuperChild = [];
  selectedCuisinesName = [];
  allMoreInfos: any = [];

  isAddTable = true
  isAddChair = true
  isBillCounter = true
  isKitchen = true
  dupChkServ = [];
  dupChkMoreInfo = [];

  selectedEstTypes = [];
  selectedMoreInfos = [];
  dupChkRules = [];
  selectedRulesInfo = [];
  dupChkAmbience = [];
  selectedAmbienceInfo = [];
  createdVdoAlbum = [];
  VideoAlbumResponse = [];
  album_image: any = [];
  album_video: any = [];
  testObj: any;
  tableCount: any = 0;
  billCount: any = 0;
  wallcount: any = 0;
  washroomCount: any = 0;
  selectRestType: any = true;
  typeDelievery: any = true;
  typeDinein: any = true;
  typeSelfservice: any = true;
  typeCloudKitchen: any = true;
  isCountryCodeSelected = '';
  // documents DROP DOWN SETTING FILES
  dropdownList = [];
  productList = []
  selectedItems = [];
  dropdownSettings = {};
  IDropdownSettings: IDropdownSettings

  cities = [
    { id: 1, name: 'Vilnius' },
    { id: 2, name: 'Kaunas' },
    { id: 3, name: 'Pavilnys', disabled: true },
    { id: 4, name: 'Pabradė' },
    { id: 5, name: 'Klaipėda' }
  ];

  selectedCity: any;

  allBuildings = [
    { type: 'Mall' },
    { type: 'Commercial' },
    { type: 'Hospital' },
    { type: 'Stadium' },
    { type: 'Airport' },
    { type: 'Train Station' },
    { type: 'Bus Station' },
    { type: 'Institute' },
    { type: 'School' },
    { type: 'Park' },
    { type: 'Theme Park' },
    { type: 'Standalone' },
    { type: 'Multiplex' }
  ]

  regAddress: any = [
    {
      infrastructure_name: 'Phoenix Market City',
      google_add: 'S No 207, Viman Nagar Road, Phoenix Road, Clover Park, Viman Nagar, Pune, Maharashtra 411014, India'
    },
    {
      infrastructure_name: 'Amanora Park Town',
      google_add: 'Amanora Park Town, Hadapsar, Pune, Maharashtra, India'
    },
    {
      infrastructure_name: 'Inorbit Mall',
      google_add: 'Viman Nagar, Pune, Maharashtra 411014, India'
    },
    {
      infrastructure_name: 'Pizza Hut',
      google_add: 'SN 1A,1B,1C,1E Gr, Millenium Plaza, FC clg Rd, Ganeshwadi, Shivajinagar, Pune, Maharashtra 411004, India'
    },
    {
      infrastructure_name: 'City Pride Kothrud',
      google_add: '20/1, 2, near Anand Residency, Paschimanagri, Kothrud, Pune, Maharashtra 411038, India'
    },
    {
      infrastructure_name: 'Adlabs Imagica Amusement Park',
      google_add: 'No. 30 31, State Highway 92 near Lonavla, Sangdewadi, Khopoli, Maharashtra 410203, India'
    },
    {
      infrastructure_name: 'WestEnd Mall',
      google_add: 'Near Parihar Chowk, Harmony Society, Ward No. 8, Wireless Colony, Aundh, Pune, Maharashtra 411007, India'
    },
  ]

  options: any = ['Phoenix Market City', 'Amanora Park Town', 'Inorbit Mall', 'Pizza Hut', 'City Pride Kothrud', 'Adlabs Imagica Amusement Park', 'WestEnd Mall']
  options1: any = []
  options2: any = []
  filteredOptions: Observable<string[]>;
  filteredOptions1: Observable<string[]>;
  filteredOptions2: Observable<string[]>;


  // **********************************
  // ***********************************
  // OUTLET STEPPER Variables Declare
  // **********************************
  // **********************************

  ot_restaurant_is: any = 'solo' // Note: - by default should be solo

  ot_infrastructure_options = ['Standalone Entity', 'Food Court', 'Food Truck']
  ot_infrastructure = 'Standalone Entity' // Note:-  by Defalut selection be food court\

  userid: any

  // VALIDATORS VARIABLES FOR OUTLET FORM 
  soloStand: boolean = true
  soloFood: boolean = false
  chainStand: boolean = false
  chainFood: boolean = false

  isOutletTrue: boolean = false


  // Layout Tabs code 
  layoutTabs: any = []

  //  ESTABLISHMENT TREEE
  estFiles: TreeNode[] = []
  menuFiles: TreeNode[] = []

  constructor(private fb: FormBuilder,
    private liqourService: LiquorService,
    private establishService: EstablishmentTypeService,
    private moreinfo: MoreinfoService,
    private mainService: MainService,
    private matDialog: MatDialog,
    private restServ: RestoService,
    private http: HttpClient,
    private addMenuFoodsService: AddMenuFoodsService,
    private router: Router,
    private ngxSpinner: NgxSpinnerService,
    private confirmationService: ConfirmationService
  ) {


    // userid

    let user = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = user._id

    console.log("USERID :--", this.userid);

    // outlet full code  unique code 

    if (this.restServ.OrganizationObj == undefined) {
      this.router.navigate(['/restaurants'])
    }
    this.allOutlets = this.restServ.OrganizationObj.outlets_arr
    console.log("organizations objects", this.allOutlets)


    if (this.allOutlets.length == 0) {
      let len = this.allOutlets.length + 1   //length if array is empty 0+1
      let str = len.toString()            // converting 1 to string '1'
      let size = 3                        // size of code
      this.outletcode = 'OUT' + str.padStart(size, '0')  // padding with 00 to left ex: 001
      console.log('OUTLETCODE code', this.outletcode);

    }
    else {

      console.log("all org lenth", this.allOutlets.length);
      let index = this.allOutlets.length - 1

      let last_code = this.allOutlets[index].outletInfo.outlet_code
      console.log('last_code', last_code);
      let str = last_code.slice(3)
      console.log('string', str);
      let digit = parseInt(str)
      digit = digit + 1
      let str1 = digit.toString()
      let size = 3
      this.outletcode = 'OUT' + str1.padStart(size, '0')
      console.log('OUTLET code', this.outletcode);


    }


    // layout code
    let exist = []

    if (exist) {
      this.rectArr = exist
      this.i = this.rectArr.length
    }


    // Keep Organization id 
    this.OrganizationObj = this.restServ.OrganizationObj

    if (this.OrganizationObj == undefined) {
      this.router.navigate(['/restaurants'])
    }
    ////console.log('organization Object', this.OrganizationObj);



    this.getAllMoreInfos();
    // Images uploaders



    //  get all establishmetn tree

    this.getEstRootType()




    this.allCuisines = ['Spanish', 'Sri Lankan', 'Taiwanese', 'Tatar', 'Thai', 'Turkish', 'Tamil', 'Udupi', 'Ukrainian', 'Vietnamese', 'Yamal', 'Zambian', 'Zanzibari']

    // Restaurant's type
    this.restotype = ['Dine-In', 'Cloud Kitchen', 'Delievery', 'Self Service']

    //dummy
    this.dummy = 'Establishment'
    this.menuNav = 'Cuisines'

    //  Calling RestoTYpe service
    this.getAllRestoType();

    // Calling all Drinks service
    this.getRootDrinkTypes()

    // Calling Revenue service
    this.getRevenue();

    // get establishment root category

    this.getEstRootType();

    // GET ALL ROOT MENU
    this.getAllRestoMenu();
    // Calling Liquor service
    this.liqourService.getAllRootDrinks().subscribe(data => {
      console.log("Liquor types", data);

      this.liquorLevel = data
      // console.table("Liquor types",data);

    })


    // *********** Calling all establishmentypes service
    this.establishService.getAllEstablishmentTypes().subscribe(data => {
      console.log("establishment types array: ", data);
      this.estFiles = [
        {
          "label": "QSR",
          "data": "root",
          "expanded": true,
          "expandedIcon": "pi pi-folder-open",
          "collapsedIcon": "pi pi-folder",
          "leaf": false,
          "children": [
            {
              "label": "Fast Food",
              "data": "child",
              "expanded": true,
              "expandedIcon": "pi pi-folder-open",
              "collapsedIcon": "pi pi-folder",
              "leaf": false,
              "children": [
                {
                  "label": "Manchurian",
                  "data": "child",

                  "expanded": true,
                  "expandedIcon": "pi pi-folder-open",
                  "collapsedIcon": "pi pi-folder",
                  "leaf": false,
                  "children": [
                    {
                      "label": "Ramen",
                      "data": "child",

                      "expanded": true,
                      "expandedIcon": "pi pi-folder-open",
                      "collapsedIcon": "pi pi-folder",
                      "leaf": false,
                      "children": []
                    }
                  ]
                }
              ]
            },
            {
              "label": "Fish Sea Food",
              "data": "child",

              "expanded": true,
              "expandedIcon": "pi pi-folder-open",
              "collapsedIcon": "pi pi-folder",
              "leaf": false,
              "children": [
                {
                  "label": "Tuna",
                  "data": "child",

                  "expanded": true,
                  "expandedIcon": "pi pi-folder-open",
                  "collapsedIcon": "pi pi-folder",
                  "leaf": false,
                  "children": [
                    {
                      "label": "Sushi",
                      "data": "child",

                      "expanded": true,
                      "expandedIcon": "pi pi-folder-open",
                      "collapsedIcon": "pi pi-folder",
                      "leaf": false,
                      "children": []
                    }
                  ]
                }
              ]
            },
            {
              label: "Dairy Food",
              data: "child",
              leaf: false,
              expanded: true,
              "expandedIcon": "pi pi-folder-open",
              "collapsedIcon": "pi pi-folder",
              children: [
                {
                  label: "Paneer",
                  data: "child",
                  leaf: false,
                  expanded: true,
                  "expandedIcon": "pi pi-folder-open",
                  "collapsedIcon": "pi pi-folder",
                  children: [
                    {
                      label: "Palak Paneer",
                      data: "child3 data",
                      leaf: false,
                      expanded: true,
                      "expandedIcon": "pi pi-folder-open",
                      "collapsedIcon": "pi pi-folder",
                      children: []
                    },
                    {
                      label: "Butter Paneer Masala",
                      data: "child3 data",
                      leaf: false,
                      expanded: true,
                      "expandedIcon": "pi pi-folder-open",
                      "collapsedIcon": "pi pi-folder",
                      children: []
                    }
                  ]
                },
                {
                  label: "Curd",
                  data: "child",
                  leaf: false,
                  expanded: true,
                  "expandedIcon": "pi pi-folder-open",
                  "collapsedIcon": "pi pi-folder",
                  children: [
                    {
                      label: "Raita",
                      data: "child3 data",
                      leaf: false,
                      expanded: true,
                      "expandedIcon": "pi pi-folder-open",
                      "collapsedIcon": "pi pi-folder",
                      children: []
                    },
                    {
                      label: "Dahi Vada",
                      data: "child3 data",
                      leaf: false,
                      expanded: true,
                      "expandedIcon": "pi pi-folder-open",
                      "collapsedIcon": "pi pi-folder",
                      children: []
                    }
                  ]
                }
              ]
            },

          ]
        },
        {
          label: "Desserts",
          data: "root",
          leaf: false,
          expanded: true,
          "expandedIcon": "pi pi-folder-open",
          "collapsedIcon": "pi pi-folder",
          children: [
            {
              label: "Indian",
              data: "child",
              leaf: false,
              expanded: true,
              "expandedIcon": "pi pi-folder-open",
              "collapsedIcon": "pi pi-folder",
              children: [
                {
                  label: "North Indian",
                  data: "child",
                  leaf: false,
                  expanded: true,
                  "expandedIcon": "pi pi-folder-open",
                  "collapsedIcon": "pi pi-folder",
                  children: [
                    {
                      label: "Gulab Jamun",
                      data: "child",
                      leaf: false,
                      expanded: true,
                      "expandedIcon": "pi pi-folder-open",
                      "collapsedIcon": "pi pi-folder",
                      children: []
                    },
                    {
                      label: "Rasmalai",
                      data: "child",
                      leaf: false,
                      expanded: true,
                      "expandedIcon": "pi pi-folder-open",
                      "collapsedIcon": "pi pi-folder",
                      children: []
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    })


    // *******Calling all cuisines API***********
    this.restServ.getAllCuisines().subscribe(data => {
      this.allCuisines = data['data']
      console.log('All cuisines form DB', this.allCuisines);

    })

    // *******Calling All More Info API*********
    this.getAllMoreInfos()



    // ********Calling All Services API******


    this.getAllServicesChips()

    //****** Calling All Rules API******

    this.getAllRulesChips();

    // ******* Calling Ambience API***********
    this.getAllAmbienceChips();

    //Calling all Dial COdes form service
    this.mainService.getAllDialCode().subscribe(data => {
      console.log('All Dial Codes', data);

      this.AllDialCodes = data

    })



    /// fetcing establishmet data array
    this.mainService.getAllEstablishment().subscribe(data => {
      console.log('establishment', data);
      this.firstLevel = data;

    })

    //fetching menus form menu mgmt
    this.mainService.getAllMenu().subscribe(data => {
      console.log(
        'Menu', data
      );

      this.menuFirstlevel = data;
    })

    this.menuFiles = [
      {
        "label": 'Starters',
        "data": 'root',
        "expanded": true,
        "expandedIcon": "pi pi-folder-open",
        "collapsedIcon": "pi pi-folder",
        "leaf": false,
        "children": [
          {
            "label": 'Chinese',
            "data": 'child',
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,
            "children": []
          },
          {
            "label": 'Soups',
            "data": 'child',
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,
            "children": []
          },
        ]
      },
      {
        "label": 'Main Course',
        "data": 'root',
        "expanded": true,
        "expandedIcon": "pi pi-folder-open",
        "collapsedIcon": "pi pi-folder",
        "leaf": false,
        "children": [
          {
            "label": 'Paneer Tikka',
            "data": 'child',
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,
            "children": []
          },
          {
            "label": 'Chicken Haryali',
            "data": 'child',
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,
            "children": []
          },
          {
            "label": 'Egg Fry',
            'data': 'child',
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,
            "children": []
          },
        ]
      },
      {
        "label": 'Deserts',
        "data": 'root',
        "expanded": true,
        "expandedIcon": "pi pi-folder-open",
        "collapsedIcon": "pi pi-folder",
        'leaf': false,
        "children": [
          {
            "label": 'Brownie',
            "data": 'child',
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,
            "children": [
              {
                "label": 'Chocolate Brownie',
                "data": 'child',
                "expanded": true,
                "expandedIcon": "pi pi-folder-open",
                "collapsedIcon": "pi pi-folder",
                "leaf": false,
                "children": []
              },
            ]
          },
          {
            "label": 'Cheese Cake',
            "data": 'child',
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,
            "children": []
          },
        ]
      },

    ]


    // fileter for cinema name

    this.restServ.getAllCinema().subscribe(data => {
      let dataArr = data['data']

      dataArr.forEach(element => {
        this.options1.push(element.display_name)
        this.multiplexArr.push(element)
      });
    })


    // filter for chain name

    this.restServ.getAllChains().subscribe(data => {
      let dataArr = data['data']
      console.log('data array', dataArr)
      dataArr.forEach(element => {
        console.log('element chain', element);


        this.options2.push(element.chain_info.display_name)
        this.chainArr.push(element)


      });
    })
  }
  ngOnInit() {

    // Google api init

    // this.setCurrentLocation();

    // outlet formgroup Builder

    this.outletFormGrp = this.fb.group({
      org_code: [''],
      org_name: [''],
      outlet_code: [''],
      restaurant_is: ['solo'],
      infrastructure_type: ['Standalone Entity'],
      chain_id: [''],
      chain_name: [''],
      type_of_building: ['Mall'],
      name_of_building: [''],
      address_of_building: [''],
      // multiplex_present: [Boolean],
      multiplexid: [''],
      multiplex_name: [''],
      multiplex_need_qr: [Boolean],
      food_court_name: [''],
      // food_court_need_qr: [Boolean],
      multiplex_screen: ['']
    })


    // Restaurant Basic Information
    this.RestaurantInfo = this.fb.group({
      legal_name: ['', [Validators.required, Validators.minLength(3), Validators.pattern('^[ a-zA-Z0-9.]+$')]],
      restaurant_name: ['', [Validators.required, Validators.pattern(''), Validators.minLength(3)]],
      restaurant_country_code: ['', Validators.required],
      // @"^[7-9]\d{9}$"
      restaurant_contact: ['', [Validators.required, Validators.pattern("^(0|[6-9][0-9]*)$")]],
      restaurant_email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      restaurant_address: ['', [Validators.required]],
      restaurant_building: ['', [Validators.required]],
      restaurant_landmark: ['', [Validators.required]],
      restaurant_street: ['', [Validators.required]],
      restaurant_sublocal: ['', [Validators.required]],
      restaurant_locality: ['', [Validators.required]],
      restaurant_town: ['', [Validators.required]],
      restaurant_city: ['', [Validators.required]],
      restaurant_state: ['', [Validators.required]],
      restaurant_country: ['', [Validators.required]],
      restaurant_price_for_two: ['', Validators.required],
      restaurant_pincode: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      restaurant_revenue: ['', Validators.required],
      restaurant_website: ['', [Validators.required, Validators.pattern("")]],
      restuarant_products: ['', [Validators.required]],
      representative_name: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      respresentative_country_code: [''],
      representative_contact: ['', [Validators.required, Validators.pattern("^(0|[6-9][0-9]*)$")]],
      representative_email: ['', [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+[\.]+[a-z]{2,4}$")]],

      openMon: { hour: 9, minute: 0 },
      openTue: [''],
      openWed: [''],
      openThur: [''],
      openFri: [''],
      openSat: [''],
      openSun: [''],

      extraopenMon: { hour: 17, minute: 0 },
      extraopenTue: [''],
      extraopenWed: [''],
      extraopenThur: [''],
      extraopenFri: [''],
      extraopenSat: [''],
      extraopenSun: [''],

      closeMon: { hour: 15, minute: 0 },
      closeTue: [''],
      closeWed: [''],
      closeThur: [''],
      closeFri: [''],
      closeSat: [''],
      closeSun: [''],

      extracloseMon: { hour: 23, minute: 0 },
      extracloseTue: [''],
      extracloseWed: [''],
      extracloseThur: [''],
      extracloseFri: [''],
      extracloseSat: [''],
      extracloseSun: [''],

    });

    this.EstablishmentType = this.fb.group({

      // Restaurants Establishment type name
      select_establishment_type: ['', Validators.required],
      establishment_more_info: ['', Validators.required],
      establishment_services: ['', Validators.required],
      establishment_rules_and_regulations: ['', Validators.required],
      establishment_place: ['', Validators.required],
      establishment_ambience: ['', Validators.required],
      establishment_around_places: ['', Validators.required]
    });

    this.LegalForm = this.fb.group({

      //Banks detail []
      restaurant_bank_details: ['', Validators.required],

      // Bank Details Form Control Names
      restaurant_account_no: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(20), Validators.pattern('^[0-9]{7,20}$')]],
      restaurant_account_type: ['', Validators.required],
      restaurant_bank_name: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      restaurant_branch_name: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      restaurant_bank_city: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      restaurant_bank_address: ['', Validators.required],
      restaurant_bank_ifsccode: ['', [Validators.required, Validators.pattern('^[A-Za-z]{4}0[A-Z0-9a-z]{6}$')]],
      restaurant_selected_docs: [[]],
      restaurant_gst_no: ['', Validators.required],
      restaurant_gst_file: ['', Validators.required],
      restaurant_pan_no: ['', Validators.required],
      restaurant_pan_image: ['', Validators.required],
      restaurant_fssai_certificate: ['', Validators.required],
      restaurant_fssai_image: ['', Validators.required],
      restaurant_noc_certificate: ['', Validators.required],
      restaurant_noc_certificate_file: ['', Validators.required],
      restaurant_liqLic: ['', Validators.required],
      restaurant_liqLic_file: ['', Validators.required],
      restauant_eatLic: ['', Validators.required],
      restauant_eatLic_file: ['', Validators.required],
      restaurant_musicLic: ['', Validators.required],
      restaurant_musicLic_file: ['', Validators.required],
      restaurant_tradeLic: ['', Validators.required],
      restaurant_tradeLic_file: ['', Validators.required],
      restaurant_cec: ['', Validators.required],
      restaurant_cec_file: ['', Validators.required],
      restaurant_shopAct: ['', Validators.required],
      restaurant_shopAct_file: ['', Validators.required],
      restaurant_sinageLic: ['', Validators.required],
      restaurant_sinageLic_file: ['', Validators.required],
      restaurant_liftClearance: ['', Validators.required],
      restaurant_liftClearance_file: ['', Validators.required],
      restuarant_healthLic: ['', Validators.required],
      restuarant_healthLic_file: ['', Validators.required],
      restaurant_legal_document: this.fb.array([])

    });



    this.MediaForm = this.fb.group({

      // other Legal Document Controls Name
      restaurant_logo: ['', Validators.required],
      restaurant_banner: ['', Validators.required],
      restaurant_pictures: ['', Validators.required],
      restaurant_videos: ['', Validators.required]

    });

    this.MenuMgmtForm = this.fb.group({


      //Restaurant Food [] contro l NAME
      restaurant_add_food: ['', Validators.required],


      //Restaurant Menu Management
      restaurant_select_cuisine: ['', Validators.required],
      restaurant_select_menu: ['', Validators.required],

      //Restaurant Liquor 
      restaurant_add_liquor_product: ['', Validators.required]

    });

    this.LayoutForm = this.fb.group({

      series: this.fb.array([])
    })


    // *****************OTHER ALUBM FORM GROUP
    this.otheralbum = this.fb.group({
      name: ['', Validators.required]
    })

    // IMAGES UPLOADER RESOPONSE (inside ngOnInit)

    this.response = '';

    // Drop Down Setting
    this.dropdownList = [
      'GST',
      'PAN Card',
      'FSSAI Certificate',
      'NOC',
      'Liquor License',
      'Eating House License',
      'Music license',
      'Trademark for Cafe License',
      'Certificate of Environmental Clearance (CEC)',
      'Shops and Establishment Act',
      'Signage license',
      'Lift Clearance',
      'Health/Trade License'

    ];

    // Poduct drop down list
    this.productList = [
      'Cheese',
      'Milk',
      'Bread',
      'Flour',
      'Refined Oil',
      'Spices'
    ]

    //     Trademark for Cafe License
    // Certificate of Environmental Clearance (CEC)
    // Shops and Establishment Act
    // Signage license
    // Lift Clearance 
    // Health/Trade License
    this.selectedItems = [
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' }
    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    // ***GST UPLOADER
    // this.gstUploader.response.subscribe(res => this.response = res);

    this.gstUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.gstResponse = response
      console.log('Gst File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.gstName = item._file.name
        this.gstStatus = true
      }

      this.ngxSpinner.hide()
      this.LegalForm.patchValue({
        restaurant_gst_file: this.gstResponse
      })
      console.log('final response', this.gstResponse);
      // this.gstResponse = this.gstResponse
      this.isgstTrash = false

    }


    //  **PAN UPLOAD
    this.panUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.panResponse = response
      console.log('Pan File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.panName = item._file.name
        this.panStatus = true
      }


      this.ngxSpinner.hide()
      console.log('response', this.panResponse);
      this.panResponse = response
      this.LegalForm.patchValue({
        restaurant_pan_image: response
      })
      // console.log('final response', this.panResponse['gstFilePhotoUrl']);
      // this.panResponse = this.panResponse['gstFilePhotoUrl']
      this.ispanTrash = false
    }

    // ** FSSAI UPLOAD

    this.fssaiUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.fssaiResponse = response

      console.log('Fssai File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.fssaiName = item._file.name
        this.fssaiStatus = true
      }


      this.ngxSpinner.hide()
      console.log('response', this.fssaiResponse);
      this.LegalForm.patchValue({
        restaurant_fssai_image: response
      })
      // console.log('final response', this.fssaiResponse['gstFilePhotoUrl']);
      // this.fssaiResponse = this.fssaiResponse['gstFilePhotoUrl']
      this.isFssaiTrash = false
    }

    // **NOC 

    this.nocUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ngxSpinner.hide()
      console.log('nocUploader response', response);
      this.nocResponse = response

      console.log('Noc File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.nocName = item._file.name
        this.nocStatus = true
      }

      this.ngxSpinner.hide()
      console.log('response', this.nocResponse);
      this.LegalForm.patchValue({
        restaurant_noc_certificate_file: response
      })

    }

    // **LiquorLicUploader 

    this.LiquorLicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ngxSpinner.hide()
      console.log('LiquorLicUploader response', response);
      this.liquorLic = response

      console.log('Noc File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.LiqLicName = item._file.name
        this.LiqLicStatus = true
      }

      this.ngxSpinner.hide()
      console.log('response', this.liquorLic);
      this.LegalForm.patchValue({
        restaurant_liqLic_file: response
      })

    }

    // **EatingHouseUploader 

    this.EatingHouseUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ngxSpinner.hide()
      console.log('EatingHouseUploader response', response);
      this.eatingHouseResponse = response

      console.log('Eating File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.eatLicName = item._file.name
        this.eatLicStatus = true
      }

      this.ngxSpinner.hide()
      console.log('response', this.eatingHouseResponse);
      this.LegalForm.patchValue({
        restauant_eatLic_file: response
      })
    }

    // **MusicLicUploader 

    this.MusicLicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ngxSpinner.hide()
      console.log('MusicLicUploader response', response);
      this.musicLicResponse = response

      console.log('Music File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.musicLicName = item._file.name
        this.musicLicStatus = true
      }

      this.ngxSpinner.hide()
      console.log('response', this.musicLicResponse);
      this.LegalForm.patchValue({
        restaurant_musicLic_file: response
      })

    }

    // **TrademarkUploader 

    this.TrademarkUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ngxSpinner.hide()
      console.log('TrademarkUploader response', response);
      this.trademarkResponse = response

      console.log('TradeMark File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.trademarkName = item._file.name
        this.trademarkStatus = true
      }

      this.ngxSpinner.hide()
      console.log('response', this.trademarkResponse);
      this.LegalForm.patchValue({
        restaurant_tradeLic_file: response
      })
    }

    // **CECUploader 

    this.CECUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ngxSpinner.hide()
      console.log('CECUploader response', response);
      this.cecResponse = response

      console.log('CEC File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.cedName = item._file.name
        this.cedStatus = true
      }

      this.ngxSpinner.hide()
      console.log('response', this.cecResponse);
      this.LegalForm.patchValue({
        restaurant_cec_file: response
      })
    }

    // **ShopsActUploader 

    this.ShopsActUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ngxSpinner.hide()
      console.log('ShopsActUploader response', response);
      this.shopActResponse = response

      console.log('Shop Act File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.shopActName = item._file.name
        this.shopActStatus = true
      }

      this.ngxSpinner.hide()
      console.log('response', this.shopActResponse);
      this.LegalForm.patchValue({
        restaurant_shopAct_file: response
      })
    }

    // **SignageUploader 

    this.SignageUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ngxSpinner.hide()
      console.log('SignageUploader response', response);
      this.sinageResponse = response

      console.log('sinage Act File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.sinageName = item._file.name
        this.sinageStatus = true
      }

      this.ngxSpinner.hide()
      console.log('response', this.sinageResponse);
      this.LegalForm.patchValue({
        restaurant_sinageLic_file: response
      })
    }

    // **LiftUploader 

    this.LiftUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ngxSpinner.hide()
      console.log('LiftUploader response', response);
      this.LiftResponse = response

      console.log('Lift Act File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.liftLicName = item._file.name
        this.liftLicStatus = true
      }

      this.ngxSpinner.hide()
      console.log('response', this.LiftResponse);
      this.LegalForm.patchValue({
        restaurant_liftClearance_file: response
      })
    }


    // **HealthTradeUploader 

    this.HealthTradeUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ngxSpinner.hide()
      console.log('HealthTradeUploader response', response);
      this.healthResponse = response

      console.log('Health File Name', item._file.name);
      console.log('uploading status', item.isUploaded)

      if (item.isUploaded) {
        this.healthLicName = item._file.name
        this.healthLicStatus = true
      }

      this.ngxSpinner.hide()
      console.log('response', this.healthResponse);
      this.LegalForm.patchValue({
        restuarant_healthLic_file: response
      })
    }







    // ** Other UPLOAD

    this.otherUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      if (status == 200) {
        if (this.otherResponse.length <= 4) {
          this.otherResponse.push(response)
          this.docs.patchValue({
            documentUrl: response
          })
        }

        Swal.fire('Document uploaded', '', 'success')
        this.docStatus[this.docFormArray.controls.indexOf(this.docs)].isDoc = true
        this.docAble = false
      }
      else {
        Swal.fire('Failed to upload', '', 'warning')
      }




      console.log('response', this.otherResponse);
      this.ngxSpinner.hide()
      // console.log('other response', this.otherResponse['gstFilePhotoUrl']);
      // this.docs.patchValue({
      //   documentUrl: this.otherResponse
      // })
      // this.otherResponse = this.otherResponse['gstFilePhotoUrl']
      // this.isOtherTrash = false

    }

    // **Other LOGO UPLOAD
    this.logoUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.logoResponse = response
      this.ngxSpinner.hide()
      console.log('Logo Response', response);

      console.log('Logo photo', this.logoResponse);

      this.logoHide = false

      Swal.fire('Logo Uploaded', '', 'success')



    }

    // **Other BannerUploader UPLOAD
    this.BannerUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.BannerResponse.push(response)

      console.log(' Banner Response', this.BannerResponse);
      this.bannerHide = false
      this.ngxSpinner.hide()
    }

    // **Other ImgAlbumUploader UPLOAD
    this.ImgAlbumUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ImgAlbumResponse.push(response)

      console.log('Images Response', this.ImgAlbumResponse);
      this.ngxSpinner.hide()
    }

    // Video collection upload

    this.VideoAlbumUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // this.VideoAlbumResponse = []
      console.log('FileUpload:uploaded:', item, status, response);
      this.VideoAlbumResponse.push(response)
      this.ngxSpinner.hide();
      console.log(' Video Response', this.VideoAlbumResponse);

    }



    // poc layout 
    this.PocLayoutForm = this.fb.group({

      multipleLayout: this.fb.array([])
    })


    this.filteredOptions = this.outletFormGrp.controls.name_of_building.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );


    this.filteredOptions1 = this.outletFormGrp.controls.multiplex_name.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter1(value))
      );

    this.filteredOptions2 = this.outletFormGrp.controls.chain_name.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter2(value))
      );


  }

  // ON Routing Back
  onBackRoute() {
    this.router.navigate(['/restaurants/add-outlet'])
  }




  // filter stirng function
  private _filter(value: string): string[] {

    if (value != undefined) {
      const filterValue = value.toLowerCase();

      return this.options.filter(option => option.toLowerCase().includes(filterValue));
    }

  }

  // filter cinema name
  private _filter1(value: string): string[] {
    if (value != undefined) {
      const filterValue = value.toLowerCase()
      return this.options1.filter(option => option.toLowerCase().includes(filterValue));
    }

  }
  // fileter chain name
  private _filter2(value: string): string[] {
    if (value != undefined) {
      const filterValue = value.toLowerCase()
      return this.options2.filter(option => option.toLowerCase().includes(filterValue));
    }

  }


  // On Change TYPE OF BUILDING
  onTypeOfBuilding(item) {
    this.outletFormGrp.patchValue({
      type_of_building: item.type
    })

    console.log('building type', item.type);

  }


  // On Selecting Building  Name

  onbuildName(item) {
    console.log('building name', item);


    this.regAddress.forEach(element => {
      if (element.infrastructure_name === item) {
        this.outletFormGrp.patchValue({
          address_of_building: element.google_add
        })
      }
    });

    // readOnlyTrue
    this.isReadOnly = true

  }


  // change read only status to false
  onChngReadOnly() {
    this.isReadOnly = false
  }
  // poc layout level 1
  initfirst() {
    return this.fb.group({

      qty_of_table: [],
      add_status: false,
      multipleSeries: this.fb.array([])
    })
  }

  // poc layout level 2
  initsecond() {
    return this.fb.group({
      series_type: '',
      fixed_intial: '',
      fixed_end: '',
      series_from_intial: [],
      series_to_initial: [],
      series_from_end: [],
      series_to_end: [],
      ct_ad_sts: false,
      table_count: 0
    })
  }

  // Add first LEVEL layout names
  addFirst() {
    const control = <FormArray>this.PocLayoutForm.controls['multipleLayout'];
    control.push(this.initfirst())
  }

  // Add second level series layout
  addSecond(i, obj) {



    console.log('quantity of table', this.qtyoftable);

    this.total_combinations = this.combinations + this.total_combinations
    console.log('total combinations', this.total_combinations);

    if (this.total_combinations < this.qtyoftable) {
      let tabvalue = obj.controls.qty_of_table.value
      this.tablesValue = obj.controls.qty_of_table.value


      if (this.tablesValue >= 1) {
        obj.controls.add_status.setValue(true)
        const control = (<FormArray>this.PocLayoutForm.controls['multipleLayout']).at(i).get('multipleSeries') as FormArray;
        control.push(this.initsecond())
      }
      else {
        alert('Please Enter Valid Number')
      }


    } else {
      alert('Created all combinations')
    }


    console.log('Layout Form poc', this.PocLayoutForm.value);

  }

  // submit layout form
  submitpoclay() {
    console.log(
      'poc layout form value', this.PocLayoutForm.value
    );

  }

  // Table quantity
  tableQty(obj) {
    this.qtyoftable = obj.controls.qty_of_table.value
    console.log('quantity of table', this.qtyoftable);
    // this.fstablecount = this.qtyoftable

  }

  // **********************************************

  // ************FIXED + SERIES*******************

  // **********************************************
  // FIXED + SERIES
  fixedSeries(obj) {


    console.log('object ser', obj);
    console.log('fixed', obj.value.fixed_intial)
    console.log('series starts', obj.value.series_from_intial)
    console.log('series to', obj.value.series_to_initial)



    let fix = obj.value.fixed_intial
    let seriesfrom = obj.value.series_from_intial
    let seriesto = obj.value.series_to_initial

    let range = seriesto - seriesfrom
    range = range + 1
    let tempcombo = this.total_combinations
    if (tempcombo > this.qtyoftable) {
      obj.controls.series_to_initial.reset()
      this.fstablecount = 0
    }
    else {
      for (let i = seriesfrom; i <= seriesto; i++) {
        let result = fix + i
        console.log('Result', result);
        this.fstablecount = i
      }
    }



    console.log('fixed series object', obj.controls.table_count.setValue(range));



    this.combinations = <number>obj.controls.table_count.value

    console.log('combinations ', this.combinations);




  }

  // FSCREATE
  createfs(obj) {
    // create hidden true edit hidden false
    obj.controls.ct_ad_sts.setValue(true)
  }


  // edit table quantity
  onEditqtyTable(i, obj) {
    obj.controls.add_status.setValue(false)



    const control = (<FormArray>this.PocLayoutForm.controls['multipleLayout']).at(i).get('multipleSeries') as FormArray;
    control.clear()
    this.total_combinations = 0
  }


  // **********************************************

  // ************SERIES+FIXED*******************

  // **********************************************

  seriesfixed(obj) {
    console.log('object ser', obj);
    console.log('fixed', obj.value.fixed_intial)
    console.log('series starts', obj.value.series_from_intial)
    console.log('series to', obj.value.series_to_initial)



    let fix = obj.value.fixed_intial
    let seriesfrom = obj.value.series_from_intial
    let seriesto = obj.value.series_to_initial

    let range = seriesto - seriesfrom
    range = range + 1
    let tempcombo = this.total_combinations
    if (tempcombo > this.qtyoftable) {
      obj.controls.series_to_initial.reset()
      this.fstablecount = 0
    }
    else {
      for (let i = seriesfrom; i <= seriesto; i++) {
        let result = fix + i
        console.log('Result', result);
        this.fstablecount = i
      }
    }



    console.log('fixed series object', obj.controls.table_count.setValue(range));



    this.combinations = <number>obj.controls.table_count.value

    console.log('combinations ', this.combinations);

  }

  // ************************
  // ngONinit FInished
  // **************************

  ngAfterViewInit() {
    this.getPlaceAutocomplete();
  }  // FORM ARRAY FOR SERIES 
  get seriesForm() {
    return this.LayoutForm.controls.series as FormArray
  }


  // CREATE FORMGROUP FOR SERIES ARRAY FORM
  addseriesForm() {
    const myseries = this.fb.group({
      series_type: '',
      fixed_intial: '',
      fixed_end: '',
      series_from_intial: [],
      series_to_initial: [],
      series_from_end: [],
      series_to_end: [],
      ct_ad_sts: false
    })

    this.seriesForm.push(myseries)


  }





  // Google Places maps

  private getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
      {
        // componentRestrictions: { country: 'IN' },
        types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
      });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      this.invokeEvent(place);


    });




  }

  // Google Invoke Event
  invokeEvent(place: Object) {
    this.setAddress.emit(place);
    console.log('place', place['address_components']);
    console.log('Google Data', place);
    this.googleplace = place



    let components = place['address_components']

    // Below code is to extract Pincode
    components.forEach(element => {

      // // Building Name

      if (element.types.includes("premise")) {
        console.log("Premise", element.long_name)
        this.RestaurantInfo.patchValue({
          restaurant_building: element.long_name
        })
      }


      // // Landmark Name


      if (element.types.includes("adr_address", "neighborhood", "floor")) {
        console.log("LandMark", element.long_name)
        this.RestaurantInfo.patchValue({
          restaurant_landmark: element.long_name
        })
      }



      // // Street No. Road Name

      if (element.types.includes("route")) {
        console.log("street or road", element.long_name)
        this.RestaurantInfo.patchValue({
          restaurant_street: element.long_name
        })
      }


      // // Sub Locality 

      if (element.types.includes("sublocality_level_2", "sublocality", "political")) {
        console.log("Sub Locality", element.long_name);
        this.RestaurantInfo.patchValue({
          restaurant_sublocal: element.long_name
        })
      }




      //  Locality 

      if (element.types.includes("sublocality_level_1", "locality")) {
        console.log("Locality", element.long_name);
        this.RestaurantInfo.patchValue({
          restaurant_locality: element.long_name
        })
      }

      // // Town/Village/Colony/Area 

      if (element.types.includes("administrative_area_level_3", "locality", "political")) {
        console.log("Town/Village/Colony/Area", element.long_name);
        this.RestaurantInfo.patchValue({
          restaurant_town: element.long_name
        })
      }


      //   // City

      if (element.types.includes("administrative_area_level_2")) {
        console.log("City", element.long_name);
        this.RestaurantInfo.patchValue({
          restaurant_city: element.long_name
        })
      }

      //   // State

      if (element.types.includes("administrative_area_level_1")) {
        console.log("State", element.long_name);
        this.RestaurantInfo.patchValue({
          restaurant_state: element.long_name
        })
      }


      //   // Country

      if (element.types.includes("country")) {
        console.log("Coutry", element.long_name);
        this.RestaurantInfo.patchValue({
          restaurant_country: element.long_name
        })
      }

      // Pincode
      if (element.types.includes("postal_code")) {
        console.log('Pincode', element.long_name);
        this.RestaurantInfo.patchValue({
          restaurant_pincode: element.long_name
        })

      }



    });




  }

  // Google Address Reset
  googleAddress() {

    this.lat = this.googleplace.geometry.location.lat()
    this.lng = this.googleplace.geometry.location.lng()

    // this.RestaurantInfo.controls.restaurant_building.reset()
    // this.RestaurantInfo.controls.restaurant_landmark.reset()
    // this.RestaurantInfo.controls.restaurant_street.reset()
    // this.RestaurantInfo.controls.restaurant_sublocal.reset()
    // this.RestaurantInfo.controls.restaurant_locality.reset()
    // this.RestaurantInfo.controls.restaurant_town.reset()
    // this.RestaurantInfo.controls.restaurant_city.reset()
    // this.RestaurantInfo.controls.restaurant_state.reset()
    // this.RestaurantInfo.controls.restaurant_country.reset()

  }


  // QR CODE HIDE SHOW
  onQRgen(a) {
    // this.isQRshow = false


    console.log('qrcode gen', a.title, a.qrcode);

    // a.qrcode = 5

    console.log(this.LayoutForm.controls['qrquantity'].value);


    a.qrcode = this.LayoutForm.controls['qrquantity'].value


    a.qrstatus = true


    let tableObj = {
      enterseats: this.LayoutForm.controls['enterseats'].value,
      shapes: this.LayoutForm.controls['shapes'].value,
      type: this.LayoutForm.controls['type'].value,
      status: false,
      asso_qr: ''
    }

    for (let i = 0; i < a.qrcode; i++) {
      a.qrcodeArr.push('QR-' + i)
      a.table.push(tableObj)
    }




    console.log('Layout arr', this.LayoutsArr);
    this.LayoutForm.controls['qrquantity'].reset()
    this.LayoutForm.controls['enterseats'].reset(),
      this.LayoutForm.controls['shapes'].reset(),
      this.LayoutForm.controls['type'].reset()
  }


  //  onCreateTables(obj){
  //       console.log('Layout Form', this.LayoutForm.value);
  //       console.log('layout a ',obj);


  //       let tableObj = {
  //         enterseats: this.LayoutForm.controls['enterseats'].value,
  //         shapes: this.LayoutForm.controls['shapes'].value,
  //         type: this.LayoutForm.controls['type'].value,
  //         status: false
  //       }


  //       for(let i = 0; i<obj.qrcode;i++){
  //         obj.table.push(tableObj)
  //         console.log('loop',i);

  //       }


  //       console.log('Layout array', this.LayoutsArr);

  //        this.LayoutForm.controls['enterseats'].reset(),
  //        this.LayoutForm.controls['shapes'].reset(),
  //        this.LayoutForm.controls['type'].reset()

  //     }

  //*******************/
  // QR CHECK SELECTED
  //*******************/
  onChkQRcode(evt, value, obj, table) {

    let qridx = obj.qrcodeArr.indexOf(value)
    // console.log('status',obj.table[].status);
    // console.log('qridx',qridx);

    if (evt.target.checked) {
      table[qridx].status = true
      table[qridx].asso_qr = value
    }
    else {
      table[qridx].status = false
      table[qridx].asso_qr = ''
    }

    console.log('checked qr codes', this.LayoutsArr);


  }



  //*******************/
  // QR Back
  //*******************/

  onQRback(value) {
    console.log('back', value)
    value.qrstatus = false
  }

  // ***********************  
  // **ADD MORE CHIPS FUNCTIONS*****  
  // ***********************

  // AddChipAmbiencesComponent,
  // AddChipMoreinfoComponent,
  // AddChipRulesComponent,
  // AddServicesChipsComponent
  // /ADD MORE CHIPS AMBIENCES
  addChipAM(a) {

    const dialogRef = this.matDialog.open(AddChipAmbiencesComponent, {
      width: '440px',
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('AMbience result', result);

    })
  }
  // ADD MORE CHIPS RULES
  addChipRL(a) {
    console.log('a', a);

    const dialogRef = this.matDialog.open(AddChipRulesComponent, {
      width: '440px',
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('rules result', result);

    })

  }
  // ADD MORE INFO CHIPS
  addChipMI(item) {

    console.log('more info item', item);


    const dialogRef = this.matDialog.open(AddChipMoreinfoComponent, {
      width: '440px',
      data: item
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('more info result', result);
      this.restServ.getAllMoreInfo().subscribe(data => {

        this.barMoreInfo = data['data']
        console.log('more infos form db: ', this.barMoreInfo);

      })

    })

  }
  //ADD SERVICE CHIPS
  addChipSV(a) {
    const dialogRef = this.matDialog.open(AddChipServicesComponent, {
      width: '440px',
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('services result', result);

    })

  }


  // ***********************on add chips**********************
  onAddservicechip() {
    console.log('add meet upo');

  }
  // ****************validations starts******************
  onAddHr(val) {
    if (val == 'mon') {
      this.addmonHr = false
    }

    if (val == 'tue') {
      this.addtueHr = false
    }
    if (val == 'wed') {
      this.addwedHr = false
    }
    if (val == 'thur') {
      this.addthurHr = false
    }
    if (val == 'fri') {
      this.addfriHr = false
    }
    if (val == 'sat') {
      this.addsatHr = false
    }
    if (val == 'sun') {
      this.addsunHr = false
    }

  }



  onRemoveAddHr(val) {
    if (val == 'mon') {
      this.addmonHr = true
    }

    if (val == 'tue') {
      this.addtueHr = true
    }
    if (val == 'wed') {
      this.addwedHr = true
    }
    if (val == 'thur') {
      this.addthurHr = true
    }
    if (val == 'fri') {
      this.addfriHr = true
    }
    if (val == 'sat') {
      this.addsatHr = true
    }
    if (val == 'sun') {
      this.addsunHr = true
    }

  }
  onEmailKeyup() {
    let abc = this.RestaurantInfo.controls['restaurant_email'].value
    this.RestaurantInfo.patchValue({
      restaurant_email: abc.trim().toLowerCase().replace(/\s/g, "")

    })


    console.log('restauant email', this.RestaurantInfo.controls['restaurant_email'].value);
  }
  onRepEmailKeyup() {
    let abc = this.RestaurantInfo.controls['representative_email'].value
    this.RestaurantInfo.patchValue({
      representative_email: abc.trim().toLowerCase().replace(/\s/g, "")

    })


    console.log('restauant email', this.RestaurantInfo.controls['restaurant_email'].value);
  }

  // pincode keyup
  onkeyUpPin(evt) {

    if (evt.keyCode < 96 || evt.keyCode > 105) {
      this.RestaurantInfo.controls.restaurant_pincode.reset()
    }

  }



  // on select drop down legal docs
  onItemSelect(item: any) {
    console.log(item);




    this.selectedDocs = this.LegalForm.controls['restaurant_selected_docs'].value
    console.log(this.selectedDocs)
    console.log(this.LegalForm.controls['restaurant_selected_docs'].value)

  }
  onSelectAll(items: any) {
    console.log(items);
    this.selectedDocs = items
    console.log(this.selectedDocs);
    console.log(this.LegalForm.controls['restaurant_selected_docs'].value)
  }

  onDeSelect(item: any) {
    console.log('deselect', item);
    this.selectedDocs.splice(this.selectedDocs.indexOf(item), 1)

    console.log(this.selectedDocs);
    console.log(this.LegalForm.controls['restaurant_selected_docs'].value)
  }

  onDeSelectAll(items: any) {
    console.log('deselect all', items);
    this.selectedDocs = []
    console.log(this.selectedDocs);

  }

  // onGENERATE NEW WEBSITE

  onGenerateWebsite() {
    const dialogRef = this.matDialog.open(GenerateWebsiteComponent, {
      width: 'auto'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);


      if (result.websitename == '' || result.websitename == undefined) {
        this.webReadOnly = false
        this.websiteId = ''
        this.RestaurantInfo.patchValue({
          restaurant_website: ''
        })
      }
      else {
        this.webReadOnly = true
        this.websiteId = result.websiteId
        this.RestaurantInfo.patchValue({
          restaurant_website: result.websitename
        })
      }


    })
  }
  // on country code patch
  onCountryCode(evt) {

    console.log('country code', evt)
    this.isCountryCodeSelected = evt

    this.RestaurantInfo.patchValue({
      respresentative_country_code: this.isCountryCodeSelected
    })
    console.log(
      'selected country', this.isCountryCodeSelected
    );

  }

  // loading spinner
  loading() {
    this.ngxSpinner.show()
  }

  // ////////////////////////////////////*********************************/////////////////////////////////////////
  // *************************************** ESTABLISHMENT STEPPER FUCNTIONS**********************************************//
  // ////////////////////////////////////*********************************/////////////////////////////////////////


  // On SELECTED ESTE NODE 
  selectEstNode(data) {
    console.log('data', data)
    // self
    if (!this.selectedEstTypes.includes(data.label)) {
      this.selectedEstTypes.push(data.label)
    }

  }

  selectEstChild(data: any) {
    // child
    console.log('data child', data.children)
    if (data.children) {
      data.children.forEach(element => {
        console.log(element.label)
        if (!this.selectedEstTypes.includes(element.label)) {
          this.selectedEstTypes.push(element.label)
        }
        data = element
        this.selectEstChild(data)

      });
    }

  }


  // IF PARENT NODE
  selectParentNode(data) {
    if (data.parent) {
      if (!this.selectedEstTypes.includes(data.parent.label)) {
        this.selectedEstTypes.push(data.parent.label)
      }
      data = data.parent
      this.selectParentNode(data)
    }
  }

  // UNSELECTING NODE
  unSelectEstNode(data) {
    console.log('data', data)
    // self
    if (this.selectedEstTypes.includes(data.label)) {
      this.selectedEstTypes.splice(this.selectedEstTypes.indexOf(data.label), 1)
    }
  }

  // UNSELECTING CHILD NODE
  unSelectChild(data) {
    // parent
    if (data.children) {
      data.children.forEach(element => {
        if (this.selectedEstTypes.includes(element.label)) {
          this.selectedEstTypes.splice(this.selectedEstTypes.indexOf(element.label), 1)
        }
        data = element
        this.unSelectChild(data)
      });
    }
  }

  //unselecteing paraent
  // IF PARENT NODE
  unSelectParentNode(data) {
    if (data.parent) {
      if (this.selectedEstTypes.includes(data.parent.label)) {
        this.selectedEstTypes.splice(this.selectedEstTypes.indexOf(data.parent.label), 1)
      }
      if (data.parent.partialSelected) {
        this.selectedEstTypes.push(data.parent.label)
      }
      data = data.parent
      this.unSelectParentNode(data)
    }
  }

  // On Removing Already Added Chip in the List
  rmvMoreInfoAvailChips(item, arr, obj) {

    console.log('object', obj)
    console.log('item  ', item)
    console.log('array ', arr)


    if (obj.moreinfo.includes(item)) {
      obj.moreinfo.splice(obj.moreinfo.indexOf(item), 1)

      if (obj.moreinfo.length == 0) {
        this.allMoreInfos.splice(this.allMoreInfos.indexOf(obj), 1)
      }
      this.restServ.putchipstoMore(obj._id, obj).subscribe(res => {
        console.log('respo', res);

      })

    }


    console.log('select selectedMoreInfos', this.selectedMoreInfos)


  }

  // On Removing with title and Chips in the List
  rmvMoreInfoWithTitle(obj) {
    console.log('object', obj)

    if (this.allMoreInfos.includes(obj)) {
      this.allMoreInfos.splice(this.allMoreInfos.indexOf(obj), 1)
    }

    this.restServ.putchipstoMore(obj._id, obj).subscribe(res => {
      console.log('respo', res);

    })

  }


  // On Removing Already Added Serivices
  rmvServicesAvailChips(item, arr, obj) {
    console.log('object', obj)
    console.log('item  ', item)
    console.log('array ', arr)


    if (obj.services.includes(item)) {
      obj.services.splice(obj.services.indexOf(item), 1)

      if (obj.services.length == 0) {
        this.serviceMoreInfo.splice(this.serviceMoreInfo.indexOf(obj), 1)
      }

      this.restServ.putchipstoService(obj._id, obj).subscribe(res => {
        console.log('respo', res);

      })
    }
  }

  // ON REMVOING SERVICE WITH TITLE
  rmvServiceWithTitle(obj) {
    console.log('object', obj)

    if (this.serviceMoreInfo.includes(obj)) {
      this.serviceMoreInfo.splice(this.serviceMoreInfo.indexOf(obj), 1)
    }

    this.restServ.putchipstoService(obj._id, obj).subscribe(res => {
      console.log('respo', res);

    })

  }

  // ON REMOVING AVAILABLE Rules CHIPS
  rmvRulesAvailChips(item, arr, obj) {
    console.log('object', obj)
    console.log('item  ', item)
    console.log('array ', arr)

    if (obj.ruleandregulations.includes(item)) {
      obj.ruleandregulations.splice(obj.ruleandregulations.indexOf(item), 1)

      if (obj.ruleandregulations.length == 0) {
        this.rulesMoreInfo.splice(this.rulesMoreInfo.indexOf(obj), 1)
      }

      this.restServ.putchipstoRules(obj._id, obj).subscribe(res => {
        console.log('respo', res);

      })
    }
  }

  // ON REMOVING Rules with TITLE
  rmvRulesWithTitle(obj) {
    console.log('object', obj)

    if (this.rulesMoreInfo.includes(obj)) {
      this.rulesMoreInfo.splice(this.rulesMoreInfo.indexOf(obj), 1)
    }

    this.restServ.putchipstoRules(obj._id, obj).subscribe(res => {
      console.log('respo', res);

    })
  }

  // ON REMOVING AVAILABLE AMBIENCE CHIPS
  rmvAmbienceAvailChips(item, arr, obj) {
    console.log('object', obj)
    console.log('item  ', item)
    console.log('array ', arr)

    if (obj.ambiences.includes(item)) {
      obj.ambiences.splice(obj.ambiences.indexOf(item), 1)

      if (obj.ambiences.length == 0) {
        this.ambienceMoreInfo.splice(this.ambienceMoreInfo.indexOf(obj), 1)
      }

      this.restServ.putchipstoAmbience(obj._id, obj).subscribe(res => {
        console.log('respo', res);

      })
    }
  }

  // ON REMOVING ABIENCES WITH TITLE
  rmvAmbienceWithTitle(obj) {
    console.log('object', obj)

    if (this.ambienceMoreInfo.includes(obj)) {
      this.ambienceMoreInfo.splice(this.ambienceMoreInfo.indexOf(obj), 1)
    }

    this.restServ.putchipstoAmbience(obj._id, obj).subscribe(res => {
      console.log('respo', res);

    })
  }
  // On Add More Info open dialog

  openAddMoreInfo() {
    const dialogRef = this.matDialog.open(AddMoreInfoComponent, {
      width: '540px',
      // data: this.allMoreInfos
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result of moreInfo', result);
      this.restServ.getAllMoreInfos().subscribe(data => {
        console.log('get all more infos for chips data', data);
        this.allMoreInfos = data['data']
      })
    })
  }



  // on click over establishment

  estSelected() {
    // alert('selected establismeent')
    // console.log('select establsiment');
    this.isLinear = false
  }

  // banners successfully uploaded
  banSuccessUp() {
    Swal.fire('Banner Successfully Uploaded', '', 'success')
  }

  // banners successfully uploaded


  // empty image collection
  emptyImgCollection() {
    this.ImgAlbumResponse = []
    console.log('empty img collection');

  }

  // empty vdo collection for next
  emptyVdoCollection() {
    this.VideoAlbumResponse = []
    console.log('empty video collection')
  }

  // images media json creation

  imgMediaJson(title, index) {
    console.log('index no.', index);
    let realIndex
    this.album_image.forEach(element => {
      if (title == element.name) {
        console.log('real index', this.album_image.indexOf(element));

        realIndex = this.album_image.indexOf(element)

      }
    });


    if (realIndex < this.album_image.length) {
      console.log('album images with index', this.album_image[realIndex]);

      let presentFiles = this.album_image[realIndex].fileurl
      console.log('presentFiles', presentFiles);

      this.album_image[realIndex].fileurl = this.ImgAlbumResponse

      presentFiles.forEach(element => {
        this.album_image[realIndex].fileurl.push(element)

      });


      console.log('AFter push', this.album_image);


    }
    else {
      this.album_image.push({ name: title, fileurl: this.ImgAlbumResponse })

      this.albumNames.splice(this.albumNames.indexOf(title), 1)
      // console.log('albumName',this.albumNames)

    }

    console.log('title', title);
    console.log('collection images', this.ImgAlbumResponse);

    // this.albumNames.splice(this.albumNames.indexOf(title),1)
    // console.log('Albums', this.albumNames);





    console.log('album_image', this.album_image)
    console.log('albumName', this.albumNames);


    Swal.fire(title + ' Album Successfully Uploaded', '', 'success')
  }

  // videos media json creation

  vdoMediaJson(title) {

    let realvdoIndex

    this.album_video.forEach(element => {
      if (title == element.name) {
        console.log('real index', this.album_video.indexOf(element));

        realvdoIndex = this.album_video.indexOf(element)

      }
    });

    if (realvdoIndex < this.album_video.length) {
      console.log('album video with index', this.album_video[realvdoIndex]);

      let presentFiles = this.album_video[realvdoIndex].fileurl
      console.log('presentFiles', presentFiles);

      this.album_video[realvdoIndex].fileurl = this.VideoAlbumResponse

      presentFiles.forEach(element => {
        this.album_video[realvdoIndex].fileurl.push(element)

      });

      console.log('album videos json', this.album_video)

    }
    else {
      this.album_video.push({ name: title, fileurl: this.VideoAlbumResponse })

      this.createdVdoAlbum.splice(this.createdVdoAlbum.indexOf(title), 1)
      // console.log('albumName',this.albumNames)

    }



    console.log('album videos json', this.album_video)

    Swal.fire(title + ' Album Successfully Uploaded', '', 'success')
  }

  // ***************************
  // Media Stepper Save  
  // ***************************
  onSaveMediaInfo() {

    let mediaObj = {
      resto_logo: this.logoResponse,
      resto_banner: this.BannerResponse,
      album_image: this.album_image,
      album_video: this.album_video
    }

    this.testObj = mediaObj
    console.log('media object ', mediaObj);
    // this.OutletId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.putMediaObjtoResto(this.OutletId, { media: mediaObj, userid: this.userid }).subscribe(res => {
      console.log('resposne after media uploade', res);
      if (res['sucess']) {
        Swal.fire('Media Uploaded Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Upload', '', 'warning')
      }

    })

  }



  // on get all SERVICES ALL CHIPS
  getAllServicesChips() {
    this.restServ.getAllServicesInfo().subscribe(data => {
      this.serviceMoreInfo = data['data']
      console.log('services from db', this.serviceMoreInfo);

    })
  }

  // Geta all rules all chips
  getAllRulesChips() {

    this.restServ.getAllRulesInfo().subscribe(data => {
      this.rulesMoreInfo = data['data']
      console.log('rules from db', this.rulesMoreInfo);

    })


  }

  // get all ambience all chips
  getAllAmbienceChips() {

    this.restServ.getAllAmbienceInfo().subscribe(data => {
      this.ambienceMoreInfo = data['data']
      console.log('ambience from db', this.ambienceMoreInfo);

    })
  }

  // on view add table controllers
  onViewTableCtrl() {
    this.isAddTable = false
    this.isAddChair = true
    this.isBillCounter = true
    this.isKitchen = true
  }

  // on view add chairs controllers
  onViewChairCtrl() {
    this.isAddTable = true
    this.isAddChair = false
    this.isBillCounter = true
    this.isKitchen = true
  }

  // on view add billing counter controllers
  onViewBillingCtrl() {
    this.isAddTable = true
    this.isAddChair = true
    this.isBillCounter = false
    this.isKitchen = true
  }

  // on view add kitchen controllers
  onViewKitchenCtrl() {
    this.isAddTable = true
    this.isAddChair = true
    this.isBillCounter = true
    this.isKitchen = false
  }

  // get all morinfos
  getAllMoreInfos() {
    this.restServ.getAllMoreInfos().subscribe(data => {
      console.log('get all more infos for chips data', data);
      this.allMoreInfos = data['data']
    })
  }


  // on Add Services open dialog
  openAddServicesChips() {
    const dialogRef = this.matDialog.open(AddServicesChipsComponent, {
      width: '540px',
      // data: this.serviceMoreInfo
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllServicesChips()
    })
  }

  // on ADD rules open dialog
  openAddRulesChips() {
    const dialogRef = this.matDialog.open(AddRulesChipsComponent, {
      width: '540px',
      // data: this.rulesMoreInfo
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRulesChips()
    })
  }

  // on Add ambience open dialog
  openAddAmbienceChips() {
    const dialogRef = this.matDialog.open(AddAmbienceChipsComponent, {
      width: '540px',
      // data: this.ambienceMoreInfo
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllAmbienceChips()
    })
  }
  // on Add dialog of Liquor

  openAddLiquorRoot() {
    const dialogRef = this.matDialog.open(AddRootLiquorPopComponent, {
      width: '540px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getRootDrinkTypes()
    })

  }

  // open add dialog liqour varient
  openAddLiquorVarient(root) {
    let root_id = root['_id']
    const dialogRef = this.matDialog.open(AddVarientPopComponent, {
      width: '540px',
      data: root_id
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('reslut', result);
      this.getRootDrinkTypes()
    })
  }

  // open add dialog SUB liquor varient
  openAddSubVarient(root, cat) {
    const dialogRef = this.matDialog.open(AddSubVarientPopComponent, {
      width: '540px',
      data: { root: root['_id'], cat: cat['_id'] }
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('reslut', result);
      this.getRootDrinkTypes()
    })
  }

  // open add dialog super liquor child
  openAddLiquorSuperChild(root, child) {
    const dialogRef = this.matDialog.open(AddSuperChildPopComponent, {
      width: '540px',
      data: { root: root._id, child: child._id }

    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('reslut', result);
      this.getRootDrinkTypes()
    })
  }
  // Add Bill Counter
  OnAddBillCounter(a) {

    const dialogRef = this.matDialog.open(AddBillcounterPopupComponent, {
      width: "540px",
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result after add bill counter', result);

    })
  }

  // open popup to add chair to layout
  onAddChairtoLayout(a) {
    const dialogRef = this.matDialog.open(AddChairPopupComponent, {
      width: "540px",
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result after add chair', result);

    })
  }

  // open popup for add kitchen
  onAddKitchentoLayout(a) {
    const dialogRef = this.matDialog.open(AddKitchenPopupComponent, {
      width: "540px",
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result after add kitchen', result);

      console.log('layout', this.LayoutsArr)
    })
  }

  // open add child child menu dialog

  openMenuChildChild(root, child) {


    const dialogRef = this.matDialog.open(AddCcMenuComponent,
      {
        width: "420px",
        data: { root: root._id, child: child._id }
      })
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRestoMenu()
    })
  }

  // open add sub child menu dialog
  openMenuChild(root, cat) {
    let root_id = root._id
    let cat_id = cat._id
    const dialogRef = this.matDialog.open(AddSubcatMenuComponent,
      {
        width: '420px',
        data: { 'root': root_id, 'cat': cat_id }
      })
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRestoMenu()
    })
  }

  // open add category menu dialog
  openMenuCate(root) {
    let root_id = root._id
    const dialogRef = this.matDialog.open(AddCateMenuComponent,
      {
        width: '420px',
        data: root_id
      })
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRestoMenu()
    })
  }
  // open add root menu dialog
  openAddRootMenu() {
    const dialogRef = this.matDialog.open(AddRootMenuComponent, {
      width: '420px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRestoMenu()
    })
  }

  // on FINAL SUBMIT
  onlayoutFinalSubmit() {
    // this.OutletId = '5e3c2bc9a588df0cb1f6d323'

    console.log("rect arr", this.LayoutsArr);

    let layoutObj = { rectArr: this.LayoutsArr }

    console.log('rectarallll', layoutObj);

    this.restServ.PutLayoutFinalSubmit(this.OutletId, { layout: layoutObj, userid: this.userid }).subscribe(res => {
      if (res['sucess']) {
        Swal.fire('Restaurant Finally Submitted', '', 'success')
        this.router.navigate(['/restaurants'])
      }
      else {
        Swal.fire('Failed to Added', '', 'warning')
      }
    })
  }

  // ON SECLECT LIQ;UOR cATEGORY
  onSelectRootDrink(root) {
    console.log('root', root._id);
    if (!this.selectedRootDrinksName.includes(root.rootCategoryName)) {
      this.selectedRootDrinksName.push(root.rootCategoryName)
    }
    else {
      this.selectedRootDrinksName.splice(this.selectedRootDrinksName.indexOf(root.rootCategoryName), 1)
    }

    console.log('select root drinks name', this.selectedRootDrinksName);

  }

  // ON Select Liqour VARIENT
  onSelectDrinkVarient(root, cat) {
    console.log('root', root._id);
    console.log('cat', cat._id);

    if (!this.selectedDrinkVarient.includes(cat.categoryName)) {
      this.selectedDrinkVarient.push(cat.categoryName)
    }
    else {
      this.selectedDrinkVarient.splice(this.selectedDrinkVarient.indexOf(cat.categoryName), 1)
    }

    console.log('select root drinks name', this.selectedDrinkVarient);

  }

  // ON SELECT LIQOUR CHILD
  onSelectDrinkSubVarient(root, cat, child) {
    console.log('root', root._id);
    console.log('cat', cat._id);
    console.log('child', child._id);

    if (!this.selectedDrinkSubVarient.includes(child.childCategoryName)) {
      this.selectedDrinkSubVarient.push(child.childCategoryName)
    }
    else {
      this.selectedDrinkVarient.splice(this.selectedDrinkSubVarient.indexOf(child.childCategoryName), 1)
    }

    console.log('select root drinks name', this.selectedDrinkSubVarient);
  }

  // ON SELECT Liquor cc
  onSelectDrinkSubSub(root, cat, child, cc) {
    console.log('root', root._id);
    console.log('cat', cat._id);
    console.log('child', child._id);
    console.log('cc', cc);

    if (!this.selectedDrinkSuperChild.includes(cc.childChildCategoryName)) {
      this.selectedDrinkSuperChild.push(cc.childChildCategoryName)
    }
    else {
      this.selectedDrinkVarient.splice(this.selectedDrinkSuperChild.indexOf(cc.childChildCategoryName), 1)
    }

    console.log('select root drinks name', this.selectedDrinkSuperChild);

  }

  // ON SELECET MENU ROOT
  onSelectMenuRoot(root) {
    console.log('menu root id', root._id);

    if (!this.selectedMenuRoot.includes(root.rootCategoryName)) {
      this.selectedMenuRoot.push(root.rootCategoryName)
    }
    else {
      this.selectedMenuRoot.splice(this.selectedMenuRoot.indexOf(root.rootCategoryName), 1)
    }
    console.log('selected root menu', this.selectedMenuRoot);

  }
  // ON SELECT MENU CATEGORY
  onSelectMenuCat(root, cat) {
    console.log('menu root id', root._id)
    console.log('menu cat._id', cat._id);

    if (!this.selectedMenuCategory.includes(cat.categoryName)) {
      this.selectedMenuCategory.push(cat.categoryName)
    }
    else {
      this.selectedMenuCategory.splice(this.selectedMenuCategory.indexOf(cat.categoryName), 1)
    }

    console.log('Selected Menu Category', this.selectedMenuCategory);

  }

  // ON SELECT MENU CHILD
  onSelectMenuChild(root, cat, child) {
    console.log('menu root id', root._id)
    console.log('menu cat._id', cat._id);
    console.log('menu child._id', child._id);

    if (!this.selectedChildCategory.includes(child.childCategoryName)) {
      this.selectedChildCategory.push(child.childCategoryName)
    }
    else {
      this.selectedChildCategory.splice(this.selectedChildCategory.indexOf(child.childCategoryName), 1)
    }

    console.log("selected child category", this.selectedChildCategory);


  }

  // ON select Menu CC
  onSelectMenuCC(root, cat, child, cc) {
    console.log('menu root id', root._id)
    console.log('menu cat._id', cat._id);
    console.log('menu child._id', child._id);
    console.log('menu cc id', cc._id);

    if (!this.selectedMenucc.includes(cc.childChildCategoryName)) {
      this.selectedMenucc.push(cc.childChildCategoryName)
    }
    else {
      this.selectedMenucc.splice(this.selectedMenucc.indexOf(cc.childChildCategoryName), 1)
    }

    console.log('selected menu child child', this.selectedMenucc);


  }

  //  On sAVE MENU PAGE
  onMenuSave() {
    console.log("selected menu root", this.selectedMenuRoot)
    console.log("selected menu category", this.selectedMenuCategory)
    console.log("selected menu child", this.selectedChildCategory)
    console.log("selected menu root", this.selectedMenucc)


    var menuObj = {
      rootCategories: this.selectedMenuRoot,
      categories: this.selectedMenuCategory,
      childCategories: this.selectedChildCategory,
      childchildCategories: this.selectedMenucc
    }

    console.log('resto id', this.OutletId);
    console.log('menuObj', menuObj);

    // posting final menu
    this.restServ.putSelectedMenu(this.OutletId, { menu: menuObj, userid: this.userid }).subscribe(res => {
      console.log('response', res);
      if (res['sucess']) {
        Swal.fire('Menu added Successfully', '', 'success')
      }
      else {
        Swal.fire(
          'Failed to add Menu', '', 'warning'
        )
      }

    })


  }
  // On Select ROOT

  onSelectRoot(root, evt) {
    // console.log("root", root._id, evt);


    console.log('log', root.rootCategoryName);

    if (!this.selectedRootNames.includes(root.rootCategoryName)) {
      this.selectedRootNames.push(root.rootCategoryName)
    }
    else {
      this.selectedRootNames.splice(this.selectedRootNames.indexOf(root.rootCategoryName), 1)
    }

    console.log('Selected Root Names', this.selectedRootNames);
  }

  // on Select Category()
  onSelectCategory(root, cat) {
    console.log("root", root._id)
    console.log("cat", cat._id);


    if (!this.selectedCatNames.includes(cat.categoryName)) {
      this.selectedCatNames.push(cat.categoryName)
    }
    else {
      this.selectedCatNames.splice(this.selectedCatNames.indexOf(cat.categoryName, 1))
    }

    console.log('Selected Categories Names', this.selectedCatNames);


  }

  // On Select Child  
  onSelectChildCat(root, cat, child) {
    console.log("root", root._id)
    console.log("cat", cat._id);
    console.log("child", child._id);

    if (!this.selectedChildName.includes(child.childCategoryName)) {
      this.selectedChildName.push(child.childCategoryName)
    }
    else {
      this.selectedChildName.splice(this.selectedChildName.indexOf(child.childCategoryName), 1)
    }

    console.log('selected Childs names', this.selectedChildName)

  }


  // On Select Child Child
  onSelectChildChild(root, cat, child, cc) {
    console.log("root", root._id)
    console.log("cat", cat._id);
    console.log("child", child._id);
    console.log("cc", cc._id);

    if (!this.selectedChildChildName.includes(cc.childChildCategoryName)) {
      this.selectedChildChildName.push(cc.childChildCategoryName)
    }
    else {
      this.selectedChildChildName.splice(this.selectedChildChildName.indexOf(cc.childChildCategoryName), 1)

    }

    console.log('selected child child names', this.selectedChildChildName);





  }
  // Layout Function

  addLayout() {

    // this.LayoutsArr.push('new  '+this.layoutCount)
    // this.LayoutsArr.push({title: 'new', table: 'abc'})
    // console.log('Layout after Add', this.LayoutsArr)

    const dialogRef = this.matDialog.open(AddLayoutComponent, {
      width: 'auto',

      data: this.LayoutsArr
    }
    )

    dialogRef.afterClosed().subscribe(result => {
      this.LayoutsArr = result
      console.log('layout', this.LayoutsArr)
    })
  }

  deleteLayout(layout) {
    this.LayoutsArr.splice(this.LayoutsArr.indexOf(layout), 1)
    console.log('Layout after delete', this.LayoutsArr);

  }

  // openAddTableDialog(a){
  //   console.log('table',a)
  //   const dialogRef = this.matDialog.open(,
  //     width: '400px',
  //     )
  // }
  //get FORM ARRAY OF DOCUMENTS NAME

  get docFormArray() {
    return this.LegalForm.controls.restaurant_legal_document as FormArray;
  }

  // adding form to it

  addDocArray() {
    ++this.docsCount
    this.docs = this.fb.group({
      documentName: [''],
      documentNumber: [''],
      documentUrl: ['']
    })
    //  console.log(this.docs.value);

    if (this.docsCount <= 4) {
      this.docFormArray.push(this.docs)
      this.docStatus.push({ isDoc: false })
      this.docAble = true
    }
    else {
      Swal.fire('You have already Added 4 Docs', '', 'warning')
    }

  }


  // Remove Docs

  onRemoveExtraLegal(docs, i) {
    this.docFormArray.controls.splice(this.docFormArray.controls.indexOf(docs), 1)
    this.docStatus.splice(i, 1)
    this.otherResponse.splice(i, 1)
    this.docsCount = this.docsCount - 1
    this.docAble = false
  }


  //  Remove OTHER IMAGE

  removeother() {
    this.otherResponse = []
    this.isOtherTrash = true
  }
  // Remove GST IMAGE
  removegst() {
    this.gstResponse = ''
    this.isgstTrash = true
    this.gstStatus = false
    this.LegalForm.controls['restaurant_gst_no'].reset()
  }

  // Remove PAN IMAGE
  removePan() {
    this.panResponse = ''
    this.ispanTrash = true
    this.panStatus = false
    this.LegalForm.controls['restaurant_pan_no'].reset()
  }

  // Remove Fssai Image
  removeFssai() {
    this.fssaiResponse = ''
    this.isFssaiTrash = true
    this.fssaiStatus = false
    this.LegalForm.controls['restaurant_fssai_certificate'].reset()
  }

  // Remove NOC from Form
  removeNoc() {
    this.nocResponse = ''
    this.nocStatus = false
    this.LegalForm.controls['restaurant_noc_certificate'].reset()
    this.LegalForm.controls['restaurant_noc_certificate_file'].reset()
  }

  // Remove Liquor License 

  removeLiqourLic() {
    this.liquorLic = ''
    this.LiqLicStatus = false
    this.LegalForm.controls['restaurant_liqLic'].reset()
    this.LegalForm.controls['restaurant_liqLic_file'].reset()
  }

  // Remove EATING LICENSE
  removeEatLic() {
    this.eatingHouseResponse = ''
    this.eatLicStatus = false
    this.LegalForm.controls['restauant_eatLic'].reset()
    this.LegalForm.controls['restauant_eatLic_file'].reset()
  }

  // Remove Music Liecense
  removeMusicLic() {
    this.musicLicResponse = ''
    this.musicLicStatus = false
    this.LegalForm.controls['restaurant_musicLic'].reset()
    this.LegalForm.controls['restaurant_musicLic_file'].reset()
  }

  //REMOVE TRADE LICENSE
  removeTradeLic() {
    this.trademarkResponse = ''
    this.trademarkStatus = false
    this.LegalForm.controls['restaurant_tradeLic'].reset()
    this.LegalForm.controls['restaurant_tradeLic_file'].reset()
  }

  // Remove CEC ENVIROMENT LICENSE
  removeCEC() {
    this.cecResponse = ''
    this.cedStatus = false
    this.LegalForm.controls['restaurant_cec'].reset()
    this.LegalForm.controls['restaurant_cec_file'].reset()
  }

  // Remove Shop Act
  removeShopAct() {
    this.shopActResponse = ''
    this.shopActStatus = false
    this.LegalForm.controls['restaurant_shopAct'].reset()
    this.LegalForm.controls['restaurant_shopAct_file'].reset()
  }

  // Remove Signage License
  removeSignageLic() {
    this.sinageResponse = ''
    this.sinageStatus = false
    this.LegalForm.controls['restaurant_sinageLic'].reset()
    this.LegalForm.controls['restaurant_sinageLic_file'].reset()
  }
  // Remove Lift License
  removeLiftLic() {
    this.LiftResponse = ''
    this.liftLicStatus = false
    this.LegalForm.controls['restaurant_liftClearance'].reset()
    this.LegalForm.controls['restaurant_liftClearance_file'].reset()
  }

  // Remove Health/Trade License
  removeHealthLic() {
    this.healthResponse = ''
    this.healthLicStatus = false
    this.LegalForm.controls['restuarant_healthLic'].reset()
    this.LegalForm.controls['restuarant_healthLic_file'].reset()
  }


  // Remove MEDI LOGO

  removeLogo() {
    this.logoResponse = null
    this.logoHide = true
  }

  // REMOVE MEDIA BANNER
  removeBanner(i) {
    this.BannerResponse.splice(i, 1)

    if (this.BannerResponse.length == 0) {
      this.bannerHide = true
    }
  }

  // REMOVE IMAGE ALBUM
  removeImgAlbum(index, image, obj) {
    console.log(this.album_image[index].fileurl.splice(this.album_image[index].fileurl.indexOf(image), 1));



    if (this.album_image[index].fileurl.length == 0) {
      this.album_image.splice(this.album_image.indexOf(obj), 1)
      this.albumNames.push(obj.name)
    }

    console.log('album', this.album_image);
  }

  // REMOVE VIDEO ALBUM
  removeVdoAlbum(index, video, obj) {
    console.log(this.album_video[index].fileurl.splice(this.album_video[index].fileurl.indexOf(video), 1));

    if (this.album_video[index].fileurl.length == 0) {
      this.album_video.splice(this.album_video.indexOf(obj), 1)
      this.createdVdoAlbum.push(obj.name)
    }

    console.log('album vdo', this.album_video);

  }

  // On Check Root
  oncheckRoot(evt) {
    console.log('chk', evt);

  }

  // *******CARET CLCIK





  // ***********ESTABLISHMENT DROP DOWN CODE******************

  // onRootCATEGORYSelections

  onRootCategorySelection(evt) {
    let a = evt.target.selectedIndex

    let singlecategoryName = this.estRootCategory[a - 1]

    this.categoryName = singlecategoryName['categories']

    console.log('categories Names', this.categoryName);



  }

  // onCategorySelection

  onCategorySelection(evt) {
    let idx = evt.target.selectedIndex

    let sinsubCategory = this.categoryName[idx - 1]

    console.log('sing sub category', sinsubCategory);

    this.childCategory = sinsubCategory['childCategories']

  }

  // on selecTION CHILD

  onSelectChild(evt) {
    let a = evt.target.selectedIndex

    let singleChild = this.childCategory[a - 1]

    this.subsubChild = singleChild['childChildCategories']

    console.log('sub sub child', this.subsubChild);

  }


  // ***********end of ESTABLISHMENT DROP DOWN CODE******************

  // *******GET ESTABLISHMENT ROOT TYPE*********
  getEstRootType() {
    this.restServ.getRootCategory().subscribe(data => {
      this.estRootCategory = data['data']
      console.log('estRootCategory', this.estRootCategory);

    })
  }


  // *******************Get All Resto Menu***************
  getAllRestoMenu() {
    this.restServ.getAllRootMenu().subscribe(data => {
      this.AllRootMenu = data['data']
      console.log('all root menu', this.AllRootMenu);

    })
  }

  // *********LIQUOR RESTO INFO code ****************

  // getting all root drink types   
  getRootDrinkTypes() {

    this.restServ.getALlLiquorTree().subscribe(data => {
      console.log('get all drinks', data);
      this.DrinkTypes = data['data']
    })
  }
  // ****** click carrot function**********

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  // *******get resto type API

  getAllRestoType() {
    this.restServ.getAllRestoType().subscribe(data => {
      this.restotype = data['data']
    })
  }

  // get function REVENUE
  getRevenue() {
    this.restServ.getAllRevenue().subscribe(data => {
      this.Revenue = data['data']
      console.log("ALL REVEUE", data['data'])


    })
  }
  // ON SAVE LAYOUTS FORM
  onSaveLayouts() {

    this.FinalLayoutObj = {
      "layout": [
        {
          "tabname": "Rooftop",
          "tab_layouts": [
            {
              "table_name": "Table 1",
              "seating_capacity": "4",
              "assests": {
                "chairs": [
                  "chair 1",
                  "chair 2"
                ],
                "walls": [
                  "side",
                  "front"
                ],
                "doors": [
                  "enter",
                  "exit"
                ],
                "billing_counter": [
                  "counter 1",
                  "couter 2"
                ]
              }
            }
          ]
        }
      ]
    }


    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: this.estobj,
      legalInfo: this.finalLegalObj,
      media: this.finalMediaObj,
      menuManagement: this.finalMenuMgmt,
      layout: this.FinalLayoutObj
    }

    this.objectvalue = obj
    console.log('final obj is here', obj)

  }
  //ON SAVE MENU FORM
  onLiquorSave() {


    // this.finalMenuMgmt = {
    //   cuisines: this.cuisinesArr,
    //   menu: this.submenu,
    //   liquor: this.liquormenu
    // }

    // let obj = {
    //   restoInfo: this.finalRestObj,
    //   establishmentType: this.estobj,
    //   legalInfo: this.finalLegalObj,
    //   media: this.finalMediaObj,
    //   menuManagement: this.finalMenuMgmt
    // }

    // console.log("after menu tab completion", obj);


    // this.objectvalue = obj;

    console.log('selected ROOT', this.selectedRootDrinksName);
    console.log('selected VARIENT', this.selectedDrinkVarient);
    console.log('selected SUB VARIENT', this.selectedDrinkSubVarient);
    console.log('selected SUPER CHILD', this.selectedDrinkSuperChild);

    var liquorObj = {
      rootVarients: this.selectedRootDrinksName,
      Varients: this.selectedDrinkVarient,
      SubVarients: this.selectedDrinkSubVarient,
      SubSubVarients: this.selectedDrinkSuperChild
    }
    this.restServ.putSelectedLiquor(this.OutletId, { liquor: liquorObj, userid: this.userid }).subscribe(res => {
      console.log('Response Liquor object', res);
      if (res['sucess']) {
        Swal.fire('Added Liquor Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }
    })
  }


  //ON SAVE MEDIA FORM
  onSaveMediaForm() {

    this.finalMediaObj = {
      resto_logo: this.logoResponse,
      resto_banner: this.BannerResponse,
      album_image: [{
        name: this.albumNames[0],
        fileurl: this.ImgAlbumResponse
      }],
      // album_video: vdo
    }


    console.log(
      "MEDIA FORM", this.finalMediaObj
    );




  }
  onSubmitMedia() {
    // this.OutletId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.PutMediaDataResto(this.OutletId, { media: this.finalMediaObj, userid: this.userid }).subscribe(res => {
      console.log(res);
      if (res['sucess']) {
        Swal.fire('Media Added Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Added', '', 'warning')
      }

    })
  }

  // ON SAVE BANK INFO
  onSaveBankInfo() {

    console.log('Bank Info Save', this.LegalForm.value);


    // console.log('json after legal', obj);

    var finalLegalFormObject = {
      "accountNo": this.LegalForm.controls['restaurant_account_no'].value,
      "accountType": this.LegalForm.controls['restaurant_account_type'].value,
      "bankName": this.LegalForm.controls['restaurant_bank_name'].value,
      "branchName": this.LegalForm.controls['restaurant_branch_name'].value,
      "city": this.LegalForm.controls['restaurant_bank_city'].value,
      "address": this.LegalForm.controls['restaurant_bank_address'].value,
      "IFSCCode": this.LegalForm.controls['restaurant_bank_ifsccode'].value,
      "selected_docs": this.LegalForm.controls['restaurant_selected_docs'].value,
      "documents": [{
        "documentName": "GST",
        "documentNumber": this.LegalForm.controls['restaurant_gst_no'].value,
        "documentUrl": this.LegalForm.controls['restaurant_gst_file'].value
      },
      {
        "documentName": "PAN Card",
        "documentNumber": this.LegalForm.controls['restaurant_pan_no'].value,
        "documentUrl": this.LegalForm.controls['restaurant_pan_image'].value
      },
      {
        "documentName": "FSSAI Certificate",
        "documentNumber": this.LegalForm.controls['restaurant_fssai_certificate'].value,
        "documentUrl": this.LegalForm.controls['restaurant_fssai_image'].value
      },
      {
        "documentName": "NOC",
        "documentNumber": this.LegalForm.controls['restaurant_noc_certificate'].value,
        "documentUrl": this.LegalForm.controls['restaurant_noc_certificate_file'].value
      },
      {
        "documentName": "Liquor License",
        "documentNumber": this.LegalForm.controls['restaurant_liqLic'].value,
        "documentUrl": this.LegalForm.controls['restaurant_liqLic_file'].value
      },
      {
        "documentName": "Eating House License",
        "documentNumber": this.LegalForm.controls['restauant_eatLic'].value,
        "documentUrl": this.LegalForm.controls['restauant_eatLic_file'].value
      },
      {
        "documentName": "Music license",
        "documentNumber": this.LegalForm.controls['restaurant_musicLic'].value,
        "documentUrl": this.LegalForm.controls['restaurant_musicLic_file'].value
      },
      {
        "documentName": "Trademark for Cafe License",
        "documentNumber": this.LegalForm.controls['restaurant_tradeLic'].value,
        "documentUrl": this.LegalForm.controls['restaurant_tradeLic_file'].value
      },
      {
        "documentName": "Certificate of Environmental Clearance (CEC)",
        "documentNumber": this.LegalForm.controls['restaurant_cec'].value,
        "documentUrl": this.LegalForm.controls['restaurant_cec_file'].value
      },
      {
        "documentName": "Shops and Establishment Act",
        "documentNumber": this.LegalForm.controls['restaurant_shopAct'].value,
        "documentUrl": this.LegalForm.controls['restaurant_shopAct_file'].value
      },
      {
        "documentName": "Signage license",
        "documentNumber": this.LegalForm.controls['restaurant_sinageLic'].value,
        "documentUrl": this.LegalForm.controls['restaurant_sinageLic_file'].value
      },
      {
        "documentName": "Lift Clearance",
        "documentNumber": this.LegalForm.controls['restaurant_liftClearance'].value,
        "documentUrl": this.LegalForm.controls['restaurant_liftClearance_file'].value
      },
      {
        "documentName": "Health/Trade License",
        "documentNumber": this.LegalForm.controls['restuarant_healthLic'].value,
        "documentUrl": this.LegalForm.controls['restuarant_healthLic_file'].value
      }],
      "otherDocuments": this.LegalForm.controls['restaurant_legal_document'].value
    }

    // console.log('only otherdocuments', this.LegalForm.controls['restaurant_legal_document'].value);


    console.log('resto legal form', finalLegalFormObject);

    // this.OutletId = '5e59e57c27bcac2f7acdd635'
    this.restServ.PutLegalDataResto(this.OutletId, { legalInfo: finalLegalFormObject, userid: this.userid }).subscribe(res => {
      console.log('Response', res);
      if (res['sucess']) {
        Swal.fire('Legal Data Added Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Added', '', 'warning')
      }

    })

    // this.checkobj = finalLegalFormObject
  }


  //ON SAVE AMBIENCES
  onSaveAmbience() {

    // this.OutletId = '5e3c2bc9a588df0cb1f6d323' 
    console.log('ambience ifno', this.selectedAmbienceInfo)
    this.restServ.putAmbiencesToResto(this.OutletId, this.selectedAmbienceInfo).subscribe(res => {
      console.log('response after adding ambiences', res);
      if (res['sucess']) {
        Swal.fire('Ambiences Added Successfully', '', 'success')
        localStorage.setItem('ambiences', JSON.stringify(this.selectedAmbienceInfo))
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }

    })
    this.finalAmbienceObj = [
      {
        ambience_name: 'Beach',
        selected_type: this.selectedAmbience
      },
      {
        ambience_name: 'Lawn',
        selected_type: this.selectedAmbience
      }
    ]
    this.estobj =
    {
      est: this.FinalEstObj,
      more_info: this.finalMoreInfobj,
      services: this.finalServicesobj,
      rules: this.finalRulesobj,
      ambience: this.finalAmbienceObj
    }

    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: this.estobj,

    }

    console.log('object final RULES AND REGULARIONT', obj);
  }

  //ON SAVE RULES & REGULATIONS
  onSaveRules() {


    // this.OutletId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.putRulesToResto(this.OutletId, this.selectedRulesInfo).subscribe(res => {
      console.log('resposne save rules to resto', res);
      if (res['sucess']) {
        Swal.fire('Rules added Successfully', '', 'success')
      }
      else {
        Swal.fire(
          'Failed to add', '', 'warning'
        )
      }

    })

    this.finalRulesobj = [
      {
        rule_name: 'Restriction',
        selected_type: this.selectedRules
      },
      {
        rule_name: 'Policy',
        selected_type: this.selectedRules
      }
    ]

    let estobj =
    {
      est: this.FinalEstObj,
      more_info: this.finalMoreInfobj,
      services: this.finalServicesobj,
      rules: this.finalRulesobj
    }

    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: estobj,

    }

    console.log('object final RULES AND REGULARIONT', obj);
  }
  // ON SAVE SERVICES
  onSaveServices() {

    // this.OutletId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.putServicesToResto(this.OutletId, this.selectedServices).subscribe(res => {
      console.log('Services saved to db status', res);
      if (res['sucess']) {
        Swal.fire('Services Added Successfully', '', 'success')
        localStorage.setItem('services', JSON.stringify(this.selectedServices))
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }

    })


    this.finalServicesobj = [
      {
        service_name: 'Bar',
        selected_type: this.selectedServices
      },
      {
        service: 'Pub',
        selected_type: this.selectedServices
      }
    ]
    let estobj =
    {
      est: this.FinalEstObj,
      more_info: this.finalMoreInfobj,
      services: this.finalServicesobj
    }

    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: estobj,

    }

    console.log('object final services', obj);


  }

  //ON SAVE MORE INFO
  onSaveMoreInfo() {

    // this.OutletId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.putMoreInfoToResto(this.OutletId, this.selectedMoreInfos).subscribe(res => {
      console.log('Response more Infos to db', res);
      if (res['sucess']) {
        Swal.fire('More Infos Added Successfully', '', 'success')
        localStorage.setItem('moreinfo', JSON.stringify(this.selectedMoreInfos))
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }
    })

    this.finalMoreInfobj = [
      {
        moreinfo_name: 'Bar',
        selected_type: this.selectedBars
      },
      {
        moreinfo_name: 'Pub',
        selected_type: this.selectedPubs
      }
    ]

    let estobj =
    {
      est: this.FinalEstObj,
      more_info: this.finalMoreInfobj
    }

    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: estobj,

    }


    console.log('show details', obj);

  }
  //ON SAVE ESTABLSIMENT TYPE
  onSaveEstType() {
    var estCategories = {
      rootCategories: this.selectedRootNames,
      categories: this.selectedCatNames,
      childCategories: this.selectedChildName,
      childchildCategories: this.selectedChildChildName
    }



    console.log('estCategories schema', estCategories);


    let obj = {
      restoInfo: this.finalRestObj,
      establishmentType: estCategories
    }

    console.log('obj after establsihment type', obj);

    // 5e40f82bf177a934fb971342
    // this.restServ.putEstablishType(this.OutletId, estCategories).subscribe(response =>{
    this.restServ.putEstablishType(this.OutletId, estCategories).subscribe(response => {
      console.log('establisment response', response);

      if (response['sucess']) {
        Swal.fire('Establishments Added', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }
    })



  }



  //ON SAVE CUSINE FORM
  onSaveCuisinesForm() {
    this.cuisinesObj = this.selectedCuisines
    let obj = {
      restoInfo: this.finalRestObj,
      legalInfo: this.finalLegalObj,
      media: this.finalMediaObj,
      cuisines: this.cuisinesObj
    }

    console.log(
      "Selected Cuisines", this.selectedCuisinesName
    );


    this.restServ.putSelectedCuisines(this.OutletId, this.cuisinesObj).subscribe(res => {
      console.log('response add selected cuisines', res);

      if (res['sucess']) {
        Swal.fire('Cuisines Added Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }
    })


  }

  // ON SAVE RESTO INFO
  onSaveRestoInfo() {

    // Save Restaurant Google Address

    let savedAddress = [
      // this.RestaurantInfo.controls.restaurant_building.reset()
      // this.RestaurantInfo.controls.restaurant_landmark.reset()
      // this.RestaurantInfo.controls.restaurant_street.reset()
      // this.RestaurantInfo.controls.restaurant_sublocal.reset()
      // this.RestaurantInfo.controls.restaurant_locality.reset()
      // this.RestaurantInfo.controls.restaurant_town.reset()
      // this.RestaurantInfo.controls.restaurant_city.reset()
      // this.RestaurantInfo.controls.restaurant_state.reset()
      // this.RestaurantInfo.controls.restaurant_country.reset()
      {
        name: this.RestaurantInfo.controls.restaurant_building.value,
        type: ['premise']
      },
      {
        name: this.RestaurantInfo.controls.restaurant_landmark.value,
        type: ['premise']
      },
      {
        name: this.RestaurantInfo.controls.restaurant_street.value,
        type: ['premise']
      },
      {
        name: this.RestaurantInfo.controls.restaurant_sublocal.value,
        type: ['premise']
      },
      {
        name: this.RestaurantInfo.controls.restaurant_locality.value,
        type: ['premise']
      },
      {
        name: this.RestaurantInfo.controls.restaurant_town.value,
        type: ["administrative_area_level_3", "locality", "political"]
      },
      {
        name: this.RestaurantInfo.controls.restaurant_city.value,
        type: ['administrative_area_level_2']
      },
      {
        name: this.RestaurantInfo.controls.restaurant_state.value,
        type: ['administrative_area_level_1']
      },
      {
        name: this.RestaurantInfo.controls.restaurant_country.value,
        type: ["country"]
      }
    ]

    // Saved Local Address
    console.log("saved address ", savedAddress)

    // ADD RESTAURABTS contact with country code: 

    this.restaurantCountryCode = this.RestaurantInfo.controls['restaurant_country_code'].value
    this.restaurantContact = this.RestaurantInfo.controls['restaurant_contact'].value
    this.respresentativeCoutryCode = this.RestaurantInfo.controls['respresentative_country_code'].value
    this.respresentativeContact = this.RestaurantInfo.controls['representative_contact'].value

    // FINAL OBJECT FOR RESTO OBJECT
    var Restobj = {
      restoLegalName: this.RestaurantInfo.controls['legal_name'].value,
      restoName: this.RestaurantInfo.controls['restaurant_name'].value,
      // restaurant_contact: this.RestaurantInfo.controls['restaurant_contact'].value,
      restoContactNo: this.restaurantContact,
      restoStatus: true,
      restoEmail: this.RestaurantInfo.controls['restaurant_email'].value,
      restoAddress: this.RestaurantInfo.controls['restaurant_address'].value,
      restoPriceForTwo: this.RestaurantInfo.controls['restaurant_price_for_two'].value,
      restoLocality: this.RestaurantInfo.controls['restaurant_locality'].value,
      restoPincode: this.RestaurantInfo.controls['restaurant_pincode'].value,
      restoRevenue: this.RestaurantInfo.controls['restaurant_revenue'].value,
      restoCountryCode: this.restaurantCountryCode.dial_code,
      restoType: this.restaurants_types,
      restoWebsite: this.RestaurantInfo.controls['restaurant_website'].value,
      restoProducts: this.RestaurantInfo.controls['restuarant_products'].value,
      restoRepresentativeName: this.RestaurantInfo.controls['representative_name'].value,
      restoRepresentativeCountryCode: this.respresentativeCoutryCode.dial_code,
      restoRepresentativeContactNo: this.respresentativeContact,
      restoRepresentativeEmail: this.RestaurantInfo.controls['representative_email'].value,

      // restoHours: this.RestaurantInfo.controls['representative_email'].value,





      restoHours: [
        {
          day: 'monday',
          open: this.RestaurantInfo.controls['openMon'].value,
          close: this.RestaurantInfo.controls['closeMon'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenMon'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseMon'].value

        },
        {
          day: 'tuesday',
          open: this.RestaurantInfo.controls['openTue'].value,
          close: this.RestaurantInfo.controls['closeTue'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenTue'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseTue'].value

        },
        {
          day: 'wednesday',
          open: this.RestaurantInfo.controls['openWed'].value,
          close: this.RestaurantInfo.controls['closeWed'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenWed'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseWed'].value

        },
        {
          day: 'thursday',
          open: this.RestaurantInfo.controls['openThur'].value,
          close: this.RestaurantInfo.controls['closeThur'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenThur'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseThur'].value

        },
        {
          day: 'friday',
          open: this.RestaurantInfo.controls['openFri'].value,
          close: this.RestaurantInfo.controls['closeFri'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenFri'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseFri'].value

        },
        {
          day: 'saturday',
          open: this.RestaurantInfo.controls['openSat'].value,
          close: this.RestaurantInfo.controls['closeSat'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenSat'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseSat'].value

        },
        {
          day: 'sunday',
          open: this.RestaurantInfo.controls['openSun'].value,
          close: this.RestaurantInfo.controls['closeSun'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenSun'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseSun'].value

        }
      ]
    }


    console.log('resto schema', Restobj);
    this.finalRestObj = Restobj

    // **********PUT SERVICE FOR RESTO INFO***************

    this.restServ.putRestoInfo(this.OutletId, { restoInfo: Restobj, userid: this.userid }).subscribe(addRestoResponse => {
      if (addRestoResponse['sucess']) {
        Swal.fire("Resto info added", '', 'success')
      }
      else {
        Swal.fire('Failed to add')
      }

      console.log('RESPONSE: ', addRestoResponse);
      // this.OutletId = addRestoResponse['data']

      console.log('Resto Id', this.OutletId)
    })





  }



  addTab(t) {

    console.log(t);

    this.tabs = t;


    this.selected.setValue(this.tabs.length - 1);

  }

  removeTab(index: number) {
    this.tabs.splice(index, 1);
  }


  //ON CHECK CUISINES 

  onChkCuisines(s) {

    if (this.allCuisines.includes(s)) {
      this.allCuisines.splice(this.allCuisines.indexOf(s), 1)
      this.selectedCuisines.push(s)
      this.selectedCuisinesName.push(s.cuisineName)
    }
  }

  //ON REMOVE CUISINES

  removeCuisines(s) {
    if (this.selectedCuisines.includes(s)) {
      this.selectedCuisines.splice(this.selectedCuisines.indexOf(s), 1)
      this.selectedCuisinesName.splice(this.selectedCuisinesName.indexOf(s.cuisineName), 1)
      this.allCuisines.push(s)
    }
  }




  // after click on sameday 
  onSameAsMonday(chk) {

    console.log('checked vlaue', chk)
    if (this.monAddHr) {
      this.tueAddHr = true;
      this.wedAddHr = true;
      this.thurAddHr = true;
      this.friAddHr = true;
      this.satAddHr = true;
      this.sunAddHr = true;
    }

    if (this.addmonHr) {
      this.addtueHr = true;
      this.addwedHr = true;
      this.addthurHr = true;
      this.addfriHr = true;
      this.addsatHr = true;
      this.addsunHr = true;

    }
    else {
      this.addtueHr = false;
      this.addwedHr = false;
      this.addthurHr = false;
      this.addfriHr = false;
      this.addsatHr = false;
      this.addsunHr = false;
    }

    // TIME VALUE SAME AS MONDAY

    this.mondayOpenTime = this.RestaurantInfo.controls['openMon'].value
    this.mondayCloseTime = this.RestaurantInfo.controls['closeMon'].value

    console.log('Extra open monday vlaue', this.RestaurantInfo.controls['extraopenMon'].value);
    this.extramonOpenTime = this.RestaurantInfo.controls['extraopenMon'].value
    this.extamonCloseTime = this.RestaurantInfo.controls['extracloseMon'].value


    console.log('tIMES values ', this.mondayOpenTime);

    this.RestaurantInfo.patchValue({
      openTue: this.mondayOpenTime,
      openWed: this.mondayOpenTime,
      openThur: this.mondayOpenTime,
      openFri: this.mondayOpenTime,
      openSat: this.mondayOpenTime,
      openSun: this.mondayOpenTime,

      extraopenTue: this.extramonOpenTime,
      extraopenWed: this.extramonOpenTime,
      extraopenThur: this.extramonOpenTime,
      extraopenFri: this.extramonOpenTime,
      extraopenSat: this.extramonOpenTime,
      extraopenSun: this.extramonOpenTime,


      closeTue: this.mondayCloseTime,
      closeWed: this.mondayCloseTime,
      closeThur: this.mondayCloseTime,
      closeFri: this.mondayCloseTime,
      closeSat: this.mondayCloseTime,
      closeSun: this.mondayCloseTime,

      extracloseTue: this.extamonCloseTime,
      extracloseWed: this.extamonCloseTime,
      extracloseThur: this.extamonCloseTime,
      extracloseFri: this.extamonCloseTime,
      extracloseSat: this.extamonCloseTime,
      extracloseSun: this.extamonCloseTime

    })




    //RADIO SAME AS MONDAY
    if (chk.target.checked) {

      this.isTues = this.isMon;
      this.isWed = this.isMon;
      this.isThur = this.isMon;
      this.isFri = this.isMon;
      this.isSat = this.isMon;
      this.isSun = this.isMon;
    }

    else {
      this.isTues = false;
      this.isWed = false;
      this.isThur = false;
      this.isFri = false;
      this.isSat = false;
      this.isSun = false;

      // **********************************

      this.RestaurantInfo.controls['openTue'].reset(),
        this.RestaurantInfo.controls['closeTue'].reset(),
        this.RestaurantInfo.controls['extraopenTue'].reset(),
        this.RestaurantInfo.controls['extracloseTue'].reset()

      this.RestaurantInfo.controls['openWed'].reset(),
        this.RestaurantInfo.controls['closeWed'].reset(),
        this.RestaurantInfo.controls['extraopenWed'].reset(),
        this.RestaurantInfo.controls['extracloseWed'].reset()



      this.RestaurantInfo.controls['openThur'].reset(),
        this.RestaurantInfo.controls['closeThur'].reset(),
        this.RestaurantInfo.controls['extraopenThur'].reset(),
        this.RestaurantInfo.controls['extracloseThur'].reset()



      this.RestaurantInfo.controls['openFri'].reset(),
        this.RestaurantInfo.controls['closeFri'].reset(),
        this.RestaurantInfo.controls['extraopenFri'].reset(),
        this.RestaurantInfo.controls['extracloseFri'].reset()


      this.RestaurantInfo.controls['openSat'].reset(),
        this.RestaurantInfo.controls['closeSat'].reset(),
        this.RestaurantInfo.controls['extraopenSat'].reset(),
        this.RestaurantInfo.controls['extracloseSat'].reset()


      this.RestaurantInfo.controls['openSun'].reset(),
        this.RestaurantInfo.controls['closeSun'].reset(),
        this.RestaurantInfo.controls['extraopenSun'].reset(),
        this.RestaurantInfo.controls['extracloseSun'].reset()


      // *********************************
    }

  }
  showtabName(t) {
    console.log('tabnaem', t);

    this.mainService.tabTableName = t
    console.log(
      'the TabName', this.mainService.tabTableName
    );

  }




  //******Open add Revenue Dialog

  openAddRevenueDialog() {

    const dialogRef = this.matDialog.open(AddRevenueComponent, {
      width: '400px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'After closing Dialog'
      );
      this.getRevenue();
    })
  }

  // ****************DELETE ALL rEVENUE
  deleteAllRevenue() {
    this.restServ.deleteAllRev().subscribe(res => {
      console.log("delete all revenue", res);

      if (res['sucess']) {
        this.getRevenue()
      }
    })
  }


  // ******************Open add Resto Type**************

  openRestoTypeDialog() {

    const dialogRef = this.matDialog.open(AddRestoTypeComponent, {
      width: '400px'
    })

    dialogRef.afterClosed().subscribe(addRestoRes => {

      console.log('after closing RestoTYpe Dialog');
      this.getAllRestoType()
    })
  }


  // Open add table dialog
  openTableDialog(a): void {

    console.log('a', a);

    const dialogRef = this.matDialog.open(OpenTableComponent, {
      width: '300px',
      data: a
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      console.log('layout', this.LayoutsArr)
    });

  }


  //  open add layout dialog

  openAddLayoutDilog(): void {
    const dialogRef = this.matDialog.open(AddLayoutComponent, {
      width: '540px',
      height: '440px',
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (this.mainService.layoutName !== undefined) {
        this.tabNames = this.mainService.layoutName
        this.addTab(this.tabNames);

        // this.animal = result;
        this.mainService.selectedAlbums = this.tabNames;
      }
    });

  }

  // open DIalog media only for images
  openMedialog() {
    const dialogRef = this.matDialog.open(CreateAlbumComponent, {
      width: '540px',
      data: {
        moreinfo: this.selectedMoreInfos,
        service: this.selectedServices,
        ambiences: this.selectedAmbienceInfo
      }
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (this.mainService.popMediaAddData != undefined) {
        // this.albumNames.push(this.mainService.popMediaAddData);


        this.albumNames = this.mainService.popMediaAddData;



        this.isAlbum = false;
        console.table("ALUBM HERE", this.albumNames)
        this.mainService.selectedAlbums = this.albumNames;
      }


    });



  }



  // open video dialog album
  openVdoAlbum() {
    const dialogRef = this.matDialog.open(CreateVdoAlbumComponent, {
      width: '540px',
      data: {
        moreinfo: this.selectedMoreInfos,
        service: this.selectedServices,
        ambiences: this.selectedAmbienceInfo
      }
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result form videos create album', result);
      // this.createdVdoAlbum.push(result)
      // console.log('created video album', this.createdVdoAlbum);

      if (this.mainService.popVideoAddData != undefined) {
        // this.albumNames.push(this.mainService.popMediaAddData);


        this.createdVdoAlbum = this.mainService.popVideoAddData;

        this.isAlbum = false;
        console.table("ALUBM video HERE", this.createdVdoAlbum)
        this.mainService.selectedVideoAlbums = this.createdVdoAlbum;
      }

    })
  }

  // open dialog add root category code

  openAddCateg(): void {
    const dialogRef = this.matDialog.open(EstAddCatComponent, {
      width: 'auto',

    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.getEstRootType()
    });


  }

  // open category add category name

  openCategoryName(root) {

    console.log('current root', root);
    let root_id = root._id

    const dialogRef = this.matDialog.open(AddCatoNameComponent, {
      width: 'auto',
      data: root_id
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('category name closed', result);
      this.getEstRootType()

    })

  }

  // AddSubCatComponent, AddSubSubCatComponent

  // open child category 
  openSubCategory(root, cat) {
    console.log('root details', root._id);

    console.log('category details', cat._id);

    const dialogRef = this.matDialog.open(AddSubCatComponent, {
      width: '540px',
      data: { 'root': root._id, "cat": cat._id }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.getEstRootType()
    })
  }


  // open child child category 
  openSubSubCategory(root, child) {

    // console.log('root', root._id);
    // console.log('child', child._id)

    const dialogRef = this.matDialog.open(AddSubSubCatComponent, {
      width: '540px',
      data: { 'root': root._id, 'child': child._id }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.getEstRootType()
    })
  }
  // /////////
  //open sub category
  // /////////
  opensubCateg(): void {
    const dialogRef = this.matDialog.open(SubCatComponent, {
      width: '580px',
      height: '440px',
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (this.mainService.popSubCategData !== undefined) {
        this.popSub = this.mainService.popSubCategData
        console.log('sub catergory data form popOver', this.popSub);
        console.table('sub catergory data form popOver', this.popSub);
      }
      // this.animal = result;
    });


  }

  // OPEN ATTRIBUTE CATEGORY

  openattr(): void {
    const dialogRef = this.matDialog.open(AttrCatComponent, {
      width: '580px',
      height: '480px',
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (this.mainService.popAtrrCategData !== undefined) {
        this.popAttr = this.mainService.popAtrrCategData
        console.log('attr catergory data form popOver', this.popAttr);
        console.table('attr data form popOver', this.popAttr);
      }
      // this.animal = result;
    });


  }



  onEstNext(next) {
    this.dummy = next
  }

  onMenuNext(next) {
    console.log(this.output);
    this.menuNav = next
  }

  // Open city Navbar 
  openCity(evt, cityName) {

    this.dummy = cityName
    console.log('dummt', this.dummy);

    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace("active", "");
    }
    // document.getElementById(cityName).style.display = "block";
    // evt.currentTarget.className += "active";
  }

  // // Get the element with id="defaultOpen" and click on it

  //GET BARMOREINFO SELECTIONS

  onCheckMat(e) {
    console.log(
      "event value here", e
    );

    if (this.barMoreInfo.includes(e)) {
      this.barMoreInfo.splice(this.barMoreInfo.indexOf(e), 1);
      this.selectedBars.push(e);
    }
    // this.ischecked = e.checked



  }

  // REMOVE BAR MORE INFOS
  removeBars(s) {
    if (this.selectedBars.includes(s)) {
      this.selectedBars.splice(this.selectedBars.indexOf(s), 1);
      this.barMoreInfo.push(s)
    }
  }

  // ON SELECTD MORE INFO PUBS
  onCheckPubs(s) {
    if (this.pubMoreInfo.includes(s)) {
      this.pubMoreInfo.splice(this.pubMoreInfo.indexOf(s), 1)
      this.selectedPubs.push(s);
    }
  }


  // ON REMOVE PUB MORE INFOS
  removePubs(s) {
    if (this.selectedPubs.includes(s)) {
      this.selectedPubs.splice(this.selectedPubs.indexOf(s), 1)
      this.pubMoreInfo.push(s);
    }
  }

  // ON MOREINFO chips
  onCheckMoreInfo(a, b) {
    if (!this.dupChkMoreInfo.includes(b)) {
      this.dupChkMoreInfo.push(b)
      this.selectedMoreInfos.push({ title: a, moreinfo: b })
      this.albumMoreinfo.push({ title: a, moreinfo: b, selected: false })
      console.log('selected moreinfos', this.selectedMoreInfos)
    }
    else {
      alert('Already Added!!')
    }

  }

  // On Remove MoreInfo 
  removeMoreInfo(s) {

    console.log('selected remove', s)
    if (this.selectedMoreInfos.includes(s)) {
      this.selectedMoreInfos.splice(this.selectedMoreInfos.indexOf(s), 1)
      this.dupChkMoreInfo.splice(this.dupChkMoreInfo.indexOf(s.moreinfo), 1)
    }
  }


  // on CHECK SERVICES

  onCheckService(a, b) {


    if (!this.dupChkServ.includes(b)) {
      this.dupChkServ.push(b)
      this.selectedServices.push({ title: a, service: b })
      this.albumService.push({ title: a, service: b, selected: false })
      console.log('selecected services', this.selectedServices)
    }
    else {
      alert('Already Added!!')
    }



  }

  // on REMOVE MORE INFO SERVICES

  removeServices(s) {
    if (this.selectedServices.includes(s)) {
      this.selectedServices.splice(this.selectedServices.indexOf(s), 1)
      this.dupChkServ.splice(this.dupChkServ.indexOf(s.service), 1)
    }

  }


  //on Check Rules more info
  onCheckRules(a, b) {

    if (!this.dupChkRules.includes(b)) {
      this.dupChkRules.push(b)
      this.selectedRulesInfo.push({ title: a, rule: b })
      this.albumRules.push({ title: a, rule: b, selected: false })
      console.log('selecected Rules', this.selectedRulesInfo)
    }
    else {
      alert('Already Added!!')
    }
  }

  // on Remove RUles from More Info
  removeRules(s) {
    if (this.selectedRulesInfo.includes(s)) {
      this.selectedRulesInfo.splice(this.selectedRulesInfo.indexOf(s), 1)
      this.dupChkRules.splice(this.dupChkRules.indexOf(s.rule), 1)
    }

    console.log('selected RULES', this.selectedRulesInfo)
  }


  // CHECK AMBIENCE MORE INFO

  onCheckAmbience(a, b) {


    if (!this.dupChkAmbience.includes(b)) {
      this.dupChkAmbience.push(b)
      this.selectedAmbienceInfo.push({ title: a, ambiences: b })
      this.albumAmbience.push({title: a, ambiences: b, selected: false})
      console.log('selecected Ambiences', this.selectedAmbienceInfo)
      this.layoutTabs.push({ title: a, ambiences: b })
      console.log('selected layoutTABS', this.layoutTabs)

    }
    else {
      alert('Already Added!!')
    }
  }

  /// remove AMABIEANCE MORE INFO
  removeAmbience(s) {
    // if (this.selectedAmbience.includes(s)) {
    //   this.selectedAmbience.splice(this.selectedAmbience.indexOf(s), 1)
    //   this.ambienceMoreInfo.push(s);
    // }
    if (this.selectedAmbienceInfo.includes(s)) {
      this.selectedAmbienceInfo.splice(this.selectedAmbienceInfo.indexOf(s), 1)
      this.dupChkAmbience.splice(this.dupChkAmbience.indexOf(s.ambiences), 1)
    }

    console.log('selected RULES', this.selectedAmbienceInfo)
  }




  createNewElement(id) {
    // First create a DIV element.
    var txtNewInputBox = document.createElement('div');

    // Then add the content (a new input box) of the element.
    txtNewInputBox.innerHTML = "+<input type='time' value='16:00:00' > to <input type='time' value='23:00:00' >";

    // Finally put it where it is supposed to appear.
    if (id == 'mon') {
      // document.getElementById("monAddHTML").appendChild(txtNewInputBox);
      this.monAddHr = true;
    }

    if (id == 'tue') {
      // document.getElementById("tueAddHTML").appendChild(txtNewInputBox);
      this.tueAddHr = true;
    }

    if (id == 'wed') {
      // document.getElementById("wedAddHtml").appendChild(txtNewInputBox);
      this.wedAddHr = true;
    }

    if (id == 'thur') {
      // document.getElementById("thurAddHtml").appendChild(txtNewInputBox);
      this.thurAddHr = true;
    }

    if (id == 'fri') {
      // document.getElementById("friAddHtml").appendChild(txtNewInputBox);
      this.friAddHr = true;
    }

    if (id == 'sat') {
      // document.getElementById("satAddHtml").appendChild(txtNewInputBox);
      this.satAddHr = true;
    }

    if (id == 'sun') {
      // document.getElementById("sunAddHtml").appendChild(txtNewInputBox);
      this.sunAddHr = true;
    }
    //sunAddHtml

    console.log('Id ', id);


  }

  removeExtraTime(id) {
    if (id == 'mon') {
      // document.getElementById("monAddHTML").appendChild(txtNewInputBox);
      this.monAddHr = false;
    }

    if (id == 'tue') {
      // document.getElementById("tueAddHTML").appendChild(txtNewInputBox);
      this.tueAddHr = false;
    }

    if (id == 'wed') {
      // document.getElementById("wedAddHtml").appendChild(txtNewInputBox);
      this.wedAddHr = false;
    }

    if (id == 'thur') {
      // document.getElementById("thurAddHtml").appendChild(txtNewInputBox);
      this.thurAddHr = false;
    }

    if (id == 'fri') {
      // document.getElementById("friAddHtml").appendChild(txtNewInputBox);
      this.friAddHr = false;
    }

    if (id == 'sat') {
      // document.getElementById("satAddHtml").appendChild(txtNewInputBox);
      this.satAddHr = false;
    }

    if (id == 'sun') {
      // document.getElementById("sunAddHtml").appendChild(txtNewInputBox);
      this.sunAddHr = false;
    }
    //su
  }

  // Resto Info on submit function

  OnRestoInfo() {
    console.log(
      'Resto Info on submit value: ', this.RestaurantInfo.value
    );

    console.table(this.RestaurantInfo.value);




    // Establishment on Submit 



  }

  onChkRStype() {

    this.selectRestType = false

    // let status = evt.target.checked

    // if (status) {
    //   this.restaurants_types.push(v);
    //   console.log('restaurants types', this.restaurants_types);

    // }
    // else {
    //   this.restaurants_types.splice(this.restaurants_types.indexOf(v), 1)
    //   console.log('restaurants types', this.restaurants_types);

    //   // this.array.splice(i,1)

    //   // array = ['a','b']

    //   // let i = this.array.indexof('b')
    // }
  }

  onDelievery() {
    this.typeDelievery = !this.typeDelievery
    if (!this.typeDelievery) {
      this.restaurants_types.push('Delievery');
      console.log('restaurants types', this.restaurants_types);

    }
    else {
      this.restaurants_types.splice(this.restaurants_types.indexOf('Delievery'), 1)
      console.log('restaurants types', this.restaurants_types);


    }
  }

  onDinein() {
    this.typeDinein = !this.typeDinein

    if (!this.typeDinein) {
      this.restaurants_types.push('Dine-In');
      console.log('restaurants types', this.restaurants_types);

    }
    else {
      this.restaurants_types.splice(this.restaurants_types.indexOf('Dine-In'), 1)
      console.log('restaurants types', this.restaurants_types);


    }
  }

  onSelfService() {
    this.typeSelfservice = !this.typeSelfservice
    if (!this.typeSelfservice) {
      this.restaurants_types.push('Self Service');
      console.log('restaurants types', this.restaurants_types);

    }
    else {
      this.restaurants_types.splice(this.restaurants_types.indexOf('Self Service'), 1)
      console.log('restaurants types', this.restaurants_types);


    }
  }

  onCloudKitchen() {
    this.typeCloudKitchen = !this.typeCloudKitchen
    if (!this.typeCloudKitchen) {
      this.restaurants_types.push('Cloud Kitchen');
      console.log('restaurants types', this.restaurants_types);

    }
    else {
      this.restaurants_types.splice(this.restaurants_types.indexOf('Cloud Kitchen'), 1)
      console.log('restaurants types', this.restaurants_types);


    }
  }
  // ********************88
  // ******************
  // WEEK RADIO BUTTON TIMES 

  onMonday(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isMon = true;
    }
    else {
      this.isMon = false;
    }

  }

  onTuesday(evt) {
    if (evt.checked) {
      this.isTues = true;
    }
    else {
      this.isTues = false;
    }
  }

  onWednesday(evt) {
    if (evt.checked) {
      this.isWed = true;
    }
    else {
      this.isWed = false;
    }
  }

  onThursday(evt) {
    if (evt.checked) {
      this.isThur = true;
    }
    else {
      this.isThur = false;
    }
  }


  onFriday(evt) {
    if (evt.checked) {
      this.isFri = true;
    }
    else {
      this.isFri = false;
    }
  }

  onSaturday(evt) {
    if (evt.checked) {
      this.isSat = true;
    }
    else {
      this.isSat = false;
    }
  }

  onSunday(evt) {
    if (evt.checked) {
      this.isSun = true;
    }
    else {
      this.isSun = false;
    }
  }


  // TABLE RELATED FUNCITONS

  onSaveLayout(a) {
    console.log('Layout json object', this.LayoutsArr);
    this.tableCount++
    var obj = {
      tablename: 'table' + this.tableCount,
      capacity: '',
      shape: 'rectangle',
      // quantity: this.createTableForm.controls['quantity'].value
    }
    console.log(a.table.push(obj));



  }

  // onSave bill counter
  onSaveBillCounter(a) {


    this.billCount++

    var obj = {
      billcounterName: 'billcounter' + this.billCount
    }

    console.log(a.billing.push(obj));
    console.log('Layout json object', this.LayoutsArr);

  }

  // on save wall

  onSaveWall(a) {


    this.wallcount++

    var obj = {
      wall: 'wall' + this.wallcount
    }

    console.log(a.walls.push(obj));

    console.log('Layout json object', this.LayoutsArr);

  }

  // on save washroom

  onSaveWashroom(a) {
    this.washroomCount++

    var obj = {
      washroom: 'washroom' + this.washroomCount
    }

    console.log(a.washroom.push(obj))
    console.log('Layout json ', this.LayoutsArr);

  }


  // Get Current Location Coordinates
  // private setCurrentLocation() {
  //   if ('geolocation' in navigator) {
  //     navigator.geolocation.getCurrentPosition((position) => {
  //       this.latitude = position.coords.latitude;
  //       this.longitude = position.coords.longitude;
  //       this.zoom = 15;
  //     });
  //   }


  // }

  onCreateTables(obj) {
    console.log('Layout Form', this.LayoutForm.value);
    console.log('layout a ', obj);


    let tableObj = {
      enterseats: this.LayoutForm.controls['enterseats'].value,
      shapes: this.LayoutForm.controls['shapes'].value,
      type: this.LayoutForm.controls['type'].value,
      status: false
    }


    for (let i = 0; i < obj.qrcode; i++) {
      obj.table.push(tableObj)
      console.log('loop', i);

    }


    console.log('Layout array', this.LayoutsArr);

    this.LayoutForm.controls['enterseats'].reset(),
      this.LayoutForm.controls['shapes'].reset(),
      this.LayoutForm.controls['type'].reset()

  }


  // onselect series
  onSelectSeries(evt, obj) {

    // obj.SeriesArr = this.LayoutForm.controls['series'].value
    // console.log('series array',obj.SeriesArr);
    console.log('object ', obj);


  }


  // *****************
  // CREATE FIXED SERIES
  // ******************

  createFs(obj) {

    obj.SeriesArr = this.LayoutForm.controls['series'].value

    console.log('a obj', obj);


    console.log('event', this.LayoutsArr);



  }






  // NUMBER OF TABLES ADD BUTTON
  addTabtoSeries(obj) {

    this.addseriesForm()
    obj.SeriesArr = this.LayoutForm.controls['series'].value

    console.log('layout array', this.LayoutsArr);




  }

  // CHANGING TOGGLING LAYOUT
  ChangeLayout(obj) {

    console.log('you have selectied', obj);

    console.log('index of cuurent', obj.index)

    let singleObj = this.LayoutsArr[obj.index]
    console.log("selected layout index value", singleObj)

    console.log('series value Before', this.LayoutForm.controls['series'].value);

    let arr = this.LayoutForm.controls['series'].value
    let len = arr.length


    this.seriesForm.clear()

    console.log('current tab ', this.LayoutsArr[obj.index].SeriesArr);

    let serobj = this.LayoutsArr[obj.index].SeriesArr

    this.seriesForm.push(serobj)
    console.log('after set value current tab ', this.LayoutsArr[obj.index].SeriesArr);
    // if(seriesObj!=undefined){
    //         seriesObj.forEach(element => {
    //           this.seriesForm.push(element)

    //           console.log('elements ', element);

    //         });
    // }




    console.log('series value After', this.LayoutForm.controls['series'].value);


    if (singleObj.title == obj.title) {
      console.log('hello yes its', obj.SeriesArr)

    }
    else {
      obj.SeriesArr = []
    }

  }



  // ****Layout code*****

  addTableLay() {


    this.rectArr.push(
      {
        index: this.i,
        transform: ""
      }
    )

    this.i = this.i + 1

    console.log('rect arr', this.rectArr)
  }


  show(event, i) {

    console.log('index', i)
    console.log('event', event)

    if (this.rectArr[i].index == i) {
      this.rectArr[i].transform = event
    }

    console.log('after', this.rectArr)
  }


  save() {
    this.client = false
    localStorage.setItem('rectArr', JSON.stringify(this.rectArr))
    console.log('Final Layout Object', this.rectArr)
  }


  clientView() {
    this.client = !this.client
  }

  delete() {
    localStorage.removeItem('rectArr')
    this.client = false
    this.i = 0
    this.rectArr = []

  }

  FinalSubmit() {

    localStorage.removeItem('rectArr')

    console.log('rectangle', this.rectArr);

    let layoutObj = { rectArr: this.rectArr }

    this.restServ.postLayout(this.OutletId, { layout: layoutObj, userid: this.userid }).subscribe(res => {
      console.log('response', res);
      if (res['sucess']) {
        Swal.fire('Submitting your Restaurant Successfully', '', 'success')
        this.router.navigate(['/restaurants/'])
      }
      else {
        Swal.fire('Failed to Add Layout', '', 'warning')
      }

    })
    // Swal.fire('Submitting your Restaurant Successfully','','success')
    // this.router.navigate(['/restaurants/add-outlet'])
  }





  // ***********************
  // ************************
  // OUTLET STEPPER FUNCTIONS
  // ***********************
  // ***********************

  ouletRestoIs() {
    console.log(this.outletFormGrp.controls.restaurant_is.value);
    this.ot_restaurant_is = this.outletFormGrp.controls.restaurant_is.value
    this.onChangeOutlet('outlet')
  }



  // ON SELECT INFRASTRUCTURE

  onSelectInfra() {



    // this.outletFormGrp.controls.infrastructure_type.reset()
    this.ot_infrastructure = this.outletFormGrp.controls.infrastructure_type.value

    console.log('selection ', this.ot_infrastructure)
    this.onChangeOutlet('infrastructure')
  }

  // Defalut on Change Outlet is
  onChangeOutlet(type) {
    if (type == 'outlet') {
      this.ot_infrastructure = "Standalone Entity"
      this.outletFormGrp.controls.infrastructure_type.setValue(this.ot_infrastructure)
      this.outletFormGrp.controls.chain_name.reset()
      this.outletFormGrp.controls.chain_id.reset()


    }

    this.outletFormGrp.controls.type_of_building.setValue('Mall')
    this.outletFormGrp.controls.name_of_building.reset()
    // this.outletFormGrp.controls.chain_name.reset()
    this.outletFormGrp.controls.address_of_building.reset()
    // this.outletFormGrp.controls.multiplex_present.reset()
    this.outletFormGrp.controls.multiplex_need_qr.reset()
    this.outletFormGrp.controls.multiplex_name.reset()
    this.outletFormGrp.controls.food_court_name.reset()
    // this.outletFormGrp.controls.food_court_need_qr.reset()
    this.outletFormGrp.controls.multiplex_screen.reset()


    // *****************Validations



    if (this.ot_restaurant_is == 'solo') {
      if (this.ot_infrastructure == "Standalone Entity") {
        this.isOutValid = true
        this.reqValid('ss')


      }

      if (this.ot_infrastructure == 'Food Court') {
        this.isOutValid = false
        this.reqValid('sf')

      }
    }

    if (this.ot_restaurant_is == 'franchise') {

      if (this.ot_infrastructure == "Standalone Entity") {
        this.isOutValid = true
        this.reqValid('cs')



      }

      if (this.ot_infrastructure == 'Food Court') {
        this.isOutValid = false
        this.reqValid('cf')

      }

    }
  }


  solostand() {

    // SOLO STANDALONE SOLO
    if (this.outletFormGrp.controls.type_of_building.value &&
      this.outletFormGrp.controls.name_of_building.value &&
      this.outletFormGrp.controls.address_of_building.value) {
      console.log("yeseeellelelelel");
      this.isSoloStand = false
    }
    else {
      console.log("NOOOOOOOO");
      this.isSoloStand = true
    }


    // SOLO FOOD 



    if (this.outletFormGrp.controls.type_of_building.value &&
      this.outletFormGrp.controls.name_of_building.value &&
      this.outletFormGrp.controls.address_of_building.value &&
      this.outletFormGrp.controls.food_court_name.value
      // this.outletFormGrp.controls.food_court_need_qr.value &&
      // this.outletFormGrp.controls.multiplex_present.value
    ) {
      console.log("yeseeellelelelel");
      this.isSoloFood = false
    }
    else {
      console.log("NOOOOOOOO");
      this.isSoloFood = true
    }


    // CHAIN STANDALONE

    if (this.outletFormGrp.controls.chain_name.value &&
      this.outletFormGrp.controls.type_of_building.value &&
      this.outletFormGrp.controls.name_of_building.value &&
      this.outletFormGrp.controls.address_of_building.value) {
      console.log("yeseeellelelelel");
      this.isChainStand = false
    }
    else {
      console.log("NOOOOOOOO");
      this.isChainStand = true
    }

    // CHAIN FOOD 
    if (this.outletFormGrp.controls.chain_name.value &&
      this.outletFormGrp.controls.type_of_building.value &&
      this.outletFormGrp.controls.name_of_building.value &&
      this.outletFormGrp.controls.address_of_building.value &&
      this.outletFormGrp.controls.food_court_name.value
      // this.outletFormGrp.controls.food_court_need_qr.value &&
      // this.outletFormGrp.controls.multiplex_present.value
    ) {
      console.log("yeseeellelelelel");
      this.isChainFood = false
    }
    else {
      console.log("NOOOOOOOO");
      this.isChainFood = true
    }

  }

  // Validations manipulation functions
  reqValid(type) {

    if (type == 'ss') {
      this.soloStand = true
      this.soloFood = false
      this.chainStand = false
      this.chainFood = false

      console.log("*****************solo----standalone*");

    }

    if (type == 'sf') {
      this.soloStand = false
      this.soloFood = true
      this.chainStand = false
      this.chainFood = false
      console.log("*****************solo----Foood court*");
    }

    if (type == 'cs') {
      this.soloStand = false
      this.soloFood = false
      this.chainStand = true
      this.chainFood = false
      console.log("*****************franchaise----standalone*");
    }

    if (type == 'cf') {
      this.soloStand = false
      this.soloFood = false
      this.chainStand = false
      this.chainFood = true
      console.log("*****************franchasie----Foood court*");
    }

  }

  // // check outlet formgroup
  // checkOutletFormGroup(){
  //   console.log(this.outletFormGrp.value)
  // }


  // ON SAVE Oultet Info
  onSaveOultet() {

    this.isOutletTrue = true
    console.log('oult type of building', this.outletFormGrp.controls.name_of_building.valid)

    let chainName = this.outletFormGrp.controls.chain_name.value

    if (chainName) {
      this.chainArr.forEach(element => {
        if (chainName == element.chain_info.display_name) {
          this.outletFormGrp.controls.chain_id.setValue(element._id)
        }
      });
    }



    let multiplexName = this.outletFormGrp.controls.multiplex_name.value

    if (multiplexName) {
      this.multiplexArr.forEach(element => {
        if (multiplexName == element.display_name) {
          this.outletFormGrp.controls.multiplexid.setValue(element._id)
        }
      });
    }

    this.outletFormGrp.patchValue({
      outlet_code: this.outletcode
    })

    console.log('Save Outlet', this.outletFormGrp.value);

    return new Promise((resolve, reject) => {
      this.restServ.addOultetInfo({ outletInfo: this.outletFormGrp.value, userid: this.userid }).subscribe(res => {
        console.log('Response add outlet', res);
        this.OutletId = res['data']   //***** THIS IS OUTLET'S ID */

        if (res['success']) {
          Swal.fire('Outlet Added Successfully', '', 'success')
        }
        else {
          Swal.fire('Failed to add Outlet', '', 'warning')
        }
        console.log("outlet id received", this.OutletId)
        resolve(this.OutletId)
      })
    }).then(outletId => {

      console.log("organization object", this.OrganizationObj);


      this.outletFormGrp.patchValue({
        org_code: this.OrganizationObj.organizationCode,
        org_name: this.OrganizationObj.organization_name
      })

      console.log("outletForm", this.outletFormGrp.value)
      // ************
      // *********** CHECKING WHETHER ORGANIZATION IS NEW OR OLD*****************
      // ************

      if (!('_id' in this.OrganizationObj)) {
        console.log('*****************Very New Organization*****************************');

      }
      else {
        console.log('************ALREADY EXIST*****************');
      }

      if (!('_id' in this.OrganizationObj)) {
        console.log("New Organization");



        return new Promise((resolve, rejec) => {

          // **** step 1 Adding New Organization
          this.restServ.addOrganization(this.OrganizationObj).subscribe(res => {
            console.log("Response after first time organization add", res);
            this.OrganizationId = res['data']
            resolve(this.OrganizationId)
          })
        }).then(organizationId => {
          // ***** step 2 Adding 1st outlet in New Organization
          this.restServ.addOutletToOrg(organizationId, { outletsid: outletId }).subscribe(res => {
            console.log("Restponse after 1st Outlet Add in New Organization", res)


          })
        })

      }
      else {
        console.log('Organization Exists')

        // **** step 1 If organization EXISTS
        let organizationId = this.OrganizationObj._id
        this.restServ.addOutletToOrg(organizationId, { outletsid: outletId }).subscribe(res => {
          console.log("Restponse after 1st Outlet Add in New Organization", res)

          if (res['success']) {
            Swal.fire('Outlet Added Successfully', '', 'success')
          }
          else {
            Swal.fire('Failed to add Outlet', '', 'warning')
          }
        })
      }
    })



  }

  // Add Multiplex
  addMultiplex() {
    this.router.navigate(['/restaurants/add-cinema'])
  }

  // Add Chain
  addChain() {
    this.router.navigate(['/restaurants/add-chain'])
  }


  // ifsc code to upper case
  ifscUpper() {
    let ifsc = this.LegalForm.controls.restaurant_bank_ifsccode.value
    this.LegalForm.patchValue({
      restaurant_bank_ifsccode: ifsc.toUpperCase()
    })
  }


  // establishment code tabs
  selectTabTouch(id) {
    this.tab_touch = id
  }

  // Menu mgmt code tabs
  selectTabTouch1(id) {
    this.tab_touch1 = id
  }


  // LAYOUT TAB 

  createDialogTab() {

    const dialogRef = this.matDialog.open(PopLayoutTabComponent, {
      width: '450px',
      height: '290px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      if (result) {
        this.layoutTabs.push(result)
        //  console.log('condition', result);
      }

    })
  }

  // Remove tab 
  DeleteTab(obj) {
    this.layoutTabs.splice(this.layoutTabs.indexOf(obj), 1)
    console.log("layoutab array", this.layoutTabs)
  }



  // Establishment Tree
  onTreeEdit() {
    this.isTreeEdit = true
    this.isEstExpand = false
    this.estFiles.forEach(element => {
      element.expanded = true
    })
  }
  // Tree save funtion
  onTreeSave() {
    this.isTreeEdit = false
  }

  // add estalbisment child
  addEstchild() {
    // Dialog box open
    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto',
      data: 'Add Child'
    })

    dialogRef.afterClosed().subscribe(res => {
      console.log('dialog closed')
      if (res) {


        // Pushing Root in Tree Node Array
        this.estNode.children.push(
          {
            "label": res,
            "data": 'child',
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": 'false',
            "children": []
          }
        )

      }
    })

  }

  // Node establishment 

  estNodeSelect(event) {
    this.estNode = event.node
    this.estDelNode = event.node.data

    console.log('est node', this.estNode)
    console.log('est delete node', this.estDelNode)
  }


  // Add Establishment Child Tree
  addEstChild() {
    // Dialog box open
    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto',
      data: 'Add Child'
    })

    // Mat dialog close
    dialogRef.afterClosed().subscribe(res => {

      if (res) {
        this.estNode.children.push(
          {
            label: res,
            data: "child",
            expanded: true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            children: []
          }
        )
      }
    })
  }


  // RENAME CHILD
  RenameChild() {

    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto',
      data: 'Rename Node'
    })

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.estNode.label = res
      }
    })
    console.log(this.estNode.label)
  }





  // Removing Selected Child

  RemoveNode() {


    // leaf node
    if (this.estNode.children.length == 0) {

      const dialogRef = this.matDialog.open(PopDelTreeComponent, {
        width: 'auto',
        data: 'child'
      })

      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.estNode.parent.children.splice(this.estNode.parent.children.indexOf(this.estNode), 1)
        }
      })
    }
    else if (this.estNode.children.length > 0) {
      const dialogRef = this.matDialog.open(PopDelTreeComponent, {
        width: 'auto',
        data: 'child+'
      })
      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.estNode.parent.children.splice(this.estNode.parent.children.indexOf(this.estNode), 1)
        }
      })
    }

  }



  RemoveNode1() {
    if (this.estNode.children.length == 0) {
      const dialogRef = this.matDialog.open(PopDelTreeComponent, {
        width: 'auto',
        data: 'root'
      })

      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.estFiles.splice(this.estFiles.indexOf(this.estNode), 1)
        }
      })
    }
    else {
      const dialogRef = this.matDialog.open(PopDelTreeComponent, {
        width: 'auto',
        data: 'root+'
      })

      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.estFiles.splice(this.estFiles.indexOf(this.estNode), 1)
        }
      })
    }
  }

  RemoveNode2() {
    if (this.estNode.children.length == 0) {
      const dialogRef = this.matDialog.open(PopDelTreeComponent, {
        width: 'auto',
        data: 'root'
      })

      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.menuFiles.splice(this.menuFiles.indexOf(this.estNode), 1)
        }
      })
    }
    else {
      const dialogRef = this.matDialog.open(PopDelTreeComponent, {
        width: 'auto',
        data: 'root+'
      })

      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.menuFiles.splice(this.menuFiles.indexOf(this.estNode), 1)
        }
      })
    }
  }




  //dIOALOG BOX FOR Delete tree node confirm DIALOG BOX
  // #########################  1. leaf node delete confirmation
  confirmDelete() {
    this.confirmationService.confirm({
      message: 'Do you want to delete this node?',
      header: 'Delete Confirmation',
      acceptIcon: 'pi pi-check-circle',
      rejectIcon: 'pi pi-times-circle',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.estNode.parent.children.splice(this.estNode.parent.children.indexOf(this.estNode), 1)
      },
      reject: () => {
        console.log('Reject')
      }
    });
  }

  // #########################  2. Node with Child nodes delete confirmation
  confirmDelete1() {
    this.confirmationService.confirm({
      message: 'Are you sure to delete it, its has ' + this.estNode.children.length + ' nodes!',
      header: 'Delete Confirmation',
      acceptIcon: 'pi pi-check-circle',
      rejectIcon: 'pi pi-times-circle',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.estNode.parent.children.splice(this.estNode.parent.children.indexOf(this.estNode), 1)
      },
      reject: () => {
        console.log('Reject')
      }
    });
  }


  // #########################  3. Root node no child delete confirmation
  confirmDelete2() {
    this.confirmationService.confirm({
      message: 'Do you want to delete this Root node?',
      header: 'Delete Confirmation',
      acceptIcon: 'pi pi-check-circle',
      rejectIcon: 'pi pi-times-circle',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.estFiles.splice(this.estFiles.indexOf(this.estNode), 1)
      },
      reject: () => {
        console.log('Reject')
      }
    });
  }

  // #########################  4. Root Node with childs
  confirmDelete3() {
    this.confirmationService.confirm({
      message: 'Are you sure to delete this root node, its has ' + this.estNode.children.length + ' nodes!',
      header: 'Delete Confirmation',
      acceptIcon: 'pi pi-check-circle',
      rejectIcon: 'pi pi-times-circle',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.estFiles.splice(this.estFiles.indexOf(this.estNode), 1)
      },
      reject: () => {
        console.log('Reject')
      }
    });
  }

  // ON ADD EDIT MENU TREE
  onAddEditMenu() {
    this.ismenuTree = true
    this.isMenuExpand = false
    this.menuFiles.forEach(element => {
      element.expanded = true
    })
  }

  // ON SAVE MENU TREE
  onSaveMenu() {
    this.ismenuTree = false
  }


  // Add Menu Root
  addMenuRoot() {
    // Dialog box open
    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto',
      data: 'Add Root'
    })

    dialogRef.afterClosed().subscribe(res => {
      console.log('dialog closed')
      if (res) {


        // Pushing Root in Tree Node Array
        this.menuFiles.push(
          {
            "label": res,
            "data": 'child',
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,
            "children": []
          }
        )

      }
    })
  }


  // Add ESTABLISH ROOT
  addEstRoot() {
    // Dialog box open
    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto',
      data: 'Add Root'
    })

    dialogRef.afterClosed().subscribe(res => {
      console.log('dialog closed')
      if (res) {


        // Pushing Root in Tree Node Array
        this.estFiles.push(
          {
            "label": res,
            "data": 'child',
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,
            "children": []
          }
        )

      }
    })
  }



  // ESTABLISHMENT EXPAND COLLAPSE
  onEstExpand() {
    this.isEstExpand = !this.isEstExpand
    this.estFiles.forEach(element => {
      element.expanded = !this.isEstExpand
    })
  }


  // MENU EXPAND COLLAPSE
  onMenuExpand() {
    this.isMenuExpand = !this.isMenuExpand
    this.menuFiles.forEach(element => {
      element.expanded = !this.isMenuExpand
    })
  }


  // OTHER ALBUM CHIPS
  addOtherChip(){
    let otherchip = this.otheralbum.controls.name.value;
    if(!this.albumOtherChip.includes(otherchip)){
      this.albumOtherChip.push(otherchip)
      this.otheralbum.controls.name.reset();
    }
    else{
      Swal.fire('Already exists!','','warning')
    }
    
    
  }

  // OTHER CHIPS
  delOtherchip(item){
    this.albumOtherChip.splice(this.albumOtherChip.indexOf(item),1)
    this.albumNames.splice(this.albumNames.indexOf(item),1)
  }

  // Remov chips
  removeAlbum(item){
    this.albumNames.splice(this.albumNames.indexOf(item),1)
  }


  // put common album
  putAlbum(item){
    if(!this.albumNames.includes(item)){
      this.albumNames.push(item)
    }
    else{
      Swal.fire('Album already exists!!','','info')
    }
  }


  // remove gallery
  removeGallery(obj){
    this.album_image.splice(this.album_image.indexOf(obj), 1)
    this.albumNames.push(obj.name)
  }
}
