import { Component, OnInit, Input } from '@angular/core';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { ChainCellRenderComponent } from '../chain-cell-render/chain-cell-render.component';
import { Router } from '@angular/router';



@Component({
  selector: 'app-chain-home',
  templateUrl: './chain-home.component.html',
  styleUrls: ['./chain-home.component.scss']
})
export class ChainHomeComponent implements OnInit {

      // column data name

      columnDefs = [
        { 
          headerName: 'Chain Name', 
          field: 'chain_info.display_name', 
          sortable: true, filter: true, 
          sortingOrder: ["asc", "desc"],
          "cellRendererFramework":  ChainCellRenderComponent
        },
        { 
          headerName: 'Legal Name', 
          field: 'chain_info.legal_name', 
          sortable: true, filter: true, 
          sortingOrder: ["asc", "desc"] 
        },
        // { headerName: 'Sub Chain', field: 'sub_chain', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'Outlet Assigned', field: 'assigned_outlet', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'Status', field: 'outletInfo.infrastructure_type', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'Action', field: 'restoInfo.restoPriceForTwo', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // // { headerName: 'Restaurants Type', field: 'restoInfo.restoType', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'Address', field: 'restoInfo.restoAddress', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'Website', field: 'restoInfo.restoWebsite', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'Account Number', field: 'legalInfo.accountNo', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'IFSC Code', field: 'legalInfo.IFSCCode', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'Account Type', field: 'legalInfo.accountType', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'GST Number', field: 'legalInfo.restoWebsite', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'FSSAI Number', field: 'legalInfo.restoWebsite', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
        // { headerName: 'PAN Number', field: 'legalInfo.restoWebsite', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    
    
      ];
      rowData: any = [
       ];


         // ngPrime Table
  cols: any = [];
  outletTable: any = [];
  _selectedColumns: any[];
  selected1: any;


  constructor(private restServ: RestoService, private router: Router) { 

    this.restServ.getAllChains().subscribe(data=>{
      console.log(data['data']);
      
      this.rowData = data['data']

      if(this.rowData!==undefined){
        this.tableData()
      }

     
    })

  }

  ngOnInit() {
  }


  // NG PRIME DATATABLE
  tableData() {

    let chainData = []

    this.rowData.forEach(element => {
      chainData.push(
        {
          id: element._id,
          code: element.chain_info.chain_code,
          name: element.chain_info.display_name,
          legal: element.chain_info.legal_name
        }
      )
    });

    this.outletTable = chainData

    this.cols=[
      {field: "code", header: "Chain Code"},
      {field: "name", header: "Chain Name"},
      {field: "legal", header: "Legal Name"}
    ]

    this._selectedColumns = this.cols
  }

   // functions ng prime

   view(id){
     console.log('id',id)
    this.rowData.forEach(element => {
      if(element._id == id){
        this.restServ.view_update = element
        console.log('elment',element)
      }
    });
   
    this.router.navigate(['restaurants/view-update-chain'])
  }

    // ON CHECKBOX CLICK
    onChk() {
      console.log("selected1", this.selected1);
    }
  
    // show hide columns
    @Input() get selectedColumns(): any[] {
      return this._selectedColumns;
    }
  
    set selectedColumns(val: any[]) {
      //restore original order
      this._selectedColumns = this.cols.filter((col) => val.includes(col));
  
      console.log("Set value of selected columns : ", this._selectedColumns);
    }

  // delete all 
  deleteAll(){
    this.restServ.deleteAllChains().subscribe(res=>{
      console.log('response after delete',res);
      
    })



  }



}
