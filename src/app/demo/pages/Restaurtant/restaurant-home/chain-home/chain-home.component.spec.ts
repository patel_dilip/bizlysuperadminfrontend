import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChainHomeComponent } from './chain-home.component';

describe('ChainHomeComponent', () => {
  let component: ChainHomeComponent;
  let fixture: ComponentFixture<ChainHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChainHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChainHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
