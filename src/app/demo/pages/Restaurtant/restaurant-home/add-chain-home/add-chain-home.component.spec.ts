import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChainHomeComponent } from './add-chain-home.component';

describe('AddChainHomeComponent', () => {
  let component: AddChainHomeComponent;
  let fixture: ComponentFixture<AddChainHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddChainHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChainHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
