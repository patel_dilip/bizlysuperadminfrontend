                                                                                                                                                                                                                        import { Component, OnInit } from '@angular/core';
import { FormGroup, Form, FormBuilder, FormControl, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { NgxSpinnerService } from "ngx-spinner";
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { TreeNode } from 'primeng/api';
import { MatDialog } from '@angular/material';
import { PopTreeComponent } from '../pop-tree/pop-tree.component';
import { element, Command } from 'protractor';
import { ConfirmationService } from 'primeng/api';
import {MenuItem} from 'primeng/api';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';


// Logo image file uploader URL
const LogoImg = environment.base_Url + "restaurant/uploadrestaurantlogo";
const ImagesAlbum = environment.base_Url + "restaurant/uploadrestoalbumimages"
const VideoAlbum = environment.base_Url + "restaurant/uploadrestoalbumvideos"

@Component({
  selector: 'app-add-chain-home',
  templateUrl: './add-chain-home.component.html',
  styleUrls: ['./add-chain-home.component.scss']
})
export class AddChainHomeComponent implements OnInit {

  nodes: any = []


  rootButton = {
    "label": 'Add Root',
    "data": 'root'
  }

  catButton = {
    "label": 'Add Category',
    "data": 'category'
  }

  files: TreeNode[] = [this.rootButton];
  files2: TreeNode[] = []
  files1: TreeNode[] = [
    {
      "label": "Document",
      "data": "Doc Folder",
      "expanded": true,
      "children": [
        {
          "label": "Pdf",
          "data": "PDF Folder",
          "expanded": true,
          "children": [
            {
              "label": "school",
              "data": "school",
              "expanded": true

            }
          ]
        }
      ]
    },
    {
      "label": "Memo",
      "data": "memo Folder",
      "expanded": true,
      "children": [
        {
          "label": "notice",
          "data": "notice Folder",
          "expanded": true,
          "children": [
            {
              "label": "college",
              "data": "college",
              "expanded": true

            }
          ]
        }
      ]
    }
  ]

  actionButton: boolean
  showTree: boolean = false

  chainInfo: FormGroup;
  // MediaInfo: FormGroup;
  // OutletInfo: FormGroup;
  // EstablishInfo: FormGroup;

  // Establishment Variables

  moreInfoArr: any = []
  servicesArr: any = []
  rulesArr: any = []
  ambienceArr: any = []
  cuisinesArr: any = []

  // Selected array of estalbisment
  selectedmore: any = []
  selectedservices: any = []
  selectedrules: any = []
  selectedambiences: any = []
  selectedcuisines: any = []


  selected = new FormControl(0);

  // FILE UPLOADER VARIABLES
  public logoUploader: FileUploader = new FileUploader({ url: LogoImg, itemAlias: 'restologo' });
  public ImgAlbumUploader: FileUploader = new FileUploader({ url: ImagesAlbum, itemAlias: 'restoalbumimages' });
  public VideoAlbumUploader: FileUploader = new FileUploader({ url: VideoAlbum, itemAlias: 'restoalbumvideos' });

  logoResponse = ''
  showLogo = false

  VideoAlbumResponse = []
  showVideos = false

  ImgAlbumResponse = []
  showImages = false
  finalObj: any;
  rowData: any = [];
  chaincode: string;


  // NEW TREEE VARIABLES

  files3: TreeNode[] = []
  // files4: TreeNode[]= []
  newSelect: TreeNode
  newSelect1: TreeNode
  extNode: any;
  isChildAct: boolean = true
  isExpand: boolean = true
  isView: boolean = false;
  nodelmsg: any;
  delNode: any = 'root';
  items2 : MenuItem[]


  filteredOptions: Observable<string[]>;
  options = []
  chaindata: any = [];
  isChainExist: boolean = false;
  isdupDisplay: boolean = false;
  allChainsTab: any[] = [];
  otheralbum: FormGroup;


  constructor(private fb: FormBuilder,
    private ngxSpinner: NgxSpinnerService,
    private restServ: RestoService,
    private router: Router,
    private matDialog: MatDialog,
    private confirmationService: ConfirmationService) {

    // ************Chain Info*****************
    this.chainInfo = this.fb.group({
      chain_code: [''],
      display_name: [''],
      legal_name: [''],
      chain_logo: [''],
      chain_pictures: [[]],
      chain_videos: [[]],
      outlets_tree: [''],
      establish_moreinfo: [''],
      establish_services: [''],
      establish_rules: [''],
      establish_ambience: [''],
      establish_cuisines: [''],
      restaurant_logo: [''],
      restaurant_banner: [''],
      
    })

    // ************Media Info*****************
    // this.MediaInfo = this.fb.group({
    //   chain_logo: [''],
    //   chain_pictures: [''],
    //   chain_videos: ['']
    // })

    // ************Outlets Info*****************
    // this.OutletInfo = this.fb.group({
    //   outlets_tree: ['']
    // })

    // ************Establishment Info*****************
    // this.EstablishInfo = this.fb.group({
    //   establish_moreinfo: [''],
    //   establish__services: [''],
    //   establish_rules_and_regulations: [''],
    //   establish_ambience: [''],
    //   establish_cuisines: [''],
    // })

    // ***************NODE array***********
    this.nodes = [
      {

        name: 'Mc Donalds',
        is_selected: 'false',
        children: [
          {


            name: 'Mc Donalds',
            is_selected: 'false',

          },
          {


            name: 'Mc Cafe',
            is_selected: 'false',

          }
        ]
      },
      {

        name: 'Burger King',
        is_selected: 'false',
        children: [
          {


            name: 'Burger Eatables',
            is_selected: 'false',

          },
          {


            name: 'Burger King Cafe',
            is_selected: 'false',

          }
        ]
      },
      {

        name: 'New Poona Bakery',
        is_selected: 'false',
        children: [

          {


            name: 'Bakery',
            is_selected: 'false',

          }
        ]
      }]


    // ****************Establishment**************

    // MORE INFO ARRAY

    this.moreInfoArr = [
      { 'id': '1', 'name': 'Events' },
      { 'id': '2', 'name': 'Hospital' },
      { 'id': '3', 'name': 'Studios' },
      { 'id': '4', 'name': 'SPA' },
      { 'id': '5', 'name': 'Nursery' },
      { 'id': '6', 'name': 'Cloak Room' },
    ]

    // SERVICES INFO ARRAY

    this.servicesArr = [
      { 'id': '1', 'name': 'Spa' },
      { 'id': '2', 'name': 'Massages' },
      { 'id': '3', 'name': 'Home Delievery' },
      { 'id': '4', 'name': 'Take Away' },
      { 'id': '5', 'name': 'Dine-In' }
    ]

    // RULES & REGULATIONS ARRAY

    this.rulesArr = [
      { 'id': '1', 'name': 'No Smoking' },
      { 'id': '2', 'name': 'No Outside Eatables' },
      { 'id': '3', 'name': 'No Pets' },
      { 'id': '4', 'name': 'No Parking' },
      { 'id': '5', 'name': 'BYOB' },
      { 'id': '6', 'name': 'Family Friendly' },
      { 'id': '7', 'name': 'Couple Friedndly' }

    ]

    // AMBIENCES ARRAY

    this.ambienceArr = [
      { 'id': '1', 'name': 'Pool Side' },
      { 'id': '2', 'name': 'Dance Floor' },
      { 'id': '3', 'name': 'Lounge' },
      { 'id': '4', 'name': 'Lawn Area' },
      { 'id': '5', 'name': 'Outdoors' },
      { 'id': '6', 'name': 'Rooftops' },
    ]

    // CUISINES ARRAY

    this.cuisinesArr = [
      { 'id': '1', 'name': 'BBQ' },
      { 'id': '2', 'name': 'Continental' },
      { 'id': '3', 'name': 'Chinese' },
      { 'id': '4', 'name': 'Indian' },
      { 'id': '5', 'name': 'Italian' },
      { 'id': '6', 'name': 'Punjabi' },
      { 'id': '7', 'name': 'South Indian' },
    ]


    // Treee variables

  

  }

  ngOnInit() {

    // other albums
      // *****************OTHER ALUBM FORM GROUP
      this.otheralbum = this.fb.group({
        name: ['', Validators.required]
      })

    // Tree variables
    this.items2 = [
      {
        label: "Edit",
        icon: 'pi pi-fw pi-pencil',
        command: (event)=> alert('Edit function')
      },
      {
        separator: true
      },
      {
        label: "Delete",
        icon: 'pi pi-fw pi-trash',
        command: (event)=> alert('Delete function')
      }
    ]

    // Generating Unique Chain Code

    this.restServ.getAllChains().subscribe(data => {
      console.log(data['data']);
      this.chaindata = data['data']

// chain data for loop options
this.chaindata.forEach(element => {
  this.options.push(element.chain_info.display_name)
});


      this.rowData = data['data']


      if (this.rowData.length == 0) {
        let len = this.rowData.length + 1   //length if array is empty 0+1
        let str = len.toString()            // converting 1 to string '1'
        let size = 3                        // size of code
        this.chaincode = 'CHN' + str.padStart(size, '0')  // padding with 00 to left ex: 001
        console.log('CHAIN code', this.chaincode);

      }
      else {

        console.log("all org lenth", this.rowData.length);
        let index = this.rowData.length - 1

        let last_code = this.rowData[index].chain_info.chain_code
        console.log('last_code', last_code);
        let str = last_code.slice(3)
        console.log('string', str);
        let digit = parseInt(str)
        digit = digit + 1
        let str1 = digit.toString()
        let size = 3
        this.chaincode = 'CHN' + str1.padStart(size, '0')
        console.log('Chian code', this.chaincode);


      }




    })



    // LOGO UPLOADS

    this.logoUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.ngxSpinner.hide()
      console.log('FileUpload:uploaded:', item, status, response);
      if (status == 200) {
        this.showLogo = true
      }
      this.logoResponse = response
      console.log('response', this.logoResponse)

      this.chainInfo.controls.chain_logo.setValue(this.logoResponse)
      console.log(this.chainInfo.value);
    }


    // Images Uploads

    this.ImgAlbumUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {


      console.log('FileUpload:uploaded:', item, status, response);
      this.ImgAlbumResponse.push(response)
      this.ngxSpinner.show()
      console.log('Images Response', this.ImgAlbumResponse);
      this.showImages = true
      this.ngxSpinner.hide()
      this.chainInfo.controls.chain_pictures.setValue(this.ImgAlbumResponse)
      console.log(this.chainInfo.value);

    }

    // chain_logo: [''],
    //   chain_pictures: [''],
    //   chain_videos: [''],
    //Videos uploads

    this.VideoAlbumUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // this.VideoAlbumResponse = []
      console.log('FileUpload:uploaded:', item, status, response);

      this.VideoAlbumResponse.push(response)

      console.log(' Video Response', this.VideoAlbumResponse);
      this.showVideos = true
      this.ngxSpinner.hide();

      this.chainInfo.controls.chain_videos.setValue(this.VideoAlbumResponse)
      console.log(this.chainInfo.value);
    }


    this.filteredOptions = this.chainInfo.controls.display_name.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );

  }

// FILTER FUNCTIONS
private _filter(value: string): string[] {
  const filterValue = value.toLowerCase();

  return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
}


  // loading spinner
  loading() {
    this.ngxSpinner.show()
  }


  // On saving Chain Info

  saveChainInfo(value) {


    this.chaindata.forEach(element => {
      if(value == element.chain_info.display_name){
       Swal.fire('Chain Name Already Exists','','info');
      }
    });

    console.log('****Chain Info*****', this.chainInfo.value);


    // CREATING TREEE ROOT NODE
    this.files3 = [{
      "label": value,
      "data": "root",
      "expanded": true,
      "expandedIcon": "pi pi-folder-open",
      "collapsedIcon": "pi pi-folder",
      "leaf": false,           
      "children": []
    }] 
    
    this.extNode = this.files3[0]


    this.allChainsTab = [value+' (Main)']

    console.log('all chain tabs', this.allChainsTab)
  }

  // On saving Media Info

  saveMediaInfo() {
    console.log('****Media Info*****', this.chainInfo.value);

  }

  // On saving Outlet Info

  saveOutletInfo() {
    console.log('****Outlet Info*****', this.chainInfo.value);

  }

  // On Establishment Chain Info

  saveEstablishInfo() {
    console.log('****Estalbish Info*****', this.chainInfo.value);

  }



  // Next Tab
  nextTab(item) {
    this.selected.setValue(parseInt(item))
  }


  // SELECTED ESTABLISHMENT CHIPS

  optMoreInfo(moreinfo) {
    this.selectedmore.push(moreinfo)
    console.log('selected more info', this.selectedmore)

    this.moreInfoArr.splice(this.moreInfoArr.indexOf(moreinfo), 1)
    console.log(' more info array', this.moreInfoArr)

    this.chainInfo.controls.establish_moreinfo.setValue(this.selectedmore)
    console.log(this.chainInfo.value)
  }

  optserviceInfo(service) {
    this.selectedservices.push(service)
    console.log('selected selectedservices', this.selectedservices)

    this.servicesArr.splice(this.servicesArr.indexOf(service), 1)
    console.log(' servicesArr array', this.servicesArr)

    this.chainInfo.controls.establish_services.setValue(this.selectedservices)
    console.log(this.chainInfo.value)
  }

  optRules(rule) {
    this.selectedrules.push(rule)
    console.log('selected selected rule', this.selectedrules)

    this.rulesArr.splice(this.rulesArr.indexOf(rule), 1)
    console.log(' rulesArr info array', this.rulesArr)

    this.chainInfo.controls.establish_rules.setValue(this.selectedrules)
    console.log(this.chainInfo.value)
  }

  optAmbience(ambience) {
    this.selectedambiences.push(ambience)
    console.log('selected selectedambiences', this.selectedambiences)

    this.ambienceArr.splice(this.ambienceArr.indexOf(ambience), 1)
    console.log(' ambienceArr array', this.ambienceArr)

    this.chainInfo.controls.establish_ambience.setValue(this.selectedambiences)
    console.log(this.chainInfo.value)
  }

  optCuisine(cuisine) {
    this.selectedcuisines.push(cuisine)
    console.log('selected selectedcuisines', this.selectedcuisines)

    this.cuisinesArr.splice(this.cuisinesArr.indexOf(cuisine), 1)
    console.log(' cuisinesArr array', this.cuisinesArr)

    this.chainInfo.controls.establish_cuisines.setValue(this.selectedcuisines)
    console.log(this.chainInfo.value)
  }


  // **REMOVE ESTABLISMENT CHIPS

  removeMoreinfo(moreinfo) {

    this.moreInfoArr.push(moreinfo)
    console.log('moreInfoArr', this.moreInfoArr)

    this.selectedmore.splice(this.selectedmore.indexOf(moreinfo), 1)
    console.log('selected more info', this.selectedmore)

    this.chainInfo.controls.establish_moreinfo.setValue(this.selectedmore)
    console.log(this.chainInfo.value)
  }

  removeServices(service) {

    this.servicesArr.push(service)
    console.log(' servicesArr array', this.servicesArr)

    this.selectedservices.splice(this.selectedservices.indexOf(service), 1)
    console.log('selected selectedservices', this.selectedservices)

    this.chainInfo.controls.establish_services.setValue(this.selectedservices)
    console.log(this.chainInfo.value)
  }
  removeRules(rule) {

    this.rulesArr.push(rule)
    console.log(' rulesArr info array', this.rulesArr)

    this.selectedrules.splice(this.selectedrules.indexOf(rule), 1)
    console.log('selected selected rule', this.selectedrules)

    this.chainInfo.controls.establish_rules.setValue(this.selectedrules)
    console.log(this.chainInfo.value)

  }

  removeAmbience(ambience) {

    this.ambienceArr.push(ambience)
    console.log(' ambienceArr array', this.ambienceArr)


    this.selectedambiences.splice(this.selectedambiences.indexOf(ambience), 1)
    console.log('selected selectedambiences', this.selectedambiences)

    this.chainInfo.controls.establish_ambience.setValue(this.selectedambiences)
    console.log(this.chainInfo.value)
  }

  removeCuisine(cuisine) {

    this.cuisinesArr.push(cuisine)
    console.log(' cuisinesArr array', this.cuisinesArr)


    this.selectedcuisines.splice(this.selectedcuisines.indexOf(cuisine), 1)
    console.log('selected selectedcuisines', this.selectedcuisines)

    this.chainInfo.controls.establish_cuisines.setValue(this.selectedcuisines)
    console.log(this.chainInfo.value)

  }


  // #########
  // FInal Submit Function
  SaveFinal() {

    let obj = {
      chain_info: {
        chain_code: this.chaincode,
        display_name: this.chainInfo.controls.display_name.value,
        legal_name: this.chainInfo.controls.legal_name.value,
      },
      media_info: {
        chain_logo: this.chainInfo.controls.chain_logo.value,
        chain_pictures: this.chainInfo.controls.chain_pictures.value,
        chain_videos: this.chainInfo.controls.chain_videos.value
      },
      outlets: {
        outlets_tree: []
      },
      establishment_info: {
        establish_moreinfo: this.chainInfo.controls.establish_moreinfo.value,
        establish_services: this.chainInfo.controls.establish_services.value,
        establish_rules: this.chainInfo.controls.establish_rules.value,
        establish_ambience: this.chainInfo.controls.establish_ambience.value,
        establish_cuisines: this.chainInfo.controls.establish_cuisines.value
      }
    }

    console.log('FINAL OBJE', obj);
    this.finalObj = obj
    this.restServ.addChain(obj).subscribe(res => {
      console.log('response', res);
      if (res['success']) {
        Swal.fire('Chain Added Successfully', '', 'success')
        this.router.navigate(['/restaurants/chain-home'])
      }
      else {
        Swal.fire('Failed Add Chain', '', 'warning')
      }

    })

  }



  // Tree Code

  addRoot(node) {

    console.log('node', node)
    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto'
    })

    dialogRef.afterClosed().subscribe(res => {
      console.log('dialog closed res', res)
      if (res) {
        console.log(res)

        this.files.splice(this.files.indexOf(this.rootButton), 1)
        this.files.push(
          {
            "label": res,
            "data": "root text",
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "children": [
              {
                "label": 'Add Category',
                "data": 'category',
                "key": res

              }
            ]
          },
          this.rootButton
        )
        // files2 code




        //  this.files2 = calc
      }
    })


  }


  // add CATEOGORY
  addCategory(node) {
    console.log('category button', node)

    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto'
    })

    dialogRef.afterClosed().subscribe(res => {
      console.log(res)

      if (res) {


        // files code 
        this.files.forEach(element => {
          if (element.label == node.key) {
            let child = element.children
            element.children.splice(
              element.children.indexOf(
                {
                  "label": 'Add Category',
                  "data": 'category',
                  "key": node.key
                }
              ), 1)
            element.children.push(
              {
                "label": res,
                "data": "category text",
                "expandedIcon": "pi pi-folder-open",
                "collapsedIcon": "pi pi-folder",
                "children": [
                  {
                    "label": 'Add Sub-Category',
                    "data": 'sub',
                    "key": res

                  }
                ]
              },
              {
                "label": 'Add Category',
                "data": 'category',
                "key": node.key
              }
            )
          }
        });


        // Files 2 push
        this.files2.forEach(element => {
          if (element.label == node.key) {
            element.children.push(
              {
                "label": res,
                "data": "category text",
                "children": []
              }
            )
          }
        })

        console.log('files2', this.files2)




      }
    })
  }


  // add Sub Category
  addSub(node) {
    console.log('node', node)

    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto'
    })

    dialogRef.afterClosed().subscribe(res => {
      console.log(res)
      if (res) {


        // files code

        this.files.forEach(element => {
          element.children.forEach(ele => {
            if (ele.label == node.key) {

              ele.children.splice(
                ele.children.indexOf(
                  {
                    "label": 'Add Sub-Category',
                    "data": 'sub',
                    "key": node.key
                  }
                ), 1)
              ele.children.push(
                {
                  "label": res,
                  "data": "sub text",
                  "expandedIcon": "pi pi-folder-open",
                  "collapsedIcon": "pi pi-folder",
                  "children": [
                    {
                      "label": "Add Child",
                      "data": "child",
                      "key": res
                    }
                  ]
                },
                {
                  "label": 'Add Sub -Category',
                  "data": 'sub',
                  "key": node.key
                }
              )


            }
          })
        })



        // files2 code

        this.files2.forEach(element => {
          element.children.forEach(ele => {
            console.log("important op", ele.label + "  " + node.key)
            if (ele.label == node.key) {

              console.log("important op", ele.label + "  " + node.key)
              ele.children.push(
                {
                  "label": res,
                  "data": "sub text",
                  "children": []
                }
              )
            }
          })
        })

        console.log('files2', this.files2)





      }
    })
  }

  // add child
  addChild(node) {
    console.log('node', node)

    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto'
    })

    dialogRef.afterClosed().subscribe(res => {
      console.log('res', res)
      if (res) {

        this.files.forEach(element => {
          element.children.forEach(element1 => {
            element1.children.forEach(element2 => {
              if (element2.label == node.key) {
                element2.children.splice(
                  element2.children.indexOf(
                    {
                      "label": "Add Child",
                      "data": "child",
                      "key": node.key
                    }
                  ), 1
                )
                element2.children.push(
                  {
                    "label": res,
                    "data": "child text",
                    "expandedIcon": "pi pi-folder-open",
                    "collapsedIcon": "pi pi-folder",
                    "children": []
                  },
                  {
                    "label": "Add Child",
                    "data": "child",
                    "key": node.key
                  }
                )
              }
            })
          })
        })

        // files2 upload

        this.files.forEach(element => {
          element.children.forEach(element1 => {
            element1.children.forEach(element2 => {
              if (element2.label == node.key) {
                element2.children.push(
                  {
                    "label": res,
                    "data": "child text",
                    "children": []
                  }
                )
              }
            })
          })
        })

        console.log('files2', this.files2)
      }
    })
  }

  // CHECK ACTION BUTTON STATUS

  onSwitch() {
    this.showTree = !this.showTree
  }


  // ****************************************NEW add tree*****************************************************************


  // function 1 new root add

  NewRoot() {

    // Dialog box open
    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto',
      data: 'Add Root'
    })

    dialogRef.afterClosed().subscribe(res => {
      console.log('dialog closed new res', res)
      if (res) {

        // Pushing Root in Tree Node Array
        this.files3.push(
          {
            "label": res,
            "data": "root",
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,           
            "children": []
          }
        )

      }
    })

  }


  // function 2 node select

  nodeSelect(event) {

    console.log('event', event.node)

    console.log('new select', this.newSelect);


    this.extNode = event.node
    this.delNode = event.node.data

    // Selected node Counting
    if (this.newSelect) {
      this.isChildAct = false
    }

    console.log('files3', this.files3)
  }


  // ADDING A NEW CHILD
  NewChild() {
    // Dialog box open
    const dialogRef = this.matDialog.open(PopTreeComponent, {
      width: 'auto',
      data: 'Add Child'
    })

    dialogRef.afterClosed().subscribe(res => {
      console.log('dialog closed')
      if (res) {

      
        // Pushing Root in Tree Node Array
        this.extNode.children.push(
          {
            "label": res,
            "data": "child",
            "expanded": true,
            "expandedIcon": "pi pi-folder-open",
            "collapsedIcon": "pi pi-folder",
            "leaf": false,
            "children": []
          }
        )



        // multiple tabs creation
        this.allChainsTab.push(res)
        console.log('all chain tabs', this.allChainsTab)
      }
    })

 
    console.log("files 3", this.files3)

  }


  // Removing Selected Child

  RemoveNode() {

      // leaf node
      if (this.extNode.children.length == 0) {
        this.confirmDelete();
      }
      else if(this.extNode.children.length>0) {
        this.confirmDelete1();
      }
    console.log(this.newSelect)
  }



  RemoveNode1(){
    if (this.extNode.children.length == 0) {
          this.confirmDelete2();
        }
        else {
          this.confirmDelete3();
        }
  }

  // Expand code

  ExpandNode() {
    this.isExpand = !this.isExpand

    console.log('is Expand', this.isExpand)

    this.files3.forEach(element => {
      element.expanded = this.isExpand
    })


  
  }


  // VIEW MODE CODE
  viewMode() {
    this.isView = !this.isView
  }


  //dIOALOG BOX FOR Delete tree node confirm DIALOG BOX
  // #########################  1. leaf node delete confirmation
  confirmDelete() {
    this.confirmationService.confirm({
      message: 'Do you want to delete this node?',
      header: 'Delete Confirmation',
      acceptIcon: 'pi pi-check-circle',
      rejectIcon: 'pi pi-times-circle',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.extNode.parent.children.splice(this.extNode.parent.children.indexOf(this.extNode), 1)
      },
      reject: () => {
        console.log('Reject')
      }
    });
  }

  // #########################  2. Node with Child nodes delete confirmation
  confirmDelete1() {
    this.confirmationService.confirm({
      message: 'Are you sure to delete it, its has ' + this.extNode.children.length + ' nodes!',
      header: 'Delete Confirmation',
      acceptIcon: 'pi pi-check-circle',
      rejectIcon: 'pi pi-times-circle',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.extNode.parent.children.splice(this.extNode.parent.children.indexOf(this.extNode), 1)
      },
      reject: () => {
        console.log('Reject')
      }
    });
  }


  // #########################  3. Root node no child delete confirmation
  confirmDelete2() {
    this.confirmationService.confirm({
      message: 'Do you want to delete this Root node?',
      header: 'Delete Confirmation',
      acceptIcon: 'pi pi-check-circle',
      rejectIcon: 'pi pi-times-circle',
      icon: 'pi pi-info-circle',
      accept: () => {
        this.files3.splice(this.files3.indexOf(this.extNode), 1)
      },
      reject: () => {
        console.log('Reject')
      }
    });
  }

 // #########################  4. Root Node with childs
 confirmDelete3() {
  this.confirmationService.confirm({
    message: 'Are you sure to delete this root node, its has ' + this.extNode.children.length + ' nodes!',
    header: 'Delete Confirmation',
    acceptIcon: 'pi pi-check-circle',
    rejectIcon: 'pi pi-times-circle',
    icon: 'pi pi-info-circle',
    accept: () => {
      this.files3.splice(this.files3.indexOf(this.extNode), 1)
    },
    reject: () => {
      console.log('Reject')
    }
  });
}

RenameChild(){

  const dialogRef = this.matDialog.open(PopTreeComponent,{
    width:'auto',
    data: 'Rename Node'
  })

  dialogRef.afterClosed().subscribe(res=>{
    if(res){
      this.extNode.label = res
    }
  })
console.log(this.extNode.label)
}


// Double click event
dbl(event){
 this.extNode.expanded = !this.extNode.expanded

 this.files3.forEach(ele =>{
   if(ele.expanded==false){
    this.isExpand = false 
   }
   else{
     this.isExpand = true
     return false
   }
 })

}



// SELECTED CHAIN 
onSelectedChain(value){
  
  this.chaindata.forEach(element => {
    if(value == element.chain_info.display_name){
      this.chainInfo.patchValue({
        display_name: element.chain_info.display_name,
        legal_name: element.chain_info.legal_name
      })

      this.isChainExist = true
    }
  });
}


// On Cancel Existing Chain
onCancelChain(){
  this.isChainExist = false

  this.chainInfo.controls.display_name.setValue('')
  this.chainInfo.controls.legal_name.setValue('')
}





}