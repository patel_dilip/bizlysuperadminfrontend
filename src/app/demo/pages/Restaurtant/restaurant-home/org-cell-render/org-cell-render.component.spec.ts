import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgCellRenderComponent } from './org-cell-render.component';

describe('OrgCellRenderComponent', () => {
  let component: OrgCellRenderComponent;
  let fixture: ComponentFixture<OrgCellRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgCellRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgCellRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
