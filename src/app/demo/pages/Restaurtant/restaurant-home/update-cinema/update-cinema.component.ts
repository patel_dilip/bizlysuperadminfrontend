import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-cinema',
  templateUrl: './update-cinema.component.html',
  styleUrls: ['./update-cinema.component.scss']
})
export class UpdateCinemaComponent implements OnInit {

  cinemaInfo: FormGroup;
  updateData: any
  series_types:any = ['FIXED + SERIES', 'SERIES + FIXED', 'SERIES + FIXED + SERIES','FIXED + SERIES + FIXED']
  type_of_series: any = 'FIXED + SERIES';

  constructor(
    private router: Router,
    private restServ: RestoService,
    private fb: FormBuilder
  ) {

    this.updateData = this.restServ.update_cinema
    console.log('updata:',this.updateData);
    
    if(this.updateData == undefined){
      this.router.navigate(['/restaurants/view-cinema'])
    }
   }

  ngOnInit() {
    this.cinemaInfo = this.fb.group({
      displayName: [''],
      legalName: [''],
      Allscreens: this.fb.array([])
      // series_array: this.fb.array([])
    })

    this.patchValue2()
  }

   // *** Screen as FormArray

   screen(): FormArray {
    return this.cinemaInfo.get("Allscreens") as FormArray
  }

// **** new Single Screen Formgroup

  newScreen(): FormGroup{
    return this.fb.group({
      screenNo: [''],
      screenName: [''],
      seatingCapacity: [''],
      livecount: [''],
      collections: [],
      generate: [],
      series_array: this.fb.array([])
    })
  }

  // *** ADD new Screen
  addScreen(){
    this.screen().push(this.newScreen())

   
    
  }

  // **** Remove screen
  removeScreen(si){
    this.screen().removeAt(si)
    console.log("Screen Array Object", this.cinemaInfo.value)
  }


  // *** Series formArray
  series(si): FormArray{
    return this.screen().at(si).get("series_array") as FormArray
  }

  // Series as formGroup
  newSeries(): FormGroup{
    return this.fb.group({
     
      seriesNo: [''],
      seriesType: [''],
      from1:[''],
      to1:[''],
      fixed1:[''],
      fixed2:[''],
      from2:[''],
      to2:[''],
      readOnly: []
    })


    
  }


  // ***first time add series
  firstaddSeries(si, screen){
    let countValue = screen.controls.livecount.value
    if(countValue >0){
      
      this.series(si).push(this.newSeries())
    }
    else{
      Swal.fire('Seating Capacity is Invalid','','warning')
    }

     
  } 

// ***add series
  addSeries(si, screen,ss){
    let countValue = screen.controls.livecount.value
    let from1= ss.controls.from1.value
    let to1= ss.controls.to1.value
    let fixed1= ss.controls.fixed1.value
    let fixed2= ss.controls.fixed2.value
    let from2= ss.controls.from2.value
    let to2= ss.controls.to2.value
    let seriesType = ss.controls.seriesType.value
    let collections = screen.controls.collections.value

    // *****************************************Sample for all series type*********************
    if(seriesType == 'FIXED + SERIES'){
      let subtract = to1 - from1    
      subtract = subtract + 1
      console.log('substract value',subtract);
      
      for(let i =1; i<=subtract;i++){
        let figure = fixed1+i
        collections.push(figure)
      }
      console.log('collections', collections);
      screen.controls.collections.setValue(collections)
      console.log('sCREEN.controls.collections', screen.controls.collections.value);

      
      let lenColl = collections.length
      if(lenColl > countValue){
        Swal.fire('Series Count Out of Limit','','warning')
        screen.controls.collections.setValue([])
      }
      else {
        countValue = countValue - lenColl
        if(countValue >0){

          this.series(si).push(this.newSeries())
        }
        else{
          Swal.fire('Series Completed','','success')
        }

        ss.controls.readOnly.setValue(true)
        ss.controls['seriesType'].disable()
      }
      
    }
    // *****************************************closing Sample for all series type*********************
    if(seriesType == 'SERIES + FIXED'){
      let subtract = to1 - from1    
      subtract = subtract + 1
      console.log('substract value',subtract);
      
      for(let i =1; i<=subtract;i++){
        let figure = i+fixed1
        collections.push(figure)
      }
      console.log('collections', collections);
      screen.controls.collections.setValue(collections)
      console.log('sCREEN.controls.collections', screen.controls.collections.value);
      
    }
    if(seriesType == 'SERIES + FIXED + SERIES'){}
    if(seriesType == 'FIXED + SERIES + FIXED'){}
    
   

     
  } 

  // *** remove series
  removeSeries(si,ssi){
    this.series(si).removeAt(ssi);

    if(this.series(si).length == 0){
      
    }
  }

// **proceed

onProceed(){
  console.log("Screen Array Object", this.cinemaInfo.value)

 

}

// setButton Disable

setDisable(screen){

  let countValue = screen.controls.livecount.value
    if(countValue >0){
      screen.controls.generate.setValue(true)
    }

}


seriesType(series,si){
  this.type_of_series = series.controls.seriesType.value

  series.controls.from1.reset()
  series.controls.to1.reset()
  series.controls.fixed1.reset()
  series.controls.fixed2.reset()
  series.controls.from2.reset()
  series.controls.to2.reset()
  
  console.log('series length', this.series(si).length);

  let len = this.series(si).length

  for(let i=0; i< len;i++){
    // this.series(si).clear

    console.log('count', i);
    
  }

 console.log(this.cinemaInfo.value);
 
  
}


// ********SET READONLY to each series included inside each screen
setReadOnly(ss){
  // console.log('each ss', ss.controls);
  // ss.controls.readOnly.setValue(true)
  // ss.controls['seriesType'].disable()
}


// ****Setting Enable Generate button at top of each screen
setEnable(screen,si){
  if(this.series(si).length==0  ){
    screen.controls.generate.setValue(false)
  }
 
}

// *********setting live count for calculation
setLiveCount(screen){
  screen.controls.livecount.setValue(parseInt(screen.controls.seatingCapacity.value))
  console.log('live count', screen.controls.livecount.value);
  
}


// ***Final save cinema
onSaveCinema(){

  let obj = {
   
      display_name: this.cinemaInfo.controls.displayName.value,
      legal_name: this.cinemaInfo.controls.legalName.value,
      screen_series: this.cinemaInfo.controls.Allscreens.value
    
  }
  this.restServ.addCinema(obj).subscribe(res=>{
    console.log('response',res);
    if(res['success']){
      Swal.fire('Cinema Added Successfully','','success')
      this.router.navigate(['restaurants/cinema-home'])
    }
    else{
      Swal.fire('Failed to Add Cinema','','error')
    }
    
  })
}



patchValue2(){

 var cinedata = {screen_series : this.updateData.data.screen_series}
console.log("cinedata:---", cinedata)

  console.log('display name this.ud',this.updateData.data.display_name)
  this.cinemaInfo.patchValue({
    displayName: this.updateData.data.display_name,
    legalName: this.updateData.data.legal_name
  })
  console.log('cinedata', cinedata);

  this.clearFormArray(); 

  cinedata.screen_series.forEach(element => {
    
    var screen1 = this.newScreen()
    this.screen().push(screen1)



    element.series_array.forEach(ser => {
      
      var series1  = this.newSeries();
        (screen1.get("series_array") as FormArray).push(series1)
      

    });
  });

  // this.cinemaInfo.patchValue(cinedata)
  this.cinemaInfo.patchValue(this.updateData.data.screen_series)
  console.log("cinema Info after patch",this.cinemaInfo.value);

  
}


clearFormArray(){
 this.screen().clear()
}



}
