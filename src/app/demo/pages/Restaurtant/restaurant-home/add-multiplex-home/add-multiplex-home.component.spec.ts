import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMultiplexHomeComponent } from './add-multiplex-home.component';

describe('AddMultiplexHomeComponent', () => {
  let component: AddMultiplexHomeComponent;
  let fixture: ComponentFixture<AddMultiplexHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMultiplexHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMultiplexHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
