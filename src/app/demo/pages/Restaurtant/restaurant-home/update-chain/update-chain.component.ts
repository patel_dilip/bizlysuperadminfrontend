import { Component, OnInit } from '@angular/core';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { NgxSpinnerService } from 'ngx-spinner';
import { element } from 'protractor';
import Swal from 'sweetalert2';

// Logo image file uploader URL
const LogoImg = environment.base_Url + "restaurant/uploadrestaurantlogo";
const ImagesAlbum = environment.base_Url + "restaurant/uploadrestoalbumimages"
const VideoAlbum = environment.base_Url + "restaurant/uploadrestoalbumvideos"

@Component({
  selector: 'app-update-chain',
  templateUrl: './update-chain.component.html',
  styleUrls: ['./update-chain.component.scss']
})
export class UpdateChainComponent implements OnInit {

  // chain info stepper variables
  chainInfo: FormGroup
  selected = new FormControl(0);

   // Establishment Variables

   moreInfoArr: any = []  
   servicesArr: any = []
   rulesArr: any = []
   ambienceArr: any = []
   cuisinesArr: any = []
 
   // Selected array of estalbisment
   selectedmore: any= []
   selectedservices: any= []
   selectedrules: any= []
   selectedambiences: any= []
   selectedcuisines: any= []

  editData: any
  chaindb: any = []
  estdb: any = []
  mediadb: any = []
  chainId: any

  // FILE UPLOADER VARIABLES
public logoUploader: FileUploader = new FileUploader({ url: LogoImg, itemAlias: 'restologo' });
public ImgAlbumUploader: FileUploader = new FileUploader({ url: ImagesAlbum, itemAlias: 'restoalbumimages' });
public VideoAlbumUploader: FileUploader = new FileUploader({ url: VideoAlbum, itemAlias: 'restoalbumvideos' });

logoResponse= ''
showLogo= false

VideoAlbumResponse=[]
showVideos = false

ImgAlbumResponse=[]
showImages = false
  constructor(
    private restServ: RestoService,
    private router: Router,
    private fb: FormBuilder,
    private ngxSpinner: NgxSpinnerService
  ) { 

    this.editData = this.restServ.chainEdit
    if(this.editData==undefined){
      this.router.navigate(['/restaurants/chain-home'])
    }

    
    this.chainId = this.editData.data._id
    console.log(this.editData.type)
    this.chaindb = this.editData.data.chain_info
    this.estdb = this.editData.data.establishment_info
    this.mediadb = this.editData.data.media_info
    console.log(this.chainId)
    // console.log(this.editData.data.chain_info)
    // this.editData = this.editData.data.chain_info
    // ********************************chain info*********************
    
        // MORE INFO ARRAY

        this.moreInfoArr = [
          {'id': '1', 'name': 'Events'},
          {'id': '2', 'name': 'Hospital'},
          {'id': '3', 'name': 'Studios'},
          {'id': '4', 'name': 'SPA'},
          {'id': '5', 'name': 'Nursery'},
          {'id': '6', 'name': 'Cloak Room'},
        ]
    
        // SERVICES INFO ARRAY
    
        this.servicesArr = [
          {'id': '1', 'name': 'Spa'},
          {'id': '2', 'name': 'Massages'},
          {'id': '3', 'name': 'Home Delievery'},
          {'id': '4', 'name': 'Take Away'},
          {'id': '5', 'name': 'Dine-In'}      
        ]
    
        // RULES & REGULATIONS ARRAY
    
        this.rulesArr = [
          {'id': '1', 'name': 'No Smoking'},
          {'id': '2', 'name': 'No Outside Eatables'},
          {'id': '3', 'name': 'No Pets'},
          {'id': '4', 'name': 'No Parking'},
          {'id': '5', 'name': 'BYOB'},
          {'id': '6', 'name': 'Family Friendly'},
          {'id': '7', 'name': 'Couple Friedndly'}    
    
        ]
    
        // AMBIENCES ARRAY
    
        this.ambienceArr = [
          {'id': '1', 'name': 'Pool Side'},
          {'id': '2', 'name': 'Dance Floor'},
          {'id': '3', 'name': 'Lounge'},
          {'id': '4', 'name': 'Lawn Area'},
          {'id': '5', 'name': 'Outdoors'},
          {'id': '6', 'name': 'Rooftops'},
        ]
    
        // CUISINES ARRAY
    
        this.cuisinesArr = [
          {'id': '1', 'name': 'BBQ'},
          {'id': '2', 'name': 'Continental'},
          {'id': '3', 'name': 'Chinese'},
          {'id': '4', 'name': 'Indian'},
          {'id': '5', 'name': 'Italian'},
          {'id': '6', 'name': 'Punjabi'},
          {'id': '7', 'name': 'South Indian'},
        ]





  }

  ngOnInit() {

       // ************Chain Info*****************
       this.chainInfo = this.fb.group({
         chain_code: [''],
        display_name: [''],
        legal_name: [''],
        chain_logo: [''],
        chain_pictures: [[]],
        chain_videos: [[]],
        outlets_tree: [''],
        establish_moreinfo: [''],
        establish_services: [''],
        establish_rules: [''],
        establish_ambience: [''],
        establish_cuisines: [''],
      })

      // P_____PATCH CHAIN
      this.patchChainInfo()

      // __________PATCH ESTABLISHMENT
      this.patchEst()

      // ___________PATCH MEDIA
      this.patchMedia()
    // LOGO UPLOADS

    this.logoUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      this.ngxSpinner.hide()
      console.log('FileUpload:uploaded:', item, status, response);
      if(status==200){
        this.showLogo=true
      }
      this.logoResponse = response
      console.log('response', this.logoResponse)

      this.chainInfo.controls.chain_logo.setValue(this.logoResponse)
      console.log(this.chainInfo.value);
    }


    // Images Uploads

    this.ImgAlbumUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
     
     
      console.log('FileUpload:uploaded:', item, status, response);
      this.ImgAlbumResponse.push(response)
     this.ngxSpinner.show()
      console.log('Images Response', this.ImgAlbumResponse);
      this.showImages = true
     this.ngxSpinner.hide()
     this.chainInfo.controls.chain_pictures.setValue(this.ImgAlbumResponse)
     console.log(this.chainInfo.value);
     
    }
   
    // chain_logo: [''],
    //   chain_pictures: [''],
    //   chain_videos: [''],
    //Videos uploads

    this.VideoAlbumUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // this.VideoAlbumResponse = []
      console.log('FileUpload:uploaded:', item, status, response);
     
      this.VideoAlbumResponse.push(response)
      
      console.log(' Video Response', this.VideoAlbumResponse);
      this.showVideos = true
      this.ngxSpinner.hide();

      this.chainInfo.controls.chain_videos.setValue(this.VideoAlbumResponse)
      console.log(this.chainInfo.value);
    }
   
  }

   // loading spinner
   loading(){
    this.ngxSpinner.show()
  }

  // Next Tab
  nextTab(item){   
    this.selected.setValue(parseInt(item))   
  }


    // **REMOVE ESTABLISMENT CHIPS

    removeMoreinfo(moreinfo) {

      this.moreInfoArr.push(moreinfo)
      console.log('moreInfoArr', this.moreInfoArr)
  
      this.selectedmore.splice(this.selectedmore.indexOf(moreinfo),1)
      console.log('selected more info', this.selectedmore)
  
      this.chainInfo.controls.establish_moreinfo.setValue(this.selectedmore)
      console.log(this.chainInfo.value)
    }
  
    removeServices(service) {
    
      this.servicesArr.push(service)
      console.log(' servicesArr array', this.servicesArr)
  
      this.selectedservices.splice(this.selectedservices.indexOf(service),1)
      console.log('selected selectedservices', this.selectedservices)
  
      this.chainInfo.controls.establish_services.setValue(this.selectedservices)
      console.log(this.chainInfo.value)
    }
    removeRules(rule) {
  
      this.rulesArr.push(rule)
      console.log(' rulesArr info array', this.rulesArr)
  
      this.selectedrules.splice(this.selectedrules.indexOf(rule),1)
      console.log('selected selected rule', this.selectedrules)
  
      this.chainInfo.controls.establish_rules.setValue(this.selectedrules)
      console.log(this.chainInfo.value)
  
    }
  
    removeAmbience(ambience) {
    
      this.ambienceArr.push(ambience)
      console.log(' ambienceArr array', this.ambienceArr)
  
  
      this.selectedambiences.splice(this.selectedambiences.indexOf(ambience),1)
      console.log('selected selectedambiences', this.selectedambiences)
  
      this.chainInfo.controls.establish_ambience.setValue(this.selectedambiences)
      console.log(this.chainInfo.value)
    }
  
    removeCuisine(cuisine) {
  
      this.cuisinesArr.push(cuisine)
      console.log(' cuisinesArr array', this.cuisinesArr)
  
  
      this.selectedcuisines.splice(this.selectedcuisines.indexOf(cuisine),1)
      console.log('selected selectedcuisines', this.selectedcuisines)
  
      this.chainInfo.controls.establish_cuisines.setValue(this.selectedcuisines)
      console.log(this.chainInfo.value)
  
    }

      // SELECTED ESTABLISHMENT CHIPS
 
  optMoreInfo(moreinfo){
    this.selectedmore.push(moreinfo)
    console.log('selected more info', this.selectedmore)

    this.moreInfoArr.splice(this.moreInfoArr.indexOf(moreinfo),1)
    console.log(' more info array', this.moreInfoArr)

    this.chainInfo.controls.establish_moreinfo.setValue(this.selectedmore)
    console.log(this.chainInfo.value)
  }

  optserviceInfo(service){
    this.selectedservices.push(service)
    console.log('selected selectedservices', this.selectedservices)

    this.servicesArr.splice(this.servicesArr.indexOf(service),1)
    console.log(' servicesArr array', this.servicesArr)

    this.chainInfo.controls.establish_services.setValue(this.selectedservices)
    console.log(this.chainInfo.value)
  }

  optRules(rule){
    this.selectedrules.push(rule)
    console.log('selected selected rule', this.selectedrules)

    this.rulesArr.splice(this.rulesArr.indexOf(rule),1)
    console.log(' rulesArr info array', this.rulesArr)

    this.chainInfo.controls.establish_rules.setValue(this.selectedrules)
    console.log(this.chainInfo.value)
  }

  optAmbience(ambience){
    this.selectedambiences.push(ambience)
    console.log('selected selectedambiences', this.selectedambiences)

    this.ambienceArr.splice(this.ambienceArr.indexOf(ambience),1)
    console.log(' ambienceArr array', this.ambienceArr)

    this.chainInfo.controls.establish_ambience.setValue(this.selectedambiences)
    console.log(this.chainInfo.value)
  }

  optCuisine(cuisine){
    this.selectedcuisines.push(cuisine)
    console.log('selected selectedcuisines', this.selectedcuisines)

    this.cuisinesArr.splice(this.cuisinesArr.indexOf(cuisine),1)
    console.log(' cuisinesArr array', this.cuisinesArr)

    this.chainInfo.controls.establish_cuisines.setValue(this.selectedcuisines)
    console.log(this.chainInfo.value)
  }


  // ****************************chain info*****************
  patchChainInfo(){

    // chaindb
    this.chainInfo.patchValue({
      display_name: this.chaindb.display_name,
      legal_name: this.chaindb.legal_name
    })
  }


  updateChainInfo(){

    var chainInfoObj = {
      // chain_code : 'CHN001',
      display_name : this.chainInfo.controls.display_name.value,
      legal_name : this.chainInfo.controls.legal_name.value
    }

    this.restServ.updateChaininfo(this.chainId, chainInfoObj).subscribe(res=>{
      console.log('response',res);
      
    })
  }
  
  // *******************Patch Est info*******************
  patchEst(){
    this.selectedmore= this.estdb.establish_moreinfo
   this.selectedservices= this.estdb.establish_services
   this.selectedrules= this.estdb.establish_rules
   this.selectedambiences= this.estdb.establish_ambience
   this.selectedcuisines= this.estdb.establish_cuisines


  //  SPLICE MORE INFO DATA

  this.selectedmore.forEach(element => {
    this.moreInfoArr.forEach(ele => {
      if(ele.name == element.name){
        this.moreInfoArr.splice(this.moreInfoArr.indexOf(ele),1)
      }
      
    });
  });
   

  // SPLICE SERVICES DATA

  this.selectedservices.forEach(element => {
    this.servicesArr.forEach(ele => {
      if(element.name == ele.name){
        this.servicesArr.splice(this.servicesArr.indexOf(ele),1)
      }
    });
  });
  // SPLICE RULES DATA
  this.selectedrules.forEach(element =>{
    this.rulesArr.forEach(ele => {
      if(element.name===ele.name){
        this.rulesArr.splice(this.rulesArr.indexOf(ele),1)
      }
    });
  })
  // SPLICE AMBIENCE DATA
  this.selectedambiences.forEach(element => {
    this.ambienceArr.forEach(ele => {
      if(element.name === ele.name){
        this.ambienceArr.splice(this.ambienceArr.indexOf(ele),1)
      }
    });
  });
  // SPLICE CUISINES DATA
  this.selectedcuisines.forEach(element => {
    this.cuisinesArr.forEach(ele => {
      if(ele.name == element.name){
        this.cuisinesArr.splice(this.cuisinesArr.indexOf(ele),1)
      }
    });
  });

  }

// ***********************UPDATE chain establisment INFO***********************
onUpdateMoreInfo(){
  console.log('chian info', this.chainInfo.value);
 
   var moreinfo= this.chainInfo.controls.establish_moreinfo.value
    
 
  this.restServ.updateChainMoreinfo(this.chainId, moreinfo).subscribe(res=>{
    console.log(res);

    if(res['success']){
      Swal.fire('Updated Successfully','','success')
    }
    else{
      Swal.fire('Failed to Update','','warning')

    }
    
  })
}
 
onUpdateServices(){
  var services= this.chainInfo.controls.establish_services.value

  this.restServ.updateChainServices(this.chainId, services).subscribe(res=>{
    console.log(res);

    if(res['success']){
      Swal.fire('Updated Successfully','','success')
    }
    else{
      Swal.fire('Failed to Update','','warning')

    }
    
  })
}

onUpdateRules(){
  var rules= this.chainInfo.controls.establish_rules.value

  this.restServ.updateChainRules(this.chainId, rules).subscribe(res=>{
    console.log(res);

    if(res['success']){
      Swal.fire('Updated Successfully','','success')
    }
    else{
      Swal.fire('Failed to Update','','warning')

    }
    
  })
}

onUpdateAmbiences(){
    var ambiences = this.chainInfo.controls.establish_ambience.value
   

    this.restServ.updateChainAmbiences(this.chainId, ambiences).subscribe(res=>{
      console.log(res);

      if(res['success']){
        Swal.fire('Updated Successfully','','success')
      }
      else{
        Swal.fire('Failed to Update','','warning')
  
      }
      
    })

}

onUpdateCuisines(){
  var cuisines= this.chainInfo.controls.establish_cuisines.value
  this.restServ.updateChainCuisines(this.chainId, cuisines).subscribe(res=>{
    console.log(res);

    if(res['success']){
      Swal.fire('Updated Successfully','','success')
      this.router.navigate(['/restaurants/view-update-chain'])
    }
    else{
      Swal.fire('Failed to Update','','warning')

    }
    
  })

}
  

// ******************************CHAIN MEDIA FUNCITONS***********************


patchMedia(){

  this.logoResponse = this.mediadb.chain_logo
  this.showLogo = true
  this.ImgAlbumResponse = this.mediadb.chain_pictures
  this.showImages = true
  this.VideoAlbumResponse = this.mediadb.chain_videos
  this.showVideos = true

}

// Edit Logo
onEditLogo(){
  this.logoResponse = ''
  this.showLogo = false
}

// EDIT IMAGES
onEditImages(){
  this.ImgAlbumResponse = []
  this.showImages = false
}
// EDIT VIDEOS
onEditVideos(){
  this.VideoAlbumResponse =[]
  this.showVideos = false
}


// UPDATE MEDIA 
onUpdateMedia(){
console.log('media form value',this.chainInfo.value);

var mediaObj ={
  chain_logo:  this.logoResponse,
  chain_pictures: this.ImgAlbumResponse,
  chain_videos: this.VideoAlbumResponse
}

this.restServ.updateMediaChain(this.chainId, mediaObj).subscribe(res=>{
  if(res['success']){
    Swal.fire('Media Updated Successfully','','success')
    this.router.navigate(['/restaurants/view-update-chain'])
  }else{
    Swal.fire('Failed to Update','','warning')
  }
})

}


}
