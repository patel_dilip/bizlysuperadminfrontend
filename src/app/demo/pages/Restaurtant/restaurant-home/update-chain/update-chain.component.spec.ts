import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateChainComponent } from './update-chain.component';

describe('UpdateChainComponent', () => {
  let component: UpdateChainComponent;
  let fixture: ComponentFixture<UpdateChainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateChainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateChainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
