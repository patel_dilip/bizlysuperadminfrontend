import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutletHomeComponent } from './outlet-home.component';

describe('OutletHomeComponent', () => {
  let component: OutletHomeComponent;
  let fixture: ComponentFixture<OutletHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutletHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutletHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
