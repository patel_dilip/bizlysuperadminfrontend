import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-outlet-home',
  templateUrl: './outlet-home.component.html',
  styleUrls: ['./outlet-home.component.scss']
})
export class OutletHomeComponent implements OnInit {

organizationForm: FormGroup

  AllOrg: any = [];
  options: any = []
  filteredOptions: Observable<string[]>;

  isReadOnly: boolean= false
  existOrg: any;

  constructor(private fb: FormBuilder,
              private router: Router,
              private restoServ: RestoService) { 

    this.organizationForm = this.fb.group({
      organization_name: ['', Validators.required],
      representative_name:['', Validators.required],
      org_email: ['', Validators.required],
      org_contact: ['', Validators.required],
      outlets_arr: []
    })

  


    // Get All Organizations
    this.restoServ.getAllOrg().subscribe(data=>{
      console.log('get all organization',data['data']);
      this.AllOrg = data['data']

      this.AllOrg.forEach(element => {
        this.options.push(element.organization_name)
        // console.log('each org naem', element.organization_name);
        
      });
    })
  }

  ngOnInit() {
    this.filteredOptions = this.organizationForm.controls.organization_name.valueChanges
    .pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): string[] {

    if(value != undefined){
      const filterValue = value.toLowerCase();

      return this.options.filter(option => option.toLowerCase().includes(filterValue));
    }
   
  }

  onSaveOrg(){
    console.log(this.organizationForm.value)

  
    this.restoServ.addOrganization(this.organizationForm.value).subscribe(res=>{
      console.log('response',res);

      if(res['success']){
        this.router.navigate(['/restaurants/list-outlet'])
        Swal.fire('New Organizationd Added','Successfully','success')
      }
      else{
        Swal.fire('Something Went Wrong','','warning')
      }
      
    })


     }



  

// SELECTED ORGANIZATIONS
    onSelectedOrg(item){
      console.log('Selected Items', item);
      this.isReadOnly = true
      this.AllOrg.forEach(element => {
        

        if(item == element.organization_name){
          console.log('found', element);

          this.existOrg = element
          this.organizationForm.patchValue({
            representative_name: element.representative_name,
            org_email: element.org_email,
            org_contact: element.org_contact,

          })
         
        }
      });
    }

    // On Change 
    onChange(){
      this.organizationForm.reset()
      this.isReadOnly = false
    }


    // ON Proceed

    // onProceed(){
    
    //   console.log('existed org', this.existOrg);
    //   this.restoServ.existOrganization = this.existOrg
    //   console.log('existed org',  this.restoServ.existOrganization);
    //   this.router.navigate(['/restaurants/list-outlet'])
    // }

    // ON DELETE
    onDeleteOrg(){
      let delid = this.existOrg._id
      this.restoServ.deleteOrg(delid).subscribe(res=>{
        console.log('response after delete org',res);
        if(res['success']){
          this.organizationForm.reset()
          Swal.fire('Delete Successfully')
        }
      })
    }
}
