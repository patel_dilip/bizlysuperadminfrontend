import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';


@Component({
  selector: 'app-cinema-cell-render',
  templateUrl: './cinema-cell-render.component.html',
  styleUrls: ['./cinema-cell-render.component.scss']
})
export class CinemaCellRenderComponent implements OnInit {

  title: any
  data: any
  constructor(
    private router: Router,
    private restServ: RestoService
  ) { }
  
  agInit(params){

    this.title = params.value
    this.data = params.data

  }
  ngOnInit() {
  }


  onCinmea(){
    console.log('data', this.data);
    this.restServ.view_cinema = this.data
    this.router.navigate(['/restaurants/view-cinema'])    
  }

}
