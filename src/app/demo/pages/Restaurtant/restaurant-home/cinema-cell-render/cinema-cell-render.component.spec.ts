import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CinemaCellRenderComponent } from './cinema-cell-render.component';

describe('CinemaCellRenderComponent', () => {
  let component: CinemaCellRenderComponent;
  let fixture: ComponentFixture<CinemaCellRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CinemaCellRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CinemaCellRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
