import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';

@Component({
  selector: 'app-chain-cell-render',
  templateUrl: './chain-cell-render.component.html',
  styleUrls: ['./chain-cell-render.component.scss']
})
export class ChainCellRenderComponent implements OnInit {

  title: any;
  data: any
  constructor(
            private router: Router,
            private restoServ: RestoService
             ) { }

  agInit(params){
    this.title = params.value
    this.data = params.data
  }
  ngOnInit() {
  }


  view(){
    console.log("view click data",this.data);
    this.restoServ.view_update = this.data
    this.router.navigate(['restaurants/view-update-chain'])
  }

}
