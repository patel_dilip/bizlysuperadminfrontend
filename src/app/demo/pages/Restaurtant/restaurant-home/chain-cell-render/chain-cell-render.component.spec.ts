import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChainCellRenderComponent } from './chain-cell-render.component';

describe('ChainCellRenderComponent', () => {
  let component: ChainCellRenderComponent;
  let fixture: ComponentFixture<ChainCellRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChainCellRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChainCellRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
