import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ColSetComponent } from '../CinemaPopUps/col-set/col-set.component';
import { AllModules, Module } from '@ag-grid-enterprise/all-modules';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { HttpClient } from '@angular/common/http';
import { CinemaCellRenderComponent } from '../cinema-cell-render/cinema-cell-render.component';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cinema-home',
  templateUrl: './cinema-home.component.html',
  styleUrls: ['./cinema-home.component.scss']
})
export class CinemaHomeComponent implements OnInit {


  searchValue: any
  public modules: Module[] = AllModules;
  chkList: any = [];
  showList: any = [];
  hideList: any = [];
  displaychk: boolean = true;

  // ngPrime Table
  cols: any = [];
  outletTable: any = [];
  _selectedColumns: any[];
  selected1: any;

  // table Variables
  AllOrg: any = [];
  loopdata: any = []
  options: any = []
  existOrg: any
  orgcode: string;
  onlyRestoInfo: any = [];
  onlyEstablish: any = [];
  onlyLegal: any = [];
  onlyMedia: any = [];
  onlyMenu: any = [];
  organization: any = [];



  constructor(private matDialog: MatDialog, private restServ: RestoService, private router: Router) {

    this.restServ.getAllCinema().subscribe(data => {
      console.log("All Cinemas", data['data']);

    })


      // Get All Organizations
      this.restServ.getAllOrg().subscribe(data => {
        console.log('get all organization', data['data']);
       
        this.AllOrg = data['data']
  
     
        
        // **********creating code serially
  
      if(this.AllOrg.length == 0){
        let len = this.AllOrg.length + 1   //length if array is empty 0+1
        let str = len.toString()            // converting 1 to string '1'
        let size = 3                        // size of code
        this.orgcode = 'ORG' + str.padStart(size, '0')  // padding with 00 to left ex: 001
        console.log('org code', this.orgcode);
        
      }
      else{
  
        console.log("all org lenth",this.AllOrg.length);
        let index = this.AllOrg.length - 1
  
        let last_code = this.AllOrg[index].organizationCode
        console.log('last_code', last_code);
        let str = last_code.slice(3)
        console.log('string', str);
        let digit = parseInt(str)
        digit = digit + 1
        let str1 = digit.toString()
        let size = 3
        this.orgcode = 'ORG' + str1.padStart(size, '0')
        console.log('role code', this.orgcode);
  
  
      }
  
  
  
       
  
        if(this.AllOrg !== undefined){
          this.AllOrg.forEach(element => {
            this.options.push(element.organization_name)
            console.log('each org naem', element.organization_name);
    
          });
        }
       
      })

        // Get ALL Restaurant API Call
    this.restServ.getAllRestaurant().subscribe(data => {

      console.log('loopdata data', data['data']);
      
       this.loopdata = data['data']
 
      if(this.loopdata !=='No restaurants available'){
       this.loopdata.forEach(element => {
         this.onlyRestoInfo.push(element.restoInfo)
         this.onlyEstablish.push(element.establishmentType)
         this.onlyLegal.push(element.legalInfo)
         this.onlyMedia.push(element.media)
         this.onlyMenu.push(element.menuManagement)
       });
       console.log('all data', this.loopdata)
       console.log('only resto info', this.onlyRestoInfo);
       console.log('only establish', this.onlyEstablish);
       console.log('only legal', this.onlyLegal);
       console.log('only media', this.onlyMedia);
       console.log('only menu', this.onlyMenu);
 
 
         // concat all arrays
 
 
   
    
      }
       
 
       // this.rowData = this.onlyRestoInfo
       this.tableData()
 
 
     })
  }

  ngOnInit() {

  }

  // tableData Ng prime
  tableData() {
   
    console.log("dataTable loop data", this.onlyRestoInfo )
           //my code prme ng data

           let outletTable = []
            let orgName;
            let orgCode;
           this.loopdata.forEach(element => {

            this.AllOrg.forEach(ele => {
              ele.outlets_arr.forEach(outlet => {
                if(outlet._id == element._id){
                  orgName = ele.organization_name
                  orgCode = ele.organizationCode
                }
              });
            });

            let Gst
            let Pan
            let Fssai
            let cuisines = []
            //GST PAN FSSAI
            if(element.legalInfo.documents){
              element.legalInfo.documents.forEach(docit => {
                if(docit.documentName=='GST'){
                   Gst = docit.documentNumber
                }
    
                if(docit.documentName=='PAN Card'){
                   Pan = docit.documentNumber
                }
    
                if(docit.documentName=='FSSAI Certificate'){
                   Fssai = docit.documentNumber
                }
             });
            }

            // Cuisines
            if(element.menuManagement.cuisines){
              element.menuManagement.cuisines.forEach(cui => {
                cuisines.push(cui)
              });
            }
      


           outletTable.push(
             {
              name: element.restoInfo.restoName,
              locality: element.restoInfo.restoLocality,
              contact: element.restoInfo.restoCountryCode +'- '+ element.restoInfo.restoContactNo,
              pincode: element.restoInfo.restoPincode,             
              infrasture: element.outletInfo.infrastructure_type,
              pricefortwo: element.restoInfo.restoPriceForTwo,
              type: element.restoInfo.restoType,
              cuisines: cuisines,
              address: element.restoInfo.restoAddress,
              website: element.restoInfo.restoWebsite,
              organization: orgName,
              orgCode: orgCode,
              dateOnBoardQrServ: 'NA',
              product:'NA',
              chainid: element.outletInfo.chain_name,
              account_no: element.legalInfo.accountNo,
              ifsc_code: element.legalInfo.IFSCCode,
              account_type: element.legalInfo.accountType,
              gst: Gst,
              pan: Pan,
              fssai: Fssai,
              average_cart_value: 0,
              orders_month: 0,
              orders_weekend: 0,
              orders_weekday: 0,
              screen_Apvr:0,
              screen_Bpvr: 0,
              screen_Cpvr:0,
              screen_Dpvr:0,
              orders_pvr:0,
              screen_Acity:0,
              screen_Bcity: 0,
              screen_Ccity:0,
              screen_Dcity:0,
              orders_city:0

             }
           ) 


         
           });
           this.outletTable = outletTable
           console.log("Main data table", this.outletTable)
          this.cols = [
            { field: "orgCode", header: "Organization Code" },
            { field: "name", header: "Outlet Name" },
            { field: "organization", header: "Organization Name" },         
            { field: "locality", header: "Locality" },
            { field: "pincode", header: "Pincode" },
            { field: "contact", header: "Contact" },
            { field: "infrasture", header: "Infrastructure" },
            { field: "pricefortwo", header: "Price For 2" },
            { field: "type", header: "Restaurant Type" },
            { field: "cuisines", header: "Cuisines" },
            { field: "address", header: "Address" },
            { field: "website", header: "Restaurant Website" },
            { field: "dateOnBoardQrServ", header: "Date of Onboarding QR Service" },
            { field: "product", header: "Product" },
            { field: "chainid", header: "Chain Id" },
            { field: "account_no", header: "Account Number" },
            { field: "ifsc_code", header: "IFSC Code" },
            { field: "account_type", header: "Account Type" },
            { field: "gst", header: "GST Number" },
            { field: "pan", header: "PAN" },
            { field: "fssai", header: "FSSAI Certificate" },
            { field: "average_cart_value", header: "Average Cart Value" },
            { field: "orders_month", header: "Orders/ Month" },
            { field: "orders_weekend", header: "Orders/ Weekend" },
            { field: "orders_weekday", header: "Orders/ Weeday" },
            { field: "screen_Apvr", header: "Screen A" },
            { field: "screen_Bpvr", header: "Screen B" },
            { field: "screen_Cpvr", header: "Screen C" },
            { field: "screen_Dpvr", header: "Screen D" },
            { field: "orders_pvr", header: "Orders/ PVR Cinemas" },
            { field: "screen_Acity", header: "Screen A" },
            { field: "screen_Bcity", header: "Screen B" },
            { field: "screen_Ccity", header: "Screen C" },
            { field: "screen_Dcity", header: "Screen D" },
            { field: "orders_city", header: "Orders/ City Pride" }
          ];

          this._selectedColumns = this.cols;

          console.log("Selected Columns : ",this._selectedColumns);
          console.log("Json : ", this.outletTable);
  }

  // onView cinema
  onCinmea(id) {

   

    this.router.navigate(['/restaurants/view-cinema'])
  }


  // ON CHECKBOX CLICK
  onChk() {
    console.log("selected1", this.selected1);
  }

  // show hide columns
  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col) => val.includes(col));

    console.log("Set value of selected columns : ", this._selectedColumns);
  }

  

  columnSet() {

    this.restServ.cinemaCol = this.chkList
    const dialogRef = this.matDialog.open(ColSetComponent, {
      width: '580px',
      height: '400px',
      data: this.chkList

    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.showList = []
      this.hideList = []
      result.forEach(element => {

        if (element.status) {
          this.showList.push(element.key)
        } else {
          this.hideList.push(element.key)
        }
      });

    
    })


  }

 


  // ********Check list events*********
  onCheckCol(item, event) {


    if (event.checked) {
      item.status = true
    }
    else {
      item.status = false
    }


    console.log('chkList', item);

    if (item.status) {
      this.showList.push(item.key)
      this.hideList.splice(this.hideList.indexOf(item.key), 1)
    } else {
      this.hideList.push(item.key)
      this.showList.splice(this.showList.indexOf(item.key), 1)
      // this.displaychk=false

    }


    console.log('showlist', this.showList);
    console.log('hidlist', this.hideList);


   

    if (this.hideList.length > 0) {
      this.displaychk = false
    } else {
      this.displaychk = true
    }




  }


  displayAll(event) {
    this.showList = []
    this.hideList = []
    if (event.checked) {

      this.chkList.forEach(element => {
        element.status = true

        if (!this.showList.includes(element)) {
          this.showList.push(element.key)
        }


      });
      // this.displaychk=true
      console.log('show list', this.showList);

      this.displaychk = true
    
    }

    else {

      this.chkList.forEach(element => {
        element.status = false

        if (!this.hideList.includes(element)) {
          this.hideList.push(element.key)
        }


      });
      // this.displaychk=true
      console.log('hide list', this.hideList);

      this.displaychk = false
      
    }
  }

  // ************Delete all
  deleteAll() {
    this.restServ.deleteAllCine().subscribe(res => {
      if (res['success']) {
        Swal.fire('Deleted Successfully', '', 'success')

      }
    })
  }

}
