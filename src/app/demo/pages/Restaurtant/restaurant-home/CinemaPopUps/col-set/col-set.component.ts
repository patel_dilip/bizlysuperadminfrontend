import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';

@Component({
  selector: 'app-col-set',
  templateUrl: './col-set.component.html',
  styleUrls: ['./col-set.component.scss']
})
export class ColSetComponent implements OnInit {

  recordObj
  columnDefs = [ ];
  obj: any
  visibles = []
  hiddens = []

  constructor(private restServ: RestoService, @Inject (MAT_DIALOG_DATA) public data ) {
  this.columnDefs = data
  console.log('column', this.columnDefs);
  
   }
  ngOnInit() {
  }


  onSelect(event, item){
    console.log('event', event)
    console.log('item',item)

    if(!event.target.checked){
      item.status = false
    }else{
      item.status = true
    }
   console.log(this.columnDefs);
   
 

 
   
  }

 

}
