import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColSetComponent } from './col-set.component';

describe('ColSetComponent', () => {
  let component: ColSetComponent;
  let fixture: ComponentFixture<ColSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
