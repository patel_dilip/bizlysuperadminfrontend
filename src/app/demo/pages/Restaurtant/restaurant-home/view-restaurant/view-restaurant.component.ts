import { Component, OnInit, AfterViewInit } from '@angular/core';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-view-restaurant',
  templateUrl: './view-restaurant.component.html',
  styleUrls: ['./view-restaurant.component.scss']
})
export class ViewRestaurantComponent implements OnInit {

 view_restaurant: any = []
  current_restoid: any
  getResto: any = []
  constructor(private restServ: RestoService,private router: Router ) { 


    this.current_restoid = this.restServ.current_restoid
  if(this.current_restoid == undefined){
    this.router.navigate(['/restaurants/add-outlet'])
  }
  else{

    this.getRestaurantData();

  
     
  }

 
  }

  ngOnInit() {

    
  }

  getRestaurantData(){
    return new Promise((resolve,reject)=>{
      this.restServ.getSelectedResto(this.current_restoid).subscribe(data=>{
        console.log("current resto", data);
      
        this.getResto = data['data']
        resolve(this.getResto)
      })

    }).then(data=>{
      this.view_restaurant = data

      console.log("view restaurants", this.view_restaurant)
    })
  }


  editResto(type){
    this.restServ.resto_update = {type: type, data: this.view_restaurant}
    this.restServ.current_restoid = this.view_restaurant._id
    this.router.navigate(['/restaurants/update-restaurant'])
  }
 
}
