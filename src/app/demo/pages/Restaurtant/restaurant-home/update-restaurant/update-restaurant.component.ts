import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
// import { map } from 'rxjs/\/operators/map';
import { MainService } from 'src/app/_services/main.service';
import { GenerateWebsiteComponent } from 'src/app/PopoversList/Restaurants-related-popups/generate-website/generate-website.component';
import { MatDialog } from '@angular/material';
import { AddRevenueComponent } from 'src/app/PopoversList/add-revenue/add-revenue.component';
import Swal from 'sweetalert2';
import { AddChipMoreinfoComponent } from 'src/app/PopoversList/Establish-related-popups/add-chip-moreinfo/add-chip-moreinfo.component';
import { AddMoreInfoComponent } from 'src/app/PopoversList/Establish-related-popups/add-more-info/add-more-info.component';
import { AddChipServicesComponent } from 'src/app/PopoversList/Establish-related-popups/add-chip-services/add-chip-services.component';
import { AddServicesChipsComponent } from 'src/app/PopoversList/Establish-related-popups/add-services-chips/add-services-chips.component';
import { AddChipRulesComponent } from 'src/app/PopoversList/Establish-related-popups/add-chip-rules/add-chip-rules.component';
import { AddRulesChipsComponent } from 'src/app/PopoversList/Establish-related-popups/add-rules-chips/add-rules-chips.component';
import { AddChipAmbiencesComponent } from 'src/app/PopoversList/Establish-related-popups/add-chip-ambiences/add-chip-ambiences.component';
import { AddAmbienceChipsComponent } from 'src/app/PopoversList/Establish-related-popups/add-ambience-chips/add-ambience-chips.component';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { NgxSpinnerService } from 'ngx-spinner';
import { CreateVdoAlbumComponent } from 'src/app/PopoversList/Media-related-Popups/create-vdo-album/create-vdo-album.component';
import { CreateAlbumComponent } from 'src/app/PopoversList/create-album/create-album.component';
import { AddCatoNameComponent } from 'src/app/PopoversList/add-cato-name/add-cato-name.component';
import { AddSubCatComponent } from 'src/app/PopoversList/add-sub-cat/add-sub-cat.component';
import { AddSubSubCatComponent } from 'src/app/PopoversList/add-sub-sub-cat/add-sub-sub-cat.component';
import { EstAddCatComponent } from 'src/app/PopoversList/est-add-cat/est-add-cat.component';
import { element } from 'protractor';
import { AddRootMenuComponent } from 'src/app/PopoversList/add-root-menu/add-root-menu.component';
import { AddCateMenuComponent } from 'src/app/PopoversList/add-cate-menu/add-cate-menu.component';
import { AddSubcatMenuComponent } from 'src/app/PopoversList/add-subcat-menu/add-subcat-menu.component';
import { AddCcMenuComponent } from 'src/app/PopoversList/add-cc-menu/add-cc-menu.component';
import { AddSuperChildPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-super-child-pop/add-super-child-pop.component';
import { AddSubVarientPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-sub-varient-pop/add-sub-varient-pop.component';
import { AddVarientPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-varient-pop/add-varient-pop.component';
import { AddRootLiquorPopComponent } from 'src/app/PopoversList/Liquor-pop-up/add-root-liquor-pop/add-root-liquor-pop.component';
// import { ConsoleReporter } from 'jasmine';



// MEDIA UPLOAD URLS
const OtherImg = environment.base_Url + "restaurantLegal/uploadother";
const LogoImg = environment.base_Url + "restaurant/uploadrestaurantlogo";
const BannerImg = environment.base_Url + "restaurant/uploadrestaurantbanner"
const ImagesAlbum = environment.base_Url + "restaurant/uploadrestoalbumimages"
const VideoAlbum = environment.base_Url + "restaurant/uploadrestoalbumvideos"


// IMAGES UPLOAD URLS
const GstImg = environment.base_Url + "restaurantLegal/uploadgst";
const PanImg = environment.base_Url + "restaurantLegal/uploadpan";
const FssaiImg = environment.base_Url + "restaurantLegal/uploadfssai";
const NocImg = environment.base_Url + "restaurantLegal/uploadnoc";
const LiquorLicImg = environment.base_Url + "restaurantLegal/uploadliquorlicense";
const EatingHouseImg = environment.base_Url + "restaurantLegal/uploadeatinghouselicense"
const MusicLicImg = environment.base_Url + "restaurantLegal/uploadmusiclicense";
const TrademarkImg = environment.base_Url + "restaurantLegal/uploadtrademarkforcafelicense";
const CECImg = environment.base_Url + "restaurantLegal/uploadcec";
const ShopsActImg = environment.base_Url + "restaurantLegal/uploadshopsandestablishmentact";
const SignageImg = environment.base_Url + "restaurantLegal/uploadsignagelicense";
const LiftImg = environment.base_Url + "restaurantLegal/uploadliftclearance"
const HealthTradeImg = environment.base_Url + "restaurantLegal/uploadhealthtradelicense"

@Component({
  selector: 'app-update-restaurant',
  templateUrl: './update-restaurant.component.html',
  styleUrls: ['./update-restaurant.component.scss']
})
export class UpdateRestaurantComponent implements OnInit {

  // test
  testTree = ['QSR', 'Pub']

  updateRestoData: any
  restoid: any

  // Declared outlet form group
  outletFormGrp: FormGroup

  // Declared oultet variables   
  ot_restaurant_is: any = 'solo' // Note: - by default should be solo
  ot_infrastructure_options = ['Standalone Entity', 'Food Court', 'Food Truck']
  ot_infrastructure = 'Standalone Entity' // Note:-  by Defalut selection be food court\

  options: any = ['Phoenix Market City', 'Amanora Park Town', 'Inorbit Mall', 'Pizza Hut', 'City Pride Kothrud', 'Adlabs Imagica Amusement Park', 'WestEnd Mall']
  options1: any = []
  options2: any = []

  isSoloStand: boolean = true;
  isSoloFood: boolean = true;
  isChainStand: boolean = true;
  isChainFood: boolean = true;

  isReadOnly: boolean = false;
  allBuildings = [
    { type: 'Mall' },
    { type: 'Commercial' },
    { type: 'Hospital' },
    { type: 'Stadium' },
    { type: 'Airport' },
    { type: 'Train Station' },
    { type: 'Bus Station' },
    { type: 'Institute' },
    { type: 'School' },
    { type: 'Park' },
    { type: 'Theme Park' },
    { type: 'Standalone' },
    { type: 'Multiplex' }
  ]

  restaurantIs = [{ name: 'Solo', value: 'solo' }, { name: 'Franchise/Chain', value: 'franchise' }]

  regAddress: any = [
    {
      infrastructure_name: 'Phoenix Market City',
      google_add: 'S No 207, Viman Nagar Road, Phoenix Road, Clover Park, Viman Nagar, Pune, Maharashtra 411014, India'
    },
    {
      infrastructure_name: 'Amanora Park Town',
      google_add: 'Amanora Park Town, Hadapsar, Pune, Maharashtra, India'
    },
    {
      infrastructure_name: 'Inorbit Mall',
      google_add: 'Viman Nagar, Pune, Maharashtra 411014, India'
    },
    {
      infrastructure_name: 'Pizza Hut',
      google_add: 'SN 1A,1B,1C,1E Gr, Millenium Plaza, FC clg Rd, Ganeshwadi, Shivajinagar, Pune, Maharashtra 411004, India'
    },
    {
      infrastructure_name: 'City Pride Kothrud',
      google_add: '20/1, 2, near Anand Residency, Paschimanagri, Kothrud, Pune, Maharashtra 411038, India'
    },
    {
      infrastructure_name: 'Adlabs Imagica Amusement Park',
      google_add: 'No. 30 31, State Highway 92 near Lonavla, Sangdewadi, Khopoli, Maharashtra 410203, India'
    },
    {
      infrastructure_name: 'WestEnd Mall',
      google_add: 'Near Parihar Chowk, Harmony Society, Ward No. 8, Wireless Colony, Aundh, Pune, Maharashtra 411007, India'
    },
  ]



  filteredOptions: Observable<string[]>;
  filteredOptions1: Observable<string[]>;
  filteredOptions2: Observable<string[]>;



  //**************DECLARE RESTAURANT FORMGRUP AND VARIABLES**************
  RestaurantInfo: FormGroup
  AllDialCodes: any = [];
  restaurants_types = [];
  typeDelievery: any = true;
  typeDinein: any = true;
  typeSelfservice: any = true;
  typeCloudKitchen: any = true;

  websiteId: any = '';
  webReadOnly: boolean = false;

  isCountryCodeSelected = '';
  Revenue: any = []

  isMon = true;
  isTues = false;
  isWed = false;
  isThur = false;
  isFri = false;
  isSat = false;
  isSun = false;

  monAddHr = false
  tueAddHr = false
  wedAddHr = false
  thurAddHr = false
  friAddHr = false
  satAddHr = false
  sunAddHr = false

  addmonHr: boolean = true;
  addtueHr: boolean = true;
  addwedHr: boolean = true;
  addthurHr: boolean = true;
  addfriHr: boolean = true;
  addsatHr: boolean = true;
  addsunHr: boolean = true;

  mondayOpenTime: any;
  mondayCloseTime: any;
  extramonOpenTime: any;
  extamonCloseTime: any;

  meridian = true;

  // GOOGLE PLACES VARIABLES
  @Input() adressType: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('addresstext', { static: false }) addresstext: any;
  restaurantCountryCode: any;
  restaurantContact: any;
  respresentativeCoutryCode: any;
  respresentativeContact: any;
  // ******************************************RESTAURATNT VARIABLES******************
  // **********************************************************************************


  //***************************************************/ESTABLISHMENT VARIABLES*********

  dummy: any = 'Establishment'
  estRootCategory: any = [];
  selectedMoreInfos: any = [];
  dupChkMoreInfo: any = [];
  barMoreInfo: any
  allMoreInfos: any = [];
  finalMoreInfobj: any;
  selectedServices: any = [];
  dupChkServ = [];
  serviceMoreInfo: any
  selectedRulesInfo = [];
  dupChkRules = [];
  rulesMoreInfo;
  selectedAmbienceInfo = [];
  dupChkAmbience = [];
  ambienceMoreInfo;
  allestdb: any = []
  selectedRootNames: any = []
  selectedCatNames: any = []
  selectedChildName: any = []
  selectedChildChildName: any = []


  //***************************************************/ESTABLISHMENT VARIABLES*********
  // **********************************************************************************

  //***************************************** */LEGAL VARIABLES DECLARATIONS**************
  allLegalDb: any = []
  LegalForm: FormGroup

  dropdownSettings = {};
  IDropdownSettings: IDropdownSettings
  selectedDocs: any = [];
  otherDocs: any = []
  dropdownList = [
    'GST',
    'PAN Card',
    'FSSAI Certificate',
    'NOC',
    'Liquor License',
    'Eating House License',
    'Music license',
    'Trademark for Cafe License',
    'Certificate of Environmental Clearance (CEC)',
    'Shops and Establishment Act',
    'Signage license',
    'Lift Clearance',
    'Health/Trade License'

  ];
  docsCount: number = 0;
  docs: FormGroup;
  otherResponse: any=[];
  isOtherTrash: boolean;
  gstResponse: string = '';
  isgstTrash: boolean;
  gstStatus: boolean;
  panResponse: string= '';
  ispanTrash: boolean;
  panStatus: boolean;
  fssaiResponse: string= '';
  isFssaiTrash: boolean;
  fssaiStatus: boolean;
  nocResponse: string= '';
  nocStatus: boolean;
  liquorLic: string;
  LiqLicStatus: boolean;
  eatingHouseResponse: string= '';
  eatLicStatus: boolean;
  musicLicResponse: string;
  musicLicStatus: boolean;
  trademarkResponse: string= '';
  trademarkStatus: boolean;
  cecResponse: string= '';
  cedStatus: boolean;
  shopActResponse: string= '';
  shopActStatus: boolean;
  sinageResponse: string= '';
  sinageStatus: boolean;
  LiftResponse: string= '';
  liftLicStatus: boolean;
  healthResponse: string= '';
  healthLicStatus: boolean;
  gstName: any;
  panName: any;
  
  fssaiName: any;

  nocName: any;
 
  LiqLicName: any;
  
  eatLicName: any;
 
  musicLicName: any;
  
  trademarkName: any;
 
  cedName: any;
  
  shopActName: any;
 
  sinageName: any;
 
  liftLicName: any;
 
  healthLicName: any;

   // Legal Images URLs
   public gstUploader: FileUploader = new FileUploader({ url: GstImg, itemAlias: 'gstfile' });
   public panUploader: FileUploader = new FileUploader({ url: PanImg, itemAlias: 'panfile' });
   public fssaiUploader: FileUploader = new FileUploader({ url: FssaiImg, itemAlias: 'fssaifile' });
   public nocUploader: FileUploader = new FileUploader({ url: NocImg, itemAlias: 'noc' });
   public LiquorLicUploader: FileUploader = new FileUploader({ url: LiquorLicImg, itemAlias: 'liquorlicense' });
   public EatingHouseUploader: FileUploader = new FileUploader({ url: EatingHouseImg, itemAlias: 'eatinghouselicense' });
   public MusicLicUploader: FileUploader = new FileUploader({ url: MusicLicImg, itemAlias: 'musiclicense' });
   public TrademarkUploader: FileUploader = new FileUploader({ url: TrademarkImg, itemAlias: 'trademarkforcafelicense' });
   public CECUploader: FileUploader = new FileUploader({ url: CECImg, itemAlias: 'cec' });
   public ShopsActUploader: FileUploader = new FileUploader({ url: ShopsActImg, itemAlias: 'shopsandestablishmentact' });
   public SignageUploader: FileUploader = new FileUploader({ url: SignageImg, itemAlias: 'signagelicense' });
   public LiftUploader: FileUploader = new FileUploader({ url: LiftImg, itemAlias: 'liftclearance' });
   public HealthTradeUploader: FileUploader = new FileUploader({ url: HealthTradeImg, itemAlias: 'healthtradelicense' });
 
   public otherUploader: FileUploader = new FileUploader({ url: OtherImg, itemAlias: 'otherfile' });


   docAble: boolean = false;
   docStatus: any = [];

  //***************************************** */LEGAL VARIABLES DECLARATIONS**************
  // **********************************************************************************

  //***************************************** */MEDIA VARIABLES DECLARATIONS**************

  allMediaDb: any
  MediaForm: FormGroup

  logoHide: boolean = true;
  logoResponse: any="";
  BannerResponse = [];
  bannerHide: boolean = true;
  ImgAlbumResponse = [];
  VideoAlbumResponse = [];
  createdVdoAlbum = [];
  isAlbum: boolean = true;
  albumNames: any = []
  album_image: any = [];
  album_video: any = [];

  public logoUploader: FileUploader = new FileUploader({ url: LogoImg, itemAlias: 'restologo' });
  public BannerUploader: FileUploader = new FileUploader({ url: BannerImg, itemAlias: 'restobanner' });
  public ImgAlbumUploader: FileUploader = new FileUploader({ url: ImagesAlbum, itemAlias: 'restoalbumimages' });
  public VideoAlbumUploader: FileUploader = new FileUploader({ url: VideoAlbum, itemAlias: 'restoalbumvideos' });


  //***************************************** */MEDIA VARIABLES DECLARATIONS**************
  // **********************************************************************************


  // ***********************************************MENU MGMT DECLARTIONS****************
  MenuMgmtForm: FormGroup

  menuNav = 'Menu'
  output: any

  allMenudb: any = []

  // cuisines
  selectedCuisines = [];
  selectedCuisinesName = [];
  allCuisines: any = []
  upallCuisines: any = []
  

  //menu 
  AllRootMenu: any = []
  selectedMenuRoot = [];
  selectedMenuCategory = [];
  selectedChildCategory = [];
  selectedMenucc = [];
 

  // liquor
  DrinkTypes: any = []
  selectedRootDrinksName = [];
  selectedDrinkVarient = [];
  selectedDrinkSubVarient = [];
  selectedDrinkSuperChild = [];



 

  // ***********************************************MENU MGMT DECLARTIONS****************
  // **************************************************************************************


  // ****************************************LAYOUT MGMT DECLARATIONS**********************
    // 
    allLayoutdb: any = [];
    client: boolean = false
  rectArr: any = [];
  i = 0

    // ****************************************LAYOUT MGMT DECLARATIONS**********************

    userid: any
  constructor(
    private router: Router,
    private restServ: RestoService,
    private fb: FormBuilder,
    private mainService: MainService,
    private matDialog: MatDialog,
    private ngxSpinner: NgxSpinnerService
  ) {

    this.selectedMoreInfos=[]

    let user = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = user._id

    this.updateRestoData = this.restServ.resto_update
    console.log('inside update oulet', this.updateRestoData);

    if (this.updateRestoData == undefined) {
      this.router.navigate(['/restaurants/'])
    }

    this.restoid = this.updateRestoData.data._id
    console.log('Resto id', this.restoid);



    //************************************************ */ OUTLET INITIAL FUNCTION

   
    if (this.updateRestoData.type == 'outletinfo') {
      console.log("yes you made it");

      // ********FILTER CINEMA 
      // fileter for cinema name

      this.restServ.getAllCinema().subscribe(data => {
        let dataArr = data['data']

        dataArr.forEach(element => {
          this.options1.push(element.display_name)
        });
      })

      // **********FILTER CHAIN
      // filter for chain name

      this.restServ.getAllChains().subscribe(data => {
        let dataArr = data['data']

        dataArr.forEach(element => {
          this.options2.push(element.chain_info.display_name)
        });
      })


    }

    // *************************************************/ RESTAURANT INITIAL FUNCTION
    if (this.updateRestoData.type == 'restoinfo') {
      //Calling all Dial COdes form service
      this.mainService.getAllDialCode().subscribe(data => {
        console.log('All Dial Codes', data);

        this.AllDialCodes = data

      })

      //  GET rEVENUE FUCNTIONS
      this.getRevenue()
    }

    // *************************************************/ Establishment INITIAL FUNCTION
    if (this.updateRestoData.type == 'estinfo') {

      this.allestdb = this.updateRestoData.data.establishmentType

      this.patchValueEst()
      // *************************************************** */ESTABLISMENT Initial function*********
      this.getAllMoreInfos();
      this.getEstRootType()

      //****** Calling All Rules API******

      this.getAllRulesChips();

      // ******* Calling Ambience API***********
      this.getAllAmbienceChips();

      // ********Calling All Services API******


      this.getAllServicesChips()


    }

    // *********************************************** */LEGAL INTITIAL Function
    if (this.updateRestoData.type == 'legalinfo') {

      this.allLegalDb = this.updateRestoData.data.legalInfo
      console.log("legal data from db",this.allLegalDb)

      if(this.allLegalDb.documents.length==0){
        console.log('document is 00');

        this.allLegalDb.documents = [{
          "documentName": "GST",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "PAN Card",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "FSSAI Certificate",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "NOC",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "Liquor License",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "Eating House License",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "Music license",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "Trademark for Cafe License",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "Certificate of Environmental Clearance (CEC)",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "Shops and Establishment Act",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "Signage license",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "Lift Clearance",
          "documentNumber": "",
          "documentUrl": ""
        },
        {
          "documentName": "Health/Trade License",
          "documentNumber": "",
          "documentUrl": ""
        }]
        
      }

      if(this.allLegalDb.otherDocuments.length==0){
        console.log('other documents is 00')
      }

      // this.patchValueLegal()
    }

    // ************************************************ */ Media Initial Functions
    if (this.updateRestoData.type == 'mediainfo') {

      this.allestdb = this.updateRestoData.data.establishmentType

      this.allMediaDb = this.updateRestoData.data.media
    }


    // ************************************************ */ MENU MANAGEMENT FUNCTIONS
    // *******Calling all cuisines API***********

    if (this.updateRestoData.type == 'menumgmt') {

      this.allMenudb = this.updateRestoData.data.menuManagement

      // GET ALL DRINKS FROM DB
      this.restServ.getALlLiquorTree().subscribe(data => {
        console.log('get all drinks', data);
        this.DrinkTypes = data['data']
      })


      // GET ALL MENU FROM DB
      this.restServ.getAllRootMenu().subscribe(data => {
        this.AllRootMenu = data['data']
        console.log('all root menu', this.AllRootMenu);

      })


      
    

      // Patch values for cuisine
      this.patchCuisines()

        //  Patch vALUES FOR MENU TREE
        this.patchMenu()


      // Pacth values for Liquor Tree
      this.patchLiqour()



    }

    // *********************************************MENU MGMGT**********************

    // **************************************Layout mgmgt*******************************
    // ******************************************************************
    if (this.updateRestoData.type =='layout'){
      // this.rectArr = this.updateRestoData.data.layout
      // layout code
    let exist = this.updateRestoData.data.layout

    console.log('exist', exist);
    
    if (exist.rectArr) {
      this.rectArr = exist.rectArr
      this.i = this.rectArr.length
    }
    else{
      this.rectArr = []
    }
    }


  }

  // ************************
  // ngONinit 
  // **************************

  ngAfterViewInit() {
    this.getPlaceAutocomplete();
  }

  ngOnInit() {

    // *********************************DEFINE OUTLET FORMGROUP******************************************8

    this.outletFormGrp = this.fb.group({
      outlet_code: [''],
      restaurant_is: ['solo'],
      infrastructure_type: ['Standalone Entity'],
      chain_name: [''],
      type_of_building: [''],
      name_of_building: [''],
      address_of_building: [''],
      multiplex_present: [Boolean],
      multiplex_name: [''],
      multiplex_need_qr: [Boolean],
      food_court_name: [''],
      food_court_need_qr: [Boolean],
      multiplex_screen: ['']
    })


    // *******************NAME OF BUILDING
    this.filteredOptions = this.outletFormGrp.controls.name_of_building.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );


    // ******************MULTIPLEX NAME
    this.filteredOptions1 = this.outletFormGrp.controls.multiplex_name.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter1(value))
      );


    // *******************CHAIN NAME
    this.filteredOptions2 = this.outletFormGrp.controls.chain_name.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter2(value))
      );


    if (this.updateRestoData.type == 'outletinfo') {
      this.patchOutletInfo()
    }
    // *********************************DEFINE OUTLET FORMGROUP******************************************
    // ********************************* ********************************* *********************************

    // ***********************************DEFINE RESTAURATN FORMGROUP**************************************

    // Restaurant Basic Information
    this.RestaurantInfo = this.fb.group({
      legal_name: ['', [Validators.required, Validators.minLength(3), Validators.pattern('^[ a-zA-Z0-9.]+$')]],
      restaurant_name: ['', [Validators.required, Validators.pattern(''), Validators.minLength(3)]],
      restaurant_country_code: ['', Validators.required],
      restaurant_contact: ['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      restaurant_email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      restaurant_address: ['', [Validators.required]],
      restaurant_locality: ['', [Validators.required]],
      restaurant_price_for_two: ['', Validators.required],
      restaurant_pincode: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      restaurant_revenue: ['', Validators.required],
      restaurant_website: ['', Validators.required],
      representative_name: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      respresentative_country_code: [''],
      representative_contact: ['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      representative_email: ['', [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+[\.]+[a-z]{2,4}$")]],
      openMon: { hour: 9, minute: 0 },
      openTue: [''],
      openWed: [''],
      openThur: [''],
      openFri: [''],
      openSat: [''],
      openSun: [''],

      extraopenMon: { hour: 17, minute: 0 },
      extraopenTue: [''],
      extraopenWed: [''],
      extraopenThur: [''],
      extraopenFri: [''],
      extraopenSat: [''],
      extraopenSun: [''],

      closeMon: { hour: 15, minute: 0 },
      closeTue: [''],
      closeWed: [''],
      closeThur: [''],
      closeFri: [''],
      closeSat: [''],
      closeSun: [''],

      extracloseMon: { hour: 23, minute: 0 },
      extracloseTue: [''],
      extracloseWed: [''],
      extracloseThur: [''],
      extracloseFri: [''],
      extracloseSat: [''],
      extracloseSun: [''],

    });

    if (this.updateRestoData.type == 'restoinfo') {
      console.log('if restoinfo');

      this.patchRestaurant()
    }

    // ***********************************DEFINE RESTAURATN FORMGROUP**************************************
    // ********************************* ********************************* *********************************

    // ************************************************LEGAL FORMGROUP*****************************************
    
    if (this.updateRestoData.type == 'legalinfo') {
      this.LegalForm = this.fb.group({

        //Banks detail []
        restaurant_bank_details: ['', Validators.required],
  
        // Bank Details Form Control Names
        restaurant_account_no: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(20), Validators.pattern('^[0-9]{7,20}$')]],
        restaurant_account_type: ['', Validators.required],
        restaurant_bank_name: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
        restaurant_branch_name: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
        restaurant_bank_city: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
        restaurant_bank_address: ['', Validators.required],
        restaurant_bank_ifsccode: ['', [Validators.required, Validators.pattern('^[A-Za-z]{4}0[A-Z0-9a-z]{6}$')]],
        restaurant_selected_docs: [],
        restaurant_gst_no: ['', Validators.required],
        restaurant_gst_file: ['', Validators.required],
        restaurant_pan_no: ['', Validators.required],
        restaurant_pan_image: ['', Validators.required],
        restaurant_fssai_certificate: ['', Validators.required],
        restaurant_fssai_image: ['', Validators.required],
        restaurant_noc_certificate: ['', Validators.required],
        restaurant_noc_certificate_file: ['', Validators.required],
        restaurant_liqLic: ['', Validators.required],
        restaurant_liqLic_file: ['', Validators.required],
        restauant_eatLic: ['', Validators.required],
        restauant_eatLic_file: ['', Validators.required],
        restaurant_musicLic: ['', Validators.required],
        restaurant_musicLic_file: ['', Validators.required],
        restaurant_tradeLic: ['', Validators.required],
        restaurant_tradeLic_file: ['', Validators.required],
        restaurant_cec: ['', Validators.required],
        restaurant_cec_file: ['', Validators.required],
        restaurant_shopAct: ['', Validators.required],
        restaurant_shopAct_file: ['', Validators.required],
        restaurant_sinageLic: ['', Validators.required],
        restaurant_sinageLic_file: ['', Validators.required],
        restaurant_liftClearance: ['', Validators.required],
        restaurant_liftClearance_file: ['', Validators.required],
        restuarant_healthLic: ['', Validators.required],
        restuarant_healthLic_file: ['', Validators.required],
        restaurant_legal_document: this.fb.array([])
  
      });
  
      // *********************************************PATCH VALUE LEGAL*********************
      // patchLegalValue()
  
          // ***GST UPLOADER
      // this.gstUploader.response.subscribe(res => this.response = res);
  
      this.gstUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
  
        this.gstResponse = response
        this.allLegalDb.documents[0].documentUrl = this.gstResponse
        console.log('Gst File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.gstName = item._file.name
          this.gstStatus = true
        }
  
        this.ngxSpinner.hide()
        this.LegalForm.patchValue({
          restaurant_gst_file: this.gstResponse
        })
        console.log('final response', this.gstResponse);
        // this.gstResponse = this.gstResponse
        this.isgstTrash = false
  
      }
  
  
      //  **PAN UPLOAD
      this.panUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
  
        this.panResponse = response
        this.allLegalDb.documents[1].documentUrl = this.panResponse
        console.log('Pan File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.panName = item._file.name
          this.panStatus = true
        }
  
  
        this.ngxSpinner.hide()
        console.log('response', this.panResponse);
        this.panResponse = response
        this.LegalForm.patchValue({
          restaurant_pan_image: response
        })
        // console.log('final response', this.panResponse['gstFilePhotoUrl']);
        // this.panResponse = this.panResponse['gstFilePhotoUrl']
        this.ispanTrash = false
      }
  
      // ** FSSAI UPLOAD
  
      this.fssaiUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
  
        this.fssaiResponse = response
        this.allLegalDb.documents[2].documentUrl = this.fssaiResponse
        console.log('Fssai File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.fssaiName = item._file.name
          this.fssaiStatus = true
        }
  
  
        this.ngxSpinner.hide()
        console.log('response', this.fssaiResponse);
        this.LegalForm.patchValue({
          restaurant_fssai_image: response
        })
        // console.log('final response', this.fssaiResponse['gstFilePhotoUrl']);
        // this.fssaiResponse = this.fssaiResponse['gstFilePhotoUrl']
        this.isFssaiTrash = false
      }
  
      // **NOC 
  
      this.nocUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
        this.ngxSpinner.hide()
        console.log('nocUploader response', response);
        this.nocResponse = response
        this.allLegalDb.documents[3].documentUrl = this.nocResponse
  
        console.log('Noc File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.nocName = item._file.name
          this.nocStatus = true
        }
  
        this.ngxSpinner.hide()
        console.log('response', this.nocResponse);
        this.LegalForm.patchValue({
          restaurant_noc_certificate_file: response
        })
  
      }
  
      // **LiquorLicUploader 
  
      this.LiquorLicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
        this.ngxSpinner.hide()
        console.log('LiquorLicUploader response', response);
        this.liquorLic = response
        this.allLegalDb.documents[4].documentUrl = this.liquorLic
  
        console.log('Noc File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.LiqLicName = item._file.name
          this.LiqLicStatus = true
        }
  
        this.ngxSpinner.hide()
        console.log('response', this.liquorLic);
        this.LegalForm.patchValue({
          restaurant_liqLic_file: response
        })
  
      }
  
      // **EatingHouseUploader 
  
      this.EatingHouseUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
        this.ngxSpinner.hide()
        console.log('EatingHouseUploader response', response);
        this.eatingHouseResponse = response
        this.allLegalDb.documents[5].documentUrl = this.eatingHouseResponse
  
        console.log('Eating File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.eatLicName = item._file.name
          this.eatLicStatus = true
        }
  
        this.ngxSpinner.hide()
        console.log('response', this.eatingHouseResponse);
        this.LegalForm.patchValue({
          restauant_eatLic_file: response
        })
      }
  
      // **MusicLicUploader 
  
      this.MusicLicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
        this.ngxSpinner.hide()
        console.log('MusicLicUploader response', response);
        this.musicLicResponse = response
        this.allLegalDb.documents[6].documentUrl = this.musicLicResponse
  
        console.log('Music File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.musicLicName = item._file.name
          this.musicLicStatus = true
        }
  
        this.ngxSpinner.hide()
        console.log('response', this.musicLicResponse);
        this.LegalForm.patchValue({
          restaurant_musicLic_file: response
        })
  
      }
  
      // **TrademarkUploader 
  
      this.TrademarkUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
        this.ngxSpinner.hide()
        console.log('TrademarkUploader response', response);
        this.trademarkResponse = response
        this.allLegalDb.documents[7].documentUrl = this.trademarkResponse
  
  
        console.log('TradeMark File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.trademarkName = item._file.name
          this.trademarkStatus = true
        }
  
        this.ngxSpinner.hide()
        console.log('response', this.trademarkResponse);
        this.LegalForm.patchValue({
          restaurant_tradeLic_file: response
        })
      }
  
      // **CECUploader 
  
      this.CECUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
        this.ngxSpinner.hide()
        console.log('CECUploader response', response);
        this.cecResponse = response
        this.allLegalDb.documents[8].documentUrl = this.cecResponse
  
        console.log('CEC File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.cedName = item._file.name
          this.cedStatus = true
        }
  
        this.ngxSpinner.hide()
        console.log('response', this.cecResponse);
        this.LegalForm.patchValue({
          restaurant_cec_file: response
        })
      }
  
      // **ShopsActUploader 
  
      this.ShopsActUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
        this.ngxSpinner.hide()
        console.log('ShopsActUploader response', response);
        this.shopActResponse = response
        this.allLegalDb.documents[9].documentUrl = this.shopActResponse
  
        console.log('Shop Act File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.shopActName = item._file.name
          this.shopActStatus = true
        }
  
        this.ngxSpinner.hide()
        console.log('response', this.shopActResponse);
        this.LegalForm.patchValue({
          restaurant_shopAct_file: response
        })
      }
  
      // **SignageUploader 
  
      this.SignageUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
        this.ngxSpinner.hide()
        console.log('SignageUploader response', response);
        this.sinageResponse = response
        this.allLegalDb.documents[10].documentUrl = this.sinageResponse
  
        console.log('sinage Act File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.sinageName = item._file.name
          this.sinageStatus = true
        }
  
        this.ngxSpinner.hide()
        console.log('response', this.sinageResponse);
        this.LegalForm.patchValue({
          restaurant_sinageLic_file: response
        })
      }
  
      // **LiftUploader 
  
      this.LiftUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
        this.ngxSpinner.hide()
        console.log('LiftUploader response', response);
        this.LiftResponse = response
        this.allLegalDb.documents[11].documentUrl = this.LiftResponse
  
        console.log('Lift Act File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.liftLicName = item._file.name
          this.liftLicStatus = true
        }
  
        this.ngxSpinner.hide()
        console.log('response', this.LiftResponse);
        this.LegalForm.patchValue({
          restaurant_liftClearance_file: response
        })
      }
  
  
      // **HealthTradeUploader 
  
      this.HealthTradeUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
        console.log('FileUpload:uploaded:', item, status, response);
        this.ngxSpinner.hide()
        console.log('HealthTradeUploader response', response);
        this.healthResponse = response
        this.allLegalDb.documents[12].documentUrl = this.healthResponse
  
        console.log('Health File Name', item._file.name);
        console.log('uploading status', item.isUploaded)
  
        if (item.isUploaded) {
          this.healthLicName = item._file.name
          this.healthLicStatus = true
        }
  
        this.ngxSpinner.hide()
        console.log('response', this.healthResponse);
        this.LegalForm.patchValue({
          restuarant_healthLic_file: response
        })
      }
  

          // ** Other UPLOAD

    this.otherUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      if(status==200){
        if (this.otherResponse.length <= 4) {
          this.otherResponse.push(response)
          this.docs.patchValue({
            documentUrl: response
          })          
        }

        Swal.fire('Document uploaded','','success')
        this.docStatus[this.docFormArray.controls.indexOf(this.docs)].isDoc = true
        this.docAble = false
      }
      else{
        Swal.fire('Failed to upload','','warning')
      }
    



      console.log('response', this.otherResponse);
      this.ngxSpinner.hide()
      // console.log('other response', this.otherResponse['gstFilePhotoUrl']);
      // this.docs.patchValue({
      //   documentUrl: this.otherResponse
      // })
      // this.otherResponse = this.otherResponse['gstFilePhotoUrl']
      // this.isOtherTrash = false

    }
  
  
      this.patchValueLegal()
    }
   
    // ************************************************LEGAL FORMGROUP*****************************************
    // ********************************* ********************************* *********************************


    // ************************************************MEDIA FORMGROUP*****************************************
  
    if (this.updateRestoData.type == 'mediainfo') {
      this.MediaForm = this.fb.group({

        // other Legal Document Controls Name
        restaurant_logo: ['', Validators.required],
        restaurant_banner: ['', Validators.required],
        restaurant_pictures: ['', Validators.required],
        restaurant_videos: ['', Validators.required]
  
      });

      this.patchMediaValue()
    }
 


    // **Other LOGO UPLOAD
    this.logoUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.logoResponse = response
      this.ngxSpinner.hide()
      console.log('Logo Response', response);

      console.log('Logo photo', this.logoResponse);

      this.logoHide = false

      Swal.fire('Logo Uploaded', '', 'success')



    }

  
     // **Other BannerUploader UPLOAD
     this.BannerUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.BannerResponse.push(response)

      console.log(' Banner Response', this.BannerResponse);
      this.bannerHide = false
      this.ngxSpinner.hide()
    }

    // **Other ImgAlbumUploader UPLOAD
    this.ImgAlbumUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.ImgAlbumResponse.push(response)

      console.log('Images Response', this.ImgAlbumResponse);
      this.ngxSpinner.hide()
    }


    // Video collection upload

    this.VideoAlbumUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // this.VideoAlbumResponse = []
      console.log('FileUpload:uploaded:', item, status, response);
      this.VideoAlbumResponse.push(response)
      this.ngxSpinner.hide();
      console.log(' Video Response', this.VideoAlbumResponse);

    }

    // ************************************************MEDIA FORMGROUP*****************************************
    // ****************************************************************************************

    // ************************************************MENU MANAGEMENT*****************************************
    this.MenuMgmtForm = this.fb.group({


      //Restaurant Food [] contro l NAME
      restaurant_add_food: ['', Validators.required],


      //Restaurant Menu Management
      restaurant_select_cuisine: ['', Validators.required],
      restaurant_select_menu: ['', Validators.required],

      //Restaurant Liquor 
      restaurant_add_liquor_product: ['', Validators.required]

    });

    // ************************************************ MENU MANAGEMENT FORMGROUP*****************************************
    // ****************************************************************************************

     // ******************************************LAYOUT MANAGEMENT FORMGROUP**********************************

     this.patchLayout()
     
     // ******************************************LAYOUT MANAGEMENT FORMGROUP**********************************
    //  ****************************************************************************************************

  }


  // **************************************Funtions outlet*****************************************
  // ************************************************************************************************

  // OUTLET INFO STEEPPER FUNCTIONS
  solostand(){

    // SOLO STANDALONE SOLO
      if(this.outletFormGrp.controls.type_of_building.value && 
        this.outletFormGrp.controls.name_of_building.value &&
        this.outletFormGrp.controls.address_of_building.value ){
        console.log("yeseeellelelelel");
        this.isSoloStand = false
      }
      else{
        console.log("NOOOOOOOO");
        this.isSoloStand = true
      }


      // SOLO FOOD 
      
      

      if(this.outletFormGrp.controls.type_of_building.value && 
        this.outletFormGrp.controls.name_of_building.value &&
        this.outletFormGrp.controls.address_of_building.value &&
        this.outletFormGrp.controls.food_court_name.value &&
        this.outletFormGrp.controls.food_court_need_qr.value &&
        this.outletFormGrp.controls.multiplex_present.value){
        console.log("yeseeellelelelel");
        this.isSoloFood = false
      }
      else{
        console.log("NOOOOOOOO");
        this.isSoloFood = true
      }


      // CHAIN STANDALONE
      
      if(this.outletFormGrp.controls.chain_name.value &&
        this.outletFormGrp.controls.type_of_building.value && 
        this.outletFormGrp.controls.name_of_building.value &&
        this.outletFormGrp.controls.address_of_building.value ){
        console.log("yeseeellelelelel");
        this.isChainStand = false
      }
      else{
        console.log("NOOOOOOOO");
        this.isChainStand = true
      }

      // CHAIN FOOD 
      if(this.outletFormGrp.controls.chain_name.value && 
        this.outletFormGrp.controls.type_of_building.value && 
        this.outletFormGrp.controls.name_of_building.value &&
        this.outletFormGrp.controls.address_of_building.value &&
        this.outletFormGrp.controls.food_court_name.value &&
        this.outletFormGrp.controls.food_court_need_qr.value &&
        this.outletFormGrp.controls.multiplex_present.value){
        console.log("yeseeellelelelel");
        this.isChainFood = false
      }
      else{
        console.log("NOOOOOOOO");
        this.isChainFood = true
      }

  }

  ouletRestoIs() {
    console.log(this.outletFormGrp.controls.restaurant_is.value);
    this.ot_restaurant_is = this.outletFormGrp.controls.restaurant_is.value
    this.onChangeOutlet('outlet')
  }

  // ON SELECT INFRASTRUCTURE

  onSelectInfra() {

    // this.outletFormGrp.controls.infrastructure_type.reset()
    this.ot_infrastructure = this.outletFormGrp.controls.infrastructure_type.value

    console.log('selection ', this.ot_infrastructure)
    this.onChangeOutlet('infrastructure')
  }

  // Defalut on Change Outlet is
  onChangeOutlet(type) {
    if (type == 'outlet') {
      this.ot_infrastructure = "Standalone Entity"
      this.outletFormGrp.controls.infrastructure_type.setValue(this.ot_infrastructure)
    }

    this.outletFormGrp.controls.type_of_building.reset()
    this.outletFormGrp.controls.name_of_building.reset()
    this.outletFormGrp.controls.chain_name.reset()
    this.outletFormGrp.controls.address_of_building.reset()
    this.outletFormGrp.controls.multiplex_present.reset()
    this.outletFormGrp.controls.multiplex_need_qr.reset()
    this.outletFormGrp.controls.multiplex_name.reset()
    this.outletFormGrp.controls.food_court_name.reset()
    this.outletFormGrp.controls.food_court_need_qr.reset()
    this.outletFormGrp.controls.multiplex_screen.reset()
  }


  // FILTER NAME OF BUILDING
  // filter stirng function
  private _filter(value: string): string[] {

    if (value != undefined) {
      const filterValue = value.toLowerCase();

      return this.options.filter(option => option.toLowerCase().includes(filterValue));
    }

  }


  // Filter Name
  // filter cinema name
  private _filter1(value: string): string[] {
    if (value != undefined) {
      const filterValue = value.toLowerCase()
      return this.options1.filter(option => option.toLowerCase().includes(filterValue));
    }
  }

  // fILTER CHAIN NAME

  // fileter chain name
  private _filter2(value: string): string[] {
    if (value != undefined) {
      const filterValue = value.toLowerCase()
      return this.options2.filter(option => option.toLowerCase().includes(filterValue));
    }
  }


  //   // On Change TYPE OF BUILDING
  onTypeOfBuilding(item) {
    this.outletFormGrp.patchValue({
      type_of_building: item.type
    })

    console.log('building type', item.type);

  }

  // On Selecting Building  Name

  onbuildName(item) {
    console.log('building name', item);


    this.regAddress.forEach(element => {
      if (element.infrastructure_name === item) {
        this.outletFormGrp.patchValue({
          address_of_building: element.google_add
        })
      }
    });

    // readOnlyTrue
    this.isReadOnly = true

  }


  // change read only status to false
  onChngReadOnly() {
    this.isReadOnly = false
  }


  // Patch value Outlet
  patchOutletInfo() {

    let update = this.updateRestoData.data
    console.log('updata', update);

    this.outletFormGrp.patchValue({
      outlet_code: update.outletInfo.outlet_code,
      restaurant_is: update.outletInfo.restaurant_is,
      infrastructure_type: update.outletInfo.infrastructure_type,
      chain_name: update.outletInfo.chain_name,
      type_of_building: update.outletInfo.type_of_building,
      name_of_building: update.outletInfo.name_of_building,
      address_of_building: update.outletInfo.address_of_building,
      multiplex_present: update.outletInfo.multiplex_present,
      multiplex_name: update.outletInfo.multiplex_name,
      multiplex_need_qr: update.outletInfo.multiplex_need_qr,
      food_court_name: update.outletInfo.food_court_name,
      food_court_need_qr: update.outletInfo.food_court_need_qr,
      multiplex_screen: update.outletInfo.multiplex_screen
    })

    this.ot_infrastructure = update.outletInfo.infrastructure_type
  }

  // UPDATE OULET
  updateOutlet() {

    // this.outletFormGrp.patchValue({
    //   outlet_code: 'OUT003'
    // })

    console.log("restoid", this.restoid)
    console.log("update oulet", this.outletFormGrp.value);

    // let obj = this.outletFormGrp.value

    //  **********PUT SERVICE FOR RESTO INFO***************

    this.restServ.putouletInfo(this.restoid,{outletInfo: this.outletFormGrp.value, userid: this.userid} ).subscribe(addRestoResponse => {
      if (addRestoResponse['sucess']) {
        Swal.fire("Outlet info Updated",'','success')
        this.restServ.current_restoid= this.restoid
        this.router.navigate(['/restaurants/view-restaurant'])
      }
      else {
        Swal.fire('Failed to add')
      }

      console.log('RESPONSE: ', addRestoResponse);
      // this.OutletId = addRestoResponse['data']

      console.log('Resto Id', this.restoid)
    })


    // this.restServ
  }

  // *********************************OUTLET FUNCTIONS************************************
  // ****************************************************************************************



  // *************************RESTAURATN FuNTIONS*********************************************************************



  patchRestaurant() {

    let update = this.updateRestoData.data
    console.log('update patchvalue', update.restoInfo);

    // Restaurant Basic Information
    this.RestaurantInfo = this.fb.group({
      legal_name: [update.restoInfo.restoLegalName, [Validators.required, Validators.minLength(3), Validators.pattern('^[ a-zA-Z0-9.]+$')]],
      restaurant_name: [update.restoInfo.restoName, [Validators.required, Validators.pattern(''), Validators.minLength(3)]],
      restaurant_country_code: update.restoInfo.restoCountryCode,
      restaurant_contact: [update.restoInfo.restoContactNo, [Validators.required, Validators.pattern("[0-9]{10}")]],
      restaurant_email: [update.restoInfo.restoEmail, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      restaurant_address: [update.restoInfo.restoAddress, [Validators.required]],
      restaurant_locality: [update.restoInfo.restoLocality, [Validators.required]],
      restaurant_price_for_two: [update.restoInfo.restoPriceForTwo, Validators.required],
      restaurant_pincode: [update.restoInfo.restoPincode, [Validators.required, Validators.pattern('^[0-9]+$')]],
      restaurant_revenue: [update.restoInfo.restoRevenue, Validators.required],
      restaurant_website: [update.restoInfo.restoWebsite, Validators.required],
      representative_name: [update.restoInfo.restoRepresentativeName, [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
      respresentative_country_code: update.restoInfo.restoRepresentativeCountryCode ,
      representative_contact: [update.restoInfo.restoRepresentativeContactNo, [Validators.required, Validators.pattern("[0-9]{10}")]],
      representative_email: [update.restoInfo.restoRepresentativeEmail, [Validators.required, Validators.pattern("[a-z0-9._%+-]+@[a-z0-9.-]+[\.]+[a-z]{2,4}$")]],
      openMon: update.restoInfo.restoHours[0].open,
      openTue: update.restoInfo.restoHours[1].open,
      openWed: update.restoInfo.restoHours[2].open,
      openThur: update.restoInfo.restoHours[3].open,
      openFri: update.restoInfo.restoHours[4].open,
      openSat: update.restoInfo.restoHours[5].open,
      openSun: update.restoInfo.restoHours[6].open,

      extraopenMon: update.restoInfo.restoHours[0].addhrOpen,
      extraopenTue: update.restoInfo.restoHours[1].addhrOpen,
      extraopenWed: update.restoInfo.restoHours[2].addhrOpen,
      extraopenThur: update.restoInfo.restoHours[3].addhrOpen,
      extraopenFri: update.restoInfo.restoHours[4].addhrOpen,
      extraopenSat: update.restoInfo.restoHours[5].addhrOpen,
      extraopenSun: update.restoInfo.restoHours[6].addhrOpen,

      closeMon: update.restoInfo.restoHours[0].close,
      closeTue: update.restoInfo.restoHours[1].close,
      closeWed: update.restoInfo.restoHours[2].close,
      closeThur: update.restoInfo.restoHours[3].close,
      closeFri: update.restoInfo.restoHours[4].close,
      closeSat: update.restoInfo.restoHours[5].close,
      closeSun: update.restoInfo.restoHours[6].close,

      extracloseMon: update.restoInfo.restoHours[0].addhrClose,
      extracloseTue: update.restoInfo.restoHours[1].addhrClose,
      extracloseWed: update.restoInfo.restoHours[2].addhrClose,
      extracloseThur: update.restoInfo.restoHours[3].addhrClose,
      extracloseFri: update.restoInfo.restoHours[4].addhrClose,
      extracloseSat: update.restoInfo.restoHours[5].addhrClose,
      extracloseSun: update.restoInfo.restoHours[6].addhrClose,

    });

    this.restaurants_types = update.restoInfo.restoType
    // restoType
    this.isMon = true
    this.isTues = true
    this.isWed = true
    this.isThur = true
    this.isFri = true
    this.isSat = true
    this.isSun = true

    this.addmonHr = false

    this.addtueHr = false

    this.addwedHr = false

    this.addthurHr = false

    this.addfriHr = false

    this.addsatHr = false

    this.addsunHr = false


    if (this.restaurants_types.includes('Delievery')) {
      this.typeDelievery = false
    }
    if (this.restaurants_types.includes('Dine-In')) {
      this.typeDinein = false
    }
    if (this.restaurants_types.includes('Self Service')) {
      this.typeSelfservice = false
    }
    if (this.restaurants_types.includes('Cloud Kitchen')) {
      this.typeCloudKitchen = false
    }





    console.log('after patchvalue', this.RestaurantInfo.value);

  }


  // Google Places maps

  private getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
      {
        // componentRestrictions: { country: 'IN' },
        types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
      });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      this.invokeEvent(place);


    });




  }

  // Google Invoke Event
  invokeEvent(place: Object) {
    this.setAddress.emit(place);
    console.log('place', place['address_components']);
    console.log('Google Data', place);


    let components = place['address_components']

    // Below code is to extract Pincode
    components.forEach(element => {
      if (element.types.includes("postal_code")) {
        console.log('Pincode', element.long_name);
        this.RestaurantInfo.patchValue({
          restaurant_pincode: element.long_name
        })

      }
    });


    let localities = []

    components.forEach(element => {
      if (element.types.includes("sublocality")) {
        console.log("sublocality", element.long_name);

        localities.push(element.long_name)

        this.RestaurantInfo.patchValue({
          restaurant_locality: localities
        })

      }

    })


  }

  // RESTAURANT DELIEVERY TYPES FUCNTIONS

  onDelievery() {
    this.typeDelievery = !this.typeDelievery
    if (!this.typeDelievery) {
      this.restaurants_types.push('Delievery');
      console.log('restaurants types', this.restaurants_types);

    }
    else {
      this.restaurants_types.splice(this.restaurants_types.indexOf('Delievery'), 1)
      console.log('restaurants types', this.restaurants_types);


    }
  }

  onDinein() {
    this.typeDinein = !this.typeDinein

    if (!this.typeDinein) {
      this.restaurants_types.push('Dine-In');
      console.log('restaurants types', this.restaurants_types);

    }
    else {
      this.restaurants_types.splice(this.restaurants_types.indexOf('Dine-In'), 1)
      console.log('restaurants types', this.restaurants_types);


    }
  }

  onSelfService() {
    this.typeSelfservice = !this.typeSelfservice
    if (!this.typeSelfservice) {
      this.restaurants_types.push('Self Service');
      console.log('restaurants types', this.restaurants_types);

    }
    else {
      this.restaurants_types.splice(this.restaurants_types.indexOf('Self Service'), 1)
      console.log('restaurants types', this.restaurants_types);


    }
  }

  onCloudKitchen() {
    this.typeCloudKitchen = !this.typeCloudKitchen
    if (!this.typeCloudKitchen) {
      this.restaurants_types.push('Cloud Kitchen');
      console.log('restaurants types', this.restaurants_types);

    }
    else {
      this.restaurants_types.splice(this.restaurants_types.indexOf('Cloud Kitchen'), 1)
      console.log('restaurants types', this.restaurants_types);


    }





  }

  // onGENERATE NEW WEBSITE

  onGenerateWebsite() {
    const dialogRef = this.matDialog.open(GenerateWebsiteComponent, {
      width: 'auto'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);


      if (result.websitename == '' || result.websitename == undefined) {
        this.webReadOnly = false
        this.websiteId = ''
        this.RestaurantInfo.patchValue({
          restaurant_website: ''
        })
      }
      else {
        this.webReadOnly = true
        this.websiteId = result.websiteId
        this.RestaurantInfo.patchValue({
          restaurant_website: result.websitename
        })
      }


    })
  }

  // on country code patch
  onCountryCode(evt) {

    console.log('country code', evt)
    this.isCountryCodeSelected = evt.dial_code

    this.RestaurantInfo.patchValue({
      restaurant_country_code: this.isCountryCodeSelected,
      // respresentative_country_code: this.isCountryCodeSelected
    })
    console.log(
      'selected country', this.isCountryCodeSelected
    );

  }

  // on representative country code
  repCountryCode(evt) {

    console.log('country code', evt)
    this.isCountryCodeSelected = evt.dial_code

    this.RestaurantInfo.patchValue({
      // restaurant_country_code: this.isCountryCodeSelected,
      respresentative_country_code: this.isCountryCodeSelected
    })
    console.log(
      'selected country', this.isCountryCodeSelected
    );

  }

  // get function REVENUE
  getRevenue() {
    this.restServ.getAllRevenue().subscribe(data => {
      this.Revenue = data['data']

    })
  }

  //******Open add Revenue Dialog

  openAddRevenueDialog() {

    const dialogRef = this.matDialog.open(AddRevenueComponent, {
      width: '400px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'After closing Dialog'
      );
      this.getRevenue();
    })
  }

  // Restauarnat Email KEY UP FUNTION
  onEmailKeyup() {
    let abc = this.RestaurantInfo.controls['restaurant_email'].value
    this.RestaurantInfo.patchValue({
      restaurant_email: abc.trim().toLowerCase().replace(/\s/g, "")

    })


    console.log('restauant email', this.RestaurantInfo.controls['restaurant_email'].value);
  }

  // Representative email keyup funtion
  onRepEmailKeyup() {
    let abc = this.RestaurantInfo.controls['representative_email'].value
    this.RestaurantInfo.patchValue({
      representative_email: abc.trim().toLowerCase().replace(/\s/g, "")

    })


    console.log('restauant email', this.RestaurantInfo.controls['restaurant_email'].value);
  }

  // after click on sameday 
  onSameAsMonday(chk) {

    console.log('checked vlaue', chk)
    if (this.monAddHr) {
      this.tueAddHr = true;
      this.wedAddHr = true;
      this.thurAddHr = true;
      this.friAddHr = true;
      this.satAddHr = true;
      this.sunAddHr = true;
    }

    if (this.addmonHr) {
      this.addtueHr = true;
      this.addwedHr = true;
      this.addthurHr = true;
      this.addfriHr = true;
      this.addsatHr = true;
      this.addsunHr = true;

    }
    else {
      this.addtueHr = false;
      this.addwedHr = false;
      this.addthurHr = false;
      this.addfriHr = false;
      this.addsatHr = false;
      this.addsunHr = false;
    }

    // TIME VALUE SAME AS MONDAY

    this.mondayOpenTime = this.RestaurantInfo.controls['openMon'].value
    this.mondayCloseTime = this.RestaurantInfo.controls['closeMon'].value

    console.log('Extra open monday vlaue', this.RestaurantInfo.controls['extraopenMon'].value);
    this.extramonOpenTime = this.RestaurantInfo.controls['extraopenMon'].value
    this.extamonCloseTime = this.RestaurantInfo.controls['extracloseMon'].value


    console.log('tIMES values ', this.mondayOpenTime);

    this.RestaurantInfo.patchValue({
      openTue: this.mondayOpenTime,
      openWed: this.mondayOpenTime,
      openThur: this.mondayOpenTime,
      openFri: this.mondayOpenTime,
      openSat: this.mondayOpenTime,
      openSun: this.mondayOpenTime,

      extraopenTue: this.extramonOpenTime,
      extraopenWed: this.extramonOpenTime,
      extraopenThur: this.extramonOpenTime,
      extraopenFri: this.extramonOpenTime,
      extraopenSat: this.extramonOpenTime,
      extraopenSun: this.extramonOpenTime,


      closeTue: this.mondayCloseTime,
      closeWed: this.mondayCloseTime,
      closeThur: this.mondayCloseTime,
      closeFri: this.mondayCloseTime,
      closeSat: this.mondayCloseTime,
      closeSun: this.mondayCloseTime,

      extracloseTue: this.extamonCloseTime,
      extracloseWed: this.extamonCloseTime,
      extracloseThur: this.extamonCloseTime,
      extracloseFri: this.extamonCloseTime,
      extracloseSat: this.extamonCloseTime,
      extracloseSun: this.extamonCloseTime

    })




    //RADIO SAME AS MONDAY
    if (chk.target.checked) {

      this.isTues = this.isMon;
      this.isWed = this.isMon;
      this.isThur = this.isMon;
      this.isFri = this.isMon;
      this.isSat = this.isMon;
      this.isSun = this.isMon;
    }

    else {
      this.isTues = false;
      this.isWed = false;
      this.isThur = false;
      this.isFri = false;
      this.isSat = false;
      this.isSun = false;

      // **********************************

      this.RestaurantInfo.controls['openTue'].reset(),
        this.RestaurantInfo.controls['closeTue'].reset(),
        this.RestaurantInfo.controls['extraopenTue'].reset(),
        this.RestaurantInfo.controls['extracloseTue'].reset()

      this.RestaurantInfo.controls['openWed'].reset(),
        this.RestaurantInfo.controls['closeWed'].reset(),
        this.RestaurantInfo.controls['extraopenWed'].reset(),
        this.RestaurantInfo.controls['extracloseWed'].reset()



      this.RestaurantInfo.controls['openThur'].reset(),
        this.RestaurantInfo.controls['closeThur'].reset(),
        this.RestaurantInfo.controls['extraopenThur'].reset(),
        this.RestaurantInfo.controls['extracloseThur'].reset()



      this.RestaurantInfo.controls['openFri'].reset(),
        this.RestaurantInfo.controls['closeFri'].reset(),
        this.RestaurantInfo.controls['extraopenFri'].reset(),
        this.RestaurantInfo.controls['extracloseFri'].reset()


      this.RestaurantInfo.controls['openSat'].reset(),
        this.RestaurantInfo.controls['closeSat'].reset(),
        this.RestaurantInfo.controls['extraopenSat'].reset(),
        this.RestaurantInfo.controls['extracloseSat'].reset()


      this.RestaurantInfo.controls['openSun'].reset(),
        this.RestaurantInfo.controls['closeSun'].reset(),
        this.RestaurantInfo.controls['extraopenSun'].reset(),
        this.RestaurantInfo.controls['extracloseSun'].reset()


      // *********************************
    }

  }

  // ********************88
  // ******************
  // WEEK RADIO BUTTON TIMES 

  onMonday(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isMon = true;
    }
    else {
      this.isMon = false;
    }

  }

  onTuesday(evt) {
    if (evt.checked) {
      this.isTues = true;
    }
    else {
      this.isTues = false;
    }
  }

  onWednesday(evt) {
    if (evt.checked) {
      this.isWed = true;
    }
    else {
      this.isWed = false;
    }
  }

  onThursday(evt) {
    if (evt.checked) {
      this.isThur = true;
    }
    else {
      this.isThur = false;
    }
  }


  onFriday(evt) {
    if (evt.checked) {
      this.isFri = true;
    }
    else {
      this.isFri = false;
    }
  }

  onSaturday(evt) {
    if (evt.checked) {
      this.isSat = true;
    }
    else {
      this.isSat = false;
    }
  }

  onSunday(evt) {
    if (evt.checked) {
      this.isSun = true;
    }
    else {
      this.isSun = false;
    }
  }

  toggleMeridian() {
    this.meridian = !this.meridian;
  }

  // ********************************ADD EXTRA HOURS***********************

  // add extra hours

  onAddHr(val) {
    if (val == 'mon') {
      this.addmonHr = false
    }

    if (val == 'tue') {
      this.addtueHr = false
    }
    if (val == 'wed') {
      this.addwedHr = false
    }
    if (val == 'thur') {
      this.addthurHr = false
    }
    if (val == 'fri') {
      this.addfriHr = false
    }
    if (val == 'sat') {
      this.addsatHr = false
    }
    if (val == 'sun') {
      this.addsunHr = false
    }

  }

  // remove extra
  onRemoveAddHr(val) {
    if (val == 'mon') {
      this.addmonHr = true
    }

    if (val == 'tue') {
      this.addtueHr = true
    }
    if (val == 'wed') {
      this.addwedHr = true
    }
    if (val == 'thur') {
      this.addthurHr = true
    }
    if (val == 'fri') {
      this.addfriHr = true
    }
    if (val == 'sat') {
      this.addsatHr = true
    }
    if (val == 'sun') {
      this.addsunHr = true
    }

  }


  // RESTAURANT UPDATE FUNCTIONS
  onRestoUpdate() {

    // ADD RESTAURABTS contact with country code: 

    this.restaurantCountryCode = this.RestaurantInfo.controls['restaurant_country_code'].value
    this.restaurantContact = this.RestaurantInfo.controls['restaurant_contact'].value
    this.respresentativeCoutryCode = this.RestaurantInfo.controls['respresentative_country_code'].value
    this.respresentativeContact = this.RestaurantInfo.controls['representative_contact'].value

    // FINAL OBJECT FOR RESTO OBJECT
    var Restobj = {
      restoLegalName: this.RestaurantInfo.controls['legal_name'].value,
      restoName: this.RestaurantInfo.controls['restaurant_name'].value,
      // restaurant_contact: this.RestaurantInfo.controls['restaurant_contact'].value,
      restoContactNo: this.restaurantContact,
      restoStatus: true,
      restoEmail: this.RestaurantInfo.controls['restaurant_email'].value,
      restoAddress: this.RestaurantInfo.controls['restaurant_address'].value,
      restoPriceForTwo: this.RestaurantInfo.controls['restaurant_price_for_two'].value,
      restoLocality: this.RestaurantInfo.controls['restaurant_locality'].value,
      restoPincode: this.RestaurantInfo.controls['restaurant_pincode'].value,
      restoRevenue: this.RestaurantInfo.controls['restaurant_revenue'].value,
      restoCountryCode: this.restaurantCountryCode,
      restoType: this.restaurants_types,
      restoWebsite: this.RestaurantInfo.controls['restaurant_website'].value,
      restoRepresentativeName: this.RestaurantInfo.controls['representative_name'].value,
      restoRepresentativeCountryCode: this.respresentativeCoutryCode,
      restoRepresentativeContactNo: this.respresentativeContact,
      restoRepresentativeEmail: this.RestaurantInfo.controls['representative_email'].value,

      // restoHours: this.RestaurantInfo.controls['representative_email'].value,
      restoHours: [
        {
          day: 'monday',
          open: this.RestaurantInfo.controls['openMon'].value,
          close: this.RestaurantInfo.controls['closeMon'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenMon'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseMon'].value

        },
        {
          day: 'tuesday',
          open: this.RestaurantInfo.controls['openTue'].value,
          close: this.RestaurantInfo.controls['closeTue'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenTue'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseTue'].value

        },
        {
          day: 'wednesday',
          open: this.RestaurantInfo.controls['openWed'].value,
          close: this.RestaurantInfo.controls['closeWed'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenWed'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseWed'].value

        },
        {
          day: 'thursday',
          open: this.RestaurantInfo.controls['openThur'].value,
          close: this.RestaurantInfo.controls['closeThur'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenThur'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseThur'].value

        },
        {
          day: 'friday',
          open: this.RestaurantInfo.controls['openFri'].value,
          close: this.RestaurantInfo.controls['closeFri'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenFri'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseFri'].value

        },
        {
          day: 'saturday',
          open: this.RestaurantInfo.controls['openSat'].value,
          close: this.RestaurantInfo.controls['closeSat'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenSat'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseSat'].value

        },
        {
          day: 'sunday',
          open: this.RestaurantInfo.controls['openSun'].value,
          close: this.RestaurantInfo.controls['closeSun'].value,
          addhrOpen: this.RestaurantInfo.controls['extraopenSun'].value,
          addhrClose: this.RestaurantInfo.controls['extracloseSun'].value

        }
      ]
    }

    console.log('update value resto', Restobj);

    this.restServ.updateRestoInfo(this.restoid, {restoInfo: Restobj, userid: this.userid}).subscribe(res => {
      console.log('response', res);
      if (res['sucess']) {
        Swal.fire('Restaurant Updated Successfully!', '', 'success')
        this.restServ.getAllRestaurant().subscribe(res => {
          console.log('res', res);

        })

        this.restServ.current_restoid= this.restoid
        this.router.navigate(['/restaurants/view-restaurant'])
      }
      else {
        Swal.fire('Failed to Updated!', '', 'warning')
        this.restServ.getAllRestaurant().subscribe(res => {
          console.log('res', res);

        })

        this.router.navigate(['/restaurants/view-restaurant'])
      }

    })

  }

  // ***************************ESTABLISHMENT FUNCTIONS**********************

  onEstNext(next) {
    this.dummy = next
  }


  //   // *******GET ESTABLISHMENT ROOT TYPE*********
  getEstRootType() {
    this.restServ.getRootCategory().subscribe(data => {
      this.estRootCategory = data['data']
      console.log('estRootCategory', this.estRootCategory);

    })
  }

  // ****** click carrot function**********

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  // On Remove MoreInfo 
  removeMoreInfo(s) {
    if (this.selectedMoreInfos.includes(s)) {
      this.selectedMoreInfos.splice(this.selectedMoreInfos.indexOf(s), 1)
      this.dupChkMoreInfo.splice(this.dupChkMoreInfo.indexOf(s.moreinfo), 1)
    }
  }

  // ON MOREINFO chips
  onCheckMoreInfo(a, b) {
    if (!this.dupChkMoreInfo.includes(b)) {
      this.dupChkMoreInfo.push(b)
      this.selectedMoreInfos.push({ title: a, moreinfo: b })
      console.log('selected moreinfos', this.selectedMoreInfos)
    }
    else {
      Swal.fire('Already Added','','info')
    }

  }

  // ADD MORE INFO CHIPS
  addChipMI(item) {

    console.log('more info item', item);


    const dialogRef = this.matDialog.open(AddChipMoreinfoComponent, {
      width: 'auto',
      data: item
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('more info result', result);
      this.restServ.getAllMoreInfo().subscribe(data => {

        this.barMoreInfo = data['data']
        console.log('more infos form db: ', this.barMoreInfo);

      })

    })

  }

  // on Add More Info open dialog

  openAddMoreInfo() {
    const dialogRef = this.matDialog.open(AddMoreInfoComponent, {
      width: '540px',
      // data: this.allMoreInfos
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result of moreInfo', result);
      this.getAllMoreInfos();
    })
  }

  // get all morinfos
  getAllMoreInfos() {
    this.restServ.getAllMoreInfos().subscribe(data => {
      console.log('get all more infos for chips data', data);
      this.allMoreInfos = data['data']
    })
  }

  // save more info
  onSaveMoreInfo() {

    console.log('selected more info', this.selectedMoreInfos)
    // this.OutletId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.updateMoreInfo(this.restoid, {more_info:this.selectedMoreInfos, userid: this.userid}).subscribe(res => {
      console.log('Response more Infos to db', res);
      if (res['sucess']) {
        Swal.fire('More Infos Updated Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }
    })


  }

  // ON SAVE SERVICES
  onSaveServices() {

    console.log("resto id", this.restoid)

    console.log("services array", this.selectedServices)
    // this.OutletId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.updateServices(this.restoid, {services:this.selectedServices, userid: this.userid}).subscribe(res => {
      console.log('Services saved to db status', res);
      if (res['sucess']) {
        Swal.fire('Services Updated Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
      }

    })



  }


  // on REMOVE MORE INFO SERVICES

  removeServices(s) {
    if (this.selectedServices.includes(s)) {
      this.selectedServices.splice(this.selectedServices.indexOf(s), 1)
      this.dupChkServ.splice(this.dupChkServ.indexOf(s.service), 1)
    }

  }

  // on get all SERVICES ALL CHIPS
  getAllServicesChips() {
    this.restServ.getAllServicesInfo().subscribe(data => {
      this.serviceMoreInfo = data['data']
      console.log('services from db', this.serviceMoreInfo);

    })
  }


  // on CHECK SERVICES

  onCheckService(a, b) {


    if (!this.dupChkServ.includes(b)) {
      this.dupChkServ.push(b)
      this.selectedServices.push({ title: a, service: b })
      console.log('selecected services', this.selectedServices)
    }
    else {
      Swal.fire('Already Added','','info')
    }



  }

  //ADD SERVICE CHIPS
  addChipSV(a) {
    const dialogRef = this.matDialog.open(AddChipServicesComponent, {
      width: 'auto',
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('services result', result);

    })

  }

  // on Add Services open dialog
  openAddServicesChips() {
    const dialogRef = this.matDialog.open(AddServicesChipsComponent, {
      width: '540px',
      // data: this.serviceMoreInfo
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllServicesChips()
    })
  }

  //ON SAVE RULES & REGULATIONS
  onSaveRules() {


    // this.OutletId = '5e3c2bc9a588df0cb1f6d323'
    this.restServ.updateRules(this.restoid, {rulesandregulations:this.selectedRulesInfo,userid: this.userid}).subscribe(res => {
      console.log('resposne save rules to resto', res);
      if (res['sucess']) {
        Swal.fire('Rules Updated Successfully', '', 'success')
      }
      else {
        Swal.fire(
          'Failed to add', '', 'warning'
        )
      }

    })


  }

  //on Check Rules more info
  onCheckRules(a, b) {

    if (!this.dupChkRules.includes(b)) {
      this.dupChkRules.push(b)
      this.selectedRulesInfo.push({ title: a, rule: b })
      console.log('selecected Rules', this.selectedRulesInfo)
    }
    else {
      Swal.fire('Already Added','','info')
    }
  }


  // on Remove RUles from More Info
  removeRules(s) {
    if (this.selectedRulesInfo.includes(s)) {
      this.selectedRulesInfo.splice(this.selectedRulesInfo.indexOf(s), 1)
      this.dupChkRules.splice(this.dupChkRules.indexOf(s.rule), 1)
    }

    console.log('selected RULES', this.selectedRulesInfo)
  }

  // ADD MORE CHIPS RULES
  addChipRL(a) {
    console.log('a', a);

    const dialogRef = this.matDialog.open(AddChipRulesComponent, {
      width: 'auto',
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('rules result', result);

    })

  }

  // on ADD rules open dialog
  openAddRulesChips() {
    const dialogRef = this.matDialog.open(AddRulesChipsComponent, {
      width: '540px',
      // data: this.rulesMoreInfo
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllRulesChips()
    })
  }

  // Geta all rules all chips
  getAllRulesChips() {

    this.restServ.getAllRulesInfo().subscribe(data => {
      this.rulesMoreInfo = data['data']
      // console.log('rules from db', data);

    })


  }

  /// remove AMABIEANCE MORE INFO
  removeAmbience(s) {
    // if (this.selectedAmbience.includes(s)) {
    //   this.selectedAmbience.splice(this.selectedAmbience.indexOf(s), 1)
    //   this.ambienceMoreInfo.push(s);
    // }
    if (this.selectedAmbienceInfo.includes(s)) {
      this.selectedAmbienceInfo.splice(this.selectedAmbienceInfo.indexOf(s), 1)
      this.dupChkAmbience.splice(this.dupChkAmbience.indexOf(s.ambiences), 1)
    }

    console.log('selected RULES', this.selectedAmbienceInfo)
  }

  // CHECK AMBIENCE MORE INFO

  onCheckAmbience(a, b) {


    if (!this.dupChkAmbience.includes(b)) {
      this.dupChkAmbience.push(b)
      this.selectedAmbienceInfo.push({ title: a, ambiences: b })
      console.log('selecected Ambiences', this.selectedAmbienceInfo)
    }
    else {
      Swal.fire('Already Added','','info')
    }
  }

  // /ADD MORE CHIPS AMBIENCES
  addChipAM(a) {

    const dialogRef = this.matDialog.open(AddChipAmbiencesComponent, {
      width: 'auto',
      data: a
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('AMbience result', result);

    })
  }

  // on Add ambience open dialog
  openAddAmbienceChips() {
    const dialogRef = this.matDialog.open(AddAmbienceChipsComponent, {
      width: '540px',
      // data: this.ambienceMoreInfo
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      this.getAllAmbienceChips()
    })
  }

  // get all ambience all chips
  getAllAmbienceChips() {

    this.restServ.getAllAmbienceInfo().subscribe(data => {
      this.ambienceMoreInfo = data['data']
      console.log('ambience from db', this.ambienceMoreInfo);

    })
  }

  //ON SAVE AMBIENCES
  onSaveAmbience() {

    // this.OutletId = '5e3c2bc9a588df0cb1f6d323' 
    console.log('ambience ifno', this.selectedAmbienceInfo)
    this.restServ.updateAmbiences(this.restoid, {ambiences:this.selectedAmbienceInfo,userid: this.userid}).subscribe(res => {
      console.log('response after adding ambiences', res);
      if (res['sucess']) {
        Swal.fire('Ambiences Updated Successfully', '', 'success')
        this.restServ.current_restoid = this.restoid
          this.router.navigate(["/restaurants/view-restaurant"])
      }
      else {
        Swal.fire('Failed to Add', '', 'warning')
        this.restServ.current_restoid = this.restoid
          this.router.navigate(["/restaurants/view-restaurant"])
      }

    })


  }

  //PATCHVALUE ESTABLISHMENT
  patchValueEst() {


    this.selectedRootNames = this.allestdb.estCategories.rootCategories
    this.selectedCatNames = this.allestdb.estCategories.categories
    this.selectedChildName = this.allestdb.estCategories.childCategories
    this.selectedChildChildName = this.allestdb.estCategories.childchildCategories



    // patch more infos
    this.selectedMoreInfos = this.allestdb.more_info
    this.allestdb.more_info.forEach(element => {
      this.dupChkMoreInfo.push(element.moreinfo)
    });
    // this.dupChkMoreInfo = this.allestdb.more_info

    // patch servcies
    this.selectedServices = this.allestdb.services
    this.allestdb.services.forEach(element => {
      this.dupChkServ.push(element.service)
    });
    // this.dupChkServ = this.allestdb.services

    // patch ruels
    this.selectedRulesInfo = this.allestdb.rulesandregulations
    this.allestdb.rulesandregulations.forEach(element => {
      this.dupChkRules.push(element.rule)
    });
    // this.dupChkRules = this.allestdb.rulesandregulations

    //patch ambience
    this.selectedAmbienceInfo = this.allestdb.ambiences
    this.allestdb.ambiences.forEach(element => {
      this.dupChkAmbience.push(element.ambiences)
    });
    // this.dupChkAmbience = this.allestdb.ambiences
  }


  // On Select ROOT

  onSelectRoot(root, evt) {
    // console.log("root", root._id, evt);


    console.log('log', root.rootCategoryName);

    if (!this.selectedRootNames.includes(root.rootCategoryName)) {
      this.selectedRootNames.push(root.rootCategoryName)
    }
    else {
      this.selectedRootNames.splice(this.selectedRootNames.indexOf(root.rootCategoryName), 1)
    }

    console.log('Selected Root Names', this.selectedRootNames);
  }

  // on Select Category()
  onSelectCategory(root, cat) {
    console.log("root", root._id)
    console.log("cat", cat._id);


    if (!this.selectedCatNames.includes(cat.categoryName)) {
      this.selectedCatNames.push(cat.categoryName)
    }
    else {
      this.selectedCatNames.splice(this.selectedCatNames.indexOf(cat.categoryName, 1))
    }

    console.log('Selected Categories Names', this.selectedCatNames);


  }

  // On Select Child  
  onSelectChildCat(root, cat, child) {
    console.log("root", root._id)
    console.log("cat", cat._id);
    console.log("child", child._id);

    if (!this.selectedChildName.includes(child.childCategoryName)) {
      this.selectedChildName.push(child.childCategoryName)
    }
    else {
      this.selectedChildName.splice(this.selectedChildName.indexOf(child.childCategoryName), 1)
    }

    console.log('selected Childs names', this.selectedChildName)

  }


  // On Select Child Child
  onSelectChildChild(root, cat, child, cc) {
    console.log("root", root._id)
    console.log("cat", cat._id);
    console.log("child", child._id);
    console.log("cc", cc._id);

    if (!this.selectedChildChildName.includes(cc.childChildCategoryName)) {
      this.selectedChildChildName.push(cc.childChildCategoryName)
    }
    else {
      this.selectedChildChildName.splice(this.selectedChildChildName.indexOf(cc.childChildCategoryName), 1)

    }

    console.log('selected child child names', this.selectedChildChildName);


  }

  //ON SAVE ESTABLSIMENT TYPE
  onSaveEstType() {
    var estCategories = {
      rootCategories: this.selectedRootNames,
      categories: this.selectedCatNames,
      childCategories: this.selectedChildName,
      childchildCategories: this.selectedChildChildName
    }



    console.log('estCategories schema', estCategories);






    this.restServ.updateEStCategory(this.restoid, {estCategories: estCategories, userid: this.userid}).subscribe(response => {
      console.log('establisment response', response);

      if (response['sucess']) {
        Swal.fire('Establishments Updated Successfully', '', 'success')
      }
      else {
        Swal.fire('Failed to Update', '', 'warning')
      }
    })



  }



  // open dialog add root category code

  openAddCateg(): void {
    const dialogRef = this.matDialog.open(EstAddCatComponent, {
      width: 'auto',

    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.getEstRootType()
    });


  }

  // open category add category name

  openCategoryName(root) {

    console.log('current root', root);
    let root_id = root._id

    const dialogRef = this.matDialog.open(AddCatoNameComponent, {
      width: 'auto',
      data: root_id
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('category name closed', result);
      this.getEstRootType()

    })

  }

  // AddSubCatComponent, AddSubSubCatComponent

  // open child category 
  openSubCategory(root, cat) {
    console.log('root details', root._id);

    console.log('category details', cat._id);

    const dialogRef = this.matDialog.open(AddSubCatComponent, {
      width: '540px',
      data: { 'root': root._id, "cat": cat._id }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.getEstRootType()
    })
  }


  // open child child category 
  openSubSubCategory(root, child) {

    // console.log('root', root._id);
    // console.log('child', child._id)

    const dialogRef = this.matDialog.open(AddSubSubCatComponent, {
      width: '540px',
      data: { 'root': root._id, 'child': child._id }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.getEstRootType()
    })
  }

  // ***************************ESTABLISHMENT FUNCTIONS**********************
  // ***********************************************************************************

  // ***************************LEGAL FORM FUNCTIONS********************************
  patchValueLegal() {

    this.LegalForm.patchValue({


      // Bank Details Form Control Names
      restaurant_account_no: this.allLegalDb.accountNo,
      restaurant_account_type: this.allLegalDb.accountType,
      restaurant_bank_name: this.allLegalDb.bankName,
      restaurant_branch_name: this.allLegalDb.branchName,
      restaurant_bank_city: this.allLegalDb.city,
      restaurant_bank_address: this.allLegalDb.address,
      restaurant_bank_ifsccode: this.allLegalDb.IFSCCode,
      restaurant_selected_docs: this.allLegalDb.selected_docs,
      restaurant_gst_no: this.allLegalDb.documents[0].documentNumber,
      // restaurant_gst_file: this.allLegalDb.documents[0].documentUrl,
      restaurant_pan_no: this.allLegalDb.documents[1].documentNumber,
      // restaurant_pan_image: this.allLegalDb.documents[1].documentNumber,
      restaurant_fssai_certificate: this.allLegalDb.documents[2].documentNumber,
      // restaurant_fssai_image: this.allLegalDb.documents[2].documentNumber,
      restaurant_noc_certificate: this.allLegalDb.documents[3].documentNumber,
      // restaurant_noc_certificate_file: this.allLegalDb.documents[3].documentNumber,
      restaurant_liqLic: this.allLegalDb.documents[4].documentNumber,
      // restaurant_liqLic_file: this.allLegalDb.documents[4].documentNumber,
      restauant_eatLic: this.allLegalDb.documents[5].documentNumber,
      // restauant_eatLic_file: this.allLegalDb.documents[5].documentNumber, 
      restaurant_musicLic: this.allLegalDb.documents[6].documentNumber,
      // restaurant_musicLic_file: this.allLegalDb.documents[6].documentNumber,
      restaurant_tradeLic: this.allLegalDb.documents[7].documentNumber,
      // restaurant_tradeLic_file:this.allLegalDb.documents[7].documentNumber,
      restaurant_cec: this.allLegalDb.documents[8].documentNumber,
      // restaurant_cec_file: this.allLegalDb.documents[8].documentNumber,
      restaurant_shopAct: this.allLegalDb.documents[9].documentNumber,
      // restaurant_shopAct_file: this.allLegalDb.documents[9].documentNumber,
      restaurant_sinageLic: this.allLegalDb.documents[10].documentNumber,
      // restaurant_sinageLic_file: this.allLegalDb.documents[10].documentNumber,
      restaurant_liftClearance: this.allLegalDb.documents[11].documentNumber,
      // restaurant_liftClearance_file: this.allLegalDb.documents[11].documentNumber,
      restuarant_healthLic: this.allLegalDb.documents[12].documentNumber,
      // restuarant_healthLic_file: this.allLegalDb.documents[12].documentNumber,
   //   restaurant_legal_document: this.fb.array([])
    })

    this.gstName = this.LegalForm.controls.restaurant_gst_no.value,
   this.gstResponse = this.allLegalDb.documents[0].documentUrl,
    this.panName= this.LegalForm.controls.restaurant_pan_no.value,
   this.panResponse=this.allLegalDb.documents[1].documentUrl,
   this.fssaiName = this.LegalForm.controls.restaurant_fssai_certificate.value,
this.fssaiResponse = this.allLegalDb.documents[2].documentUrl,
   this.nocName = this.LegalForm.controls.restaurant_noc_certificate.value,
   this.nocResponse = this.allLegalDb.documents[3].documentUrl,
    this.LiqLicName= this.LegalForm.controls.restaurant_liqLic.value,
    this.liquorLic = this.allLegalDb.documents[4].documentUrl,
    this.eatLicName= this.LegalForm.controls.restauant_eatLic.value,
    this.eatingHouseResponse= this.allLegalDb.documents[5].documentUrl, 
    this.musicLicName= this.LegalForm.controls.restaurant_musicLic.value,
    this.musicLicResponse= this.allLegalDb.documents[6].documentUrl,
    this.trademarkName= this.LegalForm.controls.restaurant_tradeLic.value,
    this.trademarkResponse=this.allLegalDb.documents[7].documentUrl,
    this.cedName= this.LegalForm.controls.restaurant_cec.value,
   this.cecResponse= this.allLegalDb.documents[8].documentUrl,
    this.shopActName= this.LegalForm.controls.restaurant_shopAct.value,
    this.shopActResponse= this.allLegalDb.documents[9].documentUrl,
    this.sinageName= this.LegalForm.controls.restaurant_sinageLic.value,
    this.sinageResponse= this.allLegalDb.documents[10].documentUrl,
    this.liftLicName= this.LegalForm.controls.restaurant_liftClearance.value,
    this.LiftResponse= this.allLegalDb.documents[11].documentUrl,
    this.healthLicName= this.LegalForm.controls.restuarant_healthLic.value,
    this.healthResponse= this.allLegalDb.documents[12].documentUrl,

    this.selectedDocs = this.allLegalDb.selected_docs
    this.LegalForm.controls.restaurant_selected_docs.setValue(this.selectedDocs)

    this.otherDocs = this.allLegalDb.otherDocuments
   console.log('otherDocs value', this.otherDocs)


    if(this.selectedDocs.includes('GST')){

    
    this.gstStatus = true
    }
    if(this.selectedDocs.includes('PAN Card')){
      
    
    this.panStatus = true
    }
    if(this.selectedDocs.includes('FSSAI Certificate')){
      
    
    this.fssaiStatus = true
    }
    if(this.selectedDocs.includes('NOC')){
      
    
    this.nocStatus = true
    }
    if(this.selectedDocs.includes('Liquor License')){
      
    
    this.LiqLicStatus = true
    }
    if(this.selectedDocs.includes('Eating House License')){
      
    
    this.eatLicStatus = true
    }
    if(this.selectedDocs.includes('Music license')){
      
    
    this.musicLicStatus = true
    }
    if(this.selectedDocs.includes('Trademark for Cafe License')){
      
    
    this.trademarkStatus = true
    }
    if(this.selectedDocs.includes('Certificate of Environmental Clearance (CEC)')){
      
    
    this.cedStatus = true
    }
    if(this.selectedDocs.includes('Shops and Establishment Act')){
      
    
    this.shopActStatus = true
    }
    if(this.selectedDocs.includes('Signage license')){
      
    
    this.sinageStatus = true
    }
    if(this.selectedDocs.includes('Lift Clearance')){
      
    
    this.liftLicStatus = true
    }
    if(this.selectedDocs.includes('Health/Trade License')){
      
    this.healthLicStatus = true
    }


    this.docsCount = this.allLegalDb.otherDocuments.length
    console.log(this.allLegalDb.otherDocuments)

  }


  // on select drop down legal docs
  onItemSelect(item: any) {
    console.log(item);

    this.selectedDocs = this.LegalForm.controls['restaurant_selected_docs'].value
    console.log(this.selectedDocs)
    console.log(this.LegalForm.controls['restaurant_selected_docs'].value)
  }

  onSelectAll(items: any) {
    console.log(items);
    this.selectedDocs = items
    console.log(this.selectedDocs);
    console.log(this.LegalForm.controls['restaurant_selected_docs'].value)
  }
  onDeSelect(item: any) {
    console.log('deselect', item);

    this.selectedDocs.splice(this.selectedDocs.indexOf(item), 1)

    // GST
    if(item == 'GST'){
     
        this.LegalForm.controls.restaurant_gst_no.reset()
        this.LegalForm.controls.restaurant_gst_file.reset()
        this.allLegalDb.documents[0].documentUrl=""
    
    }
   
  
     // 'PAN Card'
     if(item == 'PAN Card'){
     
       
        this.LegalForm.controls. restaurant_pan_no.reset()
        this.LegalForm.controls.restaurant_pan_image.reset()
        this.allLegalDb.documents[1].documentUrl=""
      
    }

     // 'FSSAI Certificate'
     if(item == 'FSSAI Certificate'){
     
      
        this.LegalForm.controls.restaurant_fssai_certificate.reset()
        this.LegalForm.controls.restaurant_fssai_image.reset()
    
        this.allLegalDb.documents[2].documentUrl=""
    }

     // NOC
    
     if(item == 'NOC'){
       
        this.LegalForm.controls.restaurant_noc_certificate.reset()
        this.LegalForm.controls.restaurant_noc_certificate_file.reset()
        this.allLegalDb.documents[3].documentUrl=""
     
    }

     // Liquor License
     if(item == 'Liquor License'){
     
       
        this.LegalForm.controls.restaurant_liqLic.reset()
        this.LegalForm.controls.restaurant_liqLic_file.reset()
       
        this.allLegalDb.documents[4].documentUrl=""
    }

     // Eating House License
     if(item == 'Eating House License'){
     
       
        this.LegalForm.controls.restauant_eatLic.reset()
        this.LegalForm.controls.restauant_eatLic_file.reset()
        this.allLegalDb.documents[5].documentUrl=""
    }
     //Music license
     if(item == 'Music license'){
      
        this.LegalForm.controls.restaurant_musicLic.reset()
        this.LegalForm.controls.restaurant_musicLic_file.reset()
        this.allLegalDb.documents[6].documentUrl=""
      
    }
     // Trademark for Cafe License
     if(item == 'Trademark for Cafe License'){
     
        this.LegalForm.controls.restaurant_tradeLic.reset()
        this.LegalForm.controls.restaurant_tradeLic_file.reset()
      
        this.allLegalDb.documents[7].documentUrl=""
    }
     // Certificate of Environmental Clearance (CEC)
     if(item == 'Certificate of Environmental Clearance (CEC)'){
    
      
        this.LegalForm.controls.restaurant_cec.reset()
        this.LegalForm.controls.restaurant_cec_file.reset()
       
        this.allLegalDb.documents[8].documentUrl=""
    }
     // Shops and Establishment Act
     if(item == 'Shops and Establishment Act'){
      
        this.LegalForm.controls.restaurant_shopAct.reset()
        this.LegalForm.controls.restaurant_shopAct_file.reset()
      
        this.allLegalDb.documents[9].documentUrl=""
    }
     // Signage license
     if(item == 'Signage license'){
    
       
        this.LegalForm.controls.restaurant_sinageLic.reset()
        this.LegalForm.controls.restaurant_sinageLic_file.reset()
        this.allLegalDb.documents[10].documentUrl=""
     
    }
     // Lift Clearance
     if(item == 'Lift Clearance'){
     
     
       
        this.LegalForm.controls.restaurant_liftClearance.reset()
        this.LegalForm.controls.restaurant_liftClearance_file.reset()
        this.allLegalDb.documents[11].documentUrl=""
    }
     // Health/Trade License
     if(item == 'Health/Trade License'){
     
      
        this.LegalForm.controls.restuarant_healthLic.reset()
        this.LegalForm.controls.restuarant_healthLic_file.reset()
        this.allLegalDb.documents[12].documentUrl=""
    }

    console.log(this.selectedDocs);
    console.log(this.LegalForm.controls['restaurant_selected_docs'].value)
  }

  onDeSelectAll(items: any) {
    console.log('deselect all', items);
    this.selectedDocs = []
    console.log(this.selectedDocs);

   
      this.LegalForm.controls.restaurant_gst_no.reset()
      this.LegalForm.controls.restaurant_gst_file.reset()
      this.LegalForm.controls.restaurant_pan_no.reset()
      this.LegalForm.controls.restaurant_pan_image.reset()
      this.LegalForm.controls.restaurant_fssai_certificate.reset()
      this.LegalForm.controls.restaurant_fssai_image.reset()
      this.LegalForm.controls.restaurant_noc_certificate.reset()
      this.LegalForm.controls.restaurant_noc_certificate_file.reset()
      this.LegalForm.controls.restaurant_liqLic.reset()
      this.LegalForm.controls.restaurant_liqLic_file.reset()
      this.LegalForm.controls.restauant_eatLic.reset()
      this.LegalForm.controls.restauant_eatLic_file.reset()
      this.LegalForm.controls.restaurant_musicLic.reset()
      this.LegalForm.controls.restaurant_musicLic_file.reset()
      this.LegalForm.controls.restaurant_tradeLic.reset()
      this.LegalForm.controls.restaurant_tradeLic_file.reset()
      this.LegalForm.controls.restaurant_cec.reset()
      this.LegalForm.controls.restaurant_cec_file.reset()
      this.LegalForm.controls.restaurant_shopAct.reset()
      this.LegalForm.controls.restaurant_shopAct_file.reset()
      this.LegalForm.controls.restaurant_sinageLic.reset()
      this.LegalForm.controls.restaurant_sinageLic_file.reset()
      this.LegalForm.controls.restaurant_liftClearance.reset()
      this.LegalForm.controls.restaurant_liftClearance_file.reset()
      this.LegalForm.controls.restuarant_healthLic.reset()
      this.LegalForm.controls.restuarant_healthLic_file.reset()

      this.allLegalDb.documents[0].documentUrl=""
      this.allLegalDb.documents[1].documentUrl=""
      this.allLegalDb.documents[2].documentUrl=""
      this.allLegalDb.documents[3].documentUrl=""
      this.allLegalDb.documents[4].documentUrl=""
      this.allLegalDb.documents[5].documentUrl=""
      this.allLegalDb.documents[6].documentUrl=""
      this.allLegalDb.documents[7].documentUrl=""
      this.allLegalDb.documents[8].documentUrl=""
      this.allLegalDb.documents[9].documentUrl=""
      this.allLegalDb.documents[10].documentUrl=""
      this.allLegalDb.documents[11].documentUrl=""
      this.allLegalDb.documents[12].documentUrl=""
      
    

  }

  onUpdateLegal() {
    console.log('legal form ', this.LegalForm.value);

    console.log('update add form',this.LegalForm.controls.restaurant_legal_document.value)

    console.log('stored data', this.allLegalDb.otherDocuments)

    // this.allLegalDb.otherDocuments[this.docsCount-1]=this.LegalForm.controls.restaurant_legal_document.value

    console.log('merge data', this.allLegalDb.otherDocuments.concat(this.LegalForm.controls.restaurant_legal_document.value));
    this.allLegalDb.otherDocuments = this.allLegalDb.otherDocuments.concat(this.LegalForm.controls.restaurant_legal_document.value)
    console.log('after merger', this.allLegalDb.otherDocuments);
    

    var legalObj = {
      "accountNo": this.LegalForm.controls['restaurant_account_no'].value,
      "accountType": this.LegalForm.controls['restaurant_account_type'].value,
      "bankName": this.LegalForm.controls['restaurant_bank_name'].value,
      "branchName": this.LegalForm.controls['restaurant_branch_name'].value,
      "city": this.LegalForm.controls['restaurant_bank_city'].value,
      "address": this.LegalForm.controls['restaurant_bank_address'].value,
      "IFSCCode": this.LegalForm.controls['restaurant_bank_ifsccode'].value,
      "selected_docs": this.LegalForm.controls['restaurant_selected_docs'].value,
      "documents": [{
        "documentName": "GST",
        "documentNumber": this.LegalForm.controls['restaurant_gst_no'].value,
        "documentUrl": this.allLegalDb.documents[0].documentUrl
      },
      {
        "documentName": "PAN Card",
        "documentNumber": this.LegalForm.controls['restaurant_pan_no'].value,
        "documentUrl": this.allLegalDb.documents[1].documentUrl
      },
      {
        "documentName": "FSSAI Certificate",
        "documentNumber": this.LegalForm.controls['restaurant_fssai_certificate'].value,
        "documentUrl": this.allLegalDb.documents[2].documentUrl
      },
      {
        "documentName": "NOC",
        "documentNumber": this.LegalForm.controls['restaurant_noc_certificate'].value,
        "documentUrl": this.allLegalDb.documents[3].documentUrl
      },
      {
        "documentName": "Liquor License",
        "documentNumber": this.LegalForm.controls['restaurant_liqLic'].value,
        "documentUrl": this.allLegalDb.documents[4].documentUrl
      },
      {
        "documentName": "Eating House License",
        "documentNumber": this.LegalForm.controls['restauant_eatLic'].value,
        "documentUrl": this.allLegalDb.documents[5].documentUrl
      },
      {
        "documentName": "Music license",
        "documentNumber": this.LegalForm.controls['restaurant_musicLic'].value,
        "documentUrl": this.allLegalDb.documents[6].documentUrl
      },
      {
        "documentName": "Trademark for Cafe License",
        "documentNumber": this.LegalForm.controls['restaurant_tradeLic'].value,
        "documentUrl": this.allLegalDb.documents[7].documentUrl
      },
      {
        "documentName": "Certificate of Environmental Clearance (CEC)",
        "documentNumber": this.LegalForm.controls['restaurant_cec'].value,
        "documentUrl": this.allLegalDb.documents[8].documentUrl
      },
      {
        "documentName": "Shops and Establishment Act",
        "documentNumber": this.LegalForm.controls['restaurant_shopAct'].value,
        "documentUrl": this.allLegalDb.documents[9].documentUrl
      },
      {
        "documentName": "Signage license",
        "documentNumber": this.LegalForm.controls['restaurant_sinageLic'].value,
        "documentUrl": this.allLegalDb.documents[10].documentUrl
      },
      {
        "documentName": "Lift Clearance",
        "documentNumber": this.LegalForm.controls['restaurant_liftClearance'].value,
        "documentUrl": this.allLegalDb.documents[11].documentUrl
      },
      {
        "documentName": "Health/Trade License",
        "documentNumber": this.LegalForm.controls['restuarant_healthLic'].value,
        "documentUrl": this.allLegalDb.documents[12].documentUrl
      }],
      "otherDocuments": this.allLegalDb.otherDocuments
    }

    this.restServ.updateLegal(this.restoid, {legalInfo:legalObj, userid: this.userid}).subscribe(res => {
      console.log("response", res);
      if (res['sucess']) {
        Swal.fire('Updated Successfully', '', 'success')
        this.restServ.current_restoid = this.restoid
        this.router.navigate(["/restaurants/view-restaurant"])
      }
      else {
        Swal.fire('Failed to Update')
      }

    })

  }

  // Delete from Other Doc
  deleteOtherDoc(idx){
    this.allLegalDb.otherDocuments.splice(idx,1)
    this.docsCount = this.docsCount-1
  }


  // }
  //get FORM ARRAY OF DOCUMENTS NAME

  get docFormArray() {
    return this.LegalForm.controls.restaurant_legal_document as FormArray;
  }

  // adding form to it

  addDocArray() {
    ++this.docsCount
    this.docs = this.fb.group({
      documentName: [''],
      documentNumber: [''],
      documentUrl: ['']
    })
    //  console.log(this.docs.value);

    if (this.docsCount <= 4) {
      this.docFormArray.push(this.docs)
      this.docStatus.push({isDoc:false})
      this.docAble = true
    }
    else {
      Swal.fire('You have already Added 4 Docs', '', 'warning')
    }

  }



  // Remove Docs

  onRemoveExtraLegal(docs,i) {
    this.docFormArray.controls.splice(this.docFormArray.controls.indexOf(docs), 1)
    this.docStatus.splice(i,1)
    this.otherResponse.splice(i,1)
    this.docsCount= this.docsCount-1
    this.docAble = false
  }


  //  Remove OTHER IMAGE

  removeother() {
    this.otherResponse = []
    this.isOtherTrash = true
  }
  // Remove GST IMAGE
  removegst() {
    this.gstResponse = ''
    this.isgstTrash = true
    this.gstStatus = false
    // this.LegalForm.controls['restaurant_gst_no'].reset()
  }

  // Remove PAN IMAGE
  removePan() {
    this.panResponse = ''
    this.ispanTrash = true
    this.panStatus = false
    // this.LegalForm.controls['restaurant_pan_no'].reset()
  }

  // Remove Fssai Image
  removeFssai() {
    this.fssaiResponse = ''
    this.isFssaiTrash = true
    this.fssaiStatus = false
    // this.LegalForm.controls['restaurant_fssai_certificate'].reset()
  }

  // Remove NOC from Form
  removeNoc() {
    this.nocResponse = ''
    this.nocStatus = false
    // this.LegalForm.controls['restaurant_noc_certificate'].reset()
    this.LegalForm.controls['restaurant_noc_certificate_file'].reset()
  }

  // Remove Liquor License 

  removeLiqourLic() {
    this.liquorLic = ''
    this.LiqLicStatus = false
    // this.LegalForm.controls['restaurant_liqLic'].reset()
    this.LegalForm.controls['restaurant_liqLic_file'].reset()
  }

  // Remove EATING LICENSE
  removeEatLic() {
    this.eatingHouseResponse = ''
    this.eatLicStatus = false
    // this.LegalForm.controls['restauant_eatLic'].reset()
    this.LegalForm.controls['restauant_eatLic_file'].reset()
  }

  // Remove Music Liecense
  removeMusicLic() {
    this.musicLicResponse = ''
    this.musicLicStatus = false
    // this.LegalForm.controls['restaurant_musicLic'].reset()
    this.LegalForm.controls['restaurant_musicLic_file'].reset()
  }

  //REMOVE TRADE LICENSE
  removeTradeLic() {
    this.trademarkResponse = ''
    this.trademarkStatus = false
    // this.LegalForm.controls['restaurant_tradeLic'].reset()
    this.LegalForm.controls['restaurant_tradeLic_file'].reset()
  }

  // Remove CEC ENVIROMENT LICENSE
  removeCEC() {
    this.cecResponse = ''
    this.cedStatus = false
    // this.LegalForm.controls['restaurant_cec'].reset()
    this.LegalForm.controls['restaurant_cec_file'].reset()
  }

  // Remove Shop Act
  removeShopAct() {
    this.shopActResponse = ''
    this.shopActStatus = false
    // this.LegalForm.controls['restaurant_shopAct'].reset()
    this.LegalForm.controls['restaurant_shopAct_file'].reset()
  }

  // Remove Signage License
  removeSignageLic() {
    this.sinageResponse = ''
    this.sinageStatus = false
    // this.LegalForm.controls['restaurant_sinageLic'].reset()
    this.LegalForm.controls['restaurant_sinageLic_file'].reset()
  }
  // Remove Lift License
  removeLiftLic() {
    this.LiftResponse = ''
    this.liftLicStatus = false
    // this.LegalForm.controls['restaurant_liftClearance'].reset()
    this.LegalForm.controls['restaurant_liftClearance_file'].reset()
  }

  // Remove Health/Trade License
  removeHealthLic() {
    this.healthResponse = ''
    this.healthLicStatus = false
    // this.LegalForm.controls['restuarant_healthLic'].reset()
    this.LegalForm.controls['restuarant_healthLic_file'].reset()
  }


    // ifsc code to upper case
    ifscUpper(){
      let ifsc = this.LegalForm.controls.restaurant_bank_ifsccode.value
      this.LegalForm.patchValue({
        restaurant_bank_ifsccode: ifsc.toUpperCase()
      })
    }

  // Remove MEDI LOGO

  // removeLogo() {
  //   this.logoResponse = null
  //   this.logoHide = true
  // }

  // REMOVE MEDIA BANNER
  // removeBanner(i) {
  //   this.BannerResponse.splice(i, 1)

  //   if (this.BannerResponse.length == 0) {
  //     this.bannerHide = true
  //   }
  // }

  // REMOVE IMAGE ALBUM
  // removeImgAlbum(index, image, obj) {
  //   console.log(this.album_image[index].fileurl.splice(this.album_image[index].fileurl.indexOf(image), 1));



  //   if (this.album_image[index].fileurl.length == 0) {
  //     this.album_image.splice(this.album_image.indexOf(obj), 1)
  //     this.albumNames.push(obj.name)
  //   }

  //   console.log('album', this.album_image);
  // }

  // REMOVE VIDEO ALBUM
  // removeVdoAlbum(index, video, obj) {
  //   console.log(this.album_video[index].fileurl.splice(this.album_video[index].fileurl.indexOf(video), 1));

  //   if (this.album_video[index].fileurl.length == 0) {
  //     this.album_video.splice(this.album_video.indexOf(obj), 1)
  //     this.createdVdoAlbum.push(obj.name)
  //   }

  //   console.log('album vdo', this.album_video);

  // }
  // ***************************LEGAL FORM FUNCTIONS********************************
  // ***********************************************************************************


  // **************************************MEDIA FUNCTIONS***********************************

  // REMOVE MEDIA BANNER
  removeBanner(i) {
    this.BannerResponse.splice(i, 1)

    if (this.BannerResponse.length == 0) {
      this.bannerHide = true
    }
  }


  // banners successfully uploaded
  banSuccessUp() {
    Swal.fire('Banner Successfully Uploaded', '', 'success')
  }

  // loading spinner
  loading() {
    this.ngxSpinner.show()
  }


  // open DIalog media only for images
  openMedialog() {
    const dialogRef = this.matDialog.open(CreateAlbumComponent, {
      width: '540px',
      data: {
        moreinfo: this.selectedMoreInfos,
        service: this.selectedServices,
        ambiences: this.selectedAmbienceInfo
      }
    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (this.mainService.popMediaAddData != undefined) {
        // this.albumNames.push(this.mainService.popMediaAddData);


        this.albumNames = this.mainService.popMediaAddData;



        this.isAlbum = false;
        console.table("ALUBM HERE", this.albumNames)
        this.mainService.selectedAlbums = this.albumNames;
      }


    });



  }



  // open video dialog album
  openVdoAlbum() {
    const dialogRef = this.matDialog.open(CreateVdoAlbumComponent, {
      width: '540px',
      data: {
        moreinfo: this.selectedMoreInfos,
        service: this.selectedServices,
        ambiences: this.selectedAmbienceInfo
      }
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result form videos create album', result);
      // this.createdVdoAlbum.push(result)
      // console.log('created video album', this.createdVdoAlbum);

      if (this.mainService.popVideoAddData != undefined) {
        // this.albumNames.push(this.mainService.popMediaAddData);


        this.createdVdoAlbum = this.mainService.popVideoAddData;

        this.isAlbum = false;
        console.table("ALUBM video HERE", this.createdVdoAlbum)
        this.mainService.selectedVideoAlbums = this.createdVdoAlbum;
      }

    })
  }

  // empty image collection
  emptyImgCollection() {
    this.ImgAlbumResponse = []
    console.log('empty img collection');

  }

  // empty vdo collection for next
  emptyVdoCollection() {
    this.VideoAlbumResponse = []
    console.log('empty video collection')
  }



  // images media json creation

  imgMediaJson(title, index) {
    console.log('index no.', index);
    let realIndex
    this.album_image.forEach(element => {
      if (title == element.name) {
        console.log('real index', this.album_image.indexOf(element));

        realIndex = this.album_image.indexOf(element)

      }
    });


    if (realIndex < this.album_image.length) {
      console.log('album images with index', this.album_image[realIndex]);

      let presentFiles = this.album_image[realIndex].fileurl
      console.log('presentFiles', presentFiles);

      this.album_image[realIndex].fileurl = this.ImgAlbumResponse

      presentFiles.forEach(element => {
        this.album_image[realIndex].fileurl.push(element)

      });


      console.log('AFter push', this.album_image);


    }
    else {
      this.album_image.push({ name: title, fileurl: this.ImgAlbumResponse })

      this.albumNames.splice(this.albumNames.indexOf(title), 1)
      // console.log('albumName',this.albumNames)

    }

    console.log('title', title);
    console.log('collection images', this.ImgAlbumResponse);

    // this.albumNames.splice(this.albumNames.indexOf(title),1)
    // console.log('Albums', this.albumNames);





    console.log('album_image', this.album_image)
    console.log('albumName', this.albumNames);


    // Swal.fire(title + ' Album Successfully Uploaded', '', 'success')
  }




  // REMOVE IMAGE ALBUM
  removeImgAlbum(index, image, obj) {
    console.log(this.album_image[index].fileurl.splice(this.album_image[index].fileurl.indexOf(image), 1));



    if (this.album_image[index].fileurl.length == 0) {
      this.album_image.splice(this.album_image.indexOf(obj), 1)
      this.albumNames.push(obj.name)
    }

    console.log('album', this.album_image);
  }

  // REMOVE VIDEO ALBUM
  removeVdoAlbum(index, video, obj) {
    console.log(this.album_video[index].fileurl.splice(this.album_video[index].fileurl.indexOf(video), 1));

    if (this.album_video[index].fileurl.length == 0) {
      this.album_video.splice(this.album_video.indexOf(obj), 1)
      this.createdVdoAlbum.push(obj.name)
    }

    console.log('album vdo', this.album_video);





  }



  // videos media json creation

  vdoMediaJson(title) {

    let realvdoIndex

    this.album_video.forEach(element => {
      if (title == element.name) {
        console.log('real index', this.album_video.indexOf(element));

        realvdoIndex = this.album_video.indexOf(element)

      }
    });

    if (realvdoIndex < this.album_video.length) {
      console.log('album video with index', this.album_video[realvdoIndex]);

      let presentFiles = this.album_video[realvdoIndex].fileurl
      console.log('presentFiles', presentFiles);

      this.album_video[realvdoIndex].fileurl = this.VideoAlbumResponse

      presentFiles.forEach(element => {
        this.album_video[realvdoIndex].fileurl.push(element)

      });

      console.log('album videos json', this.album_video)

    }
    else {
      this.album_video.push({ name: title, fileurl: this.VideoAlbumResponse })

      this.createdVdoAlbum.splice(this.createdVdoAlbum.indexOf(title), 1)
      // console.log('albumName',this.albumNames)

    }



    console.log('album videos json', this.album_video)

    // Swal.fire(title + ' Album Successfully Uploaded', '', 'success')
  }

  patchMediaValue(){

    this.logoResponse = this.allMediaDb.resto_logo
    this.BannerResponse = this.allMediaDb.resto_banner

    this.bannerHide = false
    this.logoHide = false

  console.log('establishment', this.allestdb);
  this.selectedMoreInfos = this.allestdb.more_info
  this.selectedServices = this.allestdb.services
  this.selectedAmbienceInfo = this.allestdb.ambiences

  console.log('media', this.allMediaDb);


// Media IMAGES ALBUM NAME
let i =0
  this.allMediaDb.album_image.forEach(element => {
    this.albumNames.push(element.name)
    this.emptyImgCollection()
    this.ImgAlbumResponse=element.fileurl
    this.imgMediaJson(element.name,i)
    i = i+1
  });

// Media VIDEO ALBUM NAME

 this.allMediaDb.album_video.forEach(element=>{
   this.createdVdoAlbum.push(element.name)
   this.emptyVdoCollection()
   this.VideoAlbumResponse=element.fileurl
   this.vdoMediaJson(element.name)
   
 })

//  patch create image album

this.mainService.selectedAlbums = this.albumNames;
this.mainService.selectedVideoAlbums = this.createdVdoAlbum;

  

  }

  // Remove media logo
    // Remove MEDI LOGO

    removeLogo() {
      this.logoResponse = null
      this.logoHide = true
    }
  

    // UPDATE MEDIA VALUES
    onUpdateMediaInfo() {

      let mediaObj = {
        resto_logo: this.logoResponse,
        resto_banner: this.BannerResponse,
        album_image: this.album_image,
        album_video: this.album_video
      }
  
     
      console.log('media object ', mediaObj);
      // this.OutletId = '5e3c2bc9a588df0cb1f6d323'
      this.restServ.updateMedia(this.restoid, {media: mediaObj, userid: this.userid}).subscribe(res => {
        console.log('resposne after media uploade', res);
        if (res['sucess']) {
          Swal.fire('Media Updated Successfully', '', 'success')
          this.restServ.current_restoid = this.restoid
          this.router.navigate(["/restaurants/view-restaurant"])
        }
        else {
          Swal.fire('Failed to Update', '', 'warning')
        }
  
      })
  
    }

  // **********************************MENU MGMT FUCNTIONS******************************
  onMenuNext(next) {
    console.log(this.output);
    this.menuNav = next
  }


  // **************************cuisines********
  //ON REMOVE CUISINES

  removeCuisines(s) {
    if (this.selectedCuisines.includes(s)) {
      this.selectedCuisines.splice(this.selectedCuisines.indexOf(s), 1)
      this.selectedCuisinesName.splice(this.selectedCuisinesName.indexOf(s.cuisineName), 1)
      this.allCuisines.push(s)
    }
  }


  //ON CHECK CUISINES 

  onChkCuisines(s) {




    if (this.allCuisines.includes(s)) {
      this.allCuisines.splice(this.allCuisines.indexOf(s), 1)
      this.selectedCuisines.push(s)
      this.selectedCuisinesName.push(s.cuisineName)
    }

    console.log("all cuisines", this.allCuisines);
  }

    // PATCH VLAUE CUISINES FUNCTIONS
    patchCuisines() {

      return new Promise((resolve, reject) => {
        this.selectedCuisines = this.allMenudb.cuisines
        // GET ALL CUISINES FROM DB
        this.restServ.getAllCuisines().subscribe(data => {
          this.allCuisines = data['data']
  
          console.log('All cuisines form DB', this.allCuisines);
  
          this.selectedCuisines.forEach(element => {
            // console.log("element", element)
            this.allCuisines.forEach(ele => {
              if (element.cuisineName == ele.cuisineName) {
                this.allCuisines.splice(this.allCuisines.indexOf(ele), 1)
              }
            });
          })
          resolve(this.allCuisines)
        })
      }).then(id => {
        this.upallCuisines = id
        console.log("id", id);
  
      })
    }

    // ON UPDATE CUISINES
    onUpdateCuisines() {
      console.log('selected ', this.selectedCuisines)
      console.log('selected cuisines', this.selectedCuisinesName)
  
      this.restServ.updateCuisines(this.restoid,{cuisines: this.selectedCuisines, userid: this.userid}).subscribe(res => {
        console.log('response add selected cuisines', res);
  
        if (res['sucess']) {
          Swal.fire('Cuisines Updated Successfully', '', 'success')
          this.restServ.current_restoid = this.restoid
         this.router.navigate(["/restaurants/view-restaurant"])
        }
        else {
          Swal.fire('Failed to Update', '', 'warning')
        }
      })
    }
  

  // ***************************Menu tab functions********

  // patch values MENU FUNCTIONS
  patchMenu() {
    this.selectedMenuRoot = this.allMenudb.menu.rootCategories
    this.selectedMenuCategory = this.allMenudb.menu.categories
    this.selectedChildCategory = this.allMenudb.menu.childCategories
    this.selectedMenucc = this.allMenudb.menu.childchildCategories
  }

  // open add root menu dialog
  openAddRootMenu() {
    const dialogRef = this.matDialog.open(AddRootMenuComponent, {
      width: '420px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      // GET ALL MENU FROM DB
      this.restServ.getAllRootMenu().subscribe(data => {
        this.AllRootMenu = data['data']
        console.log('all root menu', this.AllRootMenu);

      })
    })
  }

  // open add child child menu dialog

  openMenuChildChild(root, child) {


    const dialogRef = this.matDialog.open(AddCcMenuComponent,
      {
        width: "420px",
        data: { root: root._id, child: child._id }
      })
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      // GET ALL MENU FROM DB
      this.restServ.getAllRootMenu().subscribe(data => {
        this.AllRootMenu = data['data']
        console.log('all root menu', this.AllRootMenu);

      })
    })
  }

  // open add sub child menu dialog
  openMenuChild(root, cat) {
    let root_id = root._id
    let cat_id = cat._id
    const dialogRef = this.matDialog.open(AddSubcatMenuComponent,
      {
        width: '420px',
        data: { 'root': root_id, 'cat': cat_id }
      })
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      // GET ALL MENU FROM DB
      this.restServ.getAllRootMenu().subscribe(data => {
        this.AllRootMenu = data['data']
        console.log('all root menu', this.AllRootMenu);

      })
    })
  }

  // open add category menu dialog
  openMenuCate(root) {
    let root_id = root._id
    const dialogRef = this.matDialog.open(AddCateMenuComponent,
      {
        width: '420px',
        data: root_id
      })
    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
      // GET ALL MENU FROM DB
      this.restServ.getAllRootMenu().subscribe(data => {
        this.AllRootMenu = data['data']
        console.log('all root menu', this.AllRootMenu);

      })
    })
  }

  // ON SELECET MENU ROOT
  onSelectMenuRoot(root) {
    console.log('menu root id', root._id);

    if (!this.selectedMenuRoot.includes(root.rootCategoryName)) {
      this.selectedMenuRoot.push(root.rootCategoryName)
    }
    else {
      this.selectedMenuRoot.splice(this.selectedMenuRoot.indexOf(root.rootCategoryName), 1)
    }
    console.log('selected root menu', this.selectedMenuRoot);

  }

  // ON SELECT MENU CATEGORY
  onSelectMenuCat(root, cat) {
    console.log('menu root id', root._id)
    console.log('menu cat._id', cat._id);

    if (!this.selectedMenuCategory.includes(cat.categoryName)) {
      this.selectedMenuCategory.push(cat.categoryName)
    }
    else {
      this.selectedMenuCategory.splice(this.selectedMenuCategory.indexOf(cat.categoryName), 1)
    }

    console.log('Selected Menu Category', this.selectedMenuCategory);

  }

  // ON SELECT MENU CHILD
  onSelectMenuChild(root, cat, child) {
    console.log('menu root id', root._id)
    console.log('menu cat._id', cat._id);
    console.log('menu child._id', child._id);

    if (!this.selectedChildCategory.includes(child.childCategoryName)) {
      this.selectedChildCategory.push(child.childCategoryName)
    }
    else {
      this.selectedChildCategory.splice(this.selectedChildCategory.indexOf(child.childCategoryName), 1)
    }

    console.log("selected child category", this.selectedChildCategory);


  }

  // ON select Menu CC
  onSelectMenuCC(root, cat, child, cc) {
    console.log('menu root id', root._id)
    console.log('menu cat._id', cat._id);
    console.log('menu child._id', child._id);
    console.log('menu cc id', cc._id);

    if (!this.selectedMenucc.includes(cc.childChildCategoryName)) {
      this.selectedMenucc.push(cc.childChildCategoryName)
    }
    else {
      this.selectedMenucc.splice(this.selectedMenucc.indexOf(cc.childChildCategoryName), 1)
    }

    console.log('selected menu child child', this.selectedMenucc);


  }


  //  On sAVE MENU PAGE
  onUpdateMenu() {
    console.log("selected menu root", this.selectedMenuRoot)
    console.log("selected menu category", this.selectedMenuCategory)
    console.log("selected menu child", this.selectedChildCategory)
    console.log("selected menu root", this.selectedMenucc)


    var menuObj = {
      rootCategories: this.selectedMenuRoot,
      categories: this.selectedMenuCategory,
      childCategories: this.selectedChildCategory,
      childchildCategories: this.selectedMenucc
    }

    console.log('resto id', this.restoid);
    console.log('menuObj', menuObj);

    // posting final menu
    this.restServ.putSelectedMenu(this.restoid, {menu: menuObj,userid: this.userid}).subscribe(res => {
      console.log('response', res);

      if(res['sucess']){
        Swal.fire('Menu Updated Successfully','','success')
      }
      else{
        Swal.fire('Failed to Update','','warning')
      }

    })


  }

// *************************************LIQOUR FUNCITONS
  // PATCH VLAUE FOR LIQUOUR
  patchLiqour(){
    this.selectedRootDrinksName = this.allMenudb.liquor.rootVarients
    this.selectedDrinkVarient = this.allMenudb.liquor.Varients
    this.selectedDrinkSubVarient = this.allMenudb.liquor.SubVarients
    this.selectedDrinkSuperChild = this.allMenudb.liquor.SubSubVarients

  }


    // ON SECLECT LIQ;UOR cATEGORY
    onSelectRootDrink(root) {
      console.log('root', root._id);
      if (!this.selectedRootDrinksName.includes(root.rootCategoryName)) {
        this.selectedRootDrinksName.push(root.rootCategoryName)
      }
      else {
        this.selectedRootDrinksName.splice(this.selectedRootDrinksName.indexOf(root.rootCategoryName), 1)
      }
  
      console.log('select root drinks name', this.selectedRootDrinksName);
  
    }
  
    // ON Select Liqour VARIENT
    onSelectDrinkVarient(root, cat) {
      console.log('root', root._id);
      console.log('cat', cat._id);
  
      if (!this.selectedDrinkVarient.includes(cat.categoryName)) {
        this.selectedDrinkVarient.push(cat.categoryName)
      }
      else {
        this.selectedDrinkVarient.splice(this.selectedDrinkVarient.indexOf(cat.categoryName), 1)
      }
  
      console.log('select root drinks name', this.selectedDrinkVarient);
  
    }
  
    // ON SELECT LIQOUR CHILD
    onSelectDrinkSubVarient(root, cat, child) {
      console.log('root', root._id);
      console.log('cat', cat._id);
      console.log('child', child._id);
  
      if (!this.selectedDrinkSubVarient.includes(child.childCategoryName)) {
        this.selectedDrinkSubVarient.push(child.childCategoryName)
      }
      else {
        this.selectedDrinkVarient.splice(this.selectedDrinkSubVarient.indexOf(child.childCategoryName), 1)
      }
  
      console.log('select root drinks name', this.selectedDrinkSubVarient);
    }
  
    // ON SELECT Liquor cc
    onSelectDrinkSubSub(root, cat, child, cc) {
      console.log('root', root._id);
      console.log('cat', cat._id);
      console.log('child', child._id);
      console.log('cc', cc);
  
      if (!this.selectedDrinkSuperChild.includes(cc.childChildCategoryName)) {
        this.selectedDrinkSuperChild.push(cc.childChildCategoryName)
      }
      else {
        this.selectedDrinkVarient.splice(this.selectedDrinkSuperChild.indexOf(cc.childChildCategoryName), 1)
      }
  
      console.log('select root drinks name', this.selectedDrinkSuperChild);
  
    }


      // on Add dialog of Liquor

  openAddLiquorRoot() {
    const dialogRef = this.matDialog.open(AddRootLiquorPopComponent, {
      width: '540px'
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('result', result);
     
       // GET ALL DRINKS FROM DB
       this.restServ.getALlLiquorTree().subscribe(data => {
        console.log('get all drinks', data);
        this.DrinkTypes = data['data']
      })
    })

  }

  // open add dialog liqour varient
  openAddLiquorVarient(root) {
    let root_id = root['_id']
    const dialogRef = this.matDialog.open(AddVarientPopComponent, {
      width: '540px',
      data: root_id
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('reslut', result);
       // GET ALL DRINKS FROM DB
       this.restServ.getALlLiquorTree().subscribe(data => {
        console.log('get all drinks', data);
        this.DrinkTypes = data['data']
      })
    })
  }

  // open add dialog SUB liquor varient
  openAddSubVarient(root, cat) {
    const dialogRef = this.matDialog.open(AddSubVarientPopComponent, {
      width: '540px',
      data: { root: root['_id'], cat: cat['_id'] }
    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('reslut', result);
      // GET ALL DRINKS FROM DB
      this.restServ.getALlLiquorTree().subscribe(data => {
        console.log('get all drinks', data);
        this.DrinkTypes = data['data']
      })
    })
  }

  // open add dialog super liquor child
  openAddLiquorSuperChild(root, child) {
    const dialogRef = this.matDialog.open(AddSuperChildPopComponent, {
      width: '540px',
      data: { root: root._id, child: child._id }

    })

    dialogRef.afterClosed().subscribe(result => {
      console.log('reslut', result);
      // GET ALL DRINKS FROM DB
      this.restServ.getALlLiquorTree().subscribe(data => {
        console.log('get all drinks', data);
        this.DrinkTypes = data['data']
      })
    })
  }


    //ON UPDATE LIQUOR MENU FORM
    onUpdateLiqour() {

  
      console.log('selected ROOT', this.selectedRootDrinksName);
      console.log('selected VARIENT', this.selectedDrinkVarient);
      console.log('selected SUB VARIENT', this.selectedDrinkSubVarient);
      console.log('selected SUPER CHILD', this.selectedDrinkSuperChild);
  
      var liquorObj = {
        rootVarients: this.selectedRootDrinksName,
        Varients: this.selectedDrinkVarient,
        SubVarients: this.selectedDrinkSubVarient,
        SubSubVarients: this.selectedDrinkSuperChild
      }
      this.restServ.putSelectedLiquor(this.restoid,{liquor: liquorObj, userid: this.userid}).subscribe(res => {
        console.log('Response Liquor object', res);
        if (res['sucess']) {
          Swal.fire('Added Liquor Successfully', '', 'success')
          
        }
        else {
          Swal.fire('Failed to Add', '', 'warning')
        }
      })
    }
  


  // **********************************MENU MGMT FUCNTIONS******************************
  // ***************************************************************************************


  // // **********************************LAYOUT MGMT FUCNTIONS******************************
  patchLayout(){

    this.client = !this.client
  }


  
  // ****Layout code*****

  addTableLay() {


    this.rectArr.push(
      {
        index: this.i,
        transform: ""
      }
    )

    this.i = this.i + 1

    console.log('rect arr', this.rectArr)
  }


  show(event, i) {

    console.log('index', i)
    console.log('event', event)

    if (this.rectArr[i].index == i) {
      this.rectArr[i].transform = event
    }

    console.log('after', this.rectArr)
  }


  save() {
    this.client = false
    localStorage.setItem('rectArr', JSON.stringify(this.rectArr))
    console.log('Final Layout Object', this.rectArr)
  }


  clientView() {
    this.client = !this.client
  }

  delete() {
    localStorage.removeItem('rectArr')
    this.client = false
    this.i = 0
    this.rectArr = []

  }

  FinalSubmit(){

    localStorage.removeItem('rectArr')
    let layoutObj = {rectArr: this.rectArr}
    this.restServ.postLayout(this.restoid, {layout:layoutObj, userid: this.userid}).subscribe(res=>{
      console.log('response',res);
      if(res['sucess']){
        Swal.fire('Updated Layout Successfully','','success')
        this.restServ.current_restoid= this.restoid
        this.router.navigate(['/restaurants/view-restaurant'])
      }
      else{
        Swal.fire('Failed to Update Layout','','warning')
      }
      
    })
    // Swal.fire('Submitting your Restaurant Successfully','','success')
    // this.router.navigate(['/restaurants/add-outlet'])
  }


  // // **********************************LAYOUT MGMT FUCNTIONS******************************
  
  // // **************************************************************************************
}
