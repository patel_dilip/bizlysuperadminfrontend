import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCircularTemplateComponent } from './create-circular-template.component';

describe('CreateCircularTemplateComponent', () => {
  let component: CreateCircularTemplateComponent;
  let fixture: ComponentFixture<CreateCircularTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCircularTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCircularTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
