import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-create-circular-template',
  templateUrl: './create-circular-template.component.html',
  styleUrls: ['./create-circular-template.component.scss']
})
export class CreateCircularTemplateComponent implements OnInit {
  createCircularTemplate: FormGroup
  noticeTypes:string[] = ["Report","Sales"]
  reportTypes:string[] = ["Weekly Report","Monthly Report", "Employee Report"]
  addNotices:string[] = ["abc","xyz","lmn"]
  circularTypes:string[] = ["One Time", "Recurring"]
  circulars:string[] = ["Roles", "Users"]

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createCircularTemplate=this.fb.group({
      templateID:[''],
      templateName:[''],
      templateDescription:[''],
      noticeType:[''],
      reportType:[''],
      addNotice:[''],
      circularType:[''],
      circularFor:['']
    })
  }
  onCreateTemplate(){
    console.log(this.createCircularTemplate.value);
    
  }
}
