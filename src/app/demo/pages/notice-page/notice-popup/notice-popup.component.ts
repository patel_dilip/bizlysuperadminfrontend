import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-notice-popup',
  templateUrl: './notice-popup.component.html',
  styleUrls: ['./notice-popup.component.scss']
})
export class NoticePopupComponent implements OnInit {
  PopUpForm:FormGroup;
  isSubmitted=false;
  UserData:any;
  constructor(private formbuilder:FormBuilder) { }

  ngOnInit() {
    this.PopUpForm=this.formbuilder.group({
     date:[''],
     time:[''],
     openMon:[''],
     closeMon:['']

    })
  }
Save_Send(){
  this.isSubmitted=true;
  console.log("Submit",this.PopUpForm.value);
  
}
}
