import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCircularTemplateComponent } from './view-circular-template.component';

describe('ViewCircularTemplateComponent', () => {
  let component: ViewCircularTemplateComponent;
  let fixture: ComponentFixture<ViewCircularTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCircularTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCircularTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
