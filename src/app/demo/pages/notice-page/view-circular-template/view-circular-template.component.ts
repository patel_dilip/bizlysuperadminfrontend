import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-view-circular-template',
  templateUrl: './view-circular-template.component.html',
  styleUrls: ['./view-circular-template.component.scss']
})
export class ViewCircularTemplateComponent implements OnInit {
element:any
  constructor( public dialogRef: MatDialogRef<ViewCircularTemplateComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {  
      console.log(data);
     this.element=data
    }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
