import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoticeHomeComponent } from './notice-home/notice-home.component';
import { NoticeListComponent } from './notice-list/notice-list.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { CreateCircularTemplateComponent } from './create-circular-template/create-circular-template.component';
import { NoticePageOrderingComponent } from './notice-page-ordering/notice-page-ordering.component';
import { NoticeFoodOrderingComponent } from './notice-food-ordering/notice-food-ordering.component';
//import { NoticePopupComponent } from './notice-popup/notice-popup.component';
import { CircularFoodOrderingComponent } from './circular-food-ordering/circular-food-ordering.component';
import { DiscussionComponent } from './discussion/discussion.component';

import { ViewNoticeComponent } from './view-notice/view-notice.component';
import { UpdateCircularTemplateComponent } from './update-circular-template/update-circular-template.component';
import { UpdateCircularNoticeComponent } from './update-circular-notice/update-circular-notice.component';
//import { CircularPopUpComponent } from './circular-pop-up/circular-pop-up.component';




const routes: Routes = [
  {
    path: '',
    component: NoticeHomeComponent,
    children: [
      // {
      //   path: '',
      //   component: NoticeListComponent
      // }
      // },{
      //   path: 'create-circular-template',
      //   component: CreateCircularTemplateComponent
      // }
      {
        path: 'circular-food-ordering',
        component: CircularFoodOrderingComponent
      },
      {
        path: '',
        component: NoticePageOrderingComponent
      }
      ,{
        path: 'notice-food-ordering',
        component: NoticeFoodOrderingComponent
      }
      ,{
        path: 'update-circular-template',
        component: UpdateCircularTemplateComponent
      }
      ,{
        path: 'update-circular-notice',
        component: UpdateCircularNoticeComponent
      }

    ]
  }
]
@NgModule({
  declarations: [NoticeHomeComponent, NoticeListComponent, CreateCircularTemplateComponent,
     NoticePageOrderingComponent, NoticeFoodOrderingComponent,CircularFoodOrderingComponent, 
     ViewNoticeComponent,

     DiscussionComponent,
    // NoticePopupComponent,
      UpdateCircularTemplateComponent, UpdateCircularNoticeComponent,],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
  ],
  entryComponents:[

  ]
})
export class NoticePageModule { }
