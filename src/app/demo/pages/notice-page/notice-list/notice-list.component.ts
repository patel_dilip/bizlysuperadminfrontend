import { Component, OnInit, ViewChild } from '@angular/core';

import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';


export interface circularTemplateElement{
  templateID:String;
  templateDateAndTime:String;
  templateName:string;
  templateDescription:string;
  noticeType:string;
  createdByAndOn: string;
  changesDoneBy: string;
  draftOrUsed: string;
  status: boolean;
}

export interface noticeOrCircularElement{
  circularID:String;
  circularDateAndTime:String;
  circularSubjectOrHeading:string;
  circularFor:string;
  circularMode:string;
  createdByAndOn: string;
  byClickingOnit: string;
}

const circularTemplateDATA: circularTemplateElement[]=[
  {templateID:"T001", templateDateAndTime:"27/01/2020 11:03AM", templateName:"ABC", templateDescription:"Abc Def Ghi Jkl Mno Pqr", noticeType:"report", createdByAndOn: "ak on 27/01/2020", changesDoneBy: "ak on 27/01/2020", draftOrUsed: "xyz", status: true},
  {templateID:"T002", templateDateAndTime:"27/01/2020 11:03AM", templateName:"ABC", templateDescription:"Abc Def Ghi Jkl Mno Pqr", noticeType:"report", createdByAndOn: "ak on 27/01/2020", changesDoneBy: "ak on 27/01/2020", draftOrUsed: "xyz", status: false},
  {templateID:"T003", templateDateAndTime:"27/01/2020 11:03AM", templateName:"ABC", templateDescription:"Abc Def Ghi Jkl Mno Pqr", noticeType:"report", createdByAndOn: "ak on 27/01/2020", changesDoneBy: "ak on 27/01/2020", draftOrUsed: "xyz", status: true},
  {templateID:"T004", templateDateAndTime:"27/01/2020 11:03AM", templateName:"ABC", templateDescription:"Abc Def Ghi Jkl Mno Pqr", noticeType:"report", createdByAndOn: "ak on 27/01/2020", changesDoneBy: "ak on 27/01/2020", draftOrUsed: "xyz", status: true},
  {templateID:"T005", templateDateAndTime:"27/01/2020 11:03AM", templateName:"ABC", templateDescription:"Abc Def Ghi Jkl Mno Pqr", noticeType:"report", createdByAndOn: "ak on 27/01/2020", changesDoneBy: "ak on 27/01/2020", draftOrUsed: "xyz", status: false},
  {templateID:"T006", templateDateAndTime:"27/01/2020 11:03AM", templateName:"ABC", templateDescription:"Abc Def Ghi Jkl Mno Pqr", noticeType:"report", createdByAndOn: "ak on 27/01/2020", changesDoneBy: "ak on 27/01/2020", draftOrUsed: "xyz", status: true},
  {templateID:"T007", templateDateAndTime:"27/01/2020 11:03AM", templateName:"ABC", templateDescription:"Abc Def Ghi Jkl Mno Pqr", noticeType:"report", createdByAndOn: "ak on 27/01/2020", changesDoneBy: "ak on 27/01/2020", draftOrUsed: "xyz", status: false},
  {templateID:"T008", templateDateAndTime:"27/01/2020 11:03AM", templateName:"ABC", templateDescription:"Abc Def Ghi Jkl Mno Pqr", noticeType:"report", createdByAndOn: "ak on 27/01/2020", changesDoneBy: "ak on 27/01/2020", draftOrUsed: "xyz", status: true},
]

const noticeOrCircularDATA: noticeOrCircularElement[]=[
  {circularID:"C002", circularDateAndTime:"27/01/2020", circularSubjectOrHeading:"ABC", circularFor:"XYZ", circularMode:"abc", createdByAndOn: "ak on 27/01/2020", byClickingOnit: "pqr"},
  {circularID:"C003", circularDateAndTime:"27/01/2020", circularSubjectOrHeading:"ABC", circularFor:"XYZ", circularMode:"abc", createdByAndOn: "ak on 27/01/2020", byClickingOnit: "pqr"},
  {circularID:"C004", circularDateAndTime:"27/01/2020", circularSubjectOrHeading:"ABC", circularFor:"XYZ", circularMode:"abc", createdByAndOn: "ak on 27/01/2020", byClickingOnit: "pqr"},
  {circularID:"C005", circularDateAndTime:"27/01/2020", circularSubjectOrHeading:"ABC", circularFor:"XYZ", circularMode:"abc", createdByAndOn: "ak on 27/01/2020", byClickingOnit: "pqr"},
  {circularID:"C006", circularDateAndTime:"27/01/2020", circularSubjectOrHeading:"ABC", circularFor:"XYZ", circularMode:"abc", createdByAndOn: "ak on 27/01/2020", byClickingOnit: "pqr"},
  {circularID:"C007", circularDateAndTime:"27/01/2020", circularSubjectOrHeading:"ABC", circularFor:"XYZ", circularMode:"abc", createdByAndOn: "ak on 27/01/2020", byClickingOnit: "pqr"},
  {circularID:"C008", circularDateAndTime:"27/01/2020", circularSubjectOrHeading:"ABC", circularFor:"XYZ", circularMode:"abc", createdByAndOn: "ak on 27/01/2020", byClickingOnit: "pqr"},
  {circularID:"C009", circularDateAndTime:"27/01/2020", circularSubjectOrHeading:"ABC", circularFor:"XYZ", circularMode:"abc", createdByAndOn: "ak on 27/01/2020", byClickingOnit: "pqr"},
  {circularID:"C010", circularDateAndTime:"27/01/2020", circularSubjectOrHeading:"ABC", circularFor:"XYZ", circularMode:"abc", createdByAndOn: "ak on 27/01/2020", byClickingOnit: "pqr"},

]

@Component({
  selector: 'app-notice-list',
  templateUrl: './notice-list.component.html',
  styleUrls: ['./notice-list.component.scss']
})
export class NoticeListComponent implements OnInit {

//circularTemplateDATA: circularTemplateElement
  displayColumnsCircularTemplate: string[] = ['templateID', 'templateDateAndTime', 'templateName', 'templateDescription', 'noticeType', 'createdByAndOn', 'changesDoneBy', 'draftOrUsed', 'status', 'action'];
  dataSourceCircularTemplate: MatTableDataSource<circularTemplateElement>;
  @ViewChild("CircularTemplatepaginator", { static: true }) CircularTemplatepaginator: MatPaginator;
  @ViewChild("CircularTemplatesort", { static: true }) CircularTemplatesort: MatSort;

  //noticeOrCircularDATA: noticeOrCircularElement
  displayColumnsNoticeOrCircular: string[] = ['circularID', 'circularDateAndTime', 'circularSubjectOrHeading', 'circularFor', 'circularMode', 'createdByAndOn', 'byClickingOnit', 'action'];
  dataSourceNoticeOrCircular: MatTableDataSource<noticeOrCircularElement>;
  @ViewChild("NoticeOrCircularpaginator", { static: true }) NoticeOrCircularpaginator: MatPaginator;
  @ViewChild("NoticeOrCircularsort", { static: true }) NoticeOrCircularsort: MatSort;

  constructor() {
    this.dataSourceCircularTemplate= new MatTableDataSource(circularTemplateDATA)
    this.dataSourceNoticeOrCircular= new MatTableDataSource(noticeOrCircularDATA)
  }

  ngOnInit() {
    this.dataSourceCircularTemplate.paginator = this.CircularTemplatepaginator;
    this.dataSourceCircularTemplate.sort = this.CircularTemplatesort;

    this.dataSourceNoticeOrCircular.paginator = this.NoticeOrCircularpaginator;
    this.dataSourceNoticeOrCircular.sort = this.NoticeOrCircularsort;

  }
  applyFilterCirculatTemplate(filterValue: string) {
    this.dataSourceCircularTemplate.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceCircularTemplate.paginator) {
      this.dataSourceCircularTemplate.paginator.firstPage();
    }
  }

  applyFilterNoticeOrCircular(filterValue: string) {
    this.dataSourceNoticeOrCircular.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceNoticeOrCircular.paginator) {
      this.dataSourceNoticeOrCircular.paginator.firstPage();
    }
  }
}
