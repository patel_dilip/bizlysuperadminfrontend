import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticeFoodOrderingComponent } from './notice-food-ordering.component';

describe('NoticeFoodOrderingComponent', () => {
  let component: NoticeFoodOrderingComponent;
  let fixture: ComponentFixture<NoticeFoodOrderingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoticeFoodOrderingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeFoodOrderingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
