import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators} from '@angular/forms';
import { MatDialog } from '@angular/material';
import { NoticePopupComponent } from '../notice-popup/notice-popup.component';
import { CircularPopUpComponent } from '../circular-pop-up/circular-pop-up.component';
import { Router } from '@angular/router'
@Component({
  selector: 'app-notice-food-ordering',
  templateUrl: './notice-food-ordering.component.html',
  styleUrls: ['./notice-food-ordering.component.scss']
})
export class NoticeFoodOrderingComponent implements OnInit {
  foodOrderForm:FormGroup;
  notice_type_list=['Report','Sales'];
  report_type_list=['Weekly report','Monthly report','Employee report'];
  add_notice_list=['ABC','XYZ'];
  circular_type_list=['One time','Recurring'];
  circular_for_list=['Roles','Users']
  issubmitted=false;
  userid;
  constructor(private router:Router,private formbuilder:FormBuilder, private dialog: MatDialog) {
    let userdata=JSON.parse(localStorage.getItem('loginUser'));
    this.userid=userdata['_id'];
   }

  ngOnInit() {
    this.foodOrderForm=this.formbuilder.group({
      template_id:['', [Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      template_name:['', [Validators.required,Validators.pattern('^[ a-zA-Z]+$')]],
      template_description:['', [Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      notice_type:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      report_type:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      add_notice:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      circular_type:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      circular_for:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      Userid:this.userid
    })
  }
    
 
  openDialog(): void {
    const dialogRef = this.dialog.open(NoticePopupComponent, {
      disableClose: true,
      width: 'auto',
      height: 'auto',


    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }
  openDialog_Circular(): void {
    const dialogRef = this.dialog.open(CircularPopUpComponent, {
      disableClose: true,
      width: 'auto',
      height: 'auto',


    });
    
  }
 
 Send_Save(){
    this.issubmitted=true;
    let object=this.foodOrderForm.value;
    console.log("Hello",object);
  }

  back_to_page(){
    this.router.navigate(['/notice-page']);
    //this.router.navigate(['notice-page/notice-page-ordering']);
  }
}
