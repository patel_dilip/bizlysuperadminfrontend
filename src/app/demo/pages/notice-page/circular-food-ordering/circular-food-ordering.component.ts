import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { Router } from '@angular/router'

export interface UserData {
  add_circular:string
  ass_manager:string
  image:any
  add_circular_disc:string
}
@Component({
  selector: 'app-circular-food-ordering',
  templateUrl: './circular-food-ordering.component.html',
  styleUrls: ['./circular-food-ordering.component.scss']
})
export class CircularFoodOrderingComponent implements OnInit {
  CircularfoodOrderForm:FormGroup;
  UserData:any;
  isSubmitted=false;
  selectedFile: File;
  img: any
  url: string | ArrayBuffer;
  //public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'profile' });
  constructor(private formbuilder:FormBuilder,private router:Router) { }

  ngOnInit() {
    this.CircularfoodOrderForm=this.formbuilder.group({
      add_circular:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      ass_manager:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      add_circular_disc:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      image:['']
    })
  }
Save(){
 this.isSubmitted=true;
 console.log("Submitted",this.CircularfoodOrderForm.value);
 
}
back_toPage(){
  this.router.navigate(['/notice-page']);
}
}
