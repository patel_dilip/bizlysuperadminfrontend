import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CircularFoodOrderingComponent } from './circular-food-ordering.component';

describe('CircularFoodOrderingComponent', () => {
  let component: CircularFoodOrderingComponent;
  let fixture: ComponentFixture<CircularFoodOrderingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircularFoodOrderingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircularFoodOrderingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
