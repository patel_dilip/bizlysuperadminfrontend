import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FormGroup,FormBuilder, Validators } from '@angular/forms'
import { NoticeService } from 'src/app/_services/_noticeservices/notice.service';
@Component({
  selector: 'app-update-circular-notice',
  templateUrl: './update-circular-notice.component.html',
  styleUrls: ['./update-circular-notice.component.scss']
})
export class UpdateCircularNoticeComponent implements OnInit {
  updateCircularNoticeForm:FormGroup;
  elementdata:any;
  constructor(private router:Router,private formbuilder:FormBuilder,private notice_serv:NoticeService) { 
    if(this.notice_serv.noticeSerCircularData==undefined){
      this.router.navigate(['/notice-page']);
    }else{
      console.log("update element",this.notice_serv.noticeSerData);
      this.elementdata=this.notice_serv.noticeSerCircularData
       console.log("element",this.elementdata);
    }
  }

  ngOnInit() {
    this.updateCircularNoticeForm=this.formbuilder.group({
      circular_subject:[this.elementdata.circular_subject,[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      ass_manager:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      add_circular_disc:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      image:['']
    })
  }
  back_to_page(){
    this.router.navigate(['/notice-page']);
    //this.router.navigate(['notice-page/notice-page-ordering']);
  }
}
