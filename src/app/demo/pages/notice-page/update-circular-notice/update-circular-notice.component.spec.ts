import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCircularNoticeComponent } from './update-circular-notice.component';

describe('UpdateCircularNoticeComponent', () => {
  let component: UpdateCircularNoticeComponent;
  let fixture: ComponentFixture<UpdateCircularNoticeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCircularNoticeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCircularNoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
