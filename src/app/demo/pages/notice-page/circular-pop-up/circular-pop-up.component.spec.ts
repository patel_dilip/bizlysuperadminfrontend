import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CircularPopUpComponent } from './circular-pop-up.component';

describe('CircularPopUpComponent', () => {
  let component: CircularPopUpComponent;
  let fixture: ComponentFixture<CircularPopUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircularPopUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircularPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
