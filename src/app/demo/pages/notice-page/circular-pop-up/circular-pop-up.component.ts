import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-circular-pop-up',
  templateUrl: './circular-pop-up.component.html',
  styleUrls: ['./circular-pop-up.component.scss']
})
export class CircularPopUpComponent implements OnInit {
  circularPopUpForm:FormGroup;
  select_mode_list=['Active','Inactive'];
  select_priority_list=['High','Low','Medium'];
  allow_reply_list=['Yes','No'];
  isSubmitted=false;
  UserData:any;
  constructor(private formbuilder:FormBuilder) { }

  ngOnInit() {
    this.circularPopUpForm=this.formbuilder.group({
      select_mode:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      allow_reply:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      select_priority:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]]
    })
  }
  Send(){
    this.isSubmitted=true;
    console.log("Submite",this.circularPopUpForm.value)
  }
}
