import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTemplateCardComponent } from './view-template-card.component';

describe('ViewTemplateCardComponent', () => {
  let component: ViewTemplateCardComponent;
  let fixture: ComponentFixture<ViewTemplateCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTemplateCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTemplateCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
