import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-view-template-card',
  templateUrl: './view-template-card.component.html',
  styleUrls: ['./view-template-card.component.scss']
})
export class ViewTemplateCardComponent implements OnInit {
  viewTemplateForm:FormGroup;
  constructor(private formbuilder:FormBuilder,public dialogRef: MatDialogRef<ViewTemplateCardComponent>) { }

  ngOnInit() {
    this.viewTemplateForm=this.formbuilder.group({
      firstImage:[''],
      secondImage:['']
     })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
