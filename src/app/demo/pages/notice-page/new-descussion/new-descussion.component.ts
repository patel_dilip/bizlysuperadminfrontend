import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-descussion',
  templateUrl: './new-descussion.component.html',
  styleUrls: ['./new-descussion.component.scss']
})
export class NewDescussionComponent implements OnInit {
  newDiscussionForm:FormGroup;
  members_list=['a','b','c'];
  isSubmitted=false;
  constructor(private formbuilder:FormBuilder) { }

  ngOnInit() {
    this.newDiscussionForm=this.formbuilder.group({
      title:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      discription:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      members:['',[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]]
    })
  }
Save_discussion(){
  this.isSubmitted=true;
  console.log("Submitted",this.newDiscussionForm.value)
}
}
