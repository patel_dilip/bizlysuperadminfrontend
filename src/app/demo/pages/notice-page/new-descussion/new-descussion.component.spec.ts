import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewDescussionComponent } from './new-descussion.component';

describe('NewDescussionComponent', () => {
  let component: NewDescussionComponent;
  let fixture: ComponentFixture<NewDescussionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDescussionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewDescussionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
