import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NoticeService } from 'src/app/_services/_noticeservices/notice.service';
@Component({
  selector: 'app-update-circular-template',
  templateUrl: './update-circular-template.component.html',
  styleUrls: ['./update-circular-template.component.scss']
})
export class UpdateCircularTemplateComponent implements OnInit {
  updatecircularTemplateForm:FormGroup;
  notice_type_list=['Report','Sales'];
  report_type_list=['Weekly report','Monthly report','Employee report'];
  add_notice_list=['ABC','XYZ'];
  circular_type_list=['One time','Recurring'];
  circular_for_list=['Roles','Users']
  issubmitted=false;
  elementdata:any;
  constructor(private router:Router,private formbuilder:FormBuilder,private notice_serv:NoticeService) {
    if(this.notice_serv.noticeSerData==undefined){
      this.router.navigate(['/notice-page']);
    }else{
      console.log("update element",this.notice_serv.noticeSerData);
      this.elementdata=this.notice_serv.noticeSerData
       console.log("element",this.elementdata);
    }
   
   
  }
  // template_Id: 1
  // template_Date_Time: 0.0004950495049504951
  // template_Name: "abc"
  // template_Description: "H"
  // notice_type: "active"
  // created_by_On: "yes"
  // changes_by: "a"
  // draft_used: "y"
  // status: true
  // action: 
  ngOnInit() {
    this.updatecircularTemplateForm=this.formbuilder.group({
      template_id:[this.elementdata.template_Id, [Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      template_name:[this.elementdata.template_Date_Time, [Validators.required,Validators.pattern('^[ a-zA-Z]+$')]],
      template_description:[this.elementdata.template_Description, [Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      notice_type:[this.elementdata.notice_type,[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      report_type:[this.elementdata.created_by_On,[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      add_notice:[this.elementdata.changes_by,[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      circular_type:[this.elementdata.draft_used,[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]],
      circular_for:[this.elementdata.status,[Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]]
    })
  }
  onpatchValueClick(): void {  
    this.updatecircularTemplateForm.patchValue({  
     
      template_id:1,
      template_name:'ABC',
      template_description:'xyz',
      notice_type:'Report',
      report_type:'Weekly Report',
      add_notice:'ABC',
      circular_type:'One Time',
      circular_for:'Roles',
    });  
} 
submit_updateForm(){
  this.issubmitted=true;
  console.log("Submit updated form",this.updatecircularTemplateForm.value);
}
back_to_page(){
  this.router.navigate(['/notice-page']);
  //this.router.navigate(['notice-page/notice-page-ordering']);
}
update_circularTemplate(){
  
}
}
