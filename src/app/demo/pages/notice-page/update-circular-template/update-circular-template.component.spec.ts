import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCircularTemplateComponent } from './update-circular-template.component';

describe('UpdateCircularTemplateComponent', () => {
  let component: UpdateCircularTemplateComponent;
  let fixture: ComponentFixture<UpdateCircularTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateCircularTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCircularTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
