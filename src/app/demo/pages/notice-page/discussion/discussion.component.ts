import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { NewDescussionComponent } from '../new-descussion/new-descussion.component';
@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss']
})
export class DiscussionComponent implements OnInit {
  DiscussionForm:FormGroup;
  constructor(private formbuilder:FormBuilder, private dialog: MatDialog) { }

  ngOnInit() {
    this.DiscussionForm=this.formbuilder.group({
      template_name:['']
    })
  }
  openDialog_newdiscussion(): void {
    const dialogRef = this.dialog.open(NewDescussionComponent, {
      disableClose: true,
      width: 'auto',
      height: 'auto',


    });
    
  }

}
