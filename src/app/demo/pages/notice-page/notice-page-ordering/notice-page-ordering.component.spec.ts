import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticePageOrderingComponent } from './notice-page-ordering.component';

describe('NoticePageOrderingComponent', () => {
  let component: NoticePageOrderingComponent;
  let fixture: ComponentFixture<NoticePageOrderingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoticePageOrderingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticePageOrderingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
