import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { MatDialog } from '@angular/material';

import { DiscussionComponent } from '../discussion/discussion.component';
import { ViewNoticeComponent } from '../view-notice/view-notice.component';
import { ViewCircularTemplateComponent } from '../view-circular-template/view-circular-template.component';
import { ViewCircularNoticeComponent } from '../view-circular-notice/view-circular-notice.component';
import { ViewTemplateCardComponent } from '../view-template-card/view-template-card.component';
import { NoticeService } from 'src/app/_services/_noticeservices/notice.service';

export interface UserData {
  template_Date_Time: number;
  template_Id: number;
  template_Name:string;
  template_Description: string;
  notice_type:string;
  created_by_On:string;
  changes_by:string;
  draft_used:string;
  status: boolean;
  action:string;
}
export interface UserData1 {
  circular_Id:number
  circular_Date_Time:number
  circular_subject:string
  circular_roles:string
  circular_mode:string
  created_by_On:string
  status: boolean;
  action:string
}

const DATA: UserData[] = [
  {template_Id: 1,template_Date_Time: 1/1/2020, template_Name:'abc', template_Description: 'H',notice_type:'active',created_by_On:'yes',changes_by:'a',draft_used:'y',status: true,action:''},
  {template_Id: 2,template_Date_Time: 1/1/2020, template_Name:'abc', template_Description: 'H',notice_type:'active',created_by_On:'yes',changes_by:'b',draft_used:'y',status: true,action:''},
  {template_Id: 2,template_Date_Time: 1/1/2020, template_Name: 'abc', template_Description: 'H',notice_type:'active',created_by_On:'yes',changes_by:'z',draft_used:'y',status: true,action:''},
  
];
const DATA1: UserData1[] = [
  {circular_Id: 1,circular_Date_Time: 1/1/2020, circular_subject:'abc', circular_roles: 'H',circular_mode:'active',created_by_On:'yes',status: true,action:''},
  {circular_Id: 2,circular_Date_Time: 1/1/2020, circular_subject: 'xyz', circular_roles: 'H',circular_mode:'active',created_by_On:'yes',status: true,action:''},
  {circular_Id: 2,circular_Date_Time: 1/1/2020, circular_subject: 'xyz', circular_roles: 'H',circular_mode:'active',created_by_On:'yes',status: true,action:''},
  
];
@Component({
  selector: 'app-notice-page-ordering',
  templateUrl: './notice-page-ordering.component.html',
  styleUrls: ['./notice-page-ordering.component.scss']
})
export class NoticePageOrderingComponent implements OnInit {
  NoticeForm:FormGroup;
  //First Table
  displayedColumns: string[] = ['template_Id', 'template_Date_Time', 'template_Name', 'template_Description','notice_type','created_by_On',
  'changes_by','draft_used','status','action'];
  dataSource = new MatTableDataSource<UserData>();
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("sort", { static: true }) sort: MatSort;
  
  //Second Table
  displayedColumnsCircular: string[] = ['circular_Id', 'circular_Date_Time', 'circular_subject', 'circular_roles','circular_mode','created_by_On','status','action'];
  dataSource1 =  new MatTableDataSource<UserData1>();
  @ViewChild("noticepaginator", { static: true }) noticepaginator: MatPaginator;
  @ViewChild("noticesort", { static: true }) noticesort: MatSort;

 
  employee_list=['ABC','XYZ']
  issubmitted=false;
  constructor(private notice_ser:NoticeService,private router:Router,private formbuilder:FormBuilder, private dialog: MatDialog) {
    console.log("notice service data",this.notice_ser.noticeSerData);
    this.dataSource = new MatTableDataSource(DATA);
    this.dataSource1 = new MatTableDataSource(DATA1);
   }

  ngOnInit() {
    this.NoticeForm=this.formbuilder.group({
      employee:['']
    })
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.dataSource1.paginator = this.noticepaginator;
    this.dataSource.sort = this.noticesort;
  }
 
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  applyFilterNotice(filterValue: string) {
    this.dataSource1.filter = filterValue.trim().toLowerCase();
    if (this.dataSource1.paginator) {
      this.dataSource1.paginator.firstPage();
    }
  }

  openDialog_discussion(): void {
    const dialogRef = this.dialog.open(DiscussionComponent, {
      disableClose: true,
      width: 'auto',
      height: 'auto',


    });
    
  }
  openDialog_notice(): void {
    const dialogRef = this.dialog.open(ViewNoticeComponent, {
      disableClose: true,
      width: 'auto',
      height: 'auto',


    });
    
  }

  openDialog_viewCircularTemplate(element): void {
    const dialogRef = this.dialog.open(ViewCircularTemplateComponent, {
      disableClose: true,
      width: 'auto',
      height: 'auto',
      data: element

    });
    
  }

  openDialog_viewCircularNotice(element): void {
    const dialogRef = this.dialog.open(ViewCircularNoticeComponent, {
      disableClose: true,
      width: 'auto',
      height: 'auto',
      data: element

    });
    
  }

  openDialog_viewTemplateCard(): void {
    const dialogRef = this.dialog.open(ViewTemplateCardComponent, {
      disableClose: true,
      width: 'auto',
      height: 'auto',


    });
    
  }
  // applyFilter(filterValue: any) {
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  //   // if (this.dataSource.paginator) {
  //   //   this.dataSource.paginator.firstPage();
  //   // }
  // }
  notice_Submit(){
    console.log("hiis");
    
    this.router.navigate(['notice-page/notice-food-ordering']);
  }
  notice_Circular_Submit(){
    this.router.navigate(['notice-page/circular-food-ordering']);
  }
  go_toDiscussion(){
   this.router.navigate(['notice-page/discussion']);
  }
  update_circularTemplate(element){
   console.log("element",element);
   this.notice_ser.noticeSerData=element;
  this.router.navigate(['notice-page/update-circular-template']);
   }
   update_circularNotice(element){
    console.log("element",element);
    this.notice_ser.noticeSerCircularData=element;
    this.router.navigate(['notice-page/update-circular-notice']);
   }
}
