import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-view-circular-notice',
  templateUrl: './view-circular-notice.component.html',
  styleUrls: ['./view-circular-notice.component.scss']
})
export class ViewCircularNoticeComponent implements OnInit {
  element:any
  constructor( public dialogRef: MatDialogRef<ViewCircularNoticeComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {
      console.log(data);
      this.element=data;
     }
  
  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
