import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCircularNoticeComponent } from './view-circular-notice.component';

describe('ViewCircularNoticeComponent', () => {
  let component: ViewCircularNoticeComponent;
  let fixture: ComponentFixture<ViewCircularNoticeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCircularNoticeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCircularNoticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
