import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router'
@Component({
  selector: 'app-view-pos-role',
  templateUrl: './view-pos-role.component.html',
  styleUrls: ['./view-pos-role.component.scss']
})
export class ViewPosRoleComponent implements OnInit {
  obj={
    "accessPoint":true,
    "versions": [
        {
            "versionName": "version1",
            "created": {
                "dateCreatedandTime": "20/01/2020",
                "dateEditedandTime": "23/01/2020",
                "editedBy": "Emma"
            }
        },
        {
            "versionName": "version2",
            "created": {
                "dateCreatedandTime": "20/01/2020",
                "dateEditedandTime": "23/01/2020",
                "editedBy": "Kristen"
            }
        },
        {
            "versionName": "version3",
            "created": {
                "dateCreatedandTime": "20/03/2020",
                "dateEditedandTime": "23/03/2020",
                "editedBy": "Jack"
            }
        }
    ]
  }
  
  roleobj={
    "roleID":"R005","roleTag":"Manager",
    "accesControl":[ {
      "accessControlName": "restaurant",
      "controls": ["add", "delete"]
    },
    { "accessControlName": "orders", "controls": [ "view", "edit", "delete"] },
    { "accessControlName": "menu", "controls": ["add", "edit", "delete"] },
    { "accessControlName": "pos-orders", "controls": [ "delete"] }
  ]
  }
  constructor(private router:Router,private formbuilder:FormBuilder,public dialogRef: MatDialogRef<ViewPosRoleComponent>, @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit() {
  }
 //**********Pop closed function************
 onNoClick(): void {
  this.dialogRef.close();
}
//*************Navigate to User Role page***********/
BackTo_Page(){
  this.router.navigate(['/users-roles']);
}
}
