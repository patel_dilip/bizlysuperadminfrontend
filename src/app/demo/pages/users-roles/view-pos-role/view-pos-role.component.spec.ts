import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPosRoleComponent } from './view-pos-role.component';

describe('ViewPosRoleComponent', () => {
  let component: ViewPosRoleComponent;
  let fixture: ComponentFixture<ViewPosRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPosRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPosRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
