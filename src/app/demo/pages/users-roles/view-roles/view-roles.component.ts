import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';

@Component({
  selector: 'app-view-roles',
  templateUrl: './view-roles.component.html',
  styleUrls: ['./view-roles.component.scss']
})
export class ViewRolesComponent implements OnInit {
  ViewRolesForm:FormGroup;
  userid:any;
  roleid:any;
  editedData:any=['']
  versions:any=[]
  // obj={
  //   "accessPoint":true,
  //   "versions": [
  //       {
  //           "versionName": "version1",
  //           "created": {
  //               "created_by_on": " ",
  //               "user_edited": " ",
  //               "addedBy": " "
                
                
  //           }
  //       }
     
  //   ]
  // }
 
  roleobj={
    "roleID":"R005","roleTag":"Manager",
    "accesControl":[ {
      "accessControlName": "restaurant",
      "controls": ["add", "delete"]
    },
    { "accessControlName": "orders", "controls": [ "view", "edit", "delete"] },
    { "accessControlName": "menu", "controls": ["add", "edit", "delete"] },
    { "accessControlName": "pos-orders", "controls": [ "delete"] }
  ]
  }
  constructor(private user_role_service:UsersRolesService,private formbuilder:FormBuilder,public dialogRef: MatDialogRef<ViewRolesComponent>, @Inject(MAT_DIALOG_DATA) public data) { 
    console.log(data._id);
this.roleid=data._id
    //this.getEditFunction()
    this.user_role_service.getEditedHistory(this.roleid).subscribe(data=>{
      this.versions=data['data']
         console.log("edited history data",data['data']);
         
        })
    
    console.log("localStorage",this.roleid);

}

  ngOnInit() {
    this.ViewRolesForm=this.formbuilder.group({
      role_ID: [''],
      role_name: [''],   
      role_salary:[''],
      description: [''],
      role_access_points:[ ],
      edited_history:[ ],
      total_users:[''],
      user_edited:[''],
      created_by_on:[''],
      addedBy:[''],
      //roleid:this.roleid,
    });
  }
 //**********Pop closed function************
 onNoClick(): void {
  this.dialogRef.close();
}
//**********view single role************* */
// View_Role(roleid){
//   let obj=this.ViewRolesForm.value;
  
//   this.user_role_service.getSingleRole(roleid).subscribe(data=>{
//         console.log("view created role",data);
        
  
//       });
// }
// getEditFunction(){
//   return new Promise((resolve,reject)=>{
//     let localData = JSON.parse(localStorage.getItem('loginUser'));
//     this.userid=localData.username
//     resolve( this.userid)
//   }).then(id=>{
    

//     this.user_role_service.getEditedHistory(this.roleid).subscribe(data=>{
// this.versions=data['data']
//    console.log("edited history data",data['data']);
   
//   })
//   })
 
// }
}
