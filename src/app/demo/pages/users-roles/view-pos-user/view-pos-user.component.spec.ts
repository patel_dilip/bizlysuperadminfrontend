import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPosUserComponent } from './view-pos-user.component';

describe('ViewPosUserComponent', () => {
  let component: ViewPosUserComponent;
  let fixture: ComponentFixture<ViewPosUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPosUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPosUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
