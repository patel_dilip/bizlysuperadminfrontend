import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router'

@Component({
  selector: 'app-view-pos-user',
  templateUrl: './view-pos-user.component.html',
  styleUrls: ['./view-pos-user.component.scss']
})
export class ViewPosUserComponent implements OnInit {
  viewUserForm:FormGroup;

  showIncreaseUserLimit:false;
  showPaymentReceived:boolean=false;
  constructor(private formbuilder:FormBuilder,private router:Router) { }

  ngOnInit() {
    this.viewUserForm=this.formbuilder.group({
      users:['Roles=12'],
      roles:['Users=34'],
      cheque_number:[''],
      add_users:[''],
      payment_status:['']
    })
  }
//*************Navigate to User Role page***********/
BackTo_Page(){
  this.router.navigate(['/users-roles']);
}
//**************Navigate to add user ****************/
Navigate_to_user(){
  this.router.navigate(['/users-roles/create-pos-user']);
}
//*************Navigate to add role****************/
Navigate_to_role(){
  this.router.navigate(['/users-roles/create-pos-role']);
}
//***show cheque number********* */
showChequeNumber(){
  this.showPaymentReceived=true;
}

}
