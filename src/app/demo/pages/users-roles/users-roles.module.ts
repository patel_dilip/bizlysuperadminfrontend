import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { UsersRolesHomeComponent } from './users-roles-home/users-roles-home.component';
import { UsersRolesPageComponent } from './users-roles-page/users-roles-page.component';
import { EditRolesComponent } from './edit-roles/edit-roles.component';
import { NgbTabsetModule, NgbTooltipModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { CreateUserComponent } from './create-user/create-user.component';
import { TreeModule } from 'angular-tree-component';

import { CreateRolesCompComponent } from './create-roles-comp/create-roles-comp.component';
import { EditUsersComponent } from './edit-users/edit-users.component';
import { CreatePosUserComponent } from './create-pos-user/create-pos-user.component';
import { CreatePosRoleComponent } from './create-pos-role/create-pos-role.component';
import { ViewPosUserComponent } from './view-pos-user/view-pos-user.component';
import { FileUploadModule } from "ng2-file-upload";
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { AddCatRoleNameComponent } from './add-cat-role-name/add-cat-role-name.component';
import { AddSubCatRoleComponent } from './add-sub-cat-role/add-sub-cat-role.component';
import { AddSubSubCatRoleComponent } from './add-sub-sub-cat-role/add-sub-sub-cat-role.component';
import { AuthGuard } from 'src/app/auth/auth.guard';








const routes: Routes = [
  {
    path: '',
    component: UsersRolesHomeComponent,
     canActivate: [AuthGuard],
    data: {
      allowedRoles: ['admin', 'bizly']
    },
    children: [
    
      {
        path: '',
        component: UsersRolesPageComponent
      },
      {
        path: 'edit-roles',
        component: EditRolesComponent
      },
      {
        path: 'create-user',
        component: CreateUserComponent
      },
      {
        path: 'create-roles-comp',
        component: CreateRolesCompComponent
      },
      {
        path: 'edit-users',
        component: EditUsersComponent
      },
      {
        path: 'create-pos-user',
        component: CreatePosUserComponent
      },
      {
        path: 'create-pos-role',
        component: CreatePosRoleComponent
      },
      {
        path: 'view-pos-user',
        component: ViewPosUserComponent
      },
    ]
  }
]


@NgModule({
  declarations: [UsersRolesHomeComponent, UsersRolesPageComponent, EditRolesComponent, CreateUserComponent, CreateRolesCompComponent, EditUsersComponent, CreatePosUserComponent, CreatePosRoleComponent, ViewPosUserComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    NgbTabsetModule,
     NgbTooltipModule,
      NgbPopoverModule,
      FileUploadModule,
      NgxSpinnerModule,
      MaterialModule,            
      MatDatepickerModule,       
      MatNativeDateModule,        
      TreeModule.forRoot(),  
    
    RouterModule.forChild(routes),
  ]
})
export class UsersRolesModule { }
