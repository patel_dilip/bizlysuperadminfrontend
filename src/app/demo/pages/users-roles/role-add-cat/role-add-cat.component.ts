import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-role-add-cat',
  templateUrl: './role-add-cat.component.html',
  styleUrls: ['./role-add-cat.component.scss']
})
export class RoleAddCatComponent implements OnInit {
  AddRoleForm:FormGroup;
  constructor(@Inject(MAT_DIALOG_DATA) public root_id, private user_roleService:UsersRolesService, private formbuilder: FormBuilder) { }

  ngOnInit() {
    this.AddRoleForm=this.formbuilder.group({
      name:[''],      
      children:[[]]
     
    })

   
  }
saveRootRole(){
  let obj=this.AddRoleForm.value;
  console.log('before post object', obj);
  
  this.user_roleService.postParentRootCategory(obj).subscribe(res=>{
    console.log("poat res",res);
    
    if(res['success']){
      Swal.fire('Role Added Succesfully','','success')
    }
    else{
      Swal.fire('Failed to Add Role','','warning')
    }
  })
}

}
