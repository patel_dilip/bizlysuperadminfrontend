import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleAddCatComponent } from './role-add-cat.component';

describe('RoleAddCatComponent', () => {
  let component: RoleAddCatComponent;
  let fixture: ComponentFixture<RoleAddCatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleAddCatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleAddCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
