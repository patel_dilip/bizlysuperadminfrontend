import { Component, OnInit, Inject } from '@angular/core';
import { MainService } from 'src/app/_services/main.service';
import { FormGroup, FormBuilder, Form } from '@angular/forms';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-select-role',
  templateUrl: './select-role.component.html',
  styleUrls: ['./select-role.component.scss']
})
export class   SelectRoleComponent implements OnInit {
  setRolesForm:FormGroup;
  rolesForm:FormGroup;
  firstLevel: any;
  size = 12;
  showRolesCategories: boolean = false;
  roles: any;
  rolesData:any=[]
  newRolesData: any=[];
  constructor(private main: MainService,
    private formbuilder:FormBuilder,
    private UserRole_Service:UsersRolesService,
    @Inject(MAT_DIALOG_DATA) public dataRoleName) {
console.log("presnt role name",dataRoleName);

    this.main.getAllEstablishment().subscribe(data => {
      console.log(data);
      this.firstLevel = data
    });
    //************get all role********

    
    this.UserRole_Service.getAllRole().subscribe(data => {

      this.rolesData = data['data']
      console.log("get all created roles", this.rolesData);
    
    console.log(this.rolesData);
    
    })

  
   
    
    
      
      

   }

  ngOnInit() {
    this.rolesForm=this.formbuilder.group({
      root:[''],
      sub:['']
    });
    this.setRolesForm=this.formbuilder.group({
      role:['']
    })
this.setRolesForm.patchValue({
  role: this.dataRoleName
})
   
  }
//***********role category*************** */
rolesCategory(event) {
  console.log(event.srcElement.checked);
  if (event.srcElement.checked == true) {
    this.size = 8
    this.showRolesCategories = true
  } else {
    this.size = 12
    this.showRolesCategories = false

  }
}
selectRole(event,val){
//console.log("role selected",event);
console.log("value",val);
this.roles=val;
}

}
