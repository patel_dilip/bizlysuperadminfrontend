import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';

import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';
import { Router, RoutesRecognized } from '@angular/router';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { ViewRolesComponent } from '../view-roles/view-roles.component';
import { ViewUsersComponent } from '../view-users/view-users.component';
import { MainService } from 'src/app/_services/main.service';
import { ViewPosRoleComponent } from '../view-pos-role/view-pos-role.component';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { NgxSpinnerService } from "ngx-spinner";
import { filter, pairwise } from 'rxjs/operators';
import { RoleAddCatComponent } from '../role-add-cat/role-add-cat.component';
import { AddCatRoleNameComponent } from '../add-cat-role-name/add-cat-role-name.component';
import { AddSubCatRoleComponent } from '../add-sub-cat-role/add-sub-cat-role.component';
import { AddSubSubCatComponent } from 'src/app/PopoversList/add-sub-sub-cat/add-sub-sub-cat.component';


//**************Bizly to Bizly***************/
//Interface for Role 
export interface rolesElement {
  role_ID: string

  role_name: string
  role_salary: number
  total_users: number
  // role_access_points:number
  updatedAt: string
  createdAt: string
  status: Boolean;

  action: string

}
//Interface for User
export interface usersElement {
  _id: number
  photo: string
  employee_name: string
  role: string
  employee_rating: number
  bizUser_email: string
  email_id: string
  contact_number: number
  joining_date: number
  status: boolean
  action: string

}


//*******************Bizly To POS Merchant********************/
//Interface for Role 
export interface rolesMerchantElement {
  role_ID: number
  role_name: string
  //access_control: number
  status: boolean
  //restaurant_UsingRole: number

}

//Interface for Users
export interface usersMerchantElement {
  restaurant_name: string
  restaurant_role: string
  restaurant_userLimit: number
  restaurant_users: string
  outlet_id: number
  activities: string

}


const usersMerchantData: usersMerchantElement[] = [

  { restaurant_name: "Orchid", restaurant_role: "Manager", restaurant_userLimit: 70, restaurant_users: "23", outlet_id: 1, activities: "User Limits Chenged to 70 on 15/2/2020" },
  { restaurant_name: "Durga", restaurant_role: "Manager", restaurant_userLimit: 70, restaurant_users: "34", outlet_id: 2, activities: "User Limits Chenged to 70 on 15/2/2020" },


]
//**Files upload */


@Component({
  selector: 'app-users-roles-page',
  templateUrl: './users-roles-page.component.html',
  styleUrls: ['./users-roles-page.component.scss']
})
export class UsersRolesPageComponent implements OnInit {
  index: number
  size = 12;
  firstLevel: any;
  showRolesCategories: boolean = false;
  treeForm: FormGroup;
  roles: any;
  current_user: any
  obj: any;
  Roles: any;
  rolesData: any = [];
  rolesPosData: any = [];
  usersData: any = [];
  userPosData: any = [];
  //roles table



  // index Variable 
  outerIndex = new FormControl(0);
  bizbizIndex = new FormControl(0);
  posbizIndex = new FormControl(0);


  //*******Tree variable declaration**********
  nodes: any = [];
  options = {};
  //********other tree variable declaration***** */
  roleRootCategory: any = [];
  roleRootCategoryGetData: any = [];

  // displayedRolesColumns: string[] = ['role_ID', 'role_name','role_salary','total_users','role_access_points','updatedAt', 'createdAt', 'status','action'];
  displayedRolesColumns: string[] = ['role_ID', 'role_name', 'role_salary', 'total_users', 'updatedAt', 'createdAt', 'status', 'action'];

  dataSourceRoles: MatTableDataSource<rolesElement>
  @ViewChild("rolespaginator", { static: true }) rolespaginator: MatPaginator;
  @ViewChild("rolessort", { static: true }) rolessort: MatSort;

  //users table

  displayedUsersColumns: string[] = ['_id', 'photo', 'employee_name', 'role', 'employee_rating', 'bizUser_email', 'email_id', 'contact_number', 'joining_date', 'status', 'action'];
  dataSourceUsers: MatTableDataSource<usersElement>
  @ViewChild("userspaginator", { static: true }) userspaginator: MatPaginator;
  @ViewChild("userssort", { static: true }) userssort: MatSort;


  //************Bizly To POS Merchant****************/
  //Roles  Table

  displayedMerchantRolesColumns: string[] = ['role_ID', 'role_name', 'status'];
  dataSourceMerchantRoles: MatTableDataSource<rolesMerchantElement>
  @ViewChild("rolesMechantpaginator", { static: true }) rolesMechantpaginator: MatPaginator;
  @ViewChild("rolesMerchantsort", { static: true }) rolesMerchantsort: MatSort;
  //Users  Table
  displayedMerchantUsersColumns: string[] = ['restaurant_name', 'restaurant_role', 'restaurant_userLimit', 'restaurant_users', 'activities'];
  dataSourceMerchantUsers: MatTableDataSource<usersMerchantElement>
  @ViewChild("usersMechantpaginator", { static: true }) usersMechantpaginator: MatPaginator;
  @ViewChild("usersMerchantsort", { static: true }) usersMerchantsort: MatSort;

  // *********Create user & access controls**********

  userid: any
  newUserForm: FormGroup
  AllLogins: any = []

  constructor(private ngxSpinner: NgxSpinnerService, private main: MainService, private router: Router, private formbuilder: FormBuilder,
    private dialog: MatDialog, private UserRole_Service: UsersRolesService, private restServ: RestoService) {

    // *********Create user & access controls**********
    this.current_user = JSON.parse(localStorage.getItem('loginUser'))

    let user = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = user._id


    // get All Users access poinst
    this.UserRole_Service.getallUsers().subscribe(data => {
      console.log('All Data', data);
      this.AllLogins = data['data']
    })

    this.newUserForm = this.formbuilder.group({
      role: ['user'],
      userName: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      mobileno: ['', [Validators.required]],
      password: ['', [Validators.required]],
      userid: this.userid
    })



    // *************Get All Root Type***********
    this.getRoleRootType()

    // **********localstorage Outer Index*********     
    let outidx = localStorage.getItem('outerIndex')

    if (outidx != undefined) {
      console.log('outer index value', outidx);
      this.outerIndex.setValue(outidx)
    }

    // **********localstorage Bizly to Bizly Index*********     
    let bizidx = localStorage.getItem('bizbizIndex')
    if (bizidx != undefined) {
      console.log('bizly to bizly index', bizidx);
      this.bizbizIndex.setValue(bizidx)
    }

    // **********localstorage Pos to Bizly Index*********     
    let posidx = localStorage.getItem('posbizIndex')
    if (posidx != undefined) {
      console.log('pos to bizly index', posidx);
      this.posbizIndex.setValue(posidx)
    }



    //**********get all role****************/
    console.log("roles", this.rolesData);
    this.UserRole_Service.getAllRole().subscribe(data => {
      //this.ngxSpinner.show();
      this.rolesData = data['data']

      console.log("get all created roles", this.rolesData);
      this.dataSourceRoles = new MatTableDataSource(this.rolesData);
      this.dataSourceRoles.paginator = this.rolespaginator;
      this.dataSourceRoles.sort = this.rolessort;
      // this.ngxSpinner.hide();
    });


    // ***********get all Users****************//
    this.UserRole_Service.getAllUser().subscribe(data => {
      //this.ngxSpinner.show();
      this.usersData = data['data']
      console.log('userData', this.usersData);

      this.dataSourceUsers = new MatTableDataSource(this.usersData);
      this.dataSourceUsers.paginator = this.userspaginator;
      this.dataSourceUsers.sort = this.userssort;
      //this.ngxSpinner.hide();
    });

    //**********get all pos role******* */
    this.UserRole_Service.getAllPosRole().subscribe(data => {
      //this.ngxSpinner.show();
      this.rolesPosData = data['data']

      console.log('userData', this.rolesPosData);

      this.dataSourceMerchantRoles = new MatTableDataSource(this.rolesPosData);
      this.dataSourceMerchantRoles.paginator = this.rolesMechantpaginator;
      this.dataSourceMerchantRoles.sort = this.rolesMerchantsort;
      //this.ngxSpinner.hide();
    })

    this.dataSourceMerchantUsers = new MatTableDataSource(usersMerchantData);




    this.main.getAllEstablishment().subscribe(data => {
      console.log(data);

      this.firstLevel = data
    });
    //******call tree function**** */
    this.getnode()

    // ***********get all Users****************//
    // this.UserRole_Service.getAllUser().subscribe(data => {
    // ***********get all Users****************//
    // this.UserRole_Service.getAllUser().subscribe(data => {
    //   this.ngxSpinner.show();
    //   this.usersData = data['data']
    // ***********get all Users****************//
    // this.UserRole_Service.getAllUser().subscribe(data => {
    //   this.ngxSpinner.show();
    //   this.usersData = data['data']

    //   console.log('userData', this.usersData);
    //   this.ngxSpinner.hide();

    // });

    //   console.log('userData', this.usersData);
    //   this.ngxSpinner.hide();

    // });
    //   this.ngxSpinner.show();
    //   this.usersData = data['data']

    //   console.log('userData', this.usersData);
    //   this.ngxSpinner.hide();

    // });


  }

  ngOnInit() {

    this.treeForm = this.formbuilder.group({
      root: [''],
      sub: ['']
    });


  }

  // *************************CREATE ACCESS POINTS FUNCTIONS*******************
  signup() {
    console.log("users", this.newUserForm.value);


    // add user
    this.UserRole_Service.addUser(this.newUserForm.value).subscribe(res => {
      console.log("response", res);
      if (res['success']) {
        Swal.fire("User Added Successfully", '', 'success')
        this.newUserForm.reset()
        this.newUserForm.patchValue({
          userid: this.userid,
          role: "user"
        })


        // get All Users
        this.UserRole_Service.getallUsers().subscribe(data => {
          console.log('All Data', data);
          this.AllLogins = data['data']
        })
      }
      else {
        Swal.fire("Failed to Add User")
      }
    })


    // // get All Users
    // this.UserRole_Service.getallUsers().subscribe(data=>{
    //   console.log('All Data',data);
    //   this.AllLogins = data['data']
    // })

  }

  // ******************Outer Index Function*******************
  onOuterIndex(evt) {
    console.log('outer index', evt);
    localStorage.setItem('outerIndex', evt)
  }

  // ******************bizly to bizly Index Function*******************
  onbizbizIndex(evt) {
    console.log('biztobiz index', evt);
    localStorage.setItem('bizbizIndex', evt)
  }

  // ******************bizly to pos Index Function*******************
  onposbizIndex(evt) {
    console.log('postobiz index', evt);
    localStorage.setItem('posbizIndex', evt)
  }

  //******************pop up for view roles*********************
  openDialog_ViewRoles(element): void {
    console.log('view roles data', element);
    const dialogRef = this.dialog.open(ViewRolesComponent, {
      disableClose: true,
      width: 'auto',
      height: '550px',
      data: element

    });


  }
  //******************pop up for view roles*********************
  openDialog_ViewUers(element): void {
    console.log('view user ', element);

    const dialogRef = this.dialog.open(ViewUsersComponent, {
      disableClose: true,
      width: '50%',
      height: '500px',
      data: element

    });


  }
  //******************pop up for view roles*********************
  openDialog_ViewPosRole(element): void {
    console.log('view user ', element);

    const dialogRef = this.dialog.open(ViewPosRoleComponent, {
      disableClose: true,
      width: '50%',
      height: '500px',
      data: element

    });


  }


  //***********Navigate to Edit Roles************/
  EditRole(element) {
    this.UserRole_Service.rolesEdit = element;
    this.router.navigate(['users-roles/edit-roles']);
  }
  //******************Navigate to Edit Users**************/
  EditUser(element) {
    this.UserRole_Service.usersEdit = element;
    this.router.navigate(['users-roles/edit-users']);
  }
  //******************Navigate to Edit Users**************/
  ViewUser() {
    this.router.navigate(['users-roles/view-pos-user'])
  }


  //************Navigation for Create User*****************/
  CreateUser() {
    this.router.navigate(['users-roles/create-user']);
  }
  //************Navigation for create role***********/
  CreateRole() {
    this.router.navigate(['users-roles/create-roles-comp']);

  }
  //************Navigation for Create POS User*****************/
  CreatePOsUser() {
    this.router.navigate(['users-roles/create-pos-user']);
    console.log("pos user");

  }
  //************Navigation for Create POS User*****************/
  CreatePosRole() {
    this.router.navigate(['users-roles/create-pos-role']);
    console.log("pos user");

  }
  //***********apply filter*****************/
  rolesapplyFilter(filterValue: string) {
    this.dataSourceRoles.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceRoles.paginator) {
      this.dataSourceRoles.paginator.firstPage();
    }
  }

  usersaapplyFilter(filterValue: string) {
    this.dataSourceUsers.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceUsers.paginator) {
      this.dataSourceUsers.paginator.firstPage();
    }
  }
  rolesCategory(event) {
    console.log(event.srcElement.checked);
    if (event.srcElement.checked == true) {
      this.size = 8
      this.showRolesCategories = true
    } else {
      this.size = 12
      this.showRolesCategories = false

    }
  }

  //***********  Soft Delete Role************
  roleSoftDelete(element, event) {
    console.log(event.checked);
    console.log(element._id);
    this.UserRole_Service.deleteRole(element._id, { "status": event.checked }).subscribe(res => {
      console.log(res);

      if (res['success'] == true) {
        Swal.fire('Status Changed Successfully', '', 'success')
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
    })
  }
  //***********soft delete User*********** */

  userSoftDelete(element, event) {
    console.log(event.checked);
    console.log(element._id);
    this.UserRole_Service.deleteUser(element._id, { "status": event.checked }).subscribe(res => {
      console.log(res);

      if (res['success'] == true) {
        Swal.fire('Status Changed Successfully', '', 'success')
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
    })
  }
  //************soft delete for pos role********** */

  posRoleSoftDelete(element, event) {
    console.log(event.checked);
    console.log(element._id);
    this.UserRole_Service.deletePosRole(element._id, { "status": event.checked }).subscribe(res => {
      console.log(res);

      if (res['success'] == true) {
        Swal.fire('Status Changed Successfully', '', 'success')
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
    })
  }
  //***************select role ********* */
  selectRole(event, val) {
    //console.log("role selected",event);
    console.log("value", val);
    this.roles = val;
  }
  //**********tree code function***** */
  getnode() {
    this.nodes = [{
      id: '1',
      parent_id: '0',
      name: 'Super Admin',
      is_selected: 'false',
      children: [
        {

          id: '2',
          parent_id: '1',
          name: 'CEO',
          is_selected: 'false',
          children: [
            {
              id: '3',
              parent_id: '2',
              name: 'Manager',
              is_selected: 'false',
              children: [
                {
                  id: '4',
                  parent_id: '3',
                  name: 'Assistant Manager',
                  is_selected: 'false',
                  children: [
                    {
                      id: '5',
                      parent_id: '4',
                      name: 'HR',
                      is_selected: 'false',
                      children: [
                        {
                          id: '6',
                          parent_id: '5',
                          name: 'Secretory',
                          is_selected: 'false',
                          children: [
                            {
                              id: '7',
                              parent_id: '6',
                              name: 'Employee',
                              is_selected: 'false',
                              children: []
                            }
                          ]
                        }

                      ]
                    }
                  ]
                }
              ]

            }

          ]
        },

      ]
    }]
    console.log("this is tree", this.nodes);

  }


  // ****** click carrot function**********

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }
  // open dialog add root category code

  openAddCateg(): void {
    const dialogRef = this.dialog.open(RoleAddCatComponent, {
      width: '450px',


    },
    )
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      this.getRoleRootType()
    });


  }

  // open category add category name

  openCategoryName(root) {


    let root_id = root._id
    console.log('current root', root._id);
    const dialogRef = this.dialog.open(AddCatRoleNameComponent, {
      width: 'auto',
      data: root_id
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('category name closed', result);
      this.getRoleRootType()

    });

  }
  // open child category 
  openSubCategory(root, cat) {
    console.log('root details', root._id);

    console.log('category details', cat._id);

    const dialogRef = this.dialog.open(AddSubCatRoleComponent, {
      width: '540px',
      data: { 'root': root._id, "cat": cat._id }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.getRoleRootType()
    })
  }

  // open child child category 
  openSubSubCategory(root, child) {

    // console.log('root', root._id);
    // console.log('child', child._id)

    const dialogRef = this.dialog.open(AddSubSubCatComponent, {
      width: '540px',
      data: { 'root': root._id, 'child': child._id }
    })

    dialogRef.afterClosed().subscribe(result => {
      this.getRoleRootType()
    })
  }
  // *******GET ESTABLISHMENT ROOT TYPE*********
  getRoleRootType() {
    this.UserRole_Service.getRoleRootCategory().subscribe(data => {
      this.roleRootCategory = data['data']
      console.log('roleRootCategory', this.roleRootCategory);

    });
  }
  // *******post ROLE ROOT TYPE*********
  postRoleRootType(obj) {

    this.UserRole_Service.postParentRootCategory(obj).subscribe(data => {
      this.roleRootCategory = data
      console.log('post parent root of role', this.roleRootCategory);

    })
  }
  //******get ROLE ROOT TYPE****** */
  getAllRoleRootType() {
    this.UserRole_Service.getRoleRootCategory().subscribe(data => {
      this.roleRootCategoryGetData = data
      console.log('get parent root of role', this.roleRootCategoryGetData);

    })
  }

  // *****permanent delete tree********
  deleteTree() {
    this.UserRole_Service.permanentDelete().subscribe(res => {
      console.log('Delete Response', res);
      this.getAllRoleRootType()
    })
  }
}
