import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersRolesPageComponent } from './users-roles-page.component';

describe('UsersRolesPageComponent', () => {
  let component: UsersRolesPageComponent;
  let fixture: ComponentFixture<UsersRolesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersRolesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersRolesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
