import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubCatRoleComponent } from './add-sub-cat-role.component';

describe('AddSubCatRoleComponent', () => {
  let component: AddSubCatRoleComponent;
  let fixture: ComponentFixture<AddSubCatRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubCatRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubCatRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
