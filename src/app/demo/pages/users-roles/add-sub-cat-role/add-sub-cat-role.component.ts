import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-add-sub-cat-role',
  templateUrl: './add-sub-cat-role.component.html',
  styleUrls: ['./add-sub-cat-role.component.scss']
})
export class AddSubCatRoleComponent implements OnInit {

  rootparentid;
  childernid;

  constructor(@Inject(MAT_DIALOG_DATA) public  dataObj) {
    console.log('data object', dataObj)
   }

  ngOnInit() {
  }

}
