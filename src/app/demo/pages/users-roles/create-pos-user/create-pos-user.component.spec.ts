import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePosUserComponent } from './create-pos-user.component';

describe('CreatePosUserComponent', () => {
  let component: CreatePosUserComponent;
  let fixture: ComponentFixture<CreatePosUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePosUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePosUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
