import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from "ngx-spinner";
import { SelectRoleComponent } from '../select-role/select-role.component';

//*****Joining letter upload*******/
const JoiningLetUploader= environment.base_Url+"userrolePOS/uploadcorporatejoiningletter";
//*****NDA file upload*******/
const NDAdocumentfile = environment.base_Url+"userrolePOS//uploaddocumentfile";
// *** Photos Upload*//
const profilePic = environment.base_Url+"userrolePOS/uploadpospersonaldetails";
//**Files upload */
const aadharDoc = environment.base_Url+"userrolePOS/uploadaadharcard";
const driving = environment.base_Url+"userrolePOS/uploadlicence";
const passport = environment.base_Url+"userrolePOS/uploadpassport";
const joining = environment.base_Url+"userrolePOS/uploadjoiningletter";
const docs = environment.base_Url+"userrolePOS/uploaddocumentfile";

@Component({
  selector: 'app-create-pos-user',
  templateUrl: './create-pos-user.component.html',
  styleUrls: ['./create-pos-user.component.scss']
})
export class CreatePosUserComponent implements OnInit {
  userForm:FormGroup;
  PersonalDetailsForm:FormGroup;
  WorkDetailsForm:FormGroup;
  BankPaymentDetailsForm:FormGroup;
  DocumentsForm:FormGroup;
  CorporatDetailsForm:FormGroup;
  AssetDetailsForm:FormGroup; 
  CredentialsForm:FormGroup;
  AccessControlForm :FormGroup;
  supplies_cat:[];
  State_list_per=['Maharashtra','UP'];
  State_list=['Maharashtra','UP'];
  City_list_per=['pune','Amaravti','Mumbai'];
  City_list=['pune','Amaravti','Mumbai'];
  assign_city_list=['Amaravti','Nagpur'];
  blood_group_list = ['A+', 'A-', 'B-', 'B+', 'AB+', 'AB-', 'O+', 'O-'];
  allownces_list=['Petrol','Mobile Recharg','Medical'];
  deduction_list=['Petrol','Mobile Recharg','Medical'];
  overtime_list=['1 hour','2 hour','3 hour','4 hour'];
  resultRole:any=[];

 mondayOpenTime: any;
 mondayCloseTime: any;
 extramonOpenTime: any;
 extamonCloseTime: any;


 docArr: any = [0];
 OccuranceCount: number;
 addCount: number;
 Count:number;
 assetArr:any=[0]

 restaurants_types = [];
 isShowRole:boolean=false;

 isShowSalary:boolean=false;
 showAllowncesField:boolean=false;
 showDeductionField:boolean=false;
 showOverTimeField:boolean=false;

 showAssetRow:boolean=false;

 uploader: FileUploader;
 response: string;

 //working detail




 isAllownces =false;
 isDeduction =false;
 isOvertime =false;

 isMon = true;
 isTues = false;
 isWed = false;
 isThur = false;
 isFri = false;
 isSat = false;
 isSun = false;

 monAddHr = false
 tueAddHr = false
 wedAddHr = false
 thurAddHr = false
 friAddHr = false
 satAddHr = false
 sunAddHr = false

 userid:any;

//**auto generate user code */
 usersdata: any= [];
 usercode: any

  // photo upload urls

  public profilePicUploader: FileUploader = new FileUploader({url:profilePic, itemAlias: 'personaldetails'})
  //NDA file upload urls

  public NDAPicUploader:FileUploader = new FileUploader({url:NDAdocumentfile,itemAlias: 'documentfile'})
  //joining letter upload urls

 public joiningLetterUploader:FileUploader = new FileUploader({url:JoiningLetUploader,itemAlias: 'corporatejoiningletter'})
    // file upload urls
    public addharUploader: FileUploader = new FileUploader({url: aadharDoc, itemAlias: 'aadharcard'})
    public drivingUploader: FileUploader = new FileUploader({url: driving, itemAlias: 'licence'})
    public passportUploader: FileUploader = new FileUploader({url: passport, itemAlias: 'passport'})
    //public joiningUploader: FileUploader = new FileUploader({url: joining, itemAlias: 'joiningletter'})
    public docsUploader: FileUploader = new FileUploader({url: docs, itemAlias: 'documentfile'})
  profilepic: any;
  propicStatus: boolean;
  NDAdocumentfile:any;

  JoiningLetUploader:any;
  aadharRes: any;
  aadharStatus: boolean;
  LicenRes: any;
  LicStatus: boolean;
  passRes: any;
  passStatus: boolean;
  documentRes: any;
  docStatus: boolean;
  joinRes: any;
  joinStatus: boolean;
  ndaPicStatus: boolean;
  joiningLetterStatus:boolean;

  country_json=[];
  state=[];
  city=[];
  state_list: any;

  allUsers:any=[];

  constructor(private router:Router,
    private formbuilder:FormBuilder,
    private dialog: MatDialog,
    private UserRole_Service:UsersRolesService,
    private ngxSpinner: NgxSpinnerService) {
      // this.UserRole_Service.getAllPosUser().subscribe(data=>{
      //   this.allUsers = data['data']
        
      //   if(this.allUsers.length == 0){
      //   console.log('length is zero', this.allUsers);
        
      //   let len = this.allUsers.length+1 //length if array is empty 0+1
      //   let str = len.toString() // converting 1 to string '1'
      //   let size = 3 // size of code
      //   this.usercode = 'BBU'+str.padStart(size,'0') // padding with 00 to left ex: 001
      //   console.log('user code', this.usercode);
      //   }
      //   else{
      //   console.log(this.allUsers.length);
      //   let index = this.allUsers.length-1
      //   let per=this.allUsers[index].personal_details
      //   let last_code = per.user_code
      //   console.log('last_code', last_code);
        
      //   let str = last_code.slice(3)
      //   console.log('string', str);
      //   let digit = parseInt(str)
      //   digit = digit+1
      //   let str1 = digit.toString()
      //   let size = 3
        
      //   this.usercode = 'BBU'+str1.padStart(size,'0')
      //   console.log('User code', this.usercode);
      //   }
      //   });

      this.UserRole_Service.getAllRole().subscribe(data=>{
        this.usersdata = data['data']
  
        if(this.usersdata.length==0){
          console.log('length is zero', this.usersdata);
  
          let len = this.usersdata.length+1   //length if array is empty 0+1
          let str = len.toString()            // converting 1 to string '1'
          let size = 3                        // size of code
          this.usercode = 'BBR'+str.padStart(size,'0')  // padding with 00 to left ex: 001
          console.log('user code',  this.usercode);
          
          
        }
        else{
          console.log(this.usersdata.length);
          let index = this.usersdata.length-1
  
          let last_code = this.usersdata[index].role_ID
          console.log('last_code', last_code);
          let str = last_code.slice(3)
          console.log('string', str);
          let digit = parseInt(str)
          digit = digit+1
          let str1 = digit.toString()
          let size = 3
          this.usercode = 'BBR'+str1.padStart(size,'0')
          console.log('role code', this.usercode);
          
        
          
          
        }
  
      })
     }

  ngOnInit() {
    this.PersonalDetailsForm=this.formbuilder.group({
      user_code:[''],
      employee_name:[''],
      contact_number:[''],
      email_id:[''],
      photo:[''],
      first_address:[''],
      second_address:[''],
      country:[''],
      state:[''],
      city:[''],
      pin_code:[''],
      gender:[''],
      birth_date:[''],
      blood_group:[''],
      joiningDate:[''],
      marrital_status:[''],
      emergency_contact_personName:[''],
      emg_contact_number:[''],
      mother_Name:[''],
      father_husbandName:[''],
      fam_Contact_no:[''],
      assigned_role:[''],
      userid:this.userid,
      status:true
    });

    this.WorkDetailsForm=this.formbuilder.group({

      openMon: [''],
      openTue: [''],
      openWed: [''],
      openThur: [''],
      openFri: [''],
      openSat: [''],
      openSun: [''],

      extraopenMon: [''],
      extraopenTue: [''],
      extraopenWed: [''],
      extraopenThur: [''],
      extraopenFri: [''],
      extraopenSat: [''],
      extraopenSun: [''],

      closeMon: [''],
      closeTue: [''],
      closeWed: [''],
      closeThur: [''],
      closeFri: [''],
      closeSat: [''],
      closeSun: [''],

      extracloseMon: [''],
      extracloseTue: [''],
      extracloseWed: [''],
      extracloseThur: [''],
      extracloseFri: [''],
      extracloseSat: [''],
      extracloseSun: [''],
      Sanctioned_Leaves:[''],
      
      joining_letter:[''],
      joining_date:[''],
      assign_location:[''],
      assign_city:[''],
      responsibilities:[''],
      NDA_file:[''],
     // userid:['']

    });
    this.BankPaymentDetailsForm=this.formbuilder.group({
      account_number:[''],
      IFSC_code:[''],
      bank_name:[''],
      bank_address:[''],
      bank_state:[''],
      bank_city:[''],
      salary_amount:[''],
      salary:[''],
      allowances:[''],
      add_allownces:[''],
      add_deduction:[''],
      select_overtime:[''],
      incentive_amount:[''],
      net_salary:[''],
      allownces_name:[''],
      allownces_amount:[''],
      deduction_name:[''],
      deduction_amount:['']
    });
  
    this.DocumentsForm=this.formbuilder.group({
      aadhar_number:[''],
      aadhar_upload:[''],
      licence_number:[''],
      licence_upload:[''],
      passport_number:[''],
      passport_upload:[''],
      joining_letter:[''],
      joining_letter_file:[''],
      add_document:[''],
      add_document_file:[''],
      document_name:[''],
      name_document_file:['']
    
    });
    this.AccessControlForm=this.formbuilder.group({

    })

    this.AssetDetailsForm=this.formbuilder.group({

      assetsArr: this.formbuilder.array([]) // form array first step
      });
      
      // By Default 1 add row
      this.addAssetRow()
    this.CredentialsForm=this.formbuilder.group({
      
      pos_username:[''],
      admin_password:[''],

    });
    this.userForm=this.formbuilder.group({
      user_id:['']

    })
//***********joining letter uploade***** */
   
this.joiningLetterUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
  console.log('FileUpload:uploaded:', item, status, response);
}
//**************NDA file upload */

this.NDAPicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
console.log('FileUpload:uploaded:', item, status, response);
}
    // ***********************************************

    // ********profile pic upload***********/
this.profilePicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
  console.log('FileUpload:uploaded:', item, status, response);

  this.profilepic = response;
  
  if(status==200){
    Swal.fire('Profile Picture Uploaded Successfully','','success')
    this.propicStatus = true

    // patch value
    this.PersonalDetailsForm.patchValue({
      photo: response
    })

    console.log(this.PersonalDetailsForm.value);
    
  }
  else{
    Swal.fire('Failed to Upload!','','warning')
    this.propicStatus = false
  }

  this.ngxSpinner.hide()
}

// ********joining letter upload***********/
this.joiningLetterUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
  console.log('FileUpload:uploaded:', item, status, response);

  this.JoiningLetUploader = response;
  
  if(status==200){
    Swal.fire('NDA File Uploaded Successfully','','success')
    this.joiningLetterStatus = true

    // patch value
    this.WorkDetailsForm.patchValue({
      file: response
    })

    console.log(this.WorkDetailsForm.value);
    
  }
  else{
    Swal.fire('Failed to Upload!','','warning')
    this.joiningLetterStatus = false
  }

  this.ngxSpinner.hide()
}
// ********NDA pic upload***********/
this.NDAPicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
  console.log('FileUpload:uploaded:', item, status, response);

  this.NDAdocumentfile = response;
  
  if(status==200){
    Swal.fire('NDA File Uploaded Successfully','','success')
    this.ndaPicStatus = true

    // patch value
    this.WorkDetailsForm.patchValue({
      file: response
    })

    console.log(this.WorkDetailsForm.value);
    
  }
  else{
    Swal.fire('Failed to Upload!','','warning')
    this.ndaPicStatus = false
  }

  this.ngxSpinner.hide()
}
  //**************aadhar upload */

  this.addharUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.aadharRes = response

    // Check Condition upload successful
    if(status == 200){
      Swal.fire('Aadhar Uploaded Successfully','','success')
      this.aadharStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        aadhar_upload: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{
      Swal.fire('Failed to Upload!','','warning')
      this.aadharStatus = false
    }
    this.ngxSpinner.hide()
  }
  
   //**************dRIVING LICENSE upload */

   this.drivingUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.LicenRes = response
    this.ngxSpinner.hide()
    // Check Condition upload successful
    if(status == 200){

     
      Swal.fire('License Uploaded Successfully','','success')
      this.LicStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        licence_upload: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{

      this.ngxSpinner.hide()
      Swal.fire('Failed to Upload!','','warning')
      this.LicStatus = false
    }

    this.ngxSpinner.hide()
  }
   //**************pASSPORT upload */

   this.passportUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.passRes = response

    // Check Condition upload successful
    if(status == 200){

     
      Swal.fire('Passport Uploaded Successfully','','success')
      this.passStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        passport_upload: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{

      this.ngxSpinner.hide()
      Swal.fire('Failed to Upload!','','warning')
      this.passStatus = false
    }

    this.ngxSpinner.hide()
  }


  

   this.docsUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.documentRes = response

    // Check Condition upload successful
    if(status == 200){

     
      Swal.fire('Document Uploaded Successfully','','success')
      this.docStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        add_document_file: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{

      this.ngxSpinner.hide()
      Swal.fire('Failed to Upload!','','warning')
      this.docStatus = false
    }

    this.ngxSpinner.hide()
  }
  }

  // **NG ONIT FINISH

get assetForm(){
  return this.AssetDetailsForm.controls.assetsArr as FormArray /// **** Second Step in Form Array
  }
  
  
  addAssetRow(){ /// *** Third and Final Step in Form Array
  const assetrow = this.formbuilder.group({
  assets_name: [''],
  assets_quantity: [''],
  assets_date:['']
  })
  
  this.assetForm.push(assetrow)
  
  console.log(this.AssetDetailsForm.value)
  }
  
  deleteAssetRow(index){
  this.assetForm.removeAt(index)
  
  console.log(this.AssetDetailsForm.value)
  }
  
  showAssetvalue(){
  
  console.log(this.AssetDetailsForm.value)
  }

   // ******************
  // WEEK RADIO BUTTON TIMES 

  onMonday(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isMon = true;
    }
    else {
      this.isMon = false;
    }

  }

  onTuesday(evt) {
    if (evt.checked) {
      this.isTues = true;
    }
    else {
      this.isTues = false;
    }
  }

  onWednesday(evt) {
    if (evt.checked) {
      this.isWed = true;
    }
    else {
      this.isWed = false;
    }
  }

  onThursday(evt) {
    if (evt.checked) {
      this.isThur = true;
    }
    else {
      this.isThur = false;
    }
  }


  onFriday(evt) {
    if (evt.checked) {
      this.isFri = true;
    }
    else {
      this.isFri = false;
    }
  }

  onSaturday(evt) {
    if (evt.checked) {
      this.isSat = true;
    }
    else {
      this.isSat = false;
    }
  }

  onSunday(evt) {
    if (evt.checked) {
      this.isSun = true;
    }
    else {
      this.isSun = false;
    }
  }
    // after click on sameday 
    onSameAsMonday(chk) {

      console.log('checked vlaue', chk)
      if (this.monAddHr) {
        this.tueAddHr = true;
        this.wedAddHr = true;
        this.thurAddHr = true;
        this.friAddHr = true;
        this.satAddHr = true;
        this.sunAddHr = true;
      }
  
      // TIME VALUE SAME AS MONDAY
  
      this.mondayOpenTime = this.WorkDetailsForm.controls['openMon'].value
      this.mondayCloseTime = this.WorkDetailsForm.controls['closeMon'].value
  
      console.log('Extra open monday vlaue', this.WorkDetailsForm.controls['extraopenMon'].value);
      this.extramonOpenTime = this.WorkDetailsForm.controls['extraopenMon'].value
      this.extamonCloseTime = this.WorkDetailsForm.controls['extracloseMon'].value
  
  
      console.log('tIMES values ', this.mondayOpenTime);
  
      this.WorkDetailsForm.patchValue({
        openTue: this.mondayOpenTime,
        openWed: this.mondayOpenTime,
        openThur: this.mondayOpenTime,
        openFri: this.mondayOpenTime,
        openSat: this.mondayOpenTime,
        openSun: this.mondayOpenTime,
  
        extraopenTue: this.extramonOpenTime,
        extraopenWed: this.extramonOpenTime,
        extraopenThur: this.extramonOpenTime,
        extraopenFri: this.extramonOpenTime,
        extraopenSat: this.extramonOpenTime,
        extraopenSun: this.extramonOpenTime,
  
  
        closeTue: this.mondayCloseTime,
        closeWed: this.mondayCloseTime,
        closeThur: this.mondayCloseTime,
        closeFri: this.mondayCloseTime,
        closeSat: this.mondayCloseTime,
        closeSun: this.mondayCloseTime,
  
        extracloseTue: this.extamonCloseTime,
        extracloseWed: this.extamonCloseTime,
        extracloseThur: this.extamonCloseTime,
        extracloseFri: this.extamonCloseTime,
        extracloseSat: this.extamonCloseTime,
        extracloseSun: this.extamonCloseTime
  
      })
  
  
  
  
      //RADIO SAME AS MONDAY
      if (chk.target.checked) {
  
        this.isTues = this.isMon;
        this.isWed = this.isMon;
        this.isThur = this.isMon;
        this.isFri = this.isMon;
        this.isSat = this.isMon;
        this.isSun = this.isMon;
      }
  
      else {
        this.isTues = false;
        this.isWed = false;
        this.isThur = false;
        this.isFri = false;
        this.isSat = false;
        this.isSun = false;
      }
  
    }
 
    createNewElement(id) {
      // First create a DIV element.
      var txtNewInputBox = document.createElement('div');
  
      // Then add the content (a new input box) of the element.
      txtNewInputBox.innerHTML = "+<input type='time' value='16:00:00' > to <input type='time' value='23:00:00' >";
  
      // Finally put it where it is supposed to appear.
      if (id == 'mon') {
        // document.getElementById("monAddHTML").appendChild(txtNewInputBox);
        this.monAddHr = true;
      }
  
      if (id == 'tue') {
        // document.getElementById("tueAddHTML").appendChild(txtNewInputBox);
        this.tueAddHr = true;
      }
  
      if (id == 'wed') {
        // document.getElementById("wedAddHtml").appendChild(txtNewInputBox);
        this.wedAddHr = true;
      }
  
      if (id == 'thur') {
        // document.getElementById("thurAddHtml").appendChild(txtNewInputBox);
        this.thurAddHr = true;
      }
  
      if (id == 'fri') {
        // document.getElementById("friAddHtml").appendChild(txtNewInputBox);
        this.friAddHr = true;
      }
  
      if (id == 'sat') {
        // document.getElementById("satAddHtml").appendChild(txtNewInputBox);
        this.satAddHr = true;
      }
  
      if (id == 'sun') {
        // document.getElementById("sunAddHtml").appendChild(txtNewInputBox);
        this.sunAddHr = true;
      }
      //sunAddHtml
  
      console.log('Id ', id);
  
  
    }
  
    removeExtraTime(id) {
      if (id == 'mon') {
        // document.getElementById("monAddHTML").appendChild(txtNewInputBox);
        this.monAddHr = false;
      }
  
      if (id == 'tue') {
        // document.getElementById("tueAddHTML").appendChild(txtNewInputBox);
        this.tueAddHr = false;
      }
  
      if (id == 'wed') {
        // document.getElementById("wedAddHtml").appendChild(txtNewInputBox);
        this.wedAddHr = false;
      }
  
      if (id == 'thur') {
        // document.getElementById("thurAddHtml").appendChild(txtNewInputBox);
        this.thurAddHr = false;
      }
  
      if (id == 'fri') {
        // document.getElementById("friAddHtml").appendChild(txtNewInputBox);
        this.friAddHr = false;
      }
  
      if (id == 'sat') {
        // document.getElementById("satAddHtml").appendChild(txtNewInputBox);
        this.satAddHr = false;
      }
  
      if (id == 'sun') {
        // document.getElementById("sunAddHtml").appendChild(txtNewInputBox);
        this.sunAddHr = false;
      }
      //su
    }


      //**********Bank detail toggle*************/
  onAddAllownces(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isAllownces = true;
    }
    else {
      this.isAllownces = false;
    }

  }

  onAddDeduction(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isDeduction = true;
    }
    else {
      this.isDeduction = false;
    }

  }
 
  onAddOvertime(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isOvertime = true;
    }
    else {
      this.isOvertime = false;
    }

  }
//***********Save Personal Details*************/
Save_PersonalDetails(){
  this.PersonalDetailsForm.patchValue({
    user_code: this.usercode
  })
  let obj=this.PersonalDetailsForm.value;
 
  //console.log("persoal detail obj",obj);
  
  this.UserRole_Service.postUserPosPersonalDetails(obj)
  .subscribe(res=>{
        console.log("personal details is saved",res);
     this.userid=res['id']   
  
      });
      
     // console.log("personal details",obj);
      
  }
  //**********Show Assets row**********/

  Save_WorkDetails(){
    let obj=this.WorkDetailsForm.value;
    this.UserRole_Service.putUserPosWorkDetails(this.userid,obj).subscribe(res=>{
      console.log("work details is saved",res);
      

    });
    

        console.log("Save work details",obj);
    
  
  }
  Save_BankDetails(){
    
  
    let obj=this.BankPaymentDetailsForm.value;
    this.UserRole_Service.putUserPosBankdetails(this.userid,obj).subscribe(res=>{
      console.log("bank details is saved",res);
      

    });
    
 
    console.log("bank details",obj);
  }
  //**********Save Documents Details*******************/
  Save_LegalDoc(){
    let obj=this.DocumentsForm.value
    
    this.UserRole_Service.putUserPosLegaldetails(this.userid,obj).subscribe(res=>{
      console.log("legal details is saved",res);
      

    });
    console.log("Save Documents details",obj);
  }

  //****************Function For Add Salary********** */
showSalaryDiv(){
  this.isShowSalary=true;
  }
  //**********Save Credentials Details*******************/
Save_Credentials(){
 

  let obj=this.CorporatDetailsForm.value
  
  this.UserRole_Service.putUserPosCredntialsdetails(this.userid,obj).subscribe(res=>{
    console.log("credentials details is saved",res);
    

  });
  console.log("Save Credential details",obj);

  this.router.navigate(['/users-roles']);
}
//**********Save Assets Details*******************/
Save_Assets(){
  let obj=this.AssetDetailsForm.value;
  
  
  this.UserRole_Service.putUserPosAssetsdetails(this.userid,obj).subscribe(res=>{
    console.log("assets details is saved",res);
    

  });

  console.log("Save Assets details",obj);



}
  //************function for add allownces and amount***********
  showAlloncesDiv(){
    this.showAllowncesField=true;
  }
  //************function for add allownces and amount***********
  
  showDeductionDiv(){
    this.showDeductionField=true;
  }
  //************function for add over time************** */
  showOverTimeDiv(){
  this.showOverTimeField=true;
  }
  //**********Show Assets row**********/
  increaseAssets(){
    this.showAssetRow=true;
    this.assetArr.push(this.assetArr.length);
    console.log("Added Multiple name and documents", this.assetArr);
  }
  //***************Function For Add Multiple documents */

 addMultipleDocuments(){
   
  this.docArr.push(this.docArr.length);
  console.log("Added Multiple name and documents", this.docArr);

}
//*************Navigate to User Role page***********/
BackTo_Page(){
  this.router.navigate(['/users-roles']);
}

isUploading(){
  this.ngxSpinner.show()
  console.log('yes change work');
  
}
//*************country state city**************** */
country_state_city(){
  this.UserRole_Service.country_state_city().subscribe(data => {
  console.log(data.country_json);
    this.country_json = data.country_json
  })
}
changeCountry(count) {
  this.state = this.country_json.find(state => state.name == count).states
  console.log(count);
  this.state_list=this.state
  this.state=Object.keys(this.state)
}
changeState(count){
  this.city=this.state_list[count]
}
 //******************pop up for view roles*********************
 openDialog_SelectRole(): void {
  // console.log('view user ', element);

  const dialogRef = this.dialog.open(SelectRoleComponent, {
    disableClose: true,
    width: '50%',
    height: '300px',
    // data: element

  });
dialogRef.afterClosed().subscribe(res=>{
  console.log("result",res);
  this.resultRole=res;
  this.PersonalDetailsForm.patchValue({
    assigned_role:res._id
  });
  this.isShowRole=true;
  
})

}

}
