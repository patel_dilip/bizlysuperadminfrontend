import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectRoleOfRoleComponent } from './select-role-of-role.component';

describe('SelectRoleOfRoleComponent', () => {
  let component: SelectRoleOfRoleComponent;
  let fixture: ComponentFixture<SelectRoleOfRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectRoleOfRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectRoleOfRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
