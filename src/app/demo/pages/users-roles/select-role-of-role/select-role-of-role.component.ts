import { Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/_services/main.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-select-role-of-role',
  templateUrl: './select-role-of-role.component.html',
  styleUrls: ['./select-role-of-role.component.scss']
})
export class SelectRoleOfRoleComponent implements OnInit {
  setRolesForm:FormGroup;
  firstLevel: any;
  size = 12;
  showRolesCategories: boolean = false;
  roles: any;
  constructor(private main: MainService,private formbuilder:FormBuilder) {
    this.main.getAllEstablishment().subscribe(data => {
      console.log(data);
      this.firstLevel = data
    });
   }

  ngOnInit() {
    this.setRolesForm=this.formbuilder.group({
      root:[''],
      sub:[''],
      role:['']
    })
  }
//***********role category*************** */
rolesCategory(event) {
  console.log(event.srcElement.checked);
  if (event.srcElement.checked == true) {
    this.size = 8
    this.showRolesCategories = true
  } else {
    this.size = 12
    this.showRolesCategories = false

  }
}
selectRole(event,val){
//console.log("role selected",event);
console.log("value",val);
this.roles=val;
}
}
