import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-cat-role-name',
  templateUrl: './add-cat-role-name.component.html',
  styleUrls: ['./add-cat-role-name.component.scss']
})
export class AddCatRoleNameComponent implements OnInit {

  categoryForm: FormGroup


  constructor(@Inject(MAT_DIALOG_DATA) public rootid,
              private fb: FormBuilder,
              private user_roleService: UsersRolesService){
                
  this.categoryForm = this.fb.group({
    name: [''],
    chidren: [[]]
  })

  }

  ngOnInit() {
  }

  saveCatRole(){
    let obj = this.categoryForm.value

    console.log('obj : -', obj);
    console.log('root id: -', this.rootid);
    
    this.user_roleService.putChildrenCat(this.rootid,obj).subscribe(res=>{
      console.log('put category response',res);
      
      if(res['success']){
        Swal.fire('Categroy Added','','success')
      }
      else{
        Swal.fire('Failed to Add','','warning')
      }
    })
  }

}
