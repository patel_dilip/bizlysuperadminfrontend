import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCatRoleNameComponent } from './add-cat-role-name.component';

describe('AddCatRoleNameComponent', () => {
  let component: AddCatRoleNameComponent;
  let fixture: ComponentFixture<AddCatRoleNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCatRoleNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCatRoleNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
