import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosSelectRoleComponent } from './pos-select-role.component';

describe('PosSelectRoleComponent', () => {
  let component: PosSelectRoleComponent;
  let fixture: ComponentFixture<PosSelectRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosSelectRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosSelectRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
