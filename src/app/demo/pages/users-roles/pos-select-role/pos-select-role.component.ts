import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';

@Component({
  selector: 'app-pos-select-role',
  templateUrl: './pos-select-role.component.html',
  styleUrls: ['./pos-select-role.component.scss']
})
export class PosSelectRoleComponent implements OnInit {
  setRolesForm:FormGroup;
  rolesForm:FormGroup;
  firstLevel: any;
  size = 12;
  showRolesCategories: boolean = false;
  roles: any;
  rolesData:any=[]
  constructor(private main: MainService,private formbuilder:FormBuilder,private UserRole_Service:UsersRolesService) {    
    this.main.getAllEstablishment().subscribe(data => {
      console.log(data);
      this.firstLevel = data
    });
    //************get all role********
    this.UserRole_Service.getAllRole().subscribe(data => {

      this.rolesData = data['data']
      console.log("get all created roles", this.rolesData);

    
    })
}

  ngOnInit() {
    this.rolesForm=this.formbuilder.group({
      root:[''],
      sub:['']
    });
    this.setRolesForm=this.formbuilder.group({
      role:['']
    })
  }
//***********role category*************** */
rolesCategory(event) {
  console.log(event.srcElement.checked);
  if (event.srcElement.checked == true) {
    this.size = 8
    this.showRolesCategories = true
  } else {
    this.size = 12
    this.showRolesCategories = false

  }
}
selectRole(event,val){
//console.log("role selected",event);
console.log("value",val);
this.roles=val;
}
}
