import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.scss']
})
export class ViewUsersComponent implements OnInit {
  // obj={
  //   "accessPoint":true,
  //   "versions": [
  //       {
  //           "versionName": "version1",
  //           "created": {
  //               "dateCreatedandTime": "20/03/2020",
  //               "dateEditedandTime": "23/03/2020",
  //               "editedBy": "Ellen"
  //           }
  //       },
  //       {
  //           "versionName": "version2",
  //           "created": {
  //               "dateCreatedandTime": "20/01/2020",
  //               "dateEditedandTime": "23/01/2020",
  //               "editedBy": "Lizza"
  //           }
  //       },
  //       {
  //           "versionName": "version3",
  //           "created": {
  //               "dateCreatedandTime": "20/01/2020",
  //               "dateEditedandTime": "23/01/2020",
  //               "editedBy": "Jorden"
  //           }
  //       }
  //   ]
  // }
  
  roleobj={
    "roleID":"R005","roleTag":"Manager",
    "accesControl":[ {
      "accessControlName": "restaurant",
      "controls": ["add", "delete"]
    },
    { "accessControlName": "orders", "controls": [ "view", "edit", "delete"] },
    { "accessControlName": "menu", "controls": ["add", "edit", "delete"] },
    { "accessControlName": "pos-orders", "controls": [ "delete"] }
  ]
  }
  editedData:any=[];
  userid:any;
  constructor(private userRole_service:UsersRolesService,private formbuilder:FormBuilder,public dialogRef: MatDialogRef<ViewUsersComponent>,@Inject(MAT_DIALOG_DATA) public data1) { 
    console.log(data1);
 
    let localData = JSON.parse(localStorage.getItem('loginUser'));
    this.userid=localData._id
    console.log("localStorage",this.userid);
    let obj
    this.userRole_service.getUserEditedHistory(this.userid,obj).subscribe(data=>{
    this.editedData=data['data']
   console.log("edited history data",this.editedData);
   
  });
  }
  
  ngOnInit() {
  }
 //**********Pop closed function************
 onNoClick(): void {
  this.dialogRef.close();
}
}
