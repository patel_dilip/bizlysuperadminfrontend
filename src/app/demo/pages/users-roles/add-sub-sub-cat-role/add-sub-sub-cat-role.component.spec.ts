import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSubSubCatRoleComponent } from './add-sub-sub-cat-role.component';

describe('AddSubSubCatRoleComponent', () => {
  let component: AddSubSubCatRoleComponent;
  let fixture: ComponentFixture<AddSubSubCatRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSubSubCatRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSubSubCatRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
