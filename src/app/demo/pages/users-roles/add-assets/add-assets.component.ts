import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-add-assets',
  templateUrl: './add-assets.component.html',
  styleUrls: ['./add-assets.component.scss']
})
export class AddAssetsComponent implements OnInit {
  addAssetsForm:FormGroup;
  constructor(public dialogRef: MatDialogRef<AddAssetsComponent>,private formbuilder:FormBuilder) { }
  
  ngOnInit() {
    this.addAssetsForm=this.formbuilder.group({
      name:[''],
      quantity:[''],

    })
  }
 //**********Pop closed function************
 onNoClick(): void {
  this.dialogRef.close();
}
//***************save added assets***************/
saveAdded_Assets(){
  let obj=this.addAssetsForm.value;
  console.log("Assets Successfully add",obj);
  
}
}
