import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';
import { MatDialog } from '@angular/material';
import { SelectRoleOfRoleComponent } from '../select-role-of-role/select-role-of-role.component';


@Component({
  selector: 'app-create-roles-comp',
  templateUrl: './create-roles-comp.component.html',
  styleUrls: ['./create-roles-comp.component.scss']
})
export class CreateRolesCompComponent implements OnInit {
  createRoleForm: FormGroup;
  createAccessControlForm: FormGroup;
  resultRole = "";
  isShowRole: boolean = false;
  role_name_list = ['BDO', 'CEO', 'Assistant Manager', 'Manager', 'HR', 'Employee']
  rolesdata: any = [];
  rolecode: any
  userid: any
  constructor(
    private formbuilder: FormBuilder,
    private router: Router,
    private user_role_service: UsersRolesService,
    private dialog: MatDialog,
  ) {

    let localData = JSON.parse(localStorage.getItem('loginUser'));
    this.userid = localData._id
    console.log('this is user id', this.userid);
    
    // var m = '1000'
    // var size = 3
    // m = m.padStart(size, '0')

    // console.log('m vlaue', m);

    // Get All Role
    this.user_role_service.getAllRole().subscribe(data => {
      this.rolesdata = data['data']

      if (this.rolesdata.length == 0) {
        console.log('length is zero', this.rolesdata);

        let len = this.rolesdata.length + 1   //length if array is empty 0+1
        let str = len.toString()            // converting 1 to string '1'
        let size = 3                        // size of code
        this.rolecode = 'BBR' + str.padStart(size, '0')  // padding with 00 to left ex: 001
        console.log('role code', this.rolecode);


      }
      else {
        console.log(this.rolesdata.length);
        let index = this.rolesdata.length - 1

        let last_code = this.rolesdata[index].role_ID
        console.log('last_code', last_code);
        let str = last_code.slice(3)
        console.log('string', str);
        let digit = parseInt(str)
        digit = digit + 1
        let str1 = digit.toString()
        let size = 3
        this.rolecode = 'BBR' + str1.padStart(size, '0')
        console.log('role code', this.rolecode);




      }

    })


  }

  ngOnInit() {
    this.createRoleForm = this.formbuilder.group({
      role_ID: [''],
      role_name: [''],
      role_tag: [''],
      role_salary: [''],
      description: [''],  
      userid: [''],
      status: true
    });
    this.createAccessControlForm = this.formbuilder.group({
      categoryCheckbox: [''],
      categoryCheckboxFor: ['']
    });

  }
  //*************Navigate to User Role page***********/
  BackTo_Page() {
    this.router.navigate(['/users-roles']);
  }
  //*****************Create Role***********
  Create_Role() {

    this.createRoleForm.patchValue({
      role_ID: this.rolecode,      
      userid:this.userid
    })
    let obj = this.createRoleForm.value;
    console.log('role object to be posted', obj);
    
    this.user_role_service.postRole(obj).subscribe(res => {
      console.log("role is created", res);


    });

    this.router.navigate(['/users-roles']);
  }
  //******************pop up for view roles*********************
  openDialog_SelectRole(): void {
    console.log('view user ');

    const dialogRef = this.dialog.open(SelectRoleOfRoleComponent, {
      disableClose: true,
      width: '50%',
      height: '300px',
      //data: element

    });
    dialogRef.afterClosed().subscribe(res => {
      console.log("result", res);
      this.resultRole = res;
      this.createRoleForm.patchValue({
        assigned_role: res
      });
      this.isShowRole = true;

    })

  }
}
