import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRolesCompComponent } from './create-roles-comp.component';

describe('CreateRolesCompComponent', () => {
  let component: CreateRolesCompComponent;
  let fixture: ComponentFixture<CreateRolesCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRolesCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRolesCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
