import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersRolesHomeComponent } from './users-roles-home.component';

describe('UsersRolesHomeComponent', () => {
  let component: UsersRolesHomeComponent;
  let fixture: ComponentFixture<UsersRolesHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersRolesHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersRolesHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
