import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';
import { MatDialog } from '@angular/material';
import { PosSelectRoleComponent } from '../pos-select-role/pos-select-role.component';
@Component({
  selector: 'app-create-pos-role',
  templateUrl: './create-pos-role.component.html',
  styleUrls: ['./create-pos-role.component.scss']
})
export class CreatePosRoleComponent implements OnInit {
  createRoleForm:FormGroup;
  createAccessControlForm:FormGroup;
  resultRole="";
  rolecode: any
  rolesdata: any= [];
isShowRole:boolean=false;
  constructor(private dialog: MatDialog,private formbuilder:FormBuilder,private router:Router,private UserRole_Service:UsersRolesService) { 
    // var m = '1000'
    // var size = 3
    // m = m.padStart(size, '0')

    // console.log('m vlaue', m);

    // Get All Role
    this.UserRole_Service.getAllRole().subscribe(data=>{
      this.rolesdata = data['data']

      if(this.rolesdata.length==0){
        console.log('length is zero', this.rolesdata);

        let len = this.rolesdata.length+1   //length if array is empty 0+1
        let str = len.toString()            // converting 1 to string '1'
        let size = 3                        // size of code
        this.rolecode = 'BBR'+str.padStart(size,'0')  // padding with 00 to left ex: 001
        console.log('role code',  this.rolecode);
        
        
      }
      else{
        console.log(this.rolesdata.length);
        let index = this.rolesdata.length-1

        let last_code = this.rolesdata[index].role_ID
        console.log('last_code', last_code);
        let str = last_code.slice(3)
        console.log('string', str);
        let digit = parseInt(str)
        digit = digit+1
        let str1 = digit.toString()
        let size = 3
        this.rolecode = 'BBR'+str1.padStart(size,'0')
        console.log('role code', this.rolecode);
        
      
        
        
      }

    })

  }

  ngOnInit() {
    this.createRoleForm=this.formbuilder.group({
      role_ID:[''],
      role_name:[''],
      role_salary:[''],
      access_control:[''],
      role_tag:[''],
      description:['']
    });
    this.createAccessControlForm=this.formbuilder.group({

    })
  }
//*************Navigate to User Role page***********/
BackTo_Page(){
  this.router.navigate(['/users-roles']);
}
//*************Create Role********
Create_Role(){
  this.createRoleForm.patchValue({
    role_ID: this.rolecode
  })
  let obj=this.createRoleForm.value;
  
 
  
  this.UserRole_Service.postPosRole(obj).subscribe(res=>{
        console.log("pos role is created",res);
        
        this.router.navigate(['/users-roles']);
      });
}
 //******************pop up for select roles*********************
 openDialog_SelectRole(): void {
  console.log('view user ');

  const dialogRef = this.dialog.open(PosSelectRoleComponent,{
    disableClose: true,
    width: '50%',
    height: '300px',
   // data: element

  });
dialogRef.afterClosed().subscribe(res=>{
  console.log("result",res);
  this.resultRole=res;
  this.createRoleForm.patchValue({
    assigned_role:res
  });
  this.isShowRole=true;
  
})

}
}
