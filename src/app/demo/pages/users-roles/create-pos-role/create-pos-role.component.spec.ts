import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePosRoleComponent } from './create-pos-role.component';

describe('CreatePosRoleComponent', () => {
  let component: CreatePosRoleComponent;
  let fixture: ComponentFixture<CreatePosRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePosRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePosRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
