import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Form, FormArray, Validators } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';
import { AddAssetsComponent } from '../add-assets/add-assets.component';
//import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';
import { environment } from 'src/environments/environment';
import { SelectRoleComponent } from '../select-role/select-role.component';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from "ngx-spinner";

//************File uplaad url *******************/
const URL = environment.base_Url + 'notice/uploadnotice'

// *** Photos Upload*//
const profilePic = environment.base_Url+"userrolebizly/uploadpersonaldetails";
//*****NDA file upload*******/
const NDAdocumentfile = environment.base_Url+"userrolebizly/uploadworkdetailsNDAfile";
//*****Joining letter upload*******/
const JoiningLetUploader= environment.base_Url+"userrolebizly/uploadworkdetailsjoiningletter";

//**Files upload */
const aadharDoc = environment.base_Url+"userrolebizly/uploadaadharcard";
const driving = environment.base_Url+"userrolebizly/uploadlicence";
const passport = environment.base_Url+"userrolebizly/uploadpassport";
const docs = environment.base_Url+"userrolebizly/uploaddocumentfile";
@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  PersonalDetailsForm:FormGroup;
  WorkDetailsForm:FormGroup;
  BankPaymentDetailsForm:FormGroup;
  DocumentsForm:FormGroup;
  CorporatDetailsForm:FormGroup;
  AssetDetailsForm:FormGroup;
  CredentialsForm:FormGroup;
  accessControlForm:FormGroup;
  UserAllDetailsForm:FormGroup;

  completeData: any = new Array();

  docArr: any = [0];
  OccuranceCount: number;
  addCount: number;
  Count:number;
  assetArr:any=[0]

  restaurants_types = [];

  isShowSalary:boolean=false;
  showAllowncesField:boolean=false;
  showDeductionField:boolean=false;
  showOverTimeField:boolean=false;

  showAssetRow:boolean=false;

  uploader: FileUploader;
  response: string;

  isShowRole:boolean=false;
  

  //working detail
 

 mondayOpenTime: any;
  mondayCloseTime: any;
  extramonOpenTime: any;
  extamonCloseTime: any;


  isAllownces =false;
  isDeduction =false;
  isOvertime =false;

  isMon = true;
  isTues = false;
  isWed = false;
  isThur = false;
  isFri = false;
  isSat = false;
  isSun = false;

  monAddHr = false
  tueAddHr = false
  wedAddHr = false
  thurAddHr = false
  friAddHr = false
  satAddHr = false
  sunAddHr = false

  userid:any;

  supplies_cat:[];
  State_list_per=['Maharashtra','UP'];
  State_list=['Maharashtra','UP'];
  City_list_per=['pune','Amaravti','Mumbai'];
  City_list=['pune','Amaravti','Mumbai'];
  assign_city_list=['pune'];
  assign_city_location=['Dhanurkar']
  blood_group_list = ['A+', 'A-', 'B-', 'B+', 'AB+', 'AB-', 'O+', 'O-'];
  allownces_list=['Petrol','Mobile Recharg','Medical'];
  deduction_list=['Petrol','Mobile Recharg','Medical'];
  overtime_list=['1 hour','2 hour','3 hour','4 hour']
  resultRole:any=[];

  userData:any=[];
    //************File uploader***************/
    public noticeUploader: FileUploader = new FileUploader({url: URL, itemAlias: 'notice' });
  


  // photo upload urls

  public profilePicUploader: FileUploader = new FileUploader({url:profilePic, itemAlias: 'personaldetails'})

  //NDA file upload urls

  public NDAPicUploader:FileUploader = new FileUploader({url:NDAdocumentfile,itemAlias: 'workdetailsNDAfile'})
  //joining letter upload urls

  public joiningLetterUploader:FileUploader = new FileUploader({url:JoiningLetUploader,itemAlias: 'workdetailsjoiningletter'})

    // file upload urls
    public addharUploader: FileUploader = new FileUploader({url: aadharDoc, itemAlias: 'aadharcard'})
    public drivingUploader: FileUploader = new FileUploader({url: driving, itemAlias: 'licence'})
    public passportUploader: FileUploader = new FileUploader({url: passport, itemAlias: 'passport'})
    public docsUploader: FileUploader = new FileUploader({url: docs, itemAlias: 'documentfile'})
  aadharRes: any;
  aadharStatus: boolean;
  LicenRes: any;
  LicStatus: boolean;
  passRes: any;
  passStatus: boolean;
  documentRes: any;
  docStatus: boolean;
  propicStatus: boolean;
  ndaPicStatus: boolean;
  joiningLetterStatus:boolean;
  profilepic: any;
  NDAdocumentfile:any;
  JoiningLetUploader:any;
  allUsers:any=[];
  usercode:any;
  country_json=[];
  state=[];
  city=[];
  state_list: any;
    constructor(private router:Router,
    private formbuilder:FormBuilder,
    private dialog: MatDialog,
    private UserRole_Service:UsersRolesService,
    private ngxSpinner: NgxSpinnerService) {
      let localData = JSON.parse(localStorage.getItem('loginUser'));
   this.userid=localData._id
   console.log("localStorage",this.userid);
   

// ******Get all users************/

this.UserRole_Service.getAllUser().subscribe(data=>{
  this.allUsers = data['data']
  
  if(this.allUsers.length == 0){
  console.log('length is zero', this.allUsers);
  
  let len = this.allUsers.length+1 //length if array is empty 0+1
  let str = len.toString() // converting 1 to string '1'
  let size = 3 // size of code
  this.usercode = 'BBU'+str.padStart(size,'0') // padding with 00 to left ex: 001
  console.log('user code', this.usercode);
  }
  else{
  console.log(this.allUsers.length);
  let index = this.allUsers.length-1
  let per=this.allUsers[index].personal_details
  let last_code = per.user_code
  console.log('last_code', last_code);
  
  let str = last_code.slice(3)
  console.log('string', str);
  let digit = parseInt(str)
  digit = digit+1
  let str1 = digit.toString()
  let size = 3
  
  this.usercode = 'BBU'+str1.padStart(size,'0')
  console.log('User code', this.usercode);
  }
  });

  //*********select country function************ */
  this.country_state_city()
   }

  ngOnInit() {
    this.PersonalDetailsForm=this.formbuilder.group({
      user_code:[''],
      employee_name:[''],
      contact_number: ['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      email_id:['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      photo:[''],
      first_address:[''],
      second_address:[''],
      country:[''],
      state:[''],
      city:[''],
      pin_code:[''],
      gender:[''],
      birth_date:[''],
      blood_group:[''],
      joiningDate:[''],
      marrital_status:[''],
      emergency_contact_personName:[''],
      emg_contact_number: ['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      mother_Name:[''],
      father_husbandName:[''],
      fam_Contact_no: ['', [Validators.required, Validators.pattern("[0-9]{10}")]],
      assigned_role:[''],
      userid:this.userid,
      status:true
    });

    this.WorkDetailsForm=this.formbuilder.group({
     
      isMon:this.isMon,
      isTues:this.isTues,
      isWed :this.isWed,
      isThur:this.isThur,
      isFri :this.isFri,
      isSat:this.isSat,
      isSun :this.isSun,
      openMon: [''],
      openTue: [''],
      openWed: [''],
      openThur: [''],
      openFri: [''],
      openSat: [''],
      openSun: [''],

      extraopenMon: [''],
      extraopenTue: [''],
      extraopenWed: [''],
      extraopenThur: [''],
      extraopenFri: [''],
      extraopenSat: [''],
      extraopenSun: [''],

      closeMon: [''],
      closeTue: [''],
      closeWed: [''],
      closeThur: [''],
      closeFri: [''],
      closeSat: [''],
      closeSun: [''],

      extracloseMon: [''],
      extracloseTue: [''],
      extracloseWed: [''],
      extracloseThur: [''],
      extracloseFri: [''],
      extracloseSat: [''],
      extracloseSun: [''],
      Sanctioned_Leaves:[''],
      joining_letter:[''],
      joining_date:[''],
      assign_location:[''],
      assign_city:[''],
      responsibilities:[''],
      NDA_file:[''],
     // userid:['']

    });
    this.BankPaymentDetailsForm=this.formbuilder.group({
      account_number:[''],
      IFSC_code:[''],
      bank_name:[''],
      bank_address:[''],
      bank_state:[''],
      bank_city:[''],
      salary_amount:[''],
      add_allownces:[''],
      add_deduction:[''],
      select_overtime:[''],
      allownces_name:[''],
      allownces_amount:[''],
      deduction_name:[''],
      deduction_amount:[''],
      overTime_name:[''],
      overTime_amount:[''],
      incentive_amount:[''],
      net_salary:[''],

    });
    this.DocumentsForm=this.formbuilder.group({
      aadhar_number:[''],
      aadhar_upload:[''],
      licence_number:[''],
      licence_upload:[''],
      passport_number:[''],
      passport_upload:[''],
      joining_letter:[''],
      joining_letter_file:[''],
      add_document:[''],
      add_document_file:[''],
      document_name:[''],
      name_document_file:['']
    });
 
    this.accessControlForm=this.formbuilder.group({

    })
    this.AssetDetailsForm=this.formbuilder.group({

      assetsArr: this.formbuilder.array([]) // form array first step
      });
            // By Default 1 add row
            this.addAssetRow()
    this.CredentialsForm=this.formbuilder.group({
      bizUser_email:[''],
      password:[''],
      re_password:[''],
      admin_username:[''],
      admin_password:[''],
     

    });

    //***********File uploader for notice template********************/
    this.noticeUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
     
      console.log('FileUpload:uploaded:', response);
     this.DocumentsForm.patchValue({
      file:response
     })
//***********joining letter uploade***** */
   
     this.joiningLetterUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
    }
  //**************NDA file upload */

  this.NDAPicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
  }
     //**************driving license upload */

     this.drivingUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
    }

     //**************passport upload */

     this.passportUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
    }

     //**************documents upload */

     this.docsUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
    }
      
    }

   
//  NG FILE UPLOADER CODE
this.uploader = new FileUploader({
  url: URL,
  itemAlias: 'notice'

});
this.response = '';

this.uploader.response.subscribe(res => this.response = res);

// ********profile pic upload***********/
this.profilePicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
  console.log('FileUpload:uploaded:', item, status, response);

  this.profilepic = response;
  
  if(status==200){
    Swal.fire('Profile Picture Uploaded Successfully','','success')
    this.propicStatus = true

    // patch value
    this.PersonalDetailsForm.patchValue({
      photo: response
    })

    console.log(this.PersonalDetailsForm.value);
    
  }
  else{
    Swal.fire('Failed to Upload!','','warning')
    this.propicStatus = false
  }

  this.ngxSpinner.hide()
}
// ********joining letter upload***********/
this.joiningLetterUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
  console.log('FileUpload:uploaded:', item, status, response);

  this.JoiningLetUploader = response;
  
  if(status==200){
    Swal.fire('Joining Letter Uploaded Successfully','','success')
    this.joiningLetterStatus = true

    // patch value
    this.WorkDetailsForm.patchValue({
      joining_letter: response
    })

    console.log(this.WorkDetailsForm.value);
    
  }
  else{
    Swal.fire('Failed to Upload!','','warning')
    this.joiningLetterStatus = false
  }

  this.ngxSpinner.hide()
}
// ********NDA pic upload***********/
this.NDAPicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
  console.log('FileUpload:uploaded:', item, status, response);

  this.NDAdocumentfile = response;
  
  if(status==200){
    Swal.fire('NDA File Uploaded Successfully','','success')
    this.ndaPicStatus = true

    // patch value
    this.WorkDetailsForm.patchValue({
      file: response
    })

    console.log(this.WorkDetailsForm.value);
    
  }
  else{
    Swal.fire('Failed to Upload!','','warning')
    this.ndaPicStatus = false
  }

  this.ngxSpinner.hide()
}

  //**************aadhar upload */

  this.addharUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.aadharRes = response

    // Check Condition upload successful
    if(status == 200){
      Swal.fire('Aadhar Uploaded Successfully','','success')
      this.aadharStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        aadhar_upload: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{
      Swal.fire('Failed to Upload!','','warning')
      this.aadharStatus = false
    }
    this.ngxSpinner.hide()
  }
  
   //**************dRIVING LICENSE upload */

   this.drivingUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.LicenRes = response
    this.ngxSpinner.hide()
    // Check Condition upload successful
    if(status == 200){

     
      Swal.fire('License Uploaded Successfully','','success')
      this.LicStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        licence_upload: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{

      this.ngxSpinner.hide()
      Swal.fire('Failed to Upload!','','warning')
      this.LicStatus = false
    }

    this.ngxSpinner.hide()
  }
   //**************pASSPORT upload */

   this.passportUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.passRes = response

    // Check Condition upload successful
    if(status == 200){

     
      Swal.fire('Passport Uploaded Successfully','','success')
      this.passStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        passport_upload: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{

      this.ngxSpinner.hide()
      Swal.fire('Failed to Upload!','','warning')
      this.passStatus = false
    }

    this.ngxSpinner.hide()
  }
   //**************dOCUMENTS upload */

   this.docsUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.documentRes = response

    // Check Condition upload successful
    if(status == 200){

     
      Swal.fire('Document Uploaded Successfully','','success')
      this.docStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        add_document_file: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{

      this.ngxSpinner.hide()
      Swal.fire('Failed to Upload!','','warning')
      this.docStatus = false
    }

    this.ngxSpinner.hide()
  }
 
}
get assetForm(){
  return this.AssetDetailsForm.controls.assetsArr as FormArray /// **** Second Step in Form Array
  }
  
  
  addAssetRow(){ /// *** Third and Final Step in Form Array
  const assetrow = this.formbuilder.group({
  assets_name: [''],
  assets_quantity: [''],
  assets_date:['']
  })
  
  this.assetForm.push(assetrow)
  
  console.log(this.AssetDetailsForm.value)
  }
  
  deleteAssetRow(index){
  this.assetForm.removeAt(index)
  
  console.log(this.AssetDetailsForm.value)
  }
  
  showAssetvalue(){
  
  console.log(this.AssetDetailsForm.value)
  }


//******************pop up for add assets*********************
openDialog_AddAssets(): void {
  const dialogRef = this.dialog.open(AddAssetsComponent, {
    disableClose: true,
    width: '30%',
    height: '300px',
    //data: element

  });


}
 // ******************
  // WEEK RADIO BUTTON TIMES 

  onMonday(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);
console.log("toogle",this.WorkDetailsForm.controls.isMon.value);

    if (evt.checked) {
      this.isMon = true;
    }
    else {
      this.isMon = false;
    }

  }

  onTuesday(evt) {
    if (evt.checked) {
      this.isTues = true;
    }
    else {
      this.isTues = false;
    }
  }

  onWednesday(evt) {
    if (evt.checked) {
      this.isWed = true;
    }
    else {
      this.isWed = false;
    }
  }

  onThursday(evt) {
    if (evt.checked) {
      this.isThur = true;
    }
    else {
      this.isThur = false;
    }
  }


  onFriday(evt) {
    if (evt.checked) {
      this.isFri = true;
    }
    else {
      this.isFri = false;
    }
  }

  onSaturday(evt) {
    if (evt.checked) {
      this.isSat = true;
    }
    else {
      this.isSat = false;
    }
  }

  onSunday(evt) {
    if (evt.checked) {
      this.isSun = true;
    }
    else {
      this.isSun = false;
    }
  }
    // after click on sameday 
    onSameAsMonday(chk) {

      console.log('checked vlaue', chk)
      if (this.monAddHr) {
        this.tueAddHr = true;
        this.wedAddHr = true;
        this.thurAddHr = true;
        this.friAddHr = true;
        this.satAddHr = true;
        this.sunAddHr = true;
      }
  
      // TIME VALUE SAME AS MONDAY
  
      this.mondayOpenTime = this.WorkDetailsForm.controls['openMon'].value
      this.mondayCloseTime = this.WorkDetailsForm.controls['closeMon'].value
  
      console.log('Extra open monday vlaue', this.WorkDetailsForm.controls['extraopenMon'].value);
      this.extramonOpenTime = this.WorkDetailsForm.controls['extraopenMon'].value
      this.extamonCloseTime = this.WorkDetailsForm.controls['extracloseMon'].value
  
  
      console.log('tIMES values ', this.mondayOpenTime);
  
      this.WorkDetailsForm.patchValue({
        openTue: this.mondayOpenTime,
        openWed: this.mondayOpenTime,
        openThur: this.mondayOpenTime,
        openFri: this.mondayOpenTime,
        openSat: this.mondayOpenTime,
        openSun: this.mondayOpenTime,
  
        extraopenTue: this.extramonOpenTime,
        extraopenWed: this.extramonOpenTime,
        extraopenThur: this.extramonOpenTime,
        extraopenFri: this.extramonOpenTime,
        extraopenSat: this.extramonOpenTime,
        extraopenSun: this.extramonOpenTime,
  
  
        closeTue: this.mondayCloseTime,
        closeWed: this.mondayCloseTime,
        closeThur: this.mondayCloseTime,
        closeFri: this.mondayCloseTime,
        closeSat: this.mondayCloseTime,
        closeSun: this.mondayCloseTime,
  
        extracloseTue: this.extamonCloseTime,
        extracloseWed: this.extamonCloseTime,
        extracloseThur: this.extamonCloseTime,
        extracloseFri: this.extamonCloseTime,
        extracloseSat: this.extamonCloseTime,
        extracloseSun: this.extamonCloseTime
  
      })
  
  
  
  
      //RADIO SAME AS MONDAY
      if (chk.target.checked) {
  
        this.isTues = this.isMon;
        this.isWed = this.isMon;
        this.isThur = this.isMon;
        this.isFri = this.isMon;
        this.isSat = this.isMon;
        this.isSun = this.isMon;
      }
  
      else {
        this.isTues = false;
        this.isWed = false;
        this.isThur = false;
        this.isFri = false;
        this.isSat = false;
        this.isSun = false;
      }
  
    }
 
    createNewElement(id) {
      // First create a DIV element.
      var txtNewInputBox = document.createElement('div');
  
      // Then add the content (a new input box) of the element.
      txtNewInputBox.innerHTML = "+<input type='time' value='16:00:00' > to <input type='time' value='23:00:00' >";
  
      // Finally put it where it is supposed to appear.
      if (id == 'mon') {
        // document.getElementById("monAddHTML").appendChild(txtNewInputBox);
        this.monAddHr = true;
      }
  
      if (id == 'tue') {
        // document.getElementById("tueAddHTML").appendChild(txtNewInputBox);
        this.tueAddHr = true;
      }
  
      if (id == 'wed') {
        // document.getElementById("wedAddHtml").appendChild(txtNewInputBox);
        this.wedAddHr = true;
      }
  
      if (id == 'thur') {
        // document.getElementById("thurAddHtml").appendChild(txtNewInputBox);
        this.thurAddHr = true;
      }
  
      if (id == 'fri') {
        // document.getElementById("friAddHtml").appendChild(txtNewInputBox);
        this.friAddHr = true;
      }
  
      if (id == 'sat') {
        // document.getElementById("satAddHtml").appendChild(txtNewInputBox);
        this.satAddHr = true;
      }
  
      if (id == 'sun') {
        // document.getElementById("sunAddHtml").appendChild(txtNewInputBox);
        this.sunAddHr = true;
      }
      //sunAddHtml
  
      console.log('Id ', id);
  
  
    }
  
    removeExtraTime(id) {
      if (id == 'mon') {
        // document.getElementById("monAddHTML").appendChild(txtNewInputBox);
        this.monAddHr = false;
      }
  
      if (id == 'tue') {
        // document.getElementById("tueAddHTML").appendChild(txtNewInputBox);
        this.tueAddHr = false;
      }
  
      if (id == 'wed') {
        // document.getElementById("wedAddHtml").appendChild(txtNewInputBox);
        this.wedAddHr = false;
      }
  
      if (id == 'thur') {
        // document.getElementById("thurAddHtml").appendChild(txtNewInputBox);
        this.thurAddHr = false;
      }
  
      if (id == 'fri') {
        // document.getElementById("friAddHtml").appendChild(txtNewInputBox);
        this.friAddHr = false;
      }
  
      if (id == 'sat') {
        // document.getElementById("satAddHtml").appendChild(txtNewInputBox);
        this.satAddHr = false;
      }
  
      if (id == 'sun') {
        // document.getElementById("sunAddHtml").appendChild(txtNewInputBox);
        this.sunAddHr = false;
      }
      //su
    }
  //**********Bank detail toggle*************/
  onAddAllownces(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isAllownces = true;
    }
    else {
      this.isAllownces = false;
    }

  }

  onAddDeduction(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isDeduction = true;
    }
    else {
      this.isDeduction = false;
    }

  }
 
  onAddOvertime(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isOvertime = true;
    }
    else {
      this.isOvertime = false;
    }

  }

//*************Navigate to User Role page***********/
BackTo_Page(){
  this.router.navigate(['/users-roles']);
}
//***********Save Personal Details*************/
Save_PersonalDetails(){
  this.PersonalDetailsForm.patchValue({
    user_code: this.usercode
  })
  let obj=this.PersonalDetailsForm.value;
 

  
  this.UserRole_Service.postUserPersonalDetail(obj)
  .subscribe(res=>{
        console.log("personal details is saved",res);
     this.userid=res['id']   
  
      });
      
     // console.log("personal details",obj);
      
  }
//**********Save Work Details*******************/
Save_WorkDetails(){
  let obj=this.WorkDetailsForm.value;
 
  
  this.UserRole_Service.putUserWorkDetail(this.userid,obj).subscribe(res=>{
        console.log("work details is saved",res);
        
  
      });
      console.log("Save work details",obj);
  

}
//**********Save Bank Details*******************/
Save_BankDetails(){
 
  
  let obj=this.BankPaymentDetailsForm.value;
 
  
  this.UserRole_Service.putUserBankDetail(this.userid,obj).subscribe(res=>{
        console.log("bank details is saved",res);
        
  
      });
  console.log("bank details",obj);
}
//**********Save Documents Details*******************/
Save_LegalDoc(){
  let obj=this.DocumentsForm.value
 
  
  this.UserRole_Service.putUserDocumnetDetail(this.userid,obj).subscribe(res=>{
    console.log("Save Documents is saved",res);
    

  });
  console.log("Save Documents details",obj);
}

//**********Save Assets Details*******************/
Save_Assets(){
  let obj=this.AssetDetailsForm.value;
  

  
  this.UserRole_Service.putAssetsDetails(this.userid,obj).subscribe(res=>{
    console.log("assets is saved",res);
    

  });
  console.log("Save Assets details",obj);



}
//**********Save Credentials Details*******************/
Save_Credentials(){
 

  let obj=this.CredentialsForm.value
 
  console.log("creadentials save",obj);
  
  this.UserRole_Service.putUserCredentialDetails(this.userid,obj).subscribe(res=>{
    console.log("credential details is saved",res);
    

  });
  console.log("Save Credential details",obj);
this.UserRole_Service.getAllUser().subscribe(data=>{
  console.log("data",data);
  
})
  this.router.navigate(['/users-roles']);
  
}


//***************Function For Add Multiple documents */

 addMultipleDocuments(){
   
      this.docArr.push(this.docArr.length);
      console.log("Added Multiple name and documents", this.docArr);
   
  }
//****************Function For Add Salary********** */
showSalaryDiv(){
this.isShowSalary=true;
}
//************function for add allownces and amount***********
showAlloncesDiv(){
  this.showAllowncesField=true;
}
//************function for add allownces and amount***********

showDeductionDiv(){
  this.showDeductionField=true;
}
//************function for add over time************** */
showOverTimeDiv(){
this.showOverTimeField=true;
}
//**********Show Assets row**********/
increaseAssets(){
  this.showAssetRow=true;
  this.assetArr.push(this.assetArr.length);
  console.log("Added Multiple name and documents", this.assetArr);
}

//***********patch value for the table********** */
patchUserDetails(){
  this.UserAllDetailsForm.patchValue({
    employee_name:this.userData.value['employee_name'],
      contact_number:this.userData.value['contact_number'],
      email_id:this.userData.value['email_id'],
      photo:this.userData.value['photo'],
      first_address:this.userData.value['first_address'],
      second_address:this.userData.value['second_address'],
      state:this.userData.value['employee_name'],
      city:this.userData.value['employee_name'],
      pin_code:this.userData.value['pin_code'],
      gender:this.userData.value['gender'],
      birth_date:this.userData.value['birth_date'],
      joiningDate:this.userData.value['joiningDate'],
      blood_group:this.userData.value['blood_group'],
      marrital_status:this.userData.value['marrital_status'],
      emergency_contact_personName:this.userData.value['emergency_contact_personName'],
      emg_contact_number:this.userData.value['emg_contact_number'],
      openMon: this.userData.value['openMon'],
      openTue: this.userData.value['openTue'],
      openWed:this.userData.value['openWed'],
      openThur: this.userData.value['openThur'],
      openFri: this.userData.value['openFri'],
      openSat: this.userData.value['openSat'],
      openSun:this.userData.value['openSun'],

      extraopenMon:this.userData.value['extraopenMon'],
      extraopenTue: this.userData.value['extraopenTue'],
      extraopenWed:this.userData.value['extraopenWed'],
      extraopenThur: this.userData.value['extraopenThur'],
      extraopenFri:this.userData.value['extraopenFri'],
      extraopenSat:this.userData.value['extraopenSat'],
      extraopenSun:this.userData.value['extraopenSun'],

      closeMon:this.userData.value['closeMon'],
      closeTue:this.userData.value['closeTue'],
      closeWed:this.userData.value['closeWed'],
      closeThur:this.userData.value['closeThur'],
      closeFri:this.userData.value['closeFri'],
      closeSat:this.userData.value['closeSat'],
      closeSun:this.userData.value['closeSun'],

      extracloseMon:this.userData.value['extracloseMon'],
      extracloseTue:this.userData.value['extracloseTue'],
      extracloseWed:this.userData.value['extracloseWed'],
      extracloseThur:this.userData.value['extracloseThur'],
      extracloseFri:this.userData.value['extracloseFri'],
      extracloseSat:this.userData.value['extracloseSat'],
      extracloseSun:this.userData.value['extracloseSun'],
      Sanctioned_Leaves:this.userData.value['Sanctioned_Leaves'],
      employee:this.userData.value['employee'],
      responsibilities:this.userData.value['responsibilities'],
      file:this.userData.value['file'],
      account_number:this.userData.value['account_number'],
      IFSC_code:this.userData.value['IFSC_code'],
      bank_name:this.userData.value['bank_name'],
      bank_address:this.userData.value['bank_address'],
      bank_state:this.userData.value['bank_state'],
      bank_city:this.userData.value['bank_city'],
      salary_amount:this.userData.value['salary_amount'],
      add_allownces:this.userData.value['add_allownces'],
      add_deduction:this.userData.value['add_deduction'],
      select_overtime:this.userData.value['select_overtime'],
      allownces_name:this.userData.value['allownces_name'],
      allownces_amount:this.userData.value['allownces_amount'],
      deduction_name:this.userData.value['deduction_name'],
      deduction_amount:this.userData.value['deduction_amount'],
      overTime_name:this.userData.value['overTime_name'],
      overTime_amount:this.userData.value['overTime_amount'],
      incentive_amount:this.userData.value['incentive_amount'],
      net_salary:this.userData.value['net_salary'],
      aadhar_number:this.userData.value['aadhar_number'],
      aadhar_upload:this.userData.value['aadhar_upload'],
      licence_number:this.userData.value['licence_number'],
      licence_upload:this.userData.value['licence_upload'],
      passport_number:this.userData.value['passport_number'],
      passport_upload:this.userData.value['passport_upload'],
      joining_letter:this.userData.value['joining_letter'],
      joining_letter_file:this.userData.value['joining_letter_file'],
      add_document:this.userData.value['add_document'],
      add_document_file:this.userData.value['add_document_file'],
      document_name:this.userData.value['document_name'],
      name_document_file:this.userData.value['name_document_file'],
      joining_date:this.userData.value['joining_date'],
      assign_city:this.userData.value['assign_city'],
      assign_location:this.userData.value['assign_location'],
      Joining_letter_upload:this.userData.value['Joining_letter_upload'],
      // document_name:[''],
      // name_document_file:['']
      first_name:this.userData.value['first_name'],
      first_quantity:this.userData.value['first_quantity'],
      first_date:this.userData.value['first_date'],
       second_name:this.userData.value['second_name'],
      second_quntity:this.userData.value['second_quntity'],
       second_date:this.userData.value['second_date'],
       third_name:this.userData.value['third_name'],
       third_quntity:this.userData.value['third_quntity'],
      third_date:this.userData.value['third_date'],
      login_name:this.userData.value['login_name'],
      password:this.userData.value['password'],
      re_password:this.userData.value['re_password'],
      admin_username:this.userData.value['admin_username'],
      admin_password:this.userData.value['admin_password'],
  })
}
 //******************pop up for view roles*********************
 openDialog_SelectRole(): void {
  // console.log('view user ', element);

  const dialogRef = this.dialog.open(SelectRoleComponent, {
    disableClose: true,
    width: '50%',
    height: '300px',
    // data: element

  });
dialogRef.afterClosed().subscribe(res=>{
  console.log("result",res);
  this.resultRole=res;
  this.PersonalDetailsForm.patchValue({
    assigned_role:res._id
  });
  this.isShowRole=true;
  
})

}

isUploading(){
  this.ngxSpinner.show()
  console.log('yes change work');
  
}



//*************country state city**************** */
country_state_city(){
  this.UserRole_Service.country_state_city().subscribe(data => {
  console.log(data.country_json);
    this.country_json = data.country_json
  })
}
changeCountry(count) {
  this.state = this.country_json.find(state => state.name == count).states
  console.log(count);
  this.state_list=this.state
  this.state=Object.keys(this.state)
}
changeState(count){
  this.city=this.state_list[count]
}
//*******email function*** */
onEmailKeyup() {
  let abc = this.PersonalDetailsForm.controls['email_id'].value
  this.PersonalDetailsForm.patchValue({
    restaurant_email: abc.trim().toLowerCase().replace(/\s/g, "")

  })
}
}
