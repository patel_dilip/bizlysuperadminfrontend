import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';
import Swal from 'sweetalert2';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { MatDialog } from '@angular/material';
import { NgxSpinnerService } from "ngx-spinner";
import { SelectRoleComponent } from '../select-role/select-role.component';
// *** Photos Upload*//
const profilePic = environment.base_Url + "userrolebizly/uploadpersonaldetails";
//*****NDA file upload*** */
const  NDAdocumentfile = environment.base_Url + "userrolebizly/uploadpersonaldetails";
//*****Joining letter upload*******/
const JoiningLetUploader= environment.base_Url+"userrolebizly/uploadworkdetailsjoiningletter";
//**Files upload */
const aadharDoc = environment.base_Url+"userrolebizly/uploadaadharcard";
const driving = environment.base_Url+"userrolebizly/uploadlicence";
const passport = environment.base_Url+"userrolebizly/uploadpassport";
const docs = environment.base_Url+"userrolebizly/uploaddocumentfile";

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.scss']
})
export class EditUsersComponent implements OnInit {
  PersonalDetailsForm: FormGroup;
  WorkDetailsForm: FormGroup;
  BankPaymentDetailsForm: FormGroup;
  accessControlForm: FormGroup;
  DocumentsForm: FormGroup;
  CorporatDetailsForm: FormGroup;
  AssetDetailsForm: FormGroup;
  CredentialsForm: FormGroup;
  updateUserForm: FormGroup;

  State_list_per = ['Maharashtra', 'UP'];
  State_list = ['Maharashtra', 'UP'];
  City_list_per = ['pune', 'Amaravti', 'Mumbai'];
  City_list = ['pune', 'Amaravti', 'Mumbai'];

  blood_group_list = ['A+', 'A-', 'B-', 'B+', 'AB+', 'AB-', 'O+', 'O-'];

  assign_city_list=['pune'];
  assign_city_location=['Dhanurkar'];
  allownces_list=['Petrol','Mobile Recharg','Medical'];
  deduction_list=['Petrol','Mobile Recharg','Medical'];
  overtime_list=['1 hour','2 hour','3 hour','4 hour']

  isShowSalary: boolean = false;
  showAssetRow: boolean = false;

  docArr: any = [0];
  assetArr: any = [0]
  userData: any = []

  userid: any;

  country_json:any=[];
  state=[];
  city=[];
  state_list: any;
   //working detail
 

 mondayOpenTime: any;
 mondayCloseTime: any;
 extramonOpenTime: any;
 extamonCloseTime: any;


 isAllownces =false;
 isDeduction =false;
 isOvertime =false;

 isMon = true;
 isTues = false;
 isWed = false;
 isThur = false;
 isFri = false;
 isSat = false;
 isSun = false;

 monAddHr = false
 tueAddHr = false
 wedAddHr = false
 thurAddHr = false
 friAddHr = false
 satAddHr = false
 sunAddHr = false

 resultRole="";
  // photo upload urls

  public profilePicUploader: FileUploader = new FileUploader({ url: profilePic, itemAlias: 'personaldetails' });

  //NDA file upload url
  public NDAPicUploader: FileUploader = new FileUploader({ url: NDAdocumentfile, itemAlias: 'workdetailsNDAfile' });
  //joining letter upload urls

  public joiningLetterUploader:FileUploader = new FileUploader({url:JoiningLetUploader,itemAlias: 'workdetailsjoiningletter'})

      // file upload urls
      public addharUploader: FileUploader = new FileUploader({url: aadharDoc, itemAlias: 'aadharcard'})
      public drivingUploader: FileUploader = new FileUploader({url: driving, itemAlias: 'licence'})
      public passportUploader: FileUploader = new FileUploader({url: passport, itemAlias: 'passport'})
      public docsUploader: FileUploader = new FileUploader({url: docs, itemAlias: 'documentfile'})
      aadharRes: any;
      aadharStatus: boolean;
      LicenRes: any;
      LicStatus: boolean;
      passRes: any;
      passStatus: boolean;
      documentRes: any;
      docStatus: boolean;
  profilepic: any;
  propicStatus: boolean;
  photoFile: any;
  isShowPhoto: boolean = true;
  isShowUpdateProfile: boolean = false;

  NDAdocumentfile:any;
  ndaPicStatus:boolean;
  isShowNDA:boolean=true;
  isShowUpdateNDA:boolean=false;

  JoiningLetUploader:any;
  joiningLetterStatus:boolean;
  isShowJoiningLetter:boolean =true;
  isShowUpdateJoiningLetter:boolean =false;
  

  showAllowncesField:boolean=false;
  showDeductionField:boolean=false;
  showOverTimeField:boolean=false;
  assetData: any=[];
 
  isShowRole:boolean=false;

  selecteCountry:any=[];
  selectedState:any=[];
  selectedCity:any=[]
  userRole: any;
  constructor(private formbuilder: FormBuilder,
    private router: Router,
    private dialog: MatDialog,
    private userRole_service: UsersRolesService,
    private ngxSpinner: NgxSpinnerService) {
    this.userRole_service.usersEdit;
    if (this.userRole_service.usersEdit !== undefined) {
      console.log("edited roles", this.userRole_service.usersEdit);
      this.userData = this.userRole_service.usersEdit
      this.profilepic = this.userData.personal_details.photo
      this.userRole = this.userData.personal_details.assigned_role
      this.selecteCountry=this.userData.personal_details.country
      this.selectedState=this.userData.personal_details.state
      this.selectedCity= this.userData.personal_details.city
console.log(this.userData.personal_details.country,
 this.userData.personal_details.state,
 this.userData.personal_details.city,);
this.assetData=this.userData.assets.assetsArr

    } else {
      this.router.navigate(["/users-roles"])


    }
    let localData = JSON.parse(localStorage.getItem('loginUser'));
    this.userid = localData._id
    console.log("localStorage", this.userid);
    this.country_state_city();
   //this.changeCountry('Indonesia');
  //  this.changeState(this.userData.personal_details.state)
  }

  ngOnInit() {
    this.PersonalDetailsForm = this.formbuilder.group({
      employee_name: [''],
      contact_number: [''],
      email_id: [''],
      photo: [''],
      first_address: [''],
      second_address: [''],
      country:[''],
      state: [''],
      city: [''],
      pin_code: [''],
      gender: [''],
      birth_date: [''],
      blood_group: [''],
      marrital_status: [''],
      emergency_contact_personName: [''],
      emg_contact_number: [''],
      mother_Name: [''],
      father_husbandName: [''],
      fam_Contact_no: [''],
      assigned_role: [''],
      userid: this.userid,
    });

    this.WorkDetailsForm = this.formbuilder.group({
      isMon:Boolean,
      isTues:Boolean,
      isWed :Boolean,
      isThur:Boolean,
      isFri :Boolean,
      isSat:Boolean,
      isSun :Boolean,
      openMon: [''],
      openTue: [''],
      openWed: [''],
      openThur: [''],
      openFri: [''],
      openSat: [''],
      openSun: [''],

      extraopenMon: [''],
      extraopenTue: [''],
      extraopenWed: [''],
      extraopenThur: [''],
      extraopenFri: [''],
      extraopenSat: [''],
      extraopenSun: [''],

      closeMon: [''],
      closeTue: [''],
      closeWed: [''],
      closeThur: [''],
      closeFri: [''],
      closeSat: [''],
      closeSun: [''],

      extracloseMon: [''],
      extracloseTue: [''],
      extracloseWed: [''],
      extracloseThur: [''],
      extracloseFri: [''],
      extracloseSat: [''],
      extracloseSun: [''],
      Sanctioned_Leaves: [''],
      joining_letter: [''],
      joining_date: [''],
      assign_location: [''],
      assign_city: [''],
      responsibilities: [''],
      NDA_file: [''],
      // userid:['']

    });
    this.BankPaymentDetailsForm = this.formbuilder.group({
      account_number: [''],
      IFSC_code: [''],
      bank_name: [''],
      bank_address: [''],
      bank_state: [''],
      bank_city: [''],
      salary_amount: [''],
      add_allownces: [''],
      add_deduction: [''],
      select_overtime: [''],
      allownces_name: [''],
      allownces_amount: [''],
      deduction_name: [''],
      deduction_amount: [''],
      overTime_name: [''],
      overTime_amount: [''],
      incentive_amount: [''],
      net_salary: [''],
      isAllownces:Boolean,
      isDeduction:Boolean,
      isOvertime :Boolean,
    });
    this.DocumentsForm = this.formbuilder.group({
      aadhar_number: [''],
      
      aadhar_upload: [''],
      licence_number: [''],
      licence_upload: [''],
      passport_number: [''],
      passport_upload: [''],
      joining_letter: [''],
      joining_letter_file: [''],
      add_document: [''],
      add_document_file: [''],
      document_name: [''],
      name_document_file: [''],

      
      
    });

    this.accessControlForm = this.formbuilder.group({

    })
    this.AssetDetailsForm=this.formbuilder.group({

      assetsArr: ['']// form array first step
      });

 
     this.AssetDetailsForm.patchValue({
        assetsArr:this.assetData
      })

    this.addAssetRow()
   console.log('controls assets', this.AssetDetailsForm.value)

      
      // By Default 1 add row
      // this.addAssetRow()
      this.CredentialsForm=this.formbuilder.group({
        bizUser_email:[''],
        password:[''],
        re_password:[''],
        admin_username:[''],
        admin_password:[''],
       
  
      });
    // ********profile pic upload***********/

    this.profilePicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.profilepic = response;

      if (status == 200) {
        Swal.fire('Profile Picture Uploaded Successfully', '', 'success')
        this.propicStatus = true
     
        // patch value
        this.PersonalDetailsForm.patchValue({
          photo: response
        })

        console.log(this.PersonalDetailsForm.value);

      }
      else {
        Swal.fire('Failed to Upload!', '', 'warning')
        this.propicStatus = false
      }

      this.ngxSpinner.hide()
    }

    //*************NDA file upload******** */
    this.NDAPicUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.NDAdocumentfile = response;

      if (status == 200) {
        Swal.fire('Profile Picture Uploaded Successfully', '', 'success')
        this.ndaPicStatus = true
      
        // patch value
        this.BankPaymentDetailsForm.patchValue({
          NDA_file: response
        })

        console.log(this.BankPaymentDetailsForm.value);

      }
      else {
        Swal.fire('Failed to Upload!', '', 'warning')
        this.ndaPicStatus = false
      }

      this.ngxSpinner.hide()
    }

     //*************NDA file upload******** */
     this.joiningLetterUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.JoiningLetUploader = response;

      if (status == 200) {
        Swal.fire('Profile Picture Uploaded Successfully', '', 'success')
        this.joiningLetterStatus = true
        this.isShowJoiningLetter = true;
        this.isShowUpdateJoiningLetter = false;
        // patch value
        this.BankPaymentDetailsForm.patchValue({
          joining_letter: response
        })

        console.log(this.BankPaymentDetailsForm.value);

      }
      else {
        Swal.fire('Failed to Upload!', '', 'warning')
        this.joiningLetterStatus = false
      }

      this.ngxSpinner.hide()
    }
   //**************dRIVING LICENSE upload */

   this.drivingUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.LicenRes = response
    this.ngxSpinner.hide()
    // Check Condition upload successful
    if(status == 200){

     
      Swal.fire('License Uploaded Successfully','','success')
      this.LicStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        licence_upload: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{

      this.ngxSpinner.hide()
      Swal.fire('Failed to Upload!','','warning')
      this.LicStatus = false
    }

    this.ngxSpinner.hide()
  }
   //**************pASSPORT upload */

   this.passportUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.passRes = response

    // Check Condition upload successful
    if(status == 200){

     
      Swal.fire('Passport Uploaded Successfully','','success')
      this.passStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        passport_upload: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{

      this.ngxSpinner.hide()
      Swal.fire('Failed to Upload!','','warning')
      this.passStatus = false
    }

    this.ngxSpinner.hide()
  }
   //**************dOCUMENTS upload */

   this.docsUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    console.log('FileUpload:uploaded:', item, status, response);
    this.documentRes = response

    // Check Condition upload successful
    if(status == 200){

     
      Swal.fire('Document Uploaded Successfully','','success')
      this.docStatus = true
      // Patch Value 
      this.DocumentsForm.patchValue({
        add_document_file: response
      })

      console.log('Document form value', this.DocumentsForm.value);
      
    }
    else{

      this.ngxSpinner.hide()
      Swal.fire('Failed to Upload!','','warning')
      this.docStatus = false
    }

    this.ngxSpinner.hide()
  }
    //*************addhar upload */
    this.addharUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.aadharRes = response
  
      // Check Condition upload successful
      if(status == 200){
        Swal.fire('Aadhar Uploaded Successfully','','success')
        this.aadharStatus = true
        // Patch Value 
        this.DocumentsForm.patchValue({
          aadhar_upload: response
        })
  
        console.log('Document form value', this.DocumentsForm.value);
        
      }
      else{
        Swal.fire('Failed to Upload!','','warning')
        this.aadharStatus = false
      }
      this.ngxSpinner.hide()
    }
    
    //*******************patch value for personal details************** */
    this.PersonalDetailsForm.patchValue({
      employee_name: this.userData.personal_details.employee_name,
      contact_number: this.userData.personal_details.contact_number,
      email_id: this.userData.personal_details.email_id,
      photo: this.userData.personal_details.photo,
      first_address: this.userData.personal_details.first_address,
      second_address: this.userData.personal_details.second_address,
      country:this.userData.personal_details.country,
      state: this.userData.personal_details.state,
      city: this.userData.personal_details.city,
      pin_code: this.userData.personal_details.pin_code,
      gender: this.userData.personal_details.gender,
      birth_date: this.userData.personal_details.birth_date,
      joiningDate: this.userData.personal_details.joiningDate,
      blood_group: this.userData.personal_details.blood_group,
      marrital_status: this.userData.personal_details.marrital_status,
      emergency_contact_personName: this.userData.personal_details.emergency_contact_personName,
      emg_contact_number: this.userData.personal_details.emg_contact_number,

      mother_Name: this.userData.personal_details.mother_Name,
      father_husbandName: this.userData.personal_details.father_husbandName,
      fam_Contact_no: this.userData.personal_details.fam_Contact_no,
      assigned_role:this.userData.personal_details.assigned_role,
    });

console.log("after patch",this.PersonalDetailsForm.value);

    //****************patch value for work details************/

    this.WorkDetailsForm.patchValue({
      isMon: this.userData.work_details.isMon,
      isTues: this.userData.work_details.isTues,
      isWed : this.userData.work_details.isWed,
      isThur: this.userData.work_details.isThur,
      isFri : this.userData.work_details.isFri,
      isSat: this.userData.work_details.isSat,
      isSun : this.userData.work_details.isSun,
      work_schedule: this.userData.work_details.work_schedule,
      openMon: this.userData.work_details.openMon,
      closeMon: this.userData.work_details.closeMon,
      openTue: this.userData.work_details.openTue,
      closeTue: this.userData.work_details.closeTue,
      openWen: this.userData.work_details.openWen,
      closeWen: this.userData.work_details.closeWen,
      openThir: this.userData.work_details.openThir,
      closeThir: this.userData.work_details.closeThir,
      openFir: this.userData.work_details.openFir,
      closeFir: this.userData.work_details.closeFir,
      openSat: this.userData.work_details.openSat,
      closeSat: this.userData.work_details.closeSat,
      Sanctioned_Leaves: this.userData.work_details.Sanctioned_Leaves,
      employee: this.userData.work_details.employee,
      joining_date: this.userData.work_details.joining_date,
      assign_location: this.userData.work_details.assign_location,
      assign_city: this.userData.work_details.assign_city,
      responsibilities: this.userData.work_details.responsibilities,
      joining_letter:this.userData.work_details.joining_letter,
      openWed:this.userData.work_details.openWed,
      openThur:this.userData.work_details.openThur,
      openFri:this.userData.work_details.openFri,
     
      openSun:this.userData.work_details.openSun,

      extraopenMon:this.userData.work_details.extraopenMon,
      extraopenTue:this.userData.work_details.extraopenTue,
      extraopenWed:this.userData.work_details.extraopenWed,
      extraopenThur:this.userData.work_details.extraopenThur,
      extraopenFri:this.userData.work_details.extraopenFri,
      extraopenSat:this.userData.work_details.extraopenSat,
      extraopenSun:this.userData.work_details.extraopenSun,

     
      closeWed:this.userData.work_details.closeWed,
      closeThur:this.userData.work_details.closeThur,
      closeFri:this.userData.work_details.closeFri,
      closeSun:this.userData.work_details.closeSun,

      extracloseMon:this.userData.work_details.extracloseMon,
      extracloseTue: this.userData.work_details.extracloseTue,
      extracloseWed: this.userData.work_details.extracloseWed,
      extracloseThur: this.userData.work_details.extracloseThur,
      extracloseFri:this.userData.work_details.extracloseFri,
      extracloseSat: this.userData.work_details.extracloseSat,
      extracloseSun: this.userData.work_details.extracloseSun,
     
      mondayOpenTime:this.userData.work_details.mondayOpenTime,
      mondayCloseTime:this.userData.work_details.mondayCloseTime,
      extramonOpenTime:this.userData.work_details.extramonOpenTime,
      extamonCloseTime:this.userData.work_details.extamonCloseTime,
      
     
      NDA_file: this.userData.work_details.NDA_file,
      //file:this.userData.work_details.file,

    });

    console.log("work detail after patch",this.WorkDetailsForm.value);
    
    this.isMon= this.userData.work_details.isMon,
    this.isTues= this.userData.work_details.isTues,
    this.isWed=this.userData.work_details.isWed,
    this.isThur= this.userData.work_details.isThur,
    this.isFri= this.userData.work_details.isFri,
    this.isSat= this.userData.work_details.isSat,
    this.isSun= this.userData.work_details.isSun
    //****************patch value for bank details************/

    this.BankPaymentDetailsForm.patchValue({
      isAllownces:this.userData.bank_details.isAllownces,
      isDeduction:this.userData.bank_details.isDeduction,
      isOvertime :this.userData.bank_details.isOvertime,
      account_number: this.userData.bank_details.account_number,
      IFSC_code: this.userData.bank_details.IFSC_code,
      bank_name: this.userData.bank_details.bank_name,
      bank_address: this.userData.bank_details.bank_address,
      bank_state: this.userData.bank_details.bank_state,
      bank_city: this.userData.bank_details.bank_city,
      salary: this.userData.bank_details.salary,
      allowances: this.userData.bank_details.allowances,
     
      salary_amount:this.userData.bank_details.salary_amount,
      add_allownces:this.userData.bank_details.add_allownces,
      add_deduction:this.userData.bank_details.add_deduction,
      select_overtime:this.userData.bank_details.select_overtime,
      allownces_name:this.userData.bank_details.allownces_name,
      allownces_amount:this.userData.bank_details.allownces_amount,
      deduction_name:this.userData.bank_details.deduction_name,
      deduction_amount:this.userData.bank_details.deduction_amount,
      overTime_name:this.userData.bank_details.overTime_name,
      overTime_amount:this.userData.bank_details.overTime_amount,
      incentive_amount:this.userData.bank_details.incentive_amount,
      net_salary:this.userData.bank_details.net_salary,
   

    });
    this.isAllownces=this.userData.bank_details.isAllownces,
    this.isDeduction=this.userData.bank_details.isDeduction,
    this.isOvertime=this.userData.bank_details.isOvertime,
    //****************patch value for work details************/

    this.DocumentsForm.patchValue({
      aadhar_number: this.userData.legal_documents.aadhar_number,
     // aadhar_upload:this.DocumentsForm.controls['aadhar_upload'],
      aadhar_upload:this.userData.legal_documents.aadhar_upload,
      licence_number: this.userData.legal_documents.licence_number,
      licence_upload:this.userData.legal_documents.licence_upload,
      passport_number: this.userData.legal_documents.passport_number,
      passport_upload:this.userData.legal_documents.passport_upload,
      joining_letter: this.userData.legal_documents.joining_letter,
      joining_letter_file:this.userData.legal_documents.joining_letter_file,
      add_document: this.userData.legal_documents.add_document,
      add_document_file:this.userData.legal_documents.add_document_file,

    });
      // patch addhar
      this.aadharRes = this.userData.legal_documents.aadhar_upload

    //patch licence
      this.LicenRes = this.userData.legal_documents.licence_upload

      //path passport
      this.passRes = this.userData.legal_documents.passport_upload

      //patch document
      this.documentRes = this.userData.legal_documents.add_document_file,
   
    // this.AssetDetailsForm.patchValue({
    //   assetsArr:this.userData.assets.assetsArr
    // });

this.CredentialsForm.patchValue({
  // bizUser_email:this.userData.credentials.bizUser_email,
  password:this.userData.credentials.password,
  re_password:this.userData.credentials.re_password,
  admin_username:this.userData.credentials.admin_username,
  admin_password:this.userData.credentials.admin_password,
})

  }

  //***********update form***** */
 
  initRequests() {
    throw new Error("Method not implemented.");
    
  }
  // **NG ONIT FINISH

  get assetForm(){
    return this.AssetDetailsForm.controls.assetsArr as FormArray /// **** Second Step in Form Array
    }
    
    
    addAssetRow(){ /// *** Third and Final Step in Form Array
    
    console.log("assets data",this.assetData);
    // this.assetData.forEach(element => {
    //   this.assetForm.push(element)
    // });
    
    this.assetForm.setValue(this.assetData)
    console.log('inside assets',this.AssetDetailsForm.value)
    }
    
    deleteAssetRow(index){
    this.assetForm.removeAt(index)
    
    console.log(this.AssetDetailsForm.value)
    }
    
    showAssetvalue(){
    
    console.log(this.AssetDetailsForm.value)
    }

     // ******************
  // WEEK RADIO BUTTON TIMES 

  onMonday(evt) {
    // alert('CLicked'+evt)
    console.log('event details:', evt.checked);

    if (evt.checked) {
      this.isMon = true;
    }
    else {
      this.isMon = false;
    }

  }

  onTuesday(evt) {
    if (evt.checked) {
      this.isTues = true;
    }
    else {
      this.isTues = false;
    }
  }

  onWednesday(evt) {
    if (evt.checked) {
      this.isWed = true;
    }
    else {
      this.isWed = false;
    }
  }

  onThursday(evt) {
    if (evt.checked) {
      this.isThur = true;
    }
    else {
      this.isThur = false;
    }
  }


  onFriday(evt) {
    if (evt.checked) {
      this.isFri = true;
    }
    else {
      this.isFri = false;
    }
  }

  onSaturday(evt) {
    if (evt.checked) {
      this.isSat = true;
    }
    else {
      this.isSat = false;
    }
  }

  onSunday(evt) {
    if (evt.checked) {
      this.isSun = true;
    }
    else {
      this.isSun = false;
    }
  }
    // after click on sameday 
    onSameAsMonday(chk) {

      console.log('checked vlaue', chk);
      if (this.monAddHr) {
        this.tueAddHr = true;
        this.wedAddHr = true;
        this.thurAddHr = true;
        this.friAddHr = true;
        this.satAddHr = true;
        this.sunAddHr = true;
      }
  
      // TIME VALUE SAME AS MONDAY
  
      this.mondayOpenTime = this.WorkDetailsForm.controls['openMon'].value
      this.mondayCloseTime = this.WorkDetailsForm.controls['closeMon'].value
  
      console.log('Extra open monday vlaue', this.WorkDetailsForm.controls['extraopenMon'].value);
      this.extramonOpenTime = this.WorkDetailsForm.controls['extraopenMon'].value
      this.extamonCloseTime = this.WorkDetailsForm.controls['extracloseMon'].value
  
  
      console.log('tIMES values ', this.mondayOpenTime);
  
      this.WorkDetailsForm.patchValue({
        openTue: this.mondayOpenTime,
        openWed: this.mondayOpenTime,
        openThur: this.mondayOpenTime,
        openFri: this.mondayOpenTime,
        openSat: this.mondayOpenTime,
        openSun: this.mondayOpenTime,
  
        extraopenTue: this.extramonOpenTime,
        extraopenWed: this.extramonOpenTime,
        extraopenThur: this.extramonOpenTime,
        extraopenFri: this.extramonOpenTime,
        extraopenSat: this.extramonOpenTime,
        extraopenSun: this.extramonOpenTime,
  
  
        closeTue: this.mondayCloseTime,
        closeWed: this.mondayCloseTime,
        closeThur: this.mondayCloseTime,
        closeFri: this.mondayCloseTime,
        closeSat: this.mondayCloseTime,
        closeSun: this.mondayCloseTime,
  
        extracloseTue: this.extamonCloseTime,
        extracloseWed: this.extamonCloseTime,
        extracloseThur: this.extamonCloseTime,
        extracloseFri: this.extamonCloseTime,
        extracloseSat: this.extamonCloseTime,
        extracloseSun: this.extamonCloseTime
  
      })
  
  
  
  
      //RADIO SAME AS MONDAY
      if (chk.target.checked) {
  
        this.isTues = this.isMon;
        this.isWed = this.isMon;
        this.isThur = this.isMon;
        this.isFri = this.isMon;
        this.isSat = this.isMon;
        this.isSun = this.isMon;
      }
  
      else {
        this.isTues = false;
        this.isWed = false;
        this.isThur = false;
        this.isFri = false;
        this.isSat = false;
        this.isSun = false;
      }
  
    }
 
    createNewElement(id) {
      // First create a DIV element.
      var txtNewInputBox = document.createElement('div');
  
      // Then add the content (a new input box) of the element.
      txtNewInputBox.innerHTML = "+<input type='time' value='16:00:00' > to <input type='time' value='23:00:00' >";
  
      // Finally put it where it is supposed to appear.
      if (id == 'mon') {
        // document.getElementById("monAddHTML").appendChild(txtNewInputBox);
        this.monAddHr = true;
      }
  
      if (id == 'tue') {
        // document.getElementById("tueAddHTML").appendChild(txtNewInputBox);
        this.tueAddHr = true;
      }
  
      if (id == 'wed') {
        // document.getElementById("wedAddHtml").appendChild(txtNewInputBox);
        this.wedAddHr = true;
      }
  
      if (id == 'thur') {
        // document.getElementById("thurAddHtml").appendChild(txtNewInputBox);
        this.thurAddHr = true;
      }
  
      if (id == 'fri') {
        // document.getElementById("friAddHtml").appendChild(txtNewInputBox);
        this.friAddHr = true;
      }
  
      if (id == 'sat') {
        // document.getElementById("satAddHtml").appendChild(txtNewInputBox);
        this.satAddHr = true;
      }
  
      if (id == 'sun') {
        // document.getElementById("sunAddHtml").appendChild(txtNewInputBox);
        this.sunAddHr = true;
      }
      //sunAddHtml
  
      console.log('Id ', id);
  
  
    }
  
    removeExtraTime(id) {
      if (id == 'mon') {
        // document.getElementById("monAddHTML").appendChild(txtNewInputBox);
        this.monAddHr = false;
      }
  
      if (id == 'tue') {
        // document.getElementById("tueAddHTML").appendChild(txtNewInputBox);
        this.tueAddHr = false;
      }
  
      if (id == 'wed') {
        // document.getElementById("wedAddHtml").appendChild(txtNewInputBox);
        this.wedAddHr = false;
      }
  
      if (id == 'thur') {
        // document.getElementById("thurAddHtml").appendChild(txtNewInputBox);
        this.thurAddHr = false;
      }
  
      if (id == 'fri') {
        // document.getElementById("friAddHtml").appendChild(txtNewInputBox);
        this.friAddHr = false;
      }
  
      if (id == 'sat') {
        // document.getElementById("satAddHtml").appendChild(txtNewInputBox);
        this.satAddHr = false;
      }
  
      if (id == 'sun') {
        // document.getElementById("sunAddHtml").appendChild(txtNewInputBox);
        this.sunAddHr = false;
      }
      //su
    }
  //*************Navigate to User Role page***********/
  BackTo_Page() {
    this.router.navigate(['/users-roles']);
  }
  //****************Function For Add Salary********** */
  showSalaryDiv() {
    this.isShowSalary = true;
  }
  //***********Save Personal Details*************/
  Save_PersonalDetails() {
    this.PersonalDetailsForm.patchValue({
      photo:this.profilepic    })
    let obj = this.PersonalDetailsForm.value;



    console.log("personal details", obj);
    this.userRole_service.updateUsers(this.userid, obj).subscribe(res => {
      console.log("update personal details", res);

    })
    

  }
  //**********Save Work Details*******************/
  Save_WorkDetails() {
    let obj = this.WorkDetailsForm.value;



    console.log("Save work details", obj);
    this.userRole_service.updateUsers(this.userid, obj).subscribe(res => {
      console.log("update work details", res);

    })

  }
  //**********Save Bank Details*******************/
  Save_BankDetails() {


    let obj = this.BankPaymentDetailsForm.value;


    console.log("bank details", obj);


    this.userRole_service.updateUsers(this.userid, obj).subscribe(res => {
      console.log("update bank details", res);

    })
  }
  //**********Save Documents Details*******************/
  Save_LegalDoc() {
    let obj = this.DocumentsForm.value


    console.log("Save Documents details", obj);

   
    this.userRole_service.updateUsers(this.userid, obj).subscribe(res => {
      console.log("update Documents details", res);

    })
  }

  //**********Save Assets Details*******************/
  Save_Assets() {
    let obj = this.AssetDetailsForm.value;



    console.log("Save Assets details", obj);

   
    this.userRole_service.updateUsers(this.userid, obj).subscribe(res => {
      console.log("update Assets details", res);

    })


  }
  //**********Save Credentials Details*******************/
  Save_Credentials() {


    let obj = this.CorporatDetailsForm.value



    console.log("Save Credential details", obj);

    this.userRole_service.updateUsers(this.userid, obj).subscribe(res => {
      console.log("update Credential details", res);
      this.router.navigate(['/users-roles']);
    })


  }
  //***************Function For Add Multiple documents */
  //***************Function For Add Multiple documents */

  addMultipleDocuments() {

    this.docArr.push(this.docArr.length);
    console.log("Added Multiple name and documents", this.docArr);

  }
  //**********Show Assets row**********/
  increaseAssets() {
    this.showAssetRow = true;
    this.assetArr.push(this.assetArr.length);
    console.log("Added Multiple name and documents", this.assetArr);
  }
  //************update user************ */
  updateUser() {
    let obj = this.updateUserForm.value
    console.log("Successfully role updated", obj);
    this.userRole_service.updateUser(this.userData._id, obj).subscribe(res => {

    })
  }
  //**********update joining letter*** */
  updateJoiningLetters(){
    this.isShowJoiningLetter=false;
    this.isShowUpdateJoiningLetter=true;
  }
  //*********show updated profile******* */
  updateProfile() {
    this.isShowPhoto = false
    this.isShowUpdateProfile = true;
  }
  //***********show updated NDA file*******/
  updateNDAFile(){
    this.isShowNDA =false
        this.isShowUpdateNDA =true
  }
  isUploading(){
    this.ngxSpinner.show()
    console.log('yes change work');
    
  }


 //**********Bank detail toggle*************/
 onAddAllownces(evt) {
  // alert('CLicked'+evt)
  console.log('event details:', evt.checked);

  if (evt.checked) {
    this.isAllownces = true;
  }
  else {
    this.isAllownces = false;
  }

}

onAddDeduction(evt) {
  // alert('CLicked'+evt)
  console.log('event details:', evt.checked);

  if (evt.checked) {
    this.isDeduction = true;
  }
  else {
    this.isDeduction = false;
  }

}

onAddOvertime(evt) {
  // alert('CLicked'+evt)
  console.log('event details:', evt.checked);

  if (evt.checked) {
    this.isOvertime = true;
  }
  else {
    this.isOvertime = false;
  }

}
//****************Function For Add Salary********** */

  //************function for add allownces and amount***********
  showAlloncesDiv(){
    this.showAllowncesField=true;
  }
  //************function for add allownces and amount***********
  
  showDeductionDiv(){
    this.showDeductionField=true;
  }
  //************function for add over time************** */
  showOverTimeDiv(){
  this.showOverTimeField=true;
  }
   //******************pop up for view roles*********************
 openDialog_SelectRole(element): void {
  console.log('view user ', element);

  const dialogRef = this.dialog.open(SelectRoleComponent, {
    disableClose: true,
    width: '50%',
    height: '300px',
    data: element
    //roles

  });
dialogRef.afterClosed().subscribe(res=>{
  console.log("result",res);
  this.resultRole=res;
  this.PersonalDetailsForm.patchValue({
    assigned_role:res._id
  });
  this.userRole = res
  this.isShowRole=true;
  
})

}
//*************country state city*************** /
country_state_city() {

  return new Promise((resolve, reject) => {
  this.userRole_service.country_state_city().subscribe(data => {
  console.log("counttry json", data.country_json);
  this.country_json = data.country_json
  resolve(this.country_json)
  })
  }).then(id => {
  this.country_json = id
  
  console.log("country json", this.country_json);
  // this.changeCountry(this.userData.personal_details.country)
  console.log("hi this is country state city");
  console.log("this is country data", this.userData.personal_details.country);
  
  this.changeCountry(this.userData.personal_details.country)
  this.userData.personal_details.country =''
  })
  
  
  }
  changeCountry(count) {
    let country = this.country_json.findIndex(x => x.name === "India")
    console.log("selected country", country);
    
    console.log("this is change country function", count);
    
    this.state = this.country_json.find(state => state.name == count).states
    console.log(count);
    this.state_list = this.state
    this.state = Object.keys(this.state)
    //this.changeState(this.userData.personal_details.state)
    console.log("this is country json value", this.country_json);
    this.changeState(this.userData.personal_details.state)
    this.userData.personal_details.state = ''
    }
    changeState(count) {
      this.city = this.state_list[count]
      console.log(this.PersonalDetailsForm.value);
      
      }

       // show value from country state city
  showValue(){
    console.log(this.PersonalDetailsForm.value);
  }
}
