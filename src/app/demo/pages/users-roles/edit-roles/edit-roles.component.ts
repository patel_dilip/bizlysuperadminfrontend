import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UsersRolesService } from 'src/app/_services/_UsersRoles/users-roles.service';

@Component({
  selector: 'app-edit-roles',
  templateUrl: './edit-roles.component.html',
  styleUrls: ['./edit-roles.component.scss']
})
export class EditRolesComponent implements OnInit {
  EditRolesForm:FormGroup;
  roleData:any=[]
  userid: any
  constructor(private router:Router,private formbuilder:FormBuilder,private user_role_serv:UsersRolesService) { 

// localstorage userid

let localData = JSON.parse(localStorage.getItem('loginUser'));
    this.userid = localData._id
    console.log('this is user id', this.userid);

this.user_role_serv.rolesEdit;
if(this.user_role_serv.rolesEdit!=undefined){
  console.log("edited roles",this.user_role_serv.rolesEdit);
  this.roleData=this.user_role_serv.rolesEdit

}else{
  this.router.navigate(["/users-roles"])
 

}


  }

  ngOnInit() {
    this.EditRolesForm=this.formbuilder.group({    
      role_ID: [''],
      role_name: [''],
      role_tag: [''],
      role_salary: [''],
      description: [''],  
      userid: [''],
    })

    this.EditRolesForm.patchValue({
      role_ID:this.roleData.role_ID,
      role_name:this.roleData.role_name,
      role_salary:this.roleData.role_salary,
      role_tag:this.roleData.role_tag,
      description:this.roleData.description,
     userid: this.userid
      
    })
  }
//*************Navigate to User Role page***********/
BackTo_Page(){
  this.router.navigate(['/users-roles']);
}
//****************Update Role***********/
updateRole(){
let obj=this.EditRolesForm.value
console.log("Successfully role updated",obj);
this.user_role_serv.updateRole(this.roleData._id,obj).subscribe(res=>{
console.log("role updated",res);
console.log("role id",this.roleData._id);
console.log("role obj",obj);
this.router.navigate(['/users-roles']);

})
}
//*************Navigate to User Role page***********/

}
