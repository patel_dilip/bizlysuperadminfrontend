import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.scss']
})
export class CreateRoleComponent implements OnInit {
  createRoleForm: FormGroup
  accessControl = [
    {
      "accessControlName": "restaurant",
      "controls": ["add", "view", "edit", "delete"]
    },
    { "accessControlName": "orders", "controls": ["add", "view", "edit", "delete"] },
    { "accessControlName": "menu", "controls": ["add", "view", "edit", "delete"] },
    { "accessControlName": "pos-orders", "controls": ["add", "view", "edit", "delete"] }
  ]
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createRoleForm = this.fb.group({
      roleID: ['',[Validators.required, Validators.pattern('^[ a-zA-Z0-9]+$')]],
      roleTag: ['',[ Validators.required,Validators.pattern('^[ a-zA-Z0-9]+$')]]
    })
  }
  
  selectAccessControl(event, name){
    console.log(event.target.value, name)
  }
  onCreateRole(){

  }
}
