import { Component, OnInit, ViewChild } from '@angular/core';
import { MainService } from 'src/app/_services/main.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { RolesViewComponent } from '../roles-view/roles-view.component';

export interface rolesElement{
  roleID: string;
  role:string;
  dateCreated:string;
  dateEdited:string;
  userEdited:string;
  totalUsers:string;
  status:boolean;
}

export interface usersElement {
  userID:string;
  UsersName:string;
  username: string;
  emailID:string;
  role:string;
  joiningDate:string;
  contactNumber:string;
  status:boolean;
}

const rolesDATA : rolesElement[]=[
  {roleID: "R001", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:true},
  {roleID: "R002", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:true},
  {roleID: "R003", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:false},
  {roleID: "R004", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:true},
  {roleID: "R005", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:false},
  {roleID: "R006", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:true},
  {roleID: "R007", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:true},
  {roleID: "R008", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:false},
  {roleID: "R009", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:true},
  {roleID: "R0010", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:true},
  {roleID: "R0011", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:false},
  {roleID: "R0012", role:"General Manager",dateCreated:"23/01/2019", dateEdited:"24/01/2019", userEdited:"AK", totalUsers:"13", status:true},

]

const usersDATA: usersElement[]=[
  { userID:"U001", UsersName:"ABC", username: "abc123", emailID:"abc@gmail.com", role:"Manager",  joiningDate:"25/01/2020", contactNumber:"987642654", status:true},
  { userID:"U002", UsersName:"ABC", username: "abc123", emailID:"abc@gmail.com", role:"Manager",  joiningDate:"25/01/2020", contactNumber:"987642654", status:false},
  { userID:"U003", UsersName:"ABC", username: "abc123", emailID:"abc@gmail.com", role:"Manager",  joiningDate:"25/01/2020", contactNumber:"987642654", status:true},
  { userID:"U004", UsersName:"ABC", username: "abc123", emailID:"abc@gmail.com", role:"Manager",  joiningDate:"25/01/2020", contactNumber:"987642654", status:true},
  { userID:"U005", UsersName:"ABC", username: "abc123", emailID:"abc@gmail.com", role:"Manager",  joiningDate:"25/01/2020", contactNumber:"987642654", status:false},
  { userID:"U006", UsersName:"ABC", username: "abc123", emailID:"abc@gmail.com", role:"Manager",  joiningDate:"25/01/2020", contactNumber:"987642654", status:true},
  { userID:"U007", UsersName:"ABC", username: "abc123", emailID:"abc@gmail.com", role:"Manager",  joiningDate:"25/01/2020", contactNumber:"987642654", status:true},
  { userID:"U008", UsersName:"ABC", username: "abc123", emailID:"abc@gmail.com", role:"Manager",  joiningDate:"25/01/2020", contactNumber:"987642654", status:false},

]


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  size=12
  firstLevel:any
  showRolesCategories:boolean=false

  //rolesDATA : rolesElement
  displayColumnsRoles: string[] = ['roleID', 'role','dateCreated', 'dateEdited','userEdited', 'totalUsers', 'status', 'action']
  dataSourceRoles: MatTableDataSource<rolesElement>
  @ViewChild("rolespaginator", { static: true }) rolespaginator: MatPaginator;
    @ViewChild("rolessort", { static: true }) rolessort: MatSort;
 
    //usersDATA: usersElement
    displayColumnsUsers: string[] = ['userID', 'UsersName', 'username', 'emailID', 'role',  'joiningDate', 'contactNumber', 'status', 'action']
    dataSourceUsers: MatTableDataSource<usersElement>
    @ViewChild("userspaginator", { static: true }) userspaginator: MatPaginator;
      @ViewChild("userssort", { static: true }) userssort: MatSort;
      

  constructor(private main:MainService, private dialog: MatDialog) {
    this.dataSourceRoles = new MatTableDataSource(rolesDATA);
    this.dataSourceUsers=new MatTableDataSource(usersDATA);
 this.main.getAllEstablishment().subscribe(data=>{
  console.log(data);
  this.firstLevel=data
})
   }

  ngOnInit() {
    this.dataSourceRoles.paginator = this.rolespaginator;
    this.dataSourceRoles.sort = this.rolessort;

    this.dataSourceUsers.paginator = this.userspaginator;
    this.dataSourceUsers.sort = this.userssort;

  }
  
  editRoles():void {
    const dialogRef = this.dialog.open(RolesViewComponent, {
      width: '600px',
    
     // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
     // this.animal = result;
    });
  }

  rolesCategory(event){
    console.log(event.srcElement.checked);
    if(event.srcElement.checked==true){
    this.size=8
    this.showRolesCategories=true
  } else {
    this.size=12
    this.showRolesCategories=false
  
  }
  }


  rolesapplyFilter(filterValue: string) {
    this.dataSourceRoles.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceRoles.paginator) {
      this.dataSourceRoles.paginator.firstPage();
    }
  }

  usersaapplyFilter(filterValue: string) {
    this.dataSourceUsers.filter = filterValue.trim().toLowerCase();
    if (this.dataSourceUsers.paginator) {
      this.dataSourceUsers.paginator.firstPage();
    }
  }
}
