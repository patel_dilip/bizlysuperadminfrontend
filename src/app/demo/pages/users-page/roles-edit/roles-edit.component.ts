import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-roles-edit',
  templateUrl: './roles-edit.component.html',
  styleUrls: ['./roles-edit.component.scss']
})
export class RolesEditComponent implements OnInit {
  
      createRoleForm: FormGroup
    accessControl = [
      {
        "accessControlName": "restaurant",
        "controls": ["add", "view", "edit", "delete"]
      },
      { "accessControlName": "orders", "controls": ["add", "view", "edit", "delete"] },
      { "accessControlName": "menu", "controls": ["add", "view", "edit", "delete"] },
      { "accessControlName": "pos-orders", "controls": ["add", "view", "edit", "delete"] }
    ]
    constructor(private fb: FormBuilder) { }
  
    ngOnInit() {
      this.createRoleForm = this.fb.group({
        roleID: [''],
        roleTag: ['']
      })
    }
    
    selectAccessControl(event, name){
      console.log(event.target.value, name)
    }
    onCreateRole(){
  
    }
  }
  