import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';




@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  personalDetailsForm: FormGroup;
  workDetailsForm: FormGroup;
  bankpaymentDetailsForm: FormGroup;
  documentForm: FormGroup;
  allFormGroup: FormGroup;
  account_types=["Saving Account", "Current Account"]
  marital_status_list = ['Unmarried', 'Married'];
  blood_group_list = ['A+', 'A-', 'B-', 'B+', 'AB+', 'AB-', 'O+', 'O-'];

   // Legal Docs URLs
  //  public aadharUploader: FileUploader = new FileUploader({ url: aadharDoc, itemAlias: 'aadharcard'});
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.personalDetailsForm = this._formBuilder.group({
      employeeName: ['',[Validators.required,Validators.pattern('^[ a-zA-Z]+$')]],
      contactNumber: ['',[ Validators.required,Validators.pattern('[0-9]{10}')]],
      emailID: ['', [Validators.required,Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+[\.]+[a-z]{2,4}$')]],
      photo: ['', Validators.required],
      address: ['', Validators.required],
      gender: ['Male', Validators.required],
      birthDate: ['', Validators.required],
      bloodGroup: ['', Validators.required],
      marritalStatus: ['', Validators.required],
      emergencyContactPersonName: ['']

    })
    this.workDetailsForm = this._formBuilder.group({
      workSchedule: ['',Validators.required],
      employee: ['',[Validators.required,Validators.pattern('^[ a-zA-Z]+$')]],
      nonDisclosureAggrement: ['',Validators.required]
    })
    this.bankpaymentDetailsForm = this._formBuilder.group({
     account_no: ['', [Validators.required, Validators.minLength(7),Validators.maxLength(20),Validators.pattern('^[0-9]{7,20}$')]],
     account_type: ['', Validators.required],
     bank_name: ['',[Validators.required,Validators.pattern('^[ a-zA-Z]+$')]],
     branch_name: ['', [Validators.required,Validators.pattern('^[ a-zA-Z]+$')]],
     bank_city: ['', [Validators.required,Validators.pattern('^[ a-zA-Z]+$')]],
     bank_address: ['', Validators.required],
     bank_ifsccode: ['', [Validators.required, Validators.maxLength(11),Validators.minLength(11),Validators.pattern('^[A-Za-z]{4}0[A-Z0-9a-z]{6}$')]],
      salaryPerScale: ['', Validators.required,Validators.pattern('^[0-9]$')],
      allowance: ['', Validators.required,Validators.pattern('^[0-9]$')]
    })
    this.documentForm = this._formBuilder.group({
      aadharCardNumberAndProof: ['', [Validators.required, Validators.pattern('^\d{4}\s\d{4}\s\d{4}$')]],
      drivingLicence: ['', [Validators.required,Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      passport: ['', [Validators.required,Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      joiningDate: ['', [Validators.required]],
      assignedToCity: ['', [Validators.required,Validators.pattern('^[ a-zA-Z]+$')]],
      assignedToLocality: ['', [Validators.required]],
      assetprovided: ['', [Validators.required,Validators.pattern('^[ a-zA-Z]+$')]],
      joiningLetter: ['', [Validators.required]],
      addDocument: ['']
    })
    this.allFormGroup = this._formBuilder.group({

    })


    // Aadhar Card Uploader
    // this.aadharUploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    //   console.log('FileUpload:uploaded:', item, status, response);
    // }
  }

}
