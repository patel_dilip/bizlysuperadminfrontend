import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-roles-view',
  templateUrl: './roles-view.component.html',
  styleUrls: ['./roles-view.component.scss']
})
export class RolesViewComponent implements OnInit {
obj={
  "accessPoint":true,
  "versions": [
      {
          "versionName": "version1",
          "created": {
              "dateCreatedandTime": "20/01/2020",
              "dateEditedandTime": "23/01/2020",
              "editedBy": "ak"
          }
      },
      {
          "versionName": "version2",
          "created": {
              "dateCreatedandTime": "20/01/2020",
              "dateEditedandTime": "23/01/2020",
              "editedBy": "ak"
          }
      },
      {
          "versionName": "version3",
          "created": {
              "dateCreatedandTime": "20/01/2020",
              "dateEditedandTime": "23/01/2020",
              "editedBy": "ak"
          }
      }
  ]
}

roleobj={
  "roleID":"R005","roleTag":"Manager",
  "accesControl":[ {
    "accessControlName": "restaurant",
    "controls": ["add", "delete"]
  },
  { "accessControlName": "orders", "controls": [ "view", "edit", "delete"] },
  { "accessControlName": "menu", "controls": ["add", "edit", "delete"] },
  { "accessControlName": "pos-orders", "controls": [ "delete"] }
]
}
  constructor(public dialogRef: MatDialogRef<RolesViewComponent>) { }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
