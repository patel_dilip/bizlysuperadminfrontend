import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersHomeComponent } from './users-home/users-home.component';
import { UsersListComponent } from './users-list/users-list.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { RolesEditComponent } from './roles-edit/roles-edit.component';
import { CreateRoleComponent } from './create-role/create-role.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ViewUserComponent } from './view-user/view-user.component';


const routes: Routes = [
  {
    path: '',
    component: UsersHomeComponent,
    children: [
    {
      path: '',
      component: UsersListComponent
    },{
      path: 'edit-role',
      component: RolesEditComponent
    },
    {
      path: 'create-role',
      component: CreateRoleComponent
    },{
      path:'create-user',
      component: CreateUserComponent
    },{
      path: 'view-user',
      component: ViewUserComponent
    },{
      path: 'edit-user',
      component: EditUserComponent
    }
  ]
  }
]

@NgModule({
  declarations: [UsersHomeComponent, UsersListComponent, RolesEditComponent, CreateRoleComponent, CreateUserComponent, EditUserComponent, ViewUserComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    DataTablesModule
  ]
})
export class UsersPageModule { }
