import { Component, OnInit } from '@angular/core';
import{ChargeService}from '../../../../_services/charges/charge.service'

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  productlist=[];

  constructor(private chargesService:ChargeService) {
    this.getallproduct()
   }

  ngOnInit() {
    

  }
  getproduct(id)
  {
    console.log(id);
    this.chargesService.viewObject=id
    
  }
  getallproduct(){
    this.chargesService.getProduct().subscribe(data=>{
      console.log(data);
      data['data'].forEach(element => {
        if(element.completeData[0].country['status']){
          this.productlist.push(element)
        }
      });
      

      
    })
  }


}
