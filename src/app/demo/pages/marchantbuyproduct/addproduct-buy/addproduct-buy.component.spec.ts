import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddproductBuyComponent } from './addproduct-buy.component';

describe('AddproductBuyComponent', () => {
  let component: AddproductBuyComponent;
  let fixture: ComponentFixture<AddproductBuyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddproductBuyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddproductBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
