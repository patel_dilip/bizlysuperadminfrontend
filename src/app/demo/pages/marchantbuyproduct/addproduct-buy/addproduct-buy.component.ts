import { Component, OnInit, ɵSWITCH_TEMPLATE_REF_FACTORY__POST_R3__ } from '@angular/core';
import { ChargeService } from '../../../../_services/charges/charge.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { PosOrderService } from 'src/app/_services/pos_order/pos-order.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-addproduct-buy',
  templateUrl: './addproduct-buy.component.html',
  styleUrls: ['./addproduct-buy.component.scss']
})
export class AddproductBuyComponent implements OnInit {
  productid:any
  buyProduct:FormGroup
  // addMenuFoodsService: any;
  perProductPrice:any
  country_json=[];
  country=""
  state=[];
  state_list=[];
  city=[];
  productData: any;
  noOfoutlets=1
  posProductOrder:any
  tax: number;
  gstNo= false;
  //pos product ordeer

  //count
  clicked:boolean;


  constructor(private chargesService:ChargeService, private router: Router, private fb: FormBuilder, private addMenuFoodsService:AddMenuFoodsService, private posproductService : PosOrderService
    ) { 
    if(chargesService.viewObject!=undefined){
    this.productid=chargesService.viewObject
    console.log(this.productid);
    this.country_state_city()

    }else{
      router.navigate(['/marchant-Buy-product'])
    }
    
  }
  
  getProductDetiils(){
    this.chargesService.getSingleProduct(this.productid).subscribe(data=>{
      if(data)
      console.log("product data=>",data['data']);
      this.productData=data['data']
    this.country=data['data'].completeData[0].country.item_text
    this.changeCountry()
    this.posProductOrder={
      productName: this.productData._id,
      tax: 18,
      paymentStatus: "Paid",
      discount: {
          percentage: 10,
          amount: 0
      },
      pricePerOutlet:Number,
      DiscountedPrice: Number,
      PriceafterDiscount: Number,
      totalOrderAmount: Number,
      totalPaidAmountwithTax: Number,
      paidBy: "Avinash Patil",
      subscription_Type: this.productData.completeData[0].colletionMode,
      transcationID: "TRAN0002",
      transactionDate_Time: "29 sept 2020",
      // gst: {isGST: Boolean, GSTNo: String},
  }
    this.productData.completeData[0].tableData.forEach((element , index) => {
      if(element.from<=this.noOfoutlets && element.to>= this.noOfoutlets){
          console.log("pp",element.price);
          this.perProductPrice=element.price.replace(/,/g,"");
          console.log(element.price.replace(/,/g,""));
          this.posProductOrder.pricePerOutlet=this.perProductPrice
          this.posProductOrder.totalOrderAmount=this.perProductPrice*this.noOfoutlets
          this.posProductOrder.DiscountedPrice=this.posProductOrder.totalOrderAmount*(this.posProductOrder.discount.percentage / 100);
          this.posProductOrder.PriceafterDiscount=this.posProductOrder.totalOrderAmount-this.posProductOrder.DiscountedPrice
this.posProductOrder.totalPaidAmountwithTax=this.posProductOrder.PriceafterDiscount+(this.posProductOrder.PriceafterDiscount* (this.posProductOrder.tax/100))
this.tax=this.posProductOrder.PriceafterDiscount* (this.posProductOrder.tax/100)
          
      }
  });
    })
  }
  country_state_city(){
    this.addMenuFoodsService.country_state_city().subscribe(data => {
  // console.log("============", data);
  
    console.log(data.country_json);
      this.country_json = data.country_json
  
  
      for (var i = 0; i < data.country_json.length - 1; i++)
      //console.log("=========================");
      
        console.log(data.country_json[i].name);
        this.getProductDetiils()
  
    })
  }
  changeCountry() {
    console.log(this.country);
    this.state = this.country_json.find(state => state.name == this.country).states
   
    this.state_list=this.state
    this.state=Object.keys(this.state)
   console.log(this.state_list);
    // console.log(this.state['Maharashtra']);
    
  
    // console.log(this.villege);
  
  }
  changeState(count){
    console.log(count);
    
    this.city=this.state_list[count]
    console.log(this.city);
    
  }
  ngOnInit() {
    this.buyProduct=this.fb.group({
      organization_name:['', Validators.required],
      representative_email: ['', Validators.required],
      representative_contact: ['', Validators.required],
      org_email:['', Validators.required],
      org_contact:['', Validators.required],
      noOfoutlets:['', Validators.required],
      address: ['', Validators.required],
      state:['', Validators.required],
      city: ['', Validators.required],
      posProductOrder:[''],
      representative_name:['', Validators.required],
      gst:['', Validators.required]
     
    })
  
   
  }
  plus(outlets ){
    this.noOfoutlets=outlets+1
    this.posProductOrder.noOfoutlets=this.noOfoutlets
    this.productData.completeData[0].tableData.forEach((element , index) => {
    if(element.from<=this.noOfoutlets && element.to>= this.noOfoutlets){
        console.log(element.price)
        this.perProductPrice=element.price.replace(/,/g,"");
        console.log(element.price.replace(/,/g,""));
        
        this.posProductOrder.totalOrderAmount=this.perProductPrice*this.noOfoutlets
        this.posProductOrder.DiscountedPrice=this.posProductOrder.totalOrderAmount*(this.posProductOrder.discount.percentage / 100);
        this.posProductOrder.PriceafterDiscount=this.posProductOrder.totalOrderAmount-this.posProductOrder.DiscountedPrice
this.posProductOrder.totalPaidAmountwithTax=this.posProductOrder.PriceafterDiscount+(this.posProductOrder.PriceafterDiscount* (this.posProductOrder.tax/100))
this.tax=this.posProductOrder.PriceafterDiscount* (this.posProductOrder.tax/100)
        
    } 
});
   
  }
  minus(outlets ){
    this.noOfoutlets=outlets-1
    this.posProductOrder.noOfoutlets=this.noOfoutlets
    this.productData.completeData[0].tableData.forEach((element , index) => {
      if(element.from<=this.noOfoutlets && element.to>= this.noOfoutlets){  console.log("pp",element.price);
      this.perProductPrice=element.price.replace(/,/g,"");
      console.log(element.price.replace(/,/g,""));
      
      this.posProductOrder.totalOrderAmount=this.perProductPrice*this.noOfoutlets
      this.posProductOrder.DiscountedPrice=this.posProductOrder.totalOrderAmount*(this.posProductOrder.discount.percentage / 100);
      this.posProductOrder.PriceafterDiscount=this.posProductOrder.totalOrderAmount-this.posProductOrder.DiscountedPrice
this.posProductOrder.totalPaidAmountwithTax=this.posProductOrder.PriceafterDiscount+(this.posProductOrder.PriceafterDiscount* (this.posProductOrder.tax/100))
this.tax=this.posProductOrder.PriceafterDiscount* (this.posProductOrder.tax/100)
      
  }
  });
     
  }
  merchantBuyProduct(){
    this.buyProduct.patchValue({
      posProductOrder:this.posProductOrder
    })
 
    console.log(this.buyProduct.value);
    
    this.posproductService.BuyProduct(this.buyProduct.value).subscribe(data=>{
      this.clicked=false
      console.log(data);
      if(data['sucess']==true)
{
Swal.fire("Done","Buy Product Successfully", "success")
  this.router.navigate(['/marchant-Buy-product'])
}      
    })
    
  }
  
  gst(event){
    console.log(event);
   
  
      if(event.value=='false'){
      this.buyProduct.patchValue({
        gst:{isGST:event.value,GSTNo:"NA" }
      })
      this.gstNo=false

      }else{
        this.gstNo=true
      }
  
  }
  gstnum(value){
    this.buyProduct.patchValue({
      gst:{isGST:true,GSTNo:value }
    })

  }
}
