import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyproductComponent } from './buyproduct/buyproduct.component';
import { ProductListComponent } from './product-list/product-list.component';
import { AddproductBuyComponent } from './addproduct-buy/addproduct-buy.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/theme/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: BuyproductComponent,
    children: [
      {
        path  :'',
        component :ProductListComponent
      },
      {
        path  :'buy-product',
        component :AddproductBuyComponent
      }
      
    ]
  }
];


@NgModule({
  declarations: [BuyproductComponent, ProductListComponent, AddproductBuyComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
  ]
})
export class MarchantbuyproductModule { }
