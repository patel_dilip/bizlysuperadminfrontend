import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addon-pos-order-edit',
  templateUrl: './addon-pos-order-edit.component.html',
  styleUrls: ['./addon-pos-order-edit.component.scss']
})
export class AddonPosOrderEditComponent implements OnInit {

  constructor(private route:Router) { }

  ngOnInit() {
  }
  addonposorderView(): void {
    this.route.navigateByUrl("/pos-orders/addon-pos-order-view");
  }


}
