import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddonPosOrderEditComponent } from './addon-pos-order-edit.component';

describe('AddonPosOrderEditComponent', () => {
  let component: AddonPosOrderEditComponent;
  let fixture: ComponentFixture<AddonPosOrderEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddonPosOrderEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddonPosOrderEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
