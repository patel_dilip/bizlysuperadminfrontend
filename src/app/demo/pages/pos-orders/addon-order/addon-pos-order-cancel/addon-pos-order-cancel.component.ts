import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addon-pos-order-cancel',
  templateUrl: './addon-pos-order-cancel.component.html',
  styleUrls: ['./addon-pos-order-cancel.component.scss']
})
export class AddonPosOrderCancelComponent implements OnInit {

  constructor(private route:Router) { }
  addonposorderView(): void {
    this.route.navigateByUrl("/pos-orders/addon-pos-order-view");
  }

  ngOnInit() {
  }

}
