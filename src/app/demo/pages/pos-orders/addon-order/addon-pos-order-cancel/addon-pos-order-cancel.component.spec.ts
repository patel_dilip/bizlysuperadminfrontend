import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddonPosOrderCancelComponent } from './addon-pos-order-cancel.component';

describe('AddonPosOrderCancelComponent', () => {
  let component: AddonPosOrderCancelComponent;
  let fixture: ComponentFixture<AddonPosOrderCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddonPosOrderCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddonPosOrderCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
