import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddonPosOrderViewComponent } from './addon-pos-order-view.component';

describe('AddonPosOrderViewComponent', () => {
  let component: AddonPosOrderViewComponent;
  let fixture: ComponentFixture<AddonPosOrderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddonPosOrderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddonPosOrderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
