import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PosOrderService } from 'src/app/_services/pos_order/pos-order.service';

@Component({
  selector: 'app-addon-pos-order-view',
  templateUrl: './addon-pos-order-view.component.html',
  styleUrls: ['./addon-pos-order-view.component.scss']
})
export class AddonPosOrderViewComponent implements OnInit {
  posAddon:any
  constructor(private route: Router, private posOrderser: PosOrderService) {
    console.log("view object=>", this.posOrderser.viewObject);
    if(posOrderser.viewObject==undefined)
{
  this.route.navigate(['/pos-orders'])
}  else{
  this.posAddon=posOrderser.viewObject
}  
}
  ngOnInit() {
  }
  addonposorderEdit(): void {
    this.route.navigateByUrl("/pos-orders/addon-pos-order-edit");
  }
  addonposorderCancel(): void {
    this.route.navigateByUrl("/pos-orders/addon-pos-order-cancel");
  }

}
