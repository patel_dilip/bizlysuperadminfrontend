import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationPosOrderViewComponent } from './integration-pos-order-view.component';

describe('IntegrationPosOrderViewComponent', () => {
  let component: IntegrationPosOrderViewComponent;
  let fixture: ComponentFixture<IntegrationPosOrderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntegrationPosOrderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationPosOrderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
