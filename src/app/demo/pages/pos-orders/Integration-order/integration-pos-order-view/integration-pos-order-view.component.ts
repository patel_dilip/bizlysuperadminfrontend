import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PosOrderService } from 'src/app/_services/pos_order/pos-order.service';

@Component({
  selector: 'app-integration-pos-order-view',
  templateUrl: './integration-pos-order-view.component.html',
  styleUrls: ['./integration-pos-order-view.component.scss']
})
export class IntegrationPosOrderViewComponent implements OnInit {
  posIntegration:any
  constructor(private route: Router, private posOrderser: PosOrderService) {
    console.log("view object=>", this.posOrderser.viewObject);
    if(posOrderser.viewObject==undefined)
{
  this.route.navigate(['/pos-orders'])
}    else{
  this.posIntegration=this.posOrderser.viewObject
}
  }

  ngOnInit() {
  }
  integrationposorderEdit(): void {
    this.route.navigateByUrl("/pos-orders/integration-pos-order-edit");
  }
  integrationposorderCancel(): void {
    this.route.navigateByUrl("/pos-orders/integration-pos-order-cancel");
  }

}
