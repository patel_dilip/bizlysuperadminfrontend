import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationPosOrderCancelComponent } from './integration-pos-order-cancel.component';

describe('IntegrationPosOrderCancelComponent', () => {
  let component: IntegrationPosOrderCancelComponent;
  let fixture: ComponentFixture<IntegrationPosOrderCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntegrationPosOrderCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationPosOrderCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
