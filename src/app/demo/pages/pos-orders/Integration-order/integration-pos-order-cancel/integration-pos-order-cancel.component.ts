import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-integration-pos-order-cancel',
  templateUrl: './integration-pos-order-cancel.component.html',
  styleUrls: ['./integration-pos-order-cancel.component.scss']
})
export class IntegrationPosOrderCancelComponent implements OnInit {

  constructor(private route:Router) { }
  integrationposorderView(): void {
    this.route.navigateByUrl("/pos-orders/integration-pos-order-view");
  }

  ngOnInit() {
  }

}
