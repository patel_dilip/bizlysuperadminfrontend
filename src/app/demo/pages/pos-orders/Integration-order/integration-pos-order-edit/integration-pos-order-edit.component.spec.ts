import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationPosOrderEditComponent } from './integration-pos-order-edit.component';

describe('IntegrationPosOrderEditComponent', () => {
  let component: IntegrationPosOrderEditComponent;
  let fixture: ComponentFixture<IntegrationPosOrderEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntegrationPosOrderEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationPosOrderEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
