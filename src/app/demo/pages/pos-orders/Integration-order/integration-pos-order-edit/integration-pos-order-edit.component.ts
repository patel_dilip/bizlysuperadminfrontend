import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-integration-pos-order-edit',
  templateUrl: './integration-pos-order-edit.component.html',
  styleUrls: ['./integration-pos-order-edit.component.scss']
})
export class IntegrationPosOrderEditComponent implements OnInit {

  constructor(private route:Router) { }
  integrationposorderView(): void {
    this.route.navigateByUrl("/pos-orders/integration-pos-order-view");
  }

  ngOnInit() {
  }

}
