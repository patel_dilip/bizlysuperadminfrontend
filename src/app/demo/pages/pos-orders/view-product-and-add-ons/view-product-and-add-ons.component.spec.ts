import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProductAndAddOnsComponent } from './view-product-and-add-ons.component';

describe('ViewProductAndAddOnsComponent', () => {
  let component: ViewProductAndAddOnsComponent;
  let fixture: ComponentFixture<ViewProductAndAddOnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewProductAndAddOnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewProductAndAddOnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
