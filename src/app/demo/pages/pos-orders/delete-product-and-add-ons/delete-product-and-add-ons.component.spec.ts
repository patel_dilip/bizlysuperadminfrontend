import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteProductAndAddOnsComponent } from './delete-product-and-add-ons.component';

describe('DeleteProductAndAddOnsComponent', () => {
  let component: DeleteProductAndAddOnsComponent;
  let fixture: ComponentFixture<DeleteProductAndAddOnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteProductAndAddOnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteProductAndAddOnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
