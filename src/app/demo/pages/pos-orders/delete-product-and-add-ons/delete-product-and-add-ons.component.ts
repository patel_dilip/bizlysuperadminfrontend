import { Component, OnInit } from '@angular/core';
  import { MatDialogRef } from '@angular/material';
  import { FormBuilder, FormGroup } from '@angular/forms';
  
@Component({
  selector: 'app-delete-product-and-add-ons',
  templateUrl: './delete-product-and-add-ons.component.html',
  styleUrls: ['./delete-product-and-add-ons.component.scss']
})
export class DeleteProductAndAddOnsComponent implements OnInit {
  
  reasons:string[]=['a','b','c'];
  cancelOrderForm: FormGroup
    constructor(public dialogRef: MatDialogRef<DeleteProductAndAddOnsComponent>,
      private fb:FormBuilder) { }
  
    ngOnInit() {
      this.cancelOrderForm = this.fb.group({
        selectReason:[''],
        orderNumber:[''],
        customerName:[''],
        number:[''],
        restaurantChainName:[''],
        orderDate:[''],
        orderTime:[''],
        orderAmount:[''],
        orderContains:[''],
        newNumberOfOutlet:[''],
        refundableAmount:['']
      })
    }
    onNoClick(): void {
      this.dialogRef.close();
    }
    confirmRefund(){
  console.log(this.cancelOrderForm.value);
  
    }
  }
  