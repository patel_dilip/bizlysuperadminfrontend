import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentLinkHistoryComponent } from './payment-link-history.component';

describe('PaymentLinkHistoryComponent', () => {
  let component: PaymentLinkHistoryComponent;
  let fixture: ComponentFixture<PaymentLinkHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentLinkHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentLinkHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
