import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { PosOrderService } from 'src/app/_services/pos_order/pos-order.service';





@Component({
  selector: 'app-payment-link-history',
  templateUrl: './payment-link-history.component.html',
  styleUrls: ['./payment-link-history.component.scss']
})
export class PaymentLinkHistoryComponent implements OnInit {

  // paymentLinkHistoryDATA: paymentLinkHistoryElement
  cols: any = [];
  outletTable: any = [];
  _selectedColumns: any[];
  selected1: any;
  rowData=[];
  constructor(private route:Router, private posProductservice:PosOrderService) {
  this.getallPaymentLinkHistory()
  
   }

  ngOnInit() {
   
  }
  producttableData() {

    let posProductOrderData = []

    this.rowData.forEach(element => {
      posProductOrderData.push(
        {
          id: element._id,
          paymentLinkID: "PAY000"+element.paymentLinkID,
          createdAt:element.createdAt,
          organization_name: element.organization_name,
          address:element.address+', ' + element.city,
          representative_name:element.representative_name,
          representative_contact:element.representative_contact,
          noOfoutlets:element.noOfoutlets,
          totalPaidAmountwithTax:element.totalPaidAmountwithTax
          
        }
      )
    });

    this.outletTable = posProductOrderData

    this.cols=[
      {field: "paymentLinkID", header: "Payment Link ID"},
      {field:"createdAt",header:"Sent On"},
      {field: "organization_name", header: "Organization Name"},
      {field:"address", header:"Organization Address"},
      {field: "representative_name", header: "Representative Name"},
      {field: "representative_contact", header:"Representative Contact"},
      {field:"noOfoutlets", header:"Outlets"},
      {field: "totalPaidAmountwithTax", header:"Payable Amount"},

    ]

    this._selectedColumns = this.cols
    console.log(posProductOrderData);
    
  }
  getallPaymentLinkHistory(){
    this.posProductservice.getallPaymentLinkHistory().subscribe(data=>{
      console.log("payment link history:", data);
      this.rowData = data['data']

      if(this.rowData!==undefined){
        this.producttableData()
      }
      
    })
  }
  ViewPaymentLinkHistory(id){
    this.posProductservice.getPaymentHistoryById(id).subscribe(data=>{
      this.posProductservice.viewObject=data['data']
    this.route.navigateByUrl("/pos-orders/view-payment-link-history");
  })
  }

  onChk() {
    console.log("selected1", this.selected1);
  }

  // show hide columns
  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col) => val.includes(col));

    console.log("Set value of selected columns : ", this._selectedColumns);
  }
  


}
