import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosOrderCancelComponent } from './pos-order-cancel.component';

describe('PosOrderCancelComponent', () => {
  let component: PosOrderCancelComponent;
  let fixture: ComponentFixture<PosOrderCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosOrderCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosOrderCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
