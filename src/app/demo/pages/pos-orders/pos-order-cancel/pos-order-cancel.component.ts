import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { PosOrderService } from 'src/app/_services/pos_order/pos-order.service';

@Component({
  selector: 'app-pos-order-cancel',
  templateUrl: './pos-order-cancel.component.html',
  styleUrls: ['./pos-order-cancel.component.scss']
})
export class PosOrderCancelComponent implements OnInit {
  posProduct:any
  totalOutlet=[]
  Reasons=['Don’t want to continue', 'Others']
  constructor(private route:Router, private posOrderser:PosOrderService) { 
    if(posOrderser.viewObject==undefined)
    {
      this.route.navigate(['/pos-orders'])
    }    else{
     
    this.posProduct=this.posOrderser.viewObject;
    for(let i=1; i<=this.posProduct.posProductOrder.noOfoutlets; i++){
    console.log(this.posProduct.posProductOrder.noOfoutlets.length);
    
    if(this.posProduct.posProductOrder.noOfoutlets==i){
      this.totalOutlet.push('All')
    }else{    
    this.totalOutlet.push(i)
    }
  }
    console.log(this.totalOutlet);
    
}
  }

  ngOnInit() {
  }
  posorderView(): void {
    this.route.navigateByUrl("/pos-orders/pos-order-view");
  }


}
