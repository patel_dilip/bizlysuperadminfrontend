import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendpaymentlinkComponent } from './sendpaymentlink.component';

describe('SendpaymentlinkComponent', () => {
  let component: SendpaymentlinkComponent;
  let fixture: ComponentFixture<SendpaymentlinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendpaymentlinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendpaymentlinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
