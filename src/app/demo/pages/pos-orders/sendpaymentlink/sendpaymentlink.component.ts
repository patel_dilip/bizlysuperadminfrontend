import { Component, OnInit } from '@angular/core';
import { ChargeService } from 'src/app/_services/charges/charge.service';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { PosOrderService } from 'src/app/_services/pos_order/pos-order.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-sendpaymentlink',
  templateUrl: './sendpaymentlink.component.html',
  styleUrls: ['./sendpaymentlink.component.scss']
})
export class SendpaymentlinkComponent implements OnInit {
  productlist: any;
  noOfoutlets = 1
  country_json = [];
  state = [];
  state_list = [];
  city = [];
  productData: any;
  selectProduct: boolean;
  sendPaymentLink: FormGroup;
  perProductPrice: any;
  tax = 18
  totalOrderAmount: number;
  DiscountedPrice: number;
  discount = { percentage: 10 };
  PriceafterDiscount: number;
  totalPaidAmountwithTax: any;
  taxAmount: number;
  mediaList = ['email', 'sms']
  media: any;
  email = []
  sms = []

  constructor(private route: Router,private chargesService: ChargeService, private posOrder: PosOrderService, private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService) {
    this.country_state_city()
    this.getallproduct()
  }
  getallproduct() {
    this.chargesService.getProduct().subscribe(data => {
      console.log(data);
      this.productlist = data['data']

    })
  }
  country_state_city() {
    this.addMenuFoodsService.country_state_city().subscribe(data => {
      // console.log("============", data);

      console.log(data.country_json);
      this.country_json = data.country_json


      for (var i = 0; i < data.country_json.length - 1; i++)
        //console.log("=========================");

        console.log(data.country_json[i].name);


    })
  }

  changeCountry(product) {
    console.log(product);

    this.getProductDetiils(product)

    // console.log(this.state['Maharashtra']);


    // console.log(this.villege);

  }

  changeState(count) {
    console.log(count);

    this.city = this.state_list[count]
    console.log(this.city);

  }
  ngOnInit() {
    this.sendPaymentLink = this.fb.group({
      organization_name: ['', Validators.required],
      representative_email: ['', Validators.required],
      representative_contact: ['', Validators.required],
      org_email: ['', Validators.required],
      org_contact: ['', Validators.required],
      noOfoutlets: ['', Validators.required],
      address: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      representative_name: ['', Validators.required],
      productName: ['', Validators.required],
      tax: ['', Validators.required],
      discount: ['', Validators.required],
      DiscountedPrice: ['', Validators.required],
      PriceafterDiscount: ['', Validators.required],
      totalOrderAmount: ['', Validators.required],
      totalPaidAmountwithTax: ['', Validators.required],
      PaymentLinkSend: ['', Validators.required],
      paymentLinkSendTo: ['', Validators.required],
      subscription_Type: ['', Validators.required],
      pricePerOutlet: ['', Validators.required]

    })
  }
  getProductDetiils(id) {
    this.chargesService.getSingleProduct(id).subscribe(data => {

      console.log("product data=>", data['data']);
      this.productData = data['data']
      this.productData.completeData[0].tableData.forEach((element, index) => {
        if (element.from <= this.noOfoutlets && element.to >= this.noOfoutlets) {
          console.log("pp", element.price);
          this.perProductPrice = element.price.replace(/,/g, "");
          console.log(element.price.replace(/,/g, ""));
          this.perProductPrice = this.perProductPrice
          this.totalOrderAmount = this.perProductPrice * this.noOfoutlets
          this.DiscountedPrice = this.totalOrderAmount * (this.discount.percentage / 100);
          this.PriceafterDiscount = this.totalOrderAmount - this.DiscountedPrice
          this.totalPaidAmountwithTax = this.PriceafterDiscount + (this.PriceafterDiscount * (this.tax / 100))
          this.taxAmount = this.PriceafterDiscount * (this.tax / 100)

        }
      });

      this.selectProduct = true
      this.state = this.country_json.find(state => state.name == this.productData.completeData[0].country['item_text']).states

      this.state_list = this.state
      this.state = Object.keys(this.state)
      console.log(this.state_list);
      ///calculation


    })
  }
  plus(outlets) {
    this.noOfoutlets = outlets + 1
    this.noOfoutlets = this.noOfoutlets
    this.productData.completeData[0].tableData.forEach((element, index) => {
      if (element.from <= this.noOfoutlets && element.to >= this.noOfoutlets) {
        console.log(element.price)
        this.perProductPrice = element.price.replace(/,/g, "");
        console.log(element.price.replace(/,/g, ""));

        this.totalOrderAmount = this.perProductPrice * this.noOfoutlets
        this.DiscountedPrice = this.totalOrderAmount * (this.discount.percentage / 100);
        this.PriceafterDiscount = this.totalOrderAmount - this.DiscountedPrice
        this.totalPaidAmountwithTax = this.PriceafterDiscount + (this.PriceafterDiscount * (this.tax / 100))
        this.taxAmount = this.PriceafterDiscount * (this.tax / 100)

      }
    });

  }
  minus(outlets) {
    this.noOfoutlets = outlets - 1
    this.noOfoutlets = this.noOfoutlets
    this.productData.completeData[0].tableData.forEach((element, index) => {
      if (element.from <= this.noOfoutlets && element.to >= this.noOfoutlets) {
        console.log("pp", element.price);
        this.perProductPrice = element.price.replace(/,/g, "");
        console.log(element.price.replace(/,/g, ""));

        this.totalOrderAmount = this.perProductPrice * this.noOfoutlets
        this.DiscountedPrice = this.totalOrderAmount * (this.discount.percentage / 100);
        this.PriceafterDiscount = this.totalOrderAmount - this.DiscountedPrice
        this.totalPaidAmountwithTax = this.PriceafterDiscount + (this.PriceafterDiscount * (this.tax / 100))
        this.taxAmount = this.PriceafterDiscount * (this.tax / 100)

      }
    });

  }
  sendLink() {
    this.sendPaymentLink.patchValue({
      productName: this.productData._id,
      tax: this.tax,
      discount: this.discount,
      DiscountedPrice: this.DiscountedPrice,
      PriceafterDiscount: this.PriceafterDiscount,
      totalOrderAmount: this.totalOrderAmount,
      totalPaidAmountwithTax: this.totalPaidAmountwithTax,
      pricePerOutlet: this.perProductPrice,
      subscription_Type: this.productData.completeData[0].colletionMode,
      noOfoutlets:this.noOfoutlets,
      PaymentLinkSend: {
        whatsapp: "whatsapp_link",
        email: "email_link",
        sms: "sms_link"
      },
      // PaymentLinkSend: paymen
      // paymentLinkSendTo: ""
    })

    console.log(this.sendPaymentLink.value);
    this.posOrder.addPaymentLink(this.sendPaymentLink.value).subscribe(data=>{
      if(data['sucess']==true){
        this.route.navigate(['/pos-orders'])
        Swal.fire("Done", "Send Payment Link Successfully", "success")
      }
      console.log(data);
      
    })

  }
  change(value) {
    console.log(value);

    this.media = value
  }
  send(value, data) {
    console.log(value, data)
    if (value.checked == true && data == 'Representative Email') {
      this.email.push(this.sendPaymentLink.controls.representative_email.value)
      this.sendPaymentLink.patchValue({
        paymentLinkSendTo: {
          "whatsapp": [
          ],
          "email": [
            this.email
          ],
          "sms": [
            this.sms
          ]
        }
      })

    }
    else if (value.checked == true && data == 'Orgnization Email') {
      this.email.push(this.sendPaymentLink.controls.org_email.value)
      this.sendPaymentLink.patchValue({
        paymentLinkSendTo: {
          "whatsapp": [
          ],
          "email": [
            this.email
          ],
          "sms": [
            this.sms
          ]
        }
      })
    }
    else if (value.checked == true && data == 'Orgnization Contact No') {
      this.sms.push(this.sendPaymentLink.controls.org_contact.value)
      this.sendPaymentLink.patchValue({
        paymentLinkSendTo: {
          "whatsapp": [
          ],
          "email": [
            this.email
          ],
          "sms": [
            this.sms
          ]
        }
      })

    } else if (value.checked == true && data == 'Representative Contact No') {
      this.sms.push(this.sendPaymentLink.controls.representative_contact.value)
      this.sendPaymentLink.patchValue({
        paymentLinkSendTo: {
          "whatsapp": [
          ],
          "email": [
            this.email
          ],
          "sms": [
            this.sms
          ]
        }
      })
    }
  }
}
