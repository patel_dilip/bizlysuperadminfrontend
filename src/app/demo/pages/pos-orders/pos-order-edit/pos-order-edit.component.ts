import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pos-order-edit',
  templateUrl: './pos-order-edit.component.html',
  styleUrls: ['./pos-order-edit.component.scss']
})
export class PosOrderEditComponent implements OnInit {

  constructor( private route:Router) { }
  posorderView(): void {
    this.route.navigateByUrl("/pos-orders/pos-order-view");
  }

  ngOnInit() {
  }

}
