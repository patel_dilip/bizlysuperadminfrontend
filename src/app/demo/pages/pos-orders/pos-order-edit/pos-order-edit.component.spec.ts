import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosOrderEditComponent } from './pos-order-edit.component';

describe('PosOrderEditComponent', () => {
  let component: PosOrderEditComponent;
  let fixture: ComponentFixture<PosOrderEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosOrderEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosOrderEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
