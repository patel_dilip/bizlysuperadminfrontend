import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-send-payment-link',
  templateUrl: './send-payment-link.component.html',
  styleUrls: ['./send-payment-link.component.scss']
})
export class SendPaymentLinkComponent implements OnInit {
  discountType:string[]=['percentage','cashback'];
  //paymentLinks:string[]=["phonepe","text", "whatsapp"]
  sendPaymentLinkForm: FormGroup
  paymentLinks = ["phonepe","text", "whatsapp"]
//   [
//     {id: 1, name: 'phonepe'},
//     {id: 2, name: 'text'},
//     {id: 3, name: 'whatsapp'}
// ];

  constructor(public dialogRef: MatDialogRef<SendPaymentLinkComponent>,
    private fb:FormBuilder) { }

  ngOnInit() {
    this.sendPaymentLinkForm = this.fb.group({
      representativeName:[''],
      representativeNumber:[''],
      representativeEmail:[''],
      restaurantName:[''],
      address:[''],
      npOfOutlets:[''],
      country:[''],
      city:[''],
      amountToBePaid:[''],
      state:[''],
      discountType:[''],
      totalAmountForPayment:[''],
      sendPaymentLink:['']
    })
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  sendPaymentLink(){
console.log(this.sendPaymentLinkForm.value);

  }
}
