import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationsPosOrdersComponent } from './integrations-pos-orders.component';

describe('IntegrationsPosOrdersComponent', () => {
  let component: IntegrationsPosOrdersComponent;
  let fixture: ComponentFixture<IntegrationsPosOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntegrationsPosOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationsPosOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
