import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-integrations-pos-orders',
  templateUrl: './integrations-pos-orders.component.html',
  styleUrls: ['./integrations-pos-orders.component.scss']
})
export class IntegrationsPosOrdersComponent implements OnInit {
  
  reasons:string[]=['a','b','c'];
  cancelOrderForm: FormGroup
    constructor(public dialogRef: MatDialogRef<IntegrationsPosOrdersComponent>,
      private fb:FormBuilder) { }
  
    ngOnInit() {
      this.cancelOrderForm = this.fb.group({
        restaurantOutletName:[''],
        selectReason:[''],
        orderNumber:[''],
        customerName:[''],
        number:[''],
        orderDate:[''],
        orderTime:[''],
        orderAmount:[''],
        orderContains:[''],
        outletName:[''],
        refundableAmount:['']
      })
    }
    onNoClick(): void {
      this.dialogRef.close();
    }
    confirmRefund(){
  console.log(this.cancelOrderForm.value);
  
    }
  }
  