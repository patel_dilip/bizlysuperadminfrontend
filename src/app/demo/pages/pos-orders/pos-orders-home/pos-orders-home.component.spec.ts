import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosOrdersHomeComponent } from './pos-orders-home.component';

describe('PosOrdersHomeComponent', () => {
  let component: PosOrdersHomeComponent;
  let fixture: ComponentFixture<PosOrdersHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosOrdersHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosOrdersHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
