import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PosOrderService } from 'src/app/_services/pos_order/pos-order.service';

@Component({
  selector: "app-pos-order-view",
  templateUrl: "./pos-order-view.component.html",
  styleUrls: ["./pos-order-view.component.scss"],
})
export class PosOrderViewComponent implements OnInit {
  posProduct: any;
  constructor(private route: Router, private posOrderser: PosOrderService) {
    console.log("view object=>", this.posOrderser.viewObject);
    if(posOrderser.viewObject==undefined)
{
  this.route.navigate(['/pos-orders'])
}    else{
  this.posProduct=this.posOrderser.viewObject
}
  }

  ngOnInit() {}
  posorderEdit(): void {
    this.route.navigateByUrl("/pos-orders/pos-order-edit");
  }
  posorderCancel(): void {
    this.route.navigateByUrl("/pos-orders/pos-order-cancel");
  }
}
