import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosOrderViewComponent } from './pos-order-view.component';

describe('PosOrderViewComponent', () => {
  let component: PosOrderViewComponent;
  let fixture: ComponentFixture<PosOrderViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosOrderViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosOrderViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
