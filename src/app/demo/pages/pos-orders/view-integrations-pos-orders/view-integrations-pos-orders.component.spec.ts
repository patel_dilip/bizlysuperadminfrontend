import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewIntegrationsPosOrdersComponent } from './view-integrations-pos-orders.component';

describe('ViewIntegrationsPosOrdersComponent', () => {
  let component: ViewIntegrationsPosOrdersComponent;
  let fixture: ComponentFixture<ViewIntegrationsPosOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewIntegrationsPosOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewIntegrationsPosOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
