import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-view-integrations-pos-orders',
  templateUrl: './view-integrations-pos-orders.component.html',
  styleUrls: ['./view-integrations-pos-orders.component.scss']
})
export class ViewIntegrationsPosOrdersComponent implements OnInit {

object={"orderNumber":"765432","chain":"abc","outletName":"abc" , "licenseNumber":"7654gf", "NumberOFOutlet":"5", "contactNumber":"987654321", "PaymentStatus":"paid", "OrderAmmount":"20000", "discount":"500", "order":"a,b,c","address":"sdasdasda,acsasca,ascxacs,ascx ","city":"Pune","email":"abc@gmail.com"}
  constructor(public dialogRef: MatDialogRef<ViewIntegrationsPosOrdersComponent>) { }

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}
