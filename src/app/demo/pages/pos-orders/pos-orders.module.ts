import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PosOrdersHomeComponent } from "./pos-orders-home/pos-orders-home.component";
import { PosOrdersListComponent } from "./pos-orders-list/pos-orders-list.component";
import { Routes, RouterModule } from "@angular/router";
import { MaterialModule } from "src/app/material/material.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { SharedModule } from "src/app/theme/shared/shared.module";
import { DataTablesModule } from "angular-datatables";
import { PaymentLinkHistoryComponent } from "./payment-link-history/payment-link-history.component";
import { SelectModule } from "ng-select";
import { TagInputModule } from "ngx-chips";

/*PM_15/07/2020 (Vikram UID)*/
import { PosOrderViewComponent } from "./pos-order-view/pos-order-view.component";
import { PosOrderEditComponent } from "./pos-order-edit/pos-order-edit.component";
import { PosOrderCancelComponent } from "./pos-order-cancel/pos-order-cancel.component";
import { IntegrationPosOrderViewComponent } from './Integration-order/integration-pos-order-view/integration-pos-order-view.component';
import { IntegrationPosOrderEditComponent } from './Integration-order/integration-pos-order-edit/integration-pos-order-edit.component';
import { IntegrationPosOrderCancelComponent } from './Integration-order/integration-pos-order-cancel/integration-pos-order-cancel.component';
import { AddonPosOrderViewComponent } from './addon-order/addon-pos-order-view/addon-pos-order-view.component';
import { AddonPosOrderEditComponent } from './addon-order/addon-pos-order-edit/addon-pos-order-edit.component';
import { AddonPosOrderCancelComponent } from './addon-order/addon-pos-order-cancel/addon-pos-order-cancel.component';
import { ViewPaymentLinkHistoryComponent } from './view-payment-link-history/view-payment-link-history.component';
import { SendpaymentlinkComponent } from './sendpaymentlink/sendpaymentlink.component';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';

const routes: Routes = [
  {
    path: "",
    component: PosOrdersHomeComponent,
    children: [
      {
        path: "",
        component: PosOrdersListComponent,
      },
      {
        path:"send-payment-link",
        component:SendpaymentlinkComponent
      },
      {
        path: "payment-link-history",
        component: PaymentLinkHistoryComponent,
      },
      {
        path: "view-payment-link-history",
        component: ViewPaymentLinkHistoryComponent,
      },
      {
        path: "pos-order-view",
        component: PosOrderViewComponent,
      },
      {
        path: "pos-order-edit",
        component: PosOrderEditComponent,
      },
      {
        path: "pos-order-cancel",
        component: PosOrderCancelComponent,
      },
      {
        path: "addon-pos-order-view",
        component: AddonPosOrderViewComponent,
      },
      {
        path: "addon-pos-order-edit",
        component: AddonPosOrderEditComponent,
      },
      {
        path: "addon-pos-order-cancel",
        component: AddonPosOrderCancelComponent,
      },
      {
        path: "integration-pos-order-view",
        component: IntegrationPosOrderViewComponent,
      },
      {
        path: "integration-pos-order-edit",
        component: IntegrationPosOrderEditComponent,
      },
      {
        path: "integration-pos-order-cancel",
        component: IntegrationPosOrderCancelComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [
    PosOrdersHomeComponent,
    PosOrdersListComponent,
    PaymentLinkHistoryComponent,

    /*PM_15/07/2020 (Vikram UID)*/
    PosOrderViewComponent,
    PosOrderEditComponent,
    PosOrderCancelComponent,
    IntegrationPosOrderViewComponent,
    IntegrationPosOrderEditComponent,
    IntegrationPosOrderCancelComponent,
    AddonPosOrderViewComponent,
    AddonPosOrderEditComponent,
    AddonPosOrderCancelComponent,
    ViewPaymentLinkHistoryComponent,
    SendpaymentlinkComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    TagInputModule,
    SelectModule,
    TableModule,
    MultiSelectModule
  ],
})
export class PosOrdersModule {}
