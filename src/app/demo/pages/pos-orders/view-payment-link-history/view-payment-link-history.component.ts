import { Component, OnInit } from '@angular/core';
import { PosOrderService } from 'src/app/_services/pos_order/pos-order.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-payment-link-history',
  templateUrl: './view-payment-link-history.component.html',
  styleUrls: ['./view-payment-link-history.component.scss']
})
export class ViewPaymentLinkHistoryComponent implements OnInit {
  paymentLinkHistory: any;

  constructor(private posOrderser:PosOrderService, private route:Router) { 
    console.log("view object=>", this.posOrderser.viewObject);
    if(posOrderser.viewObject==undefined)
{
  this.route.navigate(['/pos-orders'])
}    else{
  this.paymentLinkHistory=this.posOrderser.viewObject
}
  }

  ngOnInit() {
  }

}
