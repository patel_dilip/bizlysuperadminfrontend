import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPaymentLinkHistoryComponent } from './view-payment-link-history.component';

describe('ViewPaymentLinkHistoryComponent', () => {
  let component: ViewPaymentLinkHistoryComponent;
  let fixture: ComponentFixture<ViewPaymentLinkHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPaymentLinkHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPaymentLinkHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
