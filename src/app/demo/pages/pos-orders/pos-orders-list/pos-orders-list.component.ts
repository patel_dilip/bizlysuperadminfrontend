import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { from } from "rxjs";

import { ViewProductAndAddOnsComponent } from "../view-product-and-add-ons/view-product-and-add-ons.component";


/*PM_15/07/2020 (Vikram UID)*/
import { PosOrderViewComponent } from "../pos-order-view/pos-order-view.component";
import { PosOrderService } from 'src/app/_services/pos_order/pos-order.service';


@Component({
  selector: "app-pos-orders-list",
  templateUrl: "./pos-orders-list.component.html",
  styleUrls: ["./pos-orders-list.component.scss"],
})
export class PosOrdersListComponent implements OnInit {
  ///////////// ng prime ///////////////////
  
  rowData: any = [
   ];
   rowData_addon: any = [
  ];
  rowData_integration: any = [
  ];

     // ngPrime Table
     //for product
cols: any = [];
outletTable: any = [];
_selectedColumns: any[];
selected1: any;
//for addon
cols_addon: any = [];
outletTable_addon: any = [];
_selectedColumns_addon: any[];
selected1_addon: any;
//for addon
cols_integration: any = [];
outletTable_integration: any = [];
_selectedColumns_integration: any[];
selected1_integration: any;

  totalPOSProductOrders: any=0
  totalPOSProductOutlets: any=0
  totalPOSProductOrderRevenueAmt: any=0
  totalPOSProductOrderDiscount: any=0
  totalAddonOutlets: any=0
  totalPOSAddonOrderRevenueAmt: any=0
  totalPOSAddonOrderDiscount: any=0
  totalPOSAddonOrders: any=0
  totalIntegrationOutets: any=0
  totalIntegrationOrderRevenueAmt: any=0
  totalPOSIntegrationOrderDiscount: any=0
  totalPOSIntegrationOrders: any=0

/////////////////////////////////////////////////////////////////////
 
 
  constructor(
    private route:Router,
    private router: Router,
    private posOrderServ: PosOrderService
  ) {
    this.getallProductOrder()
    this.getallAddonOrder()
    this.getallIntegrationOrder()
    this.getcount()
   
    
  }
  getcount(){
  
    this.posOrderServ.getallcount().subscribe(data=>{
      console.log(data);
     
      if(data['sucess']==true){
        this.totalPOSProductOrders=data['data']['posProductData']['totalPOSProductOrders']
        this.totalPOSProductOutlets=data['data']['posProductData']['totalPOSProductOutlets']
        this.totalPOSProductOrderRevenueAmt=data['data']['posProductData']['totalPOSProductOrderRevenueAmt']
        this.totalPOSProductOrderDiscount=data['data']['posProductData']['totalPOSProductOrderDiscount']
        this.totalAddonOutlets=data['data']['posAddonData']['totalAddonOutlets']
        this.totalPOSAddonOrderRevenueAmt=data['data']['posAddonData']['totalPOSAddonOrderRevenueAmt']
        this.totalPOSAddonOrderDiscount=data['data']['posAddonData']['totalPOSAddonOrderDiscount']
        this.totalPOSAddonOrders=data['data']['posAddonData']['totalPOSAddonOrders']
        this.totalIntegrationOutets=data['data']['posIntegrationData']['totalIntegrationOutets']
        this.totalIntegrationOrderRevenueAmt=data['data']['posIntegrationData']['totalIntegrationOrderRevenueAmt']
        this.totalPOSIntegrationOrderDiscount=data['data']['posIntegrationData']['totalPOSIntegrationOrderDiscount']
        this.totalPOSIntegrationOrders=data['data']['posIntegrationData']['totalPOSIntegrationOrders']
      }
    })
  }
  getallProductOrder(){
    this.posOrderServ.getProductOrder().subscribe(data=>{
      console.log("pos order:",data['data']);
      
      this.rowData = data['data']

      if(this.rowData!==undefined){
        this.producttableData()
      }

     
    })
  }
  getallAddonOrder(){
    this.posOrderServ.getAddonOrder().subscribe(data=>{
      console.log("pos order:",data['data']);
      data['data'].forEach(element => {
        let addon=[]
  
          element.posAddonOrder.addonName.forEach(ele => {
            addon.push(ele.AddonsName)
            element.addonName=addon
  
          });
        });  
            this.rowData_addon = data['data']

      if(this.rowData_addon!==undefined){
        this.addontableData()
      }

     
    })
  }
  getallIntegrationOrder(){
    this.posOrderServ.getIntegrationOrder().subscribe(data=>{
      console.log("pos Integration order:",data['data']);

      data['data'].forEach(element => {
      let integration=[]

        element.posIntegrationOrder.integrationName.forEach(ele => {
          integration.push(ele.integrationName)
          element.integrationName=integration

        });
      });
      this.rowData_integration = data['data']

      if(this.rowData_addon!==undefined){
        this.integrationtableData()
      }

     
    })
  }
  
 producttableData() {

    let posProductOrderData = []

    this.rowData.forEach(element => {
      posProductOrderData.push(
        {
          id: element._id,
          OrderID: "OR000"+element.OrderID,
          Organization_ID: "RES000"+ element.Organization_ID,
          createdAt:element.posProductOrder.createdAt,
          organization_name: element.organization_name,
          noOfoutlets:element.posProductOrder.noOfoutlets,
          subscription_Type:element.posProductOrder.subscription_Type,
          totalOrderAmount:element.posProductOrder.totalOrderAmount,
          totalPaidAmountwithTax:element.posProductOrder.totalPaidAmountwithTax,
          paymentStatus:element.posProductOrder.paymentStatus
        }
      )
    });

    this.outletTable = posProductOrderData

    this.cols=[
      {field: "OrderID", header: "Order ID"},
      {field: "Organization_ID", header: "Organization ID"},
      {field:"createdAt",header:"Order Date & Time"},
      {field: "organization_name", header: "Organization Name"},
      {field:"noOfoutlets", header:"Outlets"},
      {field: "subscription_Type", header: "Subscription Type"},
      {field: "totalOrderAmount", header:"Order Amount"},
      {field: "totalPaidAmountwithTax", header:"Total Paid"},
      {field:"paymentStatus", header:"Payment Status"},

    ]

    this._selectedColumns = this.cols
    console.log(posProductOrderData);
    
  }
  addontableData(){
    let posAddonOrderData = []

    this.rowData_addon.forEach(element => {
    if(element.posAddonOrder.status==true){
      posAddonOrderData.push(
        {
          id: element._id,
          OrderID: "OR000"+element.OrderID,
          Organization_ID: "RES000"+ element.Organization_ID,
          createdAt:element.posAddonOrder.createdAt,
          organization_name: element.organization_name,
          addonBought:element.addonName,
          noOfoutlets:element.posAddonOrder.noOfoutlets,
          subscription_Type:element.posAddonOrder.subscription_Type,
          totalOrderAmount:element.posAddonOrder.totalOrderAmount,
          totalPaidAmountwithTax:element.posAddonOrder.totalPaidAmountwithTax,
          paymentStatus:element.posAddonOrder.paymentStatus
        }
      )
    }
    });

    this.outletTable_addon = posAddonOrderData

    this.cols_addon=[
      {field: "OrderID", header: "Order ID"},
      {field: "Organization_ID", header: "Organization ID"},
      {field:"createdAt",header:"Order Date & Time"},
      {field: "organization_name", header: "Organization Name"},
      {field:"addonBought", header:"Addon Bought"},
      {field:"noOfoutlets", header:"Outlets"},
      {field: "subscription_Type", header: "Subscription Type"},
      {field: "totalOrderAmount", header:"Order Amount"},
      {field: "totalPaidAmountwithTax", header:"Total Paid"},
      {field:"paymentStatus", header:"Payment Status"},

    ]

    this._selectedColumns_addon = this.cols_addon
    console.log(posAddonOrderData);

  }
  integrationtableData(){
    let posintegrationOrderData = []

    this.rowData_integration.forEach(element => {
    if(element.posIntegrationOrder.status==true){
      posintegrationOrderData.push(
        {
          id: element._id,
          OrderID: "OR000"+element.OrderID,
          Organization_ID: "RES000"+ element.Organization_ID,
          createdAt:element.posIntegrationOrder.createdAt,
          organization_name: element.organization_name,
          integrationBought:element.integrationName,
          noOfoutlets:element.posIntegrationOrder.noOfoutlets,
          subscription_Type:element.posIntegrationOrder.subscription_Type,
          totalOrderAmount:element.posIntegrationOrder.totalOrderAmount,
          totalPaidAmountwithTax:element.posIntegrationOrder.totalPaidAmountwithTax,
          paymentStatus:element.posIntegrationOrder.paymentStatus
        }
      )
    }
    });

    this.outletTable_integration = posintegrationOrderData

    this.cols_integration=[
      {field: "OrderID", header: "Order ID"},
      {field: "Organization_ID", header: "Organization ID"},
      {field:"createdAt",header:"Order Date & Time"},   
      {field: "organization_name", header: "Organization Name"},
      {field:"integrationBought", header:"Integration Bought"},
      {field:"noOfoutlets", header:"Outlets"},
      {field: "subscription_Type", header: "Subscription Type"},
      {field: "totalOrderAmount", header:"Order Amount"},
      {field: "totalPaidAmountwithTax", header:"Total Paid"},
      {field:"paymentStatus", header:"Payment Status"},

    ]

    this._selectedColumns_integration = this.cols_integration
    console.log(posintegrationOrderData);

  }
  ngOnInit() {
   
  }

  openSendPaymentLink(): void {
    this.route.navigateByUrl("/pos-orders/send-payment-link");
  }

  

 
  // viewproductandAddOns(): void {
  //   const dialogRef = this.dialog.open(ViewProductAndAddOnsComponent, {
  //     width: "800px",

  //     // data: {name: this.name, animal: this.animal}
  //   });

  //   dialogRef.afterClosed().subscribe((result) => {
  //     console.log("The dialog was closed");
  //     // this.animal = result;
  //   });
  // }

  /*PM_15/07/2020 (Vikram UID)*/
  posorderView(id): void {
   console.log(id);
   this.posOrderServ.getProductById(id).subscribe(data=>{
    this.posOrderServ.viewObject=data['data']
    this.router.navigateByUrl('/pos-orders/pos-order-view');

     console.log("product data=>", data);
     
   })
   
  }
  addonposorderView(id): void {
    this.posOrderServ.getAddonById(id).subscribe(data=>{
      this.posOrderServ.viewObject=data['data']
      this.router.navigateByUrl('/pos-orders/addon-pos-order-view');
  
       console.log("product data=>", data);
       
     })
   
    }
    integrationposorderView(id): void {
      this.posOrderServ.getIntegrationById(id).subscribe(data=>{
        this.posOrderServ.viewObject=data['data']
        this.router.navigateByUrl('/pos-orders/integration-pos-order-view');
    
         console.log("product data=>", data);
         
       })
     
      }


 
      // ON CHECKBOX CLICK
      onChk() {
        console.log("selected1", this.selected1);
      }
    
      // show hide columns
      @Input() get selectedColumns(): any[] {
        return this._selectedColumns;
      }
    
      set selectedColumns(val: any[]) {
        //restore original order
        this._selectedColumns = this.cols.filter((col) => val.includes(col));
    
        console.log("Set value of selected columns : ", this._selectedColumns);
      }

      /////////////////////////////// addon ///////////////////////////////////////////////////////////
      onChk_addon() {
        console.log("selected1", this.selected1_addon);
      }
    
      // show hide columns
      @Input() get selectedColumns_addon(): any[] {
        return this._selectedColumns_addon;
      }
    
      set selectedColumns_addon(val: any[]) {
        //restore original order
        this._selectedColumns_addon = this.cols_addon.filter((col) => val.includes(col));
    
        console.log("Set value of selected columns : ", this._selectedColumns_addon);
      }
       /////////////////////////////// integration ///////////////////////////////////////////////////////////
       onChk_integration() {
        console.log("selected1", this.selected1_integration);
      }
    
      // show hide columns
      @Input() get selectedColumns_integration(): any[] {
        return this._selectedColumns_integration;
      }
    
      set selectedColumns_integration(val: any[]) {
        //restore original order
        this._selectedColumns_integration = this.cols_integration.filter((col) => val.includes(col));
    
        console.log("Set value of selected columns : ", this._selectedColumns_integration);
      }
  
}
