import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewQuestionSetComponent } from './view-question-set.component';

describe('ViewQuestionSetComponent', () => {
  let component: ViewQuestionSetComponent;
  let fixture: ComponentFixture<ViewQuestionSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewQuestionSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewQuestionSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
