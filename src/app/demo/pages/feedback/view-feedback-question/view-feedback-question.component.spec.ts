import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFeedbackQuestionComponent } from './view-feedback-question.component';

describe('ViewFeedbackQuestionComponent', () => {
  let component: ViewFeedbackQuestionComponent;
  let fixture: ComponentFixture<ViewFeedbackQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFeedbackQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFeedbackQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
