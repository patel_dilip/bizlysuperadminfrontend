import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatTabChangeEvent } from '@angular/material';
import { FeedbackService } from 'src/app/_services/_feedbackServices/feedback.service';
import { forkJoin } from 'rxjs';
import { ngxCsv } from 'ngx-csv';
import * as XLSX from 'xlsx';
import { RootCategoryAssetInventoryComponent } from '../../menu-page/asset-Inventory-category-dailogs/root-category-asset-inventory/root-category-asset-inventory.component';
import Swal from 'sweetalert2';

export interface feedbackQuestionElement {
  feedbackQuestionID: string;
  feedbackQuestionTitle: string;
  inputType: string;
  createdByAndOn: string;
}

export interface questionSetElement {
  questionSetCode: string;
  questionSetName: string;
  questionsAdded: string;
  status: boolean;
  createdByAndOn: string;
}

export interface TemplateElement {
  templateCode: string;
  templateName: string;
  templateQuestionsSet: string;
  platForm: string;
  status: boolean;
  collectResponsesFrom: string;
  createdByAndOn: string
}


@Component({
  selector: 'app-feedback-list',
  templateUrl: './feedback-list.component.html',
  styleUrls: ['./feedback-list.component.scss']
})
export class FeedbackListComponent implements OnInit {


  //Template_DATA: templateSetElement
  displayeTemplateSetColumns: string[] = ['templateCode', 'templateName', 'templateQuestionsSet', 'platForm', 'status', 'collectResponsesFrom', 'createdByAndOn', 'action'];
  TemplatedataSource: MatTableDataSource<TemplateElement>;
  @ViewChild("tempaltePaginator", { static: true }) tempaltePaginator: MatPaginator;
  @ViewChild("tempaltesort", { static: true }) tempaltesort: MatSort;

  //QuestionSet_DATA: questionSetElement
  displayedQuestionSetColumns: string[] = ['questionSetCode', 'questionSetName', 'questionsAdded', 'status', 'createdByAndOn', 'action'];
  QuestionSetdataSource: MatTableDataSource<questionSetElement>;
  @ViewChild("createQuestionSetpaginator", { static: true }) createQuestionSetpaginator: MatPaginator;
  @ViewChild("createQuestionSetsort", { static: true }) createQuestionSetsort: MatSort;

  //feedbackQuestion_Data: feedbackQuestionElement
  displayColumnsFeedbackQuestion: string[] = ['feedbackQuestionID', 'feedbackQuestionTitle', 'inputType', 'createdByAndOn', 'action'];
  FeedbackQuestiondataSource: MatTableDataSource<feedbackQuestionElement>;
  @ViewChild("FeedbackQuestionpaginator", { static: true }) FeedbackQuestionpaginator: MatPaginator;
  @ViewChild("FeedbackQuestionsort", { static: true }) FeedbackQuestionsort: MatSort;


  //export excel file 
  templateExportJSONDATA = []
  questionSetExportJSONDATA = []
  feedbackQuestionExportJSONDATA = []
  userid: any;
  feedbackData: any;
  date: string;
  QuestionSetdataSourceData: any;
  allquestionSets: any;
  allquestionSetsData: any;
  allQuestions: any;
  allQuestionData: any;
  allTempaletSet: any;
  allallTempaletSetData: any;
  dialog: any;
  enableQuestSetStatusIndex: any;
  enableQuestSetStatus: boolean = false;
  enableTemplateSetStatusIndex: any;
  enableTemplateSetStatus: boolean = false;
  assignPos: any;
  assignCustomer: any;
  storeData: any;
  fileUploaded: any;
  worksheet: any;
  jsonData: any;
  flag: boolean;
  collectResponsesFrom = ["Facebook", "Whats App",];
  selectedValue = 'bizlytocust'

  constructor(private feedbackService: FeedbackService) {

    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];
    // this.getAllApis()
    this.selectedValue = 'bizlytocust'

    this.onSelectedValue(this.selectedValue)
    this.getTemplateList(event);
    this.getQuestionSetList(event);
    this.getQuestionList(event);
    var d = new Date()
    this.date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  }


  ngOnInit() {



  }

  // getAllFeedbackQuestion() {
  //   this.feedbackService.getAllFeedbackQuestionList().subscribe(data => {
  //     console.log(data);
  //     // this.allDrinks = data['data']
  //     // console.log(this.allDrinks);

  //   })
  // }

  getAllApis(selectedOption) {
    console.log("in fork join");
    
      // forkJoin(
      //   {
      //     allFeedbackQuestion: this.feedbackService.getAllFeedbackQuestionList(),
      //     allQuestionSet: this.feedbackService.getAllQuestionSetList(),
      //     allTempaletSet: this.feedbackService.getAllTempaletSetList()
      //   }
      // )
      this.feedbackService.getAllFeedbackApiData(selectedOption).subscribe(result => {
        console.log("fork Join", result);
        this.feedbackData = result
        console.log("result feedbackData", this.feedbackData);


        //get all tables data function call
        this.getAllFeedQue()
        this.getAllQueSet()
        this.getAllTemplateData()
      })
  }
  //***********Feedback question table **********//
  getAllFeedQue() {
    debugger;
    let data = this.feedbackData.allFeedbackquestions
    console.log("allFeedbackQuestion", data);
    if (data['success'] == false) {

      console.log("Feedback Question Data Not Found");

    } else {


      // this.FeedbackQuestiondataSource = new MatTableDataSource(data['data']);
      this.FeedbackQuestiondataSource = new MatTableDataSource(data);

      this.FeedbackQuestiondataSource.paginator = this.FeedbackQuestionpaginator;
      this.FeedbackQuestiondataSource.sort = this.FeedbackQuestionsort;

      // data['data'].forEach
      data.forEach(element => {
        var d = new Date(element.updatedAt)
        var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
        let feedQueObj = {}
        feedQueObj = {
          "feedbackCode": element.feedbackCode,
          "feedbackQuestionTitle": element.label_Name,
          "inputType": element.inputType,
          "createdByAndOn": element.createdAt,
          "updatedAt": date,
          "action": element.action,
        }
        // console.log(feedQueObj);
        this.feedbackQuestionExportJSONDATA.push(feedQueObj)
      });
      console.log("FeedBackQuestionExport", this.feedbackQuestionExportJSONDATA);
    }
  }
  editQustion(id, element) {
    this.feedbackService.setFeedbackQuetsionId(id, 'edit');
    this.feedbackService.editObject = element


  }
  viewQustion(id, element) {
    this.feedbackService.setFeedbackQuetsionId(id, 'view');
    this.feedbackService.editObject = element
  }

  onSelectedValue(selectedOption) {
    console.log("selected value from drop", selectedOption);
    this.feedbackService.setSelectedValue(selectedOption);
    this.getAllApis(selectedOption)

  }
  createQuestionapplyFilter(event) {
    debugger;
    this.FeedbackQuestiondataSource.data['data'] = this.feedbackData

    //this.dataSourceLiquorBrand.data= this.liquorbrand_data
    const val = event.target.value.trim().toLowerCase();
    const filter = this.FeedbackQuestiondataSource.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['label_Name'].toLowerCase().indexOf(val) !== -1
        || d['inputType'].toLowerCase().indexOf(val) !== -1
      return result

    });


    console.log(filter);
    this.FeedbackQuestiondataSource.data = filter;


  }

  readAsCSVFeedbackQuestion() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["feedbackQuestionID", "feedbackQuestionTitle", "inputType", "createdByAndOn", "updatedAt", "action"]
    };
    new ngxCsv(this.feedbackQuestionExportJSONDATA, 'FeedbackQuestion_Export_' + this.date, options);
  }

  //***********Question Set table **********//
  getAllQueSet() {
    debugger;
    let data = this.feedbackData.allQuestionSet
    console.log("allQuestionSet", data);
    if (data['success'] == false) {

      console.log("Question Set Data Not Found");

    } else {
      let dataAddedCount = [];
      // data['data'].forEach(element => {
        data.forEach(element => {
        console.log('elementod data data', element)
        if (element.selectedQuestions) {
          element.count = element.selectedQuestions.length;

        } else {
          element.count = 0
        }
        console.log("Element.selectedquestion", element.selectedQuestions)
        dataAddedCount.push(element)

      });

      console.log("dataAddedCount", dataAddedCount);

      this.QuestionSetdataSource = new MatTableDataSource(dataAddedCount);
      this.QuestionSetdataSource.paginator = this.createQuestionSetpaginator;
      this.QuestionSetdataSource.sort = this.createQuestionSetsort;

      data.forEach(element => {
        var d = new Date(element.updatedAt)
        var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
        let queSetObj = {}
        queSetObj = {
          "questionSetCode": element.questionSetCode,
          "Question Set Name": element.questionSetName,
          "status": element.status,
          "createdByAndOn": element.createdAt,
          "updatedAt": date,
          "action": element.action,
        }
        // console.log(queSetObj);
        this.questionSetExportJSONDATA.push(queSetObj)
      });
      console.log("questionSetExport", this.questionSetExportJSONDATA);
    }

  }

  editQustionSet(id, element) {
    this.feedbackService.setQuetsionSetId(id, 'editQueSet');
    this.feedbackService.editQueSetObj = element

  }
  viewQustionSet(id, element) {
    this.feedbackService.setQuetsionSetId(id, 'viewQueSet');
    this.feedbackService.editQueSetObj = element
  }

  createQuestionSetapplyFilter(event) {

    this.QuestionSetdataSourceData = this.feedbackData.allQuestionSet['data']
    console.log("QuestionSetdataSourceData", this.QuestionSetdataSourceData);

    const val = event.target.value.trim().toLowerCase();
    const filter = this.QuestionSetdataSourceData.filter(function (d) {
      //console.log(d);
      let result

      result = d['questionSetName'].toLowerCase().indexOf(val) !== -1
      return result

    });
    console.log(filter);
    this.QuestionSetdataSource = filter;

  }

  readAsCSVQuestionSet() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["questionSetCode", "questionSetName", "questionsAdded", "status", "createdByAndOn", "updatedAt"]
    };
    new ngxCsv(this.questionSetExportJSONDATA, 'QuestionSet_Export_' + this.date, options);
  }

  // Question set status toggle
  questatusActiveDeactive(id, event) {
    console.log(event.checked);
    console.log(id);
    this.feedbackService.queSetStatusActiveDeactive(id, { "status": event.checked }).subscribe(res => {
      console.log(res);

      if (res['success'] == true) {
        if (event.checked) {
          Swal.fire('Question Set activated Successfully', '', 'success')
          this.getAllQueSet();
        } else {
          Swal.fire('Question Set De-activated Successfully', '', 'success')

        }

      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
    })
  }


  //***********Template table **********//
  getAllTemplateData() {
    let data = this.feedbackData.allTemplate
    console.log("allFeedbackQuestion", data);
    if (data['success'] == false) {

      console.log("template Question Data Not Found");

    } else {
      let templateDataAddedCount = [];
      data.forEach(element => {
        console.log('elementod data data', element)
        element.count = element.selectedQuestionsSet.length;
        templateDataAddedCount.push(element)
      })
      this.TemplatedataSource = new MatTableDataSource(templateDataAddedCount);
      this.TemplatedataSource.paginator = this.tempaltePaginator;
      this.TemplatedataSource.sort = this.tempaltesort;

      data.forEach(element => {

        var d = new Date(element.updatedAt)
        var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
        let templateObj = {}
        templateObj = {
          "templateCode": element.templateCode,
          "templateName": element.templateName,
          "templateQuestionSet": element.selectedQuestionsSet,
          "createdByAndOn": element.createdAt,
          "updatedAt": date,
          "action": element.action,
        }
        // console.log(feedQueObj);
        this.templateExportJSONDATA.push(templateObj)
      });
      console.log("TemplateQuestionSetExport", this.templateExportJSONDATA);
    }
  }

  editTemplateSet(id, element) {
    console.log("editTemplateSet", element)
    this.feedbackService.setTemplateSetId(id, 'editTemplateSet');
    this.feedbackService.editTemplateSetObj = element

  }
  viewTemplateSet(id, element) {
    console.log("ViewTemplateSet", element)

    this.feedbackService.setTemplateSetId(id, 'viewTemplateSet');
    this.feedbackService.editTemplateSetObj = element
  }

  feedbackTemplateapplyFilter(filterValue: string) {
    this.TemplatedataSource.filter = filterValue.trim().toLowerCase();
    if (this.TemplatedataSource.paginator) {
      this.TemplatedataSource.paginator.firstPage();
    }
  }

  readAsCSVTemplateQuestion() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["templateCode", "templateName", "templateQuestionsSet", "platForm", "status", "collectResponsesFrom", "createdByAndOn", "updatedAt", "action"]
    };
    new ngxCsv(this.templateExportJSONDATA, 'TemplateQuestion_Export_' + this.date, options);
  }

  //*****************Get All question Set ********************//
  getQuestionList(event) {

    this.feedbackService.getAllFeedbackQuestionList().subscribe(
      data => {
        this.allQuestions = data['data'];
        console.log("allquestionlist =", this.allQuestions)
        this.allQuestionData = data['data']

      }
    )
  }

  //*****************Get All question Set ********************//
  getQuestionSetList(event) {
    this.feedbackService.getAllQuestionSetList().subscribe(
      data => {
        this.allquestionSets = data['data'];
        console.log("allquestionSetslist =", this.allquestionSets)
        this.allquestionSetsData = data['data'];
      }
    )

  }

  //*****************Get All template Set ********************//
  getTemplateList(event) {

    this.feedbackService.getAllTempaletSetList().subscribe(
      data => {
        this.allTempaletSet = data['data'];
        console.log("allquestionlist =", this.allTempaletSet)
        this.allallTempaletSetData = data['data']

      }
    )
  }

  // import excel file and convert it to json object
  uploadedFile(event) {
    console.log("upload file", event);

    this.fileUploaded = event.target.files[0];
    console.log("fileUplOd", this.fileUploaded);

    this.readExcel();

  }
  readExcel() {
    let readFile = new FileReader();
    console.log(readFile);

    readFile.onload = (e) => {

      this.storeData = readFile.result;
      var data = new Uint8Array(this.storeData);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });

      var first_sheet_name = workbook.SheetNames[0];
      this.worksheet = workbook.Sheets[first_sheet_name];
      this.jsonData = XLSX.utils.sheet_to_json(this.worksheet, { raw: false });
      this.jsonData.map(ele => (ele.userid = this.userid))
      //   this.jsonData = JSON.stringify(this.jsonData);
      console.log(this.jsonData);
    }
    readFile.readAsArrayBuffer(this.fileUploaded);
    Swal.fire('File uploaded Successfully', '', 'success')


  }


  // template set status toggle

  templatestatusActiveDeactive(id, event) {
    console.log("event", event.checked);
    console.log(id);
    this.feedbackService.templateSetStatusActiveDeactive(id, { "status": event.checked }).subscribe(res => {
      console.log(res);

      if (res['success'] == true) {
        if (event.checked) {
          Swal.fire('Template activated Successfully', '', 'success')
        } else {
          Swal.fire('Template De-activated Successfully', '', 'success')

        }
        this.getAllTemplateData();

      }

      else {
        Swal.fire('Template Deactivated Successfully', '', 'success')


      }

    })
  }


  //*****************Assign template ****************//
  assignTemplate(id) {

    const queryParams = {

      'assignPOS': this.assignPos,
      'assignCustomer': this.assignCustomer,
    }

    console.log("this is a queryparams", queryParams);
    this.feedbackService.processAssign(id, queryParams).subscribe(res => {

      console.log("Resonpose data", res);

      if (res['success'] == true) {

        Swal.fire('Template Assign Successfully', '', 'success')
        // this.router.navigate(['feedback'])
       
      }
      else {
        Swal.fire('Failed to Assign Template', 'Something went wrong', 'warning')
      }
    })
  }

  onTabChanged(tabChangeEvent: MatTabChangeEvent) {
    console.log("event checked", event)
    let eventchecked = tabChangeEvent.index
    if (eventchecked === 0) {

      this.flag = true;
    }
    else {
      this.flag = false;
    }
  }

  enableEditMethod(e, i) {
    console.log('status changed', e, i);
    if (e.checked) {
      this.enableQuestSetStatus = true;
      this.enableQuestSetStatusIndex = i;
      this.enableTemplateSetStatus = true;
      this.enableTemplateSetStatusIndex = i;
    } else {
      this.enableQuestSetStatus = false;
      this.enableQuestSetStatusIndex = i;
      this.enableTemplateSetStatus = false;
      this.enableTemplateSetStatusIndex = i;
    }

    console.log(i, e);
  }
}
