import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFeedbackQuestionComponent } from './add-feedback-question.component';

describe('AddFeedbackQuestionComponent', () => {
  let component: AddFeedbackQuestionComponent;
  let fixture: ComponentFixture<AddFeedbackQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFeedbackQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFeedbackQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
