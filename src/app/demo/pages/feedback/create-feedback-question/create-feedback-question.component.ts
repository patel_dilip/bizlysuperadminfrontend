import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialog } from '@angular/material';
import { AddAttributeRadioButtonComponent } from '../../menu-page/add-attribute-radio-button/add-attribute-radio-button.component';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { FeedbackService } from 'src/app/_services/_feedbackServices/feedback.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { AddRootQuestionTypeComponent } from 'src/app/PopoversList/add-root-question-type/add-root-question-type.component';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { AddDrinkVarientComponent } from 'src/app/PopoversList/add-drink-varient/add-drink-varient.component';
import { AddSubSubVarientComponent } from 'src/app/PopoversList/add-sub-sub-varient/add-sub-sub-varient.component';
import { AddDrinkSubvarientComponent } from 'src/app/PopoversList/add-drink-subvarient/add-drink-subvarient.component';
import { AddCategoryQueTypeComponent } from 'src/app/PopoversList/add-category-que-type/add-category-que-type.component';
import { AddChildCategoryQueTypeComponent } from 'src/app/PopoversList/add-child-category-que-type/add-child-category-que-type.component';
import { AddChildchildCategoryQueTypeComponent } from 'src/app/PopoversList/add-childchild-category-que-type/add-childchild-category-que-type.component';

const URL = environment.base_Url + 'feedback/uploadassociateimage'



@Component({
  selector: 'app-create-feedback-question',
  templateUrl: './create-feedback-question.component.html',
  styleUrls: ['./create-feedback-question.component.scss']
})

export class CreateFeedbackQuestionComponent implements OnInit {

  addQuestionForm: FormGroup
  uploader: FileUploader
  response: string;
  loading = false
  imageurl: any
  imageurls: any = [];
  SelectResponses: string[] = ['Input Box', 'Yes / No', 'Radio Button']
  radiobutton = false;
  yesorNo = false;
  inputbox = false;
  isLable = false;
  radioOptions = [];
  estRootCategory: any;
  estCateName: string[];
  cateChildName: string[];
  cateChildChildName: string[];
  selectedRootType: any;
  categoryName: any;
  childCategory: any;
  subsubChild: any;
  selectedRootNames = [];
  selectedCatNames = [];
  selectedChildName = [];
  selectedChildChildName = [];
  dummy: any;
  feedbackQuestions: any;
  // bizly pos new varible
  DrinkTypes: any;
  QuestionTypes: any;
  VarientsArray: any
  questionType: any;
  subVarients: any;
  addBrandForm: FormGroup
  dataArray: any;           // all root type liquor
  subsubVarientArray: any;
  userid
  categoryType = [];
  category_data: any
  subsubFlag: any;
  typeFlag: number;
  subFlag: number;
  count = 0;
  id: any;
  radioButtondyn: FormGroup;
  radioButton: any;
  allQuestions: any = [];
  filterQuestionTypes: any;
  filterCategoryType: any;
  @ViewChild('image', { static: false }) private image: ElementRef;
  allQuestionData: any;
  createValue: void;
  feedbackQuestionCreatedFor: any;

  constructor(private fb: FormBuilder, private renderer: Renderer2, private addMenuFoodsService: AddMenuFoodsService, private dialog: MatDialog,
    private restServ: RestoService, private router: Router, private feedbackService: FeedbackService, private http: HttpClient
  ) {

    this.createValue = this.feedbackService.getSelectedValue();
    console.log("selected optio in feed que",this.createValue)
    // get establishment root category

    this.getQuestionRootType();
    // this.getFeedbackQuestionRootType();
    this.dummy = 'Establishment'
  }



  ngOnInit() {
    this.addActivity();
    console.log('controls of form', this.addQuestionForm);
    console.log('controls of form', this.addQuestionForm.controls);

    //  NG FILE UPLOADER CODE
    debugger;
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'associatedimage',
      allowedFileType: ['image']

    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      console.log('FileUpload:uploaded: resopnse', response);
      //  const a = this.addQuestionForm.get("radioButtons") as FormArray;
      // this.addQuestionForm.patchValue({
      //   associateImage: response
      // })
      this.imageurl = response;
      // const img: HTMLImageElement = this.renderer.createElement('img');
      //             img.src = response;
      //             this.renderer.addClass(img, 'product-image');
      //             this.renderer.appendChild(this.image.nativeElement, img);

      this.radioButton.patchValue({
        associateImage: response,

      })
      console.log("addRadioButton", this.addQuestionForm.value);
      console.log("event imageurlss in load", this.imageurls);

    }
  }

  addActivity() {
    debugger;
    this.addQuestionForm = this.fb.group({

      label_Name: ['', Validators.required],
      inputType: ['', Validators.required],
      inputPlaceholderName: [''],
      YesNoButtons: [''],
      radioButtons: this.fb.array([
        //  this.addRadioButtonsFromGroup()
      ]),
      //radioButtons: [''],
      categoryType: [''],
      feedbackQuestionCreatedFor:['']


    })
  }
  get radioButtonForm() {
    return this.addQuestionForm.controls.radioButtons as FormArray
  }

  addRadioButton() {
    this.radioButton = this.fb.group({
      optionLable: ['', Validators.required],
      associateImage: ['']
    })

    this.radioButtonForm.push(this.radioButton)
  }
  remove(event, i) {
    alert(i);
    debugger;
    console.log("i", i)
    const remCont = <FormArray>this.addQuestionForm.controls['radioButtons'];
    console.log("remcont", remCont);
    remCont.removeAt(i);

    this.imageurls.splice(i, 1);
  }

  onClickResponse(response) {
    debugger;
    console.log(response);
    this.radioOptions = [];
    this.addQuestionForm.get('inputPlaceholderName').reset();
    this.addQuestionForm.get('YesNoButtons').reset();
    //  this.addQuestionForm.get('radioButtons').reset();

    if (response == "Input Box") {
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true

    }
    if (response == "Yes / No") {
      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false

    }
    if (response == "Radio Button") {
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      // this.addRadioButton();
      this.addRadioButton()
    }
    this.isLable = true;

    console.log('controls of form', this.addQuestionForm);
    console.log('controls of form', this.addQuestionForm.controls);
  }

  onkeyup(event) {
    console.log("onBlur event", event)
    if (event.target.value.length === 1) {
      this.addRadioButton();
    }
    // this.addRadioButton();
  }
  checkindex(i) {
    console.log('index is btn', i)

  }
  onSelectFile(event, index) {
    // debugger;
    console.log('index is', index)
    if (event.target.files) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      console.log("event target files index", event.target.files[index]);
      reader.onload = (event) => { // called once readAsDataURL is completed
        console.log("event target", event.target);

        this.imageurl = event.target['result'];
        console.log("event imageurl", this.imageurl);
        console.log("event imageurl index", index);
        //this.imageurls[index].push(event.target['result'])
        //  this.imageurls.push(event.target['result'])
        //this.imageurls.forEach((elm,i)=>{
        //  if(event.target['result']!==""){
        //   this.imageurls.push(event.target['result'])
        //  }
        //  else{
        //    this.imageurls[index] = event.target['result']
        //  }
        if (this.imageurls[index] === '' || this.imageurls[index] === undefined) {
          console.log('in file');

          if (this.imageurls.length > 0) {
            console.log('in file nested');

            this.imageurls[index] = event.target['result']
          } else {
            console.log('else file nested');

            this.imageurls.push(event.target['result'])
          }

        }
        else {
          console.log('out file');
          console.log('out file index', index);

          this.imageurls[index] = event.target['result']
        }
        // })


        console.log("event imageurls", this.imageurls);
        //this.imageurls.splice(index,0,event.target['result'])
      }
      // var multiReader :any;
      // var filesAmount = event.target.files.length;
      // const control = <FormArray>this.addQuestionForm.controls.radioButtons['controls'][index]['controls']['associateImage']['controls']
      //  for (let i = 0; i < filesAmount; i++) {
      //         multiReader = new FileReader;
      //         multiReader.onload = (event) => {
      //           console.log(multiReader.result);
      //           control.push(this.fb.control(multiReader.result));

      //         }
      //         multiReader.readAsDataURL(event.target.files[i]);    


      //         // event.srcElement.value = null;
      //  console.log('this.addQuestionForm[ img ctroll',control)
      //       }

    }
  }
  viewSetHtml(radioOptions) {
    console.log('this.addQuestionForm viewSetHtml', this.addQuestionForm);
    this.addQuestionForm.setControl('radioButtons', this.setExistingDetails(radioOptions));
    console.log('this.viewStudentListForm viewSetHtml', this.addQuestionForm)
  }
  setExistingDetails(radioButtons: any[]): FormArray {
    const radioButtonFormArray = new FormArray([]);
    radioButtons.forEach(radio => {
      radioButtonFormArray.push(
        this.fb.group({
          optionLable: radio.optionLable,
          associateImage: radio.associateImage,

        }));
    });
    console.log('detailsFormArray setExistin', radioButtonFormArray)
    return radioButtonFormArray;
  }


  addRadioButtonsFromGroup(): FormGroup {
    return this.fb.group({
      optionLable: ['', Validators.required],
      associateImage: ['']
    });
    // this.radioButtonForm.push(this.radioButtondyn)
  }


  addFeedbackQuestion() {
    debugger;
    console.log('this.addQuestionForm.value in submit before patch', this.addQuestionForm.value);

    this.addQuestionForm.patchValue({
      categoryType: this.categoryType,
      feedbackQuestionCreatedFor: this.createValue

    })

    console.log('this.addQuestionForm.value in submit', this.addQuestionForm.value);
    const queryParams = {
      
      //'questionSetType':this.checklistSelection.selected
    }
    console.log('on create question set queryParams', queryParams);
    this.feedbackService.createQuestion(this.addQuestionForm.value).subscribe(res => {

      console.log("Resonpose data", res);

      if (res['success'] == true) {

        Swal.fire('Question Added Successfully', '', 'success')
        this.router.navigate(['feedback'])

      }
      else {
        Swal.fire('Failed to Add Question', 'Something went wrong', 'warning')
      }
    })
  }
  // ****** click carrot function**********

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }



  // *******GET ESTSERVICELIQUER ROOT TYPE*********
  getQuestionRootType() {
    debugger;
    this.QuestionTypes = [];
    this.feedbackService.getAllQuestionTypeSetFromApi().subscribe(data => {
      console.log('estRootCategory', data);
      if (data) {
        // this.estRootCategory = data
        data['data'].forEach((item, i) => {
          item._id = i + 1;
        });
        this.QuestionTypes = data['data']
        // this.QuestionTypes.push(data['data'][0])
        this.getFeedbackQuestionRootType()
      }
      console.log('This.QuestionTypes with id', this.QuestionTypes)
      //console.log('estRootCategory', this.estRootCategory);

    })

  }

  // *******GET Feedback ROOT TYPE*********
  getFeedbackQuestionRootType() {
    debugger;

    this.feedbackService.getAllFeedbackQueTypeSetFromApi().subscribe(data => {
      console.log('feedback', data);
      if (data) {
        // this.estRootCategory = data
        data['data'].forEach((item, i) => {
          // item._id = i + 1;
          //   const found =  this.QuestionTypes.some(el => el._id === item._id);
          //   if (!found) {  
          //     this.QuestionTypes.push(item);  
          // }
          var index = this.QuestionTypes.findIndex(el => el._id === item._id)
          // here you can check specific property for an object whether it exist in your array or not

          if (index === -1) {
            this.QuestionTypes.push(item);
          }
        }); console.log('QuestionTypes getFeedbackQuestionRootType ', this.QuestionTypes);
        //this.QuestionTypes = data['data']


      }
      console.log('This.QuestionTypes with id', this.QuestionTypes)
      //console.log('estRootCategory', this.estRootCategory);

    })

  }





  //******BizlyPOS code of add brand******


  // **********open root question dialog**************

  openRootDialog() {
    const dialogRef = this.dialog.open(AddRootQuestionTypeComponent, {
      width: '460px',
      // height: '200px'
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log('After dialog closed');
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);

    })
  }

  //********open category type dialog box***************

  openCategoryType(categorytypeID) {
    const dialogRef = this.dialog.open(AddCategoryQueTypeComponent, {
      width: '460px',
      data: categorytypeID,
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'after closing varient drink pop'
      );
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })

  }

  //************open sub category dialog box**************//

  openSubCategoryType(rootQueID, categorytypeID) {
    const dialogRef = this.dialog.open(AddChildCategoryQueTypeComponent, {
      width: '460px',
      data: { "rootQueID": rootQueID, "categorytypeID": categorytypeID },
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })
  }


  // **************open sub sub varient drink dialog box************
  openSubSubcategory(rootQueID, categorytypeID, categorytypename) {
    const dialogRef = this.dialog.open(AddChildchildCategoryQueTypeComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootQueID": rootQueID, "categorytypeID": categorytypeID, "categorytypename": categorytypename }
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })
  }

  changeFlag(data, event, root_level, rootindex?: any, subcategoryIndex?: any, subsubcategoryINdex?: any, typeIndex?: any) {
    //console.log( ">>>" , data, ">>>" , event.checked);
    this.count = 0
    console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
    if (root_level == 'root') {
      this.QuestionTypes

        .forEach((element, index) => {
          if (index == rootindex) {
            element.showChildren = event.checked
            if (
              this.QuestionTypes[index].hasOwnProperty('questionType')
              &&
              this.QuestionTypes[index].questionType.length > 0
            ) {
              this.QuestionTypes[index].questionType.forEach((element, subcategoryIndex = index) => {
                element.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubcategoryINdex = index) => {
                    element.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                        element.showChildren = event.checked;
                      })
                    }
                  })
                }
              })
            }
          }
        });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    }
    else if (root_level == 'sub') {
      this.QuestionTypes.forEach((Rootelement, index) => {
        if (index == rootindex) {
          // Rootelement.showChildren = event.checked
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
              if (subindex == subcategoryIndex) {
                subelement.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((subsubelement, subsubcategoryINdex = index) => {
                    subsubelement.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length

                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((typeelement, liquorSubSubVarientTypeIndex = index) => {
                        typeelement.showChildren = event.checked;
                      })
                    }
                  })
                }
              }

            })

          }
        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    } else if (root_level == 'subsub') {
      this.QuestionTypes.forEach((element, index) => {
        if (index == rootindex) {
          // element.showChildren = event.checked
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                //             element.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      element.showChildren = event.checked;
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                          element.showChildren = event.checked;
                        })
                      }
                    }

                  })
                }
                // }
              }
            })
          }
        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    } else if (root_level == 'type') {
      this.QuestionTypes.forEach((element, index) => {
        if (index == rootindex) {
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, childIndex) => {
                          if (typeIndex == childIndex)
                            element.showChildren = event.checked;
                        })
                      }
                    }
                  })
                }
                // }
              }
            })
          }
        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    }
    ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
                &&
                this.QuestionTypes[index].questionType[subindex].categories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {
                  // if(subsubINdex==0){
                  //   this.subsubFlag=0
                  // }
                  if (subsubINdex == subsubcategoryINdex) {
                    if (subsubelement.showChildren == true) {
                      this.subsubFlag++
                    }
                    //console.log("<<<", this.QuestionTypes[index].subCategories[subcategoryIndex].categories.length);
                    //console.log(">>>", this.subsubFlag);


                    if (this.QuestionTypes[index].questionType[subindex].categories.length == this.subsubFlag) {
                      subelement.showChildren = true
                    } else {
                      subelement.showChildren = false;
                    }
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      // //console.log(element);
                      this.typeFlag = 0
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {
                        // if (typeIndex == childIndex) {
                        //   typeelement.showChildren = event.checked;
                        // }
                        if (typeelement.showChildren == true) {
                          this.typeFlag++
                        }
                        console.log("<<<", this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length);
                        console.log(">>>", this.typeFlag);


                        if (this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length == this.typeFlag) {
                          subsubelement.showChildren = true
                        } else {
                          subsubelement.showChildren = false;
                        }

                      })
                    }
                  }
                })
              }
              // }
            }
          })
        }
      }
    });
    //subsub
    this.subsubFlag = 0

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {

              if (
                this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
                &&
                this.QuestionTypes[index].questionType[subindex].categories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {

                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  // //console.log("<<<", this.QuestionTypes[index].subCategories[subcategoryIndex].categories.length);
                  // //console.log(">>>", this.subsubFlag);

                  if (this.QuestionTypes[index].questionType[subindex].categories.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    // this.typeFlag=0
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {
                    })
                  }
                })
              }

            }
          })
        }
      }
    });

    //////subflag
    this.subFlag = 0
    this.subsubFlag = 0

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            // if(subindex==subcategoryIndex){
            if (subelement.showChildren == true) {
              this.subFlag++
            }
            // console.log("<<<", this.QuestionTypes[index].questionType.length);
            // console.log(">>>", this.subFlag);

            if (this.QuestionTypes[index].questionType.length == this.subFlag) {
              Rootelement.showChildren = true
            } else {
              Rootelement.showChildren = false;
            }

            if (
              this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
              &&
              this.QuestionTypes[index].questionType[subindex].categories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {

                if (
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {

                  })
                }
              })
            }
            // }
          })
        }
      }
    });
    console.log(this.QuestionTypes[rootindex]);

    if (this.QuestionTypes[rootindex].showChildren == false) {
      // element.showChildren = event.checked
      if (
        this.QuestionTypes[rootindex].hasOwnProperty('questionType')
        &&
        this.QuestionTypes[rootindex].questionType.length > 0
      ) {
        this.QuestionTypes[rootindex].questionType.forEach((element, subindex) => {
          if (element.showChildren == false) {
            if (
              this.QuestionTypes[rootindex].questionType[subindex].hasOwnProperty('categories')
              &&
              this.QuestionTypes[rootindex].questionType[subindex].categories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.QuestionTypes[rootindex].questionType[subindex].categories.forEach((element, subsubINdex) => {
                if (element.showChildren == false) {
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex) => {
                      if (element.showChildren == true) {
                        this.count = 1;
                        return false
                      }
                    })
                  }


                }
                else {
                  this.count = 1;
                  return false;
                }
              })
            }
          }
          if (element.showChildren == true) {
            this.count = 1;
            return false;
          }
        })
      }
    }
    else {
      this.count = 1
    }

    console.log(this.count);
    if (this.count == 1) {
      if (this.categoryType.length == 0) {
        this.categoryType.push(this.QuestionTypes[rootindex])
      } else {
        this.categoryType.forEach(element => {
          if (element['_id'] == this.QuestionTypes[rootindex]['_id']) {
            element = this.QuestionTypes[rootindex]
            this.id = element['_id'];
          }
        });
        if (this.id != this.QuestionTypes[rootindex]['_id']) {
          this.categoryType.push(this.QuestionTypes[rootindex])
        }
      }
    }
    if (this.count == 0) {
      let i = 0;
      this.categoryType.forEach(element => {

        if (element['_id'] == this.QuestionTypes[rootindex]['_id']) {
          this.categoryType.splice(i, 1)
        } else {
          console.log("not match");

        }
        i++;
      });
    }



    console.log(this.count);
    if (this.count == 1) {
      if (this.filterCategoryType.length == 0) {
        this.filterCategoryType.push(this.QuestionTypes[rootindex])
      } else {
        this.filterCategoryType.forEach(element => {
          if (element['_id'] == this.QuestionTypes[rootindex]['_id']) {
            element = this.QuestionTypes[rootindex]
            this.id = element['_id'];
          }
        });
        if (this.id != this.QuestionTypes[rootindex]['_id']) {
          this.filterCategoryType.push(this.QuestionTypes[rootindex])
        }
      }
    }
    if (this.count == 0) {
      let i = 0;
      this.filterCategoryType.forEach(element => {

        if (element['_id'] == this.QuestionTypes[rootindex]['_id']) {
          this.filterCategoryType.splice(i, 1)
        } else {
          console.log("not match");

        }
        i++;
      });
    }
    console.log(this.filterCategoryType);
    this.filterQuestionOnType()

  }
  filterQuestionOnType() {
    console.log('filterCategoryType in functn', this.filterCategoryType);
    const filter = this.allQuestionData.filter(function (d) {
      // console.log(d)

    });
    // let rslt = this.filterCategoryType.forEach( elm =>{
    //   if(elm.showChildren === true){
    //       console.log(elm._id)
    //       return 

    //   }
    // })
    let newArray = [];
    getAllId(this.filterCategoryType, '_id')



    function getAllId(arr, key) {
      arr.forEach(function (item) {
        for (let keys in item) {
          if (keys === key && item.showChildren === true) {
            newArray.push(item)
          } else if (Array.isArray(item[keys])) {
            getAllId(item[keys], key)
          }
        }

      })
    }
    console.log('new arrrr', newArray)

    // ********** code 1***************
    // filterQues(this.allQuestionData, 1)
    // let arr = this.allQuestionData;
    //  function filterQues(arr, term) {
    //   var matches = [];
    //   if (!Array.isArray(arr)) return matches;

    //   arr.forEach(function(i) {
    //       if (i.value.includes(term)) {
    //           matches.push(i);
    //       } else {
    //           let childResults = filterQues(i.children, term);
    //           if (childResults.length)
    //               matches.push(Object.assign({}, i, { children: childResults }));
    //       }
    //   })

    //   return matches;
    // }
    // console.log('output fltr' ,filterQues);



    // ********code 2************8

    console.log('this.allQuestionData', this.allQuestionData);

    // function find(obj, key) {
    //   if (obj.value && obj.value.indexOf(key) > -1){
    //     return true;
    //   }
    //   if (obj.children && obj.children.length > 0){
    //     return obj.children.reduce(function(obj1, obj2){
    //       return find(obj1, key) || find(obj2, key);
    //     }, {}); 
    //   } 
    //   return false;
    // }

    // var output = this.allQuestionData.filter(function(obj){
    //      return find(obj, 1);
    //  });
    // console.log('Result', output);






    // code 3 *************
    // var currentPath = [];

    // function depthFirstTraversal(o, fn) {
    //     currentPath.push(o);
    //     if(o.children) {
    //         for(var i = 0, len = o.children.length; i < len; i++) {
    //             depthFirstTraversal(o.children[i], fn);
    //         }
    //     }
    //     fn.call(null, o, currentPath);
    //     currentPath.pop();
    // }

    // function shallowCopy(o) {
    //     var result = {};
    //     for(var k in o) {
    //         if(o.hasOwnProperty(k)) {
    //             result[k] = o[k];
    //         }
    //     }
    //     return result;
    // }

    // function copyNode(node) {
    //     var n = shallowCopy(node);
    //     // if(n.children) { n.children = []; }
    //     // return n;
    // }

    // function filterTree(root, ids) {
    //     root.copied = copyNode(root); // create a copy of root
    //     var filteredResult = root.copied;

    //     depthFirstTraversal(root, function(node, branch) {
    //         // if this is a leaf node _and_ we are looking for its ID
    //         if( !node.children && ids.indexOf(node.id) !== -1 ) {
    //             // use the path that the depthFirstTraversal hands us that
    //             // leads to this leaf.  copy any part of this branch that
    //             // hasn't been copied, at minimum that will be this leaf
    //             for(var i = 0, len = branch.length; i < len; i++) {
    //                 if(branch[i].copied) { continue; } // already copied

    //                 branch[i].copied = copyNode(branch[i]);
    //                 // now attach the copy to the new 'parellel' tree we are building
    //                 branch[i-1].copied.children.push(branch[i].copied);
    //             }
    //         }
    //     });

    //     depthFirstTraversal(root, function(node, branch) {
    //         delete node.copied; // cleanup the mutation of the original tree
    //     });

    //     return filteredResult;
    // }

    // function filterTreeList(list, ids) {
    //     var filteredList = [];
    //     for(var i = 0, len = list.length; i < len; i++) {
    //         filteredList.push( filterTree(list[i], ids) );
    //     }
    //     return filteredList;
    // }

    // var hierarchy = [ /* your data here */ ];
    // var ids = [1,3];

    // var filtered = filterTreeList(this.allQuestionData, newArray);
    // console.log('filtered',filtered);





    // code 4***********
    // const deepFilter = (array, indicator) => {
    //   return array.filter(function iter(o) {
    //     return Object.keys(o).some(k => {
    //       if (typeof o[k] === 'string' && o[k].includes(indicator)) {
    //         return true;
    //       }
    //       if (Array.isArray(o[k])) {
    //         o[k] = o[k].filter(iter);
    //         return o[k].length;
    //       }
    //     });
    //   });
    // }
    // let actulreslt
    // newArray.forEach(elm => {
    //   actulreslt = deepFilter(this.allQuestions, "5ec8fbf4c89f8d7fa2be8830")
    // })
    // // // console.log('fltrrrrrr',deepFilter(this.allQuestionData, "5ec8fbf4c89f8d7fa2be8830"));

    // console.log('actulreslt', actulreslt);

    // // console.log(res);
    // this.allQuestions = filter;
  }


}
