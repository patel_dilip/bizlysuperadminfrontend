import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-template-feedback',
  templateUrl: './view-template-feedback.component.html',
  styleUrls: ['./view-template-feedback.component.scss']
})
export class ViewTemplateFeedbackComponent implements OnInit {
 viewFeedBack={ templateName: "abc", 
 templateDescription: "give your feedback", 
 selectFilter: "Bizly to Customer", platForm: "Dine in QR Code", 
 selectedQuestionsSet: [{ feedbackQuestionID: "Q001", label_Name: "how was food quality", inputType: "Input Box", inputPlaceholderName: "Enter here" },
 { feedbackQuestionID: "Q87", label_Name: "how was food quality", inputType: "Radio Button", RadioButtons: ["good", "not good", "best", "bad"] },
 { feedbackQuestionID: "Q345", label_Name: " was food quality good", inputType: "Yes / No" }

 ]}
  constructor() { }

  ngOnInit() {
  }

}
