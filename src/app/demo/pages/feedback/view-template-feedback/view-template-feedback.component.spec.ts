import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTemplateFeedbackComponent } from './view-template-feedback.component';

describe('ViewTemplateFeedbackComponent', () => {
  let component: ViewTemplateFeedbackComponent;
  let fixture: ComponentFixture<ViewTemplateFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTemplateFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTemplateFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
