import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatRadioChange } from '@angular/material';
import { FeedbackService } from 'src/app/_services/_feedbackServices/feedback.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AddChildchildCategoryQueTypeComponent } from 'src/app/PopoversList/add-childchild-category-que-type/add-childchild-category-que-type.component';
import { AddChildCategoryQueTypeComponent } from 'src/app/PopoversList/add-child-category-que-type/add-child-category-que-type.component';
import { AddCategoryQueTypeComponent } from 'src/app/PopoversList/add-category-que-type/add-category-que-type.component';
import { AddRootQuestionTypeComponent } from 'src/app/PopoversList/add-root-question-type/add-root-question-type.component';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
@Component({
  selector: 'app-update-template',
  templateUrl: './update-template.component.html',
  styleUrls: ['./update-template.component.scss']
})
export class UpdateTemplateComponent implements OnInit {
  createTemplateForm: FormGroup

  allquestionSets = []
  SelectFilters = ['Bizly to Customer', 'Bizly to POS Merchant', 'POS Merchant to Customers']
  selectPlatForms = ['Dine in QR code', 'Dine in APP']
  selectPlatFormBizlytoCust = ['Dine in QR code', 'Dine in APP']
  selectPlatFormBizlytoPOS = ['Web Mail', 'Web in APP']
  selectPlatFormPOStoCust = ['Web Mail', 'Web in APP', 'Dine in QR code', 'Dine in APP']

  selectedQuestionsSet = []

  // SelectFilters = ['Bizly to Customer', 'Bizly of POS Merchant', 'POS Merchant to Customers']
  // selectPlatForms = ['Dine in QR code', 'Dine in APP']
  selectedQuestions: any = []
  selectectedQuestionCount: any;

  selectectedQuestionSetCount: any;

  allQuestions: any;
  allQuestionData: any;
  isQueCheck: boolean = false;
  QuestionTypes: any;
  VarientsArray: any
  questionType: any;
  subVarients: any;
  addBrandForm: FormGroup
  dataArray: any;           // all root type liquor
  subsubVarientArray: any;
  userid
  categoryType = [];
  filterCategoryType = [];
  category_data: any
  subsubFlag: any;
  typeFlag: number;
  subFlag: number;
  count = 0;
  id: any;
  allquestionSetsData: any;
  searchQuestionTemplateInput: string;
  template: any;
  QuestionTree: any = [];
  filterQuestionTypes: any;
  templateSetObj: any;
  templateSetId: any;
  isViewOnly: boolean;
  templateId: any;
  createValue: any;

  // template={ templateName: "abc", templateDescription: "give feedback", selectFilter: "Bizly to Customer", 
  // platForm: "Dine in QR code", selectedQuestionsSet: [
  //   {questionSetName: "A", questionSetCode: "QS9876",
  //   selectedQuestions:["Q001", "Q87"]},
  //   {questionSetName: "D", questionSetCode: "QS9878",
  //   selectedQuestions:["Q001", "Q87"]}] }
  constructor(private fb: FormBuilder, private router: Router, private dialog: MatDialog, private feedbackService: FeedbackService) {

    this.createValue = this.feedbackService.getSelectedValue();
    this.templateSetId = this.feedbackService.getTemplateSetId();

    this.templateSetObj = this.feedbackService.editTemplateSetObj
    console.log("categoryType from edit", this.templateSetObj);
    this.categoryType = this.templateSetObj['categoryType']
    console.log("categoryType categoryType from edit", this.categoryType);
    console.log("screenFlag", this.templateSetId['screenFlag']);
    if (this.templateSetId['screenFlag'] === 'viewTemplateSet') {
      this.isViewOnly = true;
    }
    else {
      this.isViewOnly = false
    }
    this.updateCreateTemplates();
    this.editTemplate();
    this.getQuestionRootType();
    this.getQuestionList(event);
    this.getQuestionSetList(event);
    console.log("templatesetid", this.templateSetId);
   


  }

  ngOnInit() {
  }

  updateCreateTemplates() {
    this.createTemplateForm = this.fb.group({
      templateName: [{ value: '', disabled: this.isViewOnly }, [Validators.required, Validators.pattern('^[ a-zA-Z0-9]+$')]],
      templateDescription: [{ value: '', disabled: this.isViewOnly }, [Validators.required, Validators.pattern('^[ a-zA-Z0-9.,]+$')]],
      selectFilter: [{ value: '', disabled: this.isViewOnly }, Validators.required],
      platForm: [{ value: '', disabled: this.isViewOnly }],
      categoryType: [''],
      selectedQuestions: [''],
      selectedQuestionsSet: [''],
      templateCreatedFor:['']
    })
  }

  getQuestionList(event) {

    this.feedbackService.getAllFeedbackQuestionList().subscribe(
      data => {
        this.allQuestions = data['data'];
        this.allQuestionData = data['data']
        console.log('all questions', this.allQuestions)
        this.allQuestions.forEach(elm => {
          elm.selected = false;
        })

        console.log('all questions after selected', this.allQuestions)
        this.setSelectedQuestions()
      }
    )
  }

  getQuestionSetList(event) {
    this.feedbackService.getAllQuestionSetList().subscribe(
      data => {
        this.allquestionSets = data['data'];
        console.log("allquestionSetslist =", this.allquestionSets)
        this.allquestionSetsData = data['data'];
        this.allquestionSets.forEach(elm => {
          elm.selected = false;
        })

        console.log('all questions after selected', this.allQuestions)
        this.setSelectedQuestionsSet()
      }
    )

  }

  /************** selected question *******************/
  setSelectedQuestions() {
    debugger;
    this.allQuestions.forEach(element => {
      this.templateSetObj.selectedQuestions.forEach(elm => {
        if (elm.feedbackCode === element.feedbackCode) {
          console.log('elm in side codn')
          element.selected = true;
        }
      });
    });
    console.log('AFTER check true false', this.allQuestions)
    this.selectedQuestions = this.templateSetObj.selectedQuestions;

    this.selectectedQuestionCount = this.selectedQuestions.length
  }
  setSelectedQuestionsSet() {
    debugger;
    this.allquestionSets.forEach(element => {
      this.templateSetObj.selectedQuestionsSet.forEach(elm => {
        console.log('elemeent', element),
          console.log('elm', elm)

        if (elm.questionSetCode === element.questionSetCode) {
          console.log('elm in side codn')
          element.selected = true;
        }
      });
    });
    console.log('AFTER check true false', this.allQuestions)
    this.selectedQuestionsSet = this.templateSetObj.selectedQuestionsSet;

    this.selectectedQuestionSetCount = this.selectedQuestionsSet.length
  }


  onFilterSelection(event) {
    debugger;
    if (this.createTemplateForm.get('selectFilter').value !== 'Bizly to Customer'
      || this.createTemplateForm.get('selectFilter').value !== 'Bizly to POS Merchant'
      || this.createTemplateForm.get('selectFilter').value !== 'POS Merchant to Customer') {
      this.createTemplateForm.get('platForm').reset();

    }

  }

  onSelectOption(mrChange: MatRadioChange) {
    this.searchQuestionTemplateInput = ''
    console.log("mrChange.value=>", mrChange.value);
    if (mrChange.value === "question") {
      this.isQueCheck = true;
    }
    else {
      this.isQueCheck = false;
    }
  }

  onSelectQuestion(id, event) {
    debugger
    let status = event.checked
    if (status == true) {
      this.selectedQuestions.push(id)
      console.log(this.selectedQuestions);
    } else {
      this.selectedQuestions.splice(this.selectedQuestions.indexOf(id), 1)
      console.log("tis is selected question", this.selectedQuestions);
    }
    this.selectectedQuestionCount = this.selectedQuestions.length
    console.log("tis is selected selectectedQuestionCount", this.selectectedQuestionCount);
  }
  onSelectQuestionset(id, event) {
    debugger
    let status = event.checked
    if (status == true) {
      this.selectedQuestionsSet.push(id)
      console.log(this.selectedQuestionsSet);
    } else {
      this.selectedQuestionsSet.splice(this.selectedQuestionsSet.indexOf(id), 1)
      console.log("tis is selected question set", this.selectedQuestionsSet);
    }
    this.selectectedQuestionSetCount = this.selectedQuestionsSet.length
    console.log("tis is selected selectectedQuestionCount set", this.selectectedQuestionSetCount);
  }

  searchQuestionTemplate(event) {
    debugger;
    const val = event.target.value.trim().toLowerCase();
    let result
    if (this.isQueCheck === false) {
      const filter = this.allquestionSetsData.filter(function (d) {
        console.log(d);
        result = d['questionSetName'].toLowerCase().indexOf(val) !== -1
        return result
      });
      console.log('allquestionSetsData filterd', filter);
      this.allquestionSets = filter;
    } else {
      const filter = this.allQuestionData.filter(function (d) {
        console.log(d);
        result = d['label_Name'].toLowerCase().indexOf(val) !== -1
          || d['inputType'].toLowerCase().indexOf(val) !== -1
        return result
      });
      console.log('allQuestionData filterd', filter);
      this.allQuestions = filter;
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.allQuestions, event.previousIndex, event.currentIndex);
  }

  dropset(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.allquestionSets, event.previousIndex, event.currentIndex);
  }

  // ****** click carrot function**********

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  // *******GET ESTABLISHMENT ROOT TYPE*********
  getQuestionRootType() {
    debugger;
    this.QuestionTypes=[];
    this.QuestionTree =[]
    this.feedbackService.getAllQuestionTypeSetFromApi().subscribe(data => {
      console.log('QuestionTypes', data);
      if (data) {
        // this.estRootCategory = data
        data['data'].forEach((item, i) => {
          item._id = i + 1;
        });
        this.QuestionTypes = data['data']
        this.getFeedbackQuestionRootType()
      }

      //console.log('estRootCategory', this.estRootCategory);
      this.QuestionTypes.forEach(element => {
        this.categoryType.forEach(ele => {
          if (element['_id'] == ele['_id']) {
            element = ele

          }

        })
        this.QuestionTree.push(element)
      });
      this.QuestionTypes = this.QuestionTree
      console.log(this.categoryType);
    })

  }
  // *******GET Feedback ROOT TYPE*********
  getFeedbackQuestionRootType() {
    debugger;

    this.feedbackService.getAllFeedbackQueTypeSetFromApi().subscribe(data => {
      console.log('feedback', data);
      if (data) {
        // this.estRootCategory = data
        data['data'].forEach((item, i) => {
          // item._id = i + 1;
          //   const found =  this.QuestionTypes.some(el => el._id === item._id);
          //   if (!found) {  
          //     this.QuestionTypes.push(item);  
          // }
          var index = this.QuestionTypes.findIndex(el => el._id === item._id)
          // here you can check specific property for an object whether it exist in your array or not

          if (index === -1) {
            this.QuestionTypes.push(item);
          }
        }); console.log('QuestionTypes getFeedbackQuestionRootType ', this.QuestionTypes);
        //this.QuestionTypes = data['data']
        this.QuestionTypes.forEach(element => {
          this.categoryType.forEach(ele => {
            if (element['_id'] == ele['_id'])
              element = ele
  
          })

          
          this.QuestionTree.push(element)
        });
        this.QuestionTypes = this.QuestionTree



      }
      console.log('This.QuestionTypes with id', this.QuestionTypes)
      //console.log('estRootCategory', this.estRootCategory);

    })

  }


   // **********open root question dialog**************

   openRootDialog() {
    const dialogRef = this.dialog.open(AddRootQuestionTypeComponent, {
      width: '460px',
      // height: '200px'
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log('After dialog closed');
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);

    })
  }

  //********open category type dialog box***************

  openCategoryType(categorytypeID) {
    const dialogRef = this.dialog.open(AddCategoryQueTypeComponent, {
      width: '460px',
      data: categorytypeID,
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'after closing varient drink pop'
      );
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })

  }

  //************open sub category dialog box**************//

  openSubCategoryType(rootQueID, categorytypeID) {
    const dialogRef = this.dialog.open(AddChildCategoryQueTypeComponent, {
      width: '460px',
      data: { "rootQueID": rootQueID, "categorytypeID": categorytypeID },
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })
  }


  // **************open sub sub varient drink dialog box************
  openSubSubcategory(rootQueID, categorytypeID, categorytypename) {
    const dialogRef = this.dialog.open(AddChildchildCategoryQueTypeComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootQueID": rootQueID, "categorytypeID": categorytypeID, "categorytypename": categorytypename }
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getFeedbackQuestionRootType()
      }, 2000);
    })
  }

  editTemplate() {
    debugger;
    this.createTemplateForm.patchValue({
      templateName: this.templateSetObj.templateName,
      templateDescription: this.templateSetObj.templateDescription,
      selectFilter: this.templateSetObj.selectFilter,
      platForm: this.templateSetObj.platForm,
      categoryType: this.categoryType,
      selectedQuestions: this.selectedQuestions,
      selectedQuestionsSet: this.selectedQuestionsSet
    })
    console.log('createTemplateForm', this.createTemplateForm.value)
    this.allquestionSets = this.templateSetObj.selectedQuestionsSet
  }

  createTemplate() {
    this.createTemplateForm.patchValue({
      selectedQuestions: this.allQuestions,
      selectedQuestionsSet: this.allquestionSets,
      templateCreatedFor : this.createValue

    })
    console.log(this.createTemplateForm.value);
    this.feedbackService.updateTemplateSet(this.templateSetId['temId'], this.createTemplateForm.value).subscribe(res => {

      console.log("Resonpose data", res);

      if (res['success'] == true) {

        Swal.fire('Template Updated Successfully', '', 'success')
        this.router.navigate(['feedback'])

      }
      else {
        Swal.fire('Failed to update Template', 'Something went wrong', 'warning')
      }
    })
  }

  changeFlag(data, event, root_level, rootindex?: any, subcategoryIndex?: any, subsubcategoryINdex?: any, typeIndex?: any) {
    //console.log( ">>>" , data, ">>>" , event.checked);
    this.count = 0
    console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
    if (root_level == 'root') {
      this.QuestionTypes

        .forEach((element, index) => {
          if (index == rootindex) {
            element.showChildren = event.checked
            if (
              this.QuestionTypes[index].hasOwnProperty('questionType')
              &&
              this.QuestionTypes[index].questionType.length > 0
            ) {
              this.QuestionTypes[index].questionType.forEach((element, subcategoryIndex = index) => {
                element.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubcategoryINdex = index) => {
                    element.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                        element.showChildren = event.checked;
                      })
                    }


                  })
                }
              })
            }
          }
        });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    }
    else if (root_level == 'sub') {
      this.QuestionTypes.forEach((Rootelement, index) => {
        if (index == rootindex) {
          // Rootelement.showChildren = event.checked
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
              if (subindex == subcategoryIndex) {
                subelement.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((subsubelement, subsubcategoryINdex = index) => {
                    subsubelement.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length

                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((typeelement, liquorSubSubVarientTypeIndex = index) => {
                        typeelement.showChildren = event.checked;
                      })
                    }


                  })
                }
              }

            })

          }


        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    } else if (root_level == 'subsub') {
      this.QuestionTypes.forEach((element, index) => {
        if (index == rootindex) {
          // element.showChildren = event.checked
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                //             element.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      element.showChildren = event.checked;
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                          element.showChildren = event.checked;
                        })
                      }
                    }

                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    } else if (root_level == 'type') {
      this.QuestionTypes.forEach((element, index) => {
        if (index == rootindex) {
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, childIndex) => {
                          if (typeIndex == childIndex)
                            element.showChildren = event.checked;
                        })
                      }


                    }
                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    }
    ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
                &&
                this.QuestionTypes[index].questionType[subindex].categories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {
                  // if(subsubINdex==0){
                  //   this.subsubFlag=0
                  // }
                  if (subsubINdex == subsubcategoryINdex) {
                    if (subsubelement.showChildren == true) {
                      this.subsubFlag++
                    }
                    //console.log("<<<", this.QuestionTypes[index].subCategories[subcategoryIndex].categories.length);
                    //console.log(">>>", this.subsubFlag);


                    if (this.QuestionTypes[index].questionType[subindex].categories.length == this.subsubFlag) {
                      subelement.showChildren = true
                    } else {
                      subelement.showChildren = false;
                    }
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      // //console.log(element);
                      this.typeFlag = 0
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {
                        // if (typeIndex == childIndex) {
                        //   typeelement.showChildren = event.checked;
                        // }
                        if (typeelement.showChildren == true) {
                          this.typeFlag++
                        }
                        console.log("<<<", this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length);
                        console.log(">>>", this.typeFlag);


                        if (this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length == this.typeFlag) {
                          subsubelement.showChildren = true
                        } else {
                          subsubelement.showChildren = false;
                        }

                      })
                    }


                  }
                })
              }
              // }
            }
          })
        }


      }
    });
    //subsub
    this.subsubFlag = 0

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {

              if (
                this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
                &&
                this.QuestionTypes[index].questionType[subindex].categories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {

                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  // //console.log("<<<", this.QuestionTypes[index].subCategories[subcategoryIndex].categories.length);
                  // //console.log(">>>", this.subsubFlag);


                  if (this.QuestionTypes[index].questionType[subindex].categories.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    // this.typeFlag=0
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {



                    })
                  }
                })
              }

            }
          })
        }


      }
    });

    //////subflag
    this.subFlag = 0
    this.subsubFlag = 0

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            // if(subindex==subcategoryIndex){
            if (subelement.showChildren == true) {
              this.subFlag++
            }
            // console.log("<<<", this.QuestionTypes[index].questionType.length);
            // console.log(">>>", this.subFlag);


            if (this.QuestionTypes[index].questionType.length == this.subFlag) {
              Rootelement.showChildren = true
            } else {
              Rootelement.showChildren = false;
            }

            if (
              this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
              &&
              this.QuestionTypes[index].questionType[subindex].categories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {


                if (
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {


                  })
                }
              })
            }

            // }
          })
        }


      }
    });
    console.log(this.QuestionTypes[rootindex]);

    if (this.QuestionTypes[rootindex].showChildren == false) {
      // element.showChildren = event.checked
      if (
        this.QuestionTypes[rootindex].hasOwnProperty('questionType')
        &&
        this.QuestionTypes[rootindex].questionType.length > 0
      ) {
        this.QuestionTypes[rootindex].questionType.forEach((element, subindex) => {
          if (element.showChildren == false) {
            if (
              this.QuestionTypes[rootindex].questionType[subindex].hasOwnProperty('categories')
              &&
              this.QuestionTypes[rootindex].questionType[subindex].categories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.QuestionTypes[rootindex].questionType[subindex].categories.forEach((element, subsubINdex) => {
                if (element.showChildren == false) {
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex) => {
                      if (element.showChildren == true) {
                        this.count = 1;
                        return false
                      }
                    })
                  }


                }
                else {
                  this.count = 1;
                  return false;
                }
              })
            }
          }
          if (element.showChildren == true) {
            this.count = 1;
            return false;
          }
        })
      }
    }
    else {
      this.count = 1
    }

    console.log(this.count);
    if (this.count == 1) {
      if (this.categoryType.length == 0) {
        this.categoryType.push(this.QuestionTypes[rootindex])
      } else {
        this.categoryType.forEach(element => {
          if (element['_id'] == this.QuestionTypes[rootindex]['_id']) {
            element = this.QuestionTypes[rootindex]
            this.id = element['_id'];
          }
        });
        if (this.id != this.QuestionTypes[rootindex]['_id']) {
          this.categoryType.push(this.QuestionTypes[rootindex])
        }
      }
    }
    if (this.count == 0) {
      let i = 0;
      this.categoryType.forEach(element => {

        if (element['_id'] == this.QuestionTypes[rootindex]['_id']) {
          this.categoryType.splice(i, 1)
        } else {
          console.log("not match");

        }
        i++;
      });
    }
    console.log(this.categoryType);

  }

}
