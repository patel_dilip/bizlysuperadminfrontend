import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialog } from '@angular/material';
import { AddAttributeRadioButtonComponent } from '../../menu-page/add-attribute-radio-button/add-attribute-radio-button.component';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { FeedbackService } from 'src/app/_services/_feedbackServices/feedback.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { AddRootQuestionTypeComponent } from 'src/app/PopoversList/add-root-question-type/add-root-question-type.component';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { AddCategoryQueTypeComponent } from 'src/app/PopoversList/add-category-que-type/add-category-que-type.component';
import { AddChildCategoryQueTypeComponent } from 'src/app/PopoversList/add-child-category-que-type/add-child-category-que-type.component';
import { AddChildchildCategoryQueTypeComponent } from 'src/app/PopoversList/add-childchild-category-que-type/add-childchild-category-que-type.component';

@Component({
  selector: 'app-create-question-set',
  templateUrl: './create-question-set.component.html',
  styleUrls: ['./create-question-set.component.scss']
})
export class CreateQuestionSetComponent implements OnInit {

  createQuestionSetForm: FormGroup
  selectedQuestions: any = []
  allQuestions: any = [];
  uploader: FileUploader
  response: string;
  loading = false
  imageurl: any
  SelectResponses: string[] = ['Input Box', 'Yes / No', 'Radio Button']
  radiobutton = false;
  yesorNo = false;
  inputbox = false;
  isLable = false;
  radioOptions = [];
  estRootCategory: any;
  estCateName: string[];
  cateChildName: string[];
  cateChildChildName: string[];
  selectedRootType: any;
  categoryName: any;
  childCategory: any;
  subsubChild: any;
  selectedRootNames = [];
  selectedCatNames = [];
  selectedChildName = [];
  selectedChildChildName = [];
  dummy: any;
  feedbackQuestions: any;
  // bizly pos new varible
  QuestionTypes: any;
  VarientsArray: any
  questionType: any;
  subVarients: any;
  addBrandForm: FormGroup
  dataArray: any;           // all root type liquor
  subsubVarientArray: any;
  userid
  categoryType = [];
  category_data: any
  subsubFlag: any;
  typeFlag: number;
  subFlag: number;
  count = 0;
  id: any;
  radioButtondyn: FormGroup;
  radioButtons: any;
  queObj: any;
  qestionID: any;
  questionId: any;
  radioButton: any;
  allQuestionData: any;
  questionsetModalflag: boolean = false;
  selectectedQuestionCount: any;
  filterQuestionTypes: any;
  filterCategoryType: any;
  createValue: any;
  
  constructor(private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService, private dialog: MatDialog,
    private restServ: RestoService, private router: Router, private feedbackService: FeedbackService, private http: HttpClient) {
      this.createValue = this.feedbackService.getSelectedValue();
      this.getQuestionRootType();
    //this.getFeedbackQuestionRootType();
    this.getQuestionList(event);
  }

  ngOnInit() {
    this.createFeedbackForm();
  }

  createFeedbackForm() {
    this.createQuestionSetForm = this.fb.group({
      questionSetName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z0-9]+$')]],
      selectedQuestions: [''],
      categoryType: [''],
      questionSetCreatedFor:['']
    })

  }

  onSelectQuestion(id, event) {
    debugger
    let status = event.checked
    if (status == true) {
      this.selectedQuestions.push(id)
    } else {
      this.selectedQuestions.splice(this.selectedQuestions.indexOf(id), 1)
    }
    this.selectectedQuestionCount = this.selectedQuestions.length

  }

  //********** Search function *******//
  searchQuestionSet(event) {
    debugger;


    //this.dataSourceLiquorBrand.data= this.liquorbrand_data
    const val = event.target.value.trim().toLowerCase();
    const filter = this.allQuestionData.filter(function (d) {
      console.log(d);
      let result

      result = d['label_Name'].toLowerCase().indexOf(val) !== -1
        || d['inputType'].toLowerCase().indexOf(val) !== -1
      return result

    });


    console.log(filter);
    this.allQuestions = filter;
  }

  addQuestionSet() {
    this.createQuestionSetForm.patchValue({
      selectedQuestions: this.selectedQuestions,
      categoryType: this.categoryType,
      questionSetCreatedFor: this.createValue
    })
    console.log(this.createQuestionSetForm.value);


    this.feedbackService.createQuestionSet(this.createQuestionSetForm.value).subscribe(res => {

      console.log("Resonpose data", res);

      if (res['success'] == true) {

        Swal.fire('Question Set Added Successfully', '', 'success')
        this.router.navigate(['feedback'])

      }
      else {
        Swal.fire('Failed to Add Question Set', 'Something went wrong', 'warning')
      }
    })
  }
  openQuestionSetType() {
    debugger
    this.questionsetModalflag = true;


  }


  // ****** click carrot function**********

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  getQuestionList(event) {

    this.feedbackService.getAllFeedbackQuestionList().subscribe(
      data => {
        this.allQuestions = data['data'];
        console.log("allfeedbackquestionlist =", this.allQuestions)
        this.allQuestionData = data['data']
      }
    )
  }




  // *******GET ESTSERVICELIQUER ROOT TYPE*********
  getQuestionRootType() {
    debugger;

    this.feedbackService.getAllQuestionTypeSetFromApi().subscribe(data => {
      console.log('estRootCategory', data);
      if (data) {
        // this.estRootCategory = data
        data['data'].forEach((item, i) => {
          item._id = i + 1;
        });
        this.QuestionTypes = data['data']
        // this.QuestionTypes.push(data['data'][0])
        this.filterQuestionTypes =data['data']
        this.getFeedbackQuestionRootType()
      }
      console.log('This.QuestionTypes with id', this.QuestionTypes)
      //console.log('estRootCategory', this.estRootCategory);

    })

  }

  // *******GET Feedback ROOT TYPE*********
  getFeedbackQuestionRootType() {
    debugger;

    this.feedbackService.getAllFeedbackQueTypeSetFromApi().subscribe(data => {
      console.log('feedback', data);
      if (data) {
        // this.estRootCategory = data
        data['data'].forEach((item, i) => {
          // item._id = i + 1;
        //   const found =  this.QuestionTypes.some(el => el._id === item._id);
        //   if (!found) {  
        //     this.QuestionTypes.push(item);  
        // }
        var index =  this.QuestionTypes.findIndex(el => el._id === item._id)
        // here you can check specific property for an object whether it exist in your array or not
        
        if (index === -1){
          this.QuestionTypes.push(item);
        }
        }); console.log('QuestionTypes getFeedbackQuestionRootType ', this.QuestionTypes);
      //this.QuestionTypes = data['data']
    
     
      }
      console.log('This.QuestionTypes with id', this.QuestionTypes)
      //console.log('estRootCategory', this.estRootCategory);

    })

  }

  setFilterQuestionList(fltrquestType) {
    this.filterQuestionTypes = fltrquestType

  }

 
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.allQuestions, event.previousIndex, event.currentIndex);
  }
  //******BizlyPOS code of add brand******


  // **********open root question dialog**************

  openRootDialog() {
    const dialogRef = this.dialog.open(AddRootQuestionTypeComponent, {
      width: '460px',
      // height: '200px'
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log('After dialog closed');
      setTimeout(() => {    //<<<---    using ()=> syntax
       this.getQuestionRootType()
      }, 2000);

    })
  }

  //********open category type dialog box***************

  openCategoryType(categorytypeID) {
    const dialogRef = this.dialog.open(AddCategoryQueTypeComponent, {
      width: '460px',
      data: categorytypeID,
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'after closing varient drink pop'
      );
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })

  }

  //************open sub category dialog box**************//

  openSubCategoryType(rootQueID, categorytypeID) {
    const dialogRef = this.dialog.open(AddChildCategoryQueTypeComponent, {
      width: '460px',
      data: { "rootQueID": rootQueID, "categorytypeID": categorytypeID },
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })
  }


  // **************open sub sub varient drink dialog box************
  openSubSubcategory(rootQueID, categorytypeID,categorytypename) {
    const dialogRef = this.dialog.open(AddChildchildCategoryQueTypeComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootQueID": rootQueID, "categorytypeID": categorytypeID, "categorytypename": categorytypename }
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })
  }
//**************Question set type filter tree***********//
  changeFlagFilternw(rootCategory,seletedType) {
   debugger
    console.log('change filter data rootCategory', rootCategory)
    console.log('change filter seletedType', seletedType)
     this.questionsetChangeFilter(seletedType);
  }

  questionsetChangeFilter(seletedType?){
    
    this.feedbackService.questionsetFilter(seletedType).subscribe(data => {
console.log("questionsetFilter data",data)
    })
  }

  //**********Question set type tree************//

  changeFlag(data, event, root_level, rootindex?: any, subcategoryIndex?: any, subsubcategoryINdex?: any, typeIndex?: any) {
    //console.log( ">>>" , data, ">>>" , event.checked);
    this.count = 0
    console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
    if (root_level == 'root') {
      this.QuestionTypes

        .forEach((element, index) => {
          if (index == rootindex) {
            element.showChildren = event.checked
            if (
              this.QuestionTypes[index].hasOwnProperty('questionType')
              &&
              this.QuestionTypes[index].questionType.length > 0
            ) {
              this.QuestionTypes[index].questionType.forEach((element, subcategoryIndex = index) => {
                element.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubcategoryINdex = index) => {
                    element.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                        element.showChildren = event.checked;
                      })
                    }


                  })
                }
              })
            }
          }
        });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    }
    else if (root_level == 'sub') {
      this.QuestionTypes.forEach((Rootelement, index) => {
        if (index == rootindex) {
          // Rootelement.showChildren = event.checked
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
              if (subindex == subcategoryIndex) {
                subelement.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((subsubelement, subsubcategoryINdex = index) => {
                    subsubelement.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length

                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((typeelement, liquorSubSubVarientTypeIndex = index) => {
                        typeelement.showChildren = event.checked;
                      })
                    }


                  })
                }
              }

            })

          }


        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    } else if (root_level == 'subsub') {
      this.QuestionTypes.forEach((element, index) => {
        if (index == rootindex) {
          // element.showChildren = event.checked
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                //             element.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      element.showChildren = event.checked;
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                          element.showChildren = event.checked;
                        })
                      }
                    }

                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    } else if (root_level == 'type') {
      this.QuestionTypes.forEach((element, index) => {
        if (index == rootindex) {
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, childIndex) => {
                          if (typeIndex == childIndex)
                            element.showChildren = event.checked;
                        })
                      }


                    }
                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    }
    ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
                &&
                this.QuestionTypes[index].questionType[subindex].categories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {
                  // if(subsubINdex==0){
                  //   this.subsubFlag=0
                  // }
                  if (subsubINdex == subsubcategoryINdex) {
                    if (subsubelement.showChildren == true) {
                      this.subsubFlag++
                    }
                    //console.log("<<<", this.QuestionTypes[index].subCategories[subcategoryIndex].categories.length);
                    //console.log(">>>", this.subsubFlag);


                    if (this.QuestionTypes[index].questionType[subindex].categories.length == this.subsubFlag) {
                      subelement.showChildren = true
                    } else {
                      subelement.showChildren = false;
                    }
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      // //console.log(element);
                      this.typeFlag = 0
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {
                        // if (typeIndex == childIndex) {
                        //   typeelement.showChildren = event.checked;
                        // }
                        if (typeelement.showChildren == true) {
                          this.typeFlag++
                        }
                        console.log("<<<", this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length);
                        console.log(">>>", this.typeFlag);


                        if (this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length == this.typeFlag) {
                          subsubelement.showChildren = true
                        } else {
                          subsubelement.showChildren = false;
                        }

                      })
                    }


                  }
                })
              }
              // }
            }
          })
        }


      }
    });
    //subsub
    this.subsubFlag = 0

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {

              if (
                this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
                &&
                this.QuestionTypes[index].questionType[subindex].categories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {

                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  // //console.log("<<<", this.QuestionTypes[index].subCategories[subcategoryIndex].categories.length);
                  // //console.log(">>>", this.subsubFlag);


                  if (this.QuestionTypes[index].questionType[subindex].categories.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    // this.typeFlag=0
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {



                    })
                  }
                })
              }

            }
          })
        }


      }
    });

    //////subflag
    this.subFlag = 0
    this.subsubFlag = 0

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            // if(subindex==subcategoryIndex){
            if (subelement.showChildren == true) {
              this.subFlag++
            }
            // console.log("<<<", this.QuestionTypes[index].questionType.length);
            // console.log(">>>", this.subFlag);


            if (this.QuestionTypes[index].questionType.length == this.subFlag) {
              Rootelement.showChildren = true
            } else {
              Rootelement.showChildren = false;
            }

            if (
              this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
              &&
              this.QuestionTypes[index].questionType[subindex].categories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {


                if (
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {



                  })
                }
              })
            }

            // }
          })
        }


      }
    });
    console.log(this.QuestionTypes[rootindex]);
    //   if (this.categoryType.length == 0)  {
    //     if (this.QuestionTypes[rootindex].showChildren == true) {
    //       this.categoryType.push(this.QuestionTypes[rootindex])
    //     }
    //   }
    //  else{
    //   if (this.QuestionTypes[rootindex].showChildren == true) {
    //     this.categoryType.push(this.QuestionTypes[rootindex])
    //   }
    //  }
    // 

    // if(this.categoryType.length==0){
    //         this.categoryType.push(this.QuestionTypes[rootindex])
    //       }else{


    //   this.categoryType.forEach(element => {
    //     if(element['_id']==this.QuestionTypes[rootindex]['_id']){
    //       //  element==this.category_data;
    //       this.categoryType.splice(element, 1)
    //        //this.categoryType.splice(element, 1)
    //   //  this.categoryType.push(this.category_data)
    //     }


    //    });
    //    this.categoryType.push(this.QuestionTypes[rootindex])

    //        }
    //        console.log(this.categoryType);
    // this.QuestionTypes.forEach((element, index) => {
    if (this.QuestionTypes[rootindex].showChildren == false) {
      // element.showChildren = event.checked
      if (
        this.QuestionTypes[rootindex].hasOwnProperty('questionType')
        &&
        this.QuestionTypes[rootindex].questionType.length > 0
      ) {
        this.QuestionTypes[rootindex].questionType.forEach((element, subindex) => {
          if (element.showChildren == false) {
            if (
              this.QuestionTypes[rootindex].questionType[subindex].hasOwnProperty('categories')
              &&
              this.QuestionTypes[rootindex].questionType[subindex].categories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.QuestionTypes[rootindex].questionType[subindex].categories.forEach((element, subsubINdex) => {
                if (element.showChildren == false) {
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex) => {
                      if (element.showChildren == true) {
                        this.count = 1;
                        return false
                      }
                    })
                  }


                }
                else {
                  this.count = 1;
                  return false;
                }
              })
            }
          }
          if (element.showChildren == true) {
            this.count = 1;
            return false;
          }
        })
      }
    }
    else {
      this.count = 1
    }

    console.log(this.count);
    if (this.count == 1) {
      if (this.categoryType.length == 0) {
        this.categoryType.push(this.QuestionTypes[rootindex])
      } else {
        this.categoryType.forEach(element => {
          if (element['_id'] == this.QuestionTypes[rootindex]['_id']) {
            element = this.QuestionTypes[rootindex]
            this.id = element['_id'];
          }
        });
        if (this.id != this.QuestionTypes[rootindex]['_id']) {
          this.categoryType.push(this.QuestionTypes[rootindex])
        }
      }
    }
    if (this.count == 0) {
      let i = 0;
      this.categoryType.forEach(element => {

        if (element['_id'] == this.QuestionTypes[rootindex]['_id']) {
          this.categoryType.splice(i, 1)
        } else {
          console.log("not match");

        }
        i++;
      });
    }
    console.log(this.categoryType);
    let newArray = [];
    getAllId(this.QuestionTypes, '_id')



    function getAllId(arr, key) {
      arr.forEach(function (item) {
        for (let keys in item) {
          if (keys === key && item.showChildren === true) {
            newArray.push(item)
          } else if (Array.isArray(item[keys])) {
            getAllId(item[keys], key)
          }
        }

      })
    }
    console.log('new arrrr', newArray)
  }
  

  changeFlagFilter(data, event, root_level, rootindex?: any, subcategoryIndex?: any,
    subsubcategoryINdex?: any, typeIndex?: any) {
    console.log('changeFlagFilter data', data)
    console.log('changeFlagFilter event', event)
    console.log('changeFlagFilter root_level', root_level)
    console.log('changeFlagFilter rootindex', rootindex)
    console.log('changeFlagFilter subcategoryIndex', subcategoryIndex)
    console.log('changeFlagFilter subsubcategoryINdex', subsubcategoryINdex)
    console.log('changeFlagFilter typeIndex', typeIndex);
    //console.log( ">>>" , data, ">>>" , event.checked);
    this.count = 0
    console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
    if (root_level == 'root') {
      this.filterQuestionTypes

        .forEach((element, index) => {
          if (index == rootindex) {
            element.showChildren = event.checked
            if (
              this.filterQuestionTypes[index].hasOwnProperty('questionType')
              &&
              this.filterQuestionTypes[index].questionType.length > 0
            ) {
              this.filterQuestionTypes[index].questionType.forEach((element, subcategoryIndex = index) => {
                element.showChildren = event.checked;
                if (
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubcategoryINdex = index) => {
                    element.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                        element.showChildren = event.checked;
                      })
                    }


                  })
                }
              })
            }

          }
        });
      //console.log('this.filterQuestionTypes >', this.filterQuestionTypes)
    }
    else if (root_level == 'sub') {
      this.filterQuestionTypes.forEach((Rootelement, index) => {
        if (index == rootindex) {
          // Rootelement.showChildren = event.checked
          if (
            this.filterQuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.filterQuestionTypes[index].questionType.length > 0
          ) {
            this.filterQuestionTypes[index].questionType.forEach((subelement, subindex) => {
              if (subindex == subcategoryIndex) {
                subelement.showChildren = event.checked;
                if (
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].categories.forEach((subsubelement, subsubcategoryINdex = index) => {
                    subsubelement.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length

                      this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((typeelement, liquorSubSubVarientTypeIndex = index) => {
                        typeelement.showChildren = event.checked;
                      })
                    }


                  })
                }
              }

            })

          }


        }
      });
      //console.log('this.filterQuestionTypes >', this.filterQuestionTypes)
    } else if (root_level == 'subsub') {
      this.filterQuestionTypes.forEach((element, index) => {
        if (index == rootindex) {
          // element.showChildren = event.checked
          if (
            this.filterQuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.filterQuestionTypes[index].questionType.length > 0
          ) {
            this.filterQuestionTypes[index].questionType.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                //             element.showChildren = event.checked;
                if (
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      element.showChildren = event.checked;
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                          element.showChildren = event.checked;
                        })
                      }
                    }

                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.filterQuestionTypes >', this.filterQuestionTypes)
    } else if (root_level == 'type') {
      this.filterQuestionTypes.forEach((element, index) => {
        if (index == rootindex) {
          if (
            this.filterQuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.filterQuestionTypes[index].questionType.length > 0
          ) {
            this.filterQuestionTypes[index].questionType.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                if (
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.filterQuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.filterQuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, childIndex) => {
                          if (typeIndex == childIndex)
                            element.showChildren = event.checked;
                        })
                      }


                    }
                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.filterQuestionTypes >', this.filterQuestionTypes)
    }
    ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////

    this.filterQuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.filterQuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.filterQuestionTypes[index].questionType.length > 0
        ) {
          this.filterQuestionTypes[index].questionType.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.filterQuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
                &&
                this.filterQuestionTypes[index].questionType[subindex].categories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.filterQuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {
                  // if(subsubINdex==0){
                  //   this.subsubFlag=0
                  // }
                  if (subsubINdex == subsubcategoryINdex) {
                    if (subsubelement.showChildren == true) {
                      this.subsubFlag++
                    }
                    //console.log("<<<", this.filterQuestionTypes[index].subCategories[subcategoryIndex].categories.length);
                    //console.log(">>>", this.subsubFlag);


                    if (this.filterQuestionTypes[index].questionType[subindex].categories.length == this.subsubFlag) {
                      subelement.showChildren = true
                    } else {
                      subelement.showChildren = false;
                    }
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      // //console.log(element);
                      this.typeFlag = 0
                      this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {
                        // if (typeIndex == childIndex) {
                        //   typeelement.showChildren = event.checked;
                        // }
                        if (typeelement.showChildren == true) {
                          this.typeFlag++
                        }
                        console.log("<<<", this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length);
                        console.log(">>>", this.typeFlag);


                        if (this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length == this.typeFlag) {
                          subsubelement.showChildren = true
                        } else {
                          subsubelement.showChildren = false;
                        }

                      })
                    }


                  }
                })
              }
              // }
            }
          })
        }


      }
    });
    //subsub
    this.subsubFlag = 0

    this.filterQuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.filterQuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.filterQuestionTypes[index].questionType.length > 0
        ) {
          this.filterQuestionTypes[index].questionType.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {

              if (
                this.filterQuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
                &&
                this.filterQuestionTypes[index].questionType[subindex].categories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.filterQuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {

                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  // //console.log("<<<", this.filterQuestionTypes[index].subCategories[subcategoryIndex].categories.length);
                  // //console.log(">>>", this.subsubFlag);


                  if (this.filterQuestionTypes[index].questionType[subindex].categories.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    // this.typeFlag=0
                    this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {



                    })
                  }
                })
              }

            }
          })
        }


      }
    });

    //////subflag
    this.subFlag = 0
    this.subsubFlag = 0

    this.filterQuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.filterQuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.filterQuestionTypes[index].questionType.length > 0
        ) {
          this.filterQuestionTypes[index].questionType.forEach((subelement, subindex) => {
            // if(subindex==subcategoryIndex){
            if (subelement.showChildren == true) {
              this.subFlag++
            }
            // console.log("<<<", this.filterQuestionTypes[index].questionType.length);
            // console.log(">>>", this.subFlag);


            if (this.filterQuestionTypes[index].questionType.length == this.subFlag) {
              Rootelement.showChildren = true
            } else {
              Rootelement.showChildren = false;
            }

            if (
              this.filterQuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
              &&
              this.filterQuestionTypes[index].questionType[subindex].categories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.filterQuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {


                if (
                  // tslint:disable-next-line: max-line-length
                  this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.filterQuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {



                  })
                }
              })
            }

            // }
          })
        }


      }
    });
    console.log(this.filterQuestionTypes[rootindex]);
    //   if (this.categoryType.length == 0)  {
    //     if (this.filterQuestionTypes[rootindex].showChildren == true) {
    //       this.categoryType.push(this.filterQuestionTypes[rootindex])
    //     }
    //   }
    //  else{
    //   if (this.filterQuestionTypes[rootindex].showChildren == true) {
    //     this.categoryType.push(this.filterQuestionTypes[rootindex])
    //   }
    //  }
    // 

    // if(this.categoryType.length==0){
    //         this.categoryType.push(this.filterQuestionTypes[rootindex])
    //       }else{


    //   this.categoryType.forEach(element => {
    //     if(element['_id']==this.filterQuestionTypes[rootindex]['_id']){
    //       //  element==this.category_data;
    //       this.categoryType.splice(element, 1)
    //        //this.categoryType.splice(element, 1)
    //   //  this.categoryType.push(this.category_data)
    //     }


    //    });
    //    this.categoryType.push(this.filterQuestionTypes[rootindex])

    //        }
    //        console.log(this.categoryType);
    // this.filterQuestionTypes.forEach((element, index) => {
    if (this.filterQuestionTypes[rootindex].showChildren == false) {
      // element.showChildren = event.checked
      if (
        this.filterQuestionTypes[rootindex].hasOwnProperty('questionType')
        &&
        this.filterQuestionTypes[rootindex].questionType.length > 0
      ) {
        this.filterQuestionTypes[rootindex].questionType.forEach((element, subindex) => {
          if (element.showChildren == false) {
            if (
              this.filterQuestionTypes[rootindex].questionType[subindex].hasOwnProperty('categories')
              &&
              this.filterQuestionTypes[rootindex].questionType[subindex].categories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.filterQuestionTypes[rootindex].questionType[subindex].categories.forEach((element, subsubINdex) => {
                if (element.showChildren == false) {
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.filterQuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.filterQuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.filterQuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex) => {
                      if (element.showChildren == true) {
                        this.count = 1;
                        return false
                      }
                    })
                  }


                }
                else {
                  this.count = 1;
                  return false;
                }
              })
            }
          }
          if (element.showChildren == true) {
            this.count = 1;
            return false;
          }
        })
      }
    }
    else {
      this.count = 1
    }

    console.log(this.count);
    if (this.count == 1) {
      if (this.filterCategoryType.length == 0) {
        this.filterCategoryType.push(this.filterQuestionTypes[rootindex])
      } else {
        this.filterCategoryType.forEach(element => {
          if (element['_id'] == this.filterQuestionTypes[rootindex]['_id']) {
            element = this.filterQuestionTypes[rootindex]
            this.id = element['_id'];
          }
        });
        if (this.id != this.filterQuestionTypes[rootindex]['_id']) {
          this.filterCategoryType.push(this.filterQuestionTypes[rootindex])
        }
      }
    }
    if (this.count == 0) {
      let i = 0;
      this.filterCategoryType.forEach(element => {

        if (element['_id'] == this.filterQuestionTypes[rootindex]['_id']) {
          this.filterCategoryType.splice(i, 1)
        } else {
          console.log("not match");

        }
        i++;
      });
    }
    console.log(this.filterCategoryType);
    this.filterQuestionOnType()

  }
  filterQuestionOnType() {
    console.log('filterCategoryType in functn', this.filterCategoryType);
    const filter = this.allQuestionData.filter(function (d) {
      // console.log(d)

    });
    // let rslt = this.filterCategoryType.forEach( elm =>{
    //   if(elm.showChildren === true){
    //       console.log(elm._id)
    //       return 

    //   }
    // })
    let newArray = [];
    getAllId(this.filterCategoryType, '_id')



    function getAllId(arr, key) {
      arr.forEach(function (item) {
        for (let keys in item) {
          if (keys === key && item.showChildren === true) {
            newArray.push(item[key])
          } else if (Array.isArray(item[keys])) {
            getAllId(item[keys], key)
          }
        }

      })
    }
    console.log('new arrrr', newArray)

    // ********** code 1***************
    // filterQues(this.allQuestionData, 1)
    // let arr = this.allQuestionData;
    //  function filterQues(arr, term) {
    //   var matches = [];
    //   if (!Array.isArray(arr)) return matches;

    //   arr.forEach(function(i) {
    //       if (i.value.includes(term)) {
    //           matches.push(i);
    //       } else {
    //           let childResults = filterQues(i.children, term);
    //           if (childResults.length)
    //               matches.push(Object.assign({}, i, { children: childResults }));
    //       }
    //   })

    //   return matches;
    // }
    // console.log('output fltr' ,filterQues);



    // ********code 2************8

    console.log('this.allQuestionData', this.allQuestionData);

    // function find(obj, key) {
    //   if (obj.value && obj.value.indexOf(key) > -1){
    //     return true;
    //   }
    //   if (obj.children && obj.children.length > 0){
    //     return obj.children.reduce(function(obj1, obj2){
    //       return find(obj1, key) || find(obj2, key);
    //     }, {}); 
    //   } 
    //   return false;
    // }

    // var output = this.allQuestionData.filter(function(obj){
    //      return find(obj, 1);
    //  });
    // console.log('Result', output);






    // code 3 *************
    // var currentPath = [];

    // function depthFirstTraversal(o, fn) {
    //     currentPath.push(o);
    //     if(o.children) {
    //         for(var i = 0, len = o.children.length; i < len; i++) {
    //             depthFirstTraversal(o.children[i], fn);
    //         }
    //     }
    //     fn.call(null, o, currentPath);
    //     currentPath.pop();
    // }

    // function shallowCopy(o) {
    //     var result = {};
    //     for(var k in o) {
    //         if(o.hasOwnProperty(k)) {
    //             result[k] = o[k];
    //         }
    //     }
    //     return result;
    // }

    // function copyNode(node) {
    //     var n = shallowCopy(node);
    //     // if(n.children) { n.children = []; }
    //     // return n;
    // }

    // function filterTree(root, ids) {
    //     root.copied = copyNode(root); // create a copy of root
    //     var filteredResult = root.copied;

    //     depthFirstTraversal(root, function(node, branch) {
    //         // if this is a leaf node _and_ we are looking for its ID
    //         if( !node.children && ids.indexOf(node.id) !== -1 ) {
    //             // use the path that the depthFirstTraversal hands us that
    //             // leads to this leaf.  copy any part of this branch that
    //             // hasn't been copied, at minimum that will be this leaf
    //             for(var i = 0, len = branch.length; i < len; i++) {
    //                 if(branch[i].copied) { continue; } // already copied

    //                 branch[i].copied = copyNode(branch[i]);
    //                 // now attach the copy to the new 'parellel' tree we are building
    //                 branch[i-1].copied.children.push(branch[i].copied);
    //             }
    //         }
    //     });

    //     depthFirstTraversal(root, function(node, branch) {
    //         delete node.copied; // cleanup the mutation of the original tree
    //     });

    //     return filteredResult;
    // }

    // function filterTreeList(list, ids) {
    //     var filteredList = [];
    //     for(var i = 0, len = list.length; i < len; i++) {
    //         filteredList.push( filterTree(list[i], ids) );
    //     }
    //     return filteredList;
    // }

    // var hierarchy = [ /* your data here */ ];
    // var ids = [1,3];

    // var filtered = filterTreeList(this.allQuestionData, newArray);
    // console.log('filtered',filtered);





    // code 4***********
    const deepFilter = (array, indicator) => {
      return array.filter(function iter(o) {
        return Object.keys(o).some(k => {
          if (typeof o[k] === 'string' && o[k].includes(indicator)) {
            return true;
          }
          if (Array.isArray(o[k])) {
            o[k] = o[k].filter(iter);
            return o[k].length;
          }
        });
      });
    }
    let actulreslt
    newArray.forEach(elm => {
      actulreslt = deepFilter(this.allQuestions, "5ec8fbf4c89f8d7fa2be8830")
    })
    // // console.log('fltrrrrrr',deepFilter(this.allQuestionData, "5ec8fbf4c89f8d7fa2be8830"));

    console.log('actulreslt', actulreslt);

    // console.log(res);
    this.allQuestions = filter;
  }

}
