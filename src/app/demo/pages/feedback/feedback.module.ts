import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedbackHomeComponent } from './feedback-home/feedback-home.component';
import { FeedbackListComponent } from './feedback-list/feedback-list.component';
import { Router, Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { CreateFeedbackQuestionComponent } from './create-feedback-question/create-feedback-question.component';
import { CreateTemplateComponent } from './create-template/create-template.component';
import { ViewTemplateFeedbackComponent } from './view-template-feedback/view-template-feedback.component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { ViewQuestionSetComponent } from './view-question-set/view-question-set.component';
import { UpdateFeedbackQuestionComponent } from './update-feedback-question/update-feedback-question.component';
import { ViewFeedbackQuestionComponent } from './view-feedback-question/view-feedback-question.component';
import { FileUploadModule } from "ng2-file-upload";
import { CreateQuestionSetComponent } from './create-question-set/create-question-set.component';
import { UpdateQuestionSetComponent } from './update-question-set/update-question-set.component';
import { UpdateTemplateComponent } from './update-template/update-template.component';

const routes: Routes = [
  {
    path: '',
    component: FeedbackHomeComponent,
    children: [
      {
        path: '',
        component: FeedbackListComponent
      }, {
        path: 'create-feedback-question',
        component: CreateFeedbackQuestionComponent
      }, {
        path: 'update-feedback-question',
        component: UpdateFeedbackQuestionComponent
      }, {
        path: 'view-feedback-question',
        component: UpdateFeedbackQuestionComponent
      }, {
        path: 'create-question-set',
        component: CreateQuestionSetComponent
      }, {
        path: 'update-question-set',
        component: UpdateQuestionSetComponent
      },
      {
        path: 'view-question-set',
        component: UpdateQuestionSetComponent
      }, {
        path: 'create-template',
        component: CreateTemplateComponent
      }, {
        path: 'update-template',
        component: UpdateTemplateComponent
      }, {
        path: 'view-template',
        component: UpdateTemplateComponent
      }
    ]
  }
];

@NgModule({
  declarations: [FeedbackHomeComponent, FeedbackListComponent, CreateFeedbackQuestionComponent, CreateFeedbackQuestionComponent, CreateTemplateComponent,
     ViewTemplateFeedbackComponent, 
     ViewQuestionSetComponent, 
     UpdateFeedbackQuestionComponent, CreateQuestionSetComponent,
     ViewFeedbackQuestionComponent, UpdateQuestionSetComponent, UpdateTemplateComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    NgbDropdownModule,
    FileUploadModule

  ]
})
export class FeedbackModule { }
