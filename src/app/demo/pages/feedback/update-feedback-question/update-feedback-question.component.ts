import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { MatDialog } from '@angular/material';
import { AddAttributeRadioButtonComponent } from '../../menu-page/add-attribute-radio-button/add-attribute-radio-button.component';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';
import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { FeedbackService } from 'src/app/_services/_feedbackServices/feedback.service';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';
import { AddRootQuestionTypeComponent } from 'src/app/PopoversList/add-root-question-type/add-root-question-type.component';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
const URL = environment.base_Url + 'feedback/uploadassociateimage'
import { Router } from '@angular/router';
import { AddCategoryQueTypeComponent } from 'src/app/PopoversList/add-category-que-type/add-category-que-type.component';
import { AddChildCategoryQueTypeComponent } from 'src/app/PopoversList/add-child-category-que-type/add-child-category-que-type.component';
import { AddChildchildCategoryQueTypeComponent } from 'src/app/PopoversList/add-childchild-category-que-type/add-childchild-category-que-type.component';

@Component({
  selector: 'app-update-feedback-question',
  templateUrl: './update-feedback-question.component.html',
  styleUrls: ['./update-feedback-question.component.scss']
})
export class UpdateFeedbackQuestionComponent implements OnInit {
  editQuestionForm: FormGroup
  uploader: FileUploader
  response: string;
  loading = false
  imageurl: any
  SelectResponses: string[] = ['Input Box', 'Yes / No', 'Radio Button']
  radiobutton = false;
  yesorNo = false;
  inputbox = false;
  isLable = false;
  radioOptions = [];
  estRootCategory: any;
  estCateName: string[];
  cateChildName: string[];
  cateChildChildName: string[];
  selectedRootType: any;
  categoryName: any;
  childCategory: any;
  subsubChild: any;
  selectedRootNames = [];
  selectedCatNames = [];
  selectedChildName = [];
  selectedChildChildName = [];
  dummy: any;
  feedbackQuestions: any;
  // bizly pos new varible
  QuestionTypes: any;
  VarientsArray: any
  questionType: any;
  subVarients: any;
  addBrandForm: FormGroup
  dataArray: any;           // all root type liquor
  subsubVarientArray: any;
  userid
  categoryType = [];
  QuestionTree = [];

  category_data: any
  subsubFlag: any;
  typeFlag: number;
  subFlag: number;
  count = 0;
  id: any;
  radioButtondyn: FormGroup;
  radioButtons: any;
  queObj: any;
  qestionID: any;
  questionId: any;
  radioButton: any;
  isViewOnly: boolean = false;
  createValue: any;
  constructor(private fb: FormBuilder, private addMenuFoodsService: AddMenuFoodsService, private dialog: MatDialog,
    private restServ: RestoService, private router: Router, private feedbackService: FeedbackService, private http: HttpClient
  ) {

    this.createValue = this.feedbackService.getSelectedValue();
    // get establishment root category 
    this.dummy = 'Establishment'
    debugger;
    this.queObj = this.feedbackService.editObject
    console.log("categoryType from edit", this.queObj);


    this.categoryType = this.queObj['categoryType']
    this.getQuestionRootType();

    this.questionId = this.feedbackService.getFeedbackQuestionId();

    console.log("questionId", this.questionId['queId']);
    console.log("screenFlag", this.questionId['screenFlag']);
    if (this.questionId['screenFlag'] === 'view') {
      this.isViewOnly = true;
    }
    else {
      this.isViewOnly = false
    }


    this.editActivity();
    this.feedbackService.getFeedbackQuestion(this.questionId['queId']).subscribe(res => {
      console.log("response is=", res);
      // this.queObj = res['data'];
      // this.categoryType=this.queObj['categoryType']

      // this.editQuestion();
    })
    this.editQuestionForm.patchValue({
      label_Name: this.queObj.label_Name,
      inputType: this.queObj.inputType,
      inputPlaceholderName: this.queObj.inputPlaceholderName,
      YesNoButtons: this.queObj.YesNoButtons,
      radioButtons: this.queObj.radioButtons,
      categoryType: this.queObj.categoryType,


    })
    this.editQuestion();

    //  this.getExistingQusetion(this.questionId['queId']);
  }



  ngOnInit() {
    debugger;


    //   this.questionId = this.feedbackService.getFeedbackQuestionId();

    //  console.log("questionId", this.questionId['queId']);
    //  console.log("screenFlag", this.questionId['screenFlag']);
    //  if(this.questionId['screenFlag']==='view'){
    //   this.isViewOnly = true;
    //  }
    //  else{
    //   this.isViewOnly = false
    //  }
    //  this.editActivity();
    //   this.getExistingQusetion(this.questionId['queId']);


    //  NG FILE UPLOADER CODE
    debugger;
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'associatedimage'

    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      console.log('FileUpload:uploaded: resopnse', response);
      //  const a = this.editQuestionForm.get("radioButtons") as FormArray;
      // this.editQuestionForm.patchValue({
      //   associateImage: response
      // })


      this.radioButton.patchValue({
        associateImage: response
      })
      console.log("addRadioButtons", this.editQuestionForm.value);

    }
  }


  getExistingQusetion(questionId) {
    debugger;
    this.feedbackService.getFeedbackQuestion(questionId).subscribe(res => {
      console.log("response is=", res);
      this.queObj = res['data'];
      this.categoryType = this.queObj['categoryType']

      this.editQuestion();
    })

  }

  editActivity() {
    debugger;
    this.editQuestionForm = this.fb.group({
      label_Name: [{ value: '', disabled: this.isViewOnly }, [Validators.required]],
      inputType: [{ value: '', disabled: this.isViewOnly }, Validators.required],
      inputPlaceholderName: [{ value: '', disabled: this.isViewOnly }],
      YesNoButtons: [{ value: '', disabled: this.isViewOnly }],
      radioButtons: this.fb.array([
        //  this.addRadioButtonsFromGroup()
      ]),
      //radioButtons: [''],
      categoryType: [''],
      feedbackQuestionCreatedFor:['']


    })
  }

  editQuestion() {
    debugger;
    // this.editQuestionForm.patchValue({
    //   label_Name: this.queObj.label_Name,
    //   inputType: this.queObj.inputType,
    //   inputPlaceholderName: this.queObj.inputPlaceholderName,
    //   YesNoButtons: this.queObj.YesNoButtons,
    //   //radioButtons: this.queObj.radioButtons,
    //   // categoryType:this.queObj.categoryType

    // })
    this.onResponse(this.queObj.inputType);

    console.log('editQuestionFormi in edit ques', this.editQuestionForm.value)
    this.viewSetHtml(this.queObj.radioButtons);
    this.qestionID = this.queObj._id
  }


  get radioButtonForm() {
    return this.editQuestionForm.controls.radioButtons as FormArray
  }

  addRadioButton() {
    this.radioButton = this.fb.group({
      optionLable: [{ value: '', disabled: this.isViewOnly }, Validators.required],
      associateImage: ['']
    })

    this.radioButtonForm.push(this.radioButton)
  }
  remove(i) {
    debugger;
    console.log("i", i)
    const remCont = <FormArray>this.editQuestionForm.controls['radioButtons'];
    console.log("remcont", remCont);
    remCont.removeAt(i);
  }

  onResponse(response) {
    debugger
    if (response == "Input Box") {
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true

    }
    if (response == "Yes / No") {
      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false

    }
    if (response == "Radio Button") {
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false


    }
    this.isLable = true;

  }

  onClickResponse(response) {
    debugger;
    console.log(response);
    this.radioOptions = [];
    this.editQuestionForm.get('inputPlaceholderName').reset();
    this.editQuestionForm.get('YesNoButtons').reset();
    //  this.editQuestionForm.get('radioButtons').reset();

    if (response == "Input Box") {
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true

    }
    if (response == "Yes / No") {
      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false

    }
    if (response == "Radio Button") {
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      // this.addRadioButton();
      this.addRadioButton()
    }
    this.isLable = true;

  }

  onkeyup(event) {
    console.log("onBlur event", event)
    if (event.target.value.length === 1) {
      this.addRadioButton();
    }
    // this.addRadioButton();
  }
  onSelectFile(event, index) {
    debugger;
    if (event.target.files) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        console.log("event target", event.target);
        this.imageurl = event.target['result'];
        console.log("event imageurl", this.imageurl);

      }
      // var multiReader :any;
      // var filesAmount = event.target.files.length;
      // const control = <FormArray>this.addQuestionForm.controls.radioButtons['controls'][index]['controls']['associateImage']['controls']
      //  for (let i = 0; i < filesAmount; i++) {
      //         multiReader = new FileReader;
      //         multiReader.onload = (event) => {
      //           console.log(multiReader.result);
      //           control.push(this.fb.control(multiReader.result));

      //         }
      //         multiReader.readAsDataURL(event.target.files[i]);    


      //         // event.srcElement.value = null;
      //  console.log('this.addQuestionForm[ img ctroll',control)
      //       }

    }
  }



  viewSetHtml(radioOptions) {
    console.log('this.editQuestionForm viewSetHtml', this.editQuestionForm);
    this.editQuestionForm.setControl('radioButtons', this.setExistingDetails(radioOptions));
    console.log('this.viewStudentListForm viewSetHtml', this.editQuestionForm)
  }
  setExistingDetails(radioButtons: any[]): FormArray {
    const radioButtonFormArray = new FormArray([]);
    radioButtons.forEach(radio => {
      radioButtonFormArray.push(
        this.fb.group({
          optionLable: radio.optionLable,
          associateImage: radio.associateImage,

        }));
    });
    console.log('detailsFormArray setExistin', radioButtonFormArray)
    return radioButtonFormArray;
  }
  // get radioButtonForm() {
  //   return this.editQuestionForm.controls.radioButtons as FormArray
  // }
  addRadioButtonsFromGroup(): FormGroup {
    return this.fb.group({
      optionLable: [{ value: '', disabled: this.isViewOnly }, Validators.required],
      associateImage: [{ value: '', disabled: this.isViewOnly }]
    });
    // this.radioButtonForm.push(this.radioButtondyn)
  }



  updateQuestion() {
    debugger;
    console.log('this.editQuestionForm.value in submit before patch', this.editQuestionForm.value);

    this.editQuestionForm.patchValue({
      categoryType: this.categoryType,
      feedbackQuestionCreatedFor: this.createValue

    })

    console.log('this.editQuestionForm.value in submit', this.editQuestionForm.value);

    this.feedbackService.updateFeedbackQuestion(this.questionId['queId'], this.editQuestionForm.value).subscribe(res => {

      console.log("Resonpose data", res);

      if (res['success'] == true) {

        Swal.fire('Question Updated Successfully', '', 'success')
        this.router.navigate(['feedback'])

      }
      else {
        Swal.fire('Failed to update Question', 'Something went wrong', 'warning')
      }
    })
  }
  // ****** click carrot function**********

  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;


    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }
  //******BizlyPOS code of add brand******


  // *******GET ESTSERVICELIQUER ROOT TYPE*********
  getQuestionRootType() {
    debugger;
this.QuestionTypes=[];
this.QuestionTree =[]
    this.feedbackService.getAllQuestionTypeSetFromApi().subscribe(data => {
      console.log('estRootCategory', data);
      if (data) {
        // this.estRootCategory = data
        data['data'].forEach((item, i) => {
          item._id = i + 1;
        });
        this.QuestionTypes = data['data']
        // this.QuestionTypes.push(data['data'][0])
        this.getFeedbackQuestionRootType()
      }
      console.log('This.QuestionTypes with id', this.QuestionTypes)
      //console.log('estRootCategory', this.estRootCategory);
      //console.log('estRootC console.log('QuestionTypes', this.QuestionTypes);
      // this.QuestionTypes.forEach(element => {
      //   this.categoryType.forEach(ele => {
      //     if (element['_id'] == ele['_id'])
      //       element = ele

      //   })
      //   this.QuestionTree.push(element)
      // });
      // this.QuestionTypes = this.QuestionTree
      console.log(this.categoryType);
    })

  }

  // *******GET Feedback ROOT TYPE*********
  getFeedbackQuestionRootType() {
    debugger;

    this.feedbackService.getAllFeedbackQueTypeSetFromApi().subscribe(data => {
      console.log('feedback', data);
      if (data) {
        // this.estRootCategory = data
        data['data'].forEach((item, i) => {
          // item._id = i + 1;
        //   const found =  this.QuestionTypes.some(el => el._id === item._id);
        //   if (!found) {  
        //     this.QuestionTypes.push(item);  
        // }
        var index =  this.QuestionTypes.findIndex(el => el._id === item._id)
        // here you can check specific property for an object whether it exist in your array or not
        
        if (index === -1){
          this.QuestionTypes.push(item);
        }
        });  console.log('QuestionTypes getFeedbackQuestionRootType ', this.QuestionTypes);
        //this.QuestionTypes = data['data']
        this.QuestionTypes.forEach(element => {
          this.categoryType.forEach(ele => {
            if (element['_id'] == ele['_id'])
              element = ele
  
          })

          
          this.QuestionTree.push(element)
        });
        this.QuestionTypes = this.QuestionTree

      }
      console.log('This.QuestionTypes with id', this.QuestionTypes)
      //console.log('estRootCategory', this.estRootCategory);

    })

  }
  // **********open root question dialog**************

  openRootDialog() {
    const dialogRef = this.dialog.open(AddRootQuestionTypeComponent, {
      width: '460px',
      // height: '200px'
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log('After dialog closed');
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);

    })
  }

  //********open category type dialog box***************

  openCategoryType(categorytypeID) {
    debugger
    const dialogRef = this.dialog.open(AddCategoryQueTypeComponent, {
      width: '460px',
      data: categorytypeID,
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      console.log(
        'after closing varient drink pop'
      );
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })

  }

  //************open sub category dialog box**************//

  openSubCategoryType(rootQueID, categorytypeID) {
    const dialogRef = this.dialog.open(AddChildCategoryQueTypeComponent, {
      width: '460px',
      data: { "rootQueID": rootQueID, "categorytypeID": categorytypeID },
      disableClose: true
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })
  }


  // **************open sub sub varient drink dialog box************
  openSubSubcategory(rootQueID, categorytypeID, liquorSubVarient) {
    const dialogRef = this.dialog.open(AddChildchildCategoryQueTypeComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootQueID": rootQueID, "categorytypeID": categorytypeID, "liquorSubVarient": liquorSubVarient }
    })
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getQuestionRootType()
      }, 2000);
    })
  }



  changeFlag(data, event, root_level, rootindex?: any, subcategoryIndex?: any, subsubcategoryINdex?: any, typeIndex?: any) {
    //console.log( ">>>" , data, ">>>" , event.checked);
    this.count = 0
    console.log(rootindex, "===", subcategoryIndex, "======  ", subsubcategoryINdex, "===", typeIndex, "====", root_level);
    if (root_level == 'root') {
      this.QuestionTypes

        .forEach((element, index) => {
          if (index == rootindex) {
            element.showChildren = event.checked
            if (
              this.QuestionTypes[index].hasOwnProperty('questionType')
              &&
              this.QuestionTypes[index].questionType.length > 0
            ) {
              this.QuestionTypes[index].questionType.forEach((element, subcategoryIndex = index) => {
                element.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubcategoryINdex = index) => {
                    element.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      //console.log(element);
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                        element.showChildren = event.checked;
                      })
                    }


                  })
                }
              })
            }
          }
        });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    }
    else if (root_level == 'sub') {
      this.QuestionTypes.forEach((Rootelement, index) => {
        if (index == rootindex) {
          // Rootelement.showChildren = event.checked
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
              if (subindex == subcategoryIndex) {
                subelement.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((subsubelement, subsubcategoryINdex = index) => {
                    subsubelement.showChildren = event.checked;
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('liquorSubSubVarientType')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length

                      this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((typeelement, liquorSubSubVarientTypeIndex = index) => {
                        typeelement.showChildren = event.checked;
                      })
                    }


                  })
                }
              }

            })

          }


        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    } else if (root_level == 'subsub') {
      this.QuestionTypes.forEach((element, index) => {
        if (index == rootindex) {
          // element.showChildren = event.checked
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                //             element.showChildren = event.checked;
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      element.showChildren = event.checked;
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                          element.showChildren = event.checked;
                        })
                      }
                    }

                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    } else if (root_level == 'type') {
      this.QuestionTypes.forEach((element, index) => {
        if (index == rootindex) {
          if (
            this.QuestionTypes[index].hasOwnProperty('questionType')
            &&
            this.QuestionTypes[index].questionType.length > 0
          ) {
            this.QuestionTypes[index].questionType.forEach((element, subindex) => {
              if (subindex == subcategoryIndex) {
                if (
                  this.QuestionTypes[index].questionType[subcategoryIndex].hasOwnProperty('categories')
                  &&
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subcategoryIndex].categories.forEach((element, subsubINdex) => {
                    if (subsubINdex == subsubcategoryINdex) {
                      if (
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].hasOwnProperty('childCategories')
                        &&
                        // tslint:disable-next-line: max-line-length
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.length > 0
                      ) {
                        // tslint:disable-next-line: max-line-length
                        //console.log(element);
                        this.QuestionTypes[index].questionType[subcategoryIndex].categories[subsubcategoryINdex].childCategories.forEach((element, childIndex) => {
                          if (typeIndex == childIndex)
                            element.showChildren = event.checked;
                        })
                      }


                    }
                  })
                }
                // }
              }
            })
          }


        }
      });
      //console.log('this.QuestionTypes >', this.QuestionTypes)
    }
    ////////////////////////////child parent dependency/////////////////////////////////////////////////////////////////////////////

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {
              if (
                this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
                &&
                this.QuestionTypes[index].questionType[subindex].categories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {
                  // if(subsubINdex==0){
                  //   this.subsubFlag=0
                  // }
                  if (subsubINdex == subsubcategoryINdex) {
                    if (subsubelement.showChildren == true) {
                      this.subsubFlag++
                    }
                    //console.log("<<<", this.QuestionTypes[index].subCategories[subcategoryIndex].categories.length);
                    //console.log(">>>", this.subsubFlag);


                    if (this.QuestionTypes[index].questionType[subindex].categories.length == this.subsubFlag) {
                      subelement.showChildren = true
                    } else {
                      subelement.showChildren = false;
                    }
                    if (
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                      &&
                      // tslint:disable-next-line: max-line-length
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                    ) {
                      // tslint:disable-next-line: max-line-length
                      // //console.log(element);
                      this.typeFlag = 0
                      this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {
                        // if (typeIndex == childIndex) {
                        //   typeelement.showChildren = event.checked;
                        // }
                        if (typeelement.showChildren == true) {
                          this.typeFlag++
                        }
                        console.log("<<<", this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length);
                        console.log(">>>", this.typeFlag);


                        if (this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length == this.typeFlag) {
                          subsubelement.showChildren = true
                        } else {
                          subsubelement.showChildren = false;
                        }

                      })
                    }


                  }
                })
              }
              // }
            }
          })
        }


      }
    });
    //subsub
    this.subsubFlag = 0

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            if (subindex == subcategoryIndex) {

              if (
                this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
                &&
                this.QuestionTypes[index].questionType[subindex].categories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length

                this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {

                  if (subsubelement.showChildren == true) {
                    this.subsubFlag++
                  }
                  // //console.log("<<<", this.QuestionTypes[index].subCategories[subcategoryIndex].categories.length);
                  // //console.log(">>>", this.subsubFlag);


                  if (this.QuestionTypes[index].questionType[subindex].categories.length == this.subsubFlag) {
                    subelement.showChildren = true
                  } else {
                    subelement.showChildren = false;
                  }
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    // //console.log(element);
                    // this.typeFlag=0
                    this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {



                    })
                  }
                })
              }

            }
          })
        }


      }
    });

    //////subflag
    this.subFlag = 0
    this.subsubFlag = 0

    this.QuestionTypes.forEach((Rootelement, index) => {
      if (index == rootindex) {
        if (
          this.QuestionTypes[index].hasOwnProperty('questionType')
          &&
          this.QuestionTypes[index].questionType.length > 0
        ) {
          this.QuestionTypes[index].questionType.forEach((subelement, subindex) => {
            // if(subindex==subcategoryIndex){
            if (subelement.showChildren == true) {
              this.subFlag++
            }
            // console.log("<<<", this.QuestionTypes[index].questionType.length);
            // console.log(">>>", this.subFlag);


            if (this.QuestionTypes[index].questionType.length == this.subFlag) {
              Rootelement.showChildren = true
            } else {
              Rootelement.showChildren = false;
            }

            if (
              this.QuestionTypes[index].questionType[subindex].hasOwnProperty('categories')
              &&
              this.QuestionTypes[index].questionType[subindex].categories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length

              this.QuestionTypes[index].questionType[subindex].categories.forEach((subsubelement, subsubINdex) => {


                if (
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  // //console.log(element);
                  // this.typeFlag=0
                  this.QuestionTypes[index].questionType[subindex].categories[subsubINdex].childCategories.forEach((typeelement, childIndex) => {



                  })
                }
              })
            }

            // }
          })
        }


      }
    });
    console.log(this.QuestionTypes[rootindex]);
    //   if (this.categoryType.length == 0)  {
    //     if (this.QuestionTypes[rootindex].showChildren == true) {
    //       this.categoryType.push(this.QuestionTypes[rootindex])
    //     }
    //   }
    //  else{
    //   if (this.QuestionTypes[rootindex].showChildren == true) {
    //     this.categoryType.push(this.QuestionTypes[rootindex])
    //   }
    //  }
    // 

    // if(this.categoryType.length==0){
    //         this.categoryType.push(this.QuestionTypes[rootindex])
    //       }else{


    //   this.categoryType.forEach(element => {
    //     if(element['_id']==this.QuestionTypes[rootindex]['_id']){
    //       //  element==this.category_data;
    //       this.categoryType.splice(element, 1)
    //        //this.categoryType.splice(element, 1)
    //   //  this.categoryType.push(this.category_data)
    //     }


    //    });
    //    this.categoryType.push(this.QuestionTypes[rootindex])

    //        }
    //        console.log(this.categoryType);
    // this.QuestionTypes.forEach((element, index) => {
    if (this.QuestionTypes[rootindex].showChildren == false) {
      // element.showChildren = event.checked
      if (
        this.QuestionTypes[rootindex].hasOwnProperty('questionType')
        &&
        this.QuestionTypes[rootindex].questionType.length > 0
      ) {
        this.QuestionTypes[rootindex].questionType.forEach((element, subindex) => {
          if (element.showChildren == false) {
            if (
              this.QuestionTypes[rootindex].questionType[subindex].hasOwnProperty('categories')
              &&
              this.QuestionTypes[rootindex].questionType[subindex].categories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.QuestionTypes[rootindex].questionType[subindex].categories.forEach((element, subsubINdex) => {
                if (element.showChildren == false) {
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].hasOwnProperty('childCategories')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].childCategories.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.QuestionTypes[rootindex].questionType[subindex].categories[subsubINdex].childCategories.forEach((element, liquorSubSubVarientTypeIndex) => {
                      if (element.showChildren == true) {
                        this.count = 1;
                        return false
                      }
                    })
                  }


                }
                else {
                  this.count = 1;
                  return false;
                }
              })
            }
          }
          if (element.showChildren == true) {
            this.count = 1;
            return false;
          }
        })
      }
    }
    else {
      this.count = 1
    }

    console.log(this.count);
    if (this.count == 1) {
      if (this.categoryType.length == 0) {
        this.categoryType.push(this.QuestionTypes[rootindex])
      } else {
        this.categoryType.forEach(element => {
          if (element['_id'] == this.QuestionTypes[rootindex]['_id']) {
            element = this.QuestionTypes[rootindex]
            this.id = element['_id'];
          }
        });
        if (this.id != this.QuestionTypes[rootindex]['_id']) {
          this.categoryType.push(this.QuestionTypes[rootindex])
        }
      }
    }
    if (this.count == 0) {
      let i = 0;
      this.categoryType.forEach(element => {

        if (element['_id'] == this.QuestionTypes[rootindex]['_id']) {
          this.categoryType.splice(i, 1)
        } else {
          console.log("not match");

        }
        i++;
      });
    }
    console.log(this.categoryType);

  }



}