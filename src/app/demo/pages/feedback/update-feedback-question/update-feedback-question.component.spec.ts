import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFeedbackQuestionComponent } from './update-feedback-question.component';

describe('UpdateFeedbackQuestionComponent', () => {
  let component: UpdateFeedbackQuestionComponent;
  let fixture: ComponentFixture<UpdateFeedbackQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFeedbackQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFeedbackQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
