import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerOrdersHomeComponent } from './customer-orders-home/customer-orders-home.component';
import { CustomerOrdersListComponent } from './customer-orders-list/customer-orders-list.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { MaterialModule } from 'src/app/material/material.module';
import { ViewCustomerOrderComponent } from './view-customer-order/view-customer-order.component';
import { ModifyCustomerOrderComponent } from './modify-customer-order/modify-customer-order.component';
import { CancelCustomerOrderComponent } from './cancel-customer-order/cancel-customer-order.component';

const routes: Routes =[
  {
    path: '',
    component: CustomerOrdersHomeComponent,
    children:[
      {
        path : '',
        component : CustomerOrdersListComponent
      },{
        path: 'view-order',
        component: ViewCustomerOrderComponent
      },{
        path: 'modify-order',
        component: ModifyCustomerOrderComponent
      },{
        path: 'cancel-order',
        component: CancelCustomerOrderComponent
      }
    ]
  }
]

@NgModule({
  declarations: [ CustomerOrdersHomeComponent, CustomerOrdersListComponent, ViewCustomerOrderComponent, ModifyCustomerOrderComponent, CancelCustomerOrderComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    DataTablesModule
  
  ]
})
export class CustomerOrdersModule { }
