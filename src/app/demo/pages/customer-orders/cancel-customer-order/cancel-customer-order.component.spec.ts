import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelCustomerOrderComponent } from './cancel-customer-order.component';

describe('CancelCustomerOrderComponent', () => {
  let component: CancelCustomerOrderComponent;
  let fixture: ComponentFixture<CancelCustomerOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancelCustomerOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancelCustomerOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
