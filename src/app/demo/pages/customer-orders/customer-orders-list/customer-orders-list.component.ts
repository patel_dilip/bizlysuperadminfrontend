import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { FormGroup, FormBuilder } from '@angular/forms';

export interface ordersElement {
  orderDateAndTime: string;
  customerName: string;
  restaurantName: string;
  restaurantRepresentativeNumber: string;
  status: string;
  orderAmount: string;
  orderNumber: string;
  orderType: string;
}

const ordersDATA: ordersElement[] = [
  { "orderDateAndTime": "22/01/2020 -11:46 AM", "customerName": "ABC", "restaurantName": "XYZ", "restaurantRepresentativeNumber": "9999999999", "status": "Order Receive", "orderAmount": "345", "orderNumber": "O554", "orderType": "Online" },
  { "orderDateAndTime": "22/01/2020 -11:46 AM", "customerName": "ABC", "restaurantName": "XYZ", "restaurantRepresentativeNumber": "9999999999", "status": "Order Receive", "orderAmount": "345", "orderNumber": "O567", "orderType": "QR code Dine-in" },
  { "orderDateAndTime": "22/01/2020 -11:46 AM", "customerName": "ABC", "restaurantName": "XYZ", "restaurantRepresentativeNumber": "9999999999", "status": "Order Receive", "orderAmount": "345", "orderNumber": "O765", "orderType": "QR Code Take-Away" },
  { "orderDateAndTime": "22/01/2020 -11:46 AM", "customerName": "ABC", "restaurantName": "XYZ", "restaurantRepresentativeNumber": "9999999999", "status": "Order Receive", "orderAmount": "345", "orderNumber": "O534", "orderType": "Online" },
  { "orderDateAndTime": "22/01/2020 -11:46 AM", "customerName": "ABC", "restaurantName": "XYZ", "restaurantRepresentativeNumber": "9999999999", "status": "Order Receive", "orderAmount": "345", "orderNumber": "O536", "orderType": "Online" },
  { "orderDateAndTime": "22/01/2020 -11:46 AM", "customerName": "ABC", "restaurantName": "XYZ", "restaurantRepresentativeNumber": "9999999999", "status": "Order Receive", "orderAmount": "345", "orderNumber": "O58798", "orderType": "Online" },
  { "orderDateAndTime": "22/01/2020 -11:46 AM", "customerName": "ABC", "restaurantName": "XYZ", "restaurantRepresentativeNumber": "9999999999", "status": "Order Receive", "orderAmount": "345", "orderNumber": "O23", "orderType": "Online" },
  { "orderDateAndTime": "22/01/2020 -11:46 AM", "customerName": "ABC", "restaurantName": "XYZ", "restaurantRepresentativeNumber": "9999999999", "status": "Order Receive", "orderAmount": "345", "orderNumber": "O554456", "orderType": "Online" },
  { "orderDateAndTime": "22/01/2020 -11:46 AM", "customerName": "ABC", "restaurantName": "XYZ", "restaurantRepresentativeNumber": "9999999999", "status": "Order Receive", "orderAmount": "345", "orderNumber": "O4574", "orderType": "Online" },
  { "orderDateAndTime": "22/01/2020 -11:46 AM", "customerName": "ABC", "restaurantName": "XYZ", "restaurantRepresentativeNumber": "9999999999", "status": "Order Receive", "orderAmount": "345", "orderNumber": "O346", "orderType": "Online" },

]

@Component({
  selector: 'app-customer-orders-list',
  templateUrl: './customer-orders-list.component.html',
  styleUrls: ['./customer-orders-list.component.scss']
})
export class CustomerOrdersListComponent implements OnInit {
  ordersForm: FormGroup
  OrderTypes = ["Latest Orders","Modified Orders", "Cancelled Orders"]
  //ordersDATA: ordersElement, 
  displayOrdersColumns: string[] = ["orderDateAndTime", "customerName", "restaurantName", "restaurantRepresentativeNumber", "status", "orderAmount", "orderNumber", "orderType", "action"]
  ordersDataSource: MatTableDataSource<ordersElement>
  @ViewChild("ordersPaginator", { static: true }) ordersPaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) ordersSort: MatSort

  constructor(private fb: FormBuilder) {
    this.ordersDataSource = new MatTableDataSource(ordersDATA)
  }

  ngOnInit() {
this.ordersForm= this.fb.group({
  orderType:[''], 
  order:['']
})

    this.ordersDataSource.paginator = this.ordersPaginator;
    this.ordersDataSource.sort = this.ordersSort;

  }


  applyFilter(filterValue: string) {
    this.ordersDataSource.filter = filterValue.trim().toLowerCase();
    if (this.ordersDataSource.paginator) {
      this.ordersDataSource.paginator.firstPage();
    }
  }


}
