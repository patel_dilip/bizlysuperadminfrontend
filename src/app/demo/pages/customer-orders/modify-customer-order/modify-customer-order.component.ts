import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-modify-customer-order',
  templateUrl: './modify-customer-order.component.html',
  styleUrls: ['./modify-customer-order.component.scss']
})
export class ModifyCustomerOrderComponent implements OnInit {

  Quantitys=['1','2','3','4','5','6']
  
  orders = [{ "foodName": "Chicken Tikka", "Quantity": "2", "price": "353" },
  { "foodName": "Chicken masala", "Quantity": "6", "price": "500" },
  { "foodName": "Boil Chicken ", "Quantity": "1", "price": "654" }]

  customerOrder = {
    "orderNumber": "O6543", "customerName": "ABC", "customerContactNumber": "98765432",
    "restaurantRepresentativeNumber": "7654876543", "orderDate": "22/01/2020", "orderTime": "02:26 PM",
    "restaurantName": "XYZ", "orderAmount": "6445",
    "order": [
      { "foodName": "Chicken Tikka", "Quantity": "2", "price": "443" },
      { "foodName": "Chicken Tikka", "Quantity": "2", "price": "443" },
      { "foodName": "Chicken Tikka", "Quantity": "2", "price": "443" }
    ]
  }
  orderForm: FormGroup
  reasons = ["a", "b", "c"]
  
  constructor(private fb: FormBuilder) { 
    this.order()
    this.orderForm.patchValue({
      orderNumber: this.customerOrder.orderNumber,
      customerName: this.customerOrder.customerName,
      customerContactNumber: this.customerOrder.customerContactNumber,
      restaurantRepresentativeNumber: this.customerOrder.restaurantRepresentativeNumber,
      orderDate: this.customerOrder.orderDate,
      orderTime: this.customerOrder.orderTime,
      restaurantName: this.customerOrder.restaurantName,
      orderAmount: this.customerOrder.orderAmount
    })
  }

  ngOnInit() {
  }
  order() {

    this.orderForm = this.fb.group({
      reason: [''],
      orderDate: [''],
      orderTime: [''],
      customerName: [''],
      customerContactNumber: [''],
      restaurantName: [''],
      restaurantRepresentativeNumber: [''],
      status: [''],
      orderAmount: [''],
      orderNumber: [''],
      orderType: [''],
      orders: ['']

    })
  }

}
