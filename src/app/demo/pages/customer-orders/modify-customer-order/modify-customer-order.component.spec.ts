import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyCustomerOrderComponent } from './modify-customer-order.component';

describe('ModifyCustomerOrderComponent', () => {
  let component: ModifyCustomerOrderComponent;
  let fixture: ComponentFixture<ModifyCustomerOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyCustomerOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyCustomerOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
