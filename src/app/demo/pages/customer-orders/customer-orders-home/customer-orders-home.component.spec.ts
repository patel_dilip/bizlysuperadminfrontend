import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerOrdersHomeComponent } from './customer-orders-home.component';

describe('CustomerOrdersHomeComponent', () => {
  let component: CustomerOrdersHomeComponent;
  let fixture: ComponentFixture<CustomerOrdersHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerOrdersHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerOrdersHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
