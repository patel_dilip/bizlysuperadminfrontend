import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntegrationPageComponent } from '../integration-page.component';
import { Routes, RouterModule } from '@angular/router';
import { IntegrationListComponent } from '../integration-list/integration-list.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material/material.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { ViewOutletComponent } from '../view-outlet/view-outlet.component';
import { ViewChainsComponent } from '../view-chains/view-chains.component';
import { ViewChainsHistoryComponent } from '../view-chains-history/view-chains-history.component';


const routes: Routes = [
  {
    path: '',
    component: IntegrationPageComponent,
    children: [
      {
        path  :'',
        component :IntegrationListComponent
      },{
        path : 'view-outlet',
        component : ViewOutletComponent
      },{
        path: 'view-chains',
        component: ViewChainsComponent
      },{
        path: "view-chains-history",
        component: ViewChainsHistoryComponent
      }
    ]
  }
];


@NgModule({
  declarations: [IntegrationListComponent,IntegrationPageComponent, ViewOutletComponent, ViewChainsComponent, ViewChainsHistoryComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule,
    SharedModule,
    FormsModule,
    DataTablesModule
  
  ]
})


export class IntegrationModule { }
