import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-chains-history',
  templateUrl: './view-chains-history.component.html',
  styleUrls: ['./view-chains-history.component.scss']
})
export class ViewChainsHistoryComponent implements OnInit {


  integrations=[{"integrations":"Swiggy","integrationActivationDate":"20/01/2020","integrationActivatedBy":"Akshay","integrationDeactivationDate":"20/01/2020","integrationDeactivatedBy":"Akshay","lastActivated":"1/1/2020","lastDeactivated":"31/12/2019"},
  {"integrations":"FaceBook","integrationActivationDate":"20/01/2020","integrationActivatedBy":"Akshay","integrationDeactivationDate":"20/01/2020","integrationDeactivatedBy":"Akshay","lastActivated":"1/1/2020","lastDeactivated":"31/12/2019"},
  {"integrations":"Bizly Dine in QR Code","integrationActivationDate":"20/01/2020","integrationActivatedBy":"Akshay","integrationDeactivationDate":"20/01/2020","integrationDeactivatedBy":"Akshay","lastActivated":"1/1/2020","lastDeactivated":"31/12/2019"},
  {"integrations":"Bizly Online Delivery","integrationActivationDate":"20/01/2020","integrationActivatedBy":"Akshay","integrationDeactivationDate":"20/01/2020","integrationDeactivatedBy":"Akshay","lastActivated":"1/1/2020","lastDeactivated":"31/12/2019"},
  {"integrations":"Bizly QR Code Take Away","integrationActivationDate":"20/01/2020","integrationActivatedBy":"Akshay","integrationDeactivationDate":"20/01/2020","integrationDeactivatedBy":"Akshay","lastActivated":"1/1/2020","lastDeactivated":"31/12/2019"},
  
 ]

  constructor() { }

  ngOnInit() {
  }

}
