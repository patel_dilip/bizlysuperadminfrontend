import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChainsHistoryComponent } from './view-chains-history.component';

describe('ViewChainsHistoryComponent', () => {
  let component: ViewChainsHistoryComponent;
  let fixture: ComponentFixture<ViewChainsHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChainsHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChainsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
