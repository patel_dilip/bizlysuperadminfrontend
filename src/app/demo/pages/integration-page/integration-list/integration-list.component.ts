import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';


export interface outletElement {
  restaurantName: string;
  restaurantID: string;
  city_locality: string;
  activeIntegrations: string;
  pendingRequests: string;
}


export interface chainsElement {
  licenseNumber: string;
  chainName: string;
  numberofOutlets: string;
  chainIntegrationsActive: string;
}


const outletELEMENT_DATA: outletElement[] = [
  { restaurantName: "ABC", restaurantID: "R01", city_locality: "Pune", activeIntegrations: "intagram, facebook", pendingRequests: "whatsapp" },
  { restaurantName: "ABC", restaurantID: "R02", city_locality: "Pune", activeIntegrations: "intagram, facebook", pendingRequests: "whatsapp" },
  { restaurantName: "ABC", restaurantID: "R03", city_locality: "Pune", activeIntegrations: "intagram, facebook", pendingRequests: "whatsapp" },
  { restaurantName: "ABC", restaurantID: "R04", city_locality: "Pune", activeIntegrations: "intagram, facebook", pendingRequests: "whatsapp" },
  { restaurantName: "ABC", restaurantID: "R05", city_locality: "Pune", activeIntegrations: "intagram, facebook", pendingRequests: "whatsapp" },
  { restaurantName: "ABC", restaurantID: "R06", city_locality: "Pune", activeIntegrations: "intagram, facebook", pendingRequests: "whatsapp" },
  { restaurantName: "ABC", restaurantID: "R07", city_locality: "Pune", activeIntegrations: "intagram, facebook", pendingRequests: "whatsapp" },
  { restaurantName: "ABC", restaurantID: "R08", city_locality: "Pune", activeIntegrations: "intagram, facebook", pendingRequests: "whatsapp" },
  { restaurantName: "ABC", restaurantID: "R09", city_locality: "Pune", activeIntegrations: "intagram, facebook", pendingRequests: "whatsapp" },
  { restaurantName: "ABC", restaurantID: "R010", city_locality: "Pune", activeIntegrations: "intagram, facebook", pendingRequests: "whatsapp" },

];

const chainsELEMENT_DATA: chainsElement[] = [
  { licenseNumber: "L001", chainName: "PQR", numberofOutlets: "4", chainIntegrationsActive: "facebook, instagram" },
  { licenseNumber: "L002", chainName: "PQR", numberofOutlets: "7", chainIntegrationsActive: "facebook, instagram" },
  { licenseNumber: "L003", chainName: "PQR", numberofOutlets: "1", chainIntegrationsActive: "facebook, instagram" },
  { licenseNumber: "L004", chainName: "PQR", numberofOutlets: "7", chainIntegrationsActive: "facebook, instagram" },
  { licenseNumber: "L05", chainName: "PQR", numberofOutlets: "9", chainIntegrationsActive: "facebook, instagram" },
  { licenseNumber: "L006", chainName: "PQR", numberofOutlets: "4", chainIntegrationsActive: "facebook, instagram" },
  { licenseNumber: "L007", chainName: "PQR", numberofOutlets: "4", chainIntegrationsActive: "facebook, instagram" },
  { licenseNumber: "L008", chainName: "PQR", numberofOutlets: "4", chainIntegrationsActive: "facebook, instagram" },

]

@Component({
  selector: 'app-integration-list',
  templateUrl: './integration-list.component.html',
  styleUrls: ['./integration-list.component.scss']
})
export class IntegrationListComponent implements OnInit {

  isLinear = false;
  outletFormGroup: FormGroup;
  chainsFormGroup: FormGroup;


  //outletELEMENT_DATA
  displayedColumns: string[] = ['restaurantName', 'restaurantID', 'city_locality', 'activeIntegrations', 'pendingRequests', 'action'];
  dataSource: MatTableDataSource<outletElement>;
  @ViewChild("firstPaginator", { static: true }) paginator: MatPaginator;
  @ViewChild("firstSort", { static: true }) sort: MatSort;

  //chainsELEMENT_DATA: chainsElement
  chainsdisplayedColumns: string[] = ['licenseNumber', 'chainName', 'numberofOutlets', 'chainIntegrationsActive', 'action']
  chainsdataSource: MatTableDataSource<chainsElement>
  @ViewChild("chainsPaginator", { static: true }) chainsPaginator: MatPaginator;
  @ViewChild("chainsSort", { static: true }) chainsSort: MatSort;

  constructor(private _formBuilder: FormBuilder) {
    this.dataSource = new MatTableDataSource(outletELEMENT_DATA);
    this.chainsdataSource= new MatTableDataSource(chainsELEMENT_DATA)
  }

  ngOnInit() {
    this.outletFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.chainsFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });


    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.chainsdataSource.paginator = this.chainsPaginator;
    this.chainsdataSource.sort = this.chainsSort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  chainsapplyFilter(filterValue: string) {
    this.chainsdataSource.filter = filterValue.trim().toLowerCase();
    if (this.chainsdataSource.paginator) {
      this.chainsdataSource.paginator.firstPage();
    }
  }

}
