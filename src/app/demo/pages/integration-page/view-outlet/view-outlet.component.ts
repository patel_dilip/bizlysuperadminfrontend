import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-outlet',
  templateUrl: './view-outlet.component.html',
  styleUrls: ['./view-outlet.component.scss']
})
export class ViewOutletComponent implements OnInit {
outletObject={"Revenue_Month":"20000", "SocialMediaIntegrationsFree":"FB, Insta Twitter", "socialMediaIntegrationsPaid":"FB, insta" , "deliveryIntegrations":"335634", "smsService":"FB,Insta,Twitter", "BizlyIntegrations":"FB, insta, Twitter"};
 
integrations=[{"integrations":"Swiggy","integrationActivationDate":"20/01/2020","integrationActivatedBy":"Akshay","integrationDeactivationDate":"20/01/2020","integrationDeactivatedBy":"Akshay","lastActivated":"1/1/2020","lastDeactivated":"31/12/2019"},
 {"integrations":"FaceBook","integrationActivationDate":"20/01/2020","integrationActivatedBy":"Akshay","integrationDeactivationDate":"20/01/2020","integrationDeactivatedBy":"Akshay","lastActivated":"1/1/2020","lastDeactivated":"31/12/2019"},
 {"integrations":"Bizly Dine in QR Code","integrationActivationDate":"20/01/2020","integrationActivatedBy":"Akshay","integrationDeactivationDate":"20/01/2020","integrationDeactivatedBy":"Akshay","lastActivated":"1/1/2020","lastDeactivated":"31/12/2019"},
 {"integrations":"Bizly Online Delivery","integrationActivationDate":"20/01/2020","integrationActivatedBy":"Akshay","integrationDeactivationDate":"20/01/2020","integrationDeactivatedBy":"Akshay","lastActivated":"1/1/2020","lastDeactivated":"31/12/2019"},
 {"integrations":"Bizly QR Code Take Away","integrationActivationDate":"20/01/2020","integrationActivatedBy":"Akshay","integrationDeactivationDate":"20/01/2020","integrationDeactivatedBy":"Akshay","lastActivated":"1/1/2020","lastDeactivated":"31/12/2019"},
 
] 
constructor() { }

  ngOnInit() {
  }

}
