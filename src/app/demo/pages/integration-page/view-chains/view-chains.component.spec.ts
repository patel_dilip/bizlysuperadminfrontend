import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChainsComponent } from './view-chains.component';

describe('ViewChainsComponent', () => {
  let component: ViewChainsComponent;
  let fixture: ComponentFixture<ViewChainsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChainsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
