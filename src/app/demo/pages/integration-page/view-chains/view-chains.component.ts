import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-chains',
  templateUrl: './view-chains.component.html',
  styleUrls: ['./view-chains.component.scss']
})
export class ViewChainsComponent implements OnInit {
chainObject={"chainName":"ABC","LicenseNumber":"L001","chainIntegrationsActive":"FB, Insta, Tweeter"}

chainsData=[
  {"SerialNumber":"76543", "restaurantName":"ABC","restaurantID":"R047","city_Locality":"pune","activeIntegrations":"FB, Insta, Twitter"},
  {"SerialNumber":"76543", "restaurantName":"ABC","restaurantID":"R047","city_Locality":"pune","activeIntegrations":"FB, Insta, Twitter"},
  {"SerialNumber":"76543", "restaurantName":"ABC","restaurantID":"R047","city_Locality":"pune","activeIntegrations":"FB, Insta, Twitter"},
  {"SerialNumber":"76543", "restaurantName":"ABC","restaurantID":"R047","city_Locality":"pune","activeIntegrations":"FB, Insta, Twitter"},
  {"SerialNumber":"76543", "restaurantName":"ABC","restaurantID":"R047","city_Locality":"pune","activeIntegrations":"FB, Insta, Twitter"},
  {"SerialNumber":"76543", "restaurantName":"ABC","restaurantID":"R047","city_Locality":"pune","activeIntegrations":"FB, Insta, Twitter"},

]

constructor() { }

  ngOnInit() {
  }

}
