import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { AddBrandFoodInvnetoryComponent } from '../../menu-page/add-brand-food-invnetory/add-brand-food-invnetory.component';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { element } from 'protractor';
import { AddIngredientsComponent } from '../../menu-page/add-ingredients/add-ingredients.component';

@Component({
  selector: 'app-update-item-food-inventory',
  templateUrl: './update-item-food-inventory.component.html',
  styleUrls: ['./update-item-food-inventory.component.scss']
})
export class UpdateItemFoodInventoryComponent implements OnInit {

  additemForm: FormGroup
  quantitys: string[] = ['Liter-Mililiter',"Kg-Gram"]
  loginUserID: any
  allIngredient: any
  allBrand: any
  itemObj: any
  itemID: any
  add: any;
  constructor(private fb: FormBuilder, private dialog: MatDialog,
    private menuService: AddMenuFoodsService, private router: Router) {

    //get the item object for update
    this.itemObj = this.menuService.editObject

    console.log("edit obj", this.itemObj);
    if (this.itemObj == undefined) {
      this.router.navigateByUrl("/inventory")
    }
    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);

    this.getAllBrand()
    this.getAllIngredient()

    this.itemFormFunction()
    this.additemForm.patchValue({
      itemName: this.itemObj.itemName,
      brand: this.itemObj.brand.BrandName,
      ingredient: this.itemObj.ingredient.ingredientName,
      quantity: this.itemObj.quantity,
      userid: this.loginUserID,
      measurement:this.itemObj.measurement
    })
    this.itemID = this.itemObj._id
  }

  ngOnInit() {
  
  }

  itemFormFunction(){
    this.additemForm = this.fb.group({
      itemName: ['', [Validators.required, Validators.pattern('^[ 0-9a-zA-Z]+$')]],
      brand: ['', [Validators.required]],
      ingredient: ['', [Validators.required]],
      measurement: ['', [Validators.required]],
      quantity:this.fb.array([]),
      userid: this.loginUserID
    })
    for(let i=0; i< this.itemObj.quantity.length; i++){
      this.addQuantityList()
    }
  }

  getAllBrand() {
    this.menuService.getAllBrandFoodInventory().subscribe(data => {
      console.log(data);
      this.allBrand = data['data']
    })
  }

  getAllIngredient() {
    this.menuService.getAllIngredientFoodInventory().subscribe(data => {

      console.log(data);
      this.allIngredient = data['data']
    })
  }
  addingredients(): void {
    const dialogRef = this.dialog.open(AddIngredientsComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getAllIngredient()
      }, 2000);

    });
  }

  // add brand pop up
  addBrandFoodInventory() {
    const dialogRef = this.dialog.open(AddBrandFoodInvnetoryComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      // this.getAllFoodInventoryCategory()
      this.getAllBrand()
    })
  }

  additems() {
    const brand = this.allBrand.find(element => element.BrandName == this.additemForm.controls.brand.value)
    const ingrdient = this.allIngredient.find(element => element.ingredientName == this.additemForm.controls.ingredient.value)
   
   this.additemForm.patchValue({
    brand: brand,
    ingredient: ingrdient
   })
    console.log(this.additemForm.value);
    this.menuService.updateItemFoodInventory(this.itemID,this.additemForm.value).subscribe(res => {
      console.log(res);

      if (res['sucess'] == true) {

        Swal.fire('Item Updated Successfully', '', 'success')
        this.router.navigate(['/inventory'])
      }
      else {
        Swal.fire('Failed to Update Item', 'Something went wrong', 'warning')
      }
    })
  }
  convertintograms(inkg,i){
  
    let g=inkg*1000
    this.additemForm.controls.quantity['controls'][i].patchValue({
      gram:g
    })
   
  }
  convertintoKg(gram,i){
    console.log(this.additemForm.get('quantity').value[i]);
    let kg=gram/1000
    this.additemForm.controls.quantity['controls'][i].patchValue({
      kg:kg
    })
  }
  RemoveQuantityList(i){
    console.log(i);
    this.add.removeAt(i);
    console.log(this.add.value);
    
    
  }
addQuantityList(){
  this.add = this.additemForm.get('quantity') as FormArray;
    this.add.push(this.fb.group({
      kg: [''],
     gram: ['']
    }))
    console.log(this.add.value);
    
}
}
