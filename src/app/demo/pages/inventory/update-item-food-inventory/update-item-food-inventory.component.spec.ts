import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateItemFoodInventoryComponent } from './update-item-food-inventory.component';

describe('UpdateItemFoodInventoryComponent', () => {
  let component: UpdateItemFoodInventoryComponent;
  let fixture: ComponentFixture<UpdateItemFoodInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateItemFoodInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateItemFoodInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
