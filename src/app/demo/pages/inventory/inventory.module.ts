import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryHomeComponent } from './inventory-home/inventory-home.component';
import { InventoryComponent } from './inventory/inventory.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { SelectModule } from 'ng-select';
import { EquipmentAttributesComponent } from './equipment-attributes/equipment-attributes.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { NgxLoadingModule } from 'ngx-loading';
import { NgbPopoverModule, NgbTooltipModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { UpdateEquipmentAssetInventoryComponent } from '../inventory/update-equipment-asset-inventory/update-equipment-asset-inventory.component';
import { UpdateItemFoodInventoryComponent } from '../inventory/update-item-food-inventory/update-item-food-inventory.component';
import { UpdateIngrdientFoodInventoryComponent } from '../inventory/update-ingrdient-food-inventory/update-ingrdient-food-inventory.component';
// import { EquipmentattributesetComponent } from './equipmentattributeset/equipmentattributeset.component';

const routes: Routes = [
  {
    path: '',
    component: InventoryHomeComponent,
    children: [
      {
        path  :'',
        component :InventoryComponent
      },
      {
        path:'update-ingredient-food-inventory',
        component: UpdateIngrdientFoodInventoryComponent
      },
      {
        path: 'update-item-food-inventory',
        component: UpdateItemFoodInventoryComponent
      },
      {
        path:'update-equipment-asset-inventory',
        component: UpdateEquipmentAssetInventoryComponent
      },
      {
        path:'equipment-attributes',
        component: EquipmentAttributesComponent
      },
      
    ]
  }
];



@NgModule({
  declarations: [InventoryHomeComponent,EquipmentAttributesComponent, InventoryComponent, UpdateIngrdientFoodInventoryComponent , UpdateItemFoodInventoryComponent, UpdateEquipmentAssetInventoryComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    ReactiveFormsModule,
    FileUploadModule,
    SelectModule,
    SharedModule,
    FormsModule,
    DataTablesModule,
    NgxLoadingModule.forRoot({}),
    NgbPopoverModule,
    NgbTooltipModule,
    NgbTabsetModule
  ]
})
export class InventoryModule { }
