
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from '../../../../_services/_menuServices/add-menu-foods.service';
import { RootCategoryAssetInventoryComponent } from '../../menu-page/asset-Inventory-category-dailogs/root-category-asset-inventory/root-category-asset-inventory.component';
import { SubCategoryAssetInventoryComponent } from '../../menu-page/asset-Inventory-category-dailogs/sub-category-asset-inventory/sub-category-asset-inventory.component';
import { SubSubCategoryAssetInventoryComponent } from '../../menu-page/asset-Inventory-category-dailogs/sub-sub-category-asset-inventory/sub-sub-category-asset-inventory.component';
import { SubSubCategoryTypeAssetInventoryComponent } from '../../menu-page/asset-Inventory-category-dailogs/sub-sub-category-type-asset-inventory/sub-sub-category-type-asset-inventory.component';
import { environment } from '../../../../../environments/environment';
import { FileUploader } from 'ng2-file-upload';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

const URL = environment.base_Url + 'equipment/uploadphotos'

@Component({
  selector: 'app-update-equipment-asset-inventory',
  templateUrl: './update-equipment-asset-inventory.component.html',
  styleUrls: ['./update-equipment-asset-inventory.component.scss']
})
export class UpdateEquipmentAssetInventoryComponent implements OnInit {

  addEquipmentForm: FormGroup
  units: string[] = ['Gram', "Kg"]
  loginUserID: any
  allAssetInventoryCategory: any
  allAttributesSet: any


  uploader: FileUploader;
  response: string
  multiResponse = []
  equipmentObj:any
  equipmentID:any
  equipImage:any
  attributeData:any
  category= {};
  set: any;
  attribute_setlist=[];
  attribute=[];
  filteredArr=[];
  yesorNo: boolean;
  inputBoxPlaceholder: any;
  radiobutton: boolean;
  inputbox: boolean;
  checkbox: boolean;
  radioOptions: any;
  checkBoxOptions: any;
  subsubCategory: any;
  root: any;
  subcategory: any;
  categoryType: any;
  filtered=[];
  rootcat: any;
  subcat: any;
  subsubcat: any;
  typecat: any;
  constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService,
    private router: Router, private dialog: MatDialog) {

        //get the equipment object for update
    this.equipmentObj = this.menuService.editObject

    console.log("edit obj", this.equipmentObj);
    if (this.equipmentObj == undefined) {
      this.router.navigateByUrl("/inventory")
    }
    if(this.equipmentObj['root']!=undefined){
      this.rootcat=this.equipmentObj['root']['root']
      }
      if(this.equipmentObj['subcategory']!=undefined){
      this.subcat=this.equipmentObj['subcategory']['subcategory'];
      }
      if(this.equipmentObj['subsubCategory']!=undefined){
    this.subsubcat=this.equipmentObj['subsubCategory']['subsubCategory'];
      }
      if(this.equipmentObj['categoryType']!=undefined){
    this.typecat=this.equipmentObj['categoryType']['categoryType'];
      }
   
this.category=this.equipmentObj['category']
    console.log("=============", this.category);
    
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID);
    let obj=
      {
        id: this.equipmentObj.category['id'],
    string:this.equipmentObj.category['category'],
  attributeType:"equipment"
 
  }
  console.log(obj);
  this.subsubCategory=this.equipmentObj['subsubCategory']
  this.root=  this.equipmentObj['root']
  this.subcategory=  this.equipmentObj['subcategory']
  this.categoryType=  this.equipmentObj['categoryType']
 
  this.menuService.getattributeDatafromotherCategories(obj).subscribe(data => {
  
    console.log(data);
    this.attributeData = data
   this.set= this.attributeData['attributeSets']
  //  this.attribute_setlist=this.attributeData['attributeSets']
  this.attributeData['attributeSets'].forEach(element => {
    
    this.attribute_setlist.push(element['attributeSetName'])
  });
  console.log("list>",this.attribute_setlist);
  this.addEquipmentForm.patchValue({
    attribute_set:this.attribute_setlist,
    
  })
  console.log();
  this.attributeData['groupAttribute'].forEach(element => {
    console.log(element ['assignAttributes']);
    this.attribute=this.attribute.concat(element ['assignAttributes'])
  }); 
  console.log(this.attribute,">");
  this.filteredArr = this.attribute.reduce((acc, current) => {
    const x = acc.find(item => item['_id'] === current['_id']);
    if (!x) {
      return acc.concat([current]);
    } else {
      return acc;
    }
  }, []);
  console.log(this.filteredArr);
  this.filtered=this.equipmentObj['attributeSet_response']
 
  //  this.productObject['attributeSet_response'].forEach(element => {
      let result=this.filteredArr.filter(value => this.filtered.includes(value))
      console.log(result);
      
      if(result.length>0){
        this.filtered.concat(result)
       
      }
      console.log("replece array:=", this.filtered);

  // this.onClickResponse();
  
  })  
    this.getAllAssetInventoryCategory()
    this.getAllAttributeSet()

    this.addEquipForm()
    this.addEquipmentForm.patchValue({
      equipmentName: this.equipmentObj.equipmentName,
      category: this.equipmentObj.category['category'],
      root: this.equipmentObj.root,
      subcategory: this.equipmentObj.subcategory, 
      subsubCategory: this.equipmentObj.subsubCategory,
      categoryType: this.equipmentObj.categoryType,
      attributeSet_response:this.filtered,
      // assignattributeSet: this.equipmentObj.assignattributeSet.attributeSetName,
    userid: this.loginUserID
    })
    this.equipImage=this.equipmentObj.image,
    this.equipmentID= this.equipmentObj._id
  }

  ngOnInit() {
    

    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'equipment'

    });
    //file upload response with url
    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);


      this.response = JSON.parse(response)['EquipmentUrls']
      console.log('response', this.response);

      this.addEquipmentForm.patchValue({
       image: this.response
      })
      this.equipImage=this.response
      // this.multiResponse.push(this.response)
      // console.log('multi response', this.multiResponse);


    };
  }
addEquipForm(){
  this.addEquipmentForm = this.fb.group({
    equipmentName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z]+$')]],
    category: {},
    attribute_set:[],
      root: {},
      subcategory: {}, 
      subsubCategory: {},
      categoryType: {},
      attributeSet_response:[],
    // assignattributeSet: ['', Validators.required],
    image: [''],
    userid: this.loginUserID

  })
}

  //get all liquor attribute set
  getAllAttributeSet() {
    this.menuService.getAllLiquorAttributeSets().subscribe(data => {
      console.log("All attribute sets", data);
      this.allAttributesSet = data['data']
    })
  }

  //open tree node and close
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  getAllAssetInventoryCategory() {
    this.menuService.getAllAssetInventoryCategories().subscribe(data => {
      console.log("all asset inventory categories", data);
      this.allAssetInventoryCategory = data['data']
    })
  }

  // *********************** OPEN ROOT CATEGORY IN ASSET INVENTORY *************************8
  openInventoryRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,

      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB CATEGORY IN ASSET INVENTORY ********************** */
  openInventorySubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB SUB CATEGORY IN ASSET INVENTORY ********************** */
  openInventorySubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN ASSET INVENTORY ********************** */
  openInventorySubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }



  addequipment() {
    // const attribute = this.allAttributesSet.find(element => element.attributeSetName==this.addEquipmentForm.controls.assignattributeSet.value);
    // console.log(attribute);
    // this.addEquipmentForm.patchValue({
    //   assignattributeSet: attribute._id
    // })
    //  console.log(this.addEquipmentForm.value);
    
    //  console.log(this.addEquipmentForm.value);
     
    this.menuService.updateEquipmentAssetInventory(this.equipmentID ,this.addEquipmentForm.value).subscribe(res => {
      console.log(res);

      if (res['success'] == true) {

        Swal.fire('Equipment Updated Successfully', '', 'success')
        this.router.navigate(['/inventory'])
      }
      else {
        Swal.fire('Failed to Update Equipment', 'Something went wrong', 'warning')
      }
    })
  }
  getRadiovalue(id, name){
    // 
    this.category={category:name, id:id};
    console.log(id,">>", name);
   
  let obj=
      {
        id: id,
    string:name,
  attributeType:"equipment"
 
  }
  console.log("hi");
  console.log(obj)
  
    this.menuService.getattributeDatafromotherCategories(obj).subscribe(data => {
    
  
      console.log(data);
      this.attributeData = data
     this.set= this.attributeData['attributeSets']
    //  this.attribute_setlist=this.attributeData['attributeSets']
    this.attributeData['attributeSets'].forEach(element => {
      
      this.attribute_setlist.push(element['attributeSetName'])
    });
    console.log("list>",this.attribute_setlist);
    this.addEquipmentForm.patchValue({
      attribute_set:this.attribute_setlist,
      
    })
    console.log();
    this.attributeData['groupAttribute'].forEach(element => {
      console.log(element ['assignAttributes']);
      this.attribute=this.attribute.concat(element ['assignAttributes'])
    }); 
    console.log(this.attribute,">");
    this.filteredArr = this.attribute.reduce((acc, current) => {
      const x = acc.find(item => item['_id'] === current['_id']);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
    console.log(this.filteredArr);
    // this.onClickResponse();
    
    
  })
  // if(this.attributeData.length==0){
  //   Swal.fire('Attribute not available', '', 'warning')
  // }
  }
  onClickResponse(i) {
    
    // this.filteredArr.forEach((element, index) => {
      
  //  if(i==index-1){
    console.log(this.filtered[i].responseType.elementName);
    if (this.filteredArr[i].responseType.elementName == "Input Box") {
      this.inputBoxPlaceholder=this.filtered[i].responseType.fieldValue
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = true
      this.checkbox = false
    }
    if (this.filtered[i].responseType.elementName == "Yes / No") {
     
      this.radiobutton = false
      this.yesorNo = true
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filtered[i].responseType.elementName == "Radio Button") {
      this.radioOptions = this.filtered[i].responseType.options
      this.radiobutton = true
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = false
    }
    if (this.filtered[i].responseType.elementName == "Check Box") {
      this.checkBoxOptions = this.filtered[i].responseType.options
      this.radiobutton = false
      this.yesorNo = false
      this.inputbox = false
      this.checkbox = true

    }
    // }

  }
  getroot(r?:any,sub?:any,subsub?:any, type?:any){
    this.rootcat=''
    this.subcat=''
    this.subsubcat=''
    this.typecat=''
    this.root={root:r['rootCategoryName'],id:r['_id']}
    if(sub!=undefined){
    this.subcategory={subcategory:sub['subCategoryName'],id:sub['_id']}
    }
    if(subsub!=undefined){
    this.subsubCategory={subsubCategory:subsub['subSubCategoryName'],id:subsub['_id']}
    }
    if(type!=undefined){
    this.categoryType={categoryType:type['subSubCategoryTypeName'],id:type['_id']}
    }
    console.log(r,">", sub,">", subsub,">", type);
    
  }
  
}
