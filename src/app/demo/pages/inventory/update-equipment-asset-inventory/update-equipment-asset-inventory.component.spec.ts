import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateEquipmentAssetInventoryComponent } from './update-equipment-asset-inventory.component';

describe('UpdateEquipmentAssetInventoryComponent', () => {
  let component: UpdateEquipmentAssetInventoryComponent;
  let fixture: ComponentFixture<UpdateEquipmentAssetInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateEquipmentAssetInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateEquipmentAssetInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
