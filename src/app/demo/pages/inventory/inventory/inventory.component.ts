import { forkJoin } from 'rxjs';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { map } from 'rxjs/operators';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, MatSort, MatDialog, matTooltipAnimations, MatTabChangeEvent } from '@angular/material';
import { MainService } from '../../../../_services/main.service';
import Swal from 'sweetalert2';
import { AddMenuFoodsService } from '../../../../_services/_menuServices/add-menu-foods.service';
import { Route, Router } from '@angular/router';
import { ViewIngredientFoodInventoryComponent } from '../../menu-page/view-ingredient-food-inventory/view-ingredient-food-inventory.component';
import { ViewItemsFoodInventoryComponent } from '../../menu-page/view-items-food-inventory/view-items-food-inventory.component';
import { ViewEquipmentAssetInventoryComponent } from '../../menu-page/view-equipment-asset-inventory/view-equipment-asset-inventory.component';
import { AddIngredientsComponent } from '../../menu-page/add-ingredients/add-ingredients.component';
import { AddItemsComponent } from '../../menu-page/add-items/add-items.component';
import { AddEquipmentComponent } from '../../menu-page/add-equipment/add-equipment.component';
import { RootCategoryAssetInventoryComponent } from '../../menu-page/asset-Inventory-category-dailogs/root-category-asset-inventory/root-category-asset-inventory.component';
import { SubCategoryAssetInventoryComponent } from '../../menu-page/asset-Inventory-category-dailogs/sub-category-asset-inventory/sub-category-asset-inventory.component';
import { SubSubCategoryAssetInventoryComponent } from '../../menu-page/asset-Inventory-category-dailogs/sub-sub-category-asset-inventory/sub-sub-category-asset-inventory.component';
import { SubSubCategoryTypeAssetInventoryComponent } from '../../menu-page/asset-Inventory-category-dailogs/sub-sub-category-type-asset-inventory/sub-sub-category-type-asset-inventory.component';
import { RootCategoryFoodInventoryComponent } from '../../menu-page/food-inventory-category-dailogs/root-category-food-inventory/root-category-food-inventory.component';
import { SubCategoryFoodInventoryComponent } from '../../menu-page/food-inventory-category-dailogs/sub-category-food-inventory/sub-category-food-inventory.component';
import { SubSubCategoryFoodInventoryComponent } from '../../menu-page/food-inventory-category-dailogs/sub-sub-category-food-inventory/sub-sub-category-food-inventory.component';
import { SubSubCategoryTypeFoodInventoryComponent } from '../../menu-page/food-inventory-category-dailogs/sub-sub-category-type-food-inventory/sub-sub-category-type-food-inventory.component';
import { SubSubCategoryBeverageComponent } from '../../menu-page/beverages-category-dailogs/sub-sub-category-beverage/sub-sub-category-beverage.component';
import { type } from 'os';



// ******************************************* Food ************************************************


// *************************************** Liquor *********************************************

export interface ingredientElement {
  ingredientName: string;
  ingredientCode: string;
  ingredientcategory: string;
  unit: string;
  status: string;
  addedBy: string;
  updatedAt: string
}

export interface liquorAttributeSetsElement {
  attributeSetCode: string;
  attributeSetName: string;
  assignGroups: string;
  status: string
  addedBy: string;
  updatedAt: string;
}
export interface inputElement {
  itemCode: string;
  itemName: string;
  brand: string;
  ingredient: string;
  quantity: string;
  addedBy: string;
  updatedAt: string;
}
export interface liquorAttributeElement {
  attributeCode: string;
  attributeName: string;
  responseType: string;
  searchable: boolean;
  filterable: boolean;
  status: boolean;
  addedBy: string;
  updatedAt: string;
}

// ********************************** Asset Inventory *************************************

export interface equipmentElement {
  equipmentCode: string;
  equipmentName: string;
  category: string;
  // assignattributeSet: string;
  addedBy: string;
  updatedAt: string;
}


@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit {
  equipmentfilter = { root: [], addedby: [], status: [] }
  Ingredientfilter = { root: [], addedby: [], status: [] }

  Itemfilter = { brand: [], ingredient: [], addedby: [], status: [] }
  //liquorAttribute_DATA: liquorAttributeElement
  displayColumnsliquorAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceLiquorAttribute: MatTableDataSource<liquorAttributeElement>
  @ViewChild("liquorAttributepaginator", { static: true }) liquorAttributepaginator: MatPaginator;
  @ViewChild("liquorAttributesort", { static: true }) liquorAttributesort: MatSort;

  //********************************************  Food Inventory **************************************/

  //ingredient_DATA: ingredientElement
  displayColumnsingredient: string[] = ['ingredientCode', 'ingredientName', 'ingredientcategory', 'unit', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceIngredient: MatTableDataSource<ingredientElement>
  @ViewChild("ingredientpaginator", { static: true }) ingredientpaginator: MatPaginator;
  @ViewChild("ingredientsort", { static: true }) ingredientsort: MatSort;

  //input_DATA: inputElement
  displayColumnsInputData: string[] = ['itemCode', 'itemName', 'brand', 'ingredient', 'quantity', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceInput: MatTableDataSource<inputElement>
  @ViewChild("inputpaginator", { static: true }) inputpaginator: MatPaginator;
  @ViewChild("inputsort", { static: true }) inputsort: MatSort;


  // ****************************************** Asset Inventory *****************************************
  //equipment_DATA: equipmentElement
  displayColumnsequipment: string[] = ['equipmentCode', 'equipmentName', 'category', 'attribute_set', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceEquipment: MatTableDataSource<equipmentElement>
  @ViewChild("equipmentpaginator", { static: true }) equipmentpaginator: MatPaginator;
  @ViewChild("equipmentsort", { static: true }) equipmentsort: MatSort;

  //beverageAttributeElement
  displayColumnsBeverageAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceBeverageAttribute: MatTableDataSource<liquorAttributeElement>
  @ViewChild("beverageAttributepaginator", { static: true }) beverageAttributepaginator: MatPaginator;
  @ViewChild("beverageAttributesort", { static: true }) beverageAttributesort: MatSort;

  //retailFoodAttributeElement
  displayColumnsretailFoodAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceRetailFoodAttribute: MatTableDataSource<liquorAttributeElement>
  @ViewChild("retailFoodAttributepaginator", { static: true }) retailFoodAttributepaginator: MatPaginator;
  @ViewChild("retailFoodAttributesort", { static: true }) retailFoodAttributesort: MatSort;


  firstLevel: any


  //ingredient > Categories > show categories
  size = 12
  showIngredientCategories = false

  // asset inventory > categories > show categories
  inputsize = 12
  showInputCategories = false

  allAssetInventoryCategory = []
  allFoodInventoryCategory: any
  activeTab: any
  activeTabBeverage: any
  activeTabRetailFood: any

  assetInventoryAttributeCount = 0
  all: any

  allMenuData: any

  storeData: any;
  fileUploaded: File;
  worksheet: any;
  jsonData: any;
  csvData: any;
  userid: any

  date: any

  //export excel file 
  allLiquorAttribute = []

  allBeverageUnassignedAttribute = []
  allRetailFoodUnassignedAttribue = []
  cusineExportJSONDATA = []
  categoryExportJSONDATA = []
  dishExportJSONDATA = []
  addonExportJSONDATA = []
  varientExportJSONDATA = []
  liquorBrandJSONDATA = []
  liquorProductJSONDATA = []
  liquorAttributeJSONDATA = []
  beverageAttributeJSONDATA = []
  retailFoodAttributeJSONDATA = []
  liquorAttributeSetJSONDATA = []
  beverageAttributeSetJSONDATA = []
  retailFoodAttributeSetJSONDATA = []
  ingredientJSONDATA = []
  itemsJSONDATA = []
  equipmentJSONDATA = []
  beverageJSONDATA = []
  beverageProductJSONDATA = []
  beveraeBrandJSONDATA = []
  retailFoodProductJSONDATA = []
  retailFoodBrandJSONDATA = []

  noData9: any;
  noData10: any;
  noData11: any;
  noData12: any;
  noData13: any;
  noData14: any;
  noData15: any;
  noData16: any;
  liquorAttributeCount: number;
  input_data: any
  ingredient_data: any;
  equipment_data: any;
  admin = [];
  assetInventoryRoot = [];
  foodInventoryRoot = [];

  allBrand = [];
  allIngredients = [];

  constructor(private _formBuilder: FormBuilder, private main: MainService, private router: Router,
    public dialog: MatDialog, private menuService: AddMenuFoodsService) {
    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];

    this.getAllAssetInventoryCategory()
    this.getAllFoodInventoryCategory()
    this.getAllLiquorAttribute()
    this.getAllIngredientFoodInventory()
    this.getAllEquipmentAssetInventory()
    this.getAllItemsFoodInventory()
    this.getadmin()
    this.getAllIngredient()
    this.getAllBrand()
    //  this.loadSuitofAssetInventory()
    // this.loadSuitofFoodInventory()


    // this.getAllApis()
    // this.main.getAllEstablishment().subscribe(data => {
    //   //console.log(data);
    //   this.firstLevel = data
    // })

    var d = new Date()
    this.date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();

  }
  getAllBrand() {
    this.menuService.getAllBrandFoodInventory().subscribe(data => {
      console.log("Brands", data);
      this.allBrand = data['data']
    })
  }

  getAllIngredient() {
    this.menuService.getAllIngredientFoodInventory().subscribe(data => {

      console.log("Ingredient", data);
      this.allIngredients = data['data']
    })
  }


  getAllLiquorAttribute() {
    this.menuService.getAllLiquorAttribute().subscribe(data => {
      //console.log("all liquor attribute", data);

      if (data['sucess'] == false) {
        this.liquorAttributeCount = 0
        this.assetInventoryAttributeCount = 0
        //console.log("Attribute Data Not Found");
      } else {
        // LIQUOR
        data['data'].forEach(element => {
          element.attributeCode = "A" + element.attributeCode
        });
        let allAttributes = data['data'].filter(element => element.attributeType == "liquor")
        this.dataSourceLiquorAttribute = new MatTableDataSource(allAttributes);
        this.dataSourceLiquorAttribute.paginator = this.liquorAttributepaginator;
        this.dataSourceLiquorAttribute.sort = this.liquorAttributesort;
        this.allLiquorAttribute = allAttributes.filter(ele => ele['isAssign'] == false)
        this.liquorAttributeCount = allAttributes.length
        // BEVERAGE
        this.dataSourceBeverageAttribute = new MatTableDataSource(data['data'].filter(element => element.attributeType == "beverage"))
        this.dataSourceBeverageAttribute.paginator = this.beverageAttributepaginator;
        this.dataSourceBeverageAttribute.sort = this.beverageAttributesort;
        this.allBeverageUnassignedAttribute = data['data'].filter(element => element.attributeType == "beverage" && element.isAssign == false)

        // RETAIL FOOD
        this.dataSourceRetailFoodAttribute = new MatTableDataSource(data['data'].filter(element => element.attributeType == "retailFood"))
        this.dataSourceRetailFoodAttribute.paginator = this.retailFoodAttributepaginator;
        this.dataSourceRetailFoodAttribute.sort = this.retailFoodAttributesort;
        this.allRetailFoodUnassignedAttribue = data['data'].filter(element => element.attributeType == "retailFood" && element.isAssign == false)

        //Asset Inventory
        this.assetInventoryAttributeCount = data['data'].filter(element => element.attributeType == "equipment").length


        var liquorAttribute = data['data'].filter(element => element.attributeType == "liquor")
        liquorAttribute.forEach(element => {
          var optionLabels = []
          element.responseType.options.forEach(ele => {
            optionLabels.push(ele.optionLable)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let liquorObj = {}
          liquorObj = {
            "attributeCode": element.attributeCode,
            "attributeName": element.attributeName,
            "responseType": element.responseType.elementName,
            "fieldValue": element.responseType.fieldValue,
            "options": optionLabels.toString(),
            "isSearchable": element.isSearchable,
            "isFilterable": element.isFilterable,
            "isAssign": element.isAssign,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.liquorAttributeJSONDATA.push(liquorObj)
        });
        //console.log("liquor attribute export", this.liquorAttributeJSONDATA);

        var beverageAttribute = data['data'].filter(element => element.attributeType == "beverage")
        beverageAttribute.forEach(element => {
          var optionLabels = []
          element.responseType.options.forEach(ele => {
            optionLabels.push(ele.optionLable)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let beverageObj = {}
          beverageObj = {
            "attributeCode": element.attributeCode,
            "attributeName": element.attributeName,
            "responseType": element.responseType.elementName,
            "fieldValue": element.responseType.fieldValue,
            "options": optionLabels.toString(),
            "isSearchable": element.isSearchable,
            "isFilterable": element.isFilterable,
            "isAssign": element.isAssign,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.beverageAttributeJSONDATA.push(beverageObj)
        });
        //console.log("beverage attribute export", this.beverageAttributeJSONDATA);


        var retailFoodAttribute = data['data'].filter(element => element.attributeType == "retailFood")
        retailFoodAttribute.forEach(element => {
          var optionLabels = []
          element.responseType.options.forEach(ele => {
            optionLabels.push(ele.optionLable)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let retailFoodObj = {}
          retailFoodObj = {
            "attributeCode": element.attributeCode,
            "attributeName": element.attributeName,
            "responseType": element.responseType.elementName,
            "fieldValue": element.responseType.fieldValue,
            "options": optionLabels.toString(),
            "isSearchable": element.isSearchable,
            "isFilterable": element.isFilterable,
            "isAssign": element.isAssign,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.retailFoodAttributeJSONDATA.push(retailFoodObj)
        });
        //console.log("retail food attribute export", this.retailFoodAttributeJSONDATA);

      }
    })
  }
  ngOnInit() {
    let index = localStorage.getItem('userTabLocation')
    if (index == undefined) {
      let index = 0; // get stored number or zero if there is nothing stored
      this.activeTab = index; // with tabGroup being the MatTabGroup accessed through ViewChild

    } else {
      // let index = localStorage.getItem('userTabLocation') || 0; // get stored number or zero if there is nothing stored
      this.activeTab = index; // with tabGroup being the MatTabGroup accessed through ViewChild

    }

    let beverageTabIndex = localStorage.getItem('beverageTabLocation')
    if (beverageTabIndex == undefined) {
      this.activeTabBeverage = 0
    } else {
      this.activeTabBeverage = beverageTabIndex
    }


    let retailFoodTabIndex = localStorage.getItem('retailFoodMenuManagement')
    if (retailFoodTabIndex == undefined) {
      this.activeTabRetailFood = 0
    } else {
      this.activeTabRetailFood = retailFoodTabIndex
    }
  }

  // getAllApis() {
  //   //console.log("in fork join");

  //   forkJoin(
  //     {
  //       allAttributes :this.menuService.getAllLiquorAttribute(),
  //       allFoodInventoryIngrdient: this.menuService.getAllIngredientFoodInventory(),
  //       allFoodInventoryItems: this.menuService.getAllItemsFoodInventory(),
  //       allAssetInventoryEquipments: this.menuService.getAllEquipmentAssetInventory()
  //     }
  //   ).subscribe(result => {
  //     //console.log("fork Join", result);
  //     this.allMenuData = result

  //     //get all tables data function call
  //     this.getAllIngredientFoodInventory()
  //     this.getAllItemsFoodInventory()
  //     this.getAllEquipmentAssetInventory() 
  //     this.getAllLiquorAttribute()





  //   })
  // }
  ngAfterViewInit() {

  }
  //Tabs On navigate from tab back to same tab
  handleMatTabChange(event) {
    //console.log(event);
    localStorage.setItem('userTabLocation', event);
  }

  handleMatTabChangeMenuManagementBeverage(event) {
    localStorage.setItem('beverageTabLocation', event)
  }

  handleMatTabChangeRetailFood(event) {
    localStorage.setItem('retailFoodMenuManagement', event)
  }

  // import excel file and convert it to json object
  uploadedFile(event) {
    //console.log("upload file");

    this.fileUploaded = event.target.files[0];
    this.readExcel();
  }
  readExcel() {
    let readFile = new FileReader();
    //console.log(readFile);

    readFile.onload = (e) => {

      this.storeData = readFile.result;
      var data = new Uint8Array(this.storeData);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });

      var first_sheet_name = workbook.SheetNames[0];
      this.worksheet = workbook.Sheets[first_sheet_name];
      this.jsonData = XLSX.utils.sheet_to_json(this.worksheet, { raw: false });
      this.jsonData.map(ele => (ele.userid = this.userid))
      //   this.jsonData = JSON.stringify(this.jsonData);
      //console.log(this.jsonData);
    }
    readFile.readAsArrayBuffer(this.fileUploaded);


  }





  // get all ingredients food inventory
  getAllIngredientFoodInventory() {
    this.menuService.getAllIngredientFoodInventory().subscribe(data => {
      //console.log("All Ingredient food inventory", data);
      if (data['sucess'] == false) {
        //console.log("Food Inventroy Ingredient Data Not Found");
        this.noData9 = 0;
      } else {
        this.ingredient_data = data['data']
        data['data'].forEach(element => {
          element.ingredientCode = "I" + element.ingredientCode
        });
        this.dataSourceIngredient = new MatTableDataSource(data['data']);
        this.dataSourceIngredient.paginator = this.ingredientpaginator;
        this.dataSourceIngredient.sort = this.ingredientsort;
        this.noData9 = this.dataSourceIngredient.connect().pipe(map(data => data.length === 0));

        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var ingredientObj = {}
          ingredientObj = {
            "ingredientCode": element.ingredientCode,
            "ingredientName": element.ingredientName,
            "ingredientcategory": element.ingredientcategory,
            "root": element.root,
            "sub": element.sub,
            "subsub": element.subsub,
            "type": element.type,
            "unit": element.unit,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.ingredientJSONDATA.push(ingredientObj)
        });
        //console.log("ingredient export", this.ingredientJSONDATA);
      }
    })
  }

  // export ingredient excel 
  readAsCSVIngredient() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["ingredientCode", "ingredientName", "ingredientcategory", "unit", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.ingredientJSONDATA, 'Ingredient_Export_' + this.date, options);
  }

  // get all items food inventory
  getAllItemsFoodInventory() {
    this.menuService.getAllItemsFoodInventory().subscribe(data => {
      console.log(data);

      if (data['sucess'] == false) {
        //console.log("Food Inventory Items Data Not Found");
        this.noData10 = 0;
      } else {
        this.input_data = data['data']
        data['data'].forEach(element => {
          element.itemCode = "I" + element.itemCode
        });
        this.dataSourceInput = new MatTableDataSource(data['data']);
        this.dataSourceInput.paginator = this.inputpaginator;
        this.dataSourceInput.sort = this.inputsort;
        this.noData10 = this.dataSourceInput.connect().pipe(map(data => data.length === 0));

        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var itemtObj = {}
          itemtObj = {
            "itemCode": element.itemCode,
            "itemName": element.itemName,
            "brand": element.brand.BrandName,
            "ingredient": element.ingredient.ingredientName,
            "quantity": element.quantity,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.itemsJSONDATA.push(itemtObj)
        });
        //console.log("Item export", this.itemsJSONDATA);
      }
    })
  }

  // export items excel 
  readAsCSVItems() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["itemCode", "itemName", "brand", "ingredient", "quantity", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.itemsJSONDATA, 'Items_Export_' + this.date, options);
  }


  // get all equipment asset inventory
  getAllEquipmentAssetInventory() {
    this.menuService.getAllEquipmentAssetInventory().subscribe(data => {
      console.log("All Equipment Asset Inventory", data);
      if (data['success'] == false) {
        //console.log("Asset Inventory Equipment Data Not Found");
        this.noData11 = 0
      } else {
        this.equipment_data = data['data']
        data['data'].forEach(element => {
          element.equipmentCode = "E" + element.equipmentCode
        });
        this.dataSourceEquipment = new MatTableDataSource(data['data'])
        this.dataSourceEquipment.paginator = this.equipmentpaginator;
        this.dataSourceEquipment.sort = this.equipmentsort;
        this.noData11 = this.dataSourceEquipment.connect().pipe(map(data => data.length === 0));

        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var equipmentObj = {}
          equipmentObj = {
            "equipmentCode": element.equipmentCode,
            "equipmentName": element.equipmentName,
            "category": element.category,
            "attribute_set": element.attribute_set,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.equipmentJSONDATA.push(equipmentObj)
        });
        //console.log("Equipment export", this.equipmentJSONDATA);
      }
    })
  }

  // export equipment excel 
  readAsCSVEquipment() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["equipmentCode", "equipmentName", "category", "assignattributeSet", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.equipmentJSONDATA, 'Equipment_Export_' + this.date, options);
  }



  // view ingredient food inventory
  viewIngredientFoodinventory(obj): void {
    const dialogRef = this.dialog.open(ViewIngredientFoodInventoryComponent, {
      data: obj,
      disableClose: true,
      width:'60%',
      height:'80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      // this.animal = result;

    });
  }

  //view items food inventory
  viewItemsFoodInventory(obj): void {
    const dialogRef = this.dialog.open(ViewItemsFoodInventoryComponent, {
      data: obj,
      width:'60%',
      height:'80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');

      // this.animal = result;
    });
  }

  // view equipment asset inventory
  viewEquipmentAssetEquipment(obj): void {
    const dialogRef = this.dialog.open(ViewEquipmentAssetInventoryComponent, {
      data: obj,
      width:'60%',
      height:'80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      // this.animal = result;

    });
  }


  editIngredientFoodInventory(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/inventory/update-ingredient-food-inventory")
  }

  editItemFoodInventory(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/inventory/update-item-food-inventory")
  }

  editEquipmentAssetInventory(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/inventory/update-equipment-asset-inventory")
  }
  //delete ingredient food inventory
  deleteIngredientFoodInventory(obj) {
    //console.log("delete", obj);
    this.menuService.deleteIngredientFoodInventory(obj._id).subscribe(res => {
      //console.log(res);
      if (res['sucess'] == true) {
        Swal.fire('Ingredient Delete Successfully', '', 'success')
        // this.getAllApis()
      }
      else {
        Swal.fire('Failed to Delete Ingredient', 'Something went wrong', 'warning')
      }
    })
  }

  //delete item food inventory
  deleteItemFoodInventory(obj) {
    //console.log("delete", obj);
    this.menuService.deleteItemFoodInventory(obj._id).subscribe(res => {
      //console.log(res);
      if (res['sucess'] == true) {
        Swal.fire('Item Delete Successfully', '', 'success')
        // this.getAllApis()
      }
      else {
        Swal.fire('Failed to Delete Item', 'Something went wrong', 'warning')
      }
    })
  }


  // Delete equipment asset inventory
  deleteEquipmentAssetInventory(obj) {
    //console.log("delete", obj);
    this.menuService.deleteEquipmentAssetInventory(obj._id).subscribe(res => {
      //console.log(res);
      if (res['success'] == true) {
        Swal.fire('Equipment Delete Successfully', '', 'success')
        // this.getAllApis()
      }
      else {
        Swal.fire('Failed to Delete Equipment', 'Something went wrong', 'warning')
      }
    })
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Soft Delete ///////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////

  // Soft Delete Brand Liquor

  // SOFT DELETE INGREDIENT 
  softDeleteIngrdient(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteIngredient(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['sucess'] == true) {
        if (event.checked) {

          Swal.fire('Material Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Material Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllIngredientFoodInventory();
    })
  }

  //pop up for add ingredient in Food Inventory
  addingredients(): void {
    const dialogRef = this.dialog.open(AddIngredientsComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getAllIngredientFoodInventory()
      }, 2000);

    });
  }

  //popup for add items in  > Food Inventory
  addItems(): void {
    const dialogRef = this.dialog.open(AddItemsComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
     
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getAllItemsFoodInventory()
      }, 2000);
    });
  }

  //popup for add equipment in > Asset Inventory
  openAddEquipment(): void {
    const dialogRef = this.dialog.open(AddEquipmentComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,

      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.getAllEquipmentAssetInventory()
      }, 2000);
    });
  }




  // show and hide food inventory categories tree
  ingredientCategory(event) {
    //console.log(event.srcElement.checked);
    if (event.srcElement.checked == true) {
      this.size = 8
      this.showIngredientCategories = true
    } else {
      this.size = 12
      this.showIngredientCategories = false

    }
  }

  //show and hide asset inventory categories tree
  inputCategory(event) {
    //console.log(event.srcElement.checked);
    if (event.srcElement.checked == true) {
      this.inputsize = 8
      this.showInputCategories = true
    } else {
      this.inputsize = 12
      this.showInputCategories = false

    }
  }


  ////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////// All Data Table Filters //////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////




  applyFilterIngredientFoodInventory(event) {
    this.dataSourceIngredient.data = this.ingredient_data
    //console.log( this.dataSourceIngredient.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceIngredient.data.filter(function (d) {
      //console.log(d);
      let result
      if (d['root'] != undefined && d['sub'] != undefined && d['subsub'] != undefined && d['type'] != undefined) {
        result = d['ingredientName'].toLowerCase().indexOf(val) !== -1 || d['ingredientCode'].toLowerCase().indexOf(val) !== -1 || d['root']['root'].toLowerCase().indexOf(val) !== -1 || d['sub']['sub'].toLowerCase().indexOf(val) !== -1 || d['subsub']['subsub'].toLowerCase().indexOf(val) !== -1 || d['type']['type'].toLowerCase().indexOf(val) !== -1
        return result
      }
      // else if (d['root'] == undefined) {
      //   result = d['ingredientName'].toLowerCase().indexOf(val) !== -1 || d['ingredientCode'].toLowerCase().indexOf(val) !== -1
      //   return result
      // }
      else if (d['sub'] == undefined || d.hasOwnProperty("sub")==false ) {
        result = d['ingredientName'].toLowerCase().indexOf(val) !== -1 || d['ingredientCode'].toLowerCase().indexOf(val) !== -1 || d['root']['root'].toLowerCase().indexOf(val) !== -1
        return result
      }
      else if (d['subsub'] == undefined || d.hasOwnProperty("subsub")==false ) {
        result = d['ingredientName'].toLowerCase().indexOf(val) !== -1 || d['ingredientCode'].toLowerCase().indexOf(val) !== -1 || d['root']['root'].toLowerCase().indexOf(val) !== -1 || d['sub']['sub'].toLowerCase().indexOf(val) !== -1
        return result
      }
      else if (d['type'] == undefined || d.hasOwnProperty("type")==false ) {
        result = d['ingredientName'].toLowerCase().indexOf(val) !== -1 || d['ingredientCode'].toLowerCase().indexOf(val) !== -1 || d['root']['root'].toLowerCase().indexOf(val) !== -1 || d['sub']['sub'].toLowerCase().indexOf(val) !== -1 || d['subsub']['subsub'].toLowerCase().indexOf(val) !== -1
        return result
      }
    });



    //console.log(filter);
    this.dataSourceIngredient.data = filter;
  }

  applyFilterInputFoodInventory(event) {
    this.dataSourceInput.data = this.input_data
    //console.log( this.dataSourceInput.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceInput.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['itemCode'].toLowerCase().indexOf(val) !== -1 || d['itemName'].toLowerCase().indexOf(val) !== -1 || d['brand']['BrandName'].toLowerCase().indexOf(val) !== -1 || d['ingredient']['ingredientName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    //console.log(filter);
    this.dataSourceInput.data = filter;
  }

  applyFilterEquipment(event) {
    this.dataSourceEquipment.data = this.equipment_data
    //console.log( this.dataSourceEquipment.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceEquipment.data.filter(function (d) {
      //console.log(d);
      let result
      if (d['root'] != undefined && d['subcategory'] != undefined && d['subsubCategory'] != undefined && d['categoryType'] != undefined) {
        result = d['equipmentCode'].toLowerCase().indexOf(val) !== -1 || d['equipmentName'].toLowerCase().indexOf(val) !== -1 || d['categoryType']['categoryType'].toLowerCase().indexOf(val) !== -1 || d['root']['root'].toLowerCase().indexOf(val) !== -1 || d['subcategory']['subcategory'].toLowerCase().indexOf(val) !== -1 || d['subsubCategory']['subsubCategory'].toLowerCase().indexOf(val) !== -1

        return result
      }
      else if (d['root'] == undefined) {
        result = d['equipmentCode'].toLowerCase().indexOf(val) !== -1 || d['equipmentName'].toLowerCase().indexOf(val) !== -1

        return result
      } else if (d['subcategory'] == undefined) {
        result = d['equipmentCode'].toLowerCase().indexOf(val) !== -1 || d['equipmentName'].toLowerCase().indexOf(val) !== -1 || d['root']['root'].toLowerCase().indexOf(val) !== -1

        return result
      }
      else if (d['subsubCategory'] == undefined) {
        result = d['equipmentCode'].toLowerCase().indexOf(val) !== -1 || d['equipmentName'].toLowerCase().indexOf(val) !== -1 || d['root']['root'].toLowerCase().indexOf(val) !== -1 || d['subcategory']['subcategory'].toLowerCase().indexOf(val) !== -1

        return result
      }
      else if (d['categoryType'] == undefined) {
        result = d['equipmentCode'].toLowerCase().indexOf(val) !== -1 || d['equipmentName'].toLowerCase().indexOf(val) !== -1 || d['root']['root'].toLowerCase().indexOf(val) !== -1 || d['subcategory']['subcategory'].toLowerCase().indexOf(val) !== -1 || d['subsubCategory']['subsubCategory'].toLowerCase().indexOf(val) !== -1

        return result
      }
    });


    //console.log(filter);
    this.dataSourceEquipment.data = filter;
    this.dataSourceEquipment = new MatTableDataSource<any>(filter);
    this.noData11 = this.dataSourceEquipment.connect().pipe(map(data => data.length === 0));
    this.dataSourceEquipment.paginator = this.equipmentpaginator;
        this.dataSourceEquipment.sort = this.equipmentsort;

  }


  // *****************************  Liquor tree *******************
  //open tree node and close
  caretClick() {

    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }



  /////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////// ASSET INVENTORY CATEGORY TREE //////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////


  getAllAssetInventoryCategory() {
    this.menuService.getAllAssetInventoryCategories().subscribe(data => {
      //console.log("all asset inventory categories", data);
      this.allAssetInventoryCategory = data['data']
      this.allAssetInventoryCategory.forEach(element => {
        this.assetInventoryRoot.push(element['rootCategoryName'])
      })
      this.allAssetInventoryCategory.forEach((element, index) => {
      
          element.showChildren = false
          if (
            this.allAssetInventoryCategory[index].hasOwnProperty('subCategories')
            &&
            this.allAssetInventoryCategory[index].subCategories.length > 0
          ) {
            this.allAssetInventoryCategory[index].subCategories.forEach((element, subcategoryIndex = index) => {
              element.edit = false;
              if (
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
                  element.edit = false;
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                      element.edit = false;
                    })
                  }
  
  
                })
              }
            })
          }
        
      });
      //console.log(">>>>>data>>>>", this.allAssetInventoryCategory);

    })
    // this.loadSuitofAssetInventory()
  }
  // *********************** OPEN ROOT CATEGORY IN ASSET INVENTORY *************************8
  openInventoryRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,

      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB CATEGORY IN ASSET INVENTORY ********************** */
  openInventorySubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }


  //*************************OPEN SUB SUB CATEGORY IN ASSET INVENTORY ********************** */
  openInventorySubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }

  //*************************OPEN SUB SUB CATEGORY TYPE IN ASSET INVENTORY ********************** */
  openInventorySubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeAssetInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllAssetInventoryCategory()
    })
  }






  ///////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////// FOOD INVENTORY CATEGORY TREE ////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////



  getAllFoodInventoryCategory() {
    this.menuService.getAllFoodInventoryCategories().subscribe(data => {
      console.log("all food inventory categories", data);
      this.allFoodInventoryCategory = data['data']
      this.allFoodInventoryCategory.forEach(element => {
        this.foodInventoryRoot.push(element['rootCategoryName'])
      });
      this.allFoodInventoryCategory.forEach((element, index) => {
        // if (index == rootindex) {
          element.edit = false
          if (
            this.allFoodInventoryCategory[index].hasOwnProperty('subCategories')
            &&
            this.allFoodInventoryCategory[index].subCategories.length > 0
          ) {
            this.allFoodInventoryCategory[index].subCategories.forEach((element, subcategoryIndex = index) => {
              element.edit = false;
              if (
                this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
                  element.edit = false;
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, liquorSubSubVarientTypeIndex = index) => {
                      element.edit = false;
                    })
                  }
  
  
                })
              }
            })
          }
        // }
      });
     console.log("getallfoodinventory", this.allFoodInventoryCategory);
     
    })
  }

  //*************************OPEN ROOT CATEGORY IN FOOD INVENTORY ********************** */
  openFoodInventoryRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryFoodInventoryComponent, {
      width: '460px',
      disableClose: true,
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }
  //*************************OPEN SUB CATEGORY IN FOOD INVENTORY ********************** */
  openFoodInventorySubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryFoodInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }
  //*************************OPEN SUB SUB CATEGORY IN FOOD INVENTORY ********************** */
  openFoodInventorySubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryFoodInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN FOOD INVENTORY ********************** */
  openFoodInventorySubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeFoodInventoryComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
      // height: '400px'
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllFoodInventoryCategory()
    })
  }

  ///////////////////////////////change status for item in inventory
  softDeleteItem(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteItem(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['sucess'] == true) {
        if (event.checked) {

          Swal.fire('Item Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Item Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllItemsFoodInventory();
    })

  }
  ///////////////////////////////change status for equipment in inventory
  softDeleteEquipment(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteEquipment(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['sucess'] == true) {
        if (event.checked) {

          Swal.fire('Equipment Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Equipment Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllEquipmentAssetInventory()
    })

  }
  // loadSuitofAssetInventory()  {
  //   // this.getAllAssetInventoryCategory();
  //   this.allAssetInventoryCategory=[]
  //   this.menuService.getAllAssetInventoryCategories().subscribe(data=>{
  //     //console.log(">>>>>>>>>",data);

  //     this.allAssetInventoryCategory=data['data']
  //     //console.log(this.allAssetInventoryCategory);
  //     this.allAssetInventoryCategory.forEach((element, index) => {
  //       //console.log("index", index);

  //     element.showChildren = false;
  //     if (
  //     this.allAssetInventoryCategory[index].hasOwnProperty('subCategories')
  //     &&
  //     this.allAssetInventoryCategory[index].subCategories.length > 0
  //     ) {


  //     this.allAssetInventoryCategory[index].subCategories.forEach((element, subcategoryIndex = index) => {
  //     element.showChildren = false;
  //     if (
  //     this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
  //     &&
  //     this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.length > 0
  //     ) {
  //     // tslint:disable-next-line: max-line-length
  //     this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
  //     element.showChildren = false;
  //     if (
  //     // tslint:disable-next-line: max-line-length
  //     this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
  //     &&
  //     // tslint:disable-next-line: max-line-length
  //     this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
  //     ) {
  //     // tslint:disable-next-line: max-line-length
  //     //console.log(element);
  //      this.allAssetInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, subSubCategoryTypeIndex = index) => {
  //     element.showChildren = false;
  //     })
  //     }


  //     })
  //     }
  //     })


  //   //console.log(element);
  //   }
  //     else{
  //       //console.log(element);

  //     }
  //     });
  //     //console.log('this.allInHouseBeveragesCategories >',this.allAssetInventoryCategory)
  //   })

  //   // //console.log(this.allAssetInventoryCategory);

  //   }

  loadSuitofFoodInventory() {
    // this.getAllAssetInventoryCategory();
    this.allFoodInventoryCategory = []
    this.menuService.getAllFoodInventoryCategories().subscribe(data => {
      //console.log(">>>>>>>>>",data);

      this.allFoodInventoryCategory = data['data']
      //console.log(this.allFoodInventoryCategory);
      this.allFoodInventoryCategory.forEach((element, index) => {
        //console.log("index", index);

        element.showChildren = false;
        if (
          this.allFoodInventoryCategory[index].hasOwnProperty('subCategories')
          &&
          this.allFoodInventoryCategory[index].subCategories.length > 0
        ) {


          this.allFoodInventoryCategory[index].subCategories.forEach((element, subcategoryIndex = index) => {
            element.showChildren = false;
            if (
              this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
              &&
              this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
                element.showChildren = false;
                if (
                  // tslint:disable-next-line: max-line-length
                  this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  //console.log(element);
                  this.allFoodInventoryCategory[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, subSubCategoryTypeIndex = index) => {
                    element.showChildren = false;
                  })
                }


              })
            }
          })


          //console.log(element);
        }
        else {
          //console.log(element);

        }
      });
      //console.log('this.allInHouseBeveragesCategories >',this.allAssetInventoryCategory)
    })

    // //console.log(this.allAssetInventoryCategory);

  }
  getadmin() {
    this.menuService.getAdmin().subscribe(data => {
      console.log("users", data);
      data['data'].forEach(element => {
        this.admin.push(element['userName'])
      });
      console.log(this.admin);

    })
  }
  filterEquipmentvalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceEquipment.data = this.equipment_data
    } else {
      let equipment_filter = this.equipment_data
      this.dataSourceEquipment.data = this.equipment_data
      console.log(equipment_filter);

      let data = []
      let filter

      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = equipment_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        equipment_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = equipment_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        equipment_filter = filter
      }
      if (obj.hasOwnProperty('root') && obj['root'].length > 0) {
        filter = equipment_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['root'].includes(d['root']['root'])
          return result
        });
        equipment_filter = filter


      }

      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceEquipment = new MatTableDataSource<any>(data);
      this.noData11 = this.dataSourceEquipment.connect().pipe(map(data => data.length === 0));
      this.dataSourceEquipment.paginator = this.equipmentpaginator;
      this.dataSourceEquipment.sort = this.equipmentsort;

    }
  }
  resetEquipmentfilter() {
    this.dataSourceEquipment = new MatTableDataSource<any>(this.equipment_data);
      this.noData11 = this.dataSourceEquipment.connect().pipe(map(data => data.length === 0));
      this.dataSourceEquipment.paginator = this.equipmentpaginator;
      this.dataSourceEquipment.sort = this.equipmentsort;
  }

  /////////////////////////////////////////////// filter ingredient /////////////////////////////////////
  filterIngredientvalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceIngredient.data = this.ingredient_data
    } else {
      let ingredient_filter = this.ingredient_data
      this.dataSourceIngredient.data = this.ingredient_data
      console.log(ingredient_filter);

      let data = []
      let filter

      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = ingredient_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        ingredient_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = ingredient_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        ingredient_filter = filter
      }
      if (obj.hasOwnProperty('root') && obj['root'].length > 0) {
        filter = ingredient_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['root'].includes(d['root']['root'])
          return result
        });
        ingredient_filter = filter


      }

      console.log(filter);
      data = filter
      console.log(data);
      // this.dataSourceIngredient.data = data
      this.dataSourceIngredient = new MatTableDataSource<any>(data);
      this.noData9 = this.dataSourceIngredient.connect().pipe(map(data => data.length === 0));
      this.dataSourceIngredient.paginator = this.ingredientpaginator;
      this.dataSourceIngredient.sort = this.ingredientsort;

    }
  }
  resetIngredientfilter() {
    this.dataSourceIngredient = new MatTableDataSource<any>(this.ingredient_data);
    this.noData9 = this.dataSourceIngredient.connect().pipe(map(data => data.length === 0));
    this.dataSourceIngredient.paginator = this.ingredientpaginator;
    this.dataSourceIngredient.sort = this.ingredientsort;
  }

  /////////////////////////////////////////////////////  Item filter ////////////////////////////////////////////
  filterItemvalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceInput.data = this.input_data
    } else {
      let item_filter = this.input_data
      this.dataSourceInput.data = this.input_data
      console.log(item_filter);

      let data = []
      let filter

      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = item_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        item_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = item_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        item_filter = filter
      }
      if (obj.hasOwnProperty('ingredient') && obj['ingredient'].length > 0) {
        filter = item_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['ingredient'].includes(d['ingredient']['ingredientName'])
          return result
        });
        item_filter = filter


      }
      if (obj.hasOwnProperty('brand') && obj['brand'].length > 0) {
        filter = item_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['brand'].includes(d['brand']['BrandName'])
          return result
        });
        item_filter = filter


      }

      console.log(filter);
      data = filter
      console.log(data);
      // this.dataSourceInput.data = data
      this.dataSourceInput = new MatTableDataSource<any>(data);
      this.noData10 = this.dataSourceInput.connect().pipe(map(data => data.length === 0));
      this.dataSourceInput.paginator = this.inputpaginator;
      this.dataSourceInput.sort = this.inputsort;

    }
  }
  resetItemfilter() {
   this.dataSourceInput = new MatTableDataSource<any>(this.input_data);
      this.noData10 = this.dataSourceInput.connect().pipe(map(data => data.length === 0));
      this.dataSourceInput.paginator = this.inputpaginator;
      this.dataSourceInput.sort = this.inputsort;
  }

  ////////////////////////////////////////////////////////Food inventory tree edit//////////////////////////////////////////////////////////////////////////////////////
  updatefoodTreeroot(root_i) {
    this.allFoodInventoryCategory[root_i].edit = true
  }
  updateFoodTree(id, root, i) {
    console.log(id, root);
    let obj = {
      rootCategoryName: root
    }
    this.menuService.UpdateFoodinventoryTreeRoot(id, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)
        Swal.fire("Root updated successfully", "", "success")
      this.getAllFoodInventoryCategory();
      this.getAllIngredientFoodInventory()
      this.getAllItemsFoodInventory()
      this.allFoodInventoryCategory[i].edit = false
    })
  }
  cancelRoot(root_i) {
    this.allFoodInventoryCategory[root_i].edit = false
  }
  updateTreeSub(root_i, sub_i) {
    this.allFoodInventoryCategory[root_i]['subCategories'][sub_i].edit = true
  }
  updatesubTree(id, idsub, sub, root_i, sub_i) {
    // console.log(id, idv, varient, root_i, varient_i);
    let obj = {
      subCategoryName: sub
    }
    this.menuService.updateFoodinventoryTreesub(id, idsub, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Sub Category updated successfully", "", "success")
      this.getAllFoodInventoryCategory();

      this.getAllIngredientFoodInventory()
      this.getAllItemsFoodInventory()
      this.allFoodInventoryCategory[root_i]['subCategories'][sub_i].edit = false
    })

  }
  cancelSub(root_i, sub_i) {
    this.allFoodInventoryCategory[root_i]['subCategories'][sub_i].edit = false
  }
  updateTreeSubSub(root_i, sub_i, subsub_i) {
    console.log(root_i, sub_i, subsub_i);
    
    this.allFoodInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = true
  }
  updateFoodSubSubTree(id, ids, idss, subsub, root_i, sub_i, subsub_i) {
    let obj = {
      subSubCategoryName: subsub
    }
    this.menuService.updateFoodinventoryTreesubsub(id, idss, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Sub Varient updated successfully", "", "success")
      this.getAllFoodInventoryCategory();

      this.getAllIngredientFoodInventory()
      this.getAllItemsFoodInventory()
      this.allFoodInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = false


    })

  }
  cancelSubSub(root_i, sub_i, subsub_i) {
    this.allFoodInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = false
  }
  updateTreeType(root_i, sub_i, subsub_i, type_i) {
    console.log(root_i, sub_i, subsub_i, type_i);
    this.allFoodInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = true
  }
  updatefoodtypeTree(id, ids, idss, idtype, type, root_i, sub_i, subsub_i, type_i) {
    let obj = {
      subSubCategoryTypeName: type
    }
    this.menuService.UpdateFoodinventoryTreetype(id, idss, idtype, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Type updated successfully", "", "success")
      this.getAllFoodInventoryCategory();

      this.getAllIngredientFoodInventory()
      this.getAllItemsFoodInventory()
      this.allFoodInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = false
    })

  }
  cancelType(root_i, sub_i, subsub_i, type_i) {
    this.allFoodInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = false
  }

  ////////////////////////////////////////////////////////asset inventory tree edit//////////////////////////////////////////////////////////////////////////////////////
  updateassetTreeroot(root_i) {
    this.allAssetInventoryCategory[root_i].edit = true
  }
  updateassetTree(id, root, i) {
    console.log(id, root);
    let obj = {
      rootCategoryName: root
    }
    this.menuService.UpdateAssetinventoryTreeRoot(id, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)
        Swal.fire("Root updated successfully", "", "success")
      this.getAllEquipmentAssetInventory();
      this.getAllAssetInventoryCategory()

      this.allAssetInventoryCategory[i].edit = false
    })
  }
  cancelassetRoot(root_i) {
    this.allAssetInventoryCategory[root_i].edit = false
  }
  updateassetTreeSub(root_i, sub_i) {
    this.allAssetInventoryCategory[root_i]['subCategories'][sub_i].edit = true
  }
  updateassetsubTree(id, idsub, sub, root_i, sub_i) {
    // console.log(id, idv, varient, root_i, varient_i);
    let obj = {
      subCategoryName: sub
    }
    this.menuService.upateAssetinventoryTreesub(id, idsub, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Sub Category updated successfully", "", "success")
      this.getAllAssetInventoryCategory()
      this.getAllEquipmentAssetInventory()
      this.allAssetInventoryCategory[root_i]['subCategories'][sub_i].edit = false
    })

  }
  cancelassetSub(root_i, sub_i) {
    this.allAssetInventoryCategory[root_i]['subCategories'][sub_i].edit = false
  }
  updateassetTreeSubSub(root_i, sub_i, subsub_i) {
    console.log(root_i, sub_i, subsub_i);
    
    this.allAssetInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = true
  }
  updateassetSubSubTree(id, ids, idss, subsub, root_i, sub_i, subsub_i) {
    let obj = {
      subSubCategoryName: subsub
    }
    this.menuService.updateAssetinventoryTreesubsub(id, ids, idss, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Sub Sub Category updated successfully", "", "success")
      this.getAllAssetInventoryCategory()
       this.getAllEquipmentAssetInventory()
      this.allAssetInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = false


    })

  }
  cancelassetSubSub(root_i, sub_i, subsub_i) {
    this.allAssetInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = false
  }
  updateassetTreeType(root_i, sub_i, subsub_i, type_i) {
    console.log(root_i, sub_i, subsub_i, type_i);
    this.allAssetInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = true
  }
  updateassettypeTree(id, ids, idss, idtype, type, root_i, sub_i, subsub_i, type_i) {
    let obj = {
      subSubCategoryTypeName: type
    }
    this.menuService.UpdateAssetinventoryTreetype(id, ids, idss, idtype, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Type updated successfully", "", "success")
           this.getAllAssetInventoryCategory()
      this.getAllEquipmentAssetInventory()
      this.allAssetInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = false
    })

  }
  cancelassetType(root_i, sub_i, subsub_i, type_i) {
    this.allAssetInventoryCategory[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = false
  }

}
