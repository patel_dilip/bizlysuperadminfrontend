import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateIngrdientFoodInventoryComponent } from './update-ingrdient-food-inventory.component';

describe('UpdateIngrdientFoodInventoryComponent', () => {
  let component: UpdateIngrdientFoodInventoryComponent;
  let fixture: ComponentFixture<UpdateIngrdientFoodInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateIngrdientFoodInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateIngrdientFoodInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
