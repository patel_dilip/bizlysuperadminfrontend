import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from '../../../../_services/_menuServices/add-menu-foods.service';
import { Router } from '@angular/router';
import { RootCategoryFoodInventoryComponent } from '../../menu-page/food-inventory-category-dailogs/root-category-food-inventory/root-category-food-inventory.component';
import { SubCategoryFoodInventoryComponent } from '../../menu-page/food-inventory-category-dailogs/sub-category-food-inventory/sub-category-food-inventory.component';
import { SubSubCategoryFoodInventoryComponent } from '../../menu-page/food-inventory-category-dailogs/sub-sub-category-food-inventory/sub-sub-category-food-inventory.component';
import { SubSubCategoryTypeFoodInventoryComponent } from '../../menu-page/food-inventory-category-dailogs/sub-sub-category-type-food-inventory/sub-sub-category-type-food-inventory.component';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-update-ingrdient-food-inventory',
  templateUrl: './update-ingrdient-food-inventory.component.html',
  styleUrls: ['./update-ingrdient-food-inventory.component.scss']
})
export class UpdateIngrdientFoodInventoryComponent implements OnInit {

   addingredientForm : FormGroup
  units:string[]=['Liter',"Kg"]
  allFoodInventoryCategory:any 
  loginUserID:any
  

    response: string
    multiResponse = []
    ingredientObject:any
    ingredientID:any
    rootcategory:any
    subcategory: any;
  subsubcategory: any;
  typecategory: any;
  root: any;
  sub: any;
  subsub: any;
  type: any;
  ingredientcategory={}
  category: { id: any; category: any; };
    constructor(private fb:FormBuilder, private menuService: AddMenuFoodsService, 
     private router: Router, private dialog: MatDialog) {
         //get the ingrdient object for update
    this.ingredientObject = this.menuService.editObject

    console.log("edit obj", this.ingredientObject);
    if (this.ingredientObject == undefined) {
      this.router.navigateByUrl("/inventory")
    }
     
        this.getAllFoodInventoryCategory()
  
         //get login user details
      let loginUser = JSON.parse(localStorage.getItem('loginUser'))
      this.loginUserID = loginUser._id
      console.log(this.loginUserID);
      this.ingredientForm()
     console.log("a",this.ingredientObject);
     if(this.ingredientObject['root']!=undefined){
     this.rootcategory=this.ingredientObject['root']['root']
     }
     if(this.ingredientObject['sub']!=undefined){
     this.subcategory=this.ingredientObject['sub']['sub'];
     }
     if(this.ingredientObject['subsub']!=undefined){
   this.subsubcategory=this.ingredientObject['subsub']['subsub'];
     }
     if(this.ingredientObject['type']!=undefined){
   this.typecategory=this.ingredientObject['type']['type'];
     }
      
      this.addingredientForm.patchValue({
        ingredientName:this.ingredientObject['ingredientName'],
        ingredientcategory: this.ingredientObject['ingredientcategory'],
        unit:this.ingredientObject['unit'],
        sub:this.ingredientObject['sub'],
        subsub:this.ingredientObject['subsub'],
        root:this.ingredientObject['root'],
        type:this.ingredientObject['type']
      })
      this.ingredientID= this.ingredientObject._id
      this.ingredientcategory['category']=this.ingredientObject.ingredientcategory['category']
      this.sub=this.ingredientObject['sub']
      this.subsub=this.ingredientObject['subsub']
      this.root=this.ingredientObject['root']
      this.type=this.ingredientObject['type']
      console.log(this.root ,">", this.sub, ">", this.subsub, ">", this.type);
       }
  
    ngOnInit() {
     
    } 
   
    ingredientForm(){
      this.addingredientForm=this.fb.group({
        ingredientName:['',[Validators.required,Validators.pattern('^[ a-zA-Z]+$') ]],
        ingredientcategory:{},
        unit:['',[Validators.required]],
        userid: this.loginUserID,
        root:{},
        sub:{},
        subsub:{},
        type:{}
      })
    }
  
    getAllFoodInventoryCategory() {
      this.menuService.getAllFoodInventoryCategories().subscribe(data => {
        console.log("all food inventory categories", data);
        this.allFoodInventoryCategory = data['data']
      })
    }
  
  
    //*************************OPEN ROOT CATEGORY IN FOOD INVENTORY ********************** */
    openFoodInventoryRootCategoryDailog() {
      const dialogRef = this.dialog.open(RootCategoryFoodInventoryComponent, {
        width: '460px',
        disableClose: true,
  
        // height: '400px'
      })
      dialogRef.afterClosed().subscribe(result => {
        this.getAllFoodInventoryCategory()
      })
    }
    //*************************OPEN SUB CATEGORY IN FOOD INVENTORY ********************** */
    openFoodInventorySubCategoryDialog(rootCategoryid) {
      const dialogRef = this.dialog.open(SubCategoryFoodInventoryComponent, {
        width: '460px',
        disableClose: true,
        data: rootCategoryid
        // height: '400px'
      })
      dialogRef.afterClosed().subscribe(result => {
        this.getAllFoodInventoryCategory()
      })
    }
  
    //*************************OPEN SUB SUB CATEGORY IN FOOD INVENTORY ********************** */
    openFoodInventorySubSubCategoryDialog(rootCategoryID, subCategoryID) {
      const dialogRef = this.dialog.open(SubSubCategoryFoodInventoryComponent, {
        width: '460px',
        disableClose: true,
        data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
        // height: '400px'
      })
      dialogRef.afterClosed().subscribe(result => {
        this.getAllFoodInventoryCategory()
      })
    }
  
  
    //*************************OPEN SUB SUB CATEGORY TYPE IN FOOD INVENTORY ********************** */
    openFoodInventorySubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
      const dialogRef = this.dialog.open(SubSubCategoryTypeFoodInventoryComponent, {
        width: '460px',
        disableClose: true,
        data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
        // height: '400px'
      })
      dialogRef.afterClosed().subscribe(result => {
        this.getAllFoodInventoryCategory()
      })
    }
  
  
     //open tree node and close
     caretClick() {
      var toggler = document.getElementsByClassName("caret");
      var i;
      for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function () {
          this.parentElement.querySelector(".nested").classList.toggle("active");
          this.classList.toggle("caret-down");
        });
      }
    }
  
  
    addingrdient(){
      // this.addingredientForm.patchValue({
      //   sub:this.sub,
      //   subsub:this.subsub,
      //   root:this.root,
      //   type:this.type
      //   // ingredientcategory:{category:}
  
      // })
     
      console.log(this.addingredientForm.value);
      this.menuService.updateIngredientInventory(this.ingredientID ,this.addingredientForm.value).subscribe(res=>{
        console.log(res);
         if (res['sucess'] == true) {
  
          Swal.fire('Ingredient Updated Successfully', '', 'success')
          this.router.navigate(['/inventory'])
        }
        else {
          Swal.fire('Failed to Update Ingredient', 'Something went wrong', 'warning')
        }
      })
    }
    getRadiovalue(r?:any,sub?:any,subsub?:any, type?:any){
      console.log(r,"<", sub,"<", subsub ,"<",type);
      this.rootcategory=''
      this.subcategory=''
      this.subsubcategory=''
      this.typecategory=''
      this.root={root:r['rootCategoryName'],id:r['_id']}
      if(sub!=undefined){
      this.sub={sub:sub['subCategoryName'],id:sub['_id']}
      }
      if(subsub!=undefined){
      this.subsub={subsub:subsub['subSubCategoryName'],id:subsub['_id']}
      }
      if(type!=undefined){
      this.type={type:type['subSubCategoryTypeName'],id:type['_id']}
      }
      console.log(r,">", sub,">", subsub,">", type);
      
    
      this.addingredientForm.patchValue({
root:this.root,
sub:this.sub,
subsub:this.subsub,
type:this.type
      })
    
    
    }
    getradio(id, category){
    
      console.log(id, "<", category);
      this.category={id:id,category:category}
        
        }
  }
  