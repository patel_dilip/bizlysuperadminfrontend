import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentAttributesComponent } from './equipment-attributes.component';

describe('EquipmentAttributesComponent', () => {
  let component: EquipmentAttributesComponent;
  let fixture: ComponentFixture<EquipmentAttributesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipmentAttributesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentAttributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
