import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { CreateAttributeSetComponent } from '../../menu-page/create-attribute-set/create-attribute-set.component';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { ViewLiquorAttributeComponent } from '../../menu-page/view-liquor-attribute/view-liquor-attribute.component';
import { AddLiquorAttributeGroupComponent } from '../../menu-page/add-liquor-attribute-group/add-liquor-attribute-group.component';
import { AddLiquorAttributeSetComponent } from '../../menu-page/add-liquor-attribute-set/add-liquor-attribute-set.component';
import { ViewLiquorAttributeSetComponent } from '../../menu-page/view-liquor-attribute-set/view-liquor-attribute-set.component';
import { UpdateAttributeGroupSetComponent } from '../../menu-page/update-attribute-group-set/update-attribute-group-set.component';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { map } from 'rxjs/operators';

export interface equipmentAttributeElement {
  attributeCode: string;
  attributeName: string;
  responseType: string;
  searchable: boolean;
  filterable: boolean;
  status: boolean;
  addedBy: string;
  updatedAt: string;
}

export interface equipmentAttributeSetElement {
  attributeSetCode: string;
  attributeSetName: string;
  assignGroups: string;
  status: boolean;
  addedBy: string;
  updatedAt: string;
}



@Component({
  selector: 'app-equipment-attributes',
  templateUrl: './equipment-attributes.component.html',
  styleUrls: ['./equipment-attributes.component.scss']
})
export class EquipmentAttributesComponent implements OnInit {
  arrtibutefilter={filter:[],search:[],addedby: [], status:[]}
  arrtibuteSetfilter={addedby: [], status:[]}
  //equipmentAttribute_DATA: equipmentAttributeElement
  displayColumnsequipmentAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceequipmentAttribute: MatTableDataSource<equipmentAttributeElement>
  @ViewChild("equipmentAttributepaginator", { static: true }) equipmentAttributepaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) equipmentAttributesort: MatSort;

  //equipmentAttributeSet_DATA: equipmentAttributeSetElement
  displayColumnsEquipmentSet: string[] = ['attributeSetCode', 'attributeSetName', 'assignGroups', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceEquipmentSet: MatTableDataSource<equipmentAttributeSetElement>
  @ViewChild("equipmentSetpaginator", { static: true }) equipmentSetpaginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) equipmentSetSort: MatSort;

  allAttributeGroup = []
  date: any
  allUnassigned: any

  equipmentAttributeSetJSONDATA = []
  equipmentAttributeJSONDATA = []
  noData1:any
  noData2:any
noData3:any
  equipmentattribute_data: any;
  EquipmentSet_data: any;
  admin=[];
  constructor(public dialog: MatDialog, private menuService: AddMenuFoodsService, private router: Router) {
    this.getAllAttribute()
    this.getAllAttributeGroup()
    this.getAllAttributeSet()
    this.getadmin()
    var d = new Date()
    this.date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  }

  ngOnInit() {



  }
  getadmin() {
    this.menuService.getAdmin().subscribe(data => {
      console.log("users", data);
      data['data'].forEach(element => {
        this.admin.push(element['userName'])
      });
      console.log(this.admin);

    })
  }

  //get all equipment attributes
  getAllAttribute() {
    this.menuService.getAllLiquorAttribute().subscribe(data => {
      console.log("all attribute", data);
      if(data['sucess']==false){
        this.noData1=0
      }else{

      data['data'].forEach(element => {
        element.attributeCode = "A" + element.attributeCode
      });
      let allAttributes = data['data'].filter(element => element.attributeType == "equipment")
      this.dataSourceequipmentAttribute = new MatTableDataSource(allAttributes);
      this.dataSourceequipmentAttribute.paginator = this.equipmentAttributepaginator;
      this.dataSourceequipmentAttribute.sort = this.equipmentAttributesort
      this.equipmentattribute_data=this.dataSourceequipmentAttribute.data
      this.noData1 = this.dataSourceequipmentAttribute.connect().pipe(map(data => data.length === 0));
      this.allUnassigned = allAttributes.filter(ele => ele['isAssign'] == false)

      var equipmentAttribute = data['data'].filter(element => element.attributeType == "equipment")
      equipmentAttribute.forEach(element => {
        var optionLabels =[]
        element.responseType.options.forEach(ele => {
          optionLabels.push(ele.optionLable)
        });
        var d = new Date(element.updatedAt)
        var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
        let equipmentObj = {}
        equipmentObj = {
          "attributeCode": element.attributeCode,
          "attributeName": element.attributeName,
          "responseType": element.responseType.elementName,
          "fieldValue":  element.responseType.fieldValue,
          "options": optionLabels.toString(),
          "isSearchable": element.isSearchable,
          "isFilterable": element.isFilterable,
          "isAssign": element.isAssign,
          "status": element.status,
          "addedBy": element.addedBy.userName,
          "updatedBy": element.updatedBy.userName,
          "updatedAt": date
        }
        this.equipmentAttributeJSONDATA.push(equipmentObj)
      });
      console.log("retail food attribute export", this.equipmentAttributeJSONDATA);
      }
    })
    
  }
  liquorAttributeFilterable(element, event) {
    console.log(event.checked);
    console.log(element._id);

    this.menuService.updateLiquorAttributeIsFilterable(element._id, { "isFilterable": event.checked }).subscribe(res => {
      console.log(res);
      if (res['sucess'] == true) {

        if(event.checked){
       
          Swal.fire('Filterable Status Activated Successfully','','success')
        }
        else{
            
          Swal.fire('Filterable Status Deactivated Successfully ','','warning')
        }
      }
      else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllAttribute();
    })

  }

  //change searchable status of liquor attribute
  liquorAttributeSearchable(element, event) {
    console.log(event.checked);
    console.log(element._id);

    this.menuService.updateLiquorAttributeIsSearchable(element._id, { "isSearchable": event.checked }).subscribe(res => {
      console.log(res);
      if (res['sucess'] == true) {

        if(event.checked){
       
          Swal.fire('Searchable status Activated Successfully','','success')
        }
        else{
            
          Swal.fire('Searchable status Deactivated Successfully ','','warning')
        }
      }
      else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllAttribute();
    })

  }

    // export equipment attribute excel 
    readAsCSVEquipmentAttribute() {
      var options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        //  showTitle: true,
        //  title: 'Your title',
        useBom: true,
        noDownload: false,
        headers: ["Attribute Code", "Attribute Name", "Response Type", "Field Value", "Options", "Searchable", "Filterable", "Assign","Status", "Added By", "Updated By", "Updated At"]
      };
      new ngxCsv(this.equipmentAttributeJSONDATA, 'Equipment_Attributes_Export_' + this.date, options);
    }

    
  //view single  attribute
  viewAttribute(obj): void {
    console.log(obj);
    const dialogRef = this.dialog.open(ViewLiquorAttributeComponent, {
      data: obj,
      disableClose: true,
      width: "400px"
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }


  //update attribute
  editAttribute(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-attribute")
  }

  //delete attribute
  deleteAttribute(obj) {
    console.log("delete", obj);
    this.menuService.deleteAttribute(obj._id).subscribe(res => {
      console.log(res);
      if (res['sucess'] == true) {
        Swal.fire('Attribute Delete Successfully', '', 'success')
        this.getAllAttribute()
        this.router.navigate(['menu/s'])
      }
      else {
        Swal.fire('Failed to Delete Attribute', 'Something went wrong', 'warning')
      }
    })
  }


  //get all attribute set
  getAllAttributeSet(){
    this.menuService.getAllLiquorAttributeSets().subscribe(data => {
      console.log("All liquor attribute sets", data);
      if(data['sucess']==false){
        this.noData2=0;
      }else{
      data['data'].forEach(element => {
        element.attributeSetCode = "AS"+ element.attributeSetCode
      });
      this.dataSourceEquipmentSet = new MatTableDataSource(data['data'].filter(ele=>ele.attributeType=="equipment"))   
      this.EquipmentSet_data=this.dataSourceEquipmentSet.data
      this.dataSourceEquipmentSet.paginator = this.equipmentSetpaginator;

      this.dataSourceEquipmentSet.sort = this.equipmentSetSort
      this.noData2 = this.dataSourceEquipmentSet.connect().pipe(map(data => data.length === 0))

      var equipmentAttributeSet = data['data'].filter(ele=>ele.attributeType=="equipment")
      equipmentAttributeSet.forEach(element => {
        var assigngGroup = []
        
        element.assignGroups.forEach(ele => {
        
          assigngGroup.push(ele.groupName)
          
        });
        var d = new Date(element.updatedAt)
        var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
        var equipmentObj = {}
        equipmentObj = {
          "attributeSetCode": element.attributeSetCode,
          "attributeSetName": element.attributeSetName,
          "assignGroups": assigngGroup.toString(),
          "status": element.status,
          "addedBy": element.addedBy.userName,
          "updatedBy": element.updatedBy.userName,
          "updatedAt": date
        }
        this.equipmentAttributeSetJSONDATA.push(equipmentObj)
      });
      console.log("equipment attribute set export", this.equipmentAttributeSetJSONDATA);
    }
    })
  }
  viewLiquorAttributeSet(obj): void {
    const dialogRef = this.dialog.open(ViewLiquorAttributeSetComponent, {
      data: obj,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      ////console.log('The dialog was closed');
      // this.animal = result;
    });
  }

   // export equipment attribute set excel 
   readAsCSVEquipmentAttributeSet() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["attributeSetCode", "attributeSetName", "assignGroups", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.equipmentAttributeSetJSONDATA, 'Equipment_Attribute_Set_Export_' + this.date, options);
  }


  //get all attribute groups
  getAllAttributeGroup(){
  this.menuService.getLiquorAttributeGroup().subscribe(data => {
      console.log("All liquor attribute group", data);
    
      this.allAttributeGroup = data['data'].filter(ele=>ele.attributeType=="equipment")
    })
  }

   //add attribute set popup
   addAttributeSet(): void {
    const dialogRef = this.dialog.open(AddLiquorAttributeSetComponent, {
    data: "equipment",
    disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
     this.getAllAttributeSet()
    });
  }


  

  //view Attribute set popup
  viewAttributeSet(obj): void {
    const dialogRef = this.dialog.open(ViewLiquorAttributeSetComponent, {
      data: obj,
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }


//update attribute set
editAttributeSet(obj){
  //console.log(obj);
  
  this.menuService.editObject = obj
  //console.log("",this.menuService.editObject);
  
  this.router.navigate(['/menu/update-attributeset'])
 
}


  // add attribute group
  addAttributeGroup(): void {
    const dialogRef = this.dialog.open(AddLiquorAttributeGroupComponent, {
      data: "equipment",
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
      this.getAllAttribute()
      this.getAllAttributeGroup()
    });
  }



  // SOFT DELETE  ATTRIBUTE
 softDeleteAttribute(element, event){
  console.log(event.checked);
  console.log(element._id);
  this.menuService.softDeleteAttribute(element._id, { "status": event.checked }).subscribe(res => {
    console.log(res);

    if(res['sucess']==true){
      Swal.fire('Status Changed Successfully', '', 'success')
    }else{
      Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
    }
  })
  this.getAllAttribute();
}

  // SOFT DELETE ATTRIBUTE SET
  softDeleteAttributeSet(element, event){
    console.log(event.checked);
    console.log(element._id);
    this.menuService.softDeleteAttributeSet(element._id, { "status": event.checked }).subscribe(res => {
      console.log(res);
  
      if(res['sucess']==true){
        Swal.fire('Status Changed Successfully', '', 'success')
      }else{
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllAttributeSet();
    })
  }


  applyFilterequipmentAttribute(event) {
    this.dataSourceequipmentAttribute.data= this.equipmentattribute_data
    console.log( this.equipmentattribute_data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceequipmentAttribute.data.filter(function (d) {
      console.log(d);
      let result
    
               result = d['attributeCode'].toLowerCase().indexOf(val) !== -1 || d['attributeName'].toLowerCase().indexOf(val) !== -1
               return result
    
              });
     

    console.log(filter);
    this.dataSourceequipmentAttribute.data = filter;
  }


  applyFilterequipmentAttributeSet(event) {
    this.dataSourceEquipmentSet.data= this.EquipmentSet_data
   // console.log(this.beverageattributeset_data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceEquipmentSet.data.filter(function (d) {
      console.log(d);
      let result
    
               result = d['attributeSetCode'].toLowerCase().indexOf(val) !== -1 || d['attributeSetName'].toLowerCase().indexOf(val) !== -1
               return result
    
              });
     

    console.log(filter);
    this.dataSourceEquipmentSet.data = filter;

}
//////////////////////////////// filter attribute ///////////////////////////////////
filterAttributevalue(obj){
      Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined|| obj[key].length == 0) && delete obj[key]);

  console.log(obj);
   if (Object.keys(obj).length === 0) {
    this.dataSourceequipmentAttribute.data = this.equipmentattribute_data
  } else {
    let attribute_filter = this.equipmentattribute_data
    this.dataSourceequipmentAttribute.data = this.equipmentattribute_data
    console.log(attribute_filter);

    let data = []
    let filter
    if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0 ) {
      filter = attribute_filter.filter(function (d) {
        console.log(d);
        let result
        result = obj['addedby'].includes(d['addedBy']['userName'])
        return result
      });
      attribute_filter = filter
    }
    if (obj.hasOwnProperty('status') && obj['status'].length > 0 ) {
      filter = attribute_filter.filter(function (d) {
        // console.log(d);
        let result
        result = obj['status'].includes(d['status'])
        return result
      });
      attribute_filter = filter
    }
    if (obj.hasOwnProperty('filter') && obj['filter'].length > 0 ) {
      filter = attribute_filter.filter(function (d) {
        // console.log(d);
        let result
        result = obj['filter'].includes(d['isFilterable'])
        return result
      });
      attribute_filter = filter
    }
    if (obj.hasOwnProperty('search') && obj['search'].length > 0 ) {
      filter = attribute_filter.filter(function (d) {
        // console.log(d);
        let result
        result = obj['search'].includes(d['isSearchable'])
        return result
      });
      attribute_filter = filter
    }
   
    
    console.log(filter);
    data = filter
    // this.dataSourceequipmentAttribute.data = data

    console.log(data);
    // this.dataSourceequipmentAttribute = new MatTableDataSource<any>(data)
    // this.dataSourceequipmentAttribute.paginator = this.equipmentAttributepaginator;
    // this.dataSourceequipmentAttribute.sort = this.equipmentAttributesort
    // this.noData1 = this.dataSourceequipmentAttribute.connect().pipe(map(data => data.length === 0))
    this.dataSourceequipmentAttribute = new MatTableDataSource<any>(data);
    this.noData1 = this.dataSourceequipmentAttribute.connect().pipe(map(data => data.length === 0));
    this.dataSourceequipmentAttribute.paginator = this.equipmentAttributepaginator;
        this.dataSourceequipmentAttribute.sort = this.equipmentAttributesort;
  }

 
}
filterAttributeSetvalue(obj){
      Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined|| obj[key].length == 0) && delete obj[key]);

  console.log(obj);
   if (Object.keys(obj).length === 0) {
    this.dataSourceEquipmentSet.data = this.EquipmentSet_data
  } else {
    let attributeSet_filter = this.EquipmentSet_data
    this.dataSourceEquipmentSet.data = this.EquipmentSet_data
    console.log(attributeSet_filter);

    let data = []
    let filter
    if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0 ) {
      filter = attributeSet_filter.filter(function (d) {
        console.log(d);
        let result
        result = obj['addedby'].includes(d['addedBy']['userName'])
        return result
      });
      attributeSet_filter = filter
    }
    if (obj.hasOwnProperty('status') && obj['status'].length > 0 ) {
      filter = attributeSet_filter.filter(function (d) {
        // console.log(d);
        let result
        result = obj['status'].includes(d['status'])
        return result
      });
      attributeSet_filter = filter
    }
      
    console.log(filter);
    // data = 
    // this.dataSourceEquipmentSet.data = filter
    this.dataSourceEquipmentSet = new MatTableDataSource<any>(data)
    this.dataSourceEquipmentSet.paginator = this.equipmentSetpaginator;
    this.dataSourceEquipmentSet.sort = this.equipmentSetSort 
    this.noData2 = this.dataSourceEquipmentSet.connect().pipe(map(data => data.length === 0))
    
    console.log(data);

  }

 
 }

 resetAttributefilter(){
  //  debugger;
  // this.dataSourceequipmentAttribute=this.equipmentattribute_data 
  this.dataSourceequipmentAttribute = new MatTableDataSource<any>(this.equipmentattribute_data)
    this.dataSourceequipmentAttribute.paginator = this.equipmentAttributepaginator;
    this.dataSourceequipmentAttribute.sort = this.equipmentAttributesort 
   this.noData1 = this.dataSourceequipmentAttribute.connect().pipe(map(data => data.length === 0))
    

  
  
 }
 resetAttributeSetfilter(){
  this.dataSourceEquipmentSet = new MatTableDataSource<any>(this.EquipmentSet_data)
    this.dataSourceEquipmentSet.paginator = this.equipmentSetpaginator;
    this.dataSourceEquipmentSet.sort = this.equipmentSetSort 
    this.noData2 = this.dataSourceEquipmentSet.connect().pipe(map(data => data.length === 0))
    
 }
}
