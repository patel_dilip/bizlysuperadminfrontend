import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBeverageProductComponent } from './add-beverage-product.component';

describe('AddBeverageProductComponent', () => {
  let component: AddBeverageProductComponent;
  let fixture: ComponentFixture<AddBeverageProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBeverageProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBeverageProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
