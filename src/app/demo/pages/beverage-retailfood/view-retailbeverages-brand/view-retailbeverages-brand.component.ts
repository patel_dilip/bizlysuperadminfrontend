import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';

@Component({
  selector: 'app-view-retailbeverages-brand',
  templateUrl: './view-retailbeverages-brand.component.html',
  styleUrls: ['./view-retailbeverages-brand.component.scss']
})
export class ViewRetailbeveragesBrandComponent implements OnInit {
  getAllRetailFoodCategories() {
    throw new Error("Method not implemented.");
  }

  
  allRetailFoodCategories=[];
  
  categoryType=[];
  retailFoodtree=[];
  allRetailBeveragesCategories=[];
  Beveragestree=[];


  constructor(public dialogRef: MatDialogRef<ViewRetailbeveragesBrandComponent>,
    @Inject(MAT_DIALOG_DATA) public data, private menuService: AddMenuFoodsService) {
    console.log(data);
    this.getAllBeveragesCategories()
    this.categoryType=data.categoryType

  }
   //open tree node and close
 caretClick() {
  var toggler = document.getElementsByClassName("caret");
  var i;
  for (i = 0; i < toggler.length; i++) {
    toggler[i].addEventListener("click", function () {
      this.parentElement.querySelector(".nested").classList.toggle("active");
      this.classList.toggle("caret-down");
    });
  }
}

  ngOnInit() {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  getAllBeveragesCategories() {
    this.menuService.getAllBeverageCategories().subscribe(data => {
      console.log("All Beverages Categories", data);
      if (data['sucess'] == false) {
        //console.log("Beverages categories data not found");
      } else {
                      this.allRetailBeveragesCategories = data['data'].filter(element => element.beverageType == "retailBeverage")
                     this.Beveragestree=[];
                      this.allRetailBeveragesCategories.forEach(element => {
                        this.categoryType.forEach(ele=>{
                          if(element['_id']==ele['_id'])
                          element=ele
                          })
                           this.Beveragestree.push(element)
                      });
                 this.allRetailBeveragesCategories=this.Beveragestree
      }
    })
  
  }

}
