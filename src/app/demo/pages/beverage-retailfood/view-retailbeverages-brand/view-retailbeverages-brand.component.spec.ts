import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRetailbeveragesBrandComponent } from './view-retailbeverages-brand.component';

describe('ViewRetailbeveragesBrandComponent', () => {
  let component: ViewRetailbeveragesBrandComponent;
  let fixture: ComponentFixture<ViewRetailbeveragesBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRetailbeveragesBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRetailbeveragesBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
