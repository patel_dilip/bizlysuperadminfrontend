import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRetailbeveragesBrandComponent } from './update-retailbeverages-brand.component';

describe('UpdateRetailbeveragesBrandComponent', () => {
  let component: UpdateRetailbeveragesBrandComponent;
  let fixture: ComponentFixture<UpdateRetailbeveragesBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateRetailbeveragesBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRetailbeveragesBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
