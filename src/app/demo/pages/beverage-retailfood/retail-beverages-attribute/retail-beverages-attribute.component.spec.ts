import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailBeveragesAttributeComponent } from './retail-beverages-attribute.component';

describe('RetailBeveragesAttributeComponent', () => {
  let component: RetailBeveragesAttributeComponent;
  let fixture: ComponentFixture<RetailBeveragesAttributeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailBeveragesAttributeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailBeveragesAttributeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
