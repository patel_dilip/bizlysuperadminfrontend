import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRetailfoodProductComponent } from './add-retailfood-product.component';

describe('AddRetailfoodProductComponent', () => {
  let component: AddRetailfoodProductComponent;
  let fixture: ComponentFixture<AddRetailfoodProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRetailfoodProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRetailfoodProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
