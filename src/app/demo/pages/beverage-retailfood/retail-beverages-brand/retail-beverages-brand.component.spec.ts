import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailBeveragesBrandComponent } from './retail-beverages-brand.component';

describe('RetailBeveragesBrandComponent', () => {
  let component: RetailBeveragesBrandComponent;
  let fixture: ComponentFixture<RetailBeveragesBrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailBeveragesBrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailBeveragesBrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
