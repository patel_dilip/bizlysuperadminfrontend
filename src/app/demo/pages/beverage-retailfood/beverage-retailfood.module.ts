import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BeverageRetailfoodHomeComponent } from './beverage-retailfood-home/beverage-retailfood-home.component';
import { BeverageRetailfoodComponent } from './beverage-retailfood/beverage-retailfood.component';
import { Routes, RouterModule } from '@angular/router';
import { MaterialModule } from '../../../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { SelectModule } from 'ng-select';
import{AddRetailfoodProductComponent}from './add-retailfood-product/add-retailfood-product.component'
import { SharedModule } from '../../../theme/shared/shared.module';
import { DataTablesModule } from 'angular-datatables';
import { NgxLoadingModule } from 'ngx-loading';
import { NgbPopoverModule, NgbTooltipModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { AddBeveragesComponent } from '../beverage-retailfood/add-beverages/add-beverages.component';
import { AddBeverageProductComponent } from '../beverage-retailfood/add-beverage-product/add-beverage-product.component';
import { RetailBeveragesAttributeComponent } from './retail-beverages-attribute/retail-beverages-attribute.component';
import { RetailBeveragesProductComponent } from './retail-beverages-product/retail-beverages-product.component';
import { RetailBeveragesBrandComponent } from './retail-beverages-brand/retail-beverages-brand.component';
import{UpdateRetailbeveragesBrandComponent}from'./update-retailbeverages-brand/update-retailbeverages-brand.component'
import{UpdateRetailbeveragesproductComponent}from'./update-retailbeveragesproduct/update-retailbeveragesproduct.component'

const routes: Routes = [  
  {
    path: '',
    component: BeverageRetailfoodHomeComponent,
    children: [
      {
        path  :'',
        component :BeverageRetailfoodComponent
      }  
      , {
        path: 'add-beverage',
        component:AddBeveragesComponent
      },
      {
        path: 'add-beverage-product',
        component: AddBeverageProductComponent
      },
      {
        path: 'add-retailfood-product',
        component: AddRetailfoodProductComponent
      },
      {
        path: 'add-retailBeverages-product',
        component: RetailBeveragesProductComponent
      },  
     
      {
        path: 'update-retailBeverages-product',
        component: UpdateRetailbeveragesproductComponent
      },  
     
      
    ]
  }
];



@NgModule({
  declarations: [BeverageRetailfoodHomeComponent,AddRetailfoodProductComponent, BeverageRetailfoodComponent, AddBeverageProductComponent, AddBeveragesComponent, RetailBeveragesAttributeComponent, RetailBeveragesProductComponent,  UpdateRetailbeveragesproductComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
     SharedModule,
    FormsModule,
    DataTablesModule,
    NgxLoadingModule.forRoot({}),
    NgbPopoverModule,
    NgbTooltipModule,
    NgbTabsetModule,
    FileUploadModule

   // sharedModule
  ]
})
export class BeverageRetailfoodModule { }
