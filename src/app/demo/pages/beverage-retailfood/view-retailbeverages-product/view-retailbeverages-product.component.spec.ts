import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRetailbeveragesProductComponent } from './view-retailbeverages-product.component';

describe('ViewRetailbeveragesProductComponent', () => {
  let component: ViewRetailbeveragesProductComponent;
  let fixture: ComponentFixture<ViewRetailbeveragesProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRetailbeveragesProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRetailbeveragesProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
