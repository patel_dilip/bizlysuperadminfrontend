import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { element } from 'protractor';
import { MatDialog } from '@angular/material';
import { AddCuisinesComponent } from 'src/app/PopoversList/add-cuisines/add-cuisines.component';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AddVarientPopoverComponent } from 'src/app/PopoversList/add-varient-popover/add-varient-popover.component';
import { RootCategoryBeverageComponent } from '../../menu-page/beverages-category-dailogs/root-category-beverage/root-category-beverage.component';
import { SubCategoryBeverageComponent } from '../../menu-page/beverages-category-dailogs/sub-category-beverage/sub-category-beverage.component';
import { SubSubCategoryBeverageComponent } from '../../menu-page/beverages-category-dailogs/sub-sub-category-beverage/sub-sub-category-beverage.component';
import { SubSubCategoryTypeBeverageComponent } from '../../menu-page/beverages-category-dailogs/sub-sub-category-type-beverage/sub-sub-category-type-beverage.component';

const URL = environment.base_Url + 'beverage/uploadphotos'


@Component({
  selector: 'app-add-beverages',
  templateUrl: './add-beverages.component.html',
  styleUrls: ['./add-beverages.component.scss']
})
export class AddBeveragesComponent implements OnInit {

  dishUnits: string[] = ['Kilogram', 'Grams', 'Liter', 'Mililiter'];
  addBeverageForm: FormGroup
  cuisineCtrl: FormControl
  selectedAlbums = []
  selectedvarients = []
  uploader: FileUploader;
  response: string
  multiResponse = []
 
  loginUserID: any
  loading = false;
  allInHouseBeveragesCategories:any
  rootCategory: any;
  varients: any;
  subVarient: any;
  type: any;
  category: { category: any; id: any; };

  constructor(private fb: FormBuilder, private matDialog: MatDialog,
    private menuService: AddMenuFoodsService, private router: Router, private dialog: MatDialog) {

    //get login user details
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID)

      this.getAllBeveragesCategories()
  }

  ngOnInit() {
    this.addBeverage()

    //  NG FILE UPLOADER CODE
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'beverages'
    });
    //file upload response with url
    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);

      this.response = JSON.parse(response)
      console.log('response', this.response['BeveragePhotoUrls']);

      this.multiResponse.push(this.response['BeveragePhotoUrls'][0])
      console.log('multi response', this.multiResponse);

      this.loading = false
    };

  }


  setLoading() {
    this.loading = true
  }

  
  //add cuisines form 
  addBeverage() {
    this.addBeverageForm = this.fb.group({
      beverageName: ['', [Validators.required, Validators.pattern('^[ a-zA-Z0-9]+$')]],
      unit: ['', Validators.required],
      veg_nonveg: ['Veg', Validators.required],
      rootCategory: {},
      varients:{} ,
      subVarient: {},
      type:{},
      images: [''],
      userid: [''],
      category:{}
    })
  }

  getroot(r?:any,sub?:any,subsub?:any, type?:any){
    this.rootCategory={rootCategory:r['rootCategoryName'],id:r['_id']}
  if(sub!=undefined){
  this.varients={varients:sub['subCategoryName'],id:sub['_id']}
  }
  if(subsub!=undefined){
  this.subVarient={subVarient:subsub['subSubCategoryName'],id:subsub['_id']}
  }
  if(type!=undefined){
  this.type={type:type['subSubCategoryTypeName'],id:type['_id']}
  }
    console.log(r,">", sub,">", subsub,">", type);
    
  }
  getRadiovalue(id, name){

    
    this.category={category:name, id:id};
    console.log(id,">>", this.category);
  }

  getAllBeveragesCategories() {
    this.menuService.getAllBeverageCategories().subscribe(data => {
      console.log("All Beverages Categories", data);
      if (data['sucess'] == false) {
        console.log("Beverages categories data not found");
      } else {
        this.allInHouseBeveragesCategories = data['data'].filter(element => element.beverageType == "inHouseBeverage")
       }
    })
  }

  //*************************OPEN ROOT CATEGORY IN BEVERAGE ********************** */
  openBeveragesRootCategoryDailog(beverageType) {
    console.log(beverageType);
    
    const dialogRef = this.dialog.open(RootCategoryBeverageComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
      data: beverageType
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }

  //*************************OPEN SUB CATEGORY IN BEVERAGE ********************** */
  openBeveragesSubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryBeverageComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
      data: rootCategoryid
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }

  //*************************OPEN SUB SUB CATEGORY IN BEVERAGE ********************** */
  openBeveragesSubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryBeverageComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN BEVERAGE ********************** */
  openBeveragesSubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeBeverageComponent, {
      width:'60%',
      height:'80%',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }

   //open tree node and close
   caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }


 
  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }

  //add final beverage
  onAddAlbum() {
   // this.loading = true
    //get selected cuisine ids and varient ids


    //patch all value to final object
    this.addBeverageForm.patchValue({
      images: this.multiResponse,
      userid: this.loginUserID
    })
    this.addBeverageForm.patchValue({
      varients:this.varients,
      subVarient:this.subVarient,
      rootCategory:this.rootCategory,
      type:this.type,
      category:this.category

    })
   
  
    console.log("form value:",this.addBeverageForm.value);
    this.menuService.addBeverage(this.addBeverageForm.value).subscribe(res => {
      this.loading = false
      console.log("add beverage", res);
      if (res['success'] == true) {

        Swal.fire('Beverage Added Successfully', '', 'success')
        this.router.navigate(['/beverages_and_retailfood'])
      }
      else {
        Swal.fire('Failed to Add Beverage', 'Something went wrong', 'warning')
      }
    })
  }




}
