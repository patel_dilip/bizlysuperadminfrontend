import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBeveragesComponent } from './add-beverages.component';

describe('AddBeveragesComponent', () => {
  let component: AddBeveragesComponent;
  let fixture: ComponentFixture<AddBeveragesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBeveragesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBeveragesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
