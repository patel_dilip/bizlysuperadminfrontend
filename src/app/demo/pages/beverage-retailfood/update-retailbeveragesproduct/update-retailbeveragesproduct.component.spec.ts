import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRetailbeveragesproductComponent } from './update-retailbeveragesproduct.component';

describe('UpdateRetailbeveragesproductComponent', () => {
  let component: UpdateRetailbeveragesproductComponent;
  let fixture: ComponentFixture<UpdateRetailbeveragesproductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateRetailbeveragesproductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRetailbeveragesproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
