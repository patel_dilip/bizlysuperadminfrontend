import { Component, OnInit } from '@angular/core';
import { FileUploadModule, FileUploader } from 'ng2-file-upload';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
//import { FileUploader } from 'ng2-file-upload';
  import { environment } from '../../../../../environments/environment';
  import Swal from 'sweetalert2';
import { RetailBeveragesBrandComponent } from '../retail-beverages-brand/retail-beverages-brand.component';
import { SubSubCategoryTypeBeverageComponent } from '../../menu-page/beverages-category-dailogs/sub-sub-category-type-beverage/sub-sub-category-type-beverage.component';
import { SubSubCategoryBeverageComponent } from '../../menu-page/beverages-category-dailogs/sub-sub-category-beverage/sub-sub-category-beverage.component';
import { RootCategoryBeverageComponent } from '../../menu-page/beverages-category-dailogs/root-category-beverage/root-category-beverage.component';
import { SubCategoryBeverageComponent } from '../../menu-page/beverages-category-dailogs/sub-category-beverage/sub-category-beverage.component';
  const URL = environment.base_Url + 'retailbeverageproduct/uploadphoto'

@Component({
  selector: 'app-retail-beverages-product',
  templateUrl: './retail-beverages-product.component.html',
  styleUrls: ['./retail-beverages-product.component.scss']
})

export class RetailBeveragesProductComponent implements OnInit {

 
  
  addProductForm: FormGroup
   
    attributes = []
    units = ['Gram',"Kg"]
    brands = []
    allRetailBeveragesCategories: any
    loginUserID: any
  uploader: FileUploader
    response: string;
  allBrands= [];
  set=[];
  attribute=[];
  attributeData: Object;
  count: number;
  attribute_setlist=[];
  disableBrandDropdown: boolean;
  countbrand: number;
  category: { category: any; id: any; };
  filteredArr=[];
  inputbox: boolean;
  inputBoxPlaceholder: any;
  radiobutton: boolean;
  yesorNo: boolean;
  checkbox: boolean;
  radioOptions: any;
  checkBoxOptions: any;
  rootCategory: any;
  varient: any;
  subVarient: any;
  type: any;
    constructor(private fb: FormBuilder, private menuService: AddMenuFoodsService, 
      private router: Router, private dialog: MatDialog) { 
      
      //get login user details
      let loginUser = JSON.parse(localStorage.getItem('loginUser'))
      this.loginUserID = loginUser._id
      console.log(this.loginUserID);
  
      this.getAllBeveragesCategories()
     // this.getAllRetailFoodBrands()
     // this.getAllAttributeSet()
    }
  
    ngOnInit() {
          //  NG FILE UPLOADER CODE
          this.uploader = new FileUploader({
            url: URL,
            itemAlias: 'retailbeverageproduct'
      
          });
      
          this.response = '';
      
          this.uploader.response.subscribe(res => this.response = res);
      
          this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            console.log('FileUpload:uploaded:', item, status, response);
      
            this.addProductForm.patchValue({
              productImage: response
            })
            
          }
      this.addproductFormfunction()
    }
  
  addproductFormfunction(){
    this.addProductForm = this.fb.group({
      productName: ['',Validators.required],
      rootCategory: {},
      varient: {},
      subVarient: {},
      type: {},
      attributeSet: [],
      unit: ['', Validators.required],
      associatedBrand: ['',Validators.required],
      productImage: [''],
      userid: this.loginUserID,
     category:{}

    })
  }
  
   
   
  
    getAllBeveragesCategories() {
      this.menuService.getAllBeverageCategories().subscribe(data => {
        console.log("All Beverages Categories", data);
        if (data['sucess'] == false) {
          //console.log("Beverages categories data not found");
        } else {
          // this.allRetailBeveragesCategories = data['data'].filter(element => element.beverageType == "inHouseBeverage")
          // console.log("In House>>>", this.allRetailBeveragesCategories);
    
                        this.allRetailBeveragesCategories = data['data'].filter(element => element.beverageType == "retailBeverage")
          //console.log("Retail Beverage>>>", this.allRetailBeveragesCategories);
          // this.loadSuit();
    
        }
    
    
      })
    
    }
    openBeveragesSubSubCategoryTypeDialog(rootCategoryID, subsubCategortyID) {
      const dialogRef = this.dialog.open(SubSubCategoryTypeBeverageComponent, {
        width: '460px',
        disableClose: true,
        data: { "rootCategoryID": rootCategoryID, "subsubCategortyID": subsubCategortyID }
      })
      dialogRef.afterClosed().subscribe(result => {
        this.getAllBeveragesCategories()
      })
    }
    openBeveragesSubSubCategoryDialog(rootCategoryID, subCategoryID) {
      const dialogRef = this.dialog.open(SubSubCategoryBeverageComponent, {
        width: '460px',
        disableClose: true,
        data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
      })
      dialogRef.afterClosed().subscribe(result => {
        this.getAllBeveragesCategories()
      })
    }
    openBeveragesRootCategoryDailog(beverageType) {
      const dialogRef = this.dialog.open(RootCategoryBeverageComponent, {
        width: '460px',
        disableClose: true,
        data: beverageType
      })
      dialogRef.afterClosed().subscribe(result => {
        this.getAllBeveragesCategories()
      })
    }
    openBeveragesSubCategoryDialog(rootCategoryid) {
      const dialogRef = this.dialog.open(SubCategoryBeverageComponent, {
        width: '460px',
        disableClose: true,
        data: rootCategoryid
      })
      dialogRef.afterClosed().subscribe(result => {
        this.getAllBeveragesCategories()
      })
    }
  
    //get all beverage brands
    getAllRetailFoodBrands() {
      this.menuService.getAllRetailBeveargesBrands().subscribe(data => {
        console.log("All Retail Food Brands", data);
        this.brands = data['data']
      
      })
    }
  
  
    //get all attribute set
   
     //open tree node and close
     caretClick() {
      var toggler = document.getElementsByClassName("caret");
      var i;
      for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function () {
          this.parentElement.querySelector(".nested").classList.toggle("active");
          this.classList.toggle("caret-down");
        });
      }
    }
  
   
  //popup for add brand Retail food
  openpopupRetailFoodAddBrand(): void {
    const dialogRef = this.dialog.open(RetailBeveragesBrandComponent, {
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
     this.getAllRetailFoodBrands()
    });
  }
    
    onAddProduct() {
      this.addProductForm.patchValue({
        category:this.category,
        rootCategory: this.rootCategory,
        varient:this.varient,
        subVarient:  this.subVarient,
        type:this.type,
        attributeSet:this.attribute_setlist
        // associatedBrand:
        

      })
      console.log(this.addProductForm.value);
      this.menuService.addRetailBeveragesProduct(this.addProductForm.value).subscribe(res=>{
        console.log(res);
       
        if (res['success'] == true) {
  
          Swal.fire('Product Added Successfully', '', 'success')
         this.router.navigate(['beverages_and_retailfood'])
        }
        else {
          Swal.fire('Failed to Add Product', 'Something went wrong', 'warning')
        }
      })
    }
    onClickResponse(i) {

      // this.filteredArr.forEach((element, index) => {
  
      //  if(i==index-1){
      // console.log(this.filteredArr[i].responseType.elementName);
      if (this.filteredArr[i].responseType.elementName == "Input Box") {
        this.inputBoxPlaceholder = this.filteredArr[i].responseType.fieldValue
        this.radiobutton = false
        this.yesorNo = false
        this.inputbox = true
        this.checkbox = false
      }
      if (this.filteredArr[i].responseType.elementName == "Yes / No") {
  
        this.radiobutton = false
        this.yesorNo = true
        this.inputbox = false
        this.checkbox = false
      }
      if (this.filteredArr[i].responseType.elementName == "Radio Button") {
        this.radioOptions = this.filteredArr[i].responseType.options
        this.radiobutton = true
        this.yesorNo = false
        this.inputbox = false
        this.checkbox = false
      }
      if (this.filteredArr[i].responseType.elementName == "Check Box") {
        this.checkBoxOptions = this.filteredArr[i].responseType.options
        this.radiobutton = false
        this.yesorNo = false
        this.inputbox = false
        this.checkbox = true
  
      }
      // }
  
    }
    getroot(r?:any,sub?:any,subsub?:any, type?:any){
      this.rootCategory={rootCategory:r['rootCategoryName'],id:r['_id']}
    if(sub!=undefined){
    this.varient={varient:sub['subCategoryName'],id:sub['_id']}
    }
    if(subsub!=undefined){
    this.subVarient={subVarient:subsub['subSubCategoryName'],id:subsub['_id']}
    }
    if(type!=undefined){
    this.type={type:type['subSubCategoryTypeName'],id:type['_id']}
    }
      console.log(r,">", sub,">", subsub,">", type);
      
    }
    getRadiovalue(id, name){
      // fa
      this.allBrands=[]
      this.filteredArr=[]
     this.set=[]
     this.attribute_setlist=[]
      this.category={category:name, id:id};
      console.log(id,">>", this.category);
  
      let categoryOBJ = {
         id: id,
        string:name
    }
    let obj=
        {
          id: id,
      string:name,
    attributeType:"retailbeverages"
   
    }
    console.log("hi");
    console.log(categoryOBJ)
    // await new Promise(resolve => {
    this.menuService.getBrandsByCategoryinretailbeverages(categoryOBJ).subscribe(data => {
      this.countbrand=0
  
      console.log("Brand>",data);
      this.allBrands = data['data']
                  
        this.disableBrandDropdown = false
           this.allBrands = data['data']    
    })
  // console.log(";en",this.allBrands)
    
      this.menuService.getattributeDatafromotherCategories(obj).subscribe(data => {
        this.count=0
        if(data['success']==true){
      console.log("attr>",data);
      this.attributeData = data
     this.set= this.attributeData['attributeSets']
    //  this.attribute_setlist=this.attributeData['attributeSets']
    this.attributeData['attributeSets'].forEach(element => {
      
      this.attribute_setlist.push(element['attributeSetName'])
    });
    console.log("list>",this.attribute_setlist);
    // this.addProductForm.patchValue({
    //   attribute_set:this.attribute_setlist
    // })
    console.log();
    this.attributeData['groupAttribute'].forEach(element => {
      console.log(element['assignAttributes']);
      this.attribute=this.attribute.concat(element['assignAttributes'])
    }); 
    console.log(this.attribute,">");
    this.filteredArr = this.attribute.reduce((acc, current) => {
      const x = acc.find(item => item['_id'] === current['_id']);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
    console.log(this.filteredArr);
    // this.onClickResponse();
    
      }  })  
    
  // });
  setTimeout(()=> {
                
  if(this.allBrands.length==0 && this.set.length==0){
      Swal.fire('brand and Attribute not available', '', 'warning')
    }
    else if(this.set.length==0){      
      Swal.fire('Attribute not available', '', 'warning')
    } else if(this.allBrands.length==0){
      Swal.fire('Brands not available', '', 'warning')
    
    }
  }, 1000);
  
    }
  
}
