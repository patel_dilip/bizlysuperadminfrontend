import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailBeveragesProductComponent } from './retail-beverages-product.component';

describe('RetailBeveragesProductComponent', () => {
  let component: RetailBeveragesProductComponent;
  let fixture: ComponentFixture<RetailBeveragesProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailBeveragesProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailBeveragesProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
