import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeverageRetailfoodHomeComponent } from './beverage-retailfood-home.component';

describe('BeverageRetailfoodHomeComponent', () => {
  let component: BeverageRetailfoodHomeComponent;
  let fixture: ComponentFixture<BeverageRetailfoodHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeverageRetailfoodHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeverageRetailfoodHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
