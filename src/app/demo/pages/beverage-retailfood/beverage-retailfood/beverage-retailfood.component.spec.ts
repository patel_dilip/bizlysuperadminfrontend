import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeverageRetailfoodComponent } from './beverage-retailfood.component';

describe('BeverageRetailfoodComponent', () => {
  let component: BeverageRetailfoodComponent;
  let fixture: ComponentFixture<BeverageRetailfoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeverageRetailfoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeverageRetailfoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
