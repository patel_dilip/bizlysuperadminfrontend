import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import Swal from 'sweetalert2';
import { forkJoin } from 'rxjs';
import * as _ from "lodash";

import * as XLSX from 'xlsx';
import { MainService } from 'src/app/_services/main.service';
import { AddMenuFoodsService } from 'src/app/_services/_menuServices/add-menu-foods.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ngxCsv } from 'ngx-csv';
import { AddLiquorAttributeGroupComponent } from '../../menu-page/add-liquor-attribute-group/add-liquor-attribute-group.component';
import { ViewLiquorAttributeSetComponent } from '../../menu-page/view-liquor-attribute-set/view-liquor-attribute-set.component';
import { ViewBeverageBrandComponent } from '../../menu-page/view-beverage-brand/view-beverage-brand.component';
import { ViewBeverageProductComponent } from '../../menu-page/view-beverage-product/view-beverage-product.component';
import { ViewBeverageComponent } from '../../menu-page/view-beverage/view-beverage.component';
import { UpdateAttributeGroupSetComponent } from '../../menu-page/update-attribute-group-set/update-attribute-group-set.component';
import { UpdateAttributeGroupComponent } from '../../menu-page/update-attribute-group/update-attribute-group.component';
import { UpdateBeverageBrandComponent } from '../../menu-page/update-beverage-brand/update-beverage-brand.component';
import { UpdateRetailFoodBrandComponent } from '../../menu-page/update-retail-food-brand/update-retail-food-brand.component';
import { ViewLiquorAttributeComponent } from '../../menu-page/view-liquor-attribute/view-liquor-attribute.component';
import { AddLiquorAttributeSetComponent } from '../../menu-page/add-liquor-attribute-set/add-liquor-attribute-set.component';
import { AddBeverageBrandComponent } from '../../menu-page/add-beverage-brand/add-beverage-brand.component';
import { AddRetailfoodBrandComponent } from '../../menu-page/add-retailfood-brand/add-retailfood-brand.component';
import { RootCategoryBeverageComponent } from '../../menu-page/beverages-category-dailogs/root-category-beverage/root-category-beverage.component';
import { SubCategoryBeverageComponent } from '../../menu-page/beverages-category-dailogs/sub-category-beverage/sub-category-beverage.component';
import { SubSubCategoryBeverageComponent } from '../../menu-page/beverages-category-dailogs/sub-sub-category-beverage/sub-sub-category-beverage.component';
import { SubSubCategoryTypeBeverageComponent } from '../../menu-page/beverages-category-dailogs/sub-sub-category-type-beverage/sub-sub-category-type-beverage.component';
import { RootCategoryRetailFoodComponent } from '../../menu-page/retail-food-category-dailogs/root-category-retail-food/root-category-retail-food.component';
import { SubCategoryRetailFoodComponent } from '../../menu-page/retail-food-category-dailogs/sub-category-retail-food/sub-category-retail-food.component';
import { SubSubCategoryRetailFoodComponent } from '../../menu-page/retail-food-category-dailogs/sub-sub-category-retail-food/sub-sub-category-retail-food.component';
import { SubSubCategoryTypeRetailFoodComponent } from '../../menu-page/retail-food-category-dailogs/sub-sub-category-type-retail-food/sub-sub-category-type-retail-food.component';
import { element } from 'protractor';
import { UpdateRetailbeveragesBrandComponent } from '../update-retailbeverages-brand/update-retailbeverages-brand.component';
import { RetailBeveragesBrandComponent } from '../retail-beverages-brand/retail-beverages-brand.component';
import { ViewRetailbeveragesBrandComponent } from '../view-retailbeverages-brand/view-retailbeverages-brand.component';

// *************************************** Beverages *************************************

export interface beverageElement {
  beverageCode: string;
  beverageName: string;
  unit: string;
  veg_nonveg: string;
  rootCategory: string;
  subVarient: string;
  varients: string;
  type: string;
  addedBy: string;
  updatedAt: string;
}

export interface beverageBrandElement {
  brandCode: string;
  brandName: string;
  country: string;
  addedBy: string;
  updatedAt: string;
  status: boolean;
}

export interface beverageProductElement {
  productCode: string;
  productName: string;
  varient: string;
  associatedBrand: string;
  addedBy: string;
  updatedAt: string;
  status: boolean;
}

// ************************************* Retail Food *****************************************

export interface retailFoodAttributeElement {
  _id: string;
  attributeName: string;
  responseType: string;
  searchable: boolean;
  filterable: boolean;
  status: boolean;
  addedBy: string;
  updatedAt: string;
}
//////////////////*****************************attribute ********************************************///////* */
export interface liquorAttributeElement {
  attributeCode: string;
  attributeName: string;
  responseType: string;
  searchable: boolean;
  filterable: boolean;
  status: boolean;
  addedBy: string;
  updatedAt: string;
}

export interface liquorAttributeSetsElement {
  attributeSetCode: string;
  attributeSetName: string;
  assignGroups: string;
  status: string
  addedBy: string;
  updatedAt: string;
}


@Component({
  selector: 'app-beverage-retailfood',
  templateUrl: './beverage-retailfood.component.html',
  styleUrls: ['./beverage-retailfood.component.scss']
})
export class BeverageRetailfoodComponent implements OnInit {
  retailbrandfilter = { country: [], addedby: [], status: [] }
  retailbeveragesbrandfilter = { country: [], addedby: [], status: [] }
  foodattribute = { filter: [], search: [], addedby: [], status: [], type: [] }
  beveragesproductfilter = { category: [], brand: [], addedby: [], status: [] }
  beveragesattribute = { filter: [], search: [], addedby: [], status: [], type: [] }
  foodarrtibuteSetfilter = { addedby: [], status: [] }
  beveragesarrtibuteSetfilter = { addedby: [], status: [] }
  foodproductfilter = { category: [], brand: [], addedby: [], status: [] }
  // ***************************************** Beverages *******************************************
  beveragesfilter = { Category: [], Egg: true, NonVeg: true, SeaFood: true, Veg: true, all: true, addedby: [], status: [] }
  //beverageDATA
  type = ['Radio Button', 'Input Box', '	Check Box', 'Yes / No']
  Egg: boolean
  NonVeg: boolean
  SeaFood: boolean
  Veg: boolean
  displayColumnsBeverage: string[] = ['beverageCode', 'beverageName', 'type', 'unit', 'veg_nonveg', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceBeverage: MatTableDataSource<beverageElement>
  @ViewChild("beveragepaginator", { static: true }) beveragepaginator: MatPaginator;
  @ViewChild("beverageSort", { static: true }) beverageSort: MatSort

  //beverageAttributeElement
  displayColumnsBeverageAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceBeverageAttribute: MatTableDataSource<liquorAttributeElement>
  @ViewChild("beverageAttributepaginator", { static: true }) beverageAttributepaginator: MatPaginator;
  @ViewChild("beverageAttributesort", { static: true }) beverageAttributesort: MatSort;
  //retailbevearges attribute
  displayColumnsretailBeverageAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceretailBeverageAttribute: MatTableDataSource<liquorAttributeElement>
  @ViewChild("retailbeverageAttributepaginator", { static: true }) retailbeverageAttributepaginator: MatPaginator;
  @ViewChild("retailbeverageAttributesort", { static: true }) retailbeverageAttributesort: MatSort;


  //beverages attribute sets
  displayColumnsretailBeverageAttributeSet: string[] = ['attributeSetCode', 'attributeSetName', 'assignGroups', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceretailBeverageAttributeSet: MatTableDataSource<liquorAttributeSetsElement>
  @ViewChild("BeverageAttributeSetpaginator", { static: true }) retailBeverageAttributeSetpaginator: MatPaginator;
  @ViewChild("BeverageAttributeSetSort", { static: true }) retailBeverageAttributeSetSort: MatSort


  // beverage brands
  displayColumnsBeverageBrands: string[] = ['brandCode', 'brandName', 'country', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceBeverageBrands: MatTableDataSource<beverageBrandElement>
  @ViewChild("beverageBrandspaginator", { static: true }) beverageBrandspaginator: MatPaginator;
  @ViewChild("beverageBrandsSort", { static: true }) beverageBrandsSort: MatSort

  // beverage products
  displayColumnsBeverageProducts: string[] = ['productCode', 'productName', 'varient', 'associatedBrand', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceBeverageProduct: MatTableDataSource<beverageProductElement>
  @ViewChild("beverageProductspaginator", { static: true }) beverageProductspaginator: MatPaginator;
  @ViewChild("beverageProductsSort", { static: true }) beverageProductsSort: MatSort


  // ******************************************* Retail Food *****************************************

  //retailFoodAttributeElement
  displayColumnsretailFoodAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceRetailFoodAttribute: MatTableDataSource<liquorAttributeElement>
  @ViewChild("retailFoodAttributepaginator", { static: true }) retailFoodAttributepaginator: MatPaginator;
  @ViewChild("retailFoodAttributesort", { static: true }) retailFoodAttributesort: MatSort;


  //retail food attribute sets
  displayColumnsRetailFoodAttributeSet: string[] = ['attributeSetCode', 'attributeSetName', 'assignGroups', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceRetailFoodAttributeSet: MatTableDataSource<liquorAttributeSetsElement>
  @ViewChild("RetailFoodAttributeSetpaginator", { static: true }) RetailFoodAttributeSetpaginator: MatPaginator;
  @ViewChild("RetailFoodAttributeSetSort", { static: true }) RetailFoodAttributeSetSort: MatSort

  // retail food brands
  displayColumnsRetailFoodBrands: string[] = ['brandCode', 'brandName', 'country', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceRetailFoodBrands: MatTableDataSource<beverageBrandElement>
  @ViewChild("retailFoodBrandspaginator", { static: true }) retailFoodBrandspaginator: MatPaginator;
  @ViewChild("retailFoodBrandsSort", { static: true }) retailFoodBrandsSort: MatSort

  // retail beverages brands
  displayColumnsRetailBeveragesBrands: string[] = ['brandCode', 'brandName', 'country', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceRetailBeveragesBrands: MatTableDataSource<beverageBrandElement>
  @ViewChild("retailBeveragesBrandspaginator", { static: true }) retailBeveragesBrandspaginator: MatPaginator;
  @ViewChild("retailBeveragesBrandsSort", { static: true }) retailBeveragesBrandsSort: MatSort


  // retail food products
  displayColumnsRetailFoodProducts: string[] = ['productCode', 'productName', 'varient', 'associatedBrand', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceRetailFoodProduct: MatTableDataSource<beverageProductElement>
  @ViewChild("retailFoodProductspaginator", { static: true }) retailFoodProductspaginator: MatPaginator;
  @ViewChild("retailFoodProductsSort", { static: true }) retailFoodProductsSort: MatSort
  displayColumnsRetailBeveragesProducts: string[] = ['productCode', 'productName', 'varient', 'associatedBrand', 'addedBy', 'updatedAt', 'status', 'action']
  dataSourceRetailBeveragesProduct: MatTableDataSource<beverageProductElement>
  @ViewChild("retailFoodProductspaginator", { static: true }) retailBeveragesProductspaginator: MatPaginator;
  @ViewChild("retailFoodProductsSort", { static: true }) retailBeveragesProductsSort: MatSort

  displayColumnsliquorAttribute: string[] = ['attributeCode', 'attributeName', 'responseType', 'searchable', 'filterable', 'updatedAt', 'addedBy', 'status', 'action']
  dataSourceLiquorAttribute: MatTableDataSource<liquorAttributeElement>
  @ViewChild("liquorAttributepaginator", { static: true }) liquorAttributepaginator: MatPaginator;
  @ViewChild("liquorAttributesort", { static: true }) liquorAttributesort: MatSort;
  //beverages > categories > show categories
  beverageSize = 12
  showBeverageCategories = false

  //retail food > categories > show categories
  retailFoodSize = 12
  showRetailFoodCategories = false



  //un assigned attribute 
  allLiquorAttribute = []
  allBeverageUnassignedAttribute = []
  allRetailFoodUnassignedAttribue = []


  // attribute groups
  allLiquorAttributeGroup = []
  allBeverageAttributeGroups = []
  allRetailFoodAttributeGroups = []
  allInHouseBeveragesCategories: any
  allRetailBeveragesCategories: any
  allRetailFoodCategories: any

  activeTab: any
  activeTabBeverage: any
  activeTabRetailFood: any


  beverageProductCount = 0
  beverageBrandCount = 0
  retailFoodProductCount = 0
  retailFoodBrandCount = 0
  retailBeveragesBrandCount = 0

  all: any

  //  allMenuData: any

  storeData: any;
  fileUploaded: File;
  worksheet: any;
  jsonData: any;
  csvData: any;
  userid: any

  date: any

  //export excel file 

  retailFoodAttributeJSONDATA = []

  beverageAttributeSetJSONDATA = []
  retailFoodAttributeSetJSONDATA = []
  beverageJSONDATA = []
  beverageProductJSONDATA = []
  beveraeBrandJSONDATA = []
  retailFoodProductJSONDATA = []
  retailFoodBrandJSONDATA = []
  retailBeveargesBrandJSONDATA = []
  noData: any;
  noData1: any;
  noData2: any;
  noData3: any;
  noData4: any;
  noData5: any;
  noData6: any;
  noData7: any;
  noData8: any;
  noData9: any;
  noData10: any;
  noData11: any;
  noData12: any;
  noData13: any;
  noData14: any;
  noData15: any;
  noData16: any;
  noData17: any;
  noData18: any;
  noData50: any;

  retail_food_product: any;
  beverage_data: any;
  beverageproduct_data: any;
  beverageBrand_data: any;
  beverageattributeset_data: any
  retailfoodattributeset_data: any;
  beverageattribute_data: any;
  retailFoodattribute_data: any;
  retailfoodbrand_data: any;
  retailBeveragesbrand_data: any
  noData19: any;
  noData20: any;
  liquorAttributeCount: any;
  assetInventoryAttributeCount: any;
  beverageAttributeJSONDATA = [];

  liquorattribute_data: any;
  liquorAttributeJSONDATA = [];
  retailbeverageattribute_data = [];
  allretailBeverageUnassignedAttribute = [];
  retail_beverages_product = [];
  retailBeveragesProductCount = 0;
  retailBeveragesProductJSONDATA = [];
  admin = [];
  inhouseheveragesRoot = [];
  v1 = [];
  country_json = [];
  retailFoodcategory = [];
  retailbeveragesRoot = [];
  retailBeveragesBrand = [];
  noData40: any
  beverages = [];
  // retailBeveragesBrand=[];

  constructor(private _formBuilder: FormBuilder, private main: MainService, private router: Router,
    public dialog: MatDialog, private menuService: AddMenuFoodsService) {
    let userdata = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = userdata['_id'];
    this.getAllRetailFoodProducts()
    this.getAllBeveragesCategories()
    this.getAllRetailFoodCategories()
    this.getAllBeverages();
    this.country_state_city()
    this.getAllBeverageProducts()
    this.getAllLiquorAttribute()
    this.getAllLiquorAttributeSet()
    this.getAllBeverageBrands()
    this.getAllRetailFoodBrands();
    this.getAllRetailBeveragesBrands();
    this.addRetailBeveragesProduct();
    this.getadmin()
    var d = new Date()
    this.date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
  }
  country_state_city() {
    console.log("hey");

    this.menuService.country_state_city().subscribe(data => {
      // console.log("============", data);

      console.log(data.country_json);
      this.country_json = data.country_json


      for (var i = 0; i < data.country_json.length - 1; i++)
        //console.log("=========================");

        console.log(data.country_json[i].name);

    })
  }
  getadmin() {
    this.menuService.getAdmin().subscribe(data => {
      console.log("users", data);
      data['data'].forEach(element => {
        this.admin.push(element['userName'])
      });
      console.log(this.admin);

    })
  }

  ngOnInit() {
    let index = localStorage.getItem('userTabLocation')
    if (index == undefined) {
      let index = 0; // get stored number or zero if there is nothing stored
      this.activeTab = index; // with tabGroup being the MatTabGroup accessed through ViewChild

    } else {
      // let index = localStorage.getItem('userTabLocation') || 0; // get stored number or zero if there is nothing stored
      this.activeTab = index; // with tabGroup being the MatTabGroup accessed through ViewChild

    }

    let beverageTabIndex = localStorage.getItem('beverageTabLocation')
    if (beverageTabIndex == undefined) {
      this.activeTabBeverage = 0
    } else {
      this.activeTabBeverage = beverageTabIndex
    }


    let retailFoodTabIndex = localStorage.getItem('retailFoodMenuManagement')
    if (retailFoodTabIndex == undefined) {
      this.activeTabRetailFood = 0
    } else {
      this.activeTabRetailFood = retailFoodTabIndex
    }
  }


  ngAfterViewInit() {

  }
  //Tabs On navigate from tab back to same tab
  handleMatTabChange(event) {
    //console.log(event);
    localStorage.setItem('userTabLocation', event);
  }

  handleMatTabChangeMenuManagementBeverage(event) {
    localStorage.setItem('beverageTabLocation', event)
  }

  handleMatTabChangeRetailFood(event) {
    localStorage.setItem('retailFoodMenuManagement', event)
  }

  // import excel file and convert it to json object
  uploadedFile(event) {
    //console.log("upload file");

    this.fileUploaded = event.target.files[0];
    this.readExcel()
  }

  readExcel() {
    let readFile = new FileReader();
    //console.log(readFile);

    readFile.onload = (e) => {

      this.storeData = readFile.result;
      var data = new Uint8Array(this.storeData);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });

      var first_sheet_name = workbook.SheetNames[0];
      this.worksheet = workbook.Sheets[first_sheet_name];
      this.jsonData = XLSX.utils.sheet_to_json(this.worksheet, { raw: false });
      this.jsonData.map(ele => (ele.userid = this.userid))
      //   this.jsonData = JSON.stringify(this.jsonData);
      //console.log(this.jsonData);
    }
    readFile.readAsArrayBuffer(this.fileUploaded);


  }
  //get all attributes
  getAllLiquorAttribute() {
    this.menuService.getAllLiquorAttribute().subscribe(data => {
      //console.log("all liquor attribute", data);

      if (data['sucess'] == false) {
        this.liquorAttributeCount = 0
        this.assetInventoryAttributeCount = 0
        //console.log("Attribute Data Not Found");
        this.noData7 = 0
        this.noData19 = 0
        this.noData15 = 0
        this.noData3 = 0
      } else {
        // LIQUOR
        this.liquorattribute_data = data['data']
        data['data'].forEach(element => {
          element.attributeCode = "A" + element.attributeCode
        });
        let allAttributes = data['data'].filter(element => element.attributeType == "liquor")
        this.liquorattribute_data = data['data'].filter(element => element.attributeType == "liquor")
        this.dataSourceLiquorAttribute = new MatTableDataSource(allAttributes);
        this.dataSourceLiquorAttribute.paginator = this.liquorAttributepaginator;
        this.dataSourceLiquorAttribute.sort = this.liquorAttributesort;
        this.noData7 = this.dataSourceLiquorAttribute.connect().pipe(map(data => data.length === 0));
        this.allLiquorAttribute = allAttributes.filter(ele => ele['isAssign'] == false)
        this.liquorAttributeCount = allAttributes.length
        // BEVERAGE
        this.dataSourceBeverageAttribute = new MatTableDataSource(data['data'].filter(element => element.attributeType == "beverage"))
        this.beverageattribute_data = data['data'].filter(element => element.attributeType == "beverage")

        this.dataSourceBeverageAttribute.paginator = this.beverageAttributepaginator;
        this.dataSourceBeverageAttribute.sort = this.beverageAttributesort;
        this.noData15 = this.dataSourceBeverageAttribute.connect().pipe(map(data => data.length === 0));
        this.allBeverageUnassignedAttribute = data['data'].filter(element => element.attributeType == "beverage" && element.isAssign == false)

        //retail beverages
        this.dataSourceretailBeverageAttribute = new MatTableDataSource(data['data'].filter(element => element.attributeType == "retailBeverages"))
        this.retailbeverageattribute_data = data['data'].filter(element => element.attributeType == "retailbeverages")
        //console.log("retail",this.retailbeverageattribute_data);
        this.dataSourceretailBeverageAttribute.data = this.retailbeverageattribute_data
        this.noData3 = this.dataSourceretailBeverageAttribute.connect().pipe(map(data => data.length === 0));
        this.dataSourceretailBeverageAttribute.paginator = this.retailbeverageAttributepaginator;
        this.dataSourceretailBeverageAttribute.sort = this.retailbeverageAttributesort;



        // RETAIL FOOD
        this.dataSourceRetailFoodAttribute = new MatTableDataSource(data['data'].filter(element => element.attributeType == "retailfood"))
        this.retailFoodattribute_data = data['data'].filter(element => element.attributeType == "retailfood")
        this.noData19 = this.dataSourceRetailFoodAttribute.connect().pipe(map(data => data.length === 0));
        this.dataSourceRetailFoodAttribute.paginator = this.retailFoodAttributepaginator;
        this.dataSourceRetailFoodAttribute.sort = this.retailFoodAttributesort;

        this.allRetailFoodUnassignedAttribue = data['data'].filter(element => element.attributeType == "retailfood" && element.isAssign == false)

        //Asset Inventory
        this.assetInventoryAttributeCount = data['data'].filter(element => element.attributeType == "equipment").length

        var liquorAttribute = []
        liquorAttribute = data['data'].filter(element => element.attributeType == "liquor")
        liquorAttribute.forEach(element => {
          var optionLabels = []
          element.responseType.options.forEach(ele => {
            optionLabels.push(ele.optionLable)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let liquorObj = {}
          liquorObj = {
            "attributeCode": element.attributeCode,
            "attributeName": element.attributeName,
            "responseType": element.responseType.elementName,
            "fieldValue": element.responseType.fieldValue,
            "options": optionLabels.toString(),
            "isSearchable": element.isSearchable,
            "isFilterable": element.isFilterable,
            "isAssign": element.isAssign,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.liquorAttributeJSONDATA.push(liquorObj)
        });
        //console.log("liquor attribute export", this.liquorAttributeJSONDATA);

        var beverageAttribute = data['data'].filter(element => element.attributeType == "beverage")
        beverageAttribute.forEach(element => {
          var optionLabels = []
          element.responseType['options'].forEach(ele => {
            optionLabels.push(ele.optionLable)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let beverageObj = {}
          beverageObj = {
            "attributeCode": element.attributeCode,
            "attributeName": element.attributeName,
            "responseType": element.responseType.elementName,
            "fieldValue": element.responseType.fieldValue,
            "options": optionLabels.toString(),
            "isSearchable": element.isSearchable,
            "isFilterable": element.isFilterable,
            "isAssign": element.isAssign,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.beverageAttributeJSONDATA.push(beverageObj)
        });
        //console.log("beverage attribute export", this.beverageAttributeJSONDATA);


        var retailFoodAttribute = data['data'].filter(element => element.attributeType == "retailFood")
        retailFoodAttribute.forEach(element => {
          var optionLabels = []
          element.responseType.options.forEach(ele => {
            optionLabels.push(ele.optionLable)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          let retailFoodObj = {}
          retailFoodObj = {
            "attributeCode": element.attributeCode,
            "attributeName": element.attributeName,
            "responseType": element.responseType.elementName,
            "fieldValue": element.responseType.fieldValue,
            "options": optionLabels.toString(),
            "isSearchable": element.isSearchable,
            "isFilterable": element.isFilterable,
            "isAssign": element.isAssign,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.retailFoodAttributeJSONDATA.push(retailFoodObj)
        });
        //console.log("retail food attribute export", this.retailFoodAttributeJSONDATA);

      }
    })
  }
  // export beverage attribute excel 
  readAsCSVBeverageAttribute() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Attribute Code", "Attribute Name", "Response Type", "Field Value", "Options", "Searchable", "Filterable", "Assign", "Status", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.beverageAttributeJSONDATA, 'Beverage_Attribute_Export_' + this.date, options);
  }

  // export retail food attribute excel 
  readAsCSVRetailFoodAttribute() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["Attribute Code", "Attribute Name", "Response Type", "Field Value", "Options", "Searchable", "Filterable", "Assign", "Status", "Added By", "Updated By", "Updated At"]
    };
    new ngxCsv(this.retailFoodAttributeJSONDATA, 'Retail_Food_Attribute_Export_' + this.date, options);
  }


  //get all attribute group
  //  getAllLiquorAttributeGroup() {
  //    this.menuService.getall
  //    let data = this.allMenuData.allAttributeGroups
  //    //console.log("All liquor attribute group", data);
  //    if (data['sucess'] == false) {
  //      //console.log("Attribute group Data Not Found");
  //    } else {

  //      // BEVERAGE
  //      this.allBeverageAttributeGroups = data['data'].filter(ele => ele.attributeType == "beverage")

  //      // RETAIL FOOD
  //      this.allRetailFoodAttributeGroups = data['data'].filter(ele => ele.attributeType == "retailFood")
  //    }
  //  }

  //get all attribute set
  getAllLiquorAttributeSet() {
    this.menuService.getAllLiquorAttributeSets().subscribe(data => {


      //console.log("All liquor attribute sets", data);
      if (data['sucess'] == false) {
        //console.log("Attribute Set Data Not Found");
        this.noData8 = 0
        this.noData20 = 0
        this.noData50 = 0
      } else {

        // LIQUOR
        data['data'].forEach(element => {
          element.attributeSetCode = "AS" + element.attributeSetCode
        });
        // BEVERAGE
        this.dataSourceretailBeverageAttributeSet = new MatTableDataSource(data['data'].filter(ele => ele.attributeType == "retailbeverages"))
        this.beverageattributeset_data = this.dataSourceretailBeverageAttributeSet.data
        this.dataSourceretailBeverageAttributeSet.paginator = this.retailBeverageAttributeSetpaginator
        this.dataSourceretailBeverageAttributeSet.sort = this.retailBeverageAttributeSetSort
        //console.log(">>>>>", this.dataSourceretailBeverageAttributeSet.data );

        this.noData4 = this.dataSourceretailBeverageAttributeSet.connect().pipe(map(data => data.length === 0));

        // RETAIL FOOD
        this.dataSourceRetailFoodAttributeSet = new MatTableDataSource(data['data'].filter(ele => ele.attributeType == "retailfood"))
        this.retailfoodattributeset_data = this.dataSourceRetailFoodAttributeSet.data
        this.noData20 = this.dataSourceRetailFoodAttributeSet.connect().pipe(map(data => data.length === 0));
        this.dataSourceRetailFoodAttributeSet.paginator = this.RetailFoodAttributeSetpaginator
        this.dataSourceRetailFoodAttributeSet.sort = this.RetailFoodAttributeSetSort
        //console.log(this.dataSourceRetailFoodAttributeSet.data);


        var beverageAttributeSet = data['data'].filter(ele => ele.attributeType == "retailbeverages")
        //console.log(">>>>>>>>>>>>>>>>>>>", beverageAttributeSet);
        var assigngGroup = []
        beverageAttributeSet.forEach(element => {

          element.assignGroups.forEach(ele => {
            assigngGroup.push(ele.groupName)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var beverageObj = {}
          beverageObj = {
            "attributeSetCode": element.attributeSetCode,
            "attributeSetName": element.attributeSetName,
            "assignGroups": assigngGroup.toString(),
            "status": element.status,
            "categoryType": element.categoryType,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.beverageAttributeSetJSONDATA.push(beverageObj)
        });
        //console.log("beverage attribute set export", this.beverageAttributeSetJSONDATA);


        var retailFoodAttributeSet = data['data'].filter(ele => ele.attributeType == "retailfood")
        retailFoodAttributeSet.forEach(element => {
          var assigngGroup = []
          //console.log("tree data",element.categoryType);

          element.assignGroups.forEach(ele => {

            assigngGroup.push(ele.groupName)
          });
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var retailFoodObj = {}
          retailFoodObj = {
            "attributeSetCode": element.attributeSetCode,
            "attributeSetName": element.attributeSetName,
            "assignGroups": assigngGroup.toString(),
            "status": element.status,
            "categoryType": element.categoryType,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.retailFoodAttributeSetJSONDATA.push(retailFoodObj)
        });
        //console.log("retail food attribute set export", this.retailFoodAttributeSetJSONDATA);


      }
    })
  }


  // export beverage attribute set excel 
  readAsCSVBeverageAttributeSet() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["attributeSetCode", "attributeSetName", "assignGroups", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.beverageAttributeSetJSONDATA, 'Beverage_Attribute_Set_Export_' + this.date, options);
  }

  // export retail food attribute set excel 
  readAsCSVRetailFoodAttributeSet() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["attributeSetCode", "attributeSetName", "assignGroups", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.retailFoodAttributeSetJSONDATA, 'Retail_Food_Attribute_Set_Export_' + this.date, options);
  }



  //get all beverages
  getAllBeverages() {
    this.menuService.getAllBeverages().subscribe(data => {
      //console.log("All Beverages", data);
      if (data['success'] == false) {
        //console.log("Beverages Data Not Found");
        this.noData12 = 0
      } else {
        this.beverage_data = data['data']
        data['data'].forEach(element => {
          element.beverageCode = "B" + element.beverageCode
        });
        this.dataSourceBeverage = new MatTableDataSource(data['data'])
        this.dataSourceBeverage.paginator = this.beveragepaginator;
        this.dataSourceBeverage.sort = this.beverageSort;
        this.noData12 = this.dataSourceBeverage.connect().pipe(map(data => data.length === 0));

        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var beverageObj = {}
          beverageObj = {
            "category": element.category,
            "beverageCode": element.beverageCode,
            "beverageName": element.beverageName,
            "rootCategory": element.rootCategory,
            "varients": element.varients,
            "subVarient": element.subVarient,
            "type": element.type,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.beverageJSONDATA.push(beverageObj)
        });
        //console.log("beverage export", this.beverageJSONDATA);
      }
    })
  }


  // export beverage excel 
  readAsCSVBeverage() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["beverageCode", "beverageName", "rootCategory", "varients", "subVarient", "type", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.beverageJSONDATA, 'Beverage_Export_' + this.date, options);
  }

  //get all beverage brands
  getAllBeverageBrands() {
    this.menuService.getAllBeverageBrands().subscribe(data => {
      //console.log("All Beverage Brands", data);
      if (data['success'] == false) {
        this.beverageBrandCount = 0
        this.noData14 = 0
        //console.log("Beverage Brand Data Not Found");
      } else {
        this.beverageBrand_data = data['data']
        this.beverageBrandCount = data['data'].length
        data['data'].forEach(element => {
          element.brandCode = "B" + element.brandCode
        });
        this.dataSourceBeverageBrands = new MatTableDataSource(data['data'])
        this.dataSourceBeverageBrands.paginator = this.beverageBrandspaginator;
        this.dataSourceBeverageBrands.sort = this.beverageBrandsSort;
        this.noData14 = this.dataSourceBeverageBrands.connect().pipe(map(data => data.length === 0));

        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var brandObj = {}
          brandObj = {
            "brandCode": element.brandCode,
            "brandName": element.brandName,
            "country": element.country,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.beveraeBrandJSONDATA.push(brandObj)
        });
        //console.log("beverage brand export", this.beveraeBrandJSONDATA);
      }
    })
  }

  // export beverage brand excel 
  readAsCSVBeverageBrand() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["brandCode", "brandName", "country", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.beveraeBrandJSONDATA, 'Beverage_Brand_Export_' + this.date, options);
  }

  //get all beverage prouct
  getAllBeverageProducts() {
    this.menuService.getAllBeverageProducts().subscribe(data => {
      //console.log("All Beverage Products", data);
      if (data['success'] == false) {
        this.beverageProductCount = 0
        this.noData13 = 0
        //console.log("Beverage product Data Not Found");
      } else {
        this.beverageproduct_data = data['data']
        this.beverageProductCount = data['data'].length
        data['data'].forEach(element => {
          element.productCode = "P" + element.productCode
        });
        this.dataSourceBeverageProduct = new MatTableDataSource(data['data'])
        this.dataSourceBeverageProduct.paginator = this.beverageProductspaginator
        this.dataSourceBeverageProduct.sort = this.beverageProductsSort
        this.noData13 = this.dataSourceBeverageProduct.connect().pipe(map(data => data.length === 0));

        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var productObj = {}
          productObj = {
            "productCode": element.productCode,
            "productName": element.productName,
            "rootCategory": element.rootCategory,
            "varient": element.varient,
            "subVarient": element.subVarient,
            "type": element.type,
            "attributeSet": element.attributeSet.attributeSetName,
            "associatedBrand": element.associatedBrand.brandName,
            "unit": element.unit,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.beverageProductJSONDATA.push(productObj)
        });
        //console.log("beverage product export", this.beverageProductJSONDATA);
      }
    })
  }

  // export beverage product excel 
  readAsCSVBeverageProduct() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["productCode", "productName", "rootCategory", "varient", "subVarient", "type", "attributeSet", "associatedBrand", "unit", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.beverageProductJSONDATA, 'Beverage_Product_Export_' + this.date, options);
  }

  //get all retail food brands
  getAllRetailFoodBrands() {
    this.menuService.getAllRetailFoodBrands().subscribe(data => {
      //  let data = this.allMenuData.allRetailFoodBrands
      console.log("All Retail Food Brands", data);
      if (data['success'] == false) {
        this.retailFoodBrandCount = 0
        this.noData18 = 0
        //console.log("Retail Food Brand Data Not Found");
      } else {
        this.retailfoodbrand_data = data['data']

        this.retailFoodBrandCount = data['data'].length
        data['data'].forEach(element => {
          element.brandCode = "B" + element.brandCode
        });
        this.dataSourceRetailFoodBrands = new MatTableDataSource(data['data'])
        this.dataSourceRetailFoodBrands.paginator = this.retailFoodBrandspaginator
        this.dataSourceRetailFoodBrands.sort = this.retailFoodBrandsSort
        this.noData18 = this.dataSourceRetailFoodBrands.connect().pipe(map(data => data.length === 0));
        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var brandObj = {}
          brandObj = {
            "brandCode": element.brandCode,
            "brandName": element.brandName,
            "country": element.country,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.retailFoodBrandJSONDATA.push(brandObj)
        });
        //console.log("retail food brand export", this.retailFoodBrandJSONDATA);
      }
    })
  }
  getAllRetailBeveragesBrands() {
    this.menuService.getAllRetailBeveargesBrands().subscribe(data => {
      //  let data = this.allMenuData.allRetailFoodBrands
      //console.log("All Retail Beverages Brands", data);
      if (data['success'] == false) {
        this.retailBeveragesBrandCount = 0
        this.noData1 = 0
        //console.log("Retail Food Brand Data Not Found");
      } else {
        this.retailBeveragesbrand_data = data['data']

        this.retailBeveragesBrandCount = data['data'].length
        data['data'].forEach(element => {
          element.brandCode = "B" + element.brandCode
          this.retailBeveragesBrand.push(element['brandName'])
        });
        this.dataSourceRetailBeveragesBrands = new MatTableDataSource(data['data'])
        this.dataSourceRetailBeveragesBrands.paginator = this.retailBeveragesBrandspaginator
        this.dataSourceRetailBeveragesBrands.sort = this.retailBeveragesBrandsSort
        this.noData1 = this.dataSourceRetailBeveragesBrands.connect().pipe(map(data => data.length === 0));
        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          //  this.retailBeveragesBrand.push(element['brandName'])
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var brandObj = {}
          brandObj = {
            "brandCode": element.brandCode,
            "brandName": element.brandName,
            "country": element.country,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "updatedAt": date
          }
          this.retailBeveargesBrandJSONDATA.push(brandObj)
        });
        //console.log("retail food brand export", this.retailBeveargesBrandJSONDATA);
      }
    })
  }


  // export retail food brand excel 
  readAsCSVRetailFoodBrand() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["brandCode", "brandName", "country", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.retailBeveargesBrandJSONDATA, 'Retail_Food_Brand_Export_' + this.date, options);
  }



  // get all retail food products
  getAllRetailFoodProducts() {
    //  let data = this.allMenuData.allRetailFoodProducts
    this.menuService.getAllRetailFoodProducts().subscribe(data => {
      console.log("All Retail food Products", data);
      this.retail_food_product = data['data']
      if (data['sucess'] == false) {
        this.retailFoodProductCount = 0
        this.noData17 = 0
        //console.log("Retail Food Product Data Not Found");
      } else {
        this.retailFoodProductCount = data['data'].length
        data['data'].forEach(element => {
          element.productCode = "P" + element.productCode
        });
        this.dataSourceRetailFoodProduct = new MatTableDataSource(data['data'])

        this.dataSourceRetailFoodProduct.paginator = this.retailFoodProductspaginator
        this.dataSourceRetailFoodProduct.sort = this.retailFoodProductsSort
        this.noData17 = this.dataSourceRetailFoodProduct.connect().pipe(map(data => data.length === 0));

        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var productObj = {}
          productObj = {
            "productCode": element.productCode,
            "productName": element.productName,
            "rootCategory": element.rootCategory,
            "varient": element.varient,
            "subVarient": element.subVarient,
            "type": element.type,
            "attributeSet": element.attributeSet.attributeSetName,
            "associatedBrand": element.associatedBrand.brandName,
            "unit": element.unit,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "category": element.category,
            "updatedAt": date
          }
          this.retailFoodProductJSONDATA.push(productObj)
        });
        //console.log("retail food product export", this.retailFoodProductJSONDATA);
      }
    })
  }

  // export retailfood product excel 
  readAsCSVRetailFoodProduct() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["productCode", "productName", "rootCategory", "varient", "subVarient", "type", "attributeSet", "associatedBrand", "unit", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.retailFoodProductJSONDATA, 'Retail_Food_Product_Export_' + this.date, options);
  }


  readAsCSVRetailBeveragesProduct() {
    var options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      //  showTitle: true,
      //  title: 'Your title',
      useBom: true,
      noDownload: false,
      headers: ["productCode", "productName", "rootCategory", "varient", "subVarient", "type", "attributeSet", "associatedBrand", "unit", "status", "addedBy", "updatedBy", "updatedAt"]
    };
    new ngxCsv(this.retailBeveragesProductJSONDATA, 'Retail_Beverages_Product_Export_' + this.date, options);
  }


  //change filterable status of liquor attribute
  liquorAttributeFilterable(element, event) {
    //console.log(event.checked);
    //console.log(element._id);

    this.menuService.updateLiquorAttributeIsFilterable(element._id, { "isFilterable": event.checked }).subscribe(res => {
      //console.log(res);
      if (res['sucess'] == true) {

        if (event.checked) {

          Swal.fire('Filterable Status Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Filterable Status Deactivated Successfully ', '', 'warning')
        }
      }
      else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllLiquorAttribute();
    })

  }

  //change searchable status of liquor attribute
  liquorAttributeSearchable(element, event) {
    //console.log(event.checked);
    //console.log(element._id);

    this.menuService.updateLiquorAttributeIsSearchable(element._id, { "isSearchable": event.checked }).subscribe(res => {
      //console.log(res);
      if (res['sucess'] == true) {

        if (event.checked) {

          Swal.fire('Searchable status Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Searchable status Deactivated Successfully ', '', 'warning')
        }
      }
      else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllLiquorAttribute()

    })
  }
  //view single liquor attribute
  viewLiquorAttribute(obj): void {
    //console.log(obj);
    const dialogRef = this.dialog.open(ViewLiquorAttributeComponent, {
      data: obj,
      disableClose: true,
      width: '60%',
      height: '80%'
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  //add attribute group popup
  addLiquorAttributeGroup(keyType): void {
    //console.log(keyType, "attribute group key");

    const dialogRef = this.dialog.open(AddLiquorAttributeGroupComponent, {
      data: keyType,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      // this.animal = result;
      //  this.getAllApis()
    });
  }

  //add attribute set popup
  addLiquorAttributeSet(keyType): void {
    const dialogRef = this.dialog.open(AddLiquorAttributeSetComponent, {
      data: keyType,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      this.getAllLiquorAttributeSet()
    });
  }

  //view Attribute set popup
  viewLiquorAttributeSet(obj): void {
    const dialogRef = this.dialog.open(ViewLiquorAttributeSetComponent, {
      data: obj,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');

    });
  }

  // view beverage brand
  viewBeverageBrand(obj): void {
    const dialogRef = this.dialog.open(ViewBeverageBrandComponent, {
      data: obj,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
    });
  }
  viewRetailBeverageBrand(obj): void {
    const dialogRef = this.dialog.open(ViewRetailbeveragesBrandComponent, {
      data: obj,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
    });
  }

  // view beverage products
  viewBeverageProduct(obj): void {
    const dialogRef = this.dialog.open(ViewBeverageProductComponent, {
      data: obj,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
    });
  }

  // view beverage
  viewBeverage(obj): void {
    const dialogRef = this.dialog.open(ViewBeverageComponent, {
      data: obj,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
    });
  }
  editAttribute(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-attribute")
  }

  editAttributeSet(obj) {
    //console.log("edit obj:",obj);

    this.menuService.editObject = obj
    ////console.log("",this.menuService.editObject);

    this.router.navigate(['/menu/update-attributeset'])
    // // const dialogRef = this.dialog.open(UpdateAttributeGroupSetComponent, {
    //   data: obj,
    //   disableClose: true,


    // });
    // ////console.log(obj);
    // dialogRef.afterClosed().subscribe(result => {
    //   //////console.log('The dialog was closed');
    //   // this.animal = result;
    //   this.getAllApis()
    // });
  }



  editAttributeGroup(obj): void {
    const dialogRef = this.dialog.open(UpdateAttributeGroupComponent, {
      data: obj,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      // this.animal = result;

    });
  }


  editBeverageBrand(obj): void {
    const dialogRef = this.dialog.open(UpdateBeverageBrandComponent, {
      data: obj,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      this.getAllBeverageBrands()
    });
  }

  editBeverageProduct(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-beverage-product")
  }

  editRetailFoodProduct(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-retail-food-product")
  }
  editRetailBeveragesProduct(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/beverages_and_retailfood/update-retailBeverages-product")
  }


  editRetailFoodBrand(obj): void {
    const dialogRef = this.dialog.open(UpdateRetailFoodBrandComponent, {
      data: obj,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.getAllRetailFoodBrands()
      }, 2000);

    });
  }
  editRetailBeveragesBrand(obj): void {
    const dialogRef = this.dialog.open(UpdateRetailbeveragesBrandComponent, {
      data: obj,
      width: '60%',
      height: '80%',
      disableClose: true,
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      this.getAllRetailBeveragesBrands()
    });
  }

  editBeverage(obj) {
    this.menuService.editObject = obj
    this.router.navigateByUrl("/menu/update-beverage")
  }

  /////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Soft Delete ///////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////


  softDeleteAttribute(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteAttribute(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['sucess'] == true) {
        if (event.checked) {

          Swal.fire('Attribute Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Attribute Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllLiquorAttribute()

    })
  }

  // SOFT DELETE ATTRIBUTE SET
  softDeleteAttributeSet(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteAttributeSet(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['sucess'] == true) {
        if (event.checked) {

          Swal.fire('Attribute set Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Attribute set Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllLiquorAttributeSet()

    })
  }



  // SOFT DELETE BEVERAGE
  softDeleteBeverage(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteBeverage(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['success'] == true) {
        if (event.checked) {

          Swal.fire('Beverage Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Beverage Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllBeverages();
    })

  }

  //SOFT DELETE BEVERAGE PRODUCT
  softDeleteBeverageProduct(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteBeverageProduct(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['success'] == true) {
        if (event.checked) {

          Swal.fire('Beverage Product Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Beverage Product Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllBeverageProducts()
    })

  }

  //SOFT DELETE BEVERAGE BRAND
  softDeleteBeverageBrand(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteBeverageBrand(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['success'] == true) {
        if (event.checked) {

          Swal.fire('Beverage Brand Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Beverage Brand Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllBeverageBrands()
    })

  }


  // SOFT DELETE RATAIL FOOD PRODUCT
  softDeleteRetailFoodProduct(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteRetailFoodProduct(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['success'] == true) {
        if (event.checked) {

          Swal.fire('Product Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Product Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllRetailFoodProducts();
    })

  }
  softDeleteRetailBeveragesProduct(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteRetailBeveragesProduct(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['success'] == true) {
        if (event.checked) {

          Swal.fire('Product Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Product Deactivated Successfully ', '', 'warning')
        }
      } else {
        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllRetailFoodProducts();
    })

  }

  // SOFT DELETE RETAIL FOOD BRAND
  softDeleteRetailFoodBrand(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteRetailFoodBrand(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['success'] == true) {
        if (event.checked) {

          Swal.fire('Brand Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Brand Deactivated Successfully ', '', 'warning')
        }
      } else {

        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllRetailFoodBrands()
    })

  }

  softDeleteRetailBeveragesBrand(element, event) {
    //console.log(event.checked);
    //console.log(element._id);
    this.menuService.softDeleteRetailBeveragesBrand(element._id, { "status": event.checked }).subscribe(res => {
      //console.log(res);

      if (res['success'] == true) {
        if (event.checked) {

          Swal.fire('Brand Activated Successfully', '', 'success')
        }
        else {

          Swal.fire('Brand Deactivated Successfully ', '', 'warning')
        }
      } else {

        Swal.fire('Failed to Change Status', 'Something went wrong', 'warning')
      }
      this.getAllRetailBeveragesBrands()

    })
  }
  //popup for add brand Beverages
  openBeverageAddBrand(): void {
    const dialogRef = this.dialog.open(AddBeverageBrandComponent, {
      // width: '400px',
      disableClose: true,
      width: '60%',
      height: '80%',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      this.getAllBeverageBrands()
    });
  }

  //popup for add brand Retail food
  openpopupRetailFoodAddBrand(): void {
    const dialogRef = this.dialog.open(AddRetailfoodBrandComponent, {
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      setTimeout(() => {
        this.getAllRetailFoodBrands()

      }, 2000);
      //console.log('The dialog was closed');
    });

  }
  openpopupRetailBeveragesAddBrand(): void {
    const dialogRef = this.dialog.open(RetailBeveragesBrandComponent, {
      disableClose: true,
      width: '60%',
      height: '80%',
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
      this.getAllRetailBeveragesBrands()
    });

  }


  //show and hide beverages categories tree
  BeverageCategory(event) {
    //console.log(event.srcElement.checked);
    if (event.srcElement.checked == true) {
      this.beverageSize = 8
      this.showBeverageCategories = true
    } else {
      this.beverageSize = 12
      this.showBeverageCategories = false
    }
  }

  //show and hide retail food categories tree
  retailFoodCategories(event) {
    //console.log(event.srcElement.checked);
    if (event.srcElement.checked == true) {
      this.retailFoodSize = 8
      this.showRetailFoodCategories = true
    } else {
      this.retailFoodSize = 12
      this.showRetailFoodCategories = false
    }
  }



  ////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////// All Data Table Filters //////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
  applyFilterBeverage(event) {
    this.dataSourceBeverage.data = this.beverage_data
    //console.log( this.dataSourceBeverage.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceBeverage.data.filter(function (d) {
      //console.log(d);
      let result
      if (d['type'] != undefined && d['subVarient'] != undefined && d['varients'] != undefined && d['rootCategory'] != undefined) {
        result = d['beverageName'].toLowerCase().indexOf(val) !== -1 || d['beverageCode'].toLowerCase().indexOf(val) !== -1 || d['type']['type'].toLowerCase().indexOf(val) !== -1 || d['subVarient']['subVarient'].toLowerCase().indexOf(val) !== -1 || d['varients']['varients'].toLowerCase().indexOf(val) !== -1 || d['rootCategory']['rootCategory'].toLowerCase().indexOf(val) !== -1
        return result
      }
      else if (d['rootCategory'] == undefined) {
        result = d['beverageName'].toLowerCase().indexOf(val) !== -1 || d['beverageCode'].toLowerCase().indexOf(val) !== -1
        return result
      }
      else if (d['varients'] == undefined) {
        result = d['beverageName'].toLowerCase().indexOf(val) !== -1 || d['beverageCode'].toLowerCase().indexOf(val) !== -1 || d['rootCategory']['rootCategory'].toLowerCase().indexOf(val) !== -1
        return result
      }
      else if (d['subVarient'] == undefined) {
        result = d['beverageName'].toLowerCase().indexOf(val) !== -1 || d['beverageCode'].toLowerCase().indexOf(val) !== -1 || d['varients']['varients'].toLowerCase().indexOf(val) !== -1 || d['rootCategory']['rootCategory'].toLowerCase().indexOf(val) !== -1
        return result
      } else if (d['type'] == undefined) {
        result = d['beverageName'].toLowerCase().indexOf(val) !== -1 || d['beverageCode'].toLowerCase().indexOf(val) !== -1 || d['subVarient']['subVarient'].toLowerCase().indexOf(val) !== -1 || d['varients']['varients'].toLowerCase().indexOf(val) !== -1 || d['rootCategory']['rootCategory'].toLowerCase().indexOf(val) !== -1
        return result
      }

    });


    //console.log(filter);
    this.dataSourceBeverage.data = filter;
  }


  applyFilterBeverageAttributeSet(event) {
    this.dataSourceretailBeverageAttributeSet.data = this.beverageattributeset_data
    //console.log(this.beverageattributeset_data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceretailBeverageAttributeSet.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['attributeSetCode'].toLowerCase().indexOf(val) !== -1 || d['attributeSetName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    //console.log(filter);
    this.dataSourceretailBeverageAttributeSet.data = filter;
  }

  applyFilterretailFoodAttributeSet(event) {
    this.dataSourceRetailFoodAttributeSet.data = this.retailfoodattributeset_data
    // //console.log(this.beverageattributeset_data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceRetailFoodAttributeSet.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['attributeSetCode'].toLowerCase().indexOf(val) !== -1 || d['attributeSetName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    //console.log(filter);
    this.dataSourceRetailFoodAttributeSet.data = filter;
  }

  applyFilterBeverageBrand(event) {
    this.dataSourceBeverageBrands.data = this.beverageBrand_data
    //console.log( this.dataSourceBeverageBrands.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceBeverageBrands.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['country'].toLowerCase().indexOf(val) !== -1 || d['brandCode'].toLowerCase().indexOf(val) !== -1 || d['brandName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    //console.log(filter);
    this.dataSourceBeverageBrands.data = filter;
  }

  applyFilterBeveragesAttribute(event) {
    this.dataSourceBeverageAttribute.data = this.beverageattribute_data
    //console.log( this.dataSourceBeverageAttribute.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceBeverageAttribute.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['attributeCode'].toLowerCase().indexOf(val) !== -1 || d['attributeName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    //console.log(filter);
    this.dataSourceBeverageAttribute.data = filter;
  }

  applyFilterBeverageProducts(event) {
    this.dataSourceBeverageProduct.data = this.beverageproduct_data
    //console.log( this.dataSourceBeverageProduct.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceBeverageProduct.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['productCode'].toLowerCase().indexOf(val) !== -1 || d['productName'].toLowerCase().indexOf(val) !== -1 || d['associatedBrand']['brandName'].toLowerCase().indexOf(val) !== -1 || d['varient'].toLowerCase().indexOf(val) !== -1
      return result

    });


    //console.log(filter);
    this.dataSourceBeverageProduct.data = filter;

  }

  applyFilterRetailFoodBrand(event) {
    this.dataSourceRetailFoodBrands.data = this.retailfoodbrand_data
    //console.log( this.dataSourceRetailFoodBrands.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceRetailFoodBrands.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['brandCode'].toLowerCase().indexOf(val) !== -1 || d['brandName'].toLowerCase().indexOf(val) !== -1 || d['country'].toLowerCase().indexOf(val) !== -1
      return result

    });


    //console.log(filter);
    this.dataSourceRetailFoodBrands.data = filter;
  }

  applyFilterRetailBeveragesBrand(event) {
    this.dataSourceRetailBeveragesBrands.data = this.retailBeveragesbrand_data
    //console.log( this.dataSourceRetailBeveragesBrands.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceRetailBeveragesBrands.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['brandCode'].toLowerCase().indexOf(val) !== -1 || d['brandName'].toLowerCase().indexOf(val) !== -1 || d['country'].toLowerCase().indexOf(val) !== -1
      return result

    });


    //console.log(filter);
    this.dataSourceRetailBeveragesBrands.data = filter;
  }


  applyFilterRetailFoodProduct(event) {
    //console.log('HII', this.retail_food_product);
    //console.log(event);
    this.dataSourceRetailFoodProduct.data = this.retail_food_product
    const val = event.target.value.trim().toLowerCase();
    let result
    const filter = this.dataSourceRetailFoodProduct.data.filter(function (d) {

    
      if (d['type'] != undefined && d['subVarient'] != undefined && d['varient'] != undefined && d['rootCategory'] != undefined) {

        return d['productName'].toLowerCase().indexOf(val) !== -1 || d['productCode'].toLowerCase().indexOf(val) !== -1 || d['varient']['varient'].toLowerCase().indexOf(val) !== -1 || d['rootCategory']['rootCategory'].toLowerCase().indexOf(val) !== -1 || d['subVarient']['subVarient'].toLowerCase().indexOf(val) !== -1 || d['type']['type'].toLowerCase().indexOf(val) !== -1 || d['associatedBrand']['brandName'].toLowerCase().indexOf(val) !== -1
      }else if (d['varient'] == undefined) {
        return d['productName'].toLowerCase().indexOf(val) !== -1 || d['productCode'].toLowerCase().indexOf(val) !== -1 || d['rootCategory']['rootCategory'].toLowerCase().indexOf(val) !== -1 || d['associatedBrand']['brandName'].toLowerCase().indexOf(val) !== -1
      }
      else if (d['subVarient'] == undefined) {
        return d['productName'].toLowerCase().indexOf(val) !== -1 || d['productCode'].toLowerCase().indexOf(val) !== -1 || d['varient']['varient'].toLowerCase().indexOf(val) !== -1 || d['rootCategory']['rootCategory'].toLowerCase().indexOf(val) !== -1 || d['associatedBrand']['brandName'].toLowerCase().indexOf(val) !== -1 
      }
      else if (d['type'] == undefined) {
        return d['productName'].toLowerCase().indexOf(val) !== -1 || d['productCode'].toLowerCase().indexOf(val) !== -1 || d['varient']['varient'].toLowerCase().indexOf(val) !== -1 || d['rootCategory']['rootCategory'].toLowerCase().indexOf(val) !== -1 || d['subVarient']['subVarient'].toLowerCase().indexOf(val) !== -1 || d['associatedBrand']['brandName'].toLowerCase().indexOf(val) !== -1
      
      }

    });
    this.dataSourceRetailFoodProduct.data = filter;
  }

  applyFilterRetailBeveragesProduct(event) {
    //console.log('HII', this.retail_food_product);
    //console.log(event);
    this.dataSourceRetailBeveragesProduct.data = this.retail_beverages_product
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceRetailBeveragesProduct.data.filter(function (d) {
      return d['productName'].toLowerCase().indexOf(val) !== -1 || d['productCode'].toLowerCase().indexOf(val) !== -1 || d['varient'].toLowerCase().indexOf(val) !== -1 || d['rootCategory'].toLowerCase().indexOf(val) !== -1 || d['subVarient'].toLowerCase().indexOf(val) !== -1 || d['type'].toLowerCase().indexOf(val) !== -1 || d['associatedBrand']['brandName'].toLowerCase().indexOf(val) !== -1
    });

    //console.log(filter);
    this.dataSourceRetailBeveragesProduct.data = filter;
  }

  applyFilterRetailFoodAttribute(event) {
    this.dataSourceRetailFoodAttribute.data = this.retailFoodattribute_data
    //console.log( this.dataSourceRetailFoodAttribute.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceRetailFoodAttribute.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['attributeCode'].toLowerCase().indexOf(val) !== -1 || d['attributeName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    //console.log(filter);
    this.dataSourceRetailFoodAttribute.data = filter;


  }
  applyFilterRetailBeveragesAttribute(event) {
    this.dataSourceretailBeverageAttribute.data = this.retailbeverageattribute_data
    //console.log( this.dataSourceRetailFoodAttribute.data);
    const val = event.target.value.trim().toLowerCase();
    const filter = this.dataSourceretailBeverageAttribute.data.filter(function (d) {
      //console.log(d);
      let result

      result = d['attributeCode'].toLowerCase().indexOf(val) !== -1 || d['attributeName'].toLowerCase().indexOf(val) !== -1
      return result

    });


    //console.log(filter);
    this.dataSourceretailBeverageAttribute.data = filter;


  }

  ////////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////// ///////////////    LIQUOR CATEGORY TREE //////////////////////////////  ///////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////




  // *****************************  Liquor tree *******************
  //open tree node and close
  caretClick() {
    var toggler = document.getElementsByClassName("caret");
    var i;
    for (i = 0; i < toggler.length; i++) {
      toggler[i].addEventListener("click", function () {
        this.parentElement.querySelector(".nested").classList.toggle("active");
        this.classList.toggle("caret-down");
      });
    }
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////// BEVERAGE CATEGORY TREE ////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////


  getAllBeveragesCategories() {
    this.menuService.getAllBeverageCategories().subscribe(data => {
      //console.log("All Beverages Categories", data);
      if (data['sucess'] == false) {
        //console.log("Beverages categories data not found");
      } else {
        this.beverages = data['data']
        this.beverages.forEach((element, index) => {
          // if (index == rootindex) {
          element.edit = false
          if (
            this.beverages[index].hasOwnProperty('subCategories')
            &&
            this.beverages[index].subCategories.length > 0
          ) {
            this.beverages[index].subCategories.forEach((element, subcategoryIndex = index) => {
              element.edit = false
              if (
                this.beverages[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
                &&
                this.beverages[index].subCategories[subcategoryIndex].subSubCategories.length > 0
              ) {
                // tslint:disable-next-line: max-line-length
                this.beverages[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
                  element.edit = false;
                  if (
                    // tslint:disable-next-line: max-line-length
                    this.beverages[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                    &&
                    // tslint:disable-next-line: max-line-length
                    this.beverages[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                  ) {
                    // tslint:disable-next-line: max-line-length
                    //console.log(element);
                    this.beverages[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, subSubCategoryTypeIndex = index) => {
                      element.edit = false;
                    })
                  }


                })
              }
            })
          }
          // }
        });
        this.allInHouseBeveragesCategories = this.beverages.filter(element => element.beverageType == "inHouseBeverage")
        this.allRetailBeveragesCategories = this.beverages.filter(element => element.beverageType == "retailBeverage")
        this.allInHouseBeveragesCategories.forEach(element => {
          this.inhouseheveragesRoot.push(element['rootCategoryName']);

        });
        this.allRetailBeveragesCategories.forEach(element => {
          this.retailbeveragesRoot.push(element['rootCategoryName']);

        });
        console.log(this.allInHouseBeveragesCategories);
        console.log(this.allRetailBeveragesCategories);
      }

    })



  }

  //*************************OPEN ROOT CATEGORY IN BEVERAGE ********************** */
  openBeveragesRootCategoryDailog(beverageType) {
    const dialogRef = this.dialog.open(RootCategoryBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: beverageType
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }

  //*************************OPEN SUB CATEGORY IN BEVERAGE ********************** */
  openBeveragesSubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }

  //*************************OPEN SUB SUB CATEGORY IN BEVERAGE ********************** */
  openBeveragesSubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN BEVERAGE ********************** */
  openBeveragesSubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeBeverageComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllBeveragesCategories()
    })
  }




  ////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// RETAIL FOOD CATEGORY TREE //////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////////////////


  getAllRetailFoodCategories() {
    this.menuService.getAllRetailFoodCategories().subscribe(data => {
      //console.log("All Retail Food categories", data);
      this.allRetailFoodCategories = data['data']
      this.allRetailFoodCategories.forEach(element => {
        this.retailFoodcategory.push(element['rootCategoryName'])
      });
      this.allRetailFoodCategories.forEach((element, index) => {
        // if (index == rootindex) {
        element.edit = false
        if (
          this.allRetailFoodCategories[index].hasOwnProperty('subCategories')
          &&
          this.allRetailFoodCategories[index].subCategories.length > 0
        ) {
          this.allRetailFoodCategories[index].subCategories.forEach((element, subcategoryIndex = index) => {
            element.edit = false;
            if (
              this.allRetailFoodCategories[index].subCategories[subcategoryIndex].hasOwnProperty('subSubCategories')
              &&
              this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.length > 0
            ) {
              // tslint:disable-next-line: max-line-length
              this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories.forEach((element, subsubcategoryINdex = index) => {
                element.edit = false
                if (
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].hasOwnProperty('subSubCategoryType')
                  &&
                  // tslint:disable-next-line: max-line-length
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.length > 0
                ) {
                  // tslint:disable-next-line: max-line-length
                  //console.log(element);
                  this.allRetailFoodCategories[index].subCategories[subcategoryIndex].subSubCategories[subsubcategoryINdex].subSubCategoryType.forEach((element, subSubCategoryTypeIndex = index) => {
                    element.edit = false;
                  })
                }


              })
            }
          })
        }
        // }
      });
    })
  }


  //*************************OPEN ROOT CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodRootCategoryDailog() {
    const dialogRef = this.dialog.open(RootCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  //*************************OPEN SUB CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodSubCategoryDialog(rootCategoryid) {
    const dialogRef = this.dialog.open(SubCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: rootCategoryid
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  //*************************OPEN SUB SUB CATEGORY IN RETAIL FOOD ********************** */
  openRetailFoodSubSubCategoryDialog(rootCategoryID, subCategoryID) {
    const dialogRef = this.dialog.open(SubSubCategoryRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subCategoryID": subCategoryID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }


  //*************************OPEN SUB SUB CATEGORY TYPE IN RETAIL FOOD ********************** */
  openRetailFoodSubSubCategoryTypeDialog(rootCategoryID, subsubCategortID) {
    const dialogRef = this.dialog.open(SubSubCategoryTypeRetailFoodComponent, {
      width: '460px',
      disableClose: true,
      data: { "rootCategoryID": rootCategoryID, "subsubCategortID": subsubCategortID }
    })
    dialogRef.afterClosed().subscribe(result => {
      this.getAllRetailFoodCategories()
    })
  }

  ////////////////////////////////////////////////// retail Beverages Product /////////////////////////////
  addRetailBeveragesProduct() {
    //  let data = this.allMenuData.allRetailFoodProducts
    this.menuService.getAllRetailBeveragesProduct().subscribe(data => {
      console.log("All Retail food Products", data);
      this.retail_beverages_product = data['data']
      if (data['success'] == false) {
        this.retailBeveragesProductCount = 0
        this.noData2 = 0
        this.noData40 = 0
        //console.log("Retail Food Product Data Not Found");
      } else {
        this.retailBeveragesProductCount = data['data'].length
        data['data'].forEach(element => {
          element.productCode = "P" + element.productCode
        });
        this.dataSourceRetailBeveragesProduct = new MatTableDataSource(data['data'])

        this.dataSourceRetailBeveragesProduct.paginator = this.retailBeveragesProductspaginator
        this.dataSourceRetailBeveragesProduct.sort = this.retailBeveragesProductsSort
        this.noData2 = this.dataSourceRetailBeveragesProduct.connect().pipe(map(data => data.length === 0));

        data['data'].forEach(element => {
          var d = new Date(element.updatedAt)
          var date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
          var productObj = {}
          productObj = {
            "productCode": element.productCode,
            "productName": element.productName,
            "rootCategory": element.rootCategory,
            "varient": element.varient,
            "subVarient": element.subVarient,
            "type": element.type,
            "attributeSet": element.attributeSet.attributeSetName,
            "associatedBrand": element.associatedBrand,
            "unit": element.unit,
            "status": element.status,
            "addedBy": element.addedBy.userName,
            "updatedBy": element.updatedBy.userName,
            "category": element.category,
            "updatedAt": date
          }
          this.retailBeveragesProductJSONDATA.push(productObj)
        });
        //console.log("retail food product export", this.retailFoodProductJSONDATA);
      }
    })
  }
  ///////////////////////////////////////////////////////// filter In house beverages //////////////////////////////////////

  selectAllinBeverage(event) {
    console.log(event.checked);

    if (event.checked == true) {
      this.beveragesfilter.Veg = true
      this.beveragesfilter.SeaFood = true
      this.beveragesfilter.Egg = true
      this.beveragesfilter.NonVeg = true
    } else {
      this.beveragesfilter.Veg = false
      this.beveragesfilter.SeaFood = false
      this.beveragesfilter.Egg = false
      this.beveragesfilter.NonVeg = false
    }
  }
  filterInHouseBeveragesvalue(obj) {
    if (this.beveragesfilter.Veg == false || this.beveragesfilter.SeaFood == false || this.beveragesfilter.Egg == false || this.beveragesfilter.NonVeg == false) {

      this.beveragesfilter.all = false
    }
    else if (this.beveragesfilter.Veg == true && this.beveragesfilter.SeaFood == true && this.beveragesfilter.Egg == true && this.beveragesfilter.NonVeg == true) {

      this.beveragesfilter.all = true
    }
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceBeverage.data = this.beverage_data
    } else {
      let beverage_filter = this.beverage_data
      this.dataSourceBeverage.data = this.beverage_data
      console.log(beverage_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('Category') && obj['Category'].length > 0) {
        filter = beverage_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['Category'].includes(d['rootCategory']['rootCategory'])
          return result
        });
        beverage_filter = filter
      }
      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = beverage_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        beverage_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = beverage_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        beverage_filter = filter
      }
      this.v1 = []
      if (obj.hasOwnProperty('Veg') || obj.hasOwnProperty('NonVeg') || obj.hasOwnProperty('SeaFood') || obj.hasOwnProperty('Egg') || obj.hasOwnProperty('all')) {
        if (obj.hasOwnProperty('all') && obj['all'] == true) {
          this.v1 = ['Veg', 'Egg', 'Non-Veg', 'Sea Food']
        } else {
          this.v1 = []
        }
        if (obj.hasOwnProperty('Veg') && obj['Veg'] == true) {
          this.v1.push('Veg')
        }
        else if (obj.hasOwnProperty('Veg') && obj['Veg'] == false) {
          // this.v1 = this.v1.filter(e => e !== 'Veg');
          // console.log(this.v1);

          delete obj['Veg']


        }

        if (obj.hasOwnProperty('NonVeg') && obj['NonVeg'] == true) {
          this.v1.push('Non-Veg')
        }
        else if (obj.hasOwnProperty('NonVeg') && obj['NonVeg'] == false) {
          // this.v1 = this.v1.filter(e => e !== 'NonVeg');
          // console.log(this.v1);
          delete obj['NonVeg']


        }
        if (obj.hasOwnProperty('Egg') && obj['Egg'] == true) {
          this.v1.push('Egg')
        }
        else if (obj.hasOwnProperty('Egg') && obj['Egg'] == false) {
          delete obj['Egg']

        }
        if (obj.hasOwnProperty('SeaFood') && obj['SeaFood'] == true) {
          this.v1.push('Sea Food')
        }
        else if (obj.hasOwnProperty('SeaFood') && obj['SeaFood'] == false) {
          delete obj['SeaFood']


        }
        this.v1 = _.union(this.v1);
        console.log(this.v1, "><");
        let filter_veg = this.v1
        filter = beverage_filter.filter(function (d) {
          // console.log(d);
          let result
          result = filter_veg.includes(d['veg_nonveg'])
          return result
        });
      }
      console.log(filter);
      this.dataSourceBeverage.data = filter
      this.dataSourceBeverage = new MatTableDataSource<any>(filter);
      this.noData12 = this.dataSourceBeverage.connect().pipe(map(data => data.length === 0));
      this.dataSourceBeverage.paginator = this.beveragepaginator;
      this.dataSourceBeverage.sort = this.beverageSort;


    }
  }
  resetInHouseBeveragesfilter() {
    this.dataSourceBeverage = new MatTableDataSource<any>(this.beverage_data);
    this.noData12 = this.dataSourceBeverage.connect().pipe(map(data => data.length === 0));
    this.dataSourceBeverage.paginator = this.beveragepaginator;
    this.dataSourceBeverage.sort = this.beverageSort;
    setTimeout(() => {
      this.beveragesfilter.Veg = true
      this.beveragesfilter.SeaFood = true
      this.beveragesfilter.Egg = true
      this.beveragesfilter.NonVeg = true
      this.beveragesfilter.all = true
    }, 20)

  }
  /////////////////////////////////////////////// retail brnad filter ////////////////////////////////////////////////////
  filterretailBrandvalue(obj) {
    // ;
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceRetailFoodBrands.data = this.retailfoodbrand_data
    } else {
      let retailbrand_filter = this.retailfoodbrand_data
      this.dataSourceRetailFoodBrands.data = this.retailfoodbrand_data
      console.log(retailbrand_filter);

      let data = []
      let filter

      if (obj.hasOwnProperty('country') && (obj['country'].length > 0)) {
        filter = retailbrand_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['country'].includes(d['country'])
          return result
        });
        retailbrand_filter = filter
      }


      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = retailbrand_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        retailbrand_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = retailbrand_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        retailbrand_filter = filter
      }


      console.log(filter);
      data = filter
      console.log(data);
      // this.dataSourceBeverage.data = filter
      this.dataSourceRetailFoodBrands = new MatTableDataSource<any>(data);
      this.noData18 = this.dataSourceRetailFoodBrands.connect().pipe(map(data => data.length === 0));
      this.dataSourceRetailFoodBrands.paginator = this.retailFoodBrandspaginator;
      this.dataSourceRetailFoodBrands.sort = this.retailFoodBrandsSort;

    }
  }
  resetretailbrandfilter() {
    this.dataSourceRetailFoodBrands = new MatTableDataSource<any>(this.retailfoodbrand_data);
    this.noData18 = this.dataSourceRetailFoodBrands.connect().pipe(map(data => data.length === 0));
    this.dataSourceRetailFoodBrands.paginator = this.retailFoodBrandspaginator;
    this.dataSourceRetailFoodBrands.sort = this.retailFoodBrandsSort;
  }
  ///////////////////////////////// filter retail food beverages //////////////////////////////////
  filterbeveragesBrandvalue(obj) {
    // ;
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceRetailBeveragesBrands.data = this.retailBeveragesbrand_data
    } else {
      let retailbeveragesbrand_filter = this.retailBeveragesbrand_data
      this.dataSourceRetailBeveragesBrands.data = this.retailBeveragesbrand_data
      console.log(retailbeveragesbrand_filter);

      let data = []
      let filter

      if (obj.hasOwnProperty('country') && (obj['country'].length > 0)) {
        filter = retailbeveragesbrand_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['country'].includes(d['country'])
          return result
        });
        retailbeveragesbrand_filter = filter
      }


      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = retailbeveragesbrand_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        retailbeveragesbrand_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = retailbeveragesbrand_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        retailbeveragesbrand_filter = filter
      }


      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceRetailBeveragesBrands = new MatTableDataSource<any>(data);
      this.noData1 = this.dataSourceRetailBeveragesBrands.connect().pipe(map(data => data.length === 0));
      this.dataSourceRetailBeveragesBrands.paginator = this.retailBeveragesBrandspaginator;
      this.dataSourceRetailBeveragesBrands.sort = this.retailBeveragesBrandsSort;

    }
  }
  resetretailbeveragesbrandfilter() {
    this.dataSourceRetailBeveragesBrands = new MatTableDataSource<any>(this.retailBeveragesbrand_data);
    this.noData1 = this.dataSourceRetailBeveragesBrands.connect().pipe(map(data => data.length === 0));
    this.dataSourceRetailBeveragesBrands.paginator = this.retailBeveragesBrandspaginator;
    this.dataSourceRetailBeveragesBrands.sort = this.retailBeveragesBrandsSort;

  }
  /////////////////////////// filter retail food attribute ///////////////////////////////
  filterfoodAttributevalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceRetailFoodAttribute.data = this.retailFoodattribute_data
    } else {
      let attribute_filter = this.retailFoodattribute_data
      this.dataSourceRetailFoodAttribute.data = this.retailFoodattribute_data
      console.log(attribute_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('type') && obj['type'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['type'].includes(d['responseType']['elementName'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('filter') && obj['filter'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['filter'].includes(d['isFilterable'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('search') && obj['search'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['search'].includes(d['isSearchable'])
          return result
        });
        attribute_filter = filter
      }


      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceRetailFoodAttribute = new MatTableDataSource<any>(data);
      this.noData19 = this.dataSourceRetailFoodAttribute.connect().pipe(map(data => data.length === 0));
      this.dataSourceRetailFoodAttribute.paginator = this.retailFoodAttributepaginator;
      this.dataSourceRetailFoodAttribute.sort = this.retailFoodAttributesort;

    }


  }
  /////////////////////////// filter retail beverages attribute ///////////////////////////////
  filterbeveragesAttributevalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceretailBeverageAttribute.data = this.retailbeverageattribute_data
    } else {
      let attribute_filter = this.retailbeverageattribute_data
      this.dataSourceretailBeverageAttribute.data = this.retailbeverageattribute_data
      console.log(attribute_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('type') && obj['type'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['type'].includes(d['responseType']['elementName'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('filter') && obj['filter'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['filter'].includes(d['isFilterable'])
          return result
        });
        attribute_filter = filter
      }
      if (obj.hasOwnProperty('search') && obj['search'].length > 0) {
        filter = attribute_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['search'].includes(d['isSearchable'])
          return result
        });
        attribute_filter = filter
      }


      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceretailBeverageAttribute = new MatTableDataSource<any>(data);
      this.noData3= this.dataSourceretailBeverageAttribute.connect().pipe(map(data => data.length === 0));
      this.dataSourceretailBeverageAttribute.paginator = this.retailbeverageAttributepaginator;
      this.dataSourceretailBeverageAttribute.sort = this.retailbeverageAttributesort;

    }


  }
  resetbeveragesAttributefilter() {
    console.log("hi");
    
    this.dataSourceretailBeverageAttribute = new MatTableDataSource<any>(this.retailbeverageattribute_data);
      this.noData3= this.dataSourceretailBeverageAttribute.connect().pipe(map(data => data.length === 0));
      this.dataSourceretailBeverageAttribute.paginator = this.retailbeverageAttributepaginator;
      this.dataSourceretailBeverageAttribute.sort = this.retailbeverageAttributesort;

  }
  resetfoodAttributefilter() {
    this.dataSourceRetailFoodAttribute = new MatTableDataSource<any>(this.retailFoodattribute_data);
    this.noData19 = this.dataSourceRetailFoodAttribute.connect().pipe(map(data => data.length === 0));
    this.dataSourceRetailFoodAttribute.paginator = this.retailFoodAttributepaginator;
    this.dataSourceRetailFoodAttribute.sort = this.retailFoodAttributesort;
  }
  //////////////////////////////////// filter attribute set ////////////////////////////
  filterbeveragesAttributeSetvalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceretailBeverageAttributeSet.data = this.beverageattributeset_data
    } else {
      let attributeSet_filter = this.beverageattributeset_data
      this.dataSourceretailBeverageAttributeSet.data = this.beverageattributeset_data
      console.log(attributeSet_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = attributeSet_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        attributeSet_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = attributeSet_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        attributeSet_filter = filter
      }

      console.log(filter);
      data = filter
      console.log(data);
      // this.dataSourceretailBeverageAttributeSet.data = 
      this.dataSourceretailBeverageAttributeSet = new MatTableDataSource<any>(data);
      this.noData4= this.dataSourceretailBeverageAttributeSet.connect().pipe(map(data => data.length === 0));
      this.dataSourceretailBeverageAttributeSet.paginator = this.retailBeverageAttributeSetpaginator;
      this.dataSourceretailBeverageAttributeSet.sort = this.retailBeverageAttributeSetSort;

    }


  }
  filterfoodAttributeSetvalue(obj) {
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceRetailFoodAttributeSet.data = this.retailfoodattributeset_data
    } else {
      let attributeSet_filter = this.retailfoodattributeset_data
      this.dataSourceRetailFoodAttributeSet.data = this.retailfoodattributeset_data
      console.log(attributeSet_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = attributeSet_filter.filter(function (d) {
          console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        attributeSet_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = attributeSet_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        attributeSet_filter = filter
      }

      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceRetailFoodAttributeSet = new MatTableDataSource<any>(data);
      this.noData20 = this.dataSourceRetailFoodAttributeSet.connect().pipe(map(data => data.length === 0));
      this.dataSourceRetailFoodAttributeSet.paginator = this.RetailFoodAttributeSetpaginator;
      this.dataSourceRetailFoodAttributeSet.sort = this.RetailFoodAttributeSetSort;
    }


  }
  resetfoodAttributeSetfilter() {
    this.dataSourceRetailFoodAttributeSet = new MatTableDataSource<any>(this.retailfoodattributeset_data);
      this.noData20 = this.dataSourceRetailFoodAttributeSet.connect().pipe(map(data => data.length === 0));
      this.dataSourceRetailFoodAttributeSet.paginator = this.RetailFoodAttributeSetpaginator;
      this.dataSourceRetailFoodAttributeSet.sort = this.RetailFoodAttributeSetSort;
  }
  resetbeveragesAttributeSetfilter() {
    this.dataSourceretailBeverageAttributeSet = new MatTableDataSource<any>(this.beverageattributeset_data);
    this.noData4= this.dataSourceretailBeverageAttributeSet.connect().pipe(map(data => data.length === 0));
    this.dataSourceretailBeverageAttributeSet.paginator = this.retailBeverageAttributeSetpaginator;
    this.dataSourceretailBeverageAttributeSet.sort = this.retailBeverageAttributeSetSort;

  }
  filterfoodproductvalue(obj) {

    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceRetailFoodProduct.data = this.retail_food_product
    } else {
      let foodproduct_filter = this.retail_food_product
      this.dataSourceRetailFoodProduct.data = this.retail_food_product
      console.log(foodproduct_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('category') && obj['category'].length > 0) {
        filter = foodproduct_filter.filter(function (d) {
          //  console.log(d);
          let result
          result = obj['category'].includes(d['rootCategory']['rootCategory'])

          return result
        });
        foodproduct_filter = filter
      }
      if (obj.hasOwnProperty('brand') && obj['brand'].length > 0) {
        filter = foodproduct_filter.filter(function (d) {
          //  console.log(d);
          let result
          result = obj['brand'].includes(d['associatedBrand']['brandName'])

          return result
        });
        foodproduct_filter = filter
      }

      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = foodproduct_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        foodproduct_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = foodproduct_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        foodproduct_filter = filter
      }
      if (obj.hasOwnProperty('Veg') || obj.hasOwnProperty('NonVeg') || obj.hasOwnProperty('SeaFood') || obj.hasOwnProperty('Egg')) {
        if (obj.hasOwnProperty('Veg') && obj['Veg'] == true) {
          this.v1.push('Veg')
        }
        else if (obj.hasOwnProperty('Veg') && obj['Veg'] == false) {
          this.v1 = this.v1.filter(e => e !== 'Veg');
        }

        if (obj.hasOwnProperty('NonVeg') && obj['NonVeg'] == true) {
          this.v1.push('Non-Veg')
        }
        else if (obj.hasOwnProperty('NonVeg') && obj['NonVeg'] == false) {
          this.v1 = this.v1.filter(e => e !== 'NonVeg');
        }
        if (obj.hasOwnProperty('Egg') && obj['Egg'] == true) {
          this.v1.push('Egg')
        }
        else if (obj.hasOwnProperty('Egg') && obj['Egg'] == false) {
          this.v1 = this.v1.filter(e => e !== 'Egg');
        }
        if (obj.hasOwnProperty('SeaFood') && obj['SeaFood'] == true) {
          this.v1.push('Sea Food')
        }
        else if (obj.hasOwnProperty('SeaFood') && obj['SeaFood'] == false) {
          this.v1 = this.v1.filter(e => e !== 'SeaFood');
        }
        this.v1 = _.union(this.v1);
        console.log(this.v1, "><");
        let filter_veg = this.v1
        filter = foodproduct_filter.filter(function (d) {
          // console.log(d);
          let result
          result = filter_veg.includes(d['veg_nonveg'])
          return result
        });
      }
      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceRetailFoodProduct = new MatTableDataSource<any>(data);
      this.noData17 = this.dataSourceRetailFoodProduct.connect().pipe(map(data => data.length === 0));
      this.dataSourceRetailFoodProduct.paginator = this.retailFoodProductspaginator;
      this.dataSourceRetailFoodProduct.sort = this.retailFoodProductsSort;

    }

  }
  resetfoodproductfilter() {
    this.dataSourceRetailFoodProduct = new MatTableDataSource<any>(this.retail_food_product);
    this.noData17 = this.dataSourceRetailFoodProduct.connect().pipe(map(data => data.length === 0));
    this.dataSourceRetailFoodProduct.paginator = this.retailFoodProductspaginator;
    this.dataSourceRetailFoodProduct.sort = this.retailFoodProductsSort;
  }
  filterBeveragesproductvalue(obj) {

    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined || obj[key].length == 0) && delete obj[key]);

    console.log(obj);
    if (Object.keys(obj).length === 0) {
      this.dataSourceRetailBeveragesProduct.data = this.retail_beverages_product
    } else {
      let foodproduct_filter = this.retail_beverages_product
      this.dataSourceRetailBeveragesProduct.data = this.retail_beverages_product
      console.log(foodproduct_filter);

      let data = []
      let filter
      if (obj.hasOwnProperty('category') && obj['category'].length > 0) {
        filter = foodproduct_filter.filter(function (d) {
          //  console.log(d);
          let result
          result = obj['category'].includes(d['rootCategory'])

          return result
        });
        foodproduct_filter = filter
      }
      if (obj.hasOwnProperty('brand') && obj['brand'].length > 0) {
        filter = foodproduct_filter.filter(function (d) {
          //  console.log(d);
          let result
          result = obj['brand'].includes(d['associatedBrand']['brandName'])

          return result
        });
        foodproduct_filter = filter
      }

      if (obj.hasOwnProperty('addedby') && obj['addedby'].length > 0) {
        filter = foodproduct_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['addedby'].includes(d['addedBy']['userName'])
          return result
        });
        foodproduct_filter = filter
      }
      if (obj.hasOwnProperty('status') && obj['status'].length > 0) {
        filter = foodproduct_filter.filter(function (d) {
          // console.log(d);
          let result
          result = obj['status'].includes(d['status'])
          return result
        });
        foodproduct_filter = filter
      }
      if (obj.hasOwnProperty('Veg') || obj.hasOwnProperty('NonVeg') || obj.hasOwnProperty('SeaFood') || obj.hasOwnProperty('Egg')) {
        if (obj.hasOwnProperty('Veg') && obj['Veg'] == true) {
          this.v1.push('Veg')
        }
        else if (obj.hasOwnProperty('Veg') && obj['Veg'] == false) {
          this.v1 = this.v1.filter(e => e !== 'Veg');
        }

        if (obj.hasOwnProperty('NonVeg') && obj['NonVeg'] == true) {
          this.v1.push('Non-Veg')
        }
        else if (obj.hasOwnProperty('NonVeg') && obj['NonVeg'] == false) {
          this.v1 = this.v1.filter(e => e !== 'NonVeg');
        }
        if (obj.hasOwnProperty('Egg') && obj['Egg'] == true) {
          this.v1.push('Egg')
        }
        else if (obj.hasOwnProperty('Egg') && obj['Egg'] == false) {
          this.v1 = this.v1.filter(e => e !== 'Egg');
        }
        if (obj.hasOwnProperty('SeaFood') && obj['SeaFood'] == true) {
          this.v1.push('Sea Food')
        }
        else if (obj.hasOwnProperty('SeaFood') && obj['SeaFood'] == false) {
          this.v1 = this.v1.filter(e => e !== 'SeaFood');
        }
        this.v1 = _.union(this.v1);
        console.log(this.v1, "><");
        let filter_veg = this.v1
        filter = foodproduct_filter.filter(function (d) {
          // console.log(d);
          let result
          result = filter_veg.includes(d['veg_nonveg'])
          return result
        });
      }
      console.log(filter);
      data = filter
      console.log(data);
      this.dataSourceRetailBeveragesProduct = new MatTableDataSource<any>(data);
      this.noData2 = this.dataSourceRetailBeveragesProduct.connect().pipe(map(data => data.length === 0));
      this.dataSourceRetailBeveragesProduct.paginator = this.retailFoodProductspaginator;
      this.dataSourceRetailBeveragesProduct.sort = this.retailFoodProductsSort;

    }

  }
  resetBeveragesproductfilter() {
    // this.dataSourceRetailBeveragesProduct.data = this.retail_beverages_product
    this.dataSourceRetailBeveragesProduct = new MatTableDataSource<any>(this.retail_beverages_product);
      this.noData2 = this.dataSourceRetailBeveragesProduct.connect().pipe(map(data => data.length === 0));
      this.dataSourceRetailBeveragesProduct.paginator = this.retailFoodProductspaginator;
      this.dataSourceRetailBeveragesProduct.sort = this.retailFoodProductsSort;
  }
  ///////////////////////////////////////////////////////Beverages tree edit//////////////////////////////////////////////////////////////////////////////////////
  OpenRetailTreeroot(root_i) {
    this.allRetailFoodCategories[root_i].edit = true
  }
  updateRetailTreeRoot(id, root, i) {
    console.log(id, root);
    let obj = {
      rootCategoryName: root
    }
    this.menuService.UpdateRetailFoodTreeRoot(id, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)
        Swal.fire("Root updated successfully", "", "success")
      this.getAllRetailFoodCategories();
      this.getAllRetailFoodBrands();
      this.getAllRetailFoodProducts();
      this.getAllLiquorAttribute();
      this.getAllLiquorAttributeSet()
      this.allRetailFoodCategories[i].edit = false
    })
  }
  cancelRetailRoot(root_i) {
    this.allRetailFoodCategories[root_i].edit = false
  }
  OpenRetailTreeSub(root_i, sub_i) {
    this.allRetailFoodCategories[root_i]['subCategories'][sub_i].edit = true
  }
  updateRetailsubTree(id, idsub, sub, root_i, sub_i) {
    // console.log(id, idv, varient, root_i, varient_i);
    let obj = {
      subCategoryName: sub
    }
    this.menuService.UpdateRetailFoodTreeSub(id, idsub, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Sub Category updated successfully", "", "success")
      this.getAllRetailFoodCategories();
      this.getAllRetailFoodBrands();
      this.getAllRetailFoodProducts();
      this.getAllLiquorAttribute();
      this.getAllLiquorAttributeSet()
      this.allRetailFoodCategories[root_i]['subCategories'][sub_i].edit = false
    })

  }
  cancelRetailfoodSub(root_i, sub_i) {
    this.allRetailFoodCategories[root_i]['subCategories'][sub_i].edit = false
  }
  OpenRetailfoodTreeSubSub(root_i, sub_i, subsub_i) {
    this.allRetailFoodCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = true
  }
  updateRetailfoodSubSubTree(id, ids, idss, subsub, root_i, sub_i, subsub_i) {
    let obj = {
      subSubCategoryName: subsub
    }
    this.menuService.UpdateRetailFoodTreeSubSub(id, ids, idss, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Sub Sub Category updated successfully", "", "success")
      this.getAllRetailFoodCategories();
      this.getAllRetailFoodBrands();
      this.getAllRetailFoodProducts();
      this.getAllLiquorAttribute();
      this.getAllLiquorAttributeSet()
      this.allRetailFoodCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = false
    })

  }
  cancelRetailFoodSubSub(root_i, sub_i, subsub_i) {
    this.allRetailFoodCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = false
  }
  OpenRetailTreeType(root_i, sub_i, subsub_i, type_i) {
    console.log(root_i, sub_i, subsub_i, type_i);
    this.allRetailFoodCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = true
  }
  updateRetailtypeTree(id, ids, idss, idtype, type, root_i, sub_i, subsub_i, type_i) {
    let obj = {
      subSubCategoryTypeName: type
    }
    this.menuService.UpdateRetailFoodTreeType(id, ids, idss, idtype, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Type updated successfully", "", "success")
      this.getAllRetailFoodCategories();
      this.getAllRetailFoodBrands();
      this.getAllRetailFoodProducts();
      this.getAllLiquorAttribute();
      this.getAllLiquorAttributeSet()
      this.allRetailFoodCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = false
    })

  }
  cancelRetailType(root_i, sub_i, subsub_i, type_i) {
    this.allRetailFoodCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = false
  }

  ///////////////////////////////////////////////////////Beverages tree edit//////////////////////////////////////////////////////////////////////////////////////
  OpenBeverageTreeroot(root_i, type) {
    if (type == "inHouseBeverage") {
      this.allInHouseBeveragesCategories[root_i].edit = true
    } else {
      this.allRetailBeveragesCategories[root_i].edit = true

    }
  }
  updateBeverageTreeRoot(id, root, i, type) {
    console.log(id, root);
    let obj = {
      beverageType: type,
      rootCategoryName: root
    }
    this.menuService.UpdateBeveragesRoot(id, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)
        Swal.fire("Root updated successfully", "", "success")
      this.getAllRetailBeveragesBrands();
      this.addRetailBeveragesProduct();
      this.getAllBeveragesCategories();
      this.getAllLiquorAttribute();
      this.getAllLiquorAttributeSet()
      if (type == "inHouseBeverage") {
        this.allInHouseBeveragesCategories[i].edit = false
      } else {
        this.allRetailBeveragesCategories[i].edit = false

      }
    })
  }
  cancelBeverageRoot(root_i, type) {
    if (type == "inHouseBeverage") {
      this.allInHouseBeveragesCategories[root_i].edit = false
    } else {
      this.allRetailBeveragesCategories[root_i].edit = false

    }
  }
  OpenBeverageTreeSub(root_i, sub_i, type) {
    console.log(root_i, sub_i, type);

    if (type == "inHouseBeverage") {
      this.allInHouseBeveragesCategories[root_i]['subCategories'][sub_i].edit = true
    } else {
      this.allRetailBeveragesCategories[root_i]['subCategories'][sub_i].edit = true
    }
  }
  updateBeveragesubTree(id, idsub, sub, root_i, sub_i, type) {
    // console.log(id, idv, varient, root_i, varient_i);
    let obj = {
      subCategoryName: sub,
      beverageType: type
    }
    this.menuService.UpdateBeveragesTreeSub(id, idsub, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Sub Category updated successfully", "", "success")
      this.getAllRetailBeveragesBrands();
      this.addRetailBeveragesProduct();
      this.getAllBeveragesCategories();
      this.getAllLiquorAttribute();
      this.getAllLiquorAttributeSet()
      if (type == "inHouseBeverage") {
        this.allInHouseBeveragesCategories[root_i]['subCategories'][sub_i].edit = false
      } else {
        this.allRetailBeveragesCategories[root_i]['subCategories'][sub_i].edit = false
      }
    })

  }
  cancelBeveragefoodSub(root_i, sub_i, type) {
    if (type == "inHouseBeverage") {
      this.allInHouseBeveragesCategories[root_i]['subCategories'][sub_i].edit = false
    } else {
      this.allRetailBeveragesCategories[root_i]['subCategories'][sub_i].edit = false
    }
  }
  OpenBeverageTreeSubSub(root_i, sub_i, subsub_i, type) {
    if (type == "inHouseBeverage") {
      this.allInHouseBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = true
    } else {
      this.allRetailBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = true
    }
  }
  updateBeverageSubSubTree(id, ids, idss, subsub, root_i, sub_i, subsub_i, type) {
    let obj = {
      subSubCategoryName: subsub,
      beverageType: type
    }
    this.menuService.UpdateBeveragesTreeSubSub(id, ids, idss, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Sub Sub Category updated successfully", "", "success")
      this.getAllRetailBeveragesBrands();
      this.addRetailBeveragesProduct();
      this.getAllBeveragesCategories();
      this.getAllLiquorAttribute();
      this.getAllLiquorAttributeSet()
      if (type == "inHouseBeverage") {
        this.allInHouseBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = false
      } else {
        this.allRetailBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = false
      }
    })

  }
  cancelBeverageSubSub(root_i, sub_i, subsub_i, type) {
    if (type == "inHouseBeverage") {
      this.allInHouseBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = false
    } else {
      this.allRetailBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i].edit = false
    }
  }
  OpenBeverageTreeType(root_i, sub_i, subsub_i, type_i, type) {
    if (type == "inHouseBeverage") {
      this.allInHouseBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = true
    } else {
      this.allRetailBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = true
    }
  }
  updateBeveragetypeTree(id, ids, idss, idtype, typeName, root_i, sub_i, subsub_i, type_i, type) {
    let obj = {
      subSubCategoryTypeName: typeName,
      beverageType: type
    }
    this.menuService.UpdateBeveragesTreeType(id, ids, idss, idtype, obj).subscribe(data => {
      console.log(data);
      if (data['success'] == true)

        Swal.fire("Type updated successfully", "", "success")
      this.getAllRetailBeveragesBrands();
      this.addRetailBeveragesProduct();
      this.getAllBeveragesCategories();
      this.getAllLiquorAttribute();
      this.getAllLiquorAttributeSet()
      if (type == "inHouseBeverage") {
        this.allInHouseBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = false
      } else {
        this.allRetailBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = false
      }
    })

  }
  cancelBeverageType(root_i, sub_i, subsub_i, type_i, type) {
    if (type == "inHouseBeverage") {
      this.allInHouseBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = false
    } else {
      this.allRetailBeveragesCategories[root_i]['subCategories'][sub_i]['subSubCategories'][subsub_i]['subSubCategoryType'][type_i].edit = false
    }
  }


}
