import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeHomeComponent } from './employee-home.component';
import { Routes, RouterModule } from '@angular/router';
import { PosMerchantEmpComponent } from './pos-merchant-emp/pos-merchant-emp.component';
import { BizlyEmpComponent } from './bizly-emp/bizly-emp.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { MaterialModule } from '';
// import { PrimengModule } from 'src/app/primeng/primeng.module'
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { MultiSelectModule } from 'primeng/multiselect';
import { AddRoleComponent } from './bizly-emp/add-role/add-role.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ViewRoleComponent } from './bizly-emp/view-role/view-role.component';
import { ViewUserComponent } from './bizly-emp/view-user/view-user.component';
import { AddUserComponent } from './bizly-emp/add-user/add-user.component';
import { AddSalaryComponent } from './bizly-emp/add-salary/add-salary.component'
import {RatingModule} from 'primeng/rating';
import { ViewRolposComponent } from './pos-merchant-emp/view-rolpos/view-rolpos.component';
import { ViewUserposComponent } from './pos-merchant-emp/view-userpos/view-userpos.component';
import { PopAddposRoleComponent } from './pos-merchant-emp/pop-addpos-role/pop-addpos-role.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { PopAddSalComponent } from './bizly-emp/pop-add-sal/pop-add-sal.component';
import { PaySalComponent } from './bizly-emp/pay-sal/pay-sal.component';
import { PopPaySalComponent } from './bizly-emp/pop-pay-sal/pop-pay-sal.component';
import {CalendarModule} from 'primeng/calendar';
import { PayLoanComponent } from './bizly-emp/pay-loan/pay-loan.component';
import { PayAllowanceComponent } from './bizly-emp/pay-allowance/pay-allowance.component';
import { PayDeductComponent } from './bizly-emp/pay-deduct/pay-deduct.component';
import { PayIncentiveComponent } from './bizly-emp/pay-incentive/pay-incentive.component';
import { LoanHistoryComponent } from './bizly-emp/loan-history/loan-history.component';
import { SalaryHistoryComponent } from './bizly-emp/salary-history/salary-history.component';
import { ViewLoanHistoryComponent } from './bizly-emp/view-loan-history/view-loan-history.component';
import { ReceiveEmiComponent } from './bizly-emp/receive-emi/receive-emi.component';
import { ViewSalaryComponent } from './bizly-emp/view-salary/view-salary.component';
import {TabViewModule} from 'primeng/tabview';
import {MatCardModule} from '@angular/material/card';
import {CardModule} from 'primeng/card';



const routes: Routes = [

  {
    path: '',
    redirectTo: '/employee/bizly',
    pathMatch: 'full'
  },
  {
    path: 'bizly',
    component: BizlyEmpComponent
  },
  {
    path: 'pos-merchant',
    component: PosMerchantEmpComponent
  },
  {
    path: 'bizly/create-role',
    component: AddRoleComponent
  },
  {
    path: 'bizly/view-role',
    component: ViewRoleComponent
  },
  {
    path: 'bizly/create-user',
    component: AddUserComponent
  },
  {
    path: 'bizly/view-user',
    component: ViewUserComponent
  },
  {
    path:  'bizly/view-salary',
    component: ViewSalaryComponent
  },
  {
    path: 'bizly/create-salary-account',
    component: AddSalaryComponent
  },
  {
    path: 'bizly/loan-history',
    component: LoanHistoryComponent
  },
  {
    path: 'bizly/salary-history',
    component: SalaryHistoryComponent
  },
  {
    path: 'bizly/pay-loan',
    component: PayLoanComponent
  },
  {
    path: 'bizly/pay-salary',
    component: PaySalComponent
  }, 
  {
    path:'bizly/pay-allowance',
    component: PayAllowanceComponent
  },
  {
    path:'bizly/pay-deduct',
    component: PayDeductComponent
  },
  {
    path:'bizly/pay-incentive',
    component: PayIncentiveComponent
  }, 
  {
    path: 'pos-merchant',
    component: PosMerchantEmpComponent
  },  
  {
    path: 'pos-merchant/view-role',
    component: ViewRolposComponent
  },
  {
    path: 'pos-merchant/view-user',
    component: ViewUserposComponent
  },
  {
   path: 'bizly/view-loan-history',
    component:ViewLoanHistoryComponent
  },
  {
    path:'bizly/loan-history/receive-emi',
    component:ReceiveEmiComponent
  }
]



@NgModule({
  declarations: [
    EmployeeHomeComponent, 
    BizlyEmpComponent, 
    PosMerchantEmpComponent,
     AddRoleComponent,
      ViewRoleComponent,
      AddUserComponent,
      ViewUserComponent,
      AddSalaryComponent,
      ViewRolposComponent,
      ViewUserposComponent,
      PopAddposRoleComponent,
      PopAddSalComponent,
      PaySalComponent,
      PopPaySalComponent,
      PayLoanComponent,
      PayAllowanceComponent,
      PayDeductComponent,
      PayIncentiveComponent,
      LoanHistoryComponent,
      SalaryHistoryComponent,
      ViewLoanHistoryComponent,
      ReceiveEmiComponent,
      ViewSalaryComponent
    ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgbModule.forRoot(),
    // MaterialModule,
    // PrimengModule,
    InputTextModule,
    ButtonModule,
    TableModule,
    DialogModule,
    MultiSelectModule,
    ReactiveFormsModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    RatingModule,
    MatSlideToggleModule,
    MatMenuModule,
    CalendarModule,
    TabViewModule,
    MatCardModule,
    CardModule
  ],

  entryComponents: [PopAddposRoleComponent, PopAddSalComponent, PopPaySalComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class EmployeeModule { }
