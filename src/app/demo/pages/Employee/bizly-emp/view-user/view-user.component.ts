import { Component, OnInit } from '@angular/core';
import { EmpService } from 'src/app/_services/emp.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { resolve } from 'url';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {

  viewData: any = []
  userForm: FormGroup
  extNode: any
  beforeForm: FormGroup
  beforeData: any
  afterData: any
  showHistory: any[] = [];
  EditHistory: any = [];
  constructor(private empServ: EmpService, private router: Router, private fb: FormBuilder) {


    // formGroup
    // Role create form
    // Role create form
    this.userForm = this.fb.group({
      role: [''],
      access_points: [[]],
      firstName: ['', Validators.required],
      middleName: [''],
      lastName: [''],
      contact: [''],
      emailId: [''],
      photo: [''],
      address: [''],
      comm_address: [''],
      gender: [''],
      dob: [''],
      blood_grp: [''],
      marital_staus: [''],
      father_husband_name: [''],
      mother_name: [''],
      emergency: [''],
      work_schedule: [''],
      emp_responsibility: [''],
      no_disclosure: [''],
      sanction_leave: [''],
      account_no: [''],
      account_type: [''],
      bank_name: [''],
      branch_name: [''],
      bank_city: [''],
      bank_address: [''],
      bank_ifsc_code: [''],
      salary: [''],
      aadhar: [''],
      driving_lic: [''],
      passport: [''],
      legal_docName: [''],
      legal_doc: [''],
      doj: [''],
      assigned_city: [''],
      assigned_loc: [''],
      join_letter: [''],
      asset_provided: false,
      asset_name: [''],
      asset_desc: [''],
      asset_quantity: [''],
      gen_loginId: [''],
      gen_password: [''],
      status: true,
      created_by: ['']
    });

    this.beforeForm = this.fb.group({
      role: [''],
      access_points: [[]],
      firstName: ['', Validators.required],
      middleName: [''],
      lastName: [''],
      contact: [''],
      emailId: [''],
      photo: [''],
      address: [''],
      comm_address: [''],
      gender: [''],
      dob: [''],
      blood_grp: [''],
      marital_staus: [''],
      father_husband_name: [''],
      mother_name: [''],
      emergency: [''],
      work_schedule: [''],
      emp_responsibility: [''],
      no_disclosure: [''],
      sanction_leave: [''],
      account_no: [''],
      account_type: [''],
      bank_name: [''],
      branch_name: [''],
      bank_city: [''],
      bank_address: [''],
      bank_ifsc_code: [''],
      salary: [''],
      aadhar: [''],
      driving_lic: [''],
      passport: [''],
      legal_docName: [''],
      legal_doc: [''],
      doj: [''],
      assigned_city: [''],
      assigned_loc: [''],
      join_letter: [''],
      asset_provided: false,
      asset_name: [''],
      asset_desc: [''],
      asset_quantity: [''],
      gen_loginId: [''],
      gen_password: [''],
      status: true,
      created_by: ['']
    });


    this.getAllUsers();


  }

  ngOnInit() {


  }


  // getAllUsers
  getAllUsers() {


    // getAllUser
    this.empServ.getAllUsers().subscribe(data11 => {
      // debugger;
      this.viewData = data11
      console.log(this.viewData)

    })


    // userForm
    this.userForm.patchValue({
      "role": "CEO/Super Admin",
      "access_points": [
        {
          "access": "Restaurant",
          "status": true
        },
        {
          "access": "Menu",
          "status": true
        },
        {
          "access": "POS orders",
          "status": true
        },
        {
          "access": "Charges",
          "status": false
        },
        {
          "access": "User & Roles",
          "status": true
        },
        {
          "access": "Integration",
          "status": false
        },
        {
          "access": "Circular",
          "status": false
        },
        {
          "access": "Feedback",
          "status": true
        }
      ],
      "firstName": "Vijay",
      "middleName": "Dinanath",
      "lastName": "Chauhan",
      "contact": "9632587410",
      "emailId": "vijay12@gmail.com",
      "photo": "NA",
      "address": "Warje",
      "comm_address": "Karve Nagar",
      "gender": "Male",
      "dob": "12/08/1981",
      "blood_grp": "O+",
      "marital_staus": "Married",
      "father_husband_name": "Dinanath Bhima Chauhan",
      "mother_name": "Sulaxmi Chauhan",
      "emergency": "9996333222",
      "work_schedule": "9 am to 7 pm",
      "emp_responsibility": "Saving",
      "no_disclosure": "NA",
      "sanction_leave": "26",
      "account_no": "115965484534516",
      "account_type": "",
      "bank_name": "HDFC",
      "branch_name": "",
      "bank_city": "Pune",
      "bank_address": "KarveNagar HDFC Pune",
      "bank_ifsc_code": "HDFC0654898",
      "salary": "1,50,000",
      "aadhar": "97445DD5E6654",
      "driving_lic": "7IEOCKKEL",
      "passport": "TIE5F454344",
      "legal_docName": "Voter Id",
      "legal_doc": "NA",
      "doj": "13/07/2001",
      "assigned_city": "Pune",
      "assigned_loc": "Dhanukar, Colony",
      "join_letter": "NA",
      "asset_provided": false,
      "asset_name": "NA",
      "asset_desc": "NA",
      "asset_quantity": "NA",
      "gen_loginId": "vijay.cha01",
      "gen_password": "78PPUNE",
      "status": true,
      "salaries": "NA",
      "resign_data": "NA",
      "documents": "",
      "asset_return_data": "",
      "created_by": "Prasad"

    })
    // old data
    this.beforeForm.patchValue({
      "role": "CEO/Super Admin",
      "access_points": [
        {
          "access": "Restaurant",
          "status": true
        },
        {
          "access": "Menu",
          "status": true
        },
        {
          "access": "POS orders",
          "status": true
        },
        {
          "access": "Charges",
          "status": false
        },
        {
          "access": "User & Roles",
          "status": true
        },
        {
          "access": "Integration",
          "status": false
        },
        {
          "access": "Circular",
          "status": false
        },
        {
          "access": "Feedback",
          "status": true
        }
      ],
      "firstName": "Vijay",
      "middleName": "Dinanath",
      "lastName": "Chauhan",
      "contact": "9632587410",
      "emailId": "vijay12@gmail.com",
      "photo": "NA",
      "address": "Warje",
      "comm_address": "Karve Nagar",
      "gender": "Male",
      "dob": "12/08/1981",
      "blood_grp": "O+",
      "marital_staus": "Married",
      "father_husband_name": "Dinanath Bhima Chauhan",
      "mother_name": "Sulaxmi Chauhan",
      "emergency": "9996333222",
      "work_schedule": "9 am to 7 pm",
      "emp_responsibility": "Saving",
      "no_disclosure": "NA",
      "sanction_leave": "26",
      "account_no": "115965484534516",
      "account_type": "",
      "bank_name": "HDFC",
      "branch_name": "",
      "bank_city": "Pune",
      "bank_address": "KarveNagar HDFC Pune",
      "bank_ifsc_code": "HDFC0654898",
      "salary": "1,50,000",
      "aadhar": "97445DD5E6654",
      "driving_lic": "7IEOCKKEL",
      "passport": "TIE5F454344",
      "legal_docName": "Voter Id",
      "legal_doc": "NA",
      "doj": "13/07/2001",
      "assigned_city": "Pune",
      "assigned_loc": "Dhanukar, Colony",
      "join_letter": "NA",
      "asset_provided": false,
      "asset_name": "NA",
      "asset_desc": "NA",
      "asset_quantity": "NA",
      "gen_loginId": "vijay.cha01",
      "gen_password": "78PPUNE",
      "status": true,
      "salaries": "NA",
      "resign_data": "NA",
      "documents": "",
      "asset_return_data": "",
      "created_by": "Prasad"

    })

    console.log("userform", this.userForm.value)
    console.log("beforFrom", this.beforeForm.value)
    this.extNode = { data: this.userForm.controls.access_points.value }

  }

  // 

  updateUser() {
    // console.log('after update user', this.userForm.value)
    console.log("userform", this.userForm.value)
    console.log("beforFrom", this.beforeForm.value)
    this.compareChanges();
  
    console.log(this.beforeForm.value)

    this.EditHistory.push(this.showHistory)
    console.log('edit history', this.EditHistory)
    this.showHistory = []
   Object.keys(this.userForm.controls).forEach(key =>{
     this.beforeForm.controls[key].setValue(this.userForm.controls[key].value)
   })

   console.log('end', this.beforeForm.value)
  }

  // Compare changes
  compareChanges() {
    Object.keys(this.userForm.controls).forEach(key => {
      if (this.userForm.controls[key].value !== this.beforeForm.controls[key].value) {
        if (key !== "access_points") {
          this.showHistory.push(
            {
              "key": key,
              "before": this.beforeForm.controls[key].value,
              "after": this.userForm.controls[key].value,
              
            }
          )
        }
      }
    })
    this.beforeData = this.userForm.value
    console.log("show History", this.showHistory);

  }



}
