import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopAddSalComponent } from './pop-add-sal.component';

describe('PopAddSalComponent', () => {
  let component: PopAddSalComponent;
  let fixture: ComponentFixture<PopAddSalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopAddSalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopAddSalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
