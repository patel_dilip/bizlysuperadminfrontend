import { Component, OnInit } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmpService } from '../../../../../_services/emp.service';

@Component({
  selector: 'app-add-role',
  templateUrl: './add-role.component.html',
  styleUrls: ['./add-role.component.scss']
})
export class AddRoleComponent implements OnInit {

  // RoleForm

  roleForm: FormGroup

  // userid

  userid: any

  // serial code
  rolecode: any

  // Tree varialbe DEFAULT 1 DATA
  files: TreeNode[] = [
    {
      'label': 'CEO/Super Admin',
      'data': 'root',
      'key' : 'ROLE001',
      "expanded": true,
      "expandedIcon": "pi pi-folder-open",
      "collapsedIcon": "pi pi-folder",
      "leaf": false,
      'children': [      
      ]
    }

  ]

  // Radio button
  selectRdo: string = 'child';

  // Form Hidden
  formHide: boolean = true
  childChk: boolean = true

  // multi dropdown select
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  selectedColumns: any = []
  allRolesData = []
  extNode: any = [];

  constructor(private fb: FormBuilder, private empServ: EmpService) {

    // Locoal form data
    let localdata = {
      "roleid": "ROLE001",
      "roleName": "CEO/Super Admin",
      "access_points": [
        {
          "item_id": 2,
          "item_text": "Orders"
        },
        {
          "item_id": 3,
          "item_text": "Menu"
        }, {
          "item_id": 4,
          "item_text": "Inventory"
        }],
      "role_desc": "Restaurant's Manager",
      "status": true,
      "created_by": "5e2efc4cae2db81860184db0"
    }
    // GET ALL ROLES
    this.allRolesData.push(localdata)
    console.log('All Roles Data', this.allRolesData)

  

    // Role create form
    this.roleForm = this.fb.group({
      roleid: [''],
      roleName: ['',Validators.required],
      access_points: [[]],
      role_desc: [''],
      status: true,
      created_by: ['']
    });

     // this.roleForm.disable()
    // userid 

    let user = JSON.parse(localStorage.getItem('loginUser'))
    this.userid = user._id
    console.log('user', user)
    


// Generate Serial Code
this.generateCode()

  }

  ngOnInit() {
    this.dropdownList = [
      { item_id: 1, item_text: 'Restaurant' },
      { item_id: 2, item_text: 'Orders' },
      { item_id: 3, item_text: 'Menu' },
      { item_id: 4, item_text: 'Inventory' },
      { item_id: 5, item_text: 'Beverages' },
      { item_id: 6, item_text: 'Integration' },
      { item_id: 7, item_text: 'Pos Orders' },
      { item_id: 8, item_text: 'Feedback' },
      { item_id: 9, item_text: 'Notice' },
      { item_id: 10, item_text: 'Employees' },
      { item_id: 11, item_text: 'Charges' }
    ];

    this.dropdownSettings = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }


  // Radio click button function
  rdoclick(value) {
    this.selectRdo = value
    console.log('selected value', this.selectRdo)
    
    if(value=='child'){
      this.formHide = true
    }
    else{
      this.formHide = false
    }
  }


  // multiselet on select

  // multiselct on deselect

  onItemSelect(item: any) {
    console.log(item);
    this.selectedColumns.push(item)

    console.log('seleted items', this.roleForm.controls.access_points.value)

  }
  onSelectAll(items: any) {
    console.log(items);
    this.selectedColumns = items

    console.log('seleted items', this.selectedItems)
  }

  onDeSelect(item: any) {
    console.log(item);
    this.selectedColumns.splice(this.selectedColumns.indexOf(item), 1)
    console.log('seleted items', this.selectedItems)
  }
  onDeSelectAll(items: any) {
    console.log(items);
    this.selectedColumns = items

    console.log('seleted items', this.selectedItems)
  }

  // GENERATE SERIAL CODE FUNCTION
  generateCode(){

    
        // code generating serially
    
        if(this.allRolesData.length == 0){
          let len = this.allRolesData.length + 1   //length if array is empty 0+1
          let str = len.toString()            // converting 1 to string '1'
          let size = 3                        // size of code
          this.rolecode = 'ROLE' + str.padStart(size, '0')  // padding with 00 to left ex: 001
          console.log('ROLE code', this.rolecode);
          
        }
        else{
    
          console.log("all org lenth",this.allRolesData.length);
          let index = this.allRolesData.length - 1
     
          let last_code = this.allRolesData[index].roleid
          console.log('last_code', last_code);
          let str = last_code.slice(4)
          console.log('string', str);
          let digit = parseInt(str)
          digit = digit + 1
          let str1 = digit.toString()
          let size = 3
          this.rolecode = 'ROLE' + str1.padStart(size, '0')
          console.log('role code', this.rolecode);
    
    
        }
    
        this.roleForm.patchValue({
          roleid: this.rolecode
        })


    
  }

    // TREE NODE SELECTION
    onTreeNodeSelection(event){
      console.log('selected node', event.node)
  
      this.formHide = false
      this.extNode = event.node
    }
  

  // SAVE ROLE
  saveRole() {

 
    // /PATHING USER ID
    this.roleForm.patchValue({
      created_by: this.userid
    })
    console.log(this.roleForm.value)


    // IF WE WANT TO ADD ROOT
    if(this.selectRdo == 'root'){
    
      this.files.push({
        "label": this.roleForm.controls.roleName.value,
        "data": 'root',
        "key": this.roleForm.controls.roleid.value,
        "expanded": true,
        "expandedIcon": "pi pi-folder-open",
        "collapsedIcon": "pi pi-folder",
        "leaf": false,
        'children': []
      })
    }

    // IF WE WANT TO ADD CHILD
    if(this.selectRdo == 'child'){

      this.extNode.children.push({
        "label": this.roleForm.controls.roleName.value,
        "data": 'child',
        "key": this.roleForm.controls.roleid.value,
        "expanded": true,
        "expandedIcon": "pi pi-folder-open",
        "collapsedIcon": "pi pi-folder",
        "leaf": false,
        'children': []
      })

    }
   

    // ALL ROLE pushing data
    this.allRolesData.push(this.roleForm.value)
    let finalObj = {      
      "access_points": this.roleForm.controls.access_points.value,
      "status": true, 
      "roleid": this.roleForm.controls.roleid.value,
      "roleName": this.roleForm.controls.roleName.value,
    }
    // Reset page
    this.roleForm.reset()

    // add child form hide
    this.formHide= true

    this.childChk = true

    // Generating code
    this.generateCode()
    console.log("yess saved all")

    //POST REQUREST

 
    
    this.empServ.addRole(finalObj).subscribe(res=>{
      console.log("after resppnse", res)
      console.log("hellllllllllllo")
    })
  }





}
