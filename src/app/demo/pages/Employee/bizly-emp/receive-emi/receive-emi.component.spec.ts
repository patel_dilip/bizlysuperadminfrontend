import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiveEmiComponent } from './receive-emi.component';

describe('ReceiveEmiComponent', () => {
  let component: ReceiveEmiComponent;
  let fixture: ComponentFixture<ReceiveEmiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiveEmiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiveEmiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
