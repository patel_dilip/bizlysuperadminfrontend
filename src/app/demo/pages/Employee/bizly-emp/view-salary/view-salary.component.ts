import { Component, OnInit } from '@angular/core';
import { EmpService } from 'src/app/_services/emp.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-salary',
  templateUrl: './view-salary.component.html',
  styleUrls: ['./view-salary.component.scss']
})
export class ViewSalaryComponent implements OnInit {

  empName: any

  // salary form
  salaryForm: FormGroup

  // salary row data
  salrows = []

  rows1 =[
    {'amt': '100000', 'emi':'5000','date':'17-08-2019','irregular':'2'}
    ]
  
    rows2=[
      {'emi':'1','amt_paid':'5000','due':'365000','emi_date':'22-04-20','paid_date':'30-04-20'},
      {'emi':'2','amt_paid':'5000','due':'325000','emi_date':'17-05-20','paid_date':'28-05-20'},
      {'emi':'3','amt_paid':'5000','due':'305000','emi_date':'20-06-20','paid_date':'25-06-20'}
    ]
  
    NoLoans = ['','']
  

  constructor(private empServ: EmpService, private fb: FormBuilder, private router: Router) {
    this.empName = this.empServ.empSalName
  }

  ngOnInit() {

    this.salaryForm = this.fb.group({
      total_sal: [''],
      sal_pending: [''],
      current_month: [''],
      join_date: [''],
      sal_date: [''],
      amt_pending: [''],
      pay_cycle: ['']
    })


    // salary row data
    this.salrows = [
      {
        'month': 'December',
        'paidon': '05-01-20',
        'salary': '50000',
        'petrol': '500',
        'rent': '2500',
        'pf': '500',
        'health': '500',
        'policy': '1200',
        'overtime': '20 HRS',
        'overtime_amt': '4000',
        'incentive': '4000',
        'loanemi': '6000',
        'pending_amt': '0',
        'paid_amt': '51000'
      },
      {
        'month': 'November',
        'paidon': '05-01-20',
        'salary': '50000',
        'petrol': '500',
        'rent': '2500',
        'pf': '500',
        'health': '500',
        'policy': '1200',
        'overtime': '20 HRS',
        'overtime_amt': '4000',
        'incentive': '4000',
        'loanemi': '6000',
        'pending_amt': '0',
        'paid_amt': '51000'
      }
    ]



  

  }

  // view Loan History
  onViewLoan(name){
    this.empServ.emploanhistory = name
    this.router.navigate(['employee/bizly/view-loan-history'])
  }

  // Receive emi
  onReceiveEmi(){
    this.router.navigate(['employee/bizly/loan-history/receive-emi'])
  }



}
