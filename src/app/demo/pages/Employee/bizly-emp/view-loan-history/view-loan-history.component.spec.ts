import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLoanHistoryComponent } from './view-loan-history.component';

describe('ViewLoanHistoryComponent', () => {
  let component: ViewLoanHistoryComponent;
  let fixture: ComponentFixture<ViewLoanHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLoanHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLoanHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
