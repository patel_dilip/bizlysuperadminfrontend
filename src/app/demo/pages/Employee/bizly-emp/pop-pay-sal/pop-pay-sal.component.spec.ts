import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopPaySalComponent } from './pop-pay-sal.component';

describe('PopPaySalComponent', () => {
  let component: PopPaySalComponent;
  let fixture: ComponentFixture<PopPaySalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopPaySalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopPaySalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
