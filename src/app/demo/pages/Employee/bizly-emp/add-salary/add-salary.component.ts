import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { EmpService } from 'src/app/_services/emp.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-add-salary',
  templateUrl: './add-salary.component.html',
  styleUrls: ['./add-salary.component.scss']
})
export class AddSalaryComponent implements OnInit {

  salStruForm: FormGroup

  // Drownp down
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};

  // Allowance
  allowArr = [{
    allowName: 'Petrol',
    allowAmt: 2000
   }]
  allowName;
  allowAmt;




  // DeductAllow
  deductArr= [
    {
      deductName: 'Petrol',
      deductAmt: 1000
    }
  ]
  deductName;
  deductAmt;


  // set over time
  perHrEdit = false
  perHrAmt = 400


  // FIXED OVERS
  fixedHrArr = [
    {
      fixedHr: '1 Hour',
      fixedRate: 200
    }
  ]

  fixedHr;
  fixedRate


  // Total variable

  total: number = 0

  constructor(
    private fb: FormBuilder, private router: Router, private empServ: EmpService
  ) { 


  }

  ngOnInit() {

    this.salStruForm = this.fb.group({
      salary_amt :[],
      allowance_name: [''],
      allowance_amt: [],
      deduction_name:[''],
      deduction_amt:[],
      overtime_amt:[],
      fixed_hours: [''],
      fixed_amt: [],
      incentives: []
    })

    console.log(this.salStruForm.value)

    this.dropdownList = [
      { id: 1, employee: 'Sanjay' },
      { id: 2, employee: 'Ashish' },
      { id: 3, employee: 'Kishore' },
      { id: 4, employee: 'Sanket' },
      { id: 5, employee: 'Prasad' },
      { id: 6, employee: 'Mike' },
      { id: 7, employee: 'Shivraj' },
      { id: 8, employee: 'Daniel' },
      { id: 9, employee: 'Kevin' },
      { id: 10, employee: 'Mark' }
    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'employee',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 0,
      allowSearchFilter: true
    };
  }


  onItemSelect(item: any) {
    console.log(item);
    // this.selectedItems.push(item)
    console.log(this.selectedItems)
  }


  onSelectAll(items: any) {
    console.log(items);
    // this.selectedItems = []
    // this.selectedItems = items
    console.log(this.selectedItems)
  }


//  *****************************
// ADDING ALLWOANCE
addAllowance(){

  this.allowName = this.salStruForm.controls.allowance_name.value;
  this.allowAmt = this.salStruForm.controls.allowance_amt.value;

  this.allowArr.push({
    allowName: this.allowName,
    allowAmt: this.allowAmt
   } )

  //  reset variables
  this.salStruForm.controls.allowance_name.reset()
  this.salStruForm.controls.allowance_amt.reset()
  this.allowName =''
  this.allowAmt = null
}

// Adding Deduction

addDeduction(){

  this.deductName = this.salStruForm.controls.deduction_name.value;
  this.deductAmt = this.salStruForm.controls.deduction_amt.value;

  this.deductArr.push({
    deductName: this.deductName,
    deductAmt: this.deductAmt
   } )

  //  reset variables
  this.salStruForm.controls.deduction_name.reset()
  this.salStruForm.controls.deduction_amt.reset()
  this.deductName =''
  this.deductAmt = null
}

// remove allowance
removeAllow(value){
  this.allowArr.splice(this.allowArr.indexOf(value),1)
}

// remove deduction
removeDeduct(value){
this.deductArr.splice(this.deductArr.indexOf(value),1)
}


// onPerHour
onPerHour(){
 this.perHrEdit = !this.perHrEdit
}

// addPerHour
addPerHr(){
  this.perHrAmt = this.salStruForm.controls.overtime_amt.value
  this.perHrEdit = !this.perHrEdit
}

// add Fixed Hours
addFixedHrs(){

  this.fixedHr = this.salStruForm.controls.fixed_hours.value;
  this.fixedRate = this.salStruForm.controls.fixed_amt.value;

  this.fixedHrArr.push({
    fixedHr: this.fixedHr,
    fixedRate: this.fixedRate
   } )

  //  reset fixed hours variable
  this.salStruForm.controls.fixed_hours.reset()
  this.salStruForm.controls.fixed_amt.reset()
  this.fixedHr =''
  this.fixedRate = null

}


// remove fixed
removeFixedHr(value){
  this.fixedHrArr.splice(this.fixedHrArr.indexOf(value),1)
  }
  

// on Add Salary 
onAddSalary(){
  // salary
  let salary = parseInt(this.salStruForm.controls.salary_amt.value)
  // incentives
  let incentives = parseInt(this.salStruForm.controls.incentives.value)
  // allowances

  let allowance = 0
  this.allowArr.forEach(element=>{
    
    allowance = allowance + element.allowAmt
  })
  

  // deduction
  let deduct = 0
  this.deductArr.forEach(element=>{
    deduct = deduct - element.deductAmt
  })

  let perHrAmt = this. perHrAmt

  this.total = salary + incentives + allowance + deduct + perHrAmt

  console.log("total", this.total)

}

}
