import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayIncentiveComponent } from './pay-incentive.component';

describe('PayIncentiveComponent', () => {
  let component: PayIncentiveComponent;
  let fixture: ComponentFixture<PayIncentiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayIncentiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayIncentiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
