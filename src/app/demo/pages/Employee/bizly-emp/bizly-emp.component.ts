import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from 'src/app/_services/emp.service'
import { MatDialog } from '@angular/material';
import { PopAddSalComponent } from './pop-add-sal/pop-add-sal.component';
import { PopPaySalComponent } from './pop-pay-sal/pop-pay-sal.component';
import { RestoService } from 'src/app/_services/_restoregisterServices/resto.service';


interface roles {
  roleId;
  role;
  totalusers;
  roleaccess;
  status;
  createdby;
}

interface users {
  userId;
  empName;
  role;
  rating;
  loginId;
  emailId;
  contact;
  joinDate;
  status;
  createdby;
}

interface salary {
  serial_No;
  staffName;
  role;
  lastPaid;
  salaryAmt;
  emiAmt;
  allowance;
  deduction;
  pendingAmt;
  payableAmt;
  paidAmt;
  status;
  action;
}

@Component({
  selector: 'app-bizly-emp',
  templateUrl: './bizly-emp.component.html',
  styleUrls: ['./bizly-emp.component.scss']
})
export class BizlyEmpComponent implements OnInit {

  // ngPrime Table

  //Roles
  cols: any = [];
  rowData: any = [];
  RoleTable: any = [];
  _selectedColumns: any[];
  selected1: any;

  //Users
  colsusr = []
  rowUser = []
  UserTable = []
  _selUserCols = []
  selected2: any;

  // Salary
  colsalary = []
  rowSalary = []
  SalaryTable = []
  _selSalCols = []
  selected3: any;

  constructor(private router: Router, private restServ: RestoService, private empServ: EmpService, private matDialog: MatDialog) {

    // this.restServ.getAllChains().subscribe(data=>{
    //   console.log(data['data']);

    //   this.rowData = data['data']

    //   if(this.rowData!==undefined){
    //     this.tableData()
    //   }


    // })

    // get all roles
    this.empServ.getAllRole().subscribe(data => {
      console.log(data['data'])
      this.rowData = data['data']
      if (this.rowData !== undefined) {
        this.tableData()
      }
    })


    //GET ALL USERS
    this.empServ.getAllUsers().subscribe(data => {
      console.log(data['data']);
      this.rowUser = data['data']
      if (this.rowUser !== undefined) {
        this.userData()
      }
    })

    //GET ALL SALARY
    this.restServ.getAllChains().subscribe(data => {
      console.log(data['data']);
      this.rowSalary = data['data']
      if (this.rowSalary !== undefined) {
        this.salaryData()
      }
    })
  }

  ngOnInit() { }


  //  ROLE DATATABLE
  tableData() {

    let roleData = []

    this.rowData.forEach(element => {
      roleData.push(
        {
          id: element._id,
          code: element.roleid,
          name: element.roleName,
          users: element.assigned_users,
          access: element.access_points[0].accesspoint_id,
          status: element.status,
          create_by_on: element.created_by + ' ' + element.created_on
        }
      )
    });

    this.RoleTable = roleData

    this.cols = [
      { field: "code", header: "Role Code" },
      { field: "name", header: "Role" },
      { field: "users", header: "Assigned Users" },
      { field: "access", header: "Role Access Points " },
      { field: "status", header: "Status" },
      { field: "create_by_on", header: "Created By & On" }
    ]

    this._selectedColumns = this.cols
  }

  // USER DATATABLE
  userData() {
    let userData = []

    this.rowUser.forEach(element => {
      userData.push(
        {
          id: element._id,
          code: element.usercode,
          name: element.personal_details.firstName+" "+element.personal_details.lastName,
          role: element.personal_details.role.role_name,
          rating: 5,
          login: element.credentials.gen_loginId,
          email: element.credentials.gen_loginId,
          contact: element.contactDetails.address,
          joindate: element.accoutDetail.join_date,
          status: element.status,
          createdbyon: element.created_by+" "+element.createdAt

        }
      )
    });

    this.UserTable = userData

    this.colsusr = [
      { field: "code", header: "User Code" },
      { field: "name", header: "Employee Name" },
      { field: "role", header: "Role" },
      { field: "rating", header: "Rating" },
      { field: "login", header: "Login Id" },
      { field: "email", header: "Email Id" },
      { field: "contact", header: "Contact" },
      { field: "joindate", header: "Join Date" },
      { field: "status", header: "Status" },
      { field: "createdbyon", header: "Created By & On" }
    ]

    this._selUserCols = this.colsusr

  }

  // salaryData
  salaryData() {

    let salaryData = []

    this.rowSalary.forEach(element => {
      salaryData.push(
        {
          id: element._id,
          code: element.chain_info.chain_code,
          name: element.chain_info.display_name,
          legal: element.chain_info.legal_name
        }
      )
    });

    this.SalaryTable = salaryData

    this.colsalary = [
      { field: "code", header: "User Code" },
      { field: "name", header: "Employee Name" },
      { field: "legal", header: "Role" },
      { field: "code", header: "Rating" },
      { field: "name", header: "Login Id" },
      { field: "legal", header: "Email Id" },
      { field: "code", header: "Contact" },
      { field: "name", header: "Join Date" },
      { field: "legal", header: "Status" },
      { field: "code", header: "Created By & On" }
    ]

    this._selSalCols = this.colsalary
  }
  

  // ON CHECKBOX CLICK
  onChk() {
    console.log("selected1", this.selected1);
  } 

  // show hide columns
  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col) => val.includes(col));

    console.log("Set value of selected columns : ", this._selectedColumns);
  }


  // USERS

 // ON CHECKBOX CLICK
 onChk2() {
  console.log("selected2", this.selected2);
}

  // show hide columns
  @Input() get selectedColumns2(): any[] {
    return this._selUserCols;
  }

  set selectedColumns2(val: any[]) {
    //restore original order
    this._selUserCols = this.colsusr.filter((col) => val.includes(col));

    console.log("Set value of selected columns : ", this._selUserCols);
  }


  // SALARY
   // ON CHECKBOX CLICK
 onChk3() {
  console.log("selected3", this.selected3);
}

  // show hide columns
  @Input() get selectedColumns3(): any[] {
    return this._selSalCols;
  }

  set selectedColumns3(val: any[]) {
    //restore original order
    this._selSalCols = this.colsalary.filter((col) => val.includes(col));

    console.log("Set value of selected columns : ", this._selSalCols);
  }

}


