import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BizlyEmpComponent } from './bizly-emp.component';

describe('BizlyEmpComponent', () => {
  let component: BizlyEmpComponent;
  let fixture: ComponentFixture<BizlyEmpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BizlyEmpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BizlyEmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
