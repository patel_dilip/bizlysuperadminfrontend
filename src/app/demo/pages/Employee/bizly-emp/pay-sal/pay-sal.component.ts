import { Component, OnInit, ɵSWITCH_TEMPLATE_REF_FACTORY__POST_R3__ } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmpService } from 'src/app/_services/emp.service';
import Swal from 'sweetalert2';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-pay-sal',
  templateUrl: './pay-sal.component.html',
  styleUrls: ['./pay-sal.component.scss']
})
export class PaySalComponent implements OnInit {

  paymentForm: FormGroup;
  overtimeForm: FormGroup;
  payType = [
    'Salary',
    'Allowance',
    'Deduction',
    'Overtime',
    'Incentives',
    'Expenses'
  ]

  date = Date()

  // columns
  cols = []

  // rows
  rows =[
    {'salary':'40000','petrol':'2000','rent':'5000','pf':'120','health':'600','policy':'300','overtime':'2','overtime_amt':'600','incentive':'400','loanemi':'0'},
    ]

    allowances=[
      {'petrol': '','mobile':'500','rent':'2500'}
    ]

    deductions=[
      {'petrol': '','mobile':'500','rent':'2500'}
    ]

    overtime=[      
      '500'
    ]
    ovrow=[
      '500'
    ]

    ovcol= []

    seletedReceiptant
  constructor(private fb: FormBuilder, private router: Router,private empServ: EmpService) { 

    this.seletedReceiptant =  this.empServ.receiptant
    console.log(this.seletedReceiptant)
    if(this.seletedReceiptant == 'Mr. Sanket'){
      console.log('yessls')
      Swal.fire('Already Paid for this month')
      this.router.navigate(['employee/bizly'])
    }

    console.log('date', this.date)
  }


  ngOnInit() {
    this.paymentForm = this.fb.group({
      type: ['Salary',Validators.required],
      month: [ ,Validators.required],
      amount: ['', Validators.required]
    })

    // overitme form
    this.overtimeForm= this.fb.group({
      hours: ['', Validators.required],
      rate:['', Validators.required]
    })


    // columns
    this.cols = [
      { field: 'salary', header: 'Salary' },
      { field: 'allowances', header: 'Allowances' },
      { field: 'deductions', header: 'Deductions' },
      { field: 'overtime', header: 'Overtime' },
      {field: 'incentive', header: 'Incentive'},
      {field: 'loanemi', header: 'Loan Emi'}
    ]

    this.ovcol = [
      {field:'1 hour', header:'1 hour'}
    ]
  }


  // onFixed Add
  onFixedAdd(){
    let hours = this.overtimeForm.controls.hours.value+" hours"
    let rate = this.overtimeForm.controls.rate.value


    this.ovcol.push(
      {field: hours, header: hours}
    )

    this.ovrow.push(rate)

    this.overtimeForm.reset()

  }

  onPay(){
    this.router.navigate(['employee/bizly'])
  }
}