import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaySalComponent } from './pay-sal.component';

describe('PaySalComponent', () => {
  let component: PaySalComponent;
  let fixture: ComponentFixture<PaySalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaySalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaySalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
