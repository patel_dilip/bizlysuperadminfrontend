import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayAllowanceComponent } from './pay-allowance.component';

describe('PayAllowanceComponent', () => {
  let component: PayAllowanceComponent;
  let fixture: ComponentFixture<PayAllowanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayAllowanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayAllowanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
