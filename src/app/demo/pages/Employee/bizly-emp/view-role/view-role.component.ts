import { Component, OnInit } from '@angular/core';
import { EmpService } from 'src/app/_services/emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-role',
  templateUrl: './view-role.component.html',
  styleUrls: ['./view-role.component.scss']
})
export class ViewRoleComponent implements OnInit {

  viewRole: any 
  constructor(private empServ: EmpService, private router: Router) { 
this.viewRole = this.empServ.roleData

if(this.viewRole == undefined){
this.router.navigate(['/employee/bizly'])
}

console.log('view data',this.viewRole)
  }

  ngOnInit() {
  }

}
