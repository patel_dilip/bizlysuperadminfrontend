import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayDeductComponent } from './pay-deduct.component';

describe('PayDeductComponent', () => {
  let component: PayDeductComponent;
  let fixture: ComponentFixture<PayDeductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayDeductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayDeductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
