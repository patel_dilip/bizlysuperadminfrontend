import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewUserposComponent } from './view-userpos.component';

describe('ViewUserposComponent', () => {
  let component: ViewUserposComponent;
  let fixture: ComponentFixture<ViewUserposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewUserposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewUserposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
