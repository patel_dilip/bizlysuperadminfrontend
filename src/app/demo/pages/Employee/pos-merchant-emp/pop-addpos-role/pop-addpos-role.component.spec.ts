import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopAddposRoleComponent } from './pop-addpos-role.component';

describe('PopAddposRoleComponent', () => {
  let component: PopAddposRoleComponent;
  let fixture: ComponentFixture<PopAddposRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopAddposRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopAddposRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
