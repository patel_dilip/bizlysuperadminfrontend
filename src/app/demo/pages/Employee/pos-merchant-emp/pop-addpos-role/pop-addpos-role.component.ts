import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-pop-addpos-role',
  templateUrl: './pop-addpos-role.component.html',
  styleUrls: ['./pop-addpos-role.component.scss']
})
export class PopAddposRoleComponent implements OnInit {

  roleForm: FormGroup
   // multi dropdown select
   dropdownList = [];
   selectedItems = [];
   dropdownSettings = {};
   selectedColumns: any = []
  allRolesData: any = [];
  rolecode: string;

  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) public data) {

     // Locoal form data
    //  let localdata = {
    //   "roleid": "ROLE001",
    //   "roleName": "CEO/Super Admin",
    //   "access_points": [
    //     {
    //       "item_id": 2,
    //       "item_text": "Orders"
    //     },
    //     {
    //       "item_id": 3,
    //       "item_text": "Menu"
    //     }, {
    //       "item_id": 4,
    //       "item_text": "Inventory"
    //     }],
    //   "role_desc": "Restaurant's Manager",
    //   "status": true,
    //   "created_by": "5e2efc4cae2db81860184db0"
    // }

    let localdata = this.data
    // GET ALL ROLES
    this.allRolesData =localdata
    console.log('All Roles Data', this.allRolesData)

    
    // Role create form
    this.roleForm = this.fb.group({
      roleid: [''],
      roleName: ['',Validators.required],
      access_points: [[]],
      role_desc: [''],
      status: true,
      created_by: ['']
    });


    
// Generate Serial Code
this.generateCode()
   }

  ngOnInit() {

    this.dropdownList = [
      { item_id: 1, item_text: 'Restaurant' },
      { item_id: 2, item_text: 'Orders' },
      { item_id: 3, item_text: 'Menu' },
      { item_id: 4, item_text: 'Inventory' },
      { item_id: 5, item_text: 'Beverages' },
      { item_id: 6, item_text: 'Integration' },
      { item_id: 7, item_text: 'Pos Orders' },
      { item_id: 8, item_text: 'Feedback' },
      { item_id: 9, item_text: 'Notice' },
      { item_id: 10, item_text: 'Employees' },
      { item_id: 11, item_text: 'Charges' }
    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  
  }


  
  // multiselet on select

  // multiselct on deselect

  onItemSelect(item: any) {
    console.log(item);
    this.selectedColumns.push(item)

    console.log('seleted items', this.roleForm.controls.access_points.value)

  }
  onSelectAll(items: any) {
    console.log(items);
    this.selectedColumns = items

    console.log('seleted items', this.selectedItems)
  }

  onDeSelect(item: any) {
    console.log(item);
    this.selectedColumns.splice(this.selectedColumns.indexOf(item), 1)
    console.log('seleted items', this.selectedItems)
  }
  onDeSelectAll(items: any) {
    console.log(items);
    this.selectedColumns = items

    console.log('seleted items', this.selectedItems)
  }



  // GENERATE SERIAL CODE FUNCTION
  generateCode(){
    // code generating serially

    if(this.allRolesData.length == 0){
      let len = this.allRolesData.length + 1   //length if array is empty 0+1
      let str = len.toString()            // converting 1 to string '1'
      let size = 3                        // size of code
      this.rolecode = 'ROLE' + str.padStart(size, '0')  // padding with 00 to left ex: 001
      console.log('ROLE code', this.rolecode);
      
    }
    else{

      console.log("all org lenth",this.allRolesData.length);
      let index = this.allRolesData.length - 1

      let last_code = this.allRolesData[index].roleId
      console.log('last_code', last_code);
      let str = last_code.slice(4)
      console.log('string', str);
      let digit = parseInt(str)
      digit = digit + 1
      let str1 = digit.toString()
      let size = 3
      this.rolecode = 'ROLE' + str1.padStart(size, '0')
      console.log('role code', this.rolecode);


    }

    this.roleForm.patchValue({
      roleid: this.rolecode
    })

}


// RoleForm
saveRole(){
  console.log(this.roleForm.value)




}


}
