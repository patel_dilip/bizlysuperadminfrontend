import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { PopAddposRoleComponent } from './pop-addpos-role/pop-addpos-role.component';
import { Router } from '@angular/router';
import { EmpService } from 'src/app/_services/emp.service';



interface roles {
  roleId;
  role;
  totalusers;
  roleaccess;
  status;
  createdby;
}

@Component({
  selector: 'app-pos-merchant-emp',
  templateUrl: './pos-merchant-emp.component.html',
  styleUrls: ['./pos-merchant-emp.component.scss']
})
export class PosMerchantEmpComponent implements OnInit {

  

   // ******************
  // ROLES VARIABLES
  // *****************
  total_roles: any = 12
  total_active_roles: any =10


   // ******************
  // Tree DATATable code
  // *****************

  // for Role
  cols: any[] = [];
  rows: any[] = [];
  _selectedColumns: any[] = [];
  selected1: roles;


  constructor(private matDialog: MatDialog,private router: Router, private empServ: EmpService) {

    /****************************ROLE DATA TABLE************************************************* */

      // ***************************
    // TABLE DATA 
    // ***************************
    this.rows= [
      {
        roleId: 'ROLE001',
        role: 'Manager',
        // total_users: ['Mika Acosta','Naina','Bhargesh','Tanushree','Nitin','Sandeep','Shhyra','Rahul','Kabir','Aditya'],
        role_access_point: 70,
        status: 'Active',
        using_roles: 137
      },
      {
        roleId: 'ROLE002',
        role: 'Captain',
        // total_users: ['Sandeep','Shhyra','Rahul','Kabir','Aditya','Mika Acosta','Naina','Bhargesh','Tanushree','Nitin','Sandeep','Shhyra','Rahul','Kabir','Aditya'],
        status: 'De-Active',
        role_access_point: 29,
        using_roles: 220
      },
      {
        roleId: 'ROLE003',
        role: 'Chef',
        // total_users: ['Mika Acosta','Naina','Bhargesh','Tanushree','Nitin','Sandeep','Shhyra','Rahul','Kabir','Aditya'],
        status: 'Active',
        role_access_point: 13,
       using_roles: 125
      }
    ]


    // *********************
    // Columns Data Table
    // ********************
    this.cols=[
      {
        field: 'roleId', header: 'Role ID'
      },
      {
        field: 'role', header: 'Role'
      },
     
      {
        field: 'role_access_point', header: 'Role Access Points'
      },
     
      {
        field: 'using_roles', header: 'No. of Restaurants using role'
      },
    ]

       // Selected Columns
       this._selectedColumns = this.cols;

       console.log("Selected Columns : ",this._selectedColumns);
   
       // Rows data
       console.log("Rows data", this.rows)
   }

  ngOnInit() {
  }

   // Routing Creating Role
   createRole(){
    // this.router.navigate(['employee/bizly/create-role'])
    const dialogRef = this.matDialog.open(PopAddposRoleComponent,{
      width: 'auto',
      data: this.rows
    })

    dialogRef.afterClosed().subscribe(result=>{
      console.log('result', result)

      let active = ''
      if(result){

        if(result.status == true){
          active = 'Active'
        }
        else{
          active = 'De-Active'
        }

        let using_resto = result.access_points.length *3

        this.rows.push({
          role: result.roleName,
          roleId: result.roleid,
          role_access_point: result.access_points.length,
          status: active,
          using_roles : using_resto
        })
      }
      
  // access_points: (5) [{…}, {…}, {…}, {…}, {…}]
  // created_by: ""
  // roleName: "Managaer"
  // role_desc: "Manager"
  // roleid: "ROLE004"
  // status: true
  
  // role: "Manager"
  // roleId: "ROLE001"
  // role_access_point: 70
  // status: "Active"
  // using_roles: 137
    })
  }


  
    // **********************************************************************
    // ROLE
    // show hide columns
    @Input() get selectedColumns(): any[] {
      return this._selectedColumns;
    }
  
    set selectedColumns(val: any[]) {
      //restore original order
      this._selectedColumns = this.cols.filter((col) => val.includes(col));
  
      console.log("Set value of selected columns : ", this._selectedColumns);
    }
  // ************



  //onView pos roles
  onViewRole(value){

    this.empServ = value
    this.router.navigate(['/employee/pos-merchant/view-role'])
  }

}
