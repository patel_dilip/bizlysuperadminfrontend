import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosMerchantEmpComponent } from './pos-merchant-emp.component';

describe('PosMerchantEmpComponent', () => {
  let component: PosMerchantEmpComponent;
  let fixture: ComponentFixture<PosMerchantEmpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosMerchantEmpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosMerchantEmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
