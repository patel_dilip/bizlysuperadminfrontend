import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRolposComponent } from './view-rolpos.component';

describe('ViewRolposComponent', () => {
  let component: ViewRolposComponent;
  let fixture: ComponentFixture<ViewRolposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRolposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRolposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
