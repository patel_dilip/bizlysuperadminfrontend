import { Component, OnInit } from '@angular/core';
import { EmpService } from 'src/app/_services/emp.service';
@Component({
  selector: 'app-view-rolpos',
  templateUrl: './view-rolpos.component.html',
  styleUrls: ['./view-rolpos.component.scss']
})
export class ViewRolposComponent implements OnInit {

  rows=[]

    // multi dropdown select
    dropdownList = [];
    selectedItems = [];
    dropdownSettings = {};
    selectedColumns: any = []
  isActive: boolean = true;
  
  constructor(private empServ: EmpService) { 
    this.rows= [
      {
        roleId: 'ROLE001',
        role: 'Manager',
        // total_users: ['Mika Acosta','Naina','Bhargesh','Tanushree','Nitin','Sandeep','Shhyra','Rahul','Kabir','Aditya'],
        role_access_point: 70,
        status: 'Active',
        using_roles: 137
      },
      {
        roleId: 'ROLE002',
        role: 'Captain',
        // total_users: ['Sandeep','Shhyra','Rahul','Kabir','Aditya','Mika Acosta','Naina','Bhargesh','Tanushree','Nitin','Sandeep','Shhyra','Rahul','Kabir','Aditya'],
        status: 'De-Active',
        role_access_point: 29,
        using_roles: 220
      },
      {
        roleId: 'ROLE003',
        role: 'Chef',
        // total_users: ['Mika Acosta','Naina','Bhargesh','Tanushree','Nitin','Sandeep','Shhyra','Rahul','Kabir','Aditya'],
        status: 'Active',
        role_access_point: 13,
       using_roles: 125
      }
    ]
  }

  ngOnInit() {

    this.dropdownList = [
      { item_id: 1, item_text: 'Restaurant' },
      { item_id: 2, item_text: 'Orders' },
      { item_id: 3, item_text: 'Menu' },
      { item_id: 4, item_text: 'Inventory' },
      { item_id: 5, item_text: 'Beverages' },
      { item_id: 6, item_text: 'Integration' },
      { item_id: 7, item_text: 'Pos Orders' },
      { item_id: 8, item_text: 'Feedback' },
      { item_id: 9, item_text: 'Notice' },
      { item_id: 10, item_text: 'Employees' },
      { item_id: 11, item_text: 'Charges' }
    ];

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.selectedColumns = this.dropdownList
  }

  
  // multiselet on select

  // multiselct on deselect

  onItemSelect(item: any) {
    console.log(item);
    this.selectedColumns.push(item)

    // console.log('seleted items', this.roleForm.controls.access_points.value)

  }
  onSelectAll(items: any) {
    console.log(items);
    this.selectedColumns = items

    console.log('seleted items', this.selectedItems)
  }

  onDeSelect(item: any) {
    console.log(item);
    this.selectedColumns.splice(this.selectedColumns.indexOf(item), 1)
    console.log('seleted items', this.selectedItems)
  }
  onDeSelectAll(items: any) {
    console.log(items);
    this.selectedColumns = items

    console.log('seleted items', this.selectedItems)
  }

  onToggle(){
    this.isActive = !this.isActive
  }
}
