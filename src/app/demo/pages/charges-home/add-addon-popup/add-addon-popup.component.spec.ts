import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAddonPopupComponent } from './add-addon-popup.component';

describe('AddAddonPopupComponent', () => {
  let component: AddAddonPopupComponent;
  let fixture: ComponentFixture<AddAddonPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAddonPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAddonPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
