/**changed code*/
export class Outlet {
    from: number;
    to: number;
    price: number;
    outletrangeStart:number
    outletrangeend:number

    constructor(parameter?: any) {
        if (parameter == undefined) {
            this.from = 1;
            this.to = null;
            this.price = null;
            this.outletrangeStart=null
    this.outletrangeend=null
    
        } else {
            this.from = parameter.from;
            this.to = parameter.to;
            this.price = parameter.price;
            this.outletrangeStart=parameter.outletrangeStart
            this.outletrangeend=parameter.outletrangeend
            // this.package=parameter.package
        }
    }
    resetValue() {
        return {
            from: 0,
            to: 0,
            price: 0,
            outletrangeStart:0,
            outletrangeend:0,
            // package:''
        }
    }
} 