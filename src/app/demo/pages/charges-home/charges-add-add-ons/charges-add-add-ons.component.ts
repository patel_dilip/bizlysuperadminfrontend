import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MainService } from 'src/app/_services/main.service';
import { BehaviorSubject } from 'rxjs';
import { MatSort, MatPaginator, MatDialogRef, MatDialog } from '@angular/material';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { RevenueCollectionType } from './RevenueCollectionType';
import { Outlet } from './Outlet';
import { ChargeService } from 'src/app/_services/charges/charge.service';
import Swal from 'sweetalert2';
import { Router, RoutesRecognized } from '@angular/router';
import { environment } from 'src/environments/environment';
import { FileUploader } from 'ng2-file-upload';
import { OutletPerOrder } from './OutletPerOrder';
import { LOADIPHLPAPI } from 'dns';
import { filter, pairwise } from 'rxjs/operators';
import { AddProductPopupComponent } from '../add-product-popup/add-product-popup.component';
import { PackageOutlet } from './PackageOutlet';
import { OutletPerPackage } from './OutletPerPackage';

const URL = environment.base_Url + 'chargesaddon/uploadphotos'
@Component({
  selector: 'app-charges-add-add-ons',
  templateUrl: './charges-add-add-ons.component.html',
  styleUrls: ['./charges-add-add-ons.component.scss']
})
export class ChargesAddAddOnsComponent implements OnInit {
  completeData: any = new Array();
  outlet: any = new Array();
  uploader: FileUploader
  uploader_video: FileUploader
  response_video: string;
  multiResponse_video = [];
  response: string;
  multiResponse = [];
  RevenueCollectionObj: RevenueCollectionType[] = new Array();
  outletForCountry: any = {};
  currentProductOutlet: Outlet;
  _addons: any = {};
  avail_countries: any
  // packages = [{"pack":"abc"}]
  selectedItems = [];
  dropdownList = [];
  selected: any;
  selected1: any;
  selected2: any;
  dropdownSettings = {};
  free_priced_list = ["Yes", "No"]
  loginUserID: any;
  revenue_Collection_mode_list = ["Per Year", "Per Month", "Per Order", "One Time", "Package"]
  IDropdownSettings: IDropdownSettings
  _arr: any;
  length: any;
  clicked: boolean;
  packageOutelet: PackageOutlet;
  constructor(public dialog: MatDialog, private route: Router, private mainService: MainService, private charge: ChargeService) {
    this.route.events.pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
      .subscribe((events: RoutesRecognized[]) => {

        //console.log('previous url', events[0].urlAfterRedirects);
        localStorage.setItem('lastUrl', events[0].urlAfterRedirects)
        //console.log('current url', events[1].urlAfterRedirects);

      });
    let loginUser = JSON.parse(localStorage.getItem('loginUser'))
    this.loginUserID = loginUser._id
    console.log(this.loginUserID)
    this.productlist();
    // this.dataSource = new MatTableDataSource(this.chain_DATA);
    //  this.dataSource1 = new MatTableDataSource(outlet_DATA);
    this.dropdownSettings = this.IDropdownSettings = {
      limitSelection: 1,
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      enableCheckAll: false,

    };

  }
  //addAddons:FormGroup
  country = []
  per_order_commission_type_list = ['Fixed Amount', 'Percentage']
  per_order_chain_list = ['Fixed', 'Percentage']
  used_for_Outlet_list = ["Per Outlet"]
  product_associated_list = []
  ngOnInit() {
    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'chargesadons',
      allowedFileType: ['image']

    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // console.log('FileUpload:uploaded:', item, status, response);
      this.response = JSON.parse(response)
      // console.log('response', this.response);
      this.multiResponse.push(this.response['chargesadonPhotoUrls'][0])
      // console.log('multi response', this.multiResponse);
      this._addons.images = this.multiResponse
    }
    // video uploading
    this.uploader_video = new FileUploader({
      url: URL,
      itemAlias: 'chargesadons',
      allowedFileType: ['video']

    });

    this.response_video = '';

    this.uploader_video.response.subscribe(res => this.response_video = res);

    this.uploader_video.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      // console.log('FileUpload:uploaded:', item, status, response);



      this.response_video = JSON.parse(response)
      // console.log('response', this.response);

      this.multiResponse_video.push(this.response_video['chargesadonPhotoUrls'][0])
      // console.log('multi response', this.multiResponse_video);
      this._addons.videos = this.multiResponse_video

    }



  }
  addons() {
    this._addons.userid = this.loginUserID
    this._addons.completeData = this.completeData;
    console.log('this._addons >>', this._addons)
    this._addons.productAssociated = null
    this.charge.addaddons(this._addons).subscribe(data => {
      console.log(data);

      if (data['success'] == true) {
        this.clicked = false
        Swal.fire('Addons Added Successfully', '', 'success')
        // if((localStorage.getItem('lastUrl'))=='/charges'){
        this.route.navigate(['/charges'])
        // }
        // else{
        //   this.route.navigate([localStorage.getItem('lastUrl')])
        // }
      }
      else if (data['success'] == false && data['msg'] == "AddonName already Exists") {
        Swal.fire('Addon Already Exist', '', 'warning')
        this._addons.AddonsName = ""
      }


    })
  }

  countrySelected: string = '';
  currentCuntryObject: RevenueCollectionType;
  onItemSelect(item: any) {
    item.status = true
    this._arr = this.avail_countries
    this.length = this._arr.length;
    this.countrySelected = item
    this.currentCuntryObject = new RevenueCollectionType(this.countrySelected, "", "", "", "", "", "", undefined, null, undefined, undefined);
    this.completeData.push(this.currentCuntryObject);
    console.log(this.completeData)
  }

  onItemDeSelect(item: any) {
    this._arr = this.avail_countries
    this.length = this._arr.length;
    let outlet = []
    var removeIndex = this.completeData.map(function (i) {
      console.log("remove", i['country']['item_text']);
      return i['country']['item_text'];
    }).indexOf(item.item_text);
    console.log(removeIndex, ">>>");
    this.completeData.splice(removeIndex, 1);
    console.log('this._arr :', this.completeData);
  }
  pushAddonsOutlet(row, index) {
    // console.log(row, index)
    this.completeData.forEach((row, i) => {
      if (index == i) {
        let obj = Object.assign({}, this.completeData[i].outletObj)
        if (this.completeData[i].outletObj.from != null && this.completeData[i].outletObj.to != null && this.completeData[i].outletObj.price != null) {
          this.completeData[i].tableData.push(obj);
          this.completeData[i].outletObj.from = this.completeData[i].outletObj.to + 1;
          if (this.completeData[i].outletObj.from >= 2000) {
            Swal.fire("Outlet Range limit '1 to 2000' Only", "", "info")
          }
          this.completeData[i].outletObj.to = null;
          this.completeData[i].outletObj.price = null;
        }
      }
    })
  }

  deleteRow(row, i, length) {
    console.log(row, length);
    if (row == 0 && length == 0) {
      this.completeData[i].tableData.splice(row, 1);
      //this.completeData[i].outletObj={}
      this.completeData[i].outletObj.from = 1
    } else
      if (row == 0 && length == 1) {
        this.completeData[i].tableData.splice(row, 1);
        this.completeData[i].outletObj.from = 1
      } else
        if (row == 0) {
          this.completeData[i].tableData.splice(row, 1);
          this.completeData[i].tableData[row]['from'] = 1

        } else
          if (row == length - 1) {
            let from = this.completeData[i].tableData[row]['from']
            this.completeData[i].tableData.splice(row, 1);
            this.completeData[i].outletObj.from = from
          }
          else {
            console.log("row", this.completeData[i].tableData[row]);
            let from = this.completeData[i].tableData[row]['from']
            this.completeData[i].tableData.splice(row, 1);
            this.completeData[i].tableData[row]['from'] = from
            console.log("row", this.completeData[i].tableData[row]);
          }
  }
  deleteRowRange(row, r, i, length) {
   
    if (row == 0 && length == 0) {
      this.completeData[i].tableData.splice(row, 1);
      this.completeData[i].perOrderTable[r].perOrders.splice(row, 1);
      this.completeData[i].perOrderTable[r].perOrderCurrentData.rangeFrom=1
      // this.completeData[i].outletObj.from = 1
    } else
      if (row == 0 && length == 1) {
        this.completeData[i].perOrderTable[r].perOrders.splice(row, 1);
        this.completeData[i].perOrderTable[r].perOrderCurrentData.rangeFrom=1
      
        // this.completeData[i].outletObj.from = 1
      } else
        if (row == 0) {
          this.completeData[i].perOrderTable[r].perOrders.splice(row, 1);

          this.completeData[i].perOrderTable[r].perOrders[row].rangeFrom

        } else
          if (row == length - 1) {
            let from = this.completeData[i].perOrderTable[r].perOrders[row].rangeFrom
          this.completeData[i].perOrderTable[r].perOrders.splice(row, 1);
          this.completeData[i].perOrderTable[r].perOrderCurrentData.rangeFrom= from
          }
          else {
            let from = this.completeData[i].perOrderTable[r].perOrders[row].rangeFrom
            this.completeData[i].perOrderTable[r].perOrders.splice(row, 1);
            this.completeData[i].perOrderTable[r].perOrders[row].rangeFrom = from
            // console.log("row", this.completeData[i].tableData[row]);
          }
  }
  deleteTable(r, i, outletFrom, length) {

    if (r != 0) {
      console.log(r, outletFrom, length);

      this.completeData[i].perOrderTable.splice(r, 1);
      if (r != length - 1)
        this.completeData[i].perOrderTable[r]['outletFrom'] = outletFrom
    }
  }
  productlist() {

    this.product_associated_list = []
    this.charge.getProduct().subscribe(data => {
      // console.log(data['data']);
      data['data'].forEach(element => {
        if (element.status == true) {
          let countrylist = []
          //console.log("ele>", element);
          for (let i = 0; i < element.completeData.length; i++) {
            if (element.completeData[i].country != null && element.completeData[i].country != {}) {
              countrylist.push(element.completeData[i].country['item_text'])
              element.country = countrylist
            }
          }
          this.product_associated_list.push({ product: element['productName'], country: element['country'] })

        }
      });
    })
  }



  selectedCollectionMode(collectionMode, country, data, i) {
    console.log(collectionMode, ">>>>", data, ">>>>", i);
    data.colletionMode = collectionMode.value
    data.tempcolletionMode = collectionMode.value
    data.perOrderPrice = ""
    data.perOrderPercentage = ""
    data.perOrderCommissionType = ""
    data.perOrderChain = ""
    data.outletObj = {
      from: 1,
      to: null,
      price: null,
      outletrangeStart: 1,
      outletrangeend: null
    }
    data.tableData = []

    data.perOrderTable = [{
      outletFrom: 1,
      outletTo: null,
      perOrderCurrentData: { rangeFrom: 1, rangeTo: null, pricePerOutlet: null, percentagePerOutlet: null },
      perOrders: []
    }]
    data.perPackageTable = [{
      packageValue: null,
      table: [{ from: 1, to: null, price: null, innerTable: [] }]

    }]
    console.log("len>>", data.perOrderTable.length);



    console.log(collectionMode.value, this.countrySelected, this.completeData)
    for (let i = 0; i < this.completeData.length; i++) {
      console.log('this.completeData[i].country == this.countrySelected >', this.completeData[i].country == this.countrySelected)
      if (this.completeData[i].country == this.countrySelected) {
        console.log('country >', this.completeData[i].country, '==', this.countrySelected)
        if (this.completeData[i].tempcolletionMode == "Per Month" || this.completeData[i].tempcolletionMode == "Per Year" || this.completeData[i].tempcolletionMode == "One Time") {
          console.log('mode if');
          this.completeData[i].setOutletObj(new Outlet());
          this.completeData[i].setOutletArray(new Array());
          this.completeData[i].setFreePriced(this.completeData[i].tempfreePriced);
          this.completeData[i].setsoldPerOutlet(this.completeData[i].tempsoldPerOutlet);
          this.completeData[i].setcolletionMode(this.completeData[i].tempcolletionMode);
        } else {
          // console.log('this.completeData[i] >', this.completeData[i], collectionMode)
          console.log('mode else', i, this.completeData[i]);
          this.completeData[i].setFreePriced(this.completeData[i].tempfreePriced);
          this.completeData[i].setsoldPerOutlet(this.completeData[i].tempsoldPerOutlet);
          this.completeData[i].setcolletionMode(this.completeData[i].tempcolletionMode);
          this.completeData[i].setperOrderCommissionType(this.completeData[i].perOrderCommissionType);
          this.completeData[i].setperOrderPrice(this.completeData[i].perOrderPrice)

          this.completeData[i].setperOrderTable(JSON.parse(JSON.stringify(new OutletPerOrder(undefined))));
          // if(this.completeData[i].country === country){
          //   this.completeData[i].perOrderTable.push(new OutletPerOrder(undefined))
          // }
        }
      }
      else {
        console.log('country else')
        continue;
      }
    }
    if (data.perOrderTable.length > 1) {
      console.log(data.perOrderTable, ">>>>>");

      this.completeData[i].perOrderTable.pop();

    }
  }

  replicateTable(countryIndex, tableIndex) {
    this.completeData.forEach((element, index) => {
      if (index == countryIndex) {
        if (this.completeData[index].perOrderTable[tableIndex]['outletFrom'] != "" && this.completeData[index].perOrderTable[tableIndex]['outletFrom'] != null && this.completeData[index].perOrderTable[tableIndex]['outletTo'] != "" && this.completeData[index].perOrderTable[tableIndex]['outletTo']) {
          console.log(this.completeData[index].perOrderTable[tableIndex]['outletFrom']);
          if (this.completeData[index].perOrderTable[tableIndex]['outletTo'] < 1999) {
            this.completeData[countryIndex].perOrderTable.push(JSON.parse(JSON.stringify(new OutletPerOrder(undefined))))
            this.completeData[index].perOrderTable[tableIndex + 1]['outletFrom'] = this.completeData[index].perOrderTable[tableIndex]['outletTo'] + 1
          } else {
            Swal.fire("Outlet range is '1 to 2000")
          }

        }

      }
    });
  }
  removeImage(imgurl) {
    console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
  }
  removeVideo(url) {
    console.log(url);
    this.multiResponse_video.splice(this.multiResponse_video.indexOf(url), 1)
  }
  processMyValue(i): void {
    this.completeData[i].perOrderPrice = parseInt(this.completeData[i].perOrderPrice).toLocaleString();
    //  this.completeData[i].reasonForNo = ;
    if (this.completeData[i].perOrderPrice == "NaN") {
      this.completeData[i].perOrderPrice = null
    }

  }
  processChainValue(i): void {
    this.completeData[i].perOrderChain = parseInt(this.completeData[i].perOrderChain).toLocaleString();
    //  this.completeData[i].reasonForNo = ;
    if (this.completeData[i].perOrderChain == "NaN") {
      this.completeData[i].perOrderChain = null
    }

  }
  processPrice(i, rowindex): void {
    let numberVal = parseInt(this.completeData[i].tableData[rowindex].price).toLocaleString();
    this.completeData[i].tableData[rowindex].price = numberVal;
    //  this.completeData[i].outletObj.price
    if (this.completeData[i].tableData[rowindex].price == "NaN") {
      this.completeData[i].tableData[rowindex].price = null;
    }
  }
  processPerorderPrice(i, rowIndex, pertablendex): void {
    let numberVal = parseInt(this.completeData[i].perOrderTable[pertablendex].perOrders[rowIndex].pricePerOutlet).toLocaleString();
    this.completeData[i].perOrderTable[pertablendex].perOrders[rowIndex].pricePerOutlet = numberVal;
    //  this.completeData[i].outletObj.price
    if (this.completeData[i].perOrderTable[pertablendex].perOrders[rowIndex].pricePerOutlet == "NaN") {
      this.completeData[i].perOrderTable[pertablendex].perOrders[rowIndex].pricePerOutlet = null;
    }
  }
  processOutletPrice(i): void {
    let numberVal = parseInt(this.completeData[i].outletObj.price).toLocaleString();
    this.completeData[i].outletObj.price = numberVal;
    if (this.completeData[i].outletObj.price == 'NaN') {
      this.completeData[i].outletObj.price = null
    }

  }
  processPerOrderPrice(i, pertablendex): void {
    let numberVal = parseInt(this.completeData[i].perOrderTable[pertablendex].perOrderCurrentData.pricePerOutlet).toLocaleString();
    this.completeData[i].perOrderTable[pertablendex].perOrderCurrentData.pricePerOutlet = numberVal;
    if (this.completeData[i].perOrderTable[pertablendex].perOrderCurrentData.pricePerOutlet == 'NaN') {
      this.completeData[i].perOrderTable[pertablendex].perOrderCurrentData.pricePerOutlet = null
    }
  }


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 44) {
      return true;
    }


    else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  addPerOrderRow(perOrderRow, countryIndex, perOrderRowIndex) {
    this.completeData.forEach((element, index) => {
      if (index == countryIndex) {
        this.completeData[index].perOrderTable.forEach((table, tableindex = index) => {
          if (tableindex == perOrderRowIndex) {
            let Obj = JSON.parse(JSON.stringify(this.completeData[index].perOrderTable[tableindex].perOrderCurrentData));
            if (this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['rangeFrom'] != ""
              && this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['rangeTo'] != ""
              && (this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['pricePerOutlet'] != "" || this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['percentagePerOutlet'] != "")
              && this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['rangeFrom'] != null
              && this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['rangeTo'] != null
              && (this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['pricePerOutlet'] != null || this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['percentagePerOutlet'] != null)) {
              this.completeData[index].perOrderTable[tableindex].perOrders.push(Obj);
              this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['rangeFrom'] = this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['rangeTo']+1
              this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['rangeTo'] = null
              this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['pricePerOutlet'] = null
              this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['percentagePerOutlet'] = null
              if (this.completeData[index].perOrderTable[tableindex]['perOrderCurrentData']['rangeFrom']>= 1000) {
                Swal.fire("Range limit '1 to 999' Only", "", "info")
              }

            }
          }

        });
      }
    });
  }
  checkPercentage(i, event) {
    console.log("hi", event.target.value);

    if (event.target.value <= 99 && event.target.value >= 1) {
      console.log(event.target.value);
      this.completeData[i].perOrderPercentage = event.target.value
    }
    else {
      Swal.fire("Please Enter Percentage", "", "warning")
      this.completeData[i].perOrderPercentage = null
    }
  }

  checkPercentageinTable(i, event, rowIndex, pertablendex) {
    console.log("hi", event.target.value);

    if (event.target.value <= 99 && event.target.value >= 1) {
      console.log(event.target.value);
      this.completeData[i].perOrderTable[pertablendex].perOrders[rowIndex].percentagePerOutlet = event.target.value
    }
    else {
      Swal.fire("Please Enter Percentage", "", "warning")
      this.completeData[i].perOrderTable[pertablendex].perOrders[rowIndex].percentagePerOutlet = null
    }
  }
  // this.completeData[i].perOrderTable[pertablendex].perOrderCurrentData.pricePerOutlet
  checkPercentageTable(i, event, pertablendex) {
    console.log("hi", event.target.value);

    if (event.target.value <= 99 && event.target.value >= 1) {
      console.log(event.target.value);
      this.completeData[i].perOrderTable[pertablendex].perOrderCurrentData.percentagePerOutlet = event.target.value
    }
    else {
      Swal.fire("Please Enter Percentage", "", "warning")
      this.completeData[i].perOrderTable[pertablendex].perOrderCurrentData.percentagePerOutlet = null
    }
  }
  /**======================check event key====================================== */
  e(event) {
    console.log(event);

    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 101 || charCode == 46 || charCode == 43 || charCode == 45) {
      return false;
    }
  }
  checkValue(event) {
    console.log("event", event);
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    else if (event.target.value > 2000) {
      Swal.fire("Please Enter value less than 2000", "", "warning")
      event.target.value = null
      return false
    } else {
      return true
    }

  }
  checkRange(value, tableIndex, tableData, event) {
    console.log(value, tableIndex, tableData, event);

    if (value !== null) {
      if (tableData.length > 0) {
        tableData.forEach(num => {
          console.log(value, '<>', num.from, '<>', num.to);
          // return (value >= num.from || value >= num.to) ? true : false;
          if ((value <= num.from) || (value <= num.to)) {
            console.log("hi5");

            Swal.fire("Please Enter Large Value", "", "warning")
            this.completeData[tableIndex].outletObj.to = null
            event.stopPropagation();
          } else {
            this.completeData.forEach((table: any, index) => {
              if (tableIndex === index) {
                console.log('this.completeData[index] >>', this.completeData[index]);
                if (this.completeData[index].outletObj.from >= value) {
                  console.log("hi");

                  Swal.fire("Please Enter Large value", "", "warning")
                  this.completeData[index].outletObj.to = null
                  event.stopPropagation();
                }
              }

            });
          }
        });
      } else {
        this.completeData.forEach((table: any, index) => {
          if (tableIndex === index) {
            console.log('this.completeData[index] >>', this.completeData[index]);
            if (this.completeData[index].outletObj.from >= value) {
              console.log();

              Swal.fire("Please Enter Large Value", "", "warning")
              event.stopPropagation();
            }
          }
        });
      }
    }
    else {
      console.log("hi2");

      Swal.fire("Please Enter Large Value", "", "warning")
    }
  }
  checkRangeInTable(rowindex, value, tableData, event, i, length) {
    console.log(rowindex, value, tableData, event, i, length);
    if (value !== null) {
      tableData.forEach(element => {
        console.log(value, '   range >', element.from, element.to)
        if (element.to <= element.from) {
          console.log(value, ">>", element.from);
          console.log("hi3");
          Swal.fire("Please Enter Large value", "", "warning")


          this.completeData[i].tableData[rowindex].to = null
          event.stopPropagation();
        }
      });
    }
    else {
      console.log("hi4");

      Swal.fire("Please Enter Large value", "", "warning")
    }

    if (rowindex + 1 == length && this.completeData[i].tableData[rowindex].to != null) {

      let val = value + 1
      this.completeData[i].outletObj.from = val

    }
    else {
      if (value + 1 >= this.completeData[i].tableData[rowindex + 1].to || value >= 2000) {
        // intk=length-1
        if (value >= 2000) {
          Swal.fire("Outlet Range limit '1 to 2000' Only", "", "info")
        }
        let k = length - (rowindex + 1)
        this.completeData[i].tableData.splice(rowindex + 1, k);
        if (this.completeData[i].tableData[rowindex].to != null) {
          this.completeData[i].outletObj['from'] = value + 1
        }
      }


      else {
        if (this.completeData[i].tableData[rowindex].to != null)
          this.completeData[i].tableData[rowindex + 1].from = value + 1
      }
    }

  }
  // checkRangeInOrderTable(rowindex, value, tableData, event, i, length) {
  //   console.log(rowindex, value, tableData, event, i, length);
  //   if (value !== null) {
  //     tableData.forEach(element => {
  //       console.log(value, '   range >', element.from, element.to)
  //       if (element.to <= element.from) {
  //         console.log(value, ">>", element.from);
  //         console.log("hi3");
  //         Swal.fire("Please Enter Large value", "", "warning")


  //         this.completeData[i].tableData[rowindex].to = null
  //         event.stopPropagation();
  //       }
  //     });
  //   }
  //   else {
  //     console.log("hi4");

  //     Swal.fire("Please Enter Large value", "", "warning")
  //   }

  //   if (rowindex + 1 == length && this.completeData[i].tableData[rowindex].to != null) {

  //     let val = value + 1
  //     this.completeData[i].outletObj.from = val

  //   }
  //   else {
  //     if (value + 1 >= this.completeData[i].tableData[rowindex + 1].to || value >= 2000) {
  //       // intk=length-1
  //       if (value >= 2000) {
  //         Swal.fire("Outlet Range limit '1 to 2000' Only", "", "info")
  //       }
  //       let k = length - (rowindex + 1)
  //       this.completeData[i].tableData.splice(rowindex + 1, k);
  //       if (this.completeData[i].tableData[rowindex].to != null) {
  //         this.completeData[i].outletObj['from'] = value + 1
  //       }
  //     }


  //     else {
  //       if (this.completeData[i].tableData[rowindex].to != null)
  //         this.completeData[i].tableData[rowindex + 1].from = value + 1
  //     }
  //   }

  // }

  checkRangeInOutlet(pertablendex, i, value, tableData, event, length) {

    console.log(pertablendex, "<<", i, "<<", value, "<<", tableData, "<<", event, "<<", length);


    if (value !== null) {
      if (tableData.length > 0) {
        tableData.forEach(num => {
          console.log(value, '<>', num.from, '<>', num.to);
          // return (value >= num.from || value >= num.to) ? true : false;
          if ((value <= num.from) || (value <= num.to)) {
            console.log("hi5");

            Swal.fire("Please Enter Large Value", "", "warning")
            this.completeData[i].perOrderTable[pertablendex]['outletTo'] != null
            event.stopPropagation();
          } else {
            this.completeData.forEach((table: any, index) => {
              if (pertablendex === index) {
                console.log('this.completeData[index] >>', this.completeData[index].perOrderTable[pertablendex]['outletFrom']);
                if (this.completeData[index].perOrderTable[pertablendex]['outletFrom'] >= value) {
                  console.log("hi");

                  Swal.fire("Please Enter Large value", "", "warning")
                  this.completeData[index].perOrderTable[pertablendex]['outletTo'] = null
                  event.stopPropagation();
                }
              }

            });
          }
        });
      } else {
        this.completeData.forEach((table: any, index) => {
          if (pertablendex === index) {
            console.log('this.completeData[index] >>', this.completeData[index].perOrderTable[pertablendex]['outletFrom']);
            if (this.completeData[index].perOrderTable[pertablendex]['outletFrom'] >= value) {
              console.log();

              Swal.fire("Please Enter Large Value", "", "warning")
              event.stopPropagation();
            }
          }
        });
      }
    }
    else {
      console.log("hi2");

      Swal.fire("Please Enter Large Value", "", "warning")
    }
    if (pertablendex != length - 1) {
      if (this.completeData[i].perOrderTable[pertablendex + 1]['outletFrom'] <= value) {
        let del = length - (pertablendex + 1)
        this.completeData[i].perOrderTable.splice(pertablendex + 1, del);
      }
    } else
      if (value > this.completeData[i].perOrderTable[pertablendex]['outletFrom']) {
        if (pertablendex != length - 1) {
          this.completeData[i].perOrderTable[pertablendex + 1]['outletFrom'] = this.completeData[i].perOrderTable[pertablendex]['outletTo'] + 1
        }
      }
      else {
        this.completeData[i].perOrderTable[pertablendex]['outletTo'] = null
        Swal.fire("Please Enter Large Value", "", "warning")
      }


  }
  freeOrPriced(event, data, i) {
    console.log(event, ">>>>", data, ">>>>", i);
    data.freePriced = event.value
    data.soldPerOutlet = "Per Outlet"
    data.colletionMode = ""
    data.tempfreePriced = event.value
    data.tempsoldPerOutlet = "Per Outlet"
    data.tempcolletionMode = ""
    data.perOrderPrice = ""

    data.perOrderPercentage = ""
    data.perOrderCommissionType = ""
    data.perOrderChain = ""
    data.outletObj = {
      from: 1,
      to: null,
      price: null,
      outletrangeStart: null,
      outletrangeend: null
    }
    data.tableData = []
    data.perOrderTable = [{
      outletFrom: 1,
      outletTo: null,
      perOrderCurrentData: { rangeFrom: 1, rangeTo: null, pricePerOutlet: null, percentagePerOutlet: null },
      perOrders: []
    }]
    data.perPackageTable = [{
      packageValue: null,
      table: [{ from: 1, to: null, price: null, innerTable: [] }]

    }]

  }
  soldperoutlet(event, data, i) {
    console.log(event, ">>>>", data, ">>>>", i);
    data.soldPerOutlet = event.value
    data.tempsoldPerOutlet = data.soldPerOutlet
    data.colletionMode = ""
    data.tempcolletionMode = ""
    data.perOrderPrice = ""
    data.perOrderPercentage = ""
    data.perOrderCommissionType = ""
    data.perOrderChain = ""
    data.outletObj = {
      from: 1,
      to: null,
      price: null,
      outletrangeStart: null,
      outletrangeend: null
    }
    data.tableData = []

    data.perOrderTable = [{
      outletFrom: 1,
      outletTo: null,
      perOrderCurrentData: { rangeFrom: 1, rangeTo: null, pricePerOutlet: null, percentagePerOutlet: null },
      perOrders: []
    }]
    data.perPackageTable = [{
      packageValue: null,
      table: [{ from: 1, to: null, price: null, innerTable: [] }]

    }]

  }
  //commission Type
  commissionType(event, data, i) {
    console.log(event, ">>>>", data, ">>>>", i);
    data.perOrderCommissionType = event.value
    data.perOrderPrice = ""

    data.perOrderPercentage = ""
    data.perOrderChain = event.value
    data.outletObj = {
      from: 1,
      to: null,
      price: null,
      outletrangeStart: null,
      outletrangeend: null
    }
    data.tableData = []
    data.perOrderTable = [{
      outletFrom: 1,
      outletTo: null,
      perOrderCurrentData: { rangeFrom: 1, rangeTo: null, pricePerOutlet: null, percentagePerOutlet: null },
      perOrders: []
    }]

  }
  /////////////////////////////////////////////////////////////////////===============check country==============//////////////////////////////////////////////////////////////////////////

  checkCountry(event) {
    console.log("prodeuct>>>", event.target.value);

    this.charge.getAddonCountry(event.target.value).subscribe(data => {
      console.log(data);
      if (data['success'] == true) {
        let countrylist = []

        console.log(">>", data['data'].length);
        for (let i = 0; i < data['data'].length; i++) {
          console.log(data['data'][i]['completeData']);
          data['data'][i]['completeData'].forEach(element => {
            countrylist.push(element.country['item_text'])
            element.country = countrylist
          });
          Swal.fire('Addon Available ' + countrylist, "", "warning")


        }
        console.log("countrylist>>>", countrylist);
        this.mainService.getAllDialCode().subscribe((country: any[]) => {
          let i = 0;
          let temp = [];
          country.forEach(element => {
            if (countrylist.includes(element['name']) === false) {
              i++;
              temp.push({ item_id: i, item_text: element['name'], symbol: element['currency_Symbol'] });;

              console.log("HI");

            }
          });
          this.dropdownList = temp;

        });
        this.avail_countries = []
        this.completeData = []

      }
      else {
        this.mainService.getAllDialCode().subscribe((country: any[]) => {
          let i = 0;
          let temp = [];
          country.forEach(element => {
            i++
            temp.push({ item_id: i, item_text: element['name'], symbol: element['currency_Symbol'] });;

          });
          this.dropdownList = temp;

        });
        this.avail_countries = []
        this.completeData = []
      }

    })
  }
  openDialogProduct(): void {
    const dialogRef = this.dialog.open(AddProductPopupComponent, {
      width: 'auto',
      height: 'auto'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.productlist();

    });
  }

  addRowToInnerTable(countryIndex, outerTableIndex, innerTableIndex, rowIndex, currentObject) {
    console.log("current:", currentObject);

    if (this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].from != null && this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].to != null && this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].price != null
      && this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].from != '' && this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].to != '' && this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].price != '') {
      this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].innerTable
        .push(
          Object.assign({}, new PackageOutlet({
            from: currentObject.from,
            to: currentObject.to,
            price: currentObject.price,
          }))
        );

      this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].from = this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].to + 1;
      if (this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].from >= 2000) {
        Swal.fire("Outlet Range limit '1 to 2000' Only", "", "info")
      }
      this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].price = null;
      this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].to = null;
    }
  }
  duplicatePackageTable(countryIndex, outerTableIndex) {
    console.log(countryIndex, outerTableIndex);

    if (this.completeData[countryIndex].perPackageTable[0].packageValue != null && this.completeData[countryIndex].perPackageTable[0].packageValue != '') {
      this.completeData[countryIndex].perPackageTable.push(
        Object.assign({}, new OutletPerPackage(undefined))
      );
      console.log(this.completeData);
    }

  }
  checkRangeInPackageTable(rowindex, innerindex, outerindex, value, tableData, event, i, length) {
    // console.log(outerindex, innerindex,rowindex, ">",value,">", tableData,">", event,">", i,"?", length);
    console.log(tableData);



    // this.completeData[i].perPackageTable[outerindex].table[0].innerTable.
    if (value !== null) {
      tableData.forEach(element => {
        console.log(value, '   range >', element.from, element.to)
        if (element.to <= element.from) {
          console.log(value, ">>", element.from);
          console.log("hi3");
          Swal.fire("Please Enter Large value", "", "warning")


          this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].to = null
          event.stopPropagation();
        }
      });
    }
    else {
      console.log("hi4");

      Swal.fire("Please Enter Large value", "", "warning")
    }

    if (rowindex + 1 == length && this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].to != null) {

      let val = value + 1
      this.completeData[i].perPackageTable[outerindex].table[0].from = val

    }
    else {
      if (value + 1 >= this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex + 1].to || value >= 2000) {
        // intk=length-1
        if (value >= 2000) {
          Swal.fire("Outlet Range limit '1 to 2000' Only", "", "info")
        }
        let k = length - (rowindex + 1)
        this.completeData[i].perPackageTable[outerindex].table[0].innerTable.splice(rowindex + 1, k);
        if (this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].to != null) {
          this.completeData[i].perPackageTable[outerindex].table[0]['from'] = value + 1
        }
      }


      else {
        if (this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].to != null)
          this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex + 1].from = value + 1
      }
    }

  }
  checkRangeInpackage(outerindex, value, tableIndex, tableData, event) {
    console.log(value, tableIndex, tableData, event);
    console.log(this.completeData[tableIndex]);

    if (value !== null) {
      if (tableData.length > 0) {
        tableData.forEach(num => {
          console.log(value, '<>', num.from, '<>', num.to);
          // return (value >= num.from || value >= num.to) ? true : false;
          if ((value <= num.from) || (value <= num.to)) {
            Swal.fire("Please Enter Large Value4", "", "warning")
            this.completeData[tableIndex].perPackageTable[outerindex].table[0].to = null
            event.stopPropagation();
          } else {
            this.completeData.forEach((table: any, index) => {
              if (tableIndex === index) {
                console.log('this.completeData[index] >>', this.completeData[index]);
                if (this.completeData[index].perPackageTable[outerindex].table[0].from >= value) {
                  console.log("hi");

                  Swal.fire("Please Enter Large value3", "", "warning")
                  this.completeData[index].perPackageTable[outerindex].table[0].to = null
                  event.stopPropagation();
                }
              }

            });
          }
        });
      } else {
        this.completeData.forEach((table: any, index) => {
          if (tableIndex === index) {
            console.log('this.completeData[index] >>', this.completeData[index]);
            if (this.completeData[index].perPackageTable[outerindex].table[0].from >= value) {
              console.log();

              Swal.fire("Please Enter Large Value1", "", "warning")
              this.completeData[index].perPackageTable[outerindex].table[0].to = null

              event.stopPropagation();
            }
          }
        });
      }
    }
    else {

      Swal.fire("Please Enter Large Value2", "", "warning")
    }
  }
  processPackagePrice(i, outerindex, innerindex, rowindex, event): void {
    console.log(event.target.value);

    let numberVal = parseInt(this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].price).toLocaleString();
    console.log(numberVal);

    this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].price = numberVal;
    // //  this.completeData[i].outletObj.price
    if (event.target.value == "NaN") {
      this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].price = null;
    }
  }
  processPackageOutletPrice(i, outerindex, event): void {
    let numberVal = parseInt(event.target.value).toLocaleString();
    console.log(numberVal);

    this.completeData[i].perPackageTable[outerindex].table[0].innerTable.price = numberVal;
    if (event.target.value == 'NaN') {
      this.completeData[i].perPackageTable[outerindex].table[0].innerTable.price = null
    }

  }
  deletePackageTable(r, i) {
    this.completeData[i].perPackageTable.splice(r, 1);
  }
  deleteRowInnerTable(i, outerindex, innertableindex, rowindex, length) {
    console.log(rowindex, length);
    // debugger;
    if (rowindex == 0 && length == 0) {
      this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.splice(rowindex, 1);
      //this.completeData[i].outletObj={}
      this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.from = 1
    } else
      if (rowindex == 0 && length == 1) {
        this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.from = 1
        this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.splice(rowindex, 1);

      } else
        if (rowindex == 0) {
          this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.splice(rowindex, 1);
          this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable[rowindex]['from'] = 1

        } else
          if (rowindex == length - 1 && rowindex !== 0) {
            let from = this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable[rowindex]['from']
            this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.splice(rowindex, 1);
            this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.from = from
          }
          else {
            // console.log("row", this.completeData[i].tableData[rowindex]);
            let from = this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable[rowindex]['from']
            this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.splice(rowindex, 1);
            this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable[rowindex]['from'] = from
            // console.log("row", this.completeData[i].tableData[rowindex]);
          }
    // this.completeData[i].perPackageTable[outerindex].table[0].innerTable.splice(rowindex, 1);
  }
}
