export class OutletRange {
    rangeFrom:number;
    rangeTo: number;
    pricePerOutlet: number;
    percentagePerOutlet: number;

    constructor(parameter?: any) {
        if (parameter == undefined) {
            this.rangeFrom =1;
            this.rangeTo = null;
            this.pricePerOutlet = null;
            this.percentagePerOutlet=null;
        } else {
            this.rangeFrom = 1;
            this.rangeTo = parameter.rangeTo;
            this.pricePerOutlet = parameter.pricePerOutlet;
            this.percentagePerOutlet= parameter.percentagePerOutlet;
        }
    }

    public setRangeFrom(value) {
        this.rangeFrom = 1;
    }
    public setRangeTo(value) {
        this.rangeTo = value;
    }
    public setPricePerOutlet(value) {
        this.pricePerOutlet = value;
    }
    public setPercentagePerOutlet(value) {
        this.percentagePerOutlet = value;
    }

}