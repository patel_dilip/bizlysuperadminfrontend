/**changed code*/
import { Outlet } from './Outlet';
import { OutletPerOrder } from './OutletPerOrder';
import { OutletPerPackage } from './OutletPerPackage';
// import{POutlet}from './OutletPackage'

export class RevenueCollectionType {
    freePriced: String;
    soldPerOutlet: String;
    colletionMode: String;
    tempfreePriced: String;
    tempsoldPerOutlet: String;
    tempcolletionMode: String;

    perOrderChain: String;
    perOrderCommissionType: String; //for per order
    country: {};
    perOrderPrice: string //for per order
    outletObj: Outlet;
    tableData: Outlet[];
    perOrderTable: OutletPerOrder[];
    perPackageTable: OutletPerPackage[];
    constructor(country?: {},
        soldPerOutlet?: String,
        perOrderCommissionType?: String,
        perOrderChain?: String,
        freePriced?: string,
        perOrderPrice?: string,
        colletionMode?: string,
        tableData?: Outlet[],
        outletObj?: Outlet,
        tempsoldPerOutlet?: String,
        tempfreePriced?: string,
        tempcolletionMode?: string,
        perOrderTable?: OutletPerOrder[],
        perPackageTable?: OutletPerPackage[]
    ) {
        if (perOrderPrice == null) {
            perOrderPrice = ''
        } else {
            this.perOrderPrice = perOrderPrice;
        }
        if (perOrderCommissionType == null) {
            perOrderCommissionType = null
        } else {
            this.perOrderCommissionType = perOrderCommissionType;
        }

        if (perOrderChain == null) {
            perOrderChain = ''
        } else {
            this.perOrderChain = perOrderChain;
        }

        if (freePriced == null) {
            freePriced = ''
        } else {
            this.freePriced = freePriced;
        }

        if (soldPerOutlet == null) {
            soldPerOutlet = ''
        } else {
            this.soldPerOutlet = soldPerOutlet;
        }

        if (outletObj == null) {
            outletObj = null
        } else {
            this.outletObj = outletObj;
        }

        if (colletionMode == undefined) {
            this.colletionMode = "";
        } else {
            this.colletionMode = colletionMode;
        }

        if (country == undefined) {
            this.country = {};
        } else {
            this.country = country;
        }

        if (tableData == undefined) {
            this.tableData = [];
        } else {
            this.tableData = tableData;
        }
        if (perOrderPrice == undefined) {
            this.perOrderPrice = '';
        } else {
            this.perOrderPrice = perOrderPrice;
        }

        if (tempfreePriced == undefined) {
            this.tempfreePriced = ""
        } else {
            this.tempfreePriced = tempfreePriced
        }

        if (tempsoldPerOutlet == undefined) {
            this.tempsoldPerOutlet = ""
        } else {
            this.tempsoldPerOutlet = tempsoldPerOutlet
        }

        if (tempcolletionMode == undefined) {
            this.tempcolletionMode = ""
        } else {
            this.tempcolletionMode = tempcolletionMode;
        }

        if (perOrderTable == undefined) {
            this.perOrderTable = []
        } else {
            this.perOrderTable = perOrderTable;
        }

        if (perPackageTable == undefined) {
            this.perPackageTable = new Array(new OutletPerPackage());
        } else {
            this.perPackageTable = perPackageTable;
        }
    }

    getRevenueCollectionModel() {
        return {
            colletionMode: this.colletionMode,
            country: this.country,
            tableData: this.tableData,
            perOrderPrice: this.perOrderPrice,
            outletObj: this.outletObj,
            soldPerOutlet: this.outletObj,
            perOrderChain: this.perOrderChain,
            perOrderCommissionType: this.perOrderCommissionType,
            freePriced: this.freePriced
        }
    }
    public setOutletObj(outlet: Outlet) {
        this.outletObj = outlet
    }
    public setOutletArray(outlet: any) {
        this.tableData = outlet;
    }
    public setFreePriced(value: string) {
        this.freePriced = value;
    }
    public setsoldPerOutlet(value: string) {
        this.soldPerOutlet = value;
    }
    public setcolletionMode(value: string) {
        this.colletionMode = value;
    }
    public setperOrderCommissionType(value: string) {
        this.perOrderCommissionType = value;
    }
    public setperOrderPrice(value: string) {
        this.perOrderPrice = value;
    }
    public setperOrderTable(value: any) {
        this.perOrderTable.push(value);
    }
}