import { OutletRange } from "./Range";

export class OutletPerOrder {
    outletFrom: number;
    outletTo: number;
    perOrderCurrentData: OutletRange;
    perOrders: OutletRange[];
    constructor(parameter) {
        if (parameter == undefined) {
            this.outletFrom =1;
            this.outletTo = null ;
            this.perOrderCurrentData = new OutletRange();
            this.perOrders = [];
        } else {
            this.outletFrom = parameter.outletFrom;
            this.outletTo = parameter.outletTo;
            this.perOrderCurrentData = parameter.perOrderCurrentData;
            this.perOrders = parameter.perOrders;
        }
    }
    public setoutletFrom(value) {
        this.outletFrom = value.outletFrom;
    }
    public setoutletTo(value) {
        this.outletTo = value.outletTo;
    }
    public setperOrderCurrentData(value) {
        this.perOrderCurrentData = value.perOrderCurrentData;
    }
    public setperOrders(value) {
        this.perOrders = value.perOrders;
    }

}