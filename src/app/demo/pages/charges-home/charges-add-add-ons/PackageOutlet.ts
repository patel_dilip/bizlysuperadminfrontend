export class PackageOutlet {
    from: number;
    to: number;
    price: number;
    innerTable?: [] = [];
    constructor(parameter) {
        if (parameter) {
            this.from = parameter.from;
            this.to = parameter.to;
            this.price = parameter.price;
            this.innerTable = parameter.innerTable;
        } else {
            this.from = 1;
            this.to = null;
            this.price = null;
            this.innerTable = [];
        }
    }
    resetValue() {
        return {
            from: 0,
            to: 0,
            price: 0,
        };
    }
    getObject(parameter?: any) {
        return new PackageOutlet(parameter);
    }
}

