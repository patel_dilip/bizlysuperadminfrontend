import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargesAddAddOnsComponent } from './charges-add-add-ons.component';

describe('ChargesAddAddOnsComponent', () => {
  let component: ChargesAddAddOnsComponent;
  let fixture: ComponentFixture<ChargesAddAddOnsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargesAddAddOnsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargesAddAddOnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
