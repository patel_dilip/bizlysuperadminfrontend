import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChargesHomeComponent } from './charges-home/charges-home.component';
import { ChargesComponent } from './charges/charges.component';
import { ChargesAddProductComponent } from './charges-add-product/charges-add-product.component';
import { ChargesAddIntegrationComponent } from './charges-add-integration/charges-add-integration.component';
import { ChargesAddAddOnsComponent } from './charges-add-add-ons/charges-add-add-ons.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule  } from '@angular/forms';
import { BreadcrumbModule } from 'src/app/theme/shared/components';
import { MaterialModule } from 'src/app/material/material.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FileUploadModule } from 'ng2-file-upload';
import { ViewChargesAddonsComponent } from './view-charges-addons/view-charges-addons.component';
import { EditChargesAddonsComponent } from './edit-charges-addons/edit-charges-addons.component';
import { EditChargesProductComponent } from './edit-charges-product/edit-charges-product.component';
import { ViewChargesProductComponent } from './view-charges-product/view-charges-product.component';
import { ViewChargesIntegrationComponent } from './view-charges-integration/view-charges-integration.component';
import { EditChargesIntegrationComponent } from './edit-charges-integration/edit-charges-integration.component';
import { AgGridModule } from 'ag-grid-angular';
//import{FormModule} from '@angular/forms'
///import { BrowserModule } from '@angular/platform-browser';
//import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { MultiSelectModule } from 'primeng/multiselect';
const routes: Routes = [
  {
    path: '',
    component: ChargesHomeComponent,
    children: [
      {
        path  :'',
        component :ChargesComponent
      },
      {
        path: 'add-addons',
        component: ChargesAddAddOnsComponent
      },
      {
        path: 'add-integration',
        component: ChargesAddIntegrationComponent
      },
      {
        path: 'add-product',
        component: ChargesAddProductComponent
      },
      {
        path: 'view-addons',
        component: ViewChargesAddonsComponent
      },
      {
        path: 'update-addons',
        component: EditChargesAddonsComponent
      },
      {
        path: 'view-product',
        component: ViewChargesProductComponent
      },
      {
        path: 'update-product',
        component: EditChargesProductComponent
      },
      {
        path: 'view-integration',
        component: ViewChargesIntegrationComponent
      },
      {
        path: 'update-integration',
        component: EditChargesIntegrationComponent
      }
    ]
  }
];

@NgModule({
  declarations: [ChargesHomeComponent, ChargesComponent, ChargesAddProductComponent, ChargesAddIntegrationComponent, ChargesAddAddOnsComponent, ViewChargesAddonsComponent, EditChargesAddonsComponent, EditChargesProductComponent, ViewChargesProductComponent, ViewChargesIntegrationComponent, EditChargesIntegrationComponent],
  imports: [
    NgMultiSelectDropDownModule.forRoot(),
    CommonModule,
    RouterModule.forChild(routes),
    BreadcrumbModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    AgGridModule.withComponents([]),
    //BrowserModule
    TableModule,
    MultiSelectModule
  ]
})
export class ChargesHomeModule { }
