import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargesHomeComponent } from './charges-home.component';

describe('ChargesHomeComponent', () => {
  let component: ChargesHomeComponent;
  let fixture: ComponentFixture<ChargesHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargesHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargesHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
