import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChargesAddonsComponent } from './edit-charges-addons.component';

describe('EditChargesAddonsComponent', () => {
  let component: EditChargesAddonsComponent;
  let fixture: ComponentFixture<EditChargesAddonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditChargesAddonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChargesAddonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
