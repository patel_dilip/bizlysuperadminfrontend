import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIntegrationPopupComponent } from './add-integration-popup.component';

describe('AddIntegrationPopupComponent', () => {
  let component: AddIntegrationPopupComponent;
  let fixture: ComponentFixture<AddIntegrationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddIntegrationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIntegrationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
