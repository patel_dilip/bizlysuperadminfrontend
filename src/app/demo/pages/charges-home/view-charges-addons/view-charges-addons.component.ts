import { Component, OnInit } from '@angular/core';
import { ChargeService } from 'src/app/_services/charges/charge.service';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-view-charges-addons',
  templateUrl: './view-charges-addons.component.html',
  styleUrls: ['./view-charges-addons.component.scss']
})
export class ViewChargesAddonsComponent implements OnInit {
addonsData
  constructor(private charge:ChargeService, private router: Router ) {
    console.log(charge.viewObject)
    if(charge.viewObject==undefined){
      router.navigate(['/charges'])
    }
this.addonsData=charge.viewObject
   }
   viewAddons(row) {
    ////console.log(row);

    this.charge.getSingleAddons(row._id).subscribe(data => {
      ////console.log(data);
      if (data['success'] == true) {
        this.charge.editObject = row
        this.router.navigate(['/charges/update-addons'])
      }


    })
  }

  ngOnInit() {
  }

}
