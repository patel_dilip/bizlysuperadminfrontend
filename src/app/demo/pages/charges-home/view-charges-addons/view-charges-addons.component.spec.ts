import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChargesAddonsComponent } from './view-charges-addons.component';

describe('ViewChargesAddonsComponent', () => {
  let component: ViewChargesAddonsComponent;
  let fixture: ComponentFixture<ViewChargesAddonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChargesAddonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChargesAddonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
