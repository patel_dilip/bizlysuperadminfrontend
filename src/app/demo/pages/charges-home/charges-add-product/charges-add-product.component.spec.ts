import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargesAddProductComponent } from './charges-add-product.component';

describe('ChargesAddProductComponent', () => {
  let component: ChargesAddProductComponent;
  let fixture: ComponentFixture<ChargesAddProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargesAddProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargesAddProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
