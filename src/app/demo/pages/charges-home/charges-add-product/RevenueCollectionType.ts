/**changed code*/
import { Outlet } from './Outlet';

export class RevenueCollectionType {
    colletionMode: String;
    soldPerOutlet: String;
    country: { };
    tableData: Outlet[];
    outletObj: Outlet;
    constructor(country?: {}, colletionMode?: string, soldPerOutlet?: String, tableData?: Outlet[], outletObj?: Outlet) {
        if (outletObj == null) {
            outletObj = null
        } else {
            this.outletObj = outletObj;
        }


        if (colletionMode == undefined) {
            this.colletionMode = "";
        } else {
            this.colletionMode = colletionMode;
        }

        if (soldPerOutlet == undefined) {
            this.soldPerOutlet = "";
        } else {
            this.soldPerOutlet = soldPerOutlet;
        }

        if (country == undefined) {
            this.country = {};
        } else {
            this.country = country;
        }

        if (tableData == undefined) {
            this.tableData = [];
        } else {
            this.tableData = tableData;
        }

    }

    getRevenueCollectionModel() {
        return {
            colletionMode: this.colletionMode,
            soldPerOutlet: this.soldPerOutlet,
            country: this.country,
            tableData: this.tableData,
            outletObj: this.outletObj
        }
    }

}