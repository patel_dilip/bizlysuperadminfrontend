/**changed code*/
export class Outlet {
    from: number;
    to: number;
    price: number;
    isReadonly: boolean;    
    constructor(parameter?: any) {
        if (parameter === undefined) {
            this.from = 1;
            this.to = null;
            this.price = null;
            this.isReadonly = null;            
        } else {
            this.from = parameter.from;
            this.to = parameter.to;
            this.price = parameter.price;
            this.isReadonly = parameter.isReadonly;            
        }
    }
    resetValue() {
        return {
            from: 0,
            to: 0,
            price: 0,
            isReadonly: false
        }
    }
}