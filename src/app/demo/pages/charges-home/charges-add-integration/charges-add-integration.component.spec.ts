import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargesAddIntegrationComponent } from './charges-add-integration.component';

describe('ChargesAddIntegrationComponent', () => {
  let component: ChargesAddIntegrationComponent;
  let fixture: ComponentFixture<ChargesAddIntegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargesAddIntegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargesAddIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
