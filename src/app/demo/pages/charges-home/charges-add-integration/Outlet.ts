/**changed code*/
export class Outlet {
    from: number;
    to: number;
    price: number;
    packages:string;
    constructor(parameter?: any) {
        if (parameter == undefined) {
            this.packages='';
            this.from = 1;
            this.to = null;
            this.price = null;
         
        } else {
            this.packages=parameter.packages
            this.from = parameter.from;
            this.to = parameter.to;
            this.price = parameter.price;
            
        }
    }
    resetValue() {
        return {
            packages:'',
            from: 0,
            to: 0,
            price: 0,
            
        }
    }
}