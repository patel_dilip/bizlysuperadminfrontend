import { PackageOutlet } from './PackageOutlet';

export class OutletPerPackage {
    packageValue: string;
    table: PackageOutlet[];

    constructor(parameter?: any) {
        if (parameter) {
            this.packageValue = parameter.packageValue;
            this.table = parameter.table;
        } else {
            this.packageValue = '';
            this.table = new Array(new PackageOutlet(undefined));
        }
    }
}
