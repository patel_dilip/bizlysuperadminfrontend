/**changed code*/
import { Outlet } from './Outlet';
import { OutletPerPackage } from './OutletPerPackage'

export class RevenueCollectionType {
    colletionMode: String;
    freePriced: String;
    outletChains:string;
    country: {};
    tableData: Outlet[];
    outletObj: Outlet;
    perPackageTable: OutletPerPackage[];
    constructor(country?: {}, colletionMode?: string, freePriced?: String,
        outletChains?:string, tableData?: Outlet[], outletObj?: Outlet,
        perPackageTable?: OutletPerPackage[]) {
        if (outletObj == null) {
            outletObj = null
        } else {
            this.outletObj = outletObj;
        }


        if (colletionMode == undefined) {
            this.colletionMode = "";
        } else {
            this.colletionMode = colletionMode;
        }

        if (outletChains == undefined) {
            this.outletChains = "";
        } else {
            this.outletChains = outletChains;
        }
        if (freePriced == undefined) {
            this.freePriced = "";
        } else {
            this.freePriced = freePriced;
        }

        if (country == undefined) {
            this.country = {};
        } else {
            this.country = country;
        }

        if (tableData == undefined) {
            this.tableData = [];
        } else {
            this.tableData = tableData;
        }
        if (perPackageTable == undefined) {
            this.perPackageTable = new Array(new OutletPerPackage());
        } else {
            this.perPackageTable = perPackageTable;
        }

    }

    getRevenueCollectionModel() {
        return {
            colletionMode: this.colletionMode,
            freePriced: this.freePriced,
            outletChains:this.outletChains,
            country: this.country,
            tableData: this.tableData,
            outletObj: this.outletObj
        }
    }

}