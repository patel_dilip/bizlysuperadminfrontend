import { ChargesAddAddOnsComponent } from '../charges-add-add-ons/charges-add-add-ons.component';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { MainService } from 'src/app/_services/main.service';
import { RevenueCollectionType } from '../charges-add-product/RevenueCollectionType';
import { Outlet } from '../charges-add-product/Outlet';
import { ChargeService } from 'src/app/_services/charges/charge.service';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from "ngx-spinner";
import { Router, RouterLinkWithHref } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { environment } from 'src/environments/environment';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ChargesAddProductComponent } from '../charges-add-product/charges-add-product.component';
const URL = environment.base_Url + 'chargesproduct/uploadphotos';

@Component({
  selector: 'app-add-product-popup',
  templateUrl: './add-product-popup.component.html',
  styleUrls: ['./add-product-popup.component.scss']
})
export class AddProductPopupComponent implements OnInit {
  completeData: any = new Array();
  outletForCountry: any = {};
  currentProductOutlet: Outlet;
  product: any = {};
table_row:boolean=false;

  //tableData
  uploader: FileUploader;
  uploader_video: FileUploader;
  response_video: string;
  multiResponse_video = [];
  response: string;
  multiResponse = [];
  length: number;
  dropdownList = [];
  avail_addons = ['CRM', 'Inventory'];
  revenue_Collection_mode_list = ["Per Year", "Per Month", "One Time",];
  selectedItems = ['India', 'USA'];
  dropdownSettings = {};
  Settings = {};
  IDropdownSettings: IDropdownSettings;
  IDropdownSettings1: IDropdownSettings;
  selectedAlbums = ['India', 'USA', "Chaina"];
  sold_outlet = ["Per Outlet"];
  _arr: any;
  isDataAvailableAddon=false
  isDataAvailableIntegration=false

  RevenueCollectionObj: RevenueCollectionType[] = new Array();
  avail_countries: any;
  availAddons = [];
  availIntegration = [];
  outlet: any;
  loginUserID: any;
  clicked: boolean;
  countryAPI: any;

  constructor(public dialogRef: MatDialogRef<any>,public dialog: MatDialog, private spinner: NgxSpinnerService, private fb: FormBuilder, private route: Router, private mainService: MainService, private charges: ChargeService) {
    // this.AddonsList();
    // this.IntegrationList();
    // for(let i=0; i<this.completeData.length;i++){
    // this.product.completeData[i].tableData[1].from=2
    // }

    let loginUser = JSON.parse(localStorage.getItem('loginUser'));
    this.loginUserID = loginUser._id;
        this.dropdownSettings = this.IDropdownSettings = {
          limitSelection: 1,
          singleSelection: false,    
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      enableCheckAll: false
    };
    this.Settings = this.IDropdownSettings1 = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      enableCheckAll: false
    };
    //console.log("length", this.selectedItems.length);

  }
  //addProduct: FormGroup
  country = [];
  ngOnInit() {


    this.uploader = new FileUploader({
      url: URL,
      itemAlias: 'chargesproduct',
      allowedFileType: ['image']

    });
    ////console.log(">>",this.uploader);

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);
    //console.log(">>>", this.response);

    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {

      //console.log('FileUpload:uploaded:', item, status, response);
      //console.log(status);





      this.response = JSON.parse(response);
      //console.log('response', this.response);

      this.multiResponse.push(this.response['chargesproductPhotoUrls'][0]);
      //console.log('multi response', this.multiResponse);
      this.product.images = this.multiResponse;

    };

    // video uploading
    this.uploader_video = new FileUploader({
      url: URL,
      itemAlias: 'chargesproduct',
      allowedFileType: ['video']

    });

    this.response_video = '';

    this.uploader_video.response.subscribe(res => this.response_video = res);

    this.uploader_video.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      //console.log('FileUpload:uploaded:', item, status, response);



      this.response_video = JSON.parse(response);
      //console.log('response', this.response);

      this.multiResponse_video.push(this.response_video['chargesproductPhotoUrls'][0]);
      //console.log('multi response', this.multiResponse_video);
      this.product.videos = this.multiResponse_video;

    };

  }


  add_Product() {
    if(this.completeData[0].tableData.length!==0){
      this.clicked=true
    
    if (this.availAddons.length != 0) {
      this.availAddons.map(ele => {
        if (this.completeData[0].available_addons.length != 0) {

          this.completeData[0].available_addons.map(element => {
            if (ele.item_text == element.item_text) {
              element.country = ele.country
            }

          })
        }
      })
    }
    if (this.availIntegration.length != 0) {
      this.availIntegration.map(ele => {
        if (this.completeData[0].available_Integration.length != 0) {

          this.completeData[0].available_Integration.map(element => {
            if (ele.item_text == element.item_text) {
              element.country = ele.country
            }

          })
        }
      })
    }
    this.product.completeData = this.completeData;
    //console.log('this.product >>>', this.product);
    let obj = this.product;
    this.product.userid = this.loginUserID;
    this.charges.addproduct(obj).subscribe(data => {
      //console.log(data);
      if (data['success'] == true) {
        this.clicked=false
        Swal.fire('Product Added Successfully', '', 'success')
        this.dialogRef.close();
      }
      else if (data['success'] == false && data['msg'] == "ProductName Already Exists") {
        Swal.fire('Product Already Exist', '', 'warning')
        this.product.productName = ""
      }

    })
  }
  }
  checkCountry(event){
    console.log("prodeuct>>>", event.target.value);
    
    this.charges.getProductCountry(event.target.value).subscribe(data=>{
      console.log(data);
      if(data['success']==true){
        let countrylist=[]
        console.log(">>", data['data'].length);
        for(let i=0; i<data['data'].length; i++){
          console.log(data['data'][i]['completeData']);
          data['data'][i]['completeData'].forEach(element => {
            countrylist.push(element.country['item_text'])

            element.country=countrylist
          });
          Swal.fire('Product Available...'+countrylist, "", "warning")

          
          
        }
console.log("countrylist>>>", countrylist);
this.mainService.getAllDialCode().subscribe((country: any[]) => {
  let i = 0;
  let temp = [];
  country.forEach(element => {
       if(countrylist.includes(element['name'])==false){
      i++;
      temp.push({ item_id: i, item_text: element['name'], symbol: element['currency_Symbol'] });
    }
  });
  this.dropdownList = temp;

});
this.avail_countries=[]
this.completeData=[]
        
      }
else{
  this.mainService.getAllDialCode().subscribe((country: any[]) => {
    let i = 0;
    let temp = [];
    country.forEach(element => {
              i++;
              temp.push({ item_id: i, item_text: element['name'], symbol: element['currency_Symbol'] });
      
    });
    this.dropdownList = temp;
  
  });
  this.avail_countries=[]
  this.completeData=[]
}
      
    })
  }
  onItemSelect(item: any) {
    this.countryAPI=item['item_text']
    this.AddonsList(this.countryAPI)
    this.IntegrationList(this.countryAPI)

    this._arr = this.avail_countries;
    this.length = this._arr.length;
    //console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", this.completeData);
    // this._arr.forEach(row => {

    //  //console.log("result>>>.", result);
    //console.log(item);
    item.status=true
    let tempData = new RevenueCollectionType(item, "", "", this.outlet, new Outlet());
    //console.log("tempDATA", tempData);

    this.completeData.push(tempData);
    // })
    //console.log('this._arr :', this.completeData);
  }

  onItemDeSelect(item: any) {
    this._arr = this.avail_countries;
    this.length = this._arr.length;
    let outlet = [];
    // item.status=true
    let tempData = new RevenueCollectionType(item, "", "", outlet, new Outlet());
    var removeIndex = this.completeData.map(function(i) { 
      //console.log("remove", i['country']['item_text']);
      return i['country']['item_text'];
     }).indexOf(item.item_text);
//console.log(removeIndex,">>>");
    this.completeData.splice(removeIndex, 1);
    //console.log('this._arr :', this.completeData);
  }

  revenueMode(event, data,i) {
    //console.log(event,">>>>",data,">>>>",i);
   // data.
    data.colletionMode=event.value,
    data.tableData=[]
    data.outletObj={from: 1, to:null, price:null}
    data.soldPerOutlet="Per Outlet"
    data.available_addons=[]
    data.available_Integration=[]
data.reasonForNo=null
  }
  soldpersoulet(event, data, i) {
    data.soldPerOutlet=event.value,
    data.tableData=[]
    data.outletObj={from: 1, to:null, price:null}
    data.reasonForNo=""
    data.available_addons=[]
    data.available_Integration=[]

  }
  addRow(currentObj, index) {
    //console.log('rowData,countrySegment : :', currentObj);
    let outletData = new Outlet(currentObj.outletObj);
    //console.log(this.completeData);
  }
  removeImage(imgurl) {
    //console.log(imgurl);
    this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1);
  }
  removeVideo(url) {
    //console.log(url);
    this.multiResponse_video.splice(this.multiResponse_video.indexOf(url), 1);
  }

  pushProductOutlet(row, index) {
    //console.log(row, index);
    this.completeData.forEach((row, i) => {
      //console.log('complete data index ', i);
      if (index === i) {
        let obj = Object.assign({}, this.completeData[i].outletObj);
        // //console.log(this.completeData[i])
        if (this.completeData[i].outletObj.price != "NaN" && this.completeData[i].outletObj.from != '' && this.completeData[i].outletObj.to != '' && this.completeData[i].outletObj.price != '' && this.completeData[i].outletObj.from != null && this.completeData[i].outletObj.to != null && this.completeData[i].outletObj.price != null) {  
          
          this.completeData[i].tableData.push(obj);
          
          this.completeData[i].outletObj.from = this.completeData[i].outletObj.to + 1;
          if(this.completeData[i
          ].outletObj.from>=2000){
           Swal.fire("Outlet Range limit '1 to 2000' Only", "" ,"info")
          }
          this.completeData[i].outletObj.to = null;
          this.completeData[i].outletObj.price = null;
        }
        if (this.completeData[i].outletObj.price == "NaN") {
          this.completeData[i].outletObj.price = null;
        }
        else {
          // let s ="7,11,00,000"
        }
      }
    });
  }
  deleteRow(row, i, length) {
    //console.log(row, length);
    if(row==0 && length==0){
      this.completeData[i].tableData.splice(row, 1);
      this.completeData[i].outletObj={}
      this.completeData[i].outletObj.from=1
    }else
    if(row==0 && length==1){
      this.completeData[i].tableData.splice(row, 1);
      this.completeData[i].outletObj.from=1
    } else
    if(row==0){
      this.completeData[i].tableData.splice(row, 1);
      this.completeData[i].tableData[row]['from']=1
      
    }else 
    if(row==length-1){
      let from=this.completeData[i].tableData[row]['from']
        this.completeData[i].tableData.splice(row, 1);
        this.completeData[i].outletObj.from=from
      }
    else 
    {
      //console.log("row",this.completeData[i].tableData[row]);
let from=this.completeData[i].tableData[row]['from']
    this.completeData[i].tableData.splice(row, 1);
    this.completeData[i].tableData[row]['from']=from
//console.log("row",this.completeData[i].tableData[row]);
    }
  }
  AddonsList(item) {
    this.availAddons=[]
    let j = 0;
    this.charges.getAddonByCountry(item).subscribe(data => {
      console.log(data);
        if (data['sucess'] == true) {

      data['data'].forEach(element => {
        // if (element.status == true) {
          //console.log("ele>", element);
          let country
          
          for (let i = 0; i < element.completeData.length; i++) {
            if (element.completeData[i].country != null && element.completeData[i].country != {}) {
              if(element.completeData[i].country['status']){
               
                element.country={country:element.completeData[i].country['item_text'], status:element.completeData[i].country['status']}
            this.availAddons.push({item_id:j,item_text:element['AddonsName'], country:element.country})
              
              }
            }
          }
  j++
         // i++;
        // }
      });
    }
      console.log("addonslist>>>>>>", this.availAddons);
      this.isDataAvailableAddon=true
    });
  }
  IntegrationList(item) {

   // let i = 0;
   let j=0
   this.availIntegration=[]
    this.charges.getIntegrationByCountry(item).subscribe(data => {
      console.log(data);
      if (data['sucess'] == true) {

      data['data'].forEach(element => {   
          let country
                    for (let i = 0; i < element.completeData.length; i++) {
            if (element.completeData[i].country != null && element.completeData[i].country != {}) {
              if(element.completeData[i].country['status']){
              element.country={country:element.completeData[i].country['item_text'], status:element.completeData[i].country['status']}
          this.availIntegration.push({item_id:j,item_text:element['integrationName'],country:element.country});
                
              }
            }
          } 
    j++;
        
      });
    }
      console.log("available>>>>>>", this.availIntegration);
this.isDataAvailableIntegration=true
    });
  }
  processMyValue(i): void {
    this.completeData[i].reasonForNo = parseInt(this.completeData[i].reasonForNo).toLocaleString();
    //  this.completeData[i].reasonForNo = ;
    if (this.completeData[i].reasonForNo == "NaN") {
      this.completeData[i].reasonForNo = null;
    }

  }
  processPrice(i, rowindex): void {
    let numberVal = parseInt(this.completeData[i].tableData[rowindex].price).toLocaleString();
    this.completeData[i].tableData[rowindex].price = numberVal;
    //  this.completeData[i].outletObj.price
    if (this.completeData[i].tableData[rowindex].price == "NaN") {
      this.completeData[i].tableData[rowindex].price = null;
    }
  }
  processOutletPrice(i): void {
    let numberVal = parseInt(this.completeData[i].outletObj.price).toLocaleString();
    this.completeData[i].outletObj.price = numberVal;
    if (this.completeData[i].outletObj.price == "NaN") {
      this.completeData[i].outletObj.price = null;
    }

  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 44) {
      return true;
    }


    else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  checkRange(value, tableIndex, tableData, event) {

    if (value !== null) {
      if (tableData.length > 0) {
        tableData.forEach(num => {
          //console.log(value, '<>', num.from, '<>', num.to);
          // return (value >= num.from || value >= num.to) ? true : false;
          if ((value <= num.from) || (value <= num.to)) {
           Swal.fire("Please Enter Large Value", "", "warning")
           this.completeData[tableIndex].outletObj.to=null
            event.stopPropagation();
          } else {
            this.completeData.forEach((table: any, index) => {
              if (tableIndex === index) {
                //console.log('this.completeData[index] >>', this.completeData[index]);
                if (this.completeData[index].outletObj.from >= value) {
                  Swal.fire("Please Enter Large value", "", "warning")
                  this.completeData[index].outletObj.to=null
                  event.stopPropagation();
                }
              }

            });
          }
        });
      } else {
        this.completeData.forEach((table: any, index) => {
          if (tableIndex === index) {
            //console.log('this.completeData[index] >>', this.completeData[index]);
            if (this.completeData[index].outletObj.from >= value) {
              Swal.fire("Please Enter Large Value" , "", "warning")
              event.stopPropagation();
            }
          }
        });
      }
    }
    else{
      Swal.fire("Please Enter Large Value" , "", "warning")
    }
  }
  checkRangeInTable(rowindex,value, tableData, event,i , length) {
    //console.log(rowindex, length);
    if (value !== null) {
      tableData.forEach(element => {
        //console.log(value, '   range >', element.from, element.to)
        if (element.to <= element.from)  {
          //console.log(value,">>", element.from);
          
          Swal.fire("Please Enter Large value","", "warning")
          this.completeData[i].tableData[rowindex].to=null
          event.stopPropagation();
        }
      });
    }
    else{
      Swal.fire("Please Enter Large value", "","warning")
    }
    
    if(rowindex+1==length && this.completeData[i].tableData[rowindex].to!= null){
   
    let val=value+1
    this.completeData[i].outletObj.from=val
    
  }
  else{
    if(value+1>=this.completeData[i].tableData[rowindex+1].to || value>=2000){
      // intk=length-1
      if(value>=2000){
        Swal.fire("Outlet Range limit '1 to 2000' Only", "" ,"info")
      }
      let k= length-(rowindex+1)
                  this.completeData[i].tableData.splice(rowindex+1  , k);
                  if(this.completeData[i].tableData[rowindex].to!=null){
                  this.completeData[i].outletObj['from']=value+1
                  }
          }
        

    else{
      if(this.completeData[i].tableData[rowindex].to!= null)
    this.completeData[i].tableData[rowindex+1].from=value+1
    }
  }

  }
  checkValue(event){
    //console.log("event", event);
    const charCode = (event.which) ? event.which : event.keyCode;
     if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    else if(event.target.value>2000){
      Swal.fire("Please Enter value less than 2000", "", "warning")
      event.target.value=null
      return false
    }else{
      return true
    }
    
  }
  e(event){
    //console.log(event);
    
      const charCode = (event.which) ? event.which : event.keyCode;
      if(charCode==101 ||charCode==46 || charCode==43 ||charCode==45){
        return false;
            }
    }
   
  
  onNoClick(): void {
    this.dialogRef.close();
  }

}
