import { Component, OnInit } from '@angular/core';
import { ChargeService } from 'src/app/_services/charges/charge.service';
import { MainService } from 'src/app/_services/main.service';

import { IDropdownSettings } from 'ng-multiselect-dropdown';
import{Outlet} from '../charges-add-integration/Outlet'
import { FileUploader } from 'ng2-file-upload';
import { RevenueCollectionType } from '../charges-add-integration/RevenueCollectionType';

import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { AddProductPopupComponent } from '../add-product-popup/add-product-popup.component';
import { AddAddonPopupComponent } from '../add-addon-popup/add-addon-popup.component';
import { MatDialog } from '@angular/material';
import { OutletPerPackage } from '../charges-add-add-ons/OutletPerPackage';
import { PackageOutlet } from '../charges-add-add-ons/PackageOutlet';
const URL = environment.base_Url + 'chargesintegration/uploadphotos'


@Component({
  selector: 'app-edit-charges-integration',
  templateUrl: './edit-charges-integration.component.html',
  styleUrls: ['./edit-charges-integration.component.scss']
})
export class EditChargesIntegrationComponent implements OnInit {
countryAPI:string
    
  completeData: any = new Array();
  outlet:any
  uploader: FileUploader
  uploader_video: FileUploader
  response_video: string;
  multiResponse_video = [];
  response: string;
  multiResponse = [];
  RevenueCollectionObj: RevenueCollectionType[] = new Array();
  outletForCountry: any = {};
  currentProductOutlet: Outlet;
  integration: any = {};
  avail_countries:any
  // packages = [{"pack":"abc"}]
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
 selected:any
IDropdownSettings:IDropdownSettings

  countries:any;
  countryname=[]
  length:number;
  _arr=[];
  selected_countries=[];
  copyDataList:any;
  loginUserID: any;
  isLoading: boolean;
  constructor(private route:Router, private dialog: MatDialog,private mainService:MainService, private charges: ChargeService) {
    console.log('charge.editObject >>', charges.editObject)
    this.integration = charges.editObject;
    if(charges.editObject==undefined){
      route.navigate(['/charges'])
    }
    this.countryAPI=charges.editObject.completeData[0].country['item_text']
    this.multiResponse= this.charges.editObject['images']
    this.multiResponse_video= this.charges.editObject['videos']
    this.integration.addOnsAssociated=charges.editObject.addOnsAssociated
  this.productlist();
  this.addonslist(this.countryAPI);
  this.checkCountry_whenLoad();
    this.avail_countries = this.integration.completeData.map(d => {
      return d.country
    });
    this.completeData = this.integration.completeData;

    let loginUser = JSON.parse(localStorage.getItem('loginUser'))

      this.loginUserID = loginUser._id
  
   // this.dataSource = new MatTableDataSource(this.chain_DATA);
  //  this.dataSource1 = new MatTableDataSource(outlet_DATA);
  this.dropdownSettings=this.IDropdownSettings = {
    limitSelection: 1,
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true,
      enableCheckAll: false,
    
  };
     
    
   }
   chain_DATA=[];
  product_associated_list=[]
  addons_associated_list=[]
  type_of_integration_list=["Social Media", "Search Engine","Food Delivery","Communication"]
  used_for_Outlet_list=["Per Outlet"]
  free_priced_list=["Yes", "No"]
  revenue_Collection_mode_list=["Per Year","Per Month", "Per Order","One Time", "Package"]






  ngOnInit() {   
    this.uploader = new FileUploader({
      
      url: URL,
      itemAlias: 'chargesintegration',
      allowedFileType:['image']

    });

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);
    this.isLoading = true;
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('FileUpload:uploaded:', item, status, response);
      this.isLoading = false;


      this.response = JSON.parse(response)
      console.log('response', this.response);

      this.multiResponse.push(this.response['chargesintegrationPhotoUrls'][0])
      console.log('multi response', this.multiResponse);
      this.integration.images=this.multiResponse

    }
    this.dropdownList=this.countryname
// video uploading
this.uploader_video = new FileUploader({
  url: URL,
  itemAlias: 'chargesintegration',
  allowedFileType:['video']

});

this.response_video = '';

this.uploader_video.response.subscribe(res => this.response_video = res);

this.uploader_video.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
  console.log('FileUpload:uploaded:', item, status, response);



  this.response_video = JSON.parse(response)
  console.log('response', this.response);

  this.multiResponse_video.push(this.response_video['chargesintegrationPhotoUrls'][0])
  console.log('multi response', this.multiResponse_video);
  this.integration.videos=this.multiResponse_video

}

    
  }
  add_Integration(){
    // let arr=[]
    // console.log(this.addIntegration.controls.avail_countries.value);
    // this.addIntegration.controls.avail_countries.value.forEach(element => {
    //   console.log(element['item_text']);
      
    //   arr.push(element['item_text'])
      
    // });
    // console.log("avail_contries",arr);
    
    // this.addIntegration.patchValue({
    //   avail_countries:arr
    // })

    this.integration.completeData = this.completeData;

    this.integration.images=this.multiResponse
    this.integration.userid=this.loginUserID

console.log(this.integration)
this.charges.updateIntegration(this.integration._id,this.integration).subscribe(data=>{
  console.log(data);
  if(data['success']==true){
    Swal.fire('Integration Updated Successfully', '', 'success')
  this.route.navigate(['/charges'])
  }
  
})
  }
 
  onItemSelect(item: any) {
    this.countryAPI=item['item_text']
    this.addonslist(this.countryAPI)
    this._arr = this.avail_countries
    this.length = this._arr.length;
    console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",this.completeData)
    item.status=true
     let tempData = new RevenueCollectionType(item, "","", "", this.outlet, new Outlet());
      console.log("tempDATA", tempData);
      
       this.completeData.push(tempData);
    // })
    console.log('this._arr :', this.completeData);
  }
  
  onItemDeSelect(item: any) {
    this._arr = this.avail_countries
    this.length = this._arr.length;
    let outlet=[]
    var removeIndex = this.completeData.map(function(i) { 
      console.log("remove", i['country']['item_text']);
      return i['country']['item_text'];
     }).indexOf(item.item_text);
console.log(removeIndex,">>>");
    this.completeData.splice(removeIndex, 1);
    console.log('this._arr :', this.completeData);
  }
  onSelectAll(items: any) { 
    this._arr = this.dropdownList
    this.length = this.dropdownList.length
    console.log(this._arr);
    console.log(this.length);
    this.completeData = [];
    this._arr.forEach(row => {
      let tempData = new RevenueCollectionType(row, "", "","", this.outlet, new Outlet());
      this.completeData.push(tempData);
    })
    console.log('this._arr :', this.completeData);
  }
  
 
  pushIntegrationOutlet(soldPerOutlet,row, index) {
    console.log(row, index)
    this.completeData.forEach((row, i) => {
      if (index == i) {
        let obj = Object.assign({}, this.completeData[i].outletObj)
        if(soldPerOutlet=='Per Outlet'){
        if (this.completeData[i].outletObj.packages != "" && this.completeData[i].outletObj.from != null && this.completeData[i].outletObj.to != null  && this.completeData[i].outletObj.price != null) {
          this.completeData[i].tableData.push(obj);
          this.completeData[i].outletObj.from = this.completeData[i].outletObj.to + 1;
          if (this.completeData[i].outletObj.from >= 2000) {
            Swal.fire("Outlet Range limit '1 to 2000' Only", "", "info")
          }
         // this.completeData[i].outletObj.from = null;
          this.completeData[i].outletObj.to = null;
          this.completeData[i].outletObj.price = null;
          this.completeData[i].outletObj.packages = "";

          if (this.completeData[i].outletObj.price == "NaN") {
            this.completeData[i].outletObj.price = null;
          }
        }
      }else{
        if (this.completeData[i].outletObj.packages != ""  && this.completeData[i].outletObj.price != null) {
          this.completeData[i].tableData.push(obj);
        
         
         // this.completeData[i].outletObj.from = null;
          this.completeData[i].outletObj.to = null;
          this.completeData[i].outletObj.price = null;
          this.completeData[i].outletObj.packages = "";

          if (this.completeData[i].outletObj.price == "NaN") {
            this.completeData[i].outletObj.price = null;
          }
        }

      }

      }
    })
  }
  deleteRow(row,i){
        
    this.completeData[i].tableData.splice(row,1);        
} 
productlist() {
  this.product_associated_list=[]

  this.charges.getProduct().subscribe(data => {
    console.log(data['data']);
    data['data'].forEach(element => {
      if (element.status == true) {
        let countrylist=[]
        //console.log("ele>", element);
        for (let i = 0; i < element.completeData.length; i++) {
          if (element.completeData[i].country != null && element.completeData[i].country != {}) {
            countrylist.push(element.completeData[i].country['item_text'])
            element.country=countrylist
          }
        }
        this.product_associated_list.push({product:element['productName'], country:element['country']})

      }
    });
    console.log("product list>>>>>>", this.product_associated_list);

  })
}
addonslist(item) {
  this.addons_associated_list=[]

  this.charges.getAddonByCountry(item).subscribe(data => {
    console.log(data);
    data['data'].forEach(element => {
      // if (element.status == true) {
       let country
        //console.log("ele>", element);
        for (let i = 0; i < element.completeData.length; i++) {
          if (element.completeData[i].country != null && element.completeData[i].country != {}) {
           
            if(element.completeData[i].country['status']){
            element.country={country:element.completeData[i].country['item_text'], status:element.completeData[i].country['status']}
        this.addons_associated_list.push({addon:element['AddonsName'], country:element.country['country'],status:element.country['status']})
                         
          }
          }
        }


      // }
    });
    console.log("product list>>>>>>", this.addons_associated_list);

  })
}
removeVideo(url) {
  console.log(url);
  this.multiResponse_video.splice(this.multiResponse_video.indexOf(url), 1)
}
removeImage(imgurl) {
  console.log(imgurl);
  this.multiResponse.splice(this.multiResponse.indexOf(imgurl), 1)
}
processMyValue(i): void {

  let numberVal = parseInt(this.completeData[i].outletObj.price).toLocaleString();
    this.completeData[i].outletObj.price = numberVal;
    if(this.completeData[i].outletObj.price=="NaN"){
      this.completeData[i].outletObj.price=null
       }
  
}
processPrice(i,rowindex): void {
  let numberVal = parseInt(this.completeData[i].tableData[rowindex].price).toLocaleString();
  this.completeData[i].tableData[rowindex].price = numberVal;
//  this.completeData[i].outletObj.price
  if( this.completeData[i].tableData[rowindex].price=="NaN"){
    this.completeData[i].tableData[rowindex].price=null;
  }
}
numberOnly(event): boolean {
  const charCode = (event.which) ? event.which : event.keyCode;
  if(charCode==44){
return true;
  }

  
  else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;

}
revenueMode(event, data, i) {
  console.log(event, ">>>>", data, ">>>>", i);
  data.colletionMode = event.value,
    data.tableData = []
  data.outletObj={packages: "", from: 1, to: null, price: null}


}
_soldperoutlet(event, data, i) {
  console.log(event, ">>>>", data, ">>>>", i);
  data.outletObj={packages: "", from: 1, to: null, price: null}
  data.soldPerOutlet = event.value,
    data.tableData = []
  data.colletionMode = ""
}
freeOrPriced(event, data, i) {
  console.log(event, ">>>>", data, ">>>>", i);
  data.soldPerOutlet = "Per Outlet",
    data.tableData = []
    data.outletObj={packages: "", from: 1, to: null, price: null}
  data.colletionMode = ""
  data.freePriced = event.value



}

/*=========================check event key=========================================================================*/
e(event){
  console.log(event);
  
    const charCode = (event.which) ? event.which : event.keyCode;
    if(charCode==101 ||charCode==46 || charCode==43 ||charCode==45){
      return false;
          }
  }
  deleteRowOutlet(row, i, length) {
      console.log(row, length);
      if(row==0 && length==0){
        this.completeData[i].tableData.splice(row, 1);
        //this.completeData[i].outletObj={}
        this.completeData[i].outletObj.from=1
      }else
      if(row==0 && length==1){
        this.completeData[i].tableData.splice(row, 1);
        this.completeData[i].outletObj.from=1
      } else
      if(row==0){
        this.completeData[i].tableData.splice(row, 1);
        this.completeData[i].tableData[row]['from']=1
        
      }else 
      if(row==length-1){
        let from=this.completeData[i].tableData[row]['from']
          this.completeData[i].tableData.splice(row, 1);
          this.completeData[i].outletObj.from=from
        }
      else 
      {
        console.log("row",this.completeData[i].tableData[row]);
  let from=this.completeData[i].tableData[row]['from']
      this.completeData[i].tableData.splice(row, 1);
      this.completeData[i].tableData[row]['from']=from
  console.log("row",this.completeData[i].tableData[row]);
      }
      }
  checkRange(value, tableIndex, tableData, event) {

    if (value !== null) {
      if (tableData.length > 0) {
        tableData.forEach(num => {
          console.log(value, '<>', num.from, '<>', num.to);
          // return (value >= num.from || value >= num.to) ? true : false;
          if ((value <= num.from) || (value <= num.to)) {
            Swal.fire("Please Enter Large Value", "", "warning")
            this.completeData[tableIndex].outletObj.to = null
            event.stopPropagation();
          } else {
            this.completeData.forEach((table: any, index) => {
              if (tableIndex === index) {
                console.log('this.completeData[index] >>', this.completeData[index]);
                if (this.completeData[index].outletObj.from >= value) {
                  Swal.fire("Please Enter Large value", "", "warning")
                  this.completeData[index].outletObj.to = null
                  event.stopPropagation();
                }
              }

            });
          }
        });
      } else {
        this.completeData.forEach((table: any, index) => {
          if (tableIndex === index) {
            console.log('this.completeData[index] >>', this.completeData[index]);
            if (this.completeData[index].outletObj.from >= value) {
              alert('enter greater number');
              event.stopPropagation();
            }
          }
        });
      }
    }
    else {
      Swal.fire("Please Enter Large Value", "", "warning")
    }
  }
  checkRangeInTable(rowindex, value, tableData, event, i, length) {
    console.log(rowindex, length);
    if (value !== null) {
      tableData.forEach(element => {
        console.log(value, '   range >', element.from, element.to)
        if (element.to <= element.from) {
          console.log(value, ">>", element.from);

          Swal.fire("Please Enter Large value", "", "warning")
          this.completeData[i].tableData[rowindex].to = null
          event.stopPropagation();
        }
      });
    }
    else {
      Swal.fire("Please Enter Large value", "", "warning")
    }

    if (rowindex + 1 == length && this.completeData[i].tableData[rowindex].to != null) {

      let val = value + 1
      this.completeData[i].outletObj.from = val

    }
    else {
      if (value + 1 >= this.completeData[i].tableData[rowindex + 1].to || value >= 2000) {
        // intk=length-1
        if (value >= 2000) {
          Swal.fire("Outlet Range limit '1 to 2000' Only", "", "info")
        }
        let k = length - (rowindex + 1)
        this.completeData[i].tableData.splice(rowindex + 1, k);
        if (this.completeData[i].tableData[rowindex].to != null) {
          this.completeData[i].outletObj['from'] = value + 1
        }
      }


      else {
        if (this.completeData[i].tableData[rowindex].to != null)
          this.completeData[i].tableData[rowindex + 1].from = value + 1
      }
    }

  }
  checkValue(event) {
    console.log("event", event);
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    else if (event.target.value > 2000) {
      Swal.fire("Please Enter value less than 2000", "", "warning")
      event.target.value = null
      return false
    } else {
      return true
    }

  }

  ////////////////////////////////////==================check country=========================/////////////////////////////////////////////////////
  checkCountry(event) {
    console.log("prodeuct>>>", event.target.value);

    this.charges.getIntegrationCountry(event.target.value).subscribe(data => {
      console.log(data);
      if (data['success'] == true) {
        let countrylist = []
      //  Swal.fire('Addon Available ', "", "warning")
        console.log(">>", data['data'].length);
        for (let i = 0; i < data['data'].length; i++) {
          console.log(data['data'][i]['_id']);
          if(data['data'][i]['_id']!=this.integration['_id']){
          data['data'][i]['completeData'].forEach(element => {
            countrylist.push(element.country['item_text'])
            element.country = countrylist
          
          });
        }


        }
        console.log("countrylist>>>", countrylist);
        this.mainService.getAllDialCode().subscribe((country: any[]) => {
          let i = 0;
          let temp = [];
          country.forEach(element => {
            if (countrylist.includes(element['name']) === false) {
              i++;
                temp.push({ item_id: i, item_text: element['name'], symbol: element['currency_Symbol'] });;

              console.log("HI");
              
            }
          });
          this.dropdownList = temp;

        });
        

      }
      else {
        this.mainService.getAllDialCode().subscribe((country: any[]) => {
          let i = 0;
          let temp = [];
          country.forEach(element => {
i++;
              temp.push({ item_id: i, item_text: element['name'], symbol: element['currency_Symbol'] });;

          });
          this.dropdownList = temp;

        });
        this.avail_countries=[]
        this.completeData=[]
      }

    })
  }
  checkCountry_whenLoad() {
    this.charges.getIntegrationCountry(this.integration.integrationName).subscribe(data => {
  console.log(data);
  if (data['success'] == true) {
    let countrylist = []
  //  Swal.fire('Addon Available ', "", "warning")
    console.log(">>", data['data'].length);
    for (let i = 0; i < data['data'].length; i++) {
      console.log(data['data'][i]['_id']);
      if(data['data'][i]['_id']!=this.integration['_id']){
      data['data'][i]['completeData'].forEach(element => {
        countrylist.push(element.country['item_text'])
        element.country = countrylist
      
      });
    }


    }
    console.log("countrylist>>>", countrylist);
    this.mainService.getAllDialCode().subscribe((country: any[]) => {
      let i = 0;
      let temp = [];
      country.forEach(element => {
        if (countrylist.includes(element['name']) === false) {
          i++;
            temp.push({ item_id: i, item_text: element['name'], symbol: element['currency_Symbol'] });;

          console.log("HI");
          
        }
      });
      this.dropdownList = temp;

    });


  }
  else {
    this.mainService.getAllDialCode().subscribe((country: any[]) => {
      let i = 0;
      let temp = [];
      country.forEach(element => {
i++;
          temp.push({ item_id: i, item_text: element['name'], symbol: element['currency_Symbol'] });;

      });
      this.dropdownList = temp;

    });
    this.avail_countries=[]
        this.completeData=[]
  }

})
}
openDialogAddon(): void {
  //   this.dialogRef.updatePosition({ top: '50px', left: '50px' });
     const dialogRef = this.dialog.open(AddAddonPopupComponent, {
       width: 'auto',
       height : 'auto',
     
      
     });
 
     dialogRef.afterClosed().subscribe(result => {
       console.log('The dialog was closed');
      this.addonslist(this.countryAPI)
     });
   }

   openDialogProduct(): void {
     const dialogRef = this.dialog.open(AddProductPopupComponent, {
       width: 'auto',
      height:'auto'
     });
 
     dialogRef.afterClosed().subscribe(result => {
       console.log('The dialog was closed');
       this.productlist();
      
     });
   }
   getSingleIntegration(row) {
    ////console.log(row);

    this.charges.getSingleIntegration(row._id).subscribe(data => {
      ////console.log(data);
      if (data['success'] == true) {
        this.charges.viewObject = row
        this.route.navigate(['/charges/view-integration'])
      }

    })
  }
  addRowToInnerTable(countryIndex, outerTableIndex, innerTableIndex, rowIndex, currentObject) {
    console.log("current:", currentObject);
    
    if (this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].from != null && this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].to != null && this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].price != null
      && this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].from != '' && this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].to != '' && this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].price != '') {
    this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].innerTable
      .push(
        Object.assign({}, new PackageOutlet({
          from: currentObject.from,
          to: currentObject.to,
          price: currentObject.price,
        }))
      );
  
    this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].from = this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].to+1;
    if (this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].from >= 2000) {
      Swal.fire("Outlet Range limit '1 to 2000' Only", "", "info")
    }
    this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].price = null;
    this.completeData[countryIndex].perPackageTable[outerTableIndex].table[innerTableIndex].to = null;
  }
  }
  duplicatePackageTable(countryIndex, outerTableIndex) {
    console.log(countryIndex, outerTableIndex);
    
    if(this.completeData[countryIndex].perPackageTable[0].packageValue!=null && this.completeData[countryIndex].perPackageTable[0].packageValue!=''){
    this.completeData[countryIndex].perPackageTable.push(
      Object.assign({}, new OutletPerPackage(undefined))
    );
    console.log(this.completeData);
    }
    
  }
  checkRangeInPackageTable(rowindex, innerindex, outerindex, value, tableData , event, i, length) {
    // console.log(outerindex, innerindex,rowindex, ">",value,">", tableData,">", event,">", i,"?", length);
    console.log(tableData);
    
   

      // this.completeData[i].perPackageTable[outerindex].table[0].innerTable.
      if (value !== null) {
        tableData.forEach(element => {
          console.log(value, '   range >', element.from, element.to)
          if (element.to <= element.from) {
            console.log(value, ">>", element.from);
            console.log("hi3");
            Swal.fire("Please Enter Large value", "", "warning")
  
  
            this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].to = null
            event.stopPropagation();
          }
        });
      }
      else {
        console.log("hi4");
  
        Swal.fire("Please Enter Large value", "", "warning")
      }
  
      if (rowindex + 1 == length && this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].to != null) {
  
        let val = value + 1
        this.completeData[i].perPackageTable[outerindex].table[0].from = val
  
      }
      else {
        if (value + 1 >= this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex + 1].to || value >= 2000) {
          // intk=length-1
          if (value >= 2000) {
            Swal.fire("Outlet Range limit '1 to 2000' Only", "", "info")
          }
          let k = length - (rowindex + 1)
          this.completeData[i].perPackageTable[outerindex].table[0].innerTable.splice(rowindex + 1, k);
          if (this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].to != null) {
            this.completeData[i].perPackageTable[outerindex].table[0]['from'] = value + 1
          }
        }
  
  
        else {
          if (this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].to != null)
            this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex + 1].from = value + 1
        }
      }
   
  }
  checkRangeInpackage(outerindex,value, tableIndex, tableData, event) {
    console.log(value, tableIndex, tableData, event);
    console.log(this.completeData[tableIndex]);
    
        if (value !== null) {
          if (tableData.length > 0) {
            tableData.forEach(num => {
              console.log(value, '<>', num.from, '<>', num.to);
              // return (value >= num.from || value >= num.to) ? true : false;
              if ((value <= num.from) || (value <= num.to)) {
                Swal.fire("Please Enter Large Value4", "", "warning")
                this.completeData[tableIndex].perPackageTable[outerindex].table[0].to = null
                event.stopPropagation();
              } else {
                this.completeData.forEach((table: any, index) => {
                  if (tableIndex === index) {
                    console.log('this.completeData[index] >>', this.completeData[index]);
                    if (this.completeData[index].perPackageTable[outerindex].table[0].from >= value) {
                      console.log("hi");
    
                      Swal.fire("Please Enter Large value3", "", "warning")
                      this.completeData[index].perPackageTable[outerindex].table[0].to = null
                      event.stopPropagation();
                    }
                  }
    
                });
              }
            });
          } else {
            this.completeData.forEach((table: any, index) => {
              if (tableIndex === index) {
                console.log('this.completeData[index] >>', this.completeData[index]);
                if (this.completeData[index].perPackageTable[outerindex].table[0].from >= value) {
                  console.log();
    
                  Swal.fire("Please Enter Large Value1", "", "warning")
                  this.completeData[index].perPackageTable[outerindex].table[0].to = null

                  event.stopPropagation();
                }
              }
            });
          }
        }
        else {
   
          Swal.fire("Please Enter Large Value2", "", "warning")
        }
      }
      processPackagePrice(i,outerindex, innerindex, rowindex, event): void {
        console.log(event.target.value);
        
        let numberVal = parseInt(this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].price).toLocaleString();
        console.log(numberVal);
        
        this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].price = numberVal;
        // //  this.completeData[i].outletObj.price
        if (event.target.value == "NaN") {
          this.completeData[i].perPackageTable[outerindex].table[0].innerTable[rowindex].price = null;
        }
      }
      processPackageOutletPrice(i, outerindex, event): void {
        let numberVal = parseInt(event.target.value).toLocaleString();
        console.log(numberVal);
        
        this.completeData[i].perPackageTable[outerindex].table[0].innerTable.price = numberVal;
        if (event.target.value == 'NaN') {
          this.completeData[i].perPackageTable[outerindex].table[0].innerTable.price = null
        }
    
      }
      deletePackageTable(r, i) {
          this.completeData[i].perPackageTable.splice(r, 1);
      }
      deleteRowInnerTable(i,outerindex,innertableindex,rowindex, length) {
        console.log(rowindex, length); 
       
        // debugger;
        if (rowindex == 0 && length == 0) {
          this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.splice(rowindex, 1);
          //this.completeData[i].outletObj={}
          this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.from = 1
        } else
          if (rowindex == 0 && length == 1) {
            this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.from = 1
            this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.splice(rowindex, 1);
  
          } else
            if (rowindex == 0) {
              this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.splice(rowindex, 1);
              this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable[rowindex]['from'] = 1
    
            } else
              if (rowindex == length - 1 && rowindex !== 0) {
                let from = this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable[rowindex]['from']
                this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.splice(rowindex, 1);
                this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.from = from
              }
              else {
                // console.log("row", this.completeData[i].tableData[rowindex]);
                let from = this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable[rowindex]['from']
                this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable.splice(rowindex, 1);
                this.completeData[i].perPackageTable[outerindex].table[innertableindex].innerTable[rowindex]['from'] = from
                // console.log("row", this.completeData[i].tableData[rowindex]);
              }
        // this.completeData[i].perPackageTable[outerindex].table[0].innerTable.splice(rowindex, 1);
      }
}
