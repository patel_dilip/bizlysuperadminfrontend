import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChargesIntegrationComponent } from './edit-charges-integration.component';

describe('EditChargesIntegrationComponent', () => {
  let component: EditChargesIntegrationComponent;
  let fixture: ComponentFixture<EditChargesIntegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditChargesIntegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChargesIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
