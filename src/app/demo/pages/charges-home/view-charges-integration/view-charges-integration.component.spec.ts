import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChargesIntegrationComponent } from './view-charges-integration.component';

describe('ViewChargesIntegrationComponent', () => {
  let component: ViewChargesIntegrationComponent;
  let fixture: ComponentFixture<ViewChargesIntegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChargesIntegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChargesIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
