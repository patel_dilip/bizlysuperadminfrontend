import { Component, OnInit } from '@angular/core';
import { ChargeService } from 'src/app/_services/charges/charge.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-charges-integration',
  templateUrl: './view-charges-integration.component.html',
  styleUrls: ['./view-charges-integration.component.scss']
})
export class ViewChargesIntegrationComponent implements OnInit {
  integration:any
  constructor(private charge:ChargeService, private router: Router) {
    
  
    console.log(charge.viewObject)
    this.integration=charge.viewObject
    if(charge.viewObject==undefined){
      router.navigate(['/charges'])
    }
     }
   viewIntegration(row) {
    ////console.log(row);

    this.charge.getSingleIntegration(row._id).subscribe(data => {
      ////console.log(data);
      if (data['success'] == true) {
        this.charge.editObject = row
        this.router.navigate(['/charges/update-integration'])
      }

    })
  }

  ngOnInit() {
  }

}
