import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditChargesProductComponent } from './edit-charges-product.component';

describe('EditChargesProductComponent', () => {
  let component: EditChargesProductComponent;
  let fixture: ComponentFixture<EditChargesProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditChargesProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditChargesProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
