import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewChargesProductComponent } from './view-charges-product.component';

describe('ViewChargesProductComponent', () => {
  let component: ViewChargesProductComponent;
  let fixture: ComponentFixture<ViewChargesProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChargesProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChargesProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
