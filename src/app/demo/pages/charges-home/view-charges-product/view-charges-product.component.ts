import { Component, OnInit } from '@angular/core';
import { ChargeService } from 'src/app/_services/charges/charge.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view-charges-product',
  templateUrl: './view-charges-product.component.html',
  styleUrls: ['./view-charges-product.component.scss']
})
export class ViewChargesProductComponent implements OnInit {
  
product:any= {}
country=[]
  constructor(private charges:ChargeService, private router: Router ) {
    console.log(charges.viewObject)
   this.product=charges.viewObject
   if(charges.viewObject==undefined){
    router.navigate(['/charges'])
  }
   }
   

  ngOnInit() {
  }
  viewProduct(row) {
    console.log(row['_id']);

    this.charges.getSingleProduct(row['_id']).subscribe(data => {
    console.log(data);
      if (data['success'] == true) {
        this.charges.editObject = row
        this.router.navigate(['/charges/update-product'])
      }

    })
  }
}
