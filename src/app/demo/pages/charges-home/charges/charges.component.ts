import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ChargeService } from 'src/app/_services/charges/charge.service';
import { map } from 'rxjs/operators';
import { Router, NavigationEnd, RoutesRecognized } from '@angular/router';
import { AngularCsv } from 'angular7-csv/dist/Angular-csv'
import Swal from 'sweetalert2'
import { filter, pairwise } from 'rxjs/operators';
//import { Angular2Csv } from 'angular2-csv';
export interface product {

  productName: string
  createdBy: string
  updatedAt: string
  country: any
  colletionMode: any
  available_addons: string
  soldPerOutlet: string
}

export interface addOn {
  AddonsName: string
  createdBy: string
  updatedAt: string
  country: any
  colletionMode: any
  soldPerOutlet: string
  freePriced: string
}

export interface integration {
  id: number,
  integrationName: string,
  addOnsAssociated: any
  productAssociated: any,
  outletChains: any,
  freePriced: any,
  country: any,
  type_of_integration: any,
  colletionMode: any
}







@Component({
  selector: 'app-charges',
  templateUrl: './charges.component.html',
  styleUrls: ['./charges.component.scss']
})
export class ChargesComponent implements OnInit {
  rowData: any = [
  ];
  rowData_addon: any = [
 ];
 rowData_integration: any = [
 ];

    // ngPrime Table
    //for product
cols: any = [];
outletTable: any = [];
_selectedColumns: any[];
selected1: any;
//for addon
cols_addon: any = [];
outletTable_addon: any = [];
_selectedColumns_addon: any[];
selected1_addon: any;
//for addon
cols_integration: any = [];
outletTable_integration: any = [];
_selectedColumns_integration: any[];
selected1_integration: any;
  rowData1: any = []
  
  private gridApi;
  searchValue1: any

  columnDefs = [
    { headerName: 'Id', field: 'id',  sortable: true,  filter: true,  sortingOrder: ["asc", "desc"] },
    { headerName: 'Product Name', field: 'productName', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Created By', field: 'addedBy.userName', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },      
    { headerName: 'Updated At', field: 'updatedAt', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Collection Mode', field: 'colletionMode', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Country', field: 'country', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Available Addons', field: 'available_addons', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Available Integration', field: 'available_Integration', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Sold Per Outlet', field: 'soldPerOutlet', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Activity', field: 'activity', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    { headerName: 'Status', field: 'status', sortable: true, filter: true, sortingOrder: ["asc", "desc"] },
    ];

  options = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    showTitle: true,
    title: 'Product',
    useBom: true,
    noDownload: false,
    headers: [' Name', 'Revenue Collection Type', 'Countries', 'Addons Associated', 'Sold for outlet', 'Status']
  };
  options2 = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    showTitle: true,
    title: 'Add-ons',
    useBom: true,
    noDownload: false,
    headers: [' Name', 'Revenue Collection Type', 'Product Associated', 'Countries', 'Priced', 'Sold for outlet', 'Status']
  };
  options3 = {
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true,
    showTitle: true,
    title: 'Integration',
    useBom: true,
    noDownload: false,
    headers: [' Name', 'Revenue Collection Type', 'Countries', 'Addons Associated', 'Product Associated', 'Sold for outlet', 'Priced', 'Status']
  };

  displayColumns: string[] = ['id','updatedAt', 'productName', 'country',  'colletionMode', 'soldPerOutlet', 'available_addons', 'available_Integration', 'createdBy', 'status'];
  dataSource: MatTableDataSource<product>;
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  @ViewChild("sort", { static: true }) sort: MatSort;

  //createQuestionSet_DATA: createQuestionSetElement
  displayColumns2: string[] = ['id', 'updatedAt', 'integrationName', 'country', 'colletionMode', 'outletChains', 'type_of_integration','freePriced', 'addOnsAssociated',  'createdBy', 'status'];
  dataSource1: MatTableDataSource<addOn>;
  @ViewChild("paginator1", { static: true }) paginator1: MatPaginator;
  @ViewChild("sort", { static: true }) sort1: MatSort;
  noData: any
  noData1: any
  noData2: any

  //feedbackTemplate_Data: feedbackTemplatElement
  displayColumns1: string[] = ['id', 'updatedAt','AddonsName', 'country',  'colletionMode', 'soldPerOutlet', 'freePriced',  'createdBy','status'];
  dataSource2: MatTableDataSource<integration>;
  @ViewChild("paginator2", { static: true }) paginator2: MatPaginator;
  @ViewChild("sort", { static: true }) sort2: MatSort;
  productData: any;
  product = [];
  filterProduct: any = [];
  integrationData: any;         
  integration = [];
  addonsdata: any;
  addons_data: any;
  add_on = [];
  Exceldata = [];
  index: number;
  previousUrl: string
  product_data = [];
  addon_data = [];
  integration_data = []
  integration_associated_list: any;
  constructor(private charge: ChargeService, private route: Router) {
    this.route.events.pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
      .subscribe((events: RoutesRecognized[]) => {
        //console.log('previous url', events[0].urlAfterRedirects);
        localStorage.setItem('lastUrl', events[0].urlAfterRedirects)
        //console.log('current url', events[1].urlAfterRedirects);

      });
    if (localStorage.getItem('lastUrl') == "/charges/add-addons" || localStorage.getItem('lastUrl') == "/charges/update-addons" || localStorage.getItem('lastUrl') == "/charges/view-addons") {
      this.index = 1
    }
    else if (localStorage.getItem('lastUrl') == "/charges/add-integration" || localStorage.getItem('lastUrl') == "/charges/update-integration" || localStorage.getItem('lastUrl') == "/charges/view-integration") {
      this.index = 2
    }
    else {
      this.index = 0
    }

    this.getProduct();
    this.getAddons();
    this.getIntegration();

   

  }
  onGridReady(params) {
    this.gridApi = params.api;
   
  }

  searchFilter(){
    this.gridApi.setQuickFilter(this.searchValue1)
  }
  getProduct() {

    this.charge.getProduct().subscribe(data => {
console.log(data['data']);

      this.product = [];
      this.filterProduct = [];
      if (data['success'] == false) {
        this.noData = 0
      }
      else if (data['success'] == true) {
        data['data'].forEach(element => {
          let addon=[]
          element.completeData[0].available_addons.forEach(ele => {
            if(ele.country['status']==true){
            addon.push(ele['item_text'])
  }
           })
           element.addon=addon
          
        });
        data['data'].forEach(element => {
          let integration=[]
          element.completeData[0].available_Integration.forEach(ele => {
            if(ele.country['status']==true){

            integration.push(ele['item_text'])
            }  
           })
           element.integration=integration
          
        });
        this.rowData = data['data']

        if(this.rowData!==undefined){
          this.producttableData()
        }
      }
    })


  }
  producttableData() {

    let posProductOrderData = []

    this.rowData.forEach((element, index) => {
      posProductOrderData.push(
        {
          _id:element._id,
          id: index+1,
          createdAt:element.createdAt,
          productName: element.productName,
          country:element.completeData[0].country['item_text'],
          colletionMode:element.completeData[0].colletionMode,
          available_addons:element.addon,
          available_Integration:element.integration,
          addedBy:element.addedBy.userName,
          status:element.completeData[0].country['status']
        }
      )
    });

    this.outletTable = posProductOrderData

    this.cols=[
      {field: "id", header: "Sr. No"},
      {field: "createdAt", header: "	Date & Time"},
      {field:"productName",header:"Product Name"},
      {field: "country", header: "Available In Country"},
      {field:"colletionMode", header:"Collection Mode"},
      {field: "available_addons", header: "Available Addons"},
      {field: "available_Integration", header:"Available Integration"},
      {field: "addedBy", header:"	Created By"},
      {field: "status", header:"Status"},
    

    ]

    this._selectedColumns = this.cols
    console.log(posProductOrderData);
    
  }
  getIntegration() {
    this.charge.getIntegration().subscribe(data => {
      console.log("INTEGRATION:",data);

      
      if (data['success'] == false) {
        this.noData2 = 0
      }
      else

        if (data['success'] == true) {
          data['data'].forEach(element => {
            let addon
            if(element.addOnsAssociated['status']==true){
              element.addon=element.addOnsAssociated['addon']
          
            }

            
          });
          this.rowData_integration = data['data']

      if(this.rowData_integration!==undefined){
        this.integrationtableData()
      }
        }
      //this.dataSource = new MatTableDataSource(this.product);


    })
  }
  integrationtableData(){
    let posIntegrationOrderData = []

    this.rowData_integration.forEach((element, index) => {
  
      posIntegrationOrderData.push(
        {
          _id:element._id,
          id: index+1,
          createdAt:element.createdAt,
          integrationName: element.integrationName,
          country:element.completeData[0].country['item_text'],
          colletionMode:element.completeData[0].colletionMode,
          soldPerOutlet:element.completeData[0].soldPerOutlet,
          TypeOfIntegration:element.TypeOfIntegration,
          freePriced:element.completeData[0].freePriced,
          addedBy:element.addedBy.userName,
          addOnsAssociated:element.addon,
          status:element.completeData[0].country['status']
        }
      )
    
    });

    this.outletTable_integration = posIntegrationOrderData

    this.cols_integration=[
      {field: "id", header: "Sr. No"},
      {field: "createdAt", header: "	Date & Time"},
      {field:"integrationName",header:"Integration Name"},
      {field: "country", header: "Available In Country"},
      {field:"colletionMode", header:"Collection Mode"},
      {field:"soldPerOutlet", header:"Sold per outlet"},
      {field:"TypeOfIntegration", header:"Type"},
      {field: "freePriced", header: "Priced"},
      {field: "addOnsAssociated", header: "Add Ons Associated"},
      {field: "addedBy", header:"	Created By"},
      {field: "status", header:"Status"},

    ]

    this._selectedColumns_integration = this.cols_integration
    console.log(posIntegrationOrderData);


  }
  getAddons() {
    this.add_on = []
    this.charge.getAddons().subscribe(data => {
      console.log("addon", data);
      
      if (data['success'] == false) {
        this.noData1 = 0
      }
      else if (data['success'] == true) {
        this.rowData_addon=data['data']
        if(this.rowData_addon!==undefined){
        this.addontableData()
      }}

    })
  }
  addontableData(){
    let posAddonOrderData = []

    this.rowData_addon.forEach((element, index) => {
  
      posAddonOrderData.push(
        {
          _id:element._id,
          id: index+1,
          createdAt:element.createdAt,
          AddonName: element.AddonsName,
          country:element.completeData[0].country['item_text'],
          colletionMode:element.completeData[0].colletionMode,
          soldPerOutlet:element.completeData[0].soldPerOutlet,
          freePriced:element.completeData[0].freePriced,
          addedBy:element.addedBy.userName,
          status: element.completeData[0].country['status'],
        }
      )
    
    });

    this.outletTable_addon = posAddonOrderData

    this.cols_addon=[
      {field: "id", header: "Sr. No"},
      {field: "createdAt", header: "	Date & Time"},
      {field:"AddonName",header:"Addon Name"},
      {field: "country", header: "Available In Country"},
      {field:"colletionMode", header:"Collection Mode"},
      {field:"soldPerOutlet", header:"Sold per outlet"},
      {field: "freePriced", header: "Priced"},
      {field: "addedBy", header:"	Created By"},
      {field: "status", header:"Status"},

    ]

    this._selectedColumns_addon = this.cols_addon
    console.log(posAddonOrderData);

  }
  ngOnInit() {





  }
  onDestroy() {
    localStorage.clear();
  }
//this is the filter code
applyFilter(event) {
  let collection
  let country
  this.dataSource.data = this.product_data
  //console.log(this.product_data);
  const val = event.target.value.trim().toLowerCase();
  const filter = this.dataSource.data.filter(function (d) {
    //console.log(d);
    let result
    if (d['country'].length > 0) {
      for (let i = 0; i < d['country'].length; i++) {
        //console.log(d['country'][i]);

        country = d['country'][i].toLowerCase().indexOf(val) !== -1
        if (country == true) {
          break;
        }
      }
    }
    if (d['colletionMode'].length > 0) {
      for (let j = 0; j < d['colletionMode'].length; j++) {
        //console.log(d['colletionMode'][j]);

        collection = d['colletionMode'][j].toLowerCase().indexOf(val) !== -1
        if (collection == true) {
          break;
        }
      }
    }
    result = d['productName'].toLowerCase().indexOf(val) !== -1 || collection || country
    return result
  });
  //console.log(filter);
  this.dataSource.data = filter;
}
applyFilter1(event) {
  let collection
  let country
  this.dataSource1.data = this.addon_data
  //console.log(this.dataSource1.data);

  const val = event.target.value.trim().toLowerCase()
  const filter = this.dataSource1.data.filter(function (d) {
    //console.log(d);
    let result
    if (d['country'].length > 0) {
      for (let i = 0; i < d['country'].length; i++) {
        //console.log(d['country'][i]);

        country = d['country'][i].toLowerCase().indexOf(val) !== -1
        if (country == true) {
          break;
        }
      }
    }
    if (d['colletionMode'].length > 0) {
      for (let j = 0; j < d['colletionMode'].length; j++) {
        //console.log(d['colletionMode'][j]);

        collection = d['colletionMode'][j].toLowerCase().indexOf(val) !== -1
        if (collection == true) {
          break;
        }
      }
    }
    result = d['AddonsName'].toLowerCase().indexOf(val) !== -1 || collection || country


    return result
  });

  //console.log(filter);
  this.dataSource1.data = filter;
}
applyFilter2(event) {
  let collection
  let country
  this.dataSource2.data = this.integration_data
  //console.log(this.integration_data);

  const val = event.target.value.trim().toLowerCase();
  const filter = this.dataSource2.data.filter(function (d) {
    //console.log(d);
    let result
    if (d['country'].length > 0) {
      for (let i = 0; i < d['country'].length; i++) {
        //console.log(d['country'][i]);

        country = d['country'][i].toLowerCase().indexOf(val) !== -1
        if (country == true) {
          break;
        }
      }
    }
    if (d['colletionMode'].length > 0) {
      for (let j = 0; j < d['colletionMode'].length; j++) {
        //console.log(d['colletionMode'][j]);

        collection = d['colletionMode'][j].toLowerCase().indexOf(val) !== -1
        if (collection == true) {
          break;
        }
      }
    }
    result = d['integrationName'].toLowerCase().indexOf(val) !== -1 || country || collection

    return result
  });

  //console.log(filter);
  this.dataSource2.data = filter;

}
  changeStatus(id,country, event) {
    Swal.fire({
      title: 'Do you Want to change status?',
      text: "",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes..!'
    }).then((result) => {
      if (result.value) {
        this.charge.statusIntegration(id, country, { "status": event.checked }).subscribe(Response => {
          console.log(Response);
        if (Response['success']) {
          console.log(event.checked);
          
          if (event.checked) {
  
            Swal.fire('Integration Activated Successfully', '', 'success')
            this.getIntegration();
          }
          else {
  
            Swal.fire('Integration Deactivated Successfully ', '', 'warning')
            this.getIntegration();
            this.getProduct();
            this.getAddons();
  
          }
        }
  
        ////console.log(Response);
  
      })
       }
    })
    this.getIntegration();
  }
  changeStatusProduct(id,country, event) {
    console.log(country);
    
    Swal.fire({
      title: 'Do you Want to change status?',
      text: "",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes..!'
    }).then((result) => {
      if (result.value) {
        this.charge.statusProduct(id, country,{ "status": event.checked }).subscribe(Response => {
          if (Response['success']) {
            
            if (event.checked) {
    
              Swal.fire('Product Activated Successfully', '', 'success')
              this.getProduct()
            }
            else {
    
              Swal.fire('Product Deactivated Successfully ', '', 'warning')
              this.getProduct();
              this.getIntegration()
              this.getAddons()
            }
          }
        })      }
    })
    this.getProduct();
  }
  changeStatusAddons(id,country, event ) {
    Swal.fire({
      title: 'Do you Want to change status?',
      text: "",
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes..!'
    }).then((result) => {
      if (result.value) {
        this.charge.statusaddons(id, country, { "status": event.checked }).subscribe(Response => {
          console.log(Response);
          if (Response['success']) {
            if (event.checked) {
    
              Swal.fire('Addons Activated Successfully', '', 'success')
              this.getAddons()
            }
            else {
    
              Swal.fire('Addons Deactivated Successfully ', '', 'warning')
              this.getProduct();
              this.getIntegration()
              this.getAddons()
            }
          }
        })      }
    })
    this.getAddons();
  }
  getSingleProduct(id) {
    console.log(id);

    this.charge.getSingleProduct(id).subscribe(data => {
      ////console.log(data);
      if (data['success'] == true) {
        this.charge.viewObject = data['data']
        this.route.navigate(['/charges/view-product'])
      }

    })
  }
  getSingleAddons(id) {
    ////console.log(row);

    this.charge.getSingleAddons(id).subscribe(data => {
      ////console.log(data);
      if (data['success'] == true) {
        this.charge.viewObject = data['data']
        this.route.navigate(['/charges/view-addons'])
      }


    })
  }
  getSingleIntegration(id) {
    ////console.log(row);

    this.charge.getSingleIntegration(id).subscribe(data => {
      ////console.log(data);
      if (data['success'] == true) {
        this.charge.viewObject = data['data']
        this.route.navigate(['/charges/view-integration'])
      }

    })
  }
  viewProduct(row) {
    ////console.log(row);

    this.charge.getSingleProduct(row._id).subscribe(data => {
      ////console.log(data);
      if (data['success'] == true) {
        this.charge.editObject = row
        this.route.navigate(['/charges/update-product'])
      }

    })
  }
  viewAddons(row) {
    ////console.log(row);

    this.charge.getSingleAddons(row._id).subscribe(data => {
      ////console.log(data);
      if (data['success'] == true) {
        this.charge.editObject = row
        this.route.navigate(['/charges/update-addons'])
      }


    })
  }
  viewIntegration(row) {
    ////console.log(row);

    this.charge.getSingleIntegration(row._id).subscribe(data => {
      ////console.log(data);
      if (data['success'] == true) {
        this.charge.editObject = row
        this.route.navigate(['/charges/update-integration'])
      }

    })
  }

  exportproduct() {
    if (this.product.length > 0) {
      this.Exceldata = []
      // ////console.log(getStates().map(x=>x.StateName).join(","))
      ////console.log("product:",this.product);

      this.product.forEach(element => {
        element.country = element.country.toString()
        element.colletionMode = element.colletionMode.toString()
        element.available_addons = element.available_addons.toString()
        element.soldPerOutlet = element.soldPerOutlet.toString()

        // ////console.log(element);
        var obj = {
          Name: element.productName,
          Revenue_Collection_Type: element.colletionMode,
          Countries: element.country,
          Addons_Associated: element.available_addons,
          Sold_for_outlet: element.soldPerOutlet,
          Status: element.status
        }
        this.Exceldata.push(obj);
      });
      ////console.log("excel data:", this.Exceldata)
      new AngularCsv(this.Exceldata, 'Product', this.options);
    }
    else {
      Swal.fire('Data Not Available to Export', '', 'warning')
    }
    //  new AngularCsv(this.Exceldata, 'My Report' + new Date(), this.options);
    // this.Exceldata = [];
  }
  exportaddons() {
    this.Exceldata = []
    if (this.add_on.length > 0) {
      ////console.log("addons:",this.add_on);

      this.add_on.forEach(element => {
        //console.log(element);
        element.colletionMode = element.colletionMode.toString(),
          element.country = element.country.toString(),
          element.productAssociated = element.productAssociated.toString(),
          element.soldPerOutlet = element.soldPerOutlet.toString()
        var obj = {
          Name: element.AddonsName,
          Revenue_Collection_Type: element.colletionMode,
          productAssociated: element.productAssociated,
          Countries: element.country,
          Sold_for_outlet: element.soldPerOutlet,
          Status: element.status
        }
        this.Exceldata.push(obj);
      });
      ////console.log("excel data:", this.Exceldata)
      new AngularCsv(this.Exceldata, 'Addons', this.options2);
      //  new AngularCsv(this.Exceldata, 'My Report' + new Date(), this.options);
      // this.Exceldata = [];
    } else {
      Swal.fire('Data Not Available to Export', '', 'warning')
    }
  }
  exportintegration() {
    this.Exceldata = []
    if (this.product.length > 0) {
      ////console.log("integration:",this.integration);

      this.integration.forEach(element => {
        // ////console.log(element);
        element.colletionMode = element.colletionMode.toString(),
          element.country = element.country.toString(),
          element.addOnsAssociated = element.addOnsAssociated.toString(),
          element.productAssociated = element.productAssociated.toString(),
          element.soldPerOutlet = element.soldPerOutlet.toString(),
          element.freePriced = element.freePriced.toString()
        var obj = {
          Name: element.integrationName,
          Revenue_Collection_Type: element.colletionMode,
          Countries: element.country,
          addOnsAssociated: element.addOnsAssociated,
          productAssociated: element.productAssociated,
          Sold_for_outlet: element.soldPerOutlet,
          freePriced: element.freePriced,
          Status: element.status
        }
        this.Exceldata.push(obj);
      });
      ////console.log("excel data:", this.Exceldata)
      //  new AngularCsv(this.Exceldata, 'My Report', this.headers);
      new AngularCsv(this.Exceldata, 'Integration' + new Date(), this.options3);
      // this.Exceldata = [];
    }
    else {
      Swal.fire('Data Not Available to Export', '', 'warning')
    }
  }
  onChk() {
    console.log("selected1", this.selected1);
  }

  // show hide columns
  @Input() get selectedColumns(): any[] {
    return this._selectedColumns;
  }

  set selectedColumns(val: any[]) {
    //restore original order
    this._selectedColumns = this.cols.filter((col) => val.includes(col));

    console.log("Set value of selected columns : ", this._selectedColumns);
  }

  /////////////////////////////// addon ///////////////////////////////////////////////////////////
  onChk_addon() {
    console.log("selected1", this.selected1_addon);
  }

  // show hide columns
  @Input() get selectedColumns_addon(): any[] {
    return this._selectedColumns_addon;
  }

  set selectedColumns_addon(val: any[]) {
    //restore original order
    this._selectedColumns_addon = this.cols_addon.filter((col) => val.includes(col));

    console.log("Set value of selected columns : ", this._selectedColumns_addon);
  }
   /////////////////////////////// integration ///////////////////////////////////////////////////////////
   onChk_integration() {
    console.log("selected1", this.selected1_integration);
  }

  // show hide columns
  @Input() get selectedColumns_integration(): any[] {
    return this._selectedColumns_integration;
  }

  set selectedColumns_integration(val: any[]) {
    //restore original order
    this._selectedColumns_integration = this.cols_integration.filter((col) => val.includes(col));

    console.log("Set value of selected columns : ", this._selectedColumns_integration);
  }


}
